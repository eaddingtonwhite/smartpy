filename = "doc/reference.md"

types = {}
literals = {}
commands = {}
no_arg_commands = {}

decorators = ["sp.entry_point", "sp.global_lambda", "sp.utils.view", "sp.sub_entry_point", "sp.offchain_view"]

for line in open(filename, 'r').read().split('\n'):
    if line.startswith('Literals: `sp.'):
        poss = line.index(' ')
        posl = line.index('(')
        posr = line.index(')')
        literals[line[poss+2 : posl]] = line[posl : posr+1] + " [SmartPy literal]"
    elif line.startswith('`sp.T') and line.endswith('::'):
        if '(' in line:
            posl = line.index('(')
            posr = line.index(')')
            types[line[1 : posl]] = line[posl : posr+1] + " [SmartPy type]"
        else:
            posl = line.rindex('`')
            types[line[1 : posl]] = "[SmartPy type]"
    elif line.startswith('`sp.') and line.endswith('::'):
        posl = None
        posr = None
        if '(' in line:
            posl = line.index('(')
            posr = line.rindex(')')
            symbol = line[1 : posl]
            if symbol in decorators:
                commands[line[1 : posl]] = line[posl : posr+1] + " [SmartPy decorator]"
            else:
                commands[line[1 : posl]] = line[posl : posr+1] + " [SmartPy function]"
        elif ':`' in line:
            posr = line.index(':`')
            if ' ' in line:
                posl = line.index(' ')
            else:
                posl = posr
            commands[line[1 : posl]] = line[posl : posr+1] + " [SmartPy control]"
        else:
            posl = line.rindex('`')
            symbol = line[1 : posl]
            if symbol in decorators:
                commands[symbol] = "[SmartPy decorator]"
            else:
                no_arg_commands[symbol] = "[SmartPy constant]"


def pp(d):
    return ',\n '.join("{label: '%s', caption: '%s', detail: '%s', score: 1000}" % (k[3:], k, v.replace("'", "\"")) for k, v in sorted(d.items()))

template = """
const completers = [\n %s,\n\n %s,\n\n %s,\n\n %s];

export default completers;
"""

open('packages/frontend/build/completers.ts', 'w').write(template % (pp(types), pp(literals), pp(no_arg_commands), pp(commands)))
