import React from 'react';

function Michelson(props: any) {
    let name;
    if (!props.name) {
        name = <span className="entrypointVariableName">Parameter</span>;
    } else {
        name = <span className="variableName">{props.name}</span>;
    }
    return (
        <div className="michelson">
            <h4>
                {name}
                <span className="michelsonType">{props.type}</span>
            </h4>
            <div className="michelsonArgs">{props.children}</div>
        </div>
    );
}

export default Michelson;
