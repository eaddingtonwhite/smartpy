import {createContext} from 'react';

const SyntaxContext = createContext(undefined);

export default SyntaxContext;
