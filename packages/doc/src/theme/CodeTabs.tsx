import React from 'react';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

function CodeTabs(props: any) {
    return (
        <Tabs
            defaultValue={props.children[0].props.name || props.children[0].props.language}
            values={props.children.map((child) => {
                const name = child.props.name || child.props.language;
                return { label: name, value: name };
            })}
        >
            {props.children.map((child) => (
                <TabItem value={child.props.name || child.props.language}>
                    <div className="row">
                        <div className="col col--12">{child}</div>
                    </div>
                </TabItem>
            ))}
        </Tabs>
    );
}

export default CodeTabs;
