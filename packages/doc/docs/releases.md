
# Releases

## v0.7.2
|   |   |
|---|---|
| Date | 2021-08-21  |
| Commit  | 7598ba9c6d786680318a38592d3f35178fb1dde5 |
| Link  | [v0.7.2](https://smartpy.io/releases/20210821-7598ba9c6d786680318a38592d3f35178fb1dde5)  |

### Change Log

**`Typescript`**

- Add [Sp.ediv](/types/mutez/#division) expression;
- Extend inlining support;
- Add update script to the [boilerplate](/introduction/start_project);
- Add better support for `as <type>` expressions;
- Add more type checks to the linter.

**`State channels`**

- Misc interface changes.

#### Breaking changes

#### Bug fixes

- Fix `TTimestamp` type-checking in Typescript dialect;
- Fix `TAddress` linting in Typescript dialect;
- Improve the messages of some cryptic errors.

## v0.7.1
|   |   |
|---|---|
| Date | 2021-08-18  |
| Commit  | 13167a59dea2dadd1e302825d0a83c8159ede071 |
| Link  | [v0.7.1](https://smartpy.io/releases/20210818-13167a59dea2dadd1e302825d0a83c8159ede071)  |

### Change Log

- Better error handling;

#### Breaking changes

#### Bug fixes

- Fix lambda packing/unpacking with records and variants;
- Fix `Sp.createContract` return type in the typescript dialect.

## v0.7.0
|   |   |
|---|---|
| Date | 2021-08-09  |
| Commit  | f661148f050609fa1075489aea5536f0a0c6fea8 |
| Link  | [v0.7.0](https://smartpy.io/releases/20210809-f661148f050609fa1075489aea5536f0a0c6fea8)  |

### Change Log

- Includes a beta version of the Typescript syntax called SmartTS;
- New [documentation](https://smartpy.io/docs), it now uses a new system to allow multiple syntaxes;
- Inlined Michelson in SmartPy is now interpreted.

#### Breaking changes

#### Bug fixes

## v0.6.11
|   |   |
|---|---|
| Date | 2021-07-10  |
| Commit  | 06baf4f9ae06f99dc60bab2a01cfee13f4a20c13  |
| Link  | [v0.6.11](https://smartpy.io/releases/20210710-06baf4f9ae06f99dc60bab2a01cfee13f4a20c13)  |

### Change Log

#### Breaking changes

#### Bug fixes

- Metadata builder now works when using lazy entry points;

- Option types now work as expected in explorer.html

## v0.6.10
|   |   |
|---|---|
| Date | 2021-07-08  |
| Commit  | 4662b0f8b1fe2186a243078f9f1ba0a4aa1c6f16  |
| Link  | [v0.6.10](https://smartpy.io/releases/20210708-4662b0f8b1fe2186a243078f9f1ba0a4aa1c6f16)  |

### Change Log

- The online IDE now uses monaco editor, it offers new features to ease the development experience;

- *Compiler:* The Michelson compiler no longer generates IF_SOME macros since they are not handled optimally by the Tezos client.

#### Breaking changes
- Contracts now enforce that initial flags do not appear in contracts (they were not taken into account but they could appear in contracts). They can only appear in the first steps of scenarios. [Flags](./general/flags.md#adding-flags-to-a-contract
- `sp.now` and `sp.level` now keep the state from previous calls as default, instead of resetting to **zero**.

#### Bug fixes

- Fix `token_supply` configuration in the FA2 template;

## v0.6.9

|   |   |
|---|---|
| Date | 2021-06-30  |
| Commit  | d86b17adb6d0fb15c9dc009bf3a62de6665a9ae3  |
| Link  | [v0.6.9](https://smartpy.io/releases/20210630-d86b17adb6d0fb15c9dc009bf3a62de6665a9ae3)  |

### Change Log
  - Adds exception testing, users are now able to link:reference.html#_registering_and_displaying_calls_to_entry_points[test error messages] emitted by failed contract calls;
  - File names in error location;
  - Online IDE now divides tests and compilation targets into two distinct sections;
#### Bug Fixes
  - `originate-contract` CLI command can now be executed from any sub-folder;
  - `scenario.simulation` debugging feature is now working again;
  - Fixes an issue in the explorer page that would cause an exception when interpreting tuples;
  - Allow invalid `packed` bytes to be tested with `run(valid = False)`;
  - **(A work in progress)** New state channel based game platform templates:
    - [Game platform](https://smartpy.io/ide?template=state_channel_games/game_platform.py)
    - [Game tester](https://smartpy.io/ide?template=state_channel_games/game_tester.py)
    - [Game wrapper](https://smartpy.io/ide?template=state_channel_games/model_wrap.py)
    - [Game types](https://smartpy.io/ide?template=state_channel_games/types.py)
    - [Game: Head and Tail model](https://smartpy.io/ide?template=state_channel_games/models/head_tail.py)
    - [Game: Nim model](https://smartpy.io/ide?template=state_channel_games/models/nim.py)
    - [Game: tic-tac-toe model](https://smartpy.io/ide?template=state_channel_games/models/tictactoe.py)
    - [Game: transfer model](https://smartpy.io/ide?template=state_channel_games/models/transfer.py)

## v0.6.8

|   |   |
|---|---|
| Date | 2021-06-09  |
| Commit  | d64964633e98c1bd1fe8beb9f83138185cabdf90  |
| Link  | [v0.6.8](https://smartpy.io/releases/20210609-d64964633e98c1bd1fe8beb9f83138185cabdf90)  |

### Change Log

  - SmartPy CLI now shows all created operations in log.txt (it used to only show recursive operations).
  - More documentation and examples of lazy and updatable entry points.

#### Bug Fixes
  - Fix CLI originate-contract command.

## Previous Releases

View [previous releases](https://smartpy.io/releases.html).
