# Hashing Functions

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

All hashing functions take a [sp.TBytes](/types/bytes) value and return the corresponding hash as a new [sp.TBytes](/types/bytes) value.

See reference [Hash Functions](https://smartpy.io/ide?template=testHashFunctions.py) template.

To get the bytes associated to a given value you need to use `sp.pack(<value>)`. Have a look at [Pack and Unpack](/advanced/pack_unpack) for more information.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

All hashing functions take a [TBytes](/types/bytes) value and return the corresponding hash as a new [TBytes](/types/bytes) value.

See reference [Hash Functions](https://smartpy.io/ts-ide?template=HashFunctions.ts) template.

To get the bytes associated to a given value you need to use `Sp.pack(<value>)`. Have a look at [Pack and Unpack](/advanced/pack_unpack) for more information.

</Snippet>

### blake2b

<Snippet syntax={SYNTAX.PY}>

**`sp.blake2b(<bytes>)`**

```python
someBytes = sp.pack("A String")

hash = sp.blake2b(someBytes)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`Sp.blake2b(<bytes>)`

```typescript
const hash: TBytes = Sp.blake2b("0x00abc" as TBytes)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress...
```

</Snippet>

<MichelsonDocLink placeholder="BLAKE2B" url="https://tezos.gitlab.io/michelson-reference/#instr-BLAKE2B"/>

### sha512

<Snippet syntax={SYNTAX.PY}>

**`sp.sha512(<bytes>)`**

```python
someBytes = sp.pack("A String")

hash = sp.sha512(someBytes)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.sha512(<bytes>)`**

```typescript
const hash: TBytes = Sp.sha512("0x00abc" as TBytes)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress...
```

</Snippet>

<MichelsonDocLink placeholder="SHA512" url="https://tezos.gitlab.io/michelson-reference/#instr-SHA512"/>

### sha256

<Snippet syntax={SYNTAX.PY}>

**`sp.sha256(<bytes>)`**

```python
someBytes = sp.pack("A String")

hash = sp.sha256(someBytes)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.sha256(<bytes>)`**

```typescript
const hash: TBytes = Sp.sha256("0x00abc" as TBytes)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress...
```

</Snippet>

<MichelsonDocLink placeholder="SHA256" url="https://tezos.gitlab.io/michelson-reference/#instr-SHA256"/>

### sha3

<Snippet syntax={SYNTAX.PY}>

**`sp.sha3(<bytes>)`**

```python
someBytes = sp.pack("A String")

hash = sp.sha3(someBytes)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.sha3(<bytes>)`**

```typescript
const hash: TBytes = Sp.sha3("0x00abc" as TBytes)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress...
```

</Snippet>


<MichelsonDocLink placeholder="SHA3" url="https://tezos.gitlab.io/michelson-reference/#instr-SHA3"/>

### keccak

<Snippet syntax={SYNTAX.PY}>

**`sp.keccak(<bytes>)`**


```python
someBytes = sp.pack("A String")

hash = sp.keccak(someBytes)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.keccak(<bytes>)`**

```typescript
const hash: TBytes = Sp.keccak("0x00abc" as TBytes)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress...
```

</Snippet>

<MichelsonDocLink placeholder="KECCAK" url="https://tezos.gitlab.io/michelson-reference/#instr-KECCAK"/>
