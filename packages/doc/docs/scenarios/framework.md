import SyntaxSelector from '@theme/Syntax/Selector';
import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';

# General Framework

Scenarios describe a sequence of actions:
 - Originating contracts;
 - Computing expressions;
 - Calling entry points;
 - and more...

<Snippet syntax={SYNTAX.PY}>

They are directly used in SmartPy tests.

SmartPy currently supports two uses for scenarios: `tests` and `compilations`.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

They are directly used in SmartTS tests.

SmartTS currently supports two uses for scenarios: `tests` and `compilations`.

</Snippet>

## Adding a Test

<Snippet syntax={SYNTAX.PY}>

**`@sp.add_test` interface:**
```ts
@sp.add_test(name, shortname=None, profile=False, is_default=True)
```
It decorates a test function.

| Property | Description | Default Value |
|--:|:--:|:---|
| name | Test Name | **required** |
| shortname | An optional parameter. Short names need to be unique. Used in smartpy-cli outputs. | None |
| profile | Computes and pretty-prints profiling data. | False |
| is_default | Determines if the test is performed by default when evaluating all tests. | True |

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Dev.test` interface:**
```ts
// `Dev.test` interface
interface Dev {
    test: (
        {
            name: string,
            shortName?: string,
            profile?: boolean
            enabled?: boolean,
            flags?: string[][]
        },
        () => void
    ) => void;
}
```

| Property | Description | Default Value |
|--:|:--:|:---|
| name | Test Name | **required** |
| shortName | An optional parameter. Short names need to be unique. Used in smartpy-cli outputs. | undefined |
| profile | Computes and pretty-prints profiling data. | false |
| enabled | Determines if the test is performed by default when evaluating all tests. | true |
| flags | [Configuration flags](./../general/flags#flags) | [] |

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

### Test Example

<Snippet syntax={SYNTAX.PY}>

```py
@sp.add_test(name = "First test")
​def test():
    ​# We define a test scenario, called scenario,
    ​# together with some outputs and checks
    ​scenario = sp.test_scenario()
    ​# We first define a contract and add it to the scenario
    ​c1 = MyContract(12, 123)
    ​scenario += c1
    ​# And send messages to some entry points of c1
    ​scenario += c1.my_entrypoint(12)
    ​scenario += c1.my_entrypoint(13)
    ​scenario += c1.my_entrypoint(14)
    ​scenario += c1.my_entrypoint(50)
    ​scenario += c1.my_entrypoint(50)
    ​scenario += c1.my_entrypoint(50).run(valid = False) # this is expected to fail
    ​# Finally, we check the final storage of c1
    ​scenario.verify(c1.data.myParameter1 == 151)
    ​# and its balance
    ​scenario.verify(c1.balance, sp.tez(0))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```ts
Dev.test({
    name: "First test"
}, () => {
    // Originates a contract
    const c1 = Scenario.originate(new MyContract({ value1: 12, value2: 123 }));
    // Calls an entry point called `my_entrypoint` from contract `c1`
    Scenario.transfer(c1.my_entrypoint(12));
    Scenario.transfer(c1.my_entrypoint(13));
    Scenario.transfer(c1.my_entrypoint(14));
    Scenario.transfer(c1.my_entrypoint(50));
    Scenario.transfer(c1.my_entrypoint(50));
    // This call is expected to fail
    Scenario.transfer(c1.my_entrypoint(12), { valid: false });
    // Finally, we check the final storage of c1
    Scenario.verify(c1.storage.myParameter1 === 151);
    // And its balance
    Scenario.verify(c1.balance === 0);
});
```

</Snippet>


<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>
