# Utils

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';

This section includes a few util functions that can help you to ease the development.

## Methods

<Snippet syntax={SYNTAX.PY}>

### mutez_to_nat

Convert `TMutez` to `TNat`

```python
sp.utils.mutez_to_nat(...)
```

### nat_to_mutez

Convert `TNat` to `TMutez`

```python
sp.utils.nat_to_mutez(n)    # sp.nat(1) => sp.mutez(1)
# or
sp.utils.nat_to_tez(n)      # sp.nat(1) => sp.mutez(1000000)
```

### bytes_of_string

Encode a constant string as sp.TBytes.

```python
sp.utils.bytes_of_string("A String") # => 0x4120537472696e67
```

### metadata_of_url

A Simple alias for `sp.big_map({"" : sp.utils.bytes_of_string(url)})`.

```python
sp.utils.metadata_of_url("ipfs://...")
```

### same_underlying_address

It returns a boolean that informs if an address `A%foo` has the same underlying address as `A`.

```python
sp.verify(
    sp.utils.same_underlying_address(
        sp.address("KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF%foo"),
        sp.address("KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF")
    ),
    message = "Not the same underlying address"
)
```

### wrap_entry_point

`sp.utils.wrap_entry_point(name, entry_point)`

A wrapper to prepare the a python function to be used as an update for entry point name (which must be a constant string).

```python
def f(self):
    sp.verify(self.data.valid, "NOT VALID")

sp.utils.wrap_entry_point("ep", f)
```

### vector, matrix, cube

There is no array in SmartPy because they are missing in Michelson, we usually use maps instead.

There are three helper functions:

`sp.utils.vector(..)`, `sp.utils.matrix(..)` and `sp.utils.cube(..)` that take respectively a list, a list of lists and a list of lists of lists and return maps.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>
