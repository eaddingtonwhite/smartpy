# Metadata Builder

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';

Check metadata guides [here](/guides/metadata/build_metadata).

## Build metadata

Generate a JSON metadata document for string name containing an arbitrary constant Python `expression` converted into JSON.

<Snippet syntax={SYNTAX.PY}>

```python
# A python dictionary that contains metadata entries
metadata = {
    "name": "Contract Name",
    "description": "A description about the contract",
    "version": 1,
    "views" : [self.get_x, self.get_storage],
}

self.init_metadata('SOME CONTRACT', metadata)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
@MetadataBuilder
metadata = {
    name: 'SOME CONTRACT',
    decimals: 1,
    views: [this.state, this.increment, this.decrement],
    preferSymbol: true,
    source: {
        tools: ['SmartPy'],
    },
};
```

</Snippet>

## Define off-chain views

A decorator which defines an off-chain view.

<Snippet syntax={SYNTAX.PY}>

```python
@sp.offchain_view(pure = False, doc = None)
```

It has two optional parameters:

- `pure` (**default is False**): Defines the purity of view (dependent only on storage and parameters)
- `doc`: Sets the view documentation. If doc is `None`, the documentation is the docstring of the method

```python
​@sp.offchain_view(pure = True)
​def get_x(self, params):
    ​"""blah blah ' fdsfds"""
    ​sp.result(sp.record(a = self.data.x, b = 12 + params))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
@OffChainView({ pure: false, description: 'Decrement value' })
```

It has two optional parameters:

- `pure` (**default is False**): Defines the purity of view (dependent only on storage and parameters)
- `doc`: Sets the view documentation. If doc is `None`, the documentation is the docstring of the method

```typescript
@OffChainView({ pure: true, description: 'Increment value' })
increment = () => {
    return this.storage + 1;
};
```

</Snippet>

## Utils

<Snippet syntax={SYNTAX.PY}>

Convert a `URL` into a metadata `big_map`.

```python
sp.utils.metadata_of_url(url)
```
A simple alias for `sp.big_map({ "" : sp.utils.bytes_of_string(url) })`

</Snippet>


## Example

<Snippet syntax={SYNTAX.PY}>

```python
import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self, **kargs):
        self.init(**kargs)

        # A python dictionary that contains metadata entries
        metadata = {
            "name": "Contract Name",
            "description": "A description about the contract",
            "version": 1,
            "views" : [self.get_x, self.get_storage],
        }

        # Helper method that builds the metadata and produces the JSON representation as an artifact.
        self.init_metadata("example1", metadata)

    @sp.offchain_view(pure = True)
    def get_x(self, params):
        """blah blah ' some documentation """
        sp.result(sp.record(a = self.data.x, b = 12 + params))

    @sp.offchain_view(doc = "The storage")
    def get_storage(self):
        sp.result(self.data.x)

@sp.add_test(name = "Metadata")
def test():
    scenario = sp.test_scenario()
    c1 = MyContract(x=1, metadata = sp.utils.metadata_of_url("ipfs://Qme9L4y6ZvPwQtaisNGTUE7VjU7PRtnJFs8NjNyztE3dGT"))
    scenario += c1
    c1.change_metadata(sp.utils.bytes_of_string(""))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
@Contract
export class OffChainViews {
    constructor(public storage: TInt = /* Default value */ 1) {}

    @OffChainView({ name: 'getStorageState', description: 'Get Storage State' })
    state = () => {
        return this.storage;
    };

    @OffChainView({ pure: true, description: 'Increment value' })
    increment = () => {
        return this.storage + 1;
    };

    @OffChainView({ pure: true, description: 'Decrement value' })
    decrement = () => {
        return this.storage - 1;
    };

    @MetadataBuilder
    metadata = {
        name: 'SOME CONTRACT',
        decimals: 1,
        views: [this.state, this.increment, this.decrement],
        preferSymbol: true,
        source: {
            tools: ['SmartPy'],
        },
    };
}

Dev.test({ name: 'OffChainViews' }, () => {
    Scenario.originate(new OffChainViews());
});

```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>
