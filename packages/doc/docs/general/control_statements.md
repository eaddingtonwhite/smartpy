# Control Statements

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

Since Python doesn't allow its control statements to be overloaded, certain language constructs are desugared by a pre-processor: `sp.if`, `sp.else`, `sp.for`, `sp.while` are SmartPy commands. (The desugared version is `sp.if_`, `sp.else_`, `sp.for_` and `sp.while_`)

</Snippet>

## If statement

<Snippet syntax={SYNTAX.PY}>

A `if` condition that is evaluated on-chain.

```python
sp.if x == 10 :
    self.data.result = 0
sp.else:
    self.data.result += x
```

The desugared version is

```python
with sp.if_(x == 10):
    self.data.result = 0
with sp.else_():
    self.data.result += x
```

</Snippet>


<Snippet syntax={SYNTAX.TS}>

```typescript
if (x > 10) {
    this.storage.result = 0
} else if (x > 5) {
    this.storage.result += x
} else {
    this.storage.result = this.storage.result + x * 1
}
```

</Snippet>


<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="IF" url="https://tezos.gitlab.io/michelson-reference/#instr-IF"/>

## For statement

<Snippet syntax={SYNTAX.PY}>

A `for` loop that is evaluated on-chain.

```python
sp.for x in params:
    self.data.result += x
```

The desugared version is

```python
with sp.for_("x", params) as x:
    self.data.result += x
```

</Snippet>


<Snippet syntax={SYNTAX.TS}>

```typescript
for (let x of params) {
    this.storage.result += x
}
```

```typescript
for (let i = 0; i < y; i += 2) {
    this.storage.value += i;
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="ITER" url="https://tezos.gitlab.io/michelson-reference/#instr-ITER"/> and <MichelsonDocLink placeholder="MAP" url="https://tezos.gitlab.io/michelson-reference/#instr-MAP"/>

## While statement

<Snippet syntax={SYNTAX.PY}>

A `while` loop that is evaluated on-chain.

```python
sp.while 1 < y.value:
    self.data.value += 1
    y.value //= 2
```

The desugared version is

```python
with sp.while_(1 < y.value):
    self.data.value += 1
    y.value //= 2
```


</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
while (1 < y) {
    this.storage.result += 1;
    y += 1;
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="LOOP" url="https://tezos.gitlab.io/michelson-reference/#instr-LOOP"/>
<br/>
<br/>

<Snippet syntax={SYNTAX.PY}>

:::note
If we use e.g. `sp.if` instead of a plain `if`, the result will be a SmartPy conditional instead of a Python one.
:::

SmartPy conditionals are executed once the contract has been constructed and has been deployed or is being simulated. On the other hand, Python conditionals are executed immediately. Therefore the condition after the `if` cannot depend on the state of the contract. When in doubt, always use the `sp.` prefix inside a smart contract.

</Snippet>
