# Asserting Error Conditions

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';

## Assert

Check that the boolean expression `condition` evaluates to `True` and raises an error if it doesn’t. This is useful to prevent an entry point from proceeding if certain conditions are not met (**e.g.** in a contract that manages accounts a client cannot withdraw more money than they deposited).

An optional parameter `message` is raised if condition is not met. When message is not present, an exception of the form `WrongCondition: …​` is raised depending on the [Exception Optimization Level](/general/flags#exception-optimization-levels).

<Snippet syntax={SYNTAX.PY}>


```python
sp.verify(condition, message = "Condition `<condition>` is false")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Sp.verify(condition, "Condition `<condition>` is false")
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

## Equality Checking

It serves the same purpose by checking equality between `v1` and `v2`. This works on both comparable and non-comparable types.

`Equality Checking` packs `v1` and `v2` into bytes and then compares the resulting bytes.

<Snippet syntax={SYNTAX.PY}>

```python
sp.verify_equal(v1, v2, message = "Condition `<condition>` is false"​)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Sp.verifyEqual(v1, v2, "Condition `<condition>` is false"​)
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>
