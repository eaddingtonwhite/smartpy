# Entry Points

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

An entry point is a method of a contract class that can be called from the outside. Entry points need to be marked with the `@sp.entry_point` decorator.

For example, the following entry point checks that the argument given is larger than 2:

```python
​@sp.entry_point
​def check_big_enough(self, params):
    ​sp.verify(params > 2)
```

See reference [Store Value](https://smartpy.io/ide?template=storeValue.py) template for more entry points examples.

`@sp.entry_point` can also take optional parameters such as an alternative string `name`, bool `private`, `lazify` and `lazy_no_code` flags.

|Parameter     | Type   | Description                                                                          |
|--------------|--------|--------------------------------------------------------------------------------------|
| name         | string | Defines the entry point name (**By default, the function name is used as name**)     |
| private      | bool   | Defines a entry point that is only available in tests                                |
| lazify       | bool   | Sets the entry point as upgradable (**The logic code gets stored in a big_map**)     |
| lazy_no_code | bool   | Sets the entry point as upgradable (**Doesn't store any code logic at origination**) |


```python
​@sp.entry_point(name = "another_name", private = True, lazify = False, lazy_no_code = False)
​def check_big_enough(self, params):
    ​sp.verify(params > 2)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

An entry point is a method of a contract class that can be called from the outside. Entry points need to be marked with the `@EntryPoint` decorator.

For example, the following entry point checks that the argument given is larger than 2:

```typescript
​@EntryPoint
​check_big_enough(param: TNat) {
    ​Sp.verify(param > 2)
}
```

See reference [Store Value](https://smartpy.io/ts-ide?template=StoreValue.ts) template for more entry points examples.

`@EntryPoint` can also take optional parameters such as an alternative string `name`, bool `mock`, `lazify` and `lazy_no_code` flags.

|Parameter     | Type   | Description                                                                          |
|--------------|--------|--------------------------------------------------------------------------------------|
| name         | string | Defines the entry point name (**By default, the function name is used as name**)     |
| mock         | bool   | Defines a entry point that is only available in tests                                |
| lazify       | bool   | Sets the entry point as upgradable (**The logic code gets stored in a big_map**)     |
| lazy_no_code | bool   | Sets the entry point as upgradable (**Doesn't store any code logic at origination**) |


```typescript
​@EntryPoint({
    name: "another_name",
    mock: true,
    lazify: false,
    lazy_no_code: false
})
​check_big_enough(param: TNat) {
    ​Sp.verify(param > 2)
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Dummy Entry Points

<Snippet syntax={SYNTAX.PY}>

It is possible to create entry points for testing that only appear in SmartPy but are not included in the Michelson contract.

They help to implement checks in SmartPy tests after the contract origination. One can use the entire SmartPy machinery such as `sp.verify`, etc.

See reference [Private Entry Point](https://smartpy.io/ide?template=private_entry_point.py) template.

This is also useful to build custom UI through simulation by doing:

```python
@sp.entry_point(private = True)
​def set_y(self, params):
    ​self.data.y = param
```
</Snippet>

<Snippet syntax={SYNTAX.TS}>

It is possible to create entry points for testing that only appear in SmartPy but are not included in the Michelson contract.

They help to implement checks in SmartPy tests after the contract origination. One can use the entire SmartPy machinery such as `Sp.verify`, etc.

This is also useful to build custom UI through simulation by doing:

```typescript
@EntryPoint({ mock: true })
set_y(param: TNat) {
    this.storage.y = param
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Return values

<Snippet syntax={SYNTAX.PY}>

Entry points do not return values in Michelson.

SmartPy respects this constraint but allows other functions to return values.
These functions typically use `sp.result(value)` to return values.

See [Lambdas](/types/lambdas) for examples on how to use `sp.global_lambda` and `sp.sub_entry_point`.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Entry points do not return values in Michelson.

SmartTS respects this constraint but allows other functions to return values.

See [Lambdas](/types/lambdas) for examples on how to use `@GlobalLambda`.

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


## Lazy and updatable entry points

Lazy and updatable entry points are similar to regular SmartPy entry points with two special abilities:

- They are loaded on-demand (this reduces gas usage)
- Can be updated with specific operators

<Snippet syntax={SYNTAX.PY}>

See reference [Lazy entry points](https://smartpy.io/ide?template=lazy_entry_points.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress...
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


### Default behavior

Entry points can all be "lazified" by default by using the flag `"lazy-entry-points"` but it’s usually smarter to select the lazy entry points.

<Snippet syntax={SYNTAX.PY}>

See example in [Send back](https://smartpy.io/ide?template=send_back.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress...
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

### Declaring a lazy entry point

We can ask SmartPy to "lazify" an entry point:

<Snippet syntax={SYNTAX.PY}>

```python
@sp.entry_point(lazify = True)
```

Furthermore, a lazy entry point can be initially excluded from the big map:

```python
@sp.entry_point(lazify = True, lazy_no_code = True)
```
In this case, calling it will result in an error until it has been updated.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
@Entry_point({ lazify: true })
```

Furthermore, a lazy entry point can be initially excluded from the big map:

```typescript
@Entry_point({ lazify: true, lazy_no_code: true })
```
In this case, calling it will result in an error until it has been updated.

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

### Updating entry points


<Snippet syntax={SYNTAX.PY}>

Update the lazy entry point called `name` (which must be a constant string) with the code in `entry_point`.

```python
sp.set_entry_point(name, entry_point)
```

The code is stored as a lambda inside a big map that is part of the contract's storage. Such an entry point can be updated (from inside any non-lazy entry point), e.g.:

```python
@sp.entry_point
def update_ep(self, new_code):
    sp.verify(sp.sender == self.data.admin)
    sp.set_entry_point("ep", new_code)
```

In order to perform an update in a scenario, we first define the new entry point as a function, e.g.:

```python
def f(self, params):
    sp.verify(params == 42)
```

Wrapping a python function to be used as an update for entry point `name` (which must be a constant string).

```python
ep_update = sp.utils.wrap_entry_point(name, entry_point)
c.update_ep(ep_update)
```

Returning a boolean expression that indicates whether an entry point called `name` (which must be a constant string) is presently part of the big map.

```python
sp.has_entry_point(name)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>

## Calling Entry Points

### Entry points without parameters

All entry points require an input type, this even applies when no parameters are provided.

The type used in this situations is `TNat`.

<Snippet syntax={SYNTAX.PY}>

```python
class MyContract(sp.Contract):
    @sp.entry_point
    def ep1():
        contract = sp.contract(sp.TUnit, sp.self_address, "ep2").open_some()
        sp.transfer(sp.unit, sp.tez(0), contract)

    @sp.entry_point
    def ep2():
        pass
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
class MyContract {
    @EntryPoint
    ep1() {
        const contract: TUnit = Sp.contract(Sp.selfAddress, "ep2")
        Sp.transfer(Sp.unit, 0, contract)
    }

    @EntryPoint
    ep2() {
        // ...
    }
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>


## Define a Default Entry Point

Default entry points are useful to implement the same transfer behavior as `implicit accounts`.

```
(unit %default)
```

<Snippet syntax={SYNTAX.PY}>

```python
class MyContract(sp.Contract):
    @sp.entry_point
    def default():
        pass
```

**Or as an `@sp.entry_point` argument**

```python
class MyContract(sp.Contract):
    @sp.entry_point(name = "default")
    def ep():
        pass
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
class MyContract {
    @EntryPoint
    default() {
        #
    }
}
```

**Or as an `@EntryPoint` argument**

```typescript
class MyContract {
    @EntryPoint({ name: "default" })
    ep() {
        #
    }
}
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>
