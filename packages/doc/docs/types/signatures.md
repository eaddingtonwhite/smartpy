# Signatures

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';


<Snippet syntax={SYNTAX.PY}>

The type of signatures in SmartPy is [sp.TSignature](/general/types#signature).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="signature" url="https://tezos.gitlab.io/michelson-reference/#type-singature"/>.

See reference [Signatures](https://smartpy.io/ide?template=testCheckSignature.py) and [State Channels](/guides/state_channels/overview).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of signatures in SmartTS is [TSignature](/general/types#signature).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="signature" url="https://tezos.gitlab.io/michelson-reference/#type-singature"/>.

</Snippet>

## Operations

### Check signature

<Snippet syntax={SYNTAX.PY}>

**`sp.check_signature(<public_key>, <signature>, <original_content_bytes>)`**

Determine whether the signature `s` (a [sp.TSignature](/general/types#signature) value) has been produced by signing `b` (a [sp.TBytes](/general/types#bytes) value) with the private key corresponding to `k` (a [sp.TKey](/general/types#key) public key value).

#### Example

```python
k = sp.key("edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav")
s = sp.signature("edsigu3QszDjUpeqYqbvhyRxMpVFamEnvm9FYnt7YiiNt9nmjYfh8ZTbsybZ5WnBkhA7zfHsRVyuTnRsGLR6fNHt1Up1FxgyRtF")
b = sp.bytes("0x00aabb")

sp.check_signature(k, s, b)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.checkSignature(<public_key>, <signature>, <original_content_bytes>)`**

Determine whether the signature `s` (a [TSignature](/general/types#signature) value) has been produced by signing `b` (a [TBytes](/general/types#bytes) value) with the private key corresponding to `k` (a [TKey](/general/types#key) public key value).

#### Example

```typescript
const k: TKey = "edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav"
const s: TSignature = "edsigu3QszDjUpeqYqbvhyRxMpVFamEnvm9FYnt7YiiNt9nmjYfh8ZTbsybZ5WnBkhA7zfHsRVyuTnRsGLR6fNHt1Up1FxgyRtF"
const b: TBytes = "0x00aabb"

Sp.checkSignature(k, s, b)
```

</Snippet>


<MichelsonDocLink placeholder="CHECK_SIGNATURE" url="https://tezos.gitlab.io/michelson-reference/#instr-CHECK_SIGNATURE"/>

### Make signature

<Snippet syntax={SYNTAX.PY}>

**`sp.make_signature(secret_key, message, message_format = 'Raw')`**

Forge a signature compatible with `sp.check_signature(...)`; the `message` is a [sp.TBytes](/general/types#bytes) value (usually the result of an `sp.pack` call), the `message_format` can also be `"Hex"` in which case the message will be interpreted as an hexadecimal string.

#### Example

```python
sp.make_signature(
    "edskRq1xuW7TCYzdFm1JQLi1Hz4MNDVP6ukQHVEEh3bVqyuzv7pXXjaGsXZuMbwtd3kQFp3LQ7GQzkLeprNEijKhQKzsxrYrUz",
    sp.bytes("0x00aabb"),
    message_format = 'Raw'
)
```

`sp.make_signature` is not available for compilation to Michelson (a smart contract cannot manipulate secret keys). It can only be used in [Tests and Scenarios](/scenarios/testing).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.makeSignature(<secret_key>, <message>)`**

Forge a signature compatible with `Sp.checkSignature(...)`; the `message` is a [TBytes](/general/types#bytes) value (usually the result of an `Sp.pack` call).

#### Example

```typescript
Scenario.makeSignature("edskRq1xuW7TCYzdFm1JQLi1Hz4MNDVP6ukQHVEEh3bVqyuzv7pXXjaGsXZuMbwtd3kQFp3LQ7GQzkLeprNEijKhQKzsxrYrUz", '0x00aabb')
```

`Sp.makeSignature` is not available for compilation to Michelson (a smart contract cannot manipulate secret keys). It can only be used in [Tests and Scenarios](/scenarios/testing).

</Snippet>
