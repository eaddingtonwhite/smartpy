# Records

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

Records in SmartPy are of type [sp.TRecord](/general/types#record)(`**kargs`) where `kargs` is a Python `dict` of SmartPy types indexed by strings.

They generalize the Michelson type <MichelsonDocLink placeholder="pair" url="https://tezos.gitlab.io/michelson-reference/#type-pair"/>.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Records in SmartPy are of type [TRecord](/general/types#record)<{`...kargs`}> where `kargs` is a Typescript object.

They generalize the Michelson type <MichelsonDocLink placeholder="pair" url="https://tezos.gitlab.io/michelson-reference/#type-pair"/>.

</Snippet>

## Literals

<Snippet syntax={SYNTAX.PY}>

<b>

`sp.record(**kargs)`

</b>

Introduces a record of type [sp.TRecord](/general/types#record)(field1 = [sp.TNat](/general/types#nat), field2 = [sp.TString](/general/types#string))

#### Example

```python
aRecord = sp.record(field1 = 1, field2 = "A string")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`{ ...object }`**

Introduces a record of type [TRecord](/general/types#record)< { field1: [sp.TNat](/general/types#nat), field2: [sp.TString](/general/types#string) }>

#### Example

```typescript
const aRecord: TRecord<{ field1: sp.TNat, field2: sp.TString }> = { field1 = 1, field2 = "A string")
```

</Snippet>

## Layouts

The default layout can be changed by specifying **default_record_layout** [flag](../general/flags#flags-with-arguments).

<Snippet syntax={SYNTAX.PY}>

**`.layout(layout)`**

A record type, i.e. something of the form `sp.TRecord(...)`, can be used to define a record type with a layout by doing:
```python
t = sp.TRecord(owner = sp.TAddress, operator = sp.TAddress, token_id = sp.TString)
t_with_layout = t.layout(("owner", ("operator", "token_id")))
```

**`.right_comb()`**

Like **.layout(...)** but the geometry used is `(f1, (f2, .. (f_{k-1}, f_k)))))`, the list of fields is determined automatically and sorted alphabetically.

**`.with_fields(fields)`**

Adds some fields to the `TRecord(...)` where `fields` is a Python (string, type)-dictionary of fields.

**`.without_fields(fields)`**

Removes some fields from the `TRecord(...)` where `fields` is a Python list of fields.

</Snippet>


<Snippet syntax={SYNTAX.TS}>

**`TRecord<{ ...fields }, <layout>>`**

A record type, i.e. something of the form `TRecord<{ ...fields }, layout>`, can be used to define a record type with a layout by doing:

```typescript
type aType = TRecord<{
    owner   : TAddress;
    operator: TAddress;
    token_id: TString;
}, ["owner", ["operator", "token_id"]]>
```

**`Layout.right_comb`**

Like **["...", "..."]** but the geometry used is `(f1, (f2, .. (f_{k-1}, f_k)))))`, the list of fields is determined automatically and sorted alphabetically.

```typescript
type aType = TRecord<{
    owner   : TAddress;
    operator: TAddress;
    token_id: TString;
}, Layout.right_comb>
```

</Snippet>

## Accessing fields

<Snippet syntax={SYNTAX.PY}>

**`<record>.<field>`**

If `record` is a record and `field` one of its fields, we can obtain the field's value by writing `<record>.<field>`.

#### Example

```python
record = sp.record(x = 1, y = 2)
value  = record.x # 1
```

Furthermore, records can be matched using the following command:

- `sp.match_record(x, *fields)`

    If `x` is a SmartPy record (i.e. a value of type `sp.TRecord(...)`), this returns a Python tuple of selected record fields.  The list of fields can be all the record's fields or just some of them.  This is typically used as follows:

    ```python
    fld1, fld2, fld3 = sp.match_record(x, "fld1", "fld2", "fld3" )
    ```

- `sp.modify_record(x, name = None)`

    A variant of this command allows modifying a record that is held in the contract storage or in a local variable:

    ```python
    with sp.modify_record(self.data, "data") as data:
        ...
        data.x = 12
        data.y += 1
    ```

This command tells the SmartPy compiler to open the record, handle its fields independently and recreate the record afterwards in a _linear_ way.<br/>
This command is mostly useful when dealing with tickets.

See reference [Modify](https://smartpy.io/ide?template=test_modify.py) and [Tickets](https://smartpy.io/ide?template=test_ticket.py) templates for examples.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<record>.<field>`**

If `record` is a record and `field` one of its fields, we can obtain the field's value by writing `<record>.<field>`.

#### Example

```typescript
const record = { x:1, y: 2};
const value  = record.x // 1
```

</Snippet>
