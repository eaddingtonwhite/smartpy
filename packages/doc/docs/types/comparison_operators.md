# Comparison operators

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';

The comparison operators `==`, `!=`, `<`, `<=`, `>`, `>=` behave just like in python.

### Equal

<Snippet syntax={SYNTAX.PY}>

**`expr1 == expr2`**

Return `True` if `expr1` is equal to `expr2`, `False` otherwise.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 == expr2`**,
**`expr1 === expr2`**

Return `True` if `expr1` is equal to `expr2`, `False` otherwise.

</Snippet>

<MichelsonDocLink placeholder="EQ" url="https://tezos.gitlab.io/michelson-reference/#instr-EQ"/>

### Not equal

<Snippet syntax={SYNTAX.PY}>

**`expr1 != expr2`**

Return `True` if `expr1` is not equal to `expr2`, `False` otherwise.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 != expr2`**,
**`expr1 !== expr2`**

Return `True` if `expr1` is not equal to `expr2`, `False` otherwise.

</Snippet>

<MichelsonDocLink placeholder="NEQ" url="https://tezos.gitlab.io/michelson-reference/#instr-NEQ"/>

### Less than

<Snippet syntax={SYNTAX.PY}>

**`expr1 < expr2`**

Return `True` if `expr1` is less than `expr2`, `False` otherwise.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 < expr2`**

Return `True` if `expr1` is less than `expr2`, `False` otherwise.

</Snippet>

<MichelsonDocLink placeholder="LT" url="https://tezos.gitlab.io/michelson-reference/#instr-LT"/>

### Less than or equal

<Snippet syntax={SYNTAX.PY}>

**`expr1 <= expr2`**

Return `True` if `expr1` is less than or equal to `expr2`, `False` otherwise.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 <= expr2`**

Return `True` if `expr1` is less than or equal to `expr2`, `False` otherwise.

</Snippet>

<MichelsonDocLink placeholder="LE" url="https://tezos.gitlab.io/michelson-reference/#instr-LE"/>

### Greater than

<Snippet syntax={SYNTAX.PY}>

**`expr1 > expr2`**

Return `True` if `expr1` is greater than `expr2`, `False` otherwise.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 > expr2`**

Return `True` if `expr1` is greater than `expr2`, `False` otherwise.

</Snippet>

<MichelsonDocLink placeholder="GT" url="https://tezos.gitlab.io/michelson-reference/#instr-GT"/>

### Greater than or equal

<Snippet syntax={SYNTAX.PY}>


**`expr1 >= expr2`**

Return `True` if `expr1` is greater than or equal to `expr2`, `False` otherwise.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 >= expr2`**

Return `True` if `expr1` is greater than or equal to `expr2`, `False` otherwise.

</Snippet>

<MichelsonDocLink placeholder="GE" url="https://tezos.gitlab.io/michelson-reference/#instr-GE"/>


## Operations

### Get the minimum of two values

<Snippet syntax={SYNTAX.PY}>

**`sp.min(expr1, expr2)`**

Return the minimum of `expr1` and `expr2`.

#### Example

```python
sp.min(10, 11) # 10
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Not available

</Snippet>

### Get the maximum of two values

<Snippet syntax={SYNTAX.PY}>

**`sp.max(expr1, expr2)`**

Return the maximum of `expr1` and `expr2`.

#### Example


```python
sp.max(10, 11) # 11
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Not available

</Snippet>
