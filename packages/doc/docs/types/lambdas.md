# Lambdas

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

The type of functions in SmartPy is [sp.TLambda](/general/types#lambda)(`t1`, `t2`) where `t1` is the parameter type and `t2` the result type.<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="lambda" url="https://tezos.gitlab.io/michelson-reference/#type-lambda"/>.

See reference [Lambdas](https://smartpy.io/ide?template=lambdas.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of functions in SmartTS is [TLambda](/general/types#lambda)<`t1`, `t2`> where `t1` is the parameter type and `t2` the result type.<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="lambda" url="https://tezos.gitlab.io/michelson-reference/#type-lambda"/>.

See reference [Lambdas](https://smartpy.io/ts-ide?template=Lambdas.ts) template.

</Snippet>

## Operations

### Build a lambda

<Snippet syntax={SYNTAX.PY}>

**`sp.build_lambda(l)`**

Build a SmartPy lambda from a Python function or lambda.

For example, `sp.build_lambda(lambda x: x + 3)` represents a function that takes an argument `x` and returns `x + 3`.<br/>
This function is usually useless as it is called automatically by SmartPy in most contexts.

#### Example

```python
# Explicit call
logic = sp.build_lambda(lambda x : x + 3)
x = logic(3); # A SmartPy expression that evaluates to 6

# sp.build_lambda is called automatically
# because it needs to be converted to a SmartPy expression
self.data.f = lambda x : x + 3

# This is regular Python
logic = lambda x : x + 3
x = logic(3); # 6
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`() => some logic`**

Build a SmartTS lambda from a Typescript function.

For example, `(x: TNat) => x + 3` represents a function that takes an argument `x` and returns `x + 3`.

#### Example

```typescript
const logic: TLambda<TNat, TNat> = (x: TNat) => x + 3;
logic(3); // 6
```

</Snippet>

<MichelsonDocLink placeholder="LAMBDA" url="https://tezos.gitlab.io/michelson-reference/#instr-LAMBDA"/>

### Define a global lambda

<Snippet syntax={SYNTAX.PY}>

**`@sp.global_lambda`**

Decorator to introduce a lambda that is also a global variable.<br/>
This is used for pure functions that are expected to be used more than once and that do not change the global state nor create operations.<br/>

Values are returned by using `sp.result(value)`.

See reference [WorldCalculator](https://smartpy.io/ide?template=worldCalculator.py) template.

#### Example

```python
class MyContract(sp.Contract):
    # ...

    @sp.global_lambda
    def transformer(x):
        sp.result(x + 5)

    @sp.entry_point
    def h(self, params):
        self.data.result = self.transformer(params)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`@GlobalLambda`**

Decorator to introduce a lambda that is also a global variable.<br/>
This is used for pure functions that are expected to be used more than once and that do not change the global state nor create operations.<br/>

See reference [Lambdas](https://smartpy.io/ts-ide?template=Lambdas.ts) template.

```typescript
class MyContract {
    storage: TNat = 0;

    @GlobalLambda
    transformer = (value: TNat): TNat => {
        return value + 5;
    };

    @EntryPoint
    ep(params: TNat): void {
        this.storage = this.transformer(params)
    }
}
```

</Snippet>


<MichelsonDocLink placeholder="LAMBDA" url="https://tezos.gitlab.io/michelson-reference/#instr-LAMBDA"/>

### Define a sub entry point

<Snippet syntax={SYNTAX.PY}>

**`@sp.sub_entry_point`**

Like `sp.global_lambda` but impure, for functions that can change the global state or create operations.<br/>
Values are returned by using `sp.result(value)`.

See reference [Sub entry point](https://smartpy.io/ide?template=sub_entry_point.py) template.

#### Example

```python
class MyContract(sp.Contract):
    def __init__(self):
        self.init(x = 2, z = 0)

    @sp.sub_entry_point
    def a(self, params):
        sp.set_delegate(sp.none)
        self.data.x += 1
        sp.result(params  * self.data.x)

    @sp.entry_point
    def g(self, params):
        self.data.z = self.a(6)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`@sp.GlobalLambda`**

Like in [Define a global lambda](#define-a-global-lambda) but impure, for functions that can change the global state or create operations.<br/>

See reference [Lambdas](https://smartpy.io/ts-ide?template=Lambdas.ts) template.

#### Example

```typescript
class MyContract {
    storage: TNat = 0;

    @GlobalLambda({ pure: false })
    transformer = (value: TNat): TNat => {
        Sp.setDelegate(Sp.none);
        this.storage = 1;
        return value + this.storage;
    };

    @EntryPoint
    ep(params: TNat) {
        this.storage = this.transformer(params);
    }
}
```

</Snippet>


### Calling lambdas

<Snippet syntax={SYNTAX.PY}>

**`f(x)`**

Call a lambda.

If `f` is of type `sp.TLambda(t1, t2)` and `x` is of type `t1` then `f(x)` is of type `t2`.<br/>

<MichelsonDocLink placeholder="EXEC" url="https://tezos.gitlab.io/michelson-reference/#instr-EXEC"/>
<br/>
<br/>

:::note
As for every SmartPy expression, simply writing `y = f(x)`
doesn't directly compute `f(x)`. It builds an expression that will be computed _if_ used in some action.
:::

**`f.apply(x)`**

Partially apply a lambda.

If `f` is of type `sp.TLambda(sp.TPair(tp1, tp2), target)` and `x` is of type `tp1` then `f.apply(x)` is of type `sp.TLambda(tp2, target)`.<br/>

<MichelsonDocLink placeholder="APPLY" url="https://tezos.gitlab.io/michelson-reference/#instr-APPLY"/>


</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`f(x)`**

Call a lambda.

If `f` is of type `TLambda(t1, t2)` and `x` is of type `t1` then `f(x)` is of type `t2`.<br/>

<MichelsonDocLink placeholder="EXEC" url="https://tezos.gitlab.io/michelson-reference/#instr-EXEC"/>
<br/>
<br/>

:::note
As for every SmartTS expression, simply writing `y = f(x)`
doesn't directly compute `f(x)`. It builds an expression that will be computed _if_ used in some action.
:::


</Snippet>
