# Bytes

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

The type used to represent sequences of bytes in SmartPy is [sp.TBytes](/general/types#bytes).<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="bytes" url="https://tezos.gitlab.io/michelson-reference/#type-bytes"/>.

See reference [Strings and Bytes](https://smartpy.io/ide?template=stringManipulations.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of byte arrays in SmartTS is [TBytes](/general/types#bytes).<br/>
The corresponding type in Michelson is [bytes](https://tezos.gitlab.io/michelson-reference/#type-bytes).

</Snippet>

## Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.bytes(<byte_sequence_string>)`**

Create a literal of type [sp.TBytes](/general/types#bytes) in hexadecimal notation.

#### Examples

```python
sp.bytes("0x0dae11")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Create a literal of type [TBytes](/general/types#bytes) in hexadecimal notation.

```typescript
const bytes: TBytes = "0x0dae11"
```

</Snippet>

## Operations

### Concatenation

<Snippet syntax={SYNTAX.PY}>

**`expr1 + expr2`**

Concatenate two sequences of [sp.TBytes](/general/types#bytes).

#### Example

```python
result = sp.bytes("0x0dae11") + sp.bytes("0x0dae11")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<expr1>.concat(<expr2>)`**

```typescript
const bytes: TBytes = ("0x0dae11" as TBytes).concat("0x0dae11");
```

</Snippet>


<Snippet syntax={SYNTAX.PY}>

**`sp.concat(<l>)`**

Concatenate a list `l` of [sp.TBytes](/general/types#bytes).

#### Example

```python
result = sp.concat([sp.bytes("0x0dae11"), sp.bytes("0x0dae11")])
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.concat(<l>)`**

Concatenate a list `l` of [TBytes](/general/types#bytes).

```typescript
const result: TBytes = Sp.concat(["0x0dae11" as TBytes, "0x0dae11" as TBytes])
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="CONCAT" url="https://tezos.gitlab.io/michelson-reference/#instr-CONCAT"/>


### Obtaining Size

<Snippet syntax={SYNTAX.PY}>

**`sp.len(<expression>)`**

Return the length of `<expression>`.

#### Example

```python
size = sp.len(sp.bytes("0x0dae11"))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`<expression>.size()`

Return the length of `<expression>`.

#### Example

```typescript
const size: TNat = ("0x0dae11" as TBytes).size()
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="SIZE" url="https://tezos.gitlab.io/michelson-reference/#instr-SIZE"/>


### Slicing

<Snippet syntax={SYNTAX.PY}>

**`sp.slice(<expression>, offset=<offset>, length=<length>)`**

Slice `expression` of type [sp.TBytes](/general/types#bytes) from `offset` for `length` characters.

It returns an expression of type [sp.TOption](/general/types#option)([sp.TBytes](/general/types#bytes)).

#### Example

```python
sp.slice(sp.bytes("0x0dae11"), 1, 2) # 0xae11
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<expr>.slice(<offset>, <length>)`**

Slice `expr` of type [TBytes](/general/types#bytes) from `offset` for `length` characters.

It returns an expression of type [TOption](/general/types#option)([TBytes](/general/types#bytes)).

```typescript
("0x0dae11"as TBytes).slice(1, 2); // 0xae11
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="SLICE" url="https://tezos.gitlab.io/michelson-reference/#instr-SLICE"/>


## Other operations

### Packing and Unpacking Data

<Snippet syntax={SYNTAX.PY}>

[sp.pack](/advanced/pack_unpack#packing-data) and [sp.unpack](/advanced/pack_unpack#unpacking-data) are used for serializing/deserializing pieces of data. It heavily relies on [sp.TBytes](/types/bytes) type.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

[Sp.pack](/advanced/pack_unpack#packing-data) and [Sp.unpack](/advanced/pack_unpack#unpacking-data) are used for serializing/deserializing pieces of data. It heavily relies on [TBytes](/types/bytes) type.

</Snippet>

### Hashing Functions

<Snippet syntax={SYNTAX.PY}>

[sp.blake2b](/advanced/hashing_functions#blake2b), [sp.sha256](/advanced/hashing_functions#sha256), [sp.sha512](/advanced/hashing_functions#sha512), [sp.sha3](/advanced/hashing_functions#sha3), [sp.keccak](/advanced/hashing_functions#keccak) expect a single argument of type [sp.TBytes](/types/bytes) representing the content to be hashed and return the result as [sp.TBytes](/types/bytes).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

[Sp.blake2b](/advanced/hashing_functions#blake2b), [Sp.sha256](/advanced/hashing_functions#sha256), [Sp.sha512](/advanced/hashing_functions#sha512), [Sp.sha3](/advanced/hashing_functions#sha3), [Sp.keccak](/advanced/hashing_functions#keccak) expect a single argument of type [TBytes](/types/bytes) representing the content to be hashed and return the result as [TBytes](/types/bytes).

</Snippet>

Hashing functions are documented [here](/advanced/hashing_functions).


## Utils

<Snippet syntax={SYNTAX.PY}>

### Conversion from string to bytes

**`sp.utils.bytes_of_string(<string>)`**

Encode a constant string to [sp.TBytes](/types/bytes).

:::caution
This expression is not evaluated at runtime because no such instruction exists in Michelson.

It is only evaluated during compilation.
:::

#### Example

```python
result = sp.utils.bytes_of_string("A String")
```

</Snippet>
