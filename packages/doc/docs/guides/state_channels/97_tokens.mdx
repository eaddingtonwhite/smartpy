---
sidebar_position: 97
---

import SyntaxSelector from '@theme/Syntax/Selector';
import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import Entrypoint from '@theme/MichelsonDoc/Entrypoint';
import OffchainView from '@theme/MichelsonDoc/OffchainView';
import MichelsonArg from '@theme/MichelsonDoc/MichelsonArg';
import MichelsonArgs from '@theme/MichelsonDoc/MichelsonArgs';
import CodeTabs from '@theme/CodeTabs';

# Tokens

The GamePlatform contains:

+ 3 bigmaps related to tokens: `ledger`, `token_metadata` and `token_permissions`. <br/>
  The two first are those describe in [FA2 standard](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-12/tzip-12.md).<br/>
  The `token_permissions` describes specific permissions associated with the token.<br/>
+ 4 entrypoints: `set_token_metadata`, `push_bonds`, `ledger_to_bonds` and `withdraw_ledger`.

Platform tokens should not be confused with bonds.
Tokens are stored outside of channels
and if their metadata allow it they can be withdrawn instantly.

Token 0 corresponds to real mutez, other tokens can correspond to FA1.2/FA2 tokens or internal tokens.

The admin can authorize a model or a specific game to distribute platform tokens
(like reputation tokens).

## Push bonds

See [push_bonds](push_bonds).

## Withdraw tokens

Transfer the tokens from the platform ledger sender's account to outside of the platform.

If token id is 0: send as mutez as the tokens withdrawn.<br/>
If tokens metadata corresponds to a FA1.2/FA2 address and FA2 token_id: Transfer as much tokens from the platform to the receiver.
Else: the token can't be withdrawn.

<CodeTabs>
    <Entrypoint name="withdraw_ledger">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="receiver"
                type="address">
                Address of the receiver
            </MichelsonArg>
            <MichelsonArg
                name="tokens"
                type="map(nat, nat)">
                Map of <code>token_id => amount</code> you want to withdraw
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
platform.withdraw_ledger(
    receiver = player1.address,
    tokens = sp.map({0: 10_000_000})
).run(sender = player1)
```
</block>
</CodeTabs>

## Ledger to bonds

Transfer the tokens from the platform ledger to a channel.

<CodeTabs>
    <Entrypoint name="ledger_to_bonds">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="channel_id"
                type="bytes">
                The id of the channel (see <a href="open_channel#build-the-channel_id">open_channel</a>)
            </MichelsonArg>
            <MichelsonArg
                name="receiver"
                type="address">
                Address of the receiver
            </MichelsonArg>
            <MichelsonArg
                name="tokens"
                type="map(nat, nat)">
                Map of <code>token_id => amount</code> you want to withdraw
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
# Push 10 tez to channel bonds
platform.push_bonds(
    channel_id = player1.address,
    receiver = player1.address,
    tokens = sp.map({0: 10_000_000})
).run(sender = player1, amount = sp.tez(10))
```
</block>
</CodeTabs>

## Tokens metadata and authorization

### Token Metadata

Set the token metadata.

Token metadata corresponds to those described in
[TZIP-12](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-12/tzip-12.md) and
[TZIP-16](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-16/tzip-16.md).

<CodeTabs>
    <Entrypoint name="set_token_metadata">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="token_id"
                type="nat">
                The id of the token as represented in the platform
            </MichelsonArg>
            <MichelsonArg
                name="metadata"
                type="map(string, bytes">
                Metadata of the tokens
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
c1.set_token_metadata(
    token_id = 1,
    metadata = {
        "name"        : sp.utils.bytes_of_string("Wrapped Dummy FA2"),
        "decimals"    : sp.utils.bytes_of_string("%d" % 0),
        "symbol"      : sp.utils.bytes_of_string("WDFA2"),
        "type"        : sp.pack("FA2"),
        "max_supply"  : sp.pack(100_000_000_000),
        "fa2_address" : sp.pack(dummyToken.address),
        "fa2_token_id": sp.pack(0),
    }
).run(sender = admin)
```
</block>
</CodeTabs>

### Set Token Permissions

Set the token permissions.

The token permissions are pack of the value.

| Key          | Value Type                               | Description                                                                         |
| ------------ | ---------------------------------------- | ----------------------------------------------------------------------------------- |
| type*        | string {`"FA2"`, `"INTERNAL"`, `"FREE"`} | Type of token represented. <br/>`FREE` is used for tokens that can be pushed freely |
| max_supply   | nat                                      | Maximum tokens that the platform will mint                                          |
| fa2_address  | addresss                                 | address of the corresponding fa2 contract                                           |
| fa2_token_id | nat                                      | id of the corresponding fa2 token                                                   |

\* required

<CodeTabs>
    <Entrypoint name="set_token_permissions">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="token_id"
                type="nat">
                The id of the token as represented in the platform
            </MichelsonArg>
            <MichelsonArg
                name="metadata"
                type="map(string, bytes">
                Permissions of the tokens
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
c1.set_token_metadata(
    token_id = 1,
    metadata = {
        "type"        : sp.pack("FA2"),
        "max_supply"  : sp.pack(100_000_000_000),
        "fa2_address" : sp.pack(dummyToken.address),
        "fa2_token_id": sp.pack(0),
    }
).run(sender = admin)
```
</block>
</CodeTabs>

## Game permissions

Each running game can have the permission to mint tokens.

The metadata of the game must contain the following key: `allowed_mint`.

Its value is a map of bonds and amount.

The game can at most ask the platform to mint this number of tokens when being settled.

### Change the game metadata

Only admins can change the game metadata.

```python
gameplatform.update_game_metadata(game_id, "allowed_mint", {42: 1000})
```

## Model permissions

The models can have the permission to mint tokens.

The metadata of the model must contain the following key: `allowed_mint`.

Its value is a map of bonds and amount.

The model can at most ask the platform to mint this number of tokens.

The value is decreased or left unchanged after each settle of a game held by the model.

### Change the model metadata

Only admins can change the model metadata.

```python
gameplatform.update_model_metadata(model_id, "allowed_mint", {42: 100_000_000})
```
