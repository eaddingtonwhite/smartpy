---
sidebar_position: 6
---

# Step 5: Play a game

import SyntaxSelector from '@theme/Syntax/Selector';
import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import Entrypoint from '@theme/MichelsonDoc/Entrypoint';
import OffchainView from '@theme/MichelsonDoc/OffchainView';
import MichelsonArg from '@theme/MichelsonDoc/MichelsonArg';
import MichelsonArgs from '@theme/MichelsonDoc/MichelsonArgs';
import CodeTabs from '@theme/CodeTabs';

A move within a game is an updated new game state signed by both
players.

If everything goes well, the current player sends both
[move_data](new_model#model-class)
and signed [new_state](#sign-a-new-state).

## Compute `new_current` and `new_state`

:::note
More info about offchain views and signatures can be found in [offchain views and signatures](offchain_views_signatures)
:::

To compute the `new_current` and `new_state` you can use the offchain view.<br/>
It's both useful to compute your moves and to verify other's.

<OffchainView name="offchain_play">
    <MichelsonArgs type="pair">
        <MichelsonArg
            name="game"
            type="$game">
            Game as returned by <span className="entrypointName">offchain_play</span> of the last move
        </MichelsonArg>
        <MichelsonArg
            name="move_data"
            type="bytes">
            Data of the move
        </MichelsonArg>
        <MichelsonArg
            name="move_nb"
            type="int">
            Incrementing id of the move
        </MichelsonArg>
        <MichelsonArg
            name="sender"
            type="address">
            Address of the player that send this move
        </MichelsonArg>
    </MichelsonArgs>
</OffchainView>

## Sign a new state

Each player needs to sign a `pack` of a pair of `"New State"`,
`<game_id>`, `<new_current>`, `<new_state>`.

`<game_id>` the game id (see [compute_game_id](new_game#compute-the-game_id)). <br/>
`<new_current>` the current returned by the offchain view <span className="entrypointName">offchain_play</span>. <br/>
`<new_state>` will be passed to the `apply` lambda of the model referenced in the constants.

The pair is of type `string bytes $current bytes`.

In SmartPy it corresponds to a call to the `action_new_state` method of `game_platform.py`

```python
def action_new_state(game_id, new_current, new_state):
    game_id    = sp.set_type_expr(game_id, sp.TBytes)
    new_current   = sp.set_type_expr(new_current, types.t_current)
    new_state  = sp.set_type_expr(new_state, sp.TBytes)
    return sp.pack(("New State", game_id, new_current, new_state))
```

## Push the last state onchain

<CodeTabs>
    <Entrypoint name="game_set_state">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="game_id"
                type="bytes">
                See <a href="new_game#compute-the-game_id">Game id</a>
            </MichelsonArg>
            <MichelsonArg
                name="new_current"
                type="$current">
                Returned by <a href="#compute-new_current-and-new_state">offchain_play</a>
            </MichelsonArg>
            <MichelsonArg
                name="new_state"
                type="bytes">
                Returned by <a href="#compute-new_current-and-new_state">offchain_play</a>
            </MichelsonArg>
            <MichelsonArg
                name="signatures"
                type="map (key, signature)">
                Public key of each player associated with the <a href="#sign-a-new-state">signature of the state</a>
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
# ... (new game scenario)

def action_state_sigs(game_id, game):
    action_new_state = gp.action_new_state(game_id, game.current, game.state)
    signatures = make_signatures(player1, player2, action_new_state)
    return (game, action_new_state, signatures)

offchain_play = TestView(platform, platform.offchain_play)
sc += offchain_play
data = {}
move_nb = 0

sc.h3("Move 0")
offchain_play.compute(sp.record(
    data = platform.data,
    params = sp.record(
        game      = game,
        move_data = sp.pack(sp.record(i = 1, j = 1)),
        move_nb   = 0,
        sender    = player1.address
    )
)).run(sender = player1)

data[move_nb] = action_state_sigs(game_id, offchain_play.data.result.open_some())
move_nb += 1

# ... (some other moves)

platform.game_set_state(
    game_id     = game_id,
    new_current = data[move_nb-1][0].current,
    new_state   = data[move_nb-1][0].state,
    signatures  = data[move_nb-1][2]
).run(sender = player1)
```

</block>
</CodeTabs>

:::info
Nothing prevents players from signing a state
that doesn't correspond to what the `apply_` lambda would have done.

This means that if **everyone agrees** the rules of the game can be violated.
:::

## Playing directly on-chain

If the other player indicated that you are not playing (see [non playing opponent](disputes#non-playing-opponent))
or if the other player doesn't want to sign your move, you have to play directly onchain.

This is why the <span className="entrypointName">game_play</span> entrypoint exists.

<CodeTabs>
    <Entrypoint name="game_play">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="game_id"
                type="bytes">
                See <a href="new_game#compute-the-game_id">Game id</a>
            </MichelsonArg>
            <MichelsonArg
                name="move_data"
                type="bytes">
                Data of the move
            </MichelsonArg>
            <MichelsonArg
                name="move_nb"
                type="nat">
                Incrementing id of the move
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
platform.game_play(
    game_id    = game_id,
    move_data  = sp.pack(sp.record(i = 1, j = 1)),
    move_nb    = 0,
).run(sender = player1)
`}
```
</block>
</CodeTabs>

## Agree on the outcome

At any time a player can send a request to end the game with a defined
outcome as if it were returned by the `apply_` lambda.

For example, if for any reason the game cannot be finished, players
can agree to define an outcome that will not provoke any transfer.

In SmartPy it corresponds to a call to the `action_new_outcome` method
of `game_platform.py`

```python
def action_new_outcome(game_id, new_outcome, timeout):
    game_id     = sp.set_type_expr(game_id, sp.TBytes)
    new_outcome = sp.set_type_expr(new_outcome, sp.TString)
    timeout     = sp.set_type_expr(now, sp.TTimestamp)
    return sp.pack(("New Outcome", game_id, new_outcome, timeout))
```

It can be pushed on-chain with signatures by calling <span
className="entrypointName">game_set_outcome</span>.

The `new_outcome` action expires depending on the timestamp it held.

<CodeTabs>
    <Entrypoint name="game_set_outcome">
        <MichelsonArgs type="pair">
            <MichelsonArg
                name="game_id"
                type="bytes">
                See <a href="new_game#compute-the-game_id">Game id</a>
            </MichelsonArg>
            <MichelsonArg
                name="outcome"
                type="string">
                Any string. Can correspond or not to a settlement outcome.
            </MichelsonArg>
            <MichelsonArg
                name="timeout"
                type="timestamp">
                Timeout after which the proposal expires.<br/>
                It should be a very short delay..
            </MichelsonArg>
            <MichelsonArg
                name="signatures"
                type="map (key, signature)">
                Public key of each player associated with the signature of the <code>action_new_outcome</code>
            </MichelsonArg>
        </MichelsonArgs>
    </Entrypoint>
<block name="python">

```python
# ... (playing game scenario)

def action_outcome_sig(game_id, outcome, timeout):
    action_new_outcome = gp.action_new_outcome(game_id, outcome, timeout)
    signatures = sp.make_signature(player1, player2, action_new_outcome)
    return (action_new_outcome, signatures)

import time
timeout = sp.timestamp(time.time()).add_minutes(2)
signatures = action_outcome_sigs(game_id, "draw", timeout)
platform.game_set_state(
    game_id    = game_id,
    outcome    = "draw",
    signatures = signatures
).run(sender = player1)
`}
```
</block>
</CodeTabs>
