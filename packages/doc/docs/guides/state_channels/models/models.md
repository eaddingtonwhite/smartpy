---
sidebar_position: 1
---
# List of models


|      Name       | uses randomness | has parameters |
| :-------------: | :----: | ---------- |
| [Transfer][m1]  |        |            |
| [Tictactoe][m2] |        |            |
|    [Nim][m3]    |        | ✅         |
| [Head tail][m4] |   ✅   |            |

A detailed scenario that uses some of the models can be found
[here](https://smartpy.io/ide?template=state_channel_games/tests/test_game_platform_offchain.py).

[m1]: https://smartpy.io/ide?template=state_channel_games/models/transfer.py
[m2]: https://smartpy.io/ide?template=state_channel_games/models/tictactoe.py
[m3]: https://smartpy.io/ide?template=state_channel_games/models/nim.py
[m4]: https://smartpy.io/ide?template=state_channel_games/models/head_tail.py
