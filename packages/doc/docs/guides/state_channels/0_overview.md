---
sidebar_position: 1
---

import Michelson from '@theme/MichelsonDoc/Michelson';
import MichelsonArg from '@theme/MichelsonDoc/MichelsonArg';
import MichelsonArgs from '@theme/MichelsonDoc/MichelsonArgs';
import CodeTabs from '@theme/CodeTabs';

# Overview

:::info
A game platform contract (see [below](#contracts-and-examples)) has been originated on *Granadanet*: [KT1DK9ZPjmqQQ8iUjqec6hNuCALpjaWnQ4Uv](https://granada.tzstats.com/KT1DK9ZPjmqQQ8iUjqec6hNuCALpjaWnQ4Uv)
:::

## What are state channels?

State channels combine the decentralization and security offered by
the Tezos blockchain with the throughput and low latency offered by
off-chain operations. As such, they provide a "layer two solution" that
overcomes scalability limits and reduces operational costs.

By way of analogy, an ordinary legal contract does not require a judge
to be present at every interaction of the involved parties. Indeed
this is only needed in the comparatively rare cases of disagreement or
foul play. The same is true for state channels: they provide a way for
parties to interact off-chain and only need to fall back to onchain
operations to settle conflicts.

## Games!

To explain how state channels work, this guide describes a *game
platform*. It allows users to play games such as chess or
tic-tac-toe. A prize for the winner is agreed upon at the start and
settled at the end of a game. Actually, the provided framework is
slightly more general and it is easy to see how games generalize to
other types of contracts, e.g. financial ones.

## How does it work?

The game platform, represented on-chain by a contract, allows
participants to open channels between each other. To assure each other
that debt resulting from game outcomes will be settled, they then push
bonds that can only be withdrawn under certain conditions.  The next
step is to actually play the game: the players send each other signed
off-chain operations, without. In case of disagreement either player
can move the game to the blockchain.  This ensures that the players
can force each other to respect the rules of the game.  If all goes
well, very few operations take place on-chain, making game play
cheaper (off-chain operations have no assocaiated gas cost) and
possibly faster.

The rules of a game are stipulated in a so-called *model*. (using
SmartPy or any other methods to generate Michelson).  Each instance fo
a model is called a *game*.  In each game two players take turns in
making moves.

## Contracts and examples

The contracts for the platform, as well as the game models, can be
found under [smartpy.io/ide](https://smartpy.io/ide) > Templates >
Regular Templates > State Channels. In particular, there is:

- The main [platform
  contract](https://smartpy.io/ide?template=state_channel_games/game_platform.py):
  management of state channels, games and bonds.

- A [model
  tester](https://smartpy.io/ide?template=state_channel_games/game_tester.py):
  test a single model before deploying it.

- A [set of example models](models/models), together with scenarios
  and tests. Each model contains a scenario for the model tester.

- A [realistic scenario](realistic_scenario) between two players using the testnet platform

- A scenario illustrating the [offchain
interaction](https://smartpy.io/ide?template=state_channel_games/tests/test_game_platform_offchain.py)
of players.

- A scenario illustrating the signatures with faucet accounts [testnet
interaction](https://smartpy.io/ide?template=state_channel_games/tests/test_game_platform_testnet.py)
of players.


## API: instantiating the platform

<CodeTabs>
    <Michelson name="gamePlatform" type="sp.Contract">
        <MichelsonArg
            name="admins"
            type="sp.TSet(sp.TAddress)">
            Set of admins of the platform. They are allowed to set token/model/games metada.
        </MichelsonArg>
        <MichelsonArg
            name="platform_address"
            type="sp.TAddress">
            (optional) The address of the platform you want to replicate.
        </MichelsonArg>
    </Michelson>
<block name="Python">

```python
import smartpy as sp
gp = sp.io.import_template("state_channel_games/game_platform.py")

@sp.add_test(name="Game Platform")
def test():
    sc = sp.test_scenario()
    admin = sp.test_account("admin")
    platform_address = sp.address('KT1_ADDRESS_OF_THE_PLATFORM') # can be `None`
    platform = gp.GamePlatform(admins = sp.set([player1.address]), self_addr = platform_address)
    sc += platform
```
</block>
</CodeTabs>
