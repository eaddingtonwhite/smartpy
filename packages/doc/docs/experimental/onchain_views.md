# Views

Views are sugared entry points that receive a callback in their inputs, then process some logic to obtain the requested data, and call back the requester with a response.

The specification can be found [here](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-7/tzip-7.md)

The decorator `@sp.utils.view` is used to define a view entry point.

A `view` is an **entry point** that:

- Doesn’t change the storage;
- Calls a callback sp.TContract(t).

```python
​@sp.utils.view(sp.TNat)
​def getBalance(self, params):
    ​sp.result(self.data.balances[params].balance
```

This code above is a simpler version of the equivalent:

```python
​@sp.entry_point
​def getBalance(self, params):
    ​sp.set_type(sp.snd(params), sp.TContract(sp.TNat))
    ​response = self.data.balances[sp.fst(params)].balance
    ​sp.transfer(​response, sp.tez(0), sp.snd(params))
```
