/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

module.exports = {
    guides: [
        {
            type: 'category',
            label: 'State Channels',
            items: [
                {
                    type: 'autogenerated',
                    dirName: 'guides/state_channels',
                },
            ],
        },
        {
            type: 'category',
            label: 'Metadata',
            items: [
                {
                    type: 'autogenerated',
                    dirName: 'guides/metadata',
                },
            ],
        },
    ],
    templates: [
        {
            type: 'category',
            label: 'Templates',
            items: [
                {
                    type: 'autogenerated',
                    dirName: 'templates',
                },
            ],
        },
    ],
    sidebar: [
        ...[
            {
                type: 'category',
                label: 'Getting Started',
                items: [
                    'introduction/overview',
                    'introduction/start_project',
                    'introduction/expressions',
                    'introduction/entry_points',
                    'introduction/contracts',
                    'introduction/types',
                    'introduction/meta_programming',
                    'introduction/constants_vs_expressions',
                ],
            },
            {
                type: 'category',
                label: 'General',
                items: [
                    'general/types',
                    'general/variables',
                    'general/control_statements',
                    'general/checking_condition',
                    'general/raising_exceptions',
                    'general/code_inlining',
                    'general/flags',
                    'general/import',
                ],
            },
            {
                type: 'category',
                label: 'Types and Operations',
                items: [
                    {
                        type: 'autogenerated',
                        dirName: 'types',
                    },
                ],
            },
            {
                type: 'category',
                label: 'Other Operations',
                items: [
                    {
                        type: 'autogenerated',
                        dirName: 'advanced',
                    },
                ],
            },
            {
                type: 'category',
                label: 'Helpers',
                items: ['helpers/metadata_builder', 'helpers/utils'],
            },
            {
                type: 'category',
                label: 'Tests and Scenarios',
                items: [
                    'scenarios/framework',
                    'scenarios/testing',
                    'scenarios/custom_targets',
                    'scenarios/simulation_target',
                    'scenarios/cryptography',
                ],
            },
            {
                type: 'doc',
                label: 'Debugging Contracts',
                id: 'debugging_contracts',
            },
            {
                type: 'doc',
                label: 'Compilation Targets',
                id: 'compilation_targets',
            },
            {
                type: 'doc',
                label: 'CLI',
                id: 'cli',
            },
            {
                type: 'category',
                label: 'Experimental Features',
                items: [
                    {
                        type: 'autogenerated',
                        dirName: 'experimental',
                    },
                ],
            },
        ],
    ],
};
