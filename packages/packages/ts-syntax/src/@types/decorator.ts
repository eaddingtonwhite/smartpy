import type { ST_DecoratorKind } from '../enums/decorator';
import type { ST_Expression } from './expression';
import type { ST_Flag } from './output';

export interface ST_EntryPoint_Decorator {
    kind: ST_DecoratorKind.EntryPoint;
    name: string;
    mock: boolean;
    lazy: boolean;
    lazy_no_code: boolean;
    functionRef: string;
}

export interface ST_OffChainView_Decorator {
    kind: ST_DecoratorKind.OffChainView;
    name: string;
    pure: boolean;
    description?: string;
    functionRef: string;
}

export interface ST_Metadata_Decorator {
    kind: ST_DecoratorKind.MetadataBuilder;
    name: string;
    properties: Record<string, ST_Expression>;
}

export interface ST_GlobalLambda_Decorator {
    kind: ST_DecoratorKind.GlobalLambda;
    name: string;
    pure: boolean;
    functionRef: string;
}

export interface ST_Contract_Decorator {
    kind: ST_DecoratorKind.Contract;
    name: string;
    flags: ST_Flag[];
}

export interface ST_Inlined_Decorator {
    kind: ST_DecoratorKind.Inline;
    name: string;
}

export type ST_Class_Decorator = ST_Contract_Decorator;
export type ST_Class_Decorators = Record<string, ST_Class_Decorator>;

export type ST_Property_Decorator =
    | ST_EntryPoint_Decorator
    | ST_OffChainView_Decorator
    | ST_Metadata_Decorator
    | ST_GlobalLambda_Decorator
    | ST_Inlined_Decorator;
export type ST_Property_Decorators = {
    [ST_DecoratorKind.Contract]?: ST_Contract_Decorator;
    [ST_DecoratorKind.EntryPoint]?: ST_EntryPoint_Decorator;
    [ST_DecoratorKind.Inline]?: ST_Inlined_Decorator;
    [ST_DecoratorKind.MetadataBuilder]?: ST_Metadata_Decorator;
    [ST_DecoratorKind.OffChainView]?: ST_OffChainView_Decorator;
    [ST_DecoratorKind.GlobalLambda]?: ST_GlobalLambda_Decorator;
};
