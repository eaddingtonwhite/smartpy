import type { ST_TypeDef, ST_Layout } from '../../@types/type';
import Type, { Layout } from '../../enums/type';
import { FrontendType } from '../../enums/type';
import ErrorUtils from '../utils/Error';
import guards from '../utils/guard';
import LineUtils from '../utils/Line';
import PrinterUtils from '../utils/printer';

const map = (keyType: string, valueType: string) => `(map ${keyType} ${valueType})`;
const big_map = (keyType: string, valueType: string) => `(bigmap ${keyType} ${valueType})`;
const tuple = (children: string[]) => `(tuple ${children.join(' ')})`;
const record_or_variant = (kind: string, children: string[], layout: string) =>
    `(${kind} (${children.join(' ')}) ${layout})`;

const translateType = (typeDef?: ST_TypeDef): string => {
    if (!typeDef) return `()`;
    if (guards.type.isUnknown(typeDef)) {
        return `(unknown 0)`;
    }
    switch (typeDef.type) {
        case FrontendType.TBig_map:
            return big_map(translateType(typeDef.keyType), translateType(typeDef.valueType));
        case FrontendType.TMap:
            return map(translateType(typeDef.keyType), translateType(typeDef.valueType));
        case FrontendType.TList:
        case FrontendType.TSet:
            return `(${Type.backendOfFrontend[typeDef.type]} ${translateType(typeDef.innerTypes[0])})`;
        case FrontendType.TTuple:
            return tuple(typeDef.innerTypes.map(translateType));
        case FrontendType.TChain_id:
            return `"${Type.backendOfFrontend[typeDef.type]}"`;
        case FrontendType.TOption:
            return `(${Type.backendOfFrontend[typeDef.type]} ${translateType(typeDef.innerType)})`;
        case FrontendType.TRecord: {
            const layout = translateLayout(typeDef.layout || []);
            const properties = Object.keys(typeDef.properties);
            if (Array.isArray(typeDef.layout)) {
                const layoutFields = destructLayout(typeDef.layout);
                if (!properties.every((field) => layoutFields?.includes(field))) {
                    return ErrorUtils.failWith(
                        `TRecord layout malformed. Expected fields (${properties.join(
                            ', ',
                        )}), but received (${typeDef.layout.join(', ')})`,
                    );
                }
            }
            return record_or_variant(
                Type.backendOfFrontend[typeDef.type],
                Object.entries(typeDef.properties)
                    .sort(([name1], [name2]) => (name1 > name2 ? 1 : -1))
                    .map(([k, v]) => `(${k} ${translateType(v)})`),
                layout,
            );
        }
        case FrontendType.TVariant: {
            const layout = translateLayout(typeDef.layout || []);
            const properties = Object.keys(typeDef.properties);
            if (Array.isArray(typeDef.layout)) {
                const layoutFields = destructLayout(typeDef.layout);
                if (!properties.every((field) => layoutFields?.includes(field))) {
                    return ErrorUtils.failWith(
                        `TVariant layout malformed. Expected fields (${properties.join(
                            ', ',
                        )}), but received (${typeDef.layout.join(', ')})`,
                    );
                }
            }
            return record_or_variant(
                Type.backendOfFrontend[typeDef.type],
                Object.entries(typeDef.properties)
                    .sort(([name1], [name2]) => (name1 > name2 ? 1 : -1))
                    .map(([k, v]) => {
                        if (k && v.type === FrontendType.TRecord && 'value' in v.properties) {
                            return `(${k} ${translateType(v.properties['value'])})`;
                        }
                        return ErrorUtils.failWith(`Variant malformed, kind ${k} is invalid.`);
                    }),
                layout,
            );
        }
        case FrontendType.TContract:
            return `(${Type.backendOfFrontend[typeDef.type]} ${translateType(typeDef.inputType)})`;
        case FrontendType.TLambda:
            const inputType = translateType(typeDef.inputTypes['input']);
            const outputType = translateType(typeDef.outputType);
            return `(${Type.backendOfFrontend[typeDef.type]} ${inputType} ${outputType})`;
        case FrontendType.TAddress:
        case FrontendType.TNat:
        case FrontendType.TInt:
        case FrontendType.TIntOrNat:
        case FrontendType.TBls12_381_fr:
        case FrontendType.TBls12_381_g1:
        case FrontendType.TBls12_381_g2:
        case FrontendType.TBool:
        case FrontendType.TBytes:
        case FrontendType.TChain_id:
        case FrontendType.TKey:
        case FrontendType.TKey_hash:
        case FrontendType.TMutez:
        case FrontendType.TNat:
        case FrontendType.TNever:
        case FrontendType.TSignature:
        case FrontendType.TString:
        case FrontendType.TTimestamp:
        case FrontendType.TUnit:
        case FrontendType.TOperation:
            return `"${Type.backendOfFrontend[typeDef.type]}"`;
    }

    return ErrorUtils.failWith(`Cannot translate type ${PrinterUtils.type.toString(typeDef)}`);
};

const translateInputType = (
    typeDef: Extract<ST_TypeDef, { type: FrontendType.TFunction | FrontendType.TLambda }>,
): string => {
    const types = Object.entries(typeDef.inputTypes);
    const line = LineUtils.getLineNumber(typeDef.line);

    // Single parameters don't need need pair notation
    if (types.length === 1) {
        const [, type] = types.shift() as [string, ST_TypeDef];
        const valueLine = LineUtils.getLineNumber(type.line);

        return `(set_type (params ${line}) ${translateType(type)} ${valueLine})`;
    }

    // Multiple parameters need pair notation
    return types.reduce((prev, [key, type]) => {
        const valueLine = LineUtils.getLineNumber(type.line);
        const setType = `(set_type (attr (params ${line}) "${key}" ${line}) ${translateType(type)} ${valueLine})`;
        return prev ? `${prev} ${setType}` : setType;
    }, '');
};

const translateLayout = (layout: ST_Layout | Layout): string => {
    const normalizeTuple = (elements: ST_Layout): string =>
        elements.reduce(
            (p, c) =>
                Array.isArray(c)
                    ? p
                        ? `(${p} ${normalizeTuple(c)})`
                        : normalizeTuple(c)
                    : p
                    ? `(${p} ("${c}"))`
                    : `("${c}")`,
            '',
        ) as string;
    if (Array.isArray(layout)) {
        const tuples = normalizeTuple(layout);
        return tuples ? `(Some ${tuples})` : 'None';
    }
    return '(Some Right)';
};

const destructLayout = (layout: string | ST_Layout): string[] => {
    if (Array.isArray(layout)) {
        return layout
            .map((l) => destructLayout(l))
            .reduce((acc, cur) => [...acc, ...(Array.isArray(cur) ? cur : [cur])], []);
    }
    return [layout];
};

const TypeTranslator = {
    translateType,
    translateInputType,
};

export default TypeTranslator;
