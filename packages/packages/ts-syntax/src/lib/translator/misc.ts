export const staticId = (id: number, line: string): string => `(static_id ${id} ${line})`;
