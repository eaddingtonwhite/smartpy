import type { ST_Statements, ST_Statement } from '../../@types/statement';
import { ST_StatementKeywords, ST_StatementKind } from '../../enums/statement';
import LineUtils from '../utils/Line';
import { TranslatorBase } from './Base';

const defineLocal = (name: string, node: string, line: string | number): string =>
    `(defineLocal "${name}" ${node} ${line})`;

const ifBlock = (condition: string, blockStatements: string, line: string | number): string =>
    `(${ST_StatementKeywords.ifBlock} ${condition} (${blockStatements}) ${line})`;

const elseBlock = (blockStatements: string): string => `(${ST_StatementKeywords.elseBlock} (${blockStatements}))`;

const whileBlock = (condition: string, blockStatements: string, line: string | number): string =>
    `(${ST_StatementKeywords.whileBlock} ${condition} (${blockStatements}) ${line})`;

const forGroup = (iteratorName: string, expr: string, statements: string, line: string | number) =>
    `(${ST_StatementKeywords.forGroup} "${iteratorName}" ${expr} (${statements}) ${line})`;

const cases_arg = (lineSneakCased: string, lineAtom: string) => `(cases_arg "match_${lineSneakCased}" ${lineAtom})`;

const match = (caseName: string, casesArg: string, statements: string, line: string) =>
    `(match ${casesArg} "${caseName}" "${caseName}" (${statements}) ${line})`;

const matchCases = (expr: string, cases: string, lineSneakCased: string, line: string | number) =>
    `(match_cases ${expr} "match_${lineSneakCased}" (${cases}) ${line})`;

const bind = (name: string, statements: string) => `(bind "${name}" (${statements}))`;

const result = (expr: string, line: string | number) => `(result ${expr} ${line})`;

export default class StatementTranslator extends TranslatorBase {
    translateStatements = (statements: ST_Statements): string => {
        return Object.values(statements).map(this.translateStatement).join(' ');
    };

    translateStatement = (statement: ST_Statement): string => {
        const line = LineUtils.getLineNumber(statement.line);
        switch (statement.kind) {
            case ST_StatementKind.Expression:
                return this.translators.Expression.translateExpression(statement.expression);
            case ST_StatementKind.VariableStatement:
                const expr = this.translators.Expression.translateExpression(statement.expression);
                return statement.local ? defineLocal(statement.name, expr, line) : expr;
            case ST_StatementKind.IfStatement: {
                const conditionExpr = this.translators.Expression.translateExpression(statement.expression);
                const thenStatement = statement.thenStatement.map(this.translateStatement).join(' ');
                const elseStatement = statement.elseStatement.map(this.translateStatement).join(' ');
                if (elseStatement) {
                    return `${ifBlock(conditionExpr, thenStatement, line)} ${elseBlock(elseStatement)}`;
                } else if (thenStatement) {
                    return ifBlock(conditionExpr, thenStatement, line);
                }
                return '';
            }
            case ST_StatementKind.WhileStatement: {
                const conditionExpr = this.translators.Expression.translateExpression(statement.condition);
                const initializer = statement.initializer ? this.translateStatement(statement.initializer) : '';
                let statements = statement.statements.map(this.translateStatement).join(' ');
                if (statement.incrementor) {
                    statements = `${statements} ${this.translators.Expression.translateExpression(
                        statement.incrementor,
                    )}`;
                }
                if (initializer) {
                    return `${initializer} ${whileBlock(conditionExpr, statements, line)}`;
                }
                return whileBlock(conditionExpr, statements, line);
            }
            case ST_StatementKind.ForOfStatement: {
                const expr = this.translators.Expression.translateExpression(statement.expression);
                const iteratorName = statement.iterator.name;
                const statements = statement.statements.map(this.translateStatement).join(' ');
                return forGroup(iteratorName, expr, statements, line);
            }
            case ST_StatementKind.SwitchStatement: {
                const expr = this.translators.Expression.translateExpression(statement.expression);
                const lineSneakCased = LineUtils.getFileLineSneakCased(statement.line);
                const casesArg = cases_arg(lineSneakCased, line);
                const cases = Object.entries(statement.cases)
                    .map(([name, sts]) => {
                        const statements = sts.map(this.translateStatement).join(' ');
                        return match(name, casesArg, statements, line);
                    })
                    .join(' ');
                return matchCases(expr, cases, lineSneakCased, line);
            }
            case ST_StatementKind.Result: {
                const expr = this.translators.Expression.translateExpression(statement.expression);
                return result(expr, line);
            }
            case ST_StatementKind.Bind: {
                const statements = this.translateStatements(statement.statements);
                return bind(statement.name, statements);
            }
        }
    };
}
