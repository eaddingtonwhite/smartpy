import {
    ArrowFunction,
    ClassDeclaration,
    FunctionDeclaration,
    HeritageClause,
    ImportDeclaration,
    isIdentifier,
    isPropertyAccessExpression,
    MethodDeclaration,
    Node,
    NodeArray,
    NodeFlags,
    SourceFile,
    SyntaxKind,
} from 'typescript';
import type { Nullable } from '../@types/common';
import type { ST_Property_Decorators, ST_Class_Decorator } from '../@types/decorator';
import type { ST_Expression, ST_TypedExpression } from '../@types/expression';
import type { ST_ImportedModule, ST_Module } from '../@types/module';
import type { ST_Class, ST_Flag } from '../@types/output';
import type { ST_FunctionProperty, ST_Properties } from '../@types/property';
import type { ST_CompilationTarget, ST_Scenario, ST_ScenarioAction } from '../@types/scenario';
import type { ST_Statement, ST_VariableStatement } from '../@types/statement';
import type { ST_TypeDefs, ST_TypeDef } from '../@types/type';
import { ST_DecoratorKind } from '../enums/decorator';
import { ST_ExpressionKind } from '../enums/expression';
import { ST_LiteralKind } from '../enums/literal';
import { ST_Modifier } from '../enums/Modifiers';
import { ST_ScenarioActionKind, ST_ScenarioKind } from '../enums/scenario';
import { ST_ScopeKind } from '../enums/scope';
import { ST_StatementKind } from '../enums/statement';
import { FrontendType } from '../enums/type';
import MetadataTranslator from './translator/metadata';
import { staticId } from './translator/misc';
import TypeTranslator from './translator/type';
import type { Translators } from './Transpiler';
import ExpressionBuilder from './utils/builders/expression';
import TypeBuilder from './utils/builders/type';
import ErrorUtils from './utils/Error';
import { nonFalsy } from './utils/filters/falsy';
import guards from './utils/guard';
import IdentifierUtils from './utils/Identifier';
import LineUtils from './utils/Line';
import ModifiersUtils from './utils/Modifiers';

class Scope {
    iterators: Record<string, Extract<ST_Expression, { kind: ST_ExpressionKind.ForIterator }>> = {};
    properties: ST_Properties = {};
    statements: ST_Statement[] = [];
    functions: Record<string, ST_FunctionProperty> = {};
    typeDefs: ST_TypeDefs = {};
    classes: Record<string, ST_Class> = {};
    varCounter = 0;

    constructor(public kind: ST_ScopeKind, public parent?: Scope) {}

    public addIterator = (name: string, expr: Extract<ST_Expression, { kind: ST_ExpressionKind.ForIterator }>) =>
        (this.iterators[name] = expr);
    public removeIterator = (name: string) => delete this.iterators[name];

    /**
     * @description Records a statement.
     * @param {ST_Statement} statement
     */
    public emitStatement = (statement: ST_Statement): void => {
        this.statements = [...this.statements, statement];
    };

    /**
     * @description Generates a variable identifier.
     * @return {string} Variable identifier.
     */
    public generateVarID = (): string => {
        return `var_${this.varCounter++}`;
    };
}

class BaseContext {
    importing: Nullable<string>;
    declaringClass: Nullable<string>;
    declaringMethod: Nullable<string>;
    switchCase: Nullable<{
        variant: string;
        accessExpr: Extract<ST_Expression, { kind: ST_ExpressionKind.VariantAccess }>;
    }>;
    modules: Record<string, ST_ImportedModule> = {};
    exported: string[] = [];
    scopes: Scope[] = [];
    //scenarioScope: TestScope = new TestScope();
    result: ST_Module = {
        contracts: {},
        scope: {
            kind: ST_ScopeKind.Global,
            typeDefs: {},
            properties: {},
            functions: {},
            classes: {},
        },
        scenarios: [],
        warnings: [],
    };

    constructor(public sourceFile: SourceFile, public translators: Translators) {}

    get scope(): Scope {
        return this.scopes?.[this.scopes.length - 1];
    }

    /**
     * @description Filter all non exported (classes, properties, type definition) and return the module scope
     */
    get exportedModule(): ST_Module {
        // Remove any class that was not exported
        Object.keys(this.result.scope.classes).forEach((key) => {
            if (!this.exported.includes(key)) {
                delete this.result.scope.classes[key];
                // Also delete the contract class reference if any
                delete this.result.contracts[key];
            }
        });
        // Remove any property that was not exported
        Object.keys(this.result.scope.properties).forEach((key) => {
            if (!this.exported.includes(key)) {
                delete this.result.scope.properties[key];
            }
        });
        // Remove any type definition that was not exported
        Object.keys(this.result.scope.typeDefs).forEach((key) => {
            if (!this.exported.includes(key)) {
                delete this.result.scope.typeDefs[key];
            }
        });

        return this.result;
    }

    /**
     * @description Add imported module.
     * @param {string} name Module name
     * @param {ST_Module} module Module scope
     * @param {ImportDeclaration} node Import declaration node
     */
    public addModule = (name: string, module: ST_Module, node: ImportDeclaration) => {
        this.modules = {
            ...this.modules,
            [name]: {
                ...module,
                name,
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            },
        };
    };

    /**
     * @description Getter that returns a frozen context of the imported modules
     */
    get importedModules() {
        return Object.freeze(this.modules);
    }

    get classes(): Record<string, ST_Class> {
        return this.result.scope.classes;
    }

    /**
     * @description Get global properties.
     * @returns {ST_Properties} ROOT properties
     */
    get globalProperties(): ST_Properties {
        return this.result.scope.properties;
    }

    /**
     * @description Get Global type definitions.
     * @returns {ST_TypeDefs} ROOT type definitions.
     */
    get globalTypeDefs(): ST_TypeDefs {
        return this.result.scope.typeDefs;
    }

    lambdaCounter = 0;
    get nextLambdaId(): number {
        return this.lambdaCounter++;
    }
}

export class OutputContext extends BaseContext {
    /**
     * @description Initialize new scope.
     */
    enterScope(kind: ST_ScopeKind = ST_ScopeKind.Generic): void {
        this.scopes.push(new Scope(kind, /* Parent scope */ this.scope));
        if (kind === ST_ScopeKind.Global) {
            if (this.scopes.length !== 1) {
                ErrorUtils.failWith('Can only exist a single global scope.');
            }
            this.result.scope = this.scope;
        }
    }

    /**
     * @description Remove previous scope.
     */
    exitScope(): void {
        this.scopes.pop();
    }

    /**
     * @description Get properties by class.
     * @param {string} className
     * @returns {ST_Properties} properties
     */
    getClassProperties(className: string): ST_Properties {
        return this.result.scope.classes[className].scope.properties;
    }

    /**
     * @description Get class method
     * @param {string} className
     * @param {string} methodName
     * @returns {ST_FunctionProperty} The method
     */
    getClassMethod(className: string, methodName: string): ST_FunctionProperty {
        return this.result.scope.classes[className].scope.functions[methodName];
    }

    /**
     * @description Get current class
     * @returns {ST_Class} class being declared
     */
    get currentClass(): ST_Class {
        if (this.declaringClass) {
            return this.result.scope.classes[this.declaringClass];
        }
        return ErrorUtils.failWith('Currently, there is no class being declared.');
    }

    /**
     * @description Get current method being interpreted.
     * @returns {ST_FunctionProperty} The method
     */
    get currentMethod(): ST_FunctionProperty {
        if (this.declaringMethod) {
            if (
                this.declaringClass &&
                this.result.scope.classes[this.declaringClass].scope.functions[this.declaringMethod]
            ) {
                return this.result.scope.classes[this.declaringClass].scope.functions[this.declaringMethod];
            }

            for (const scope of [...this.scopes].reverse()) {
                if (this.declaringMethod in scope.functions) {
                    return scope.functions[this.declaringMethod];
                }
            }
        }
        return ErrorUtils.failWith('Currently, there is no method being declared.');
    }

    /**
     * @description Get current scenario being interpreted.
     * @returns {ST_Scenario} The scenario
     */
    get currentScenario(): ST_Scenario {
        if (this.result.scenarios.length > 0) {
            return [...this.result.scenarios].reverse()[0];
        }
        return ErrorUtils.failWith('Currently, there is no scenario being interpreted.');
    }

    /**
     * @description Record class declaration;
     * @param {ClassDeclaration} node Class declaration node.
     * @returns {string} Class name.
     */
    emitClassDeclaration = (node: ClassDeclaration): string => {
        // Get class name
        const className = IdentifierUtils.extractName(node);
        if (className) {
            // Get Heritage
            const getExtendedClass = (nodes: NodeArray<HeritageClause>): ST_Class => {
                for (const node of nodes) {
                    if (node.token === SyntaxKind.ExtendsKeyword) {
                        // It is an extends heritage
                        const expression = node.types[0].expression;
                        if (isIdentifier(expression)) {
                            const name = IdentifierUtils.extractName(expression)!;
                            return this.classes[name];
                        }
                        if (isPropertyAccessExpression(expression)) {
                            const moduleName = IdentifierUtils.extractName(expression.expression)!;
                            const extendedClassName = IdentifierUtils.extractName(expression)!;

                            const module = this.modules[moduleName];
                            if (!module) {
                                return this.emitError(expression, `Cannot find any module named (${moduleName}).`);
                            }

                            const extendedClass = this.modules[moduleName].scope.classes[extendedClassName];
                            if (!extendedClass) {
                                return this.emitError(
                                    expression,
                                    `Cannot find any class named (${extendedClassName}) in module (${moduleName}).`,
                                );
                            }

                            return extendedClass;
                        }
                    }
                }
                // This should never happen, but we have the logs if it happens.
                return this.emitError(nodes[0], 'Expected an extend heritage, but could not find any.');
            };

            if (node.heritageClauses) {
                // Extract scope to avoid cycle references when cloning class object.
                const { scope, ...classObj } = getExtendedClass(node.heritageClauses);
                this.scope.properties = scope.properties;
                this.scope.functions = scope.functions;
                this.classes[className] = {
                    ...JSON.parse(JSON.stringify(classObj)),
                    scope: this.scope,
                    name: className,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            } else {
                this.classes[className] = {
                    name: className,
                    id: Object.keys(this.classes).length,
                    constructorArgs: {},
                    decorators: {},
                    entry_points: {},
                    off_chain_view: {},
                    global_lambdas: {},
                    metadata: {},
                    scope: this.scope,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            }
        } else {
            return this.emitError(node, 'Anonymous classes are not allowed.');
        }

        if (ModifiersUtils.extractModifiers(node).includes(ST_Modifier.Export)) {
            this.exported = [...this.exported, className];
        }

        return className;
    };

    /**
     * @description Record type definition.
     * @param {string} name Type name
     * @param {ST_TypeDef} typeDef Type specification
     * @param {ST_Modifier[]} modifiers type modifiers (export, etc...)
     */
    emitTypeDef = (name: string, typeDef: ST_TypeDef, modifiers: ST_Modifier[]): void => {
        // Add type definition to the current scope
        this.scope.typeDefs[name] = typeDef;

        if (modifiers.includes(ST_Modifier.Export)) {
            this.exported = [...this.exported, name];
        }
    };

    /**
     * @description Record variable declaration.
     * @param {ST_VariableStatement} variable
     */
    emitVariableDeclaration = (variable: ST_VariableStatement): void => {
        if (this.scope.properties[variable.name]) {
            return ErrorUtils.failWithInfo({
                msg: `Duplicated variable (${variable.name}).`,
                line: variable.line,
            });
        }
        // Add property to the current scope
        this.scope.properties[variable.name] = variable;

        // Interpret as exported every variable declaration with export modifier and declared on the root scope
        if (variable.modifiers.includes(ST_Modifier.Export) && this.scopes.length === 1) {
            this.exported = [...this.exported, variable.name];
        } else if (this.declaringClass && this.scopes.length === 2) {
            // If on class scope
            this.result.scope.classes[this.declaringClass].scope.properties[variable.name] = variable;
        }
    };

    /**
     * @description Record method declaration.
     * @param {string} name method name
     * @param {ST_TypeDef} type method signature
     * @param {ST_Modifier[]} modifiers method modifiers (export, etc...)
     * @param {Node} node function node
     */
    emitFunctionDeclaration = (
        name: string,
        type: Extract<ST_TypeDef, { type: FrontendType.TFunction | FrontendType.TLambda }>,
        modifiers: ST_Modifier[],
        node: FunctionDeclaration | MethodDeclaration | ArrowFunction,
        decorators: ST_Property_Decorators,
    ): void => {
        const line = LineUtils.getLineAndCharacter(this.sourceFile, node);
        const id = this.nextLambdaId;

        // Check if globalLambda decorator exists and if it is configured as pure
        const isPure = decorators.GlobalLambda ? decorators.GlobalLambda.pure : true; // All lambdas are pure by default
        this.emitPropertyDecorators(decorators);
        // Add function to the current scope
        if (this.scope.parent) {
            this.scope.parent.functions[name] = {
                name,
                type,
                id,
                typeDefs: {},
                properties: {},
                statements: {},
                pure: isPure,
                scope: this.scope,
                decorators,
                node,
                line,
            };
        }

        if (modifiers.includes(ST_Modifier.Export)) {
            this.exported = [...this.exported, name];
        }
    };

    /**
     * @description Record method statement.
     * @param {ST_Statement} statement method statement
     */
    emitFunctionStatement = (statement: ST_Statement): void => {
        if (!this.declaringMethod) {
            return ErrorUtils.failWithInfo({
                msg: 'Cannot emit function statements outside of a function block.',
                line: statement.line,
                fileName: this.sourceFile.fileName,
            });
        }
        this.currentMethod.statements = {
            ...this.currentMethod.statements,
            [Object.keys(this.currentMethod.statements).length]: statement,
        };
    };

    /**
     * @description Record class decorator.
     * @param name Class name
     * @param decorator Class decorator
     */
    emitClassDecorator = (className: string, decorator: ST_Class_Decorator): void => {
        if (decorator.kind === ST_DecoratorKind.Contract) {
            // Means that current class is a contract
            this.result.contracts[className] = {
                name: className,
                classRef: className,
            };
        }
        this.classes[className].decorators[decorator.kind] = decorator;
    };

    /**
     * @description Record property decorator.
     * @param decorator Class decorator
     * @param propertyName Property name
     */
    emitPropertyDecorators = (decorators: ST_Property_Decorators): void => {
        Object.values(decorators).forEach((decorator) => {
            if (decorator && this.declaringClass) {
                switch (decorator.kind) {
                    case ST_DecoratorKind.EntryPoint:
                        // Means that current property is an entry_point
                        this.currentClass.entry_points[decorator.functionRef] = decorator;
                        break;
                    case ST_DecoratorKind.OffChainView:
                        // Means that current property is an off_chain view
                        this.currentClass.off_chain_view[decorator.functionRef] = decorator;
                        break;
                    case ST_DecoratorKind.GlobalLambda:
                        // Means that current property is an global_lambda view
                        this.currentClass.global_lambdas[decorator.functionRef] = decorator;
                        break;
                    case ST_DecoratorKind.MetadataBuilder:
                        this.currentClass.metadata[decorator.name] = decorator.properties;
                        break;
                }
            }
        });
    };

    /**
     * @description Record compilation target.
     * @param {string} name compilation target name
     * @param {string} className Contract className
     * @param {ST_Expression[]} args arguments expected by the class constructor
     */
    emitCompilationTarget = (name: string, className: string, args: ST_TypedExpression[]): void => {
        this.result.scenarios = [
            ...this.result.scenarios,
            {
                kind: ST_ScenarioKind.Compilation,
                name,
                scenario: [
                    this.getNewContractAction(
                        {
                            name,
                            className,
                            args,
                        },
                        this.result.scenarios.reduce(
                            (acc, { kind }) => (kind === ST_ScenarioKind.Compilation ? acc + 1 : acc),
                            0,
                        ),
                    ),
                ],
                longname: name,
                shortname: name,
                show: false,
            },
        ];
    };

    /**
     * @description Emit scenario.
     * @param {ST_Scenario} scenario
     */
    emitScenario = (scenario: ST_Scenario): void => {
        this.result.scenarios = [...this.result.scenarios, scenario];
    };

    /**
     * @description Emit a scenario action.
     * @param {ST_ScenarioAction} action Scenario Action
     */
    emitScenarioAction = (action: ST_ScenarioAction): void => {
        this.currentScenario.scenario = [...this.currentScenario.scenario, action];
    };

    emitError = (node: Node, msg: string): never => {
        return ErrorUtils.failWithInfo({
            msg,
            line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            text: node.getText(this.sourceFile),
            fileName: this.sourceFile.fileName,
        });
    };

    emitWarning = (node: Node, msg: string): void => {
        this.result.warnings = [
            ...this.result.warnings,
            {
                msg,
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            },
        ];
    };

    getResult(): ST_Scenario[] {
        return this.result.scenarios;
    }

    public exportContract = (
        contract: ST_Class,
        storage: ST_Expression & { type?: ST_TypeDef },
        initialBalance = '0',
    ): string => {
        const templateId = staticId(contract.id, LineUtils.getLineNumber(contract.line));
        const storageString = `${this.translators.Expression.translateExpression(storage)}`;
        const storageType = `${TypeTranslator.translateType(storage?.type || TypeBuilder.unknown())}`;
        const messages = `${this.buildMessages(contract)}`;
        const views = `${this.buildViews(contract)}`;
        const globals = `${this.buildGlobals(contract)}`;
        const initial_metadata = `${this.buildMetadata(contract)}`;

        return `(
            template_id ${templateId}
            storage ${storageString}
            storage_type (${storageType})
            messages (${messages})
            flags (${this.stringifyFlags(contract)})
            globals (${globals})
            views (${views})
            entry_points_layout ()
            initial_metadata (${initial_metadata})
            balance ${this.translators.Expression.translateExpression(
                ExpressionBuilder.mutezLiteral(initialBalance, contract.line),
            )}
        )`;
    };

    public getNewContractAction = (
        compilation: ST_CompilationTarget,
        id: number,
        show = true,
        initialBalance = '0',
    ): ST_ScenarioAction => {
        const contract = this.classes[compilation.className];
        const storage: ST_TypedExpression = compilation.args?.[contract.constructorArgs?.['storage']?.index] ||
            contract.constructorArgs['storage']?.initializer ||
            contract.scope.properties['storage']?.expression || {
                kind: ST_ExpressionKind.LiteralExpr,
                type: {
                    type: FrontendType.TUnit,
                },
                literal: {
                    kind: ST_LiteralKind.Unit,
                    line: contract.line,
                },
                line: contract.line,
            };
        const templateId = staticId(id, LineUtils.getLineNumber(contract.line));
        const exportInfo = this.exportContract(contract, storage, initialBalance);
        return {
            action: ST_ScenarioActionKind.NEW_CONTRACT,
            name: compilation.className,
            id: templateId,
            export: exportInfo,
            line_no: LineUtils.getLineNumber(contract.line),
            show,
            accept_unknown_types: false,
        };
    };

    stringifyFlags(contract: ST_Class): string {
        const flags = contract.decorators.Contract?.flags || [];
        const getFlag = ({ name, args }: ST_Flag): string => {
            return '(' + `${name} ${args.join(' ')}`.trim() + ')';
        };
        return flags.map<string>(getFlag).join(' ');
    }

    buildSubEntryPointStatements(func: ST_FunctionProperty): ST_FunctionProperty {
        const line = func.line;
        const id = func.id;
        return {
            ...func,
            statements: {
                // Add __operations__
                0: {
                    kind: ST_StatementKind.VariableStatement,
                    expression: {
                        kind: ST_ExpressionKind.VariableDeclaration,
                        name: '__operations__',
                        type: {
                            type: FrontendType.TList,
                            innerTypes: [
                                {
                                    type: FrontendType.TOperation,
                                },
                            ],
                        },
                        value: {
                            kind: ST_ExpressionKind.TypeAnnotation,
                            expression: {
                                kind: ST_ExpressionKind.ArrayLiteralExpression,
                                elements: [],
                                type: {
                                    type: FrontendType.TList,
                                    innerTypes: [
                                        {
                                            type: FrontendType.TOperation,
                                        },
                                    ],
                                },
                                line,
                            },
                            type: {
                                type: FrontendType.TList,
                                innerTypes: [
                                    {
                                        type: FrontendType.TOperation,
                                    },
                                ],
                            },
                            line,
                        },
                        line,
                    },
                    flags: NodeFlags.Const,
                    name: '__operations__',
                    modifiers: [],
                    decorators: {},
                    type: {
                        type: FrontendType.TList,
                        innerTypes: [
                            {
                                type: FrontendType.TOperation,
                            },
                        ],
                    },
                    line,
                },
                1: {
                    kind: ST_StatementKind.VariableStatement,
                    expression: {
                        kind: ST_ExpressionKind.VariableDeclaration,
                        name: '__storage__',
                        type: {
                            type: FrontendType.TUnknown,
                        },
                        value: {
                            kind: ST_ExpressionKind.TypeAnnotation,
                            expression: {
                                kind: ST_ExpressionKind.AttrAccessExpr,
                                attr: 'in_storage',
                                type: {
                                    type: FrontendType.TUnknown,
                                },
                                prev: {
                                    kind: ST_ExpressionKind.LambdaParamAccessExpr,
                                    attr: '',
                                    id,
                                    pure: true,
                                    singleParam: true,
                                    type: {
                                        type: FrontendType.TUnknown,
                                    },
                                    line,
                                },
                                line,
                            },
                            type: {
                                type: FrontendType.TUnknown,
                            },
                            line,
                        },
                        line,
                    },
                    flags: NodeFlags.Const,
                    name: '__storage__',
                    modifiers: [],
                    decorators: {},
                    type: {
                        type: FrontendType.TUnknown,
                    },
                    line,
                },
                2: {
                    kind: ST_StatementKind.Bind,
                    name: `__bind${id}`,
                    statements: func.statements,
                    line,
                },
                3: {
                    kind: ST_StatementKind.Result,
                    expression: {
                        kind: ST_ExpressionKind.ObjectLiteralExpression,
                        properties: {
                            operations: {
                                kind: ST_ExpressionKind.MethodPropAccessExpr,
                                attr: '__operations__',
                                property: undefined as any,
                                type: {
                                    type: FrontendType.TList,
                                    innerTypes: [{ type: FrontendType.TOperation }],
                                },
                                line,
                            },
                            storage: {
                                kind: ST_ExpressionKind.MethodPropAccessExpr,
                                attr: '__storage__',
                                type: {
                                    type: FrontendType.TUnknown,
                                },
                                line,
                            },
                            result: {
                                kind: ST_ExpressionKind.MethodPropAccessExpr,
                                attr: `__bind${id}`,
                                type: func.type.outputType,
                                line,
                            },
                        },
                        type: {
                            type: FrontendType.TRecord,
                            properties: {
                                operations: {
                                    type: FrontendType.TList,
                                    innerTypes: [
                                        {
                                            type: FrontendType.TOperation,
                                        },
                                    ],
                                },
                                storage: {
                                    type: FrontendType.TUnknown,
                                },
                                result: func.type.outputType,
                            },
                        },
                        line,
                    },
                    line,
                },
            },
        };
    }

    buildMessages(contract: ST_Class): string {
        /**
         * For each entry_point:
         *  - Template: (name private(bool) has_params(bool) line_no (instructions))
         */
        return Object.values(contract.entry_points)
            .map((ep): string | void => {
                const func = this.getClassMethod(contract.name, ep.functionRef);
                if (guards.type.isFunction(func.type)) {
                    const notMock = this.capitalizeBoolean(!ep.mock);
                    const isLazy = this.capitalizeBoolean(ep.lazy);
                    const emptyLazy = this.capitalizeBoolean(ep.lazy_no_code);
                    const hasParams = this.capitalizeBoolean(Object.keys(func.type.inputTypes).length > 0);
                    const line = LineUtils.getLineNumber(func.line);
                    const inputTypes = TypeTranslator.translateInputType(func.type);
                    const statements = this.translators.Statement.translateStatements(func.statements);
                    return `(${ep.name} ${notMock} ${isLazy} ${emptyLazy} ${hasParams} ${line} (${
                        inputTypes ? `${inputTypes} ` : ''
                    }${statements}))`;
                }
            })
            .filter(nonFalsy)
            .join(' ');
    }

    buildViews(contract: ST_Class): string {
        /**
         * For each offchain_view:
         *  - Template: (name has_params(bool) line_no is_pure doc (instructions))
         */
        return Object.values(contract.off_chain_view)
            .map(({ name, pure, description = '', functionRef }): string | void => {
                const func = this.getClassMethod(contract.name, functionRef);
                if (guards.type.isFunction(func.type)) {
                    const isPure = this.capitalizeBoolean(pure);
                    const docString = JSON.stringify(description);
                    const hasParams = this.capitalizeBoolean(Object.keys(func.type.inputTypes).length > 0);
                    const line = LineUtils.getLineNumber(func.line);
                    const inputTypes = TypeTranslator.translateInputType(func.type);
                    const statements = this.translators.Statement.translateStatements(func.statements);
                    return `(${name} ${hasParams} ${line} ${isPure} ${docString} (${
                        inputTypes ? ` ${inputTypes}` : ''
                    }${statements}))`;
                }
            })
            .filter(nonFalsy)
            .join(' ');
    }

    buildGlobals(contract: ST_Class): string {
        /**
         * For each global_view:
         *  - Template: (<name> (lambda <id> <argNames> line_no (instructions))
         */
        return Object.values(contract.global_lambdas)
            .map(({ name, functionRef }): string | void => {
                const func = this.getClassMethod(contract.name, functionRef);
                if (guards.type.isFunction(func.type)) {
                    if (func.pure) {
                        const expression: ST_Expression = {
                            kind: ST_ExpressionKind.LambdaExpression,
                            lambda: func,
                            line: func.line,
                        };
                        return `(${name} ${this.translators.Expression.translateExpression(expression)})`;
                    } else {
                        const expression: ST_Expression = {
                            kind: ST_ExpressionKind.LambdaExpression,
                            lambda: this.buildSubEntryPointStatements(func),
                            line: func.line,
                        };
                        return `(${name} ${this.translators.Expression.translateExpression(expression)})`;
                    }
                }
            })
            .filter(nonFalsy)
            .join(' ');
    }

    buildMetadata(contract: ST_Class): string {
        return Object.entries(contract.metadata)
            .map(([name, properties]): string | void => {
                return `(${name} ${MetadataTranslator.translateMetadataObject(
                    properties,
                    LineUtils.getLineNumber(contract.line),
                )})`;
            })
            .join(' ');
    }

    capitalizeBoolean = (bool: boolean): string => (bool ? 'True' : 'False');
}

export default OutputContext;
