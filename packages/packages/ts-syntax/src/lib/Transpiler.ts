import {
    isArrowFunction,
    isCallExpression,
    isClassDeclaration,
    isEnumDeclaration,
    isExpressionStatement,
    isFunctionDeclaration,
    isImportDeclaration,
    isInterfaceDeclaration,
    isStringLiteral,
    isTypeAliasDeclaration,
    isVariableDeclaration,
    isVariableStatement,
    SourceFile,
    Statement,
    Node,
    SyntaxKind,
    isPropertyAccessExpression,
    isQualifiedName,
    NodeFlags,
    isNamespaceImport,
    ImportDeclaration,
} from 'typescript';

import { createProgram } from 'typescript';

import { STCompilerHost } from './CompilerHost';
import { CompilerOptions } from '../constants';
import OutputContext from './OutputContext';
import TypeInterpreter from './interpreter/type';
import LiteralInterpreter from './interpreter/literal';
import DecoratorInterpreter from './interpreter/decorator';
import DeclarationInterpreter from './interpreter/declaration';
import StatementInterpreter from './interpreter/statement';

import typings from '../ide.generated';
import logger from '../services/Logger';
import { ST_ScopeKind } from '../enums/scope';
import IdentifierUtils from './utils/Identifier';
import MetadataInterpreter from './interpreter/metadata';
import { ST_FileExtension } from '../enums/import';
import FileUtils from './utils/file/FileUtils';
import InliningInterpreter from './interpreter/inlining';
import type { FileInfo } from '../@types/common';
import type { ST_Scenario } from '../@types/scenario';
import ScenarioInterpreter from './interpreter/scenario';
import ExpressionTranslator from './translator/expression';
import StatementTranslator from './translator/statement';

enum InterpreterKind {
    Declaration = 'Declaration',
    Decorator = 'Decorator',
    Literal = 'Literal',
    Statement = 'Statement',
    Type = 'Type',
    Metadata = 'Metadata',
    Inlining = 'Inlining',
    Scenario = 'Scenario',
}

enum TranslatorKind {
    Expression = 'Expression',
    Statement = 'Statement',
}

export type Interpreters = Readonly<{
    [InterpreterKind.Declaration]: DeclarationInterpreter;
    [InterpreterKind.Decorator]: DecoratorInterpreter;
    [InterpreterKind.Literal]: LiteralInterpreter;
    [InterpreterKind.Statement]: StatementInterpreter;
    [InterpreterKind.Type]: TypeInterpreter;
    [InterpreterKind.Metadata]: MetadataInterpreter;
    [InterpreterKind.Inlining]: InliningInterpreter;
    [InterpreterKind.Scenario]: ScenarioInterpreter;
}>;

export type Translators = Readonly<{
    [TranslatorKind.Expression]: ExpressionTranslator;
    [TranslatorKind.Statement]: StatementTranslator;
}>;

export class Transpiler {
    sourceFile: SourceFile;
    output: OutputContext;

    interpreters: Interpreters;
    translators: Translators;

    constructor(public source: FileInfo) {
        // Virtual source file name
        source.name ||= 'main.ts';

        // File name must end with .ts extension
        if (!source.name.endsWith('.ts')) {
            source.name += '.ts';
        }

        const compilerHost = new STCompilerHost([
            {
                name: source.name,
                code: source.code,
            },
            {
                name: 'global.ts',
                code: typings,
            },
        ]);
        const program = createProgram({
            rootNames: [source.name, 'global.ts'],
            options: CompilerOptions,
            host: compilerHost,
        });

        const sourceFile = program.getSourceFile(source.name);
        if (!sourceFile) {
            throw Error('The source code is invalid.');
        }

        this.sourceFile = sourceFile;

        this.interpreters = Object.freeze({
            [InterpreterKind.Declaration]: new DeclarationInterpreter(this),
            [InterpreterKind.Decorator]: new DecoratorInterpreter(this),
            [InterpreterKind.Literal]: new LiteralInterpreter(this),
            [InterpreterKind.Statement]: new StatementInterpreter(this),
            [InterpreterKind.Type]: new TypeInterpreter(this),
            [InterpreterKind.Metadata]: new MetadataInterpreter(this),
            [InterpreterKind.Inlining]: new InliningInterpreter(this),
            [InterpreterKind.Scenario]: new ScenarioInterpreter(this),
        });

        this.translators = Object.freeze({
            [TranslatorKind.Expression]: new ExpressionTranslator(this),
            [TranslatorKind.Statement]: new StatementTranslator(this),
        });

        this.output = new OutputContext(sourceFile, this.translators);
    }

    transpile = async (): Promise<ST_Scenario[]> => {
        // 1. Visit import statements
        let children = await this.sourceFile.statements.reduce<Promise<Statement[]>>(async (prev, statement) => {
            if (isImportDeclaration(statement)) {
                await this.visitImportDeclaration(statement);
                return await prev;
            }

            return [...(await prev), statement];
        }, Promise.resolve([]));

        // Initialize the ROOT scope
        this.output.enterScope(ST_ScopeKind.Global);

        // 2. Visit type definitions
        children = children.filter((statement) => {
            if (isInterfaceDeclaration(statement)) {
                this.interpreters.Type.visitInterfaceDeclaration(statement);
                return false;
            }
            if (isTypeAliasDeclaration(statement)) {
                this.interpreters.Type.visitTypeAliasDeclaration(statement);
                return false;
            }
            return true;
        });
        // 3. Visit variable statements
        children = children.filter((statement) => {
            if (isEnumDeclaration(statement)) {
                this.output.emitVariableDeclaration(this.interpreters.Statement.extractEnumDeclaration(statement));
                return false;
            }
            if (isVariableStatement(statement)) {
                statement.declarationList.declarations.forEach((st) => {
                    if (isVariableDeclaration(st) && st.initializer && isArrowFunction(st.initializer)) {
                        this.interpreters.Declaration.visitFunctionDeclarationNode(st);
                    } else {
                        this.output.emitVariableDeclaration(
                            this.interpreters.Statement.extractVariableInitializer(st, NodeFlags.Const),
                        );
                    }
                });
                return false;
            }
            return true;
        });
        // 4. Visit function declarations
        children = children.filter((statement) => {
            if (isFunctionDeclaration(statement)) {
                this.interpreters.Declaration.visitFunctionDeclarationNode(statement);
                return false;
            }
            return true;
        });
        // 5. Visit class declarations
        children = children.filter((statement) => {
            if (isClassDeclaration(statement)) {
                this.interpreters.Declaration.visitClassDeclarationNode(statement);
                return false;
            }
            return true;
        });

        // 6. Visit expression statements
        children = children.filter((statement) => {
            if (isExpressionStatement(statement) && isCallExpression(statement.expression)) {
                this.interpreters.Scenario.resolveDevExpression(statement.expression);
                return false;
            }
            return true;
        });

        if (children.length > 0) {
            // @TODO - Remove when complete
            logger.error('Remaining ROOT nodes', children);
        }

        // Terminate ROOT scope
        this.output.exitScope();

        return this.output.getResult();
    };

    /**
     * @description Extract node name.
     * @param {Node} node
     * @returns {string} node name
     */
    extractName = (node: Node): string => {
        const name = IdentifierUtils.extractName(node);
        if (name) {
            return name;
        }
        return this.output.emitError(node, `Unexpected unnamed node (${SyntaxKind[node.kind]}).`);
    };

    /**
     * @description Extract expression namespace.
     * @param {Node} node
     * @returns {string[]} namespace path
     */
    extractNamespace = (expr: Node): string[] => {
        if (expr.kind === SyntaxKind.ThisKeyword) {
            return ['this'];
        }
        if (isQualifiedName(expr)) {
            const name = this.extractName(expr.left);
            return [name, ...this.extractNamespace(expr.right)];
        }
        const name = this.extractName(expr);

        if (isPropertyAccessExpression(expr)) {
            return [...this.extractNamespace(expr.expression), name];
        }

        return [name];
    };

    /**
     * @description Get Imported module name
     * @param {ImportDeclaration} node
     * @returns {string} Module name
     */
    getImportName = (node: ImportDeclaration): string => {
        const namedBindings = node.importClause?.namedBindings;
        if (namedBindings && isNamespaceImport(namedBindings)) {
            // Get imported module
            const moduleName = this.extractName(namedBindings.name);
            if (moduleName) {
                return moduleName;
            }
        }

        return this.output.emitError(node, `You must specify an import clause. (import * as <Name> from "...")`);
    };

    /**
     * @description Visit import declaration and apply transformations
     * @param {ImportDeclaration} node
     * @returns {@Promise<void>}
     */
    public async visitImportDeclaration(node: ImportDeclaration): Promise<void> {
        if (isStringLiteral(node.moduleSpecifier)) {
            let filePath: string = node.moduleSpecifier.text;

            if (!filePath.endsWith(`.${ST_FileExtension.TS}`) && !filePath.endsWith(`.${ST_FileExtension.JSON}`)) {
                filePath = filePath.concat(`.${ST_FileExtension.TS}`);
            }

            const moduleName = this.getImportName(node);

            // If window is not undefined, it means we are running on a browser
            const inBrowser = typeof window !== 'undefined';
            const isURL = filePath.startsWith('http');

            if (!isURL && inBrowser) {
                this.output.emitError(node, `Cannot import local files from the browser, you must use an http URL.`);
            }

            // Fetch source code.
            const sourceCode = await FileUtils.getSourceFromFile(filePath, isURL, this.source.baseDir);
            const extension = filePath.split('.').reverse()[0];

            switch (extension) {
                case ST_FileExtension.TS:
                    // Create a new transpiler context to transpile the file being imported.
                    // After transpiling the file, it will be added to the main context as an imported module.
                    const transpiler = new Transpiler({
                        name: moduleName,
                        code: sourceCode,
                    });
                    // Transpile TS CODE
                    await transpiler.transpile();
                    return this.output.addModule(moduleName, transpiler.output.exportedModule, node);
                case ST_FileExtension.JSON:
                // @TODO - Interpret JSON file
                default:
                    this.output.emitError(node, `File extension (${extension}) not supported.`);
            }
        }

        this.output.emitError(
            node,
            `Import statements should have the following format (import A from "some_url_or_path.<extension>").`,
        );
    }
}

export default Transpiler;
