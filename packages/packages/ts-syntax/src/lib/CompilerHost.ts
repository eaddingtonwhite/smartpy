import type { CompilerHost, CompilerOptions, SourceFile } from 'typescript';

import { createSourceFile, getDefaultLibFileName, ScriptKind, ScriptTarget } from 'typescript';
import type { FileInfo } from '../@types/common';

export class STCompilerHost implements CompilerHost {
    private sourceFiles: {
        [name: string]: SourceFile;
    } = {};

    constructor(files: FileInfo[]) {
        // Instantiate source files
        this.sourceFiles = files.reduce(
            (l, c) => ({
                ...l,
                [c.name]: createSourceFile(c.name, c.code, ScriptTarget.ES2020, false, ScriptKind.TS),
            }),
            {},
        );
    }

    getSourceFile = (fileName: string): SourceFile => this.sourceFiles[fileName];

    getDefaultLibFileName = (defaultLibOptions: CompilerOptions): string =>
        '/' + getDefaultLibFileName(defaultLibOptions);

    writeFile = (): void => {
        /* do nothing */
    };

    getCurrentDirectory = (): string => '/';

    getDirectories = (_: string): string[] => [];

    fileExists = (fileName: string): boolean => this.sourceFiles[fileName] !== null;

    readFile = (fileName: string) => this.sourceFiles[fileName]?.getFullText();

    getCanonicalFileName = (fileName: string): string => fileName;

    useCaseSensitiveFileNames = (): boolean => true;

    getNewLine = (): string => '\n';

    getEnvironmentVariable = (): string => '';
}

export default CompilerHost;
