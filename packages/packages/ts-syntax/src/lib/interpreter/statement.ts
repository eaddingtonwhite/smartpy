import {
    isBinaryExpression,
    BinaryOperatorToken,
    Expression,
    isPropertyAccessExpression,
    isLiteralExpression,
    BinaryExpression,
    isPostfixUnaryExpression,
    isPrefixUnaryExpression,
    isIdentifier,
    isCallExpression,
    isObjectLiteralExpression,
    isToken,
    isArrayLiteralExpression,
    isAsExpression,
    ObjectLiteralExpression,
    VariableDeclaration,
    PropertyDeclaration,
    Identifier,
    isTypeAliasDeclaration,
    Block,
    isVariableStatement,
    isExpressionStatement,
    isIfStatement,
    Statement,
    isBlock,
    isForStatement,
    isVariableDeclarationList,
    isWhileStatement,
    isForOfStatement,
    isParenthesizedExpression,
    isSwitchStatement,
    EnumDeclaration,
    CallExpression,
    isReturnStatement,
    isArrowFunction,
    isInterfaceDeclaration,
    NodeFlags,
    NodeArray,
    isStringLiteral,
    isNewExpression,
    isPropertyAssignment,
    isShorthandPropertyAssignment,
} from 'typescript';

import { SyntaxKind } from 'typescript';

import { InterpreterBase } from './Base';
import LineUtils from '../utils/Line';
import { ST_StatementKind } from '../../enums/statement';
import * as Native from '../../enums/native';
import { StringOfBinaryToken, ST_BinaryToken, ST_ExpressionKind } from '../../enums/expression';
import { FrontendType } from '../../enums/type';
import guards from '../utils/guard';
import PrinterUtils from '../utils/printer';
import ModifiersUtils from '../utils/Modifiers';
import { ST_ScopeKind } from '../../enums/scope';
import { ST_LiteralKind } from '../../enums/literal';
import CheckerUtils from '../utils/Checker';
import ErrorUtils from '../utils/Error';
import type { ST_FunctionProperty } from '../../@types/property';
import type { FileLineInfo, Nullable } from '../../@types/common';
import type { ST_Expression, ST_TypedExpression } from '../../@types/expression';
import type { ST_Statement, ST_VariableStatement, ST_ExpressionStatement } from '../../@types/statement';
import type { ST_TypeDef, ST_TypeDefs } from '../../@types/type';
import { ST_ScenarioActionKind } from '../../enums/scenario';
import type { ST_ScenarioAction } from '../../@types/scenario';
import { staticId } from '../translator/misc';
import ExpressionBuilder from '../utils/builders/expression';
import ScenarioBuilder from '../utils/builders/scenario';
import TypeBuilder from '../utils/builders/type';

export default class StatementInterpreter extends InterpreterBase {
    visitBlock = (node: Block): void => {
        // Extract and emit all type definitions first (type definition have priority in typescript)
        const statements = node.statements.filter((statement) => {
            if (isTypeAliasDeclaration(statement)) {
                const typeName = this.transpiler.extractName(statement);
                const typeDef = this.interpreters.Type.extractTypeDef(statement.type);
                this.output.emitTypeDef(typeName, typeDef, []);
                return false;
            }
            return true;
        });

        this.extractStatements(statements).forEach((statement) => {
            this.output.emitFunctionStatement(statement);
        });
    };

    extractStatements = (statements: Statement[]): ST_Statement[] => {
        statements = statements.filter((statement) => {
            if (isInterfaceDeclaration(statement)) {
                this.interpreters.Type.visitInterfaceDeclaration(statement);
                return false;
            }
            if (isTypeAliasDeclaration(statement)) {
                this.interpreters.Type.visitTypeAliasDeclaration(statement);
                return false;
            }
            return true;
        });

        statements.forEach((statementNode): void => {
            if (isExpressionStatement(statementNode)) {
                const expression = statementNode.expression;
                if (isBinaryExpression(expression)) {
                    // Is a binary expression
                    // - <var> = 1;
                    // - <var> = 1 + 1;
                    // - <var> += 1; ...
                    const statement = this.extractBinaryExpression(expression);
                    this.output.scope.emitStatement(statement);
                } else if (isCallExpression(expression)) {
                    const line = LineUtils.getLineAndCharacter(this.sourceFile, expression);
                    const name = this.transpiler.extractName(expression);
                    const expr = this.lookupIdentifierInScopes(name, line);
                    if (expr?.kind === ST_ExpressionKind.InlineFunction) {
                        const args = this.extractArgExpressions(expression, expr.function.type.inputTypes);
                        this.interpreters.Inlining.visitInlineCall(args, expr.function, line);
                    } else {
                        const e = this.extractAccessExpression(expression);
                        if (e?.kind !== ST_ExpressionKind.CreateContractResult) {
                            const statement: ST_Statement = {
                                kind: ST_StatementKind.Expression,
                                expression: e,
                                line,
                            };
                            this.output.scope.emitStatement(statement);
                        }
                    }
                }
            } else if (isVariableStatement(statementNode)) {
                const flags = statementNode.declarationList.flags;
                statementNode.declarationList.declarations.map((d) => {
                    const statement = this.extractVariableInitializer(d, flags);
                    this.output.emitVariableDeclaration(statement);
                    this.output.scope.emitStatement(statement);
                });
            } else if (isWhileStatement(statementNode)) {
                // Initialize new scope
                this.output.enterScope();
                const statements = this.extractStatements(Array.from((<Block>statementNode.statement).statements));
                const statement: ST_Statement = {
                    kind: ST_StatementKind.WhileStatement,
                    condition: this.extractExpression(statementNode.expression),
                    statements,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, statementNode),
                };
                // Exit scope
                this.output.exitScope();
                this.output.scope.emitStatement(statement);
            } else if (isForOfStatement(statementNode)) {
                // Initialize new scope
                this.output.enterScope();

                // Get For initialized => for (const <initializer> of something)
                if (!isVariableDeclarationList(statementNode.initializer)) {
                    return this.output.emitError(
                        statementNode,
                        `For statement initialized must have a variable declaration.`,
                    );
                }
                const expression = this.extractExpression(
                    statementNode.expression,
                    TypeBuilder.list([TypeBuilder.unknown()]),
                );
                const type = this.interpreters.Type.checkType(statementNode.expression);
                if (type.type !== FrontendType.TList) {
                    return this.output.emitError(statementNode.expression, `You can only iterate over lists.`);
                }
                const iterator: ST_Expression = {
                    kind: ST_ExpressionKind.ForIterator,
                    name: this.transpiler.extractName(statementNode.initializer.declarations[0]),
                    type: type.innerTypes[0],
                    line: LineUtils.getLineAndCharacter(this.sourceFile, statementNode),
                };
                // Add iterator
                this.output.scope.addIterator(iterator.name, iterator);

                const statements = this.extractStatements(Array.from((<Block>statementNode.statement).statements));
                const statement: ST_Statement = {
                    kind: ST_StatementKind.ForOfStatement,
                    iterator,
                    expression,
                    statements,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, statementNode),
                };

                // Exit scope (iterator will also be removed)
                this.output.exitScope();
                this.output.scope.emitStatement(statement);
            } else if (isForStatement(statementNode)) {
                if (!statementNode.condition) {
                    return this.output.emitError(statementNode, `For statements must have a condition.`);
                }
                if (!statementNode.initializer) {
                    return this.output.emitError(statementNode, `For statements must have an initializer.`);
                }
                if (!statementNode.incrementor) {
                    return this.output.emitError(statementNode, `For statements must have an incrementor.`);
                }
                // Initialize new scope
                this.output.enterScope();
                let initializer: ST_VariableStatement | undefined;
                if (isVariableDeclarationList(statementNode.initializer)) {
                    if (statementNode.initializer.declarations.length !== 1) {
                        this.output.emitError(statementNode, `For statements can only contain one declaration.`);
                    }
                    const flags = statementNode.initializer.flags;
                    initializer = this.extractVariableInitializer(statementNode.initializer.declarations[0], flags);
                    this.output.emitVariableDeclaration(initializer);
                }
                const statements = this.extractStatements(Array.from((<Block>statementNode.statement).statements));
                const statement: ST_Statement = {
                    kind: ST_StatementKind.WhileStatement,
                    initializer,
                    incrementor: this.extractExpression(statementNode.incrementor),
                    condition: this.extractExpression(statementNode.condition),
                    statements,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, statementNode),
                };
                // Exit scope
                this.output.exitScope();
                this.output.scope.emitStatement(statement);
            } else if (isIfStatement(statementNode)) {
                // Initialize new scope
                this.output.enterScope();
                const thenStatement = this.extractStatements(
                    Array.from((<Block>statementNode.thenStatement).statements),
                );
                // Exit scope
                this.output.exitScope();

                let elseStatement: ST_Statement[] = [];
                if (statementNode.elseStatement) {
                    if (isBlock(statementNode.elseStatement)) {
                        // <== else branch

                        // Initialize new scope
                        this.output.enterScope();

                        elseStatement = this.extractStatements(Array.from(statementNode.elseStatement.statements));

                        // Exit scope
                        this.output.exitScope();
                    } else if (isIfStatement(statementNode.elseStatement)) {
                        // <== else if branch

                        // Initialize new scope
                        this.output.enterScope();

                        elseStatement = this.extractStatements([statementNode.elseStatement]);

                        // Exit scope
                        this.output.exitScope();
                    }
                }

                const statement: ST_Statement = {
                    kind: ST_StatementKind.IfStatement,
                    expression: this.extractExpression(statementNode.expression),
                    thenStatement,
                    elseStatement,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, statementNode),
                };
                this.output.scope.emitStatement(statement);
            } else if (isSwitchStatement(statementNode) && isPropertyAccessExpression(statementNode.expression)) {
                let expression = this.extractExpression(statementNode.expression);
                if (
                    expression.kind === ST_ExpressionKind.AttrAccessExpr &&
                    !!expression.prev &&
                    expression.attr === 'kind'
                ) {
                    const variantName = this.transpiler.extractName(statementNode.expression.expression);
                    expression = expression.prev;

                    const cases = statementNode.caseBlock.clauses.reduce((prev, cur) => {
                        const caseName = this.transpiler.extractName(cur);
                        this.output.switchCase = {
                            variant: variantName,
                            accessExpr: {
                                kind: ST_ExpressionKind.VariantAccess,
                                name: caseName,
                                line: LineUtils.getLineAndCharacter(this.sourceFile, statementNode),
                            },
                        };
                        // Initialize new scope
                        this.output.enterScope();
                        const statements = this.extractStatements(Array.from(cur.statements));
                        // Exit scope
                        this.output.exitScope();
                        delete this.output.switchCase;
                        return {
                            ...prev,
                            [caseName]: statements,
                        };
                    }, {});
                    const statement: ST_Statement = {
                        kind: ST_StatementKind.SwitchStatement,
                        expression,
                        cases,
                        line: LineUtils.getLineAndCharacter(this.sourceFile, statementNode),
                    };
                    this.output.scope.emitStatement(statement);
                } else {
                    this.output.emitError(statementNode, `Variant switch cases must use <variant>.kind as expression.`);
                }
            } else if (isBlock(statementNode)) {
                this.extractStatements(Array.from(statementNode.statements));
            } else if (isReturnStatement(statementNode)) {
                if (this.output.currentMethod.type.type !== FrontendType.TLambda) {
                    this.output.emitError(statementNode, `Only lambdas can contain return statements.`);
                }
                const line = LineUtils.getLineAndCharacter(this.sourceFile, statementNode);
                let expression: ST_Expression = {
                    kind: ST_ExpressionKind.LiteralExpr,
                    type: {
                        type: FrontendType.TUnit,
                    },
                    literal: { kind: ST_LiteralKind.Unit, line },
                    line,
                };
                if (statementNode.expression) {
                    expression = this.extractExpression(statementNode.expression);
                }
                const statement: ST_Statement = {
                    kind: ST_StatementKind.Result,
                    expression,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, statementNode),
                };
                this.output.scope.emitStatement(statement);
            } else {
                this.output.emitError(statementNode, `Unexpected expression kind (${SyntaxKind[statementNode.kind]})`);
            }
        });

        return this.output.scope.statements;
    };

    extractBinaryOperator = (token: BinaryOperatorToken): ST_BinaryToken => {
        switch (token.kind) {
            case SyntaxKind.EqualsToken:
                return ST_BinaryToken.Equals;
            //
            case SyntaxKind.EqualsEqualsToken:
                return ST_BinaryToken.EqualsEquals;
            case SyntaxKind.EqualsEqualsEqualsToken:
                return ST_BinaryToken.EqualsEqualsEquals;
            case SyntaxKind.ExclamationEqualsToken:
                return ST_BinaryToken.ExclamationEquals;
            case SyntaxKind.ExclamationEqualsEqualsToken:
                return ST_BinaryToken.ExclamationEqualsEquals;
            //
            case SyntaxKind.PlusToken:
                return ST_BinaryToken.Plus;
            case SyntaxKind.MinusToken:
                return ST_BinaryToken.Minus;
            case SyntaxKind.AsteriskToken:
                return ST_BinaryToken.Mul;
            case SyntaxKind.SlashToken:
                return ST_BinaryToken.Div;
            case SyntaxKind.PercentToken:
                return ST_BinaryToken.Mod;
            //
            case SyntaxKind.MinusEqualsToken:
                return ST_BinaryToken.MinusEquals;
            case SyntaxKind.FirstCompoundAssignment:
                return ST_BinaryToken.PlusEquals;
            case SyntaxKind.AsteriskEqualsToken:
                return ST_BinaryToken.MulEquals;
            case SyntaxKind.SlashEqualsToken:
                return ST_BinaryToken.DivEquals;
            //
            case SyntaxKind.LessThanToken:
                return ST_BinaryToken.LessThan;
            case SyntaxKind.GreaterThanToken:
                return ST_BinaryToken.GreaterThan;
            case SyntaxKind.LessThanEqualsToken:
                return ST_BinaryToken.LessThanEquals;
            case SyntaxKind.GreaterThanEqualsToken:
                return ST_BinaryToken.GreaterThanEquals;
            //
            case SyntaxKind.BarBarToken:
                return ST_BinaryToken.Or;
            case SyntaxKind.AmpersandAmpersandToken:
                return ST_BinaryToken.And;
        }

        return this.output.emitError(token, `Invalid Binary Operator (${SyntaxKind[token.kind]}).`);
    };

    extractExpression = (expression: Expression, type?: ST_TypeDef): ST_Expression => {
        if (isPostfixUnaryExpression(expression)) {
            switch (expression.operator) {
                case SyntaxKind.PlusPlusToken:
                case SyntaxKind.MinusMinusToken:
                    return this.output.emitError(expression, 'Prefix unary expressions (++i/--i) are not allowed.');
            }
        }
        if (isPrefixUnaryExpression(expression)) {
            switch (expression.operator) {
                case SyntaxKind.ExclamationToken:
                    // negation (!<expression>)
                    return {
                        kind: ST_ExpressionKind.Not,
                        expression: this.extractExpression(expression.operand),
                        line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                    };
                case SyntaxKind.PlusPlusToken:
                case SyntaxKind.MinusMinusToken:
                    return this.output.emitError(expression, 'Prefix unary expressions (++i/--i) are not allowed.');
            }
        }
        if (isBinaryExpression(expression)) {
            return this.extractBinaryExpression(expression).expression;
        }
        if (isPropertyAccessExpression(expression)) {
            if (isCallExpression(expression.expression)) {
                // This means that the returned value is being used after a call expression
                // Example: => prop.method(...).something
                return {
                    kind: ST_ExpressionKind.AttrAccessExpr,
                    attr: this.transpiler.extractName(expression.name),
                    type: {
                        type: FrontendType.TUnknown,
                    },
                    prev: this.extractExpression(expression.expression),
                    line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                };
            }
            return this.extractAccessExpression(expression);
        }
        if (isIdentifier(expression) || isCallExpression(expression)) {
            return this.extractAccessExpression(expression, type);
        }
        if (isArrayLiteralExpression(expression)) {
            const t = type || this.interpreters.Type.checkType(expression);
            if (guards.type.isTuple(t)) {
                return {
                    kind: ST_ExpressionKind.ArrayLiteralExpression,
                    elements: expression.elements.map((el, i) => this.extractExpression(el, t.innerTypes[i])),
                    type: t,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                };
            }
            if (guards.type.isMap(t)) {
                return {
                    kind: ST_ExpressionKind.ArrayLiteralExpression,
                    elements: expression.elements.map((el) => {
                        if (isArrayLiteralExpression(el)) {
                            return {
                                kind: ST_ExpressionKind.ArrayLiteralExpression,
                                elements: el.elements.map((entry, i) =>
                                    this.extractExpression(entry, i === 0 ? t.keyType : t.valueType),
                                ),
                                type: {
                                    type: FrontendType.TTuple,
                                    innerTypes: [t.keyType, t.valueType],
                                    line: LineUtils.getLineAndCharacter(this.sourceFile, el),
                                },
                                line: LineUtils.getLineAndCharacter(this.sourceFile, el),
                            };
                        }
                        return this.output.emitError(
                            expression,
                            `Expected an array object, but received (${PrinterUtils.type.toString(t)}).`,
                        );
                    }),
                    type: t,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                };
            }
            this.output.emitError(
                expression,
                `Expected an array object, but received (${PrinterUtils.type.toString(t)}).`,
            );
        }
        if (isObjectLiteralExpression(expression)) {
            const t = type || this.interpreters.Type.checkType(expression);
            if (guards.type.hasProperties(t)) {
                return this.extractObjectLiteralExpression(expression, t);
            }
            this.output.emitError(expression, `Expected an object, but received (${PrinterUtils.type.toString(t)}).`);
        }
        if (isAsExpression(expression)) {
            return {
                kind: ST_ExpressionKind.AsExpression,
                type: this.interpreters.Type.extractTypeDef(expression.type),
                expression: this.extractExpression(expression.expression),
                line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
            };
        }
        if (isLiteralExpression(expression) || isToken(expression)) {
            const t = type || this.interpreters.Type.checkType(expression);
            return {
                kind: ST_ExpressionKind.LiteralExpr,
                type: t,
                literal: this.interpreters.Literal.extractLiteral(t, expression),
                line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
            };
        }
        if (isParenthesizedExpression(expression)) {
            return this.extractExpression(expression.expression);
        }
        if (isArrowFunction(expression)) {
            const lambdaId = this.output.nextLambdaId;
            const line = LineUtils.getLineAndCharacter(this.sourceFile, expression);
            this.output.enterScope(ST_ScopeKind.Method);
            // Set declaringMethod since it is being processed
            const prevMethod = this.output.declaringMethod;
            this.output.declaringMethod = `lambda_${lambdaId}`;
            const lambdaType = this.interpreters.Type.checkType(expression);

            // This should never happen, but enforces the correct type to be used bellow
            if (!guards.type.isFunction(lambdaType)) {
                return this.output.emitError(expression, `Cannot identify lambda expression.`);
            }

            const lambda = (this.output.scope.functions[`lambda_${lambdaId}`] = {
                name: `lambda_${lambdaId}`,
                type: lambdaType,
                id: lambdaId,
                typeDefs: {},
                properties: {},
                pure: true,
                statements: {},
                scope: this.output.scope,
                decorators: {},
                node: expression,
                line,
            });
            this.interpreters.Declaration.visitFunctionNode(expression, /* inlined */ false);

            // Update declaringMethod to the previous one (closure)
            this.output.declaringMethod = prevMethod;
            this.output.exitScope();

            return {
                kind: ST_ExpressionKind.LambdaExpression,
                lambda,
                line,
            };
        }
        return this.output.emitError(expression, `Unexpected Expression (${SyntaxKind[expression.kind]}).`);
    };

    extractFunction = (expression: CallExpression): ST_FunctionProperty | void => {
        const namespace = this.transpiler.extractNamespace(expression.expression);

        // Check if function comes from another module
        if (namespace.length === 2 && this.output.modules[namespace[0]]?.scope.functions[namespace[1]]) {
            return this.output.modules[namespace[0]].scope.functions[namespace[1]];
        }

        for (const scope of [...this.output.scopes].reverse()) {
            if (namespace.length === 1 && scope.functions[namespace[0]]) {
                return scope.functions[namespace[0]];
            }
        }
    };

    extractObjectLiteralExpression = (
        expression: ObjectLiteralExpression,
        type: Extract<ST_TypeDef, { type: FrontendType.TRecord | FrontendType.TVariant }>,
    ): Extract<ST_Expression, { kind: ST_ExpressionKind.ObjectLiteralExpression }> => {
        return expression.properties.reduce(
            (prev, cur) => {
                const propName = this.transpiler.extractName(cur);
                if (type.type === FrontendType.TRecord && !type.properties[propName]) {
                    this.output.emitError(
                        cur,
                        `Field ${propName} does not exist on type ${PrinterUtils.type.toString(type)}.`,
                    );
                }
                switch (cur.kind) {
                    case SyntaxKind.PropertyAssignment:
                        prev.properties = {
                            ...prev.properties,
                            [propName]: this.extractExpression(cur.initializer, type.properties[propName]),
                        };
                        break;
                    case SyntaxKind.ShorthandPropertyAssignment:
                        prev.properties = {
                            ...prev.properties,
                            [propName]: this.extractAccessExpression(cur.name),
                        };
                        break;
                    default:
                        return this.output.emitError(
                            cur,
                            `Expected property assignment, but received (${SyntaxKind[cur.kind]}).`,
                        );
                }
                return prev;
            },
            {
                kind: ST_ExpressionKind.ObjectLiteralExpression,
                properties: {},
                type,
                line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
            },
        );
    };

    extractEnumExpression = (
        expression: EnumDeclaration,
        type: Extract<ST_TypeDef, { type: FrontendType.TRecord }>,
    ): Extract<ST_Expression, { kind: ST_ExpressionKind.ObjectLiteralExpression }> => {
        return expression.members.reduce(
            (prev, cur) => {
                const propName = this.transpiler.extractName(cur);
                const line = LineUtils.getLineAndCharacter(this.sourceFile, cur);
                prev.properties = {
                    ...prev.properties,
                    [propName]: cur.initializer
                        ? this.extractExpression(cur.initializer)
                        : {
                              kind: ST_ExpressionKind.LiteralExpr,
                              literal: {
                                  kind: ST_LiteralKind.String,
                                  value: propName,
                              },
                              type: {
                                  type: FrontendType.TString,
                              },
                              line,
                          },
                };

                return prev;
            },
            {
                kind: ST_ExpressionKind.ObjectLiteralExpression,
                properties: {},
                type,
                line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
            },
        );
    };

    lookupIdentifierInScopes = (identifier: string, line: FileLineInfo): Nullable<ST_Expression> => {
        for (const scope of [...this.output.scopes].reverse()) {
            // Check iterators
            if (scope.iterators[identifier]) {
                return scope.iterators[identifier];
            }
            if (scope.kind === ST_ScopeKind.Method && this.output.currentMethod.type.inputTypes[identifier]) {
                const paramTypes = this.output.currentMethod.type.inputTypes;
                const singleParam = Object.keys(paramTypes).length === 1;
                return this.output.currentMethod.type.type === FrontendType.TLambda &&
                    !this.output.currentMethod.decorators.OffChainView
                    ? this.output.currentMethod.pure
                        ? {
                              kind: ST_ExpressionKind.LambdaParamAccessExpr,
                              attr: identifier,
                              singleParam,
                              pure: this.output.currentMethod.pure,
                              id: this.output.currentMethod.id,
                              line,
                              type: paramTypes[identifier],
                          }
                        : {
                              kind: ST_ExpressionKind.AttrAccessExpr,
                              attr: 'in_param',
                              type: this.output.currentMethod.type.inputTypes[identifier],
                              prev: {
                                  kind: ST_ExpressionKind.LambdaParamAccessExpr,
                                  attr: '',
                                  singleParam,
                                  pure: this.output.currentMethod.pure,
                                  id: this.output.currentMethod.id,
                                  line,
                                  type: {
                                      type: FrontendType.TUnknown,
                                  },
                              },
                              line,
                          }
                    : {
                          kind: ST_ExpressionKind.MethodParamAccessExpr,
                          attr: identifier,
                          type: paramTypes[identifier],
                          singleParam,
                          line,
                      };
            }
            // Check properties
            if (scope.properties[identifier]) {
                const property = scope.properties[identifier];
                if (scope.kind === ST_ScopeKind.Test) {
                    if (property.expression.kind === ST_ExpressionKind.ScenarioTestAccount) {
                        return {
                            kind: ST_ExpressionKind.ScenarioTestAccountAccess,
                            account: property.expression,
                            prop: '',
                            line,
                        };
                    }
                    return property.expression;
                }
                if (scope.kind === ST_ScopeKind.Method || scope.kind === ST_ScopeKind.Generic) {
                    return {
                        kind: ST_ExpressionKind.MethodPropAccessExpr,
                        attr: identifier,
                        property,
                        type: property.type,
                        line,
                    };
                }
                return property.expression;
            }
            if (scope.functions[identifier] && !!scope.functions[identifier].decorators.Inline) {
                if (this.output.scope === scope.functions[identifier].scope) {
                    return ErrorUtils.failWithInfo({
                        msg: `Recursion is not allowed here.`,
                        fileName: this.sourceFile.fileName,
                        line,
                    });
                }
                return {
                    kind: ST_ExpressionKind.InlineFunction,
                    function: scope.functions[identifier],
                    line,
                };
            }
            if (scope.functions[identifier] && scope.functions[identifier].type.type === FrontendType.TLambda) {
                return {
                    kind: ST_ExpressionKind.LambdaExpression,
                    lambda: scope.functions[identifier],
                    line,
                };
            }
        }

        if (identifier in this.output.result.contracts) {
            const contractInfo = this.output.result.contracts[identifier];
            return {
                kind: ST_ExpressionKind.ClassAccess,
                class: this.output.classes[contractInfo.classRef],
                line,
            };
        }

        return null;
    };

    extractCallExpression = (expression: CallExpression, typeDef?: ST_TypeDef): ST_Expression => {
        const line = LineUtils.getLineAndCharacter(this.sourceFile, expression);
        const name = this.transpiler.extractName(expression);
        if (isPropertyAccessExpression(expression.expression)) {
            if (expression.expression.expression.kind === SyntaxKind.ThisKeyword) {
                const expr = this.lookupIdentifierInScopes(name, line);
                if (expr?.kind === ST_ExpressionKind.LambdaExpression) {
                    if (expr.lambda.pure) {
                        return {
                            kind: ST_ExpressionKind.LambdaCallExpr,
                            arguments: this.extractExpression(expression.arguments[0]), // Only 1 argument
                            lambda: {
                                kind: ST_ExpressionKind.GlobalLambdaAccessExpr,
                                name,
                                line,
                            },
                            type: expr.lambda.type,
                            line,
                        };
                    } else {
                        return this.resolveImpureLambdaStatements(expr, expression.arguments, line);
                    }
                }
                if (expr?.kind === ST_ExpressionKind.InlineFunction) {
                    const func = expr.function;
                    const argExpressions = this.extractArgExpressions(expression, expr.function.type.inputTypes);
                    // Inline Expansion
                    // - Inlined functions can only have a single statement
                    const statement = func.statements[0];
                    if (statement.kind === ST_StatementKind.Expression) {
                        return this.interpreters.Inlining.expandInlineExpression(statement.expression, argExpressions);
                    } else {
                        return this.output.emitError(
                            expression,
                            `Inline function cannot inline a statement in this location.`,
                        );
                    }
                }
                return this.output.emitError(expression, `Cannot find any reference to function (${name}).`);
            }
            if (isIdentifier(expression.expression.expression)) {
                const namespace = this.transpiler.extractName(expression.expression.expression);
                switch (namespace) {
                    case Native.ST_Namespace.Sp:
                        if (name in Native.ST_NamespaceSpMethod) {
                            // Validate arguments
                            const methodArgs = Native.ArgTypesOfSpMethod[name as Native.ST_NamespaceSpMethod];
                            this.interpreters.Type.validateNativeMethodArgs(methodArgs, expression);
                            // Native call
                            const kind = Native.ExpressionOfNativeCall.Sp[name as Native.ST_NamespaceSpMethod];
                            switch (kind) {
                                case ST_ExpressionKind.Ediv:
                                    if (expression.arguments.length !== 2) {
                                        return this.output.emitError(expression, `(Sp.ediv) expected 2 arguments.`);
                                    }
                                    return {
                                        kind,
                                        left: this.extractExpression(expression.arguments[0]),
                                        right: this.extractExpression(expression.arguments[1]),
                                        type: TypeBuilder.option(
                                            TypeBuilder.tuple([TypeBuilder.unknown(), TypeBuilder.unknown()]),
                                        ),
                                        line,
                                    };
                                case ST_ExpressionKind.Concat:
                                    return {
                                        kind,
                                        expression: this.extractExpression(expression.arguments[0]),
                                        line,
                                    };
                                case ST_ExpressionKind.VotingPower:
                                    return {
                                        kind,
                                        keyHash: this.extractExpression(expression.arguments[0]),
                                        type: TypeBuilder.nat(),
                                        line,
                                    };
                                case ST_ExpressionKind.Verify:
                                    if (name === Native.ST_NamespaceSpMethod.verifyEqual) {
                                        // Extract arguments
                                        const leftExpr = this.extractExpression(expression.arguments[0]);
                                        const rightExpr = this.extractExpression(expression.arguments[1]);

                                        let message: ST_Expression | undefined;
                                        if (expression.arguments?.[2]) {
                                            message = this.extractExpression(expression.arguments[2]);
                                        }

                                        return {
                                            kind: ST_ExpressionKind.Verify,
                                            condition: ExpressionBuilder.binaryExpression(
                                                ExpressionBuilder.pack(leftExpr),
                                                ExpressionBuilder.pack(rightExpr),
                                                ST_BinaryToken.EqualsEquals,
                                                line,
                                            ),
                                            message,
                                            line,
                                        };
                                    }
                                    let message: ST_Expression | undefined;
                                    if (expression.arguments?.[1]) {
                                        message = this.extractExpression(expression.arguments[1]);
                                    }
                                    return {
                                        kind,
                                        condition: this.extractExpression(expression.arguments[0]),
                                        message,
                                        line,
                                    };
                                case ST_ExpressionKind.Variant: {
                                    // Extract arguments
                                    const action = this.extractExpression(
                                        expression.arguments[0],
                                        TypeBuilder.string(),
                                    );
                                    const value = this.extractExpression(expression.arguments[1]);

                                    return {
                                        kind,
                                        action,
                                        value,
                                        line,
                                    };
                                }
                                case ST_ExpressionKind.CheckSignature: {
                                    // Extract arguments
                                    const publicKey = this.extractExpression(
                                        expression.arguments[0],
                                        TypeBuilder.key(),
                                    );
                                    const signature = this.extractExpression(
                                        expression.arguments[1],
                                        TypeBuilder.signature(),
                                    );
                                    const content = this.extractExpression(
                                        expression.arguments[2],
                                        TypeBuilder.bytes(),
                                    );

                                    return {
                                        kind: ST_ExpressionKind.CheckSignature,
                                        publicKey,
                                        signature,
                                        content,
                                        type: TypeBuilder.bool(),
                                        line,
                                    };
                                }
                                case ST_ExpressionKind.PairingCheck: {
                                    // Extract arguments
                                    const expr = this.extractExpression(
                                        expression.arguments[0],
                                        TypeBuilder.list([
                                            TypeBuilder.tuple([TypeBuilder.bls12_381_g1(), TypeBuilder.bls12_381_g2()]),
                                        ]),
                                    );

                                    return {
                                        kind: ST_ExpressionKind.PairingCheck,
                                        expression: expr,
                                        type: TypeBuilder.bool(),
                                        line,
                                    };
                                }
                                case ST_ExpressionKind.SetDelegate: {
                                    // Extract arguments
                                    const baker = this.extractExpression(
                                        expression.arguments[0],
                                        TypeBuilder.option(TypeBuilder.keyHash()),
                                    );

                                    return {
                                        kind: ST_ExpressionKind.SetDelegate,
                                        baker,
                                        line,
                                    };
                                }
                                case ST_ExpressionKind.CreateContractOperation:
                                case ST_ExpressionKind.CreateContract: {
                                    // Extract arguments
                                    const contract = this.extractExpression(expression.arguments[0]);
                                    if (contract.kind !== ST_ExpressionKind.ClassAccess) {
                                        return this.output.emitError(
                                            expression.arguments[0],
                                            'Expected a contract class.',
                                        );
                                    }

                                    // Get the contract storage (Use the class default storage if the user did not provide a storage)
                                    let storage = contract.class.scope.properties['storage']?.expression;
                                    if (expression.arguments[1]) {
                                        storage = this.extractExpression(
                                            expression.arguments[1],
                                            contract.class.scope.properties['storage'].type,
                                        );
                                    }
                                    // Fail if:
                                    // - User did not provide a storage
                                    // - Contract doesn't have a default storage
                                    // - Contract storage type is missing
                                    if (!storage) {
                                        return this.output.emitError(expression, 'The contract storage is missing.');
                                    }
                                    if (!guards.expression.hasType(storage)) {
                                        return this.output.emitError(
                                            expression,
                                            'The contract storage type is missing.',
                                        );
                                    }
                                    // Get the amount (default is 0)
                                    let amount = ExpressionBuilder.mutezLiteral(0, line);
                                    if (expression.arguments[2]) {
                                        amount = this.extractExpression(expression.arguments[2], TypeBuilder.mutez());
                                    }

                                    // Get the baker (optional value)
                                    let baker: ST_Expression | undefined;
                                    if (expression.arguments[3]) {
                                        baker = this.extractExpression(expression.arguments[3], TypeBuilder.keyHash());
                                    }

                                    const originationID = this.output.scope.generateVarID();
                                    const expr = {
                                        id: originationID,
                                        contract: this.output.exportContract(contract.class, storage),
                                        storage,
                                        amount,
                                        baker,
                                        type: TypeBuilder.address(),
                                        line,
                                    };
                                    if (kind === ST_ExpressionKind.CreateContractOperation) {
                                        return {
                                            ...expr,
                                            kind: ST_ExpressionKind.CreateContractOperation,
                                        };
                                    } else {
                                        this.output.scope.emitStatement({
                                            kind: ST_StatementKind.Expression,
                                            expression: {
                                                ...expr,
                                                kind: ST_ExpressionKind.CreateContract,
                                            },
                                            line,
                                        });

                                        return {
                                            kind: ST_ExpressionKind.CreateContractResult,
                                            id: originationID,
                                            type: TypeBuilder.address(),
                                            line,
                                        };
                                    }
                                }
                                case ST_ExpressionKind.ImplicitAccount: {
                                    // Extract arguments
                                    const keyHash = this.extractExpression(
                                        expression.arguments[0],
                                        TypeBuilder.keyHash(),
                                    );

                                    return {
                                        kind,
                                        keyHash,
                                        type: TypeBuilder.contract(TypeBuilder.unit()),
                                        line,
                                    };
                                }
                                case ST_ExpressionKind.HashKey: {
                                    // Extract arguments
                                    const key = this.extractExpression(expression.arguments[0], TypeBuilder.key());

                                    return {
                                        kind,
                                        key,
                                        type: TypeBuilder.keyHash(),
                                        line,
                                    };
                                }
                                case ST_ExpressionKind.ToAddress: {
                                    // Extract arguments
                                    const contract = this.extractExpression(
                                        expression.arguments[0],
                                        TypeBuilder.contract(TypeBuilder.unknown()),
                                    );

                                    return {
                                        kind,
                                        contract,
                                        type: TypeBuilder.address(),
                                        line,
                                    };
                                }
                                case ST_ExpressionKind.FailWith:
                                    if (!expression.arguments?.[0]) {
                                        return this.output.emitError(
                                            expression,
                                            `Call (${kind}) expects an error message as argument.`,
                                        );
                                    }
                                    return {
                                        kind,
                                        message: this.extractExpression(expression.arguments[0]),
                                        line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                                    };
                                case ST_ExpressionKind.Some: {
                                    if (typeDef && typeDef.type !== FrontendType.TOption) {
                                        return this.output.emitError(
                                            expression,
                                            `Sp.some(...) returns an option type, but expected ${PrinterUtils.type.toString(
                                                typeDef,
                                            )}`,
                                        );
                                    }
                                    const inputType =
                                        expression.typeArguments?.length === 1
                                            ? this.interpreters.Type.extractTypeDef(expression.typeArguments[0])
                                            : typeDef?.innerType;
                                    return {
                                        kind,
                                        expression: this.extractExpression(expression.arguments[0], inputType),
                                        type: inputType
                                            ? TypeBuilder.option(inputType)
                                            : this.interpreters.Type.checkType(expression),
                                        line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                                    };
                                }
                                case ST_ExpressionKind.AsExpression:
                                    const inputType = this.interpreters.Type.checkType(expression);
                                    return {
                                        kind,
                                        expression: this.extractExpression(expression.arguments[0], inputType),
                                        type: inputType,
                                        line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                                    };
                                case ST_ExpressionKind.Transfer:
                                    if (expression.arguments.length !== 3) {
                                        return this.output.emitError(
                                            expression,
                                            `${JSON.stringify(
                                                kind,
                                            )} call requires 3 arguments (<params>, <amount>, <contract>).`,
                                        );
                                    }
                                    const amountExpr = this.extractExpression(
                                        expression.arguments[1],
                                        TypeBuilder.mutez(),
                                    );
                                    return {
                                        kind,
                                        param: this.extractExpression(expression.arguments[0]),
                                        amount: amountExpr,
                                        contract: this.extractExpression(expression.arguments[2]),
                                        line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                                    };
                                case ST_ExpressionKind.Blake2b:
                                case ST_ExpressionKind.Sha256:
                                case ST_ExpressionKind.Sha512:
                                case ST_ExpressionKind.Sha3:
                                case ST_ExpressionKind.Keccak:
                                case ST_ExpressionKind.Pack: {
                                    if (expression.arguments.length !== 1) {
                                        return this.output.emitError(expression, `${kind} call expects 1 argument.`);
                                    }
                                    return {
                                        kind,
                                        expression: this.extractExpression(expression.arguments[0]),
                                        line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                                    };
                                }
                                case ST_ExpressionKind.Unpack: {
                                    if (expression.arguments.length !== 1) {
                                        return this.output.emitError(expression, `${kind} call expects 1 argument.`);
                                    }
                                    if (expression.typeArguments?.length !== 1) {
                                        return this.output.emitError(
                                            expression,
                                            `${kind} call expects 1 type argument.`,
                                        );
                                    }
                                    return {
                                        kind: kind,
                                        expression: this.extractExpression(expression.arguments[0]),
                                        type: this.interpreters.Type.extractTypeDef(expression.typeArguments[0]),
                                        line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                                    };
                                }
                                case ST_ExpressionKind.Contract: {
                                    if (expression.typeArguments?.length !== 1) {
                                        return this.output.emitError(
                                            expression,
                                            `${namespace}.${name}<T>(...), T is missing.`,
                                        );
                                    }
                                    const inputType = this.interpreters.Type.extractTypeDef(
                                        expression.typeArguments[0],
                                    );
                                    return {
                                        kind,
                                        address: this.extractExpression(expression.arguments[0]),
                                        epName: expression.arguments[1]
                                            ? this.interpreters.Literal.extractStringLiteral(expression.arguments[1])
                                            : undefined,
                                        paramType: this.interpreters.Type.extractTypeDef(expression.typeArguments[0]),
                                        type: {
                                            type: FrontendType.TOption,
                                            innerType: {
                                                type: FrontendType.TContract,
                                                inputType,
                                            },
                                        },
                                        line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                                    };
                                }
                                case ST_ExpressionKind.SelfEntryPoint: {
                                    return {
                                        kind,
                                        epName: expression.arguments[0]
                                            ? this.interpreters.Literal.extractStringLiteral(expression.arguments[0])
                                            : undefined,
                                        line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                                    };
                                }
                            }
                        }
                        return this.output.emitError(expression, `Cannot evaluated (${namespace}.${name})`);
                    case Native.ST_Namespace.Scenario:
                        switch (name) {
                            case Native.ST_NamespaceScenarioMethod.h1:
                            case Native.ST_NamespaceScenarioMethod.h2:
                            case Native.ST_NamespaceScenarioMethod.h3:
                            case Native.ST_NamespaceScenarioMethod.h4:
                            case Native.ST_NamespaceScenarioMethod.p: {
                                const stringArg = expression.arguments[0];
                                if (!isStringLiteral(stringArg)) {
                                    return this.output.emitError(
                                        stringArg,
                                        `(${namespace}.${name}) call expects a string as argument.`,
                                    );
                                }
                                this.output.emitScenarioAction({
                                    action: ST_ScenarioActionKind.HTML,
                                    inner: stringArg.text,
                                    line_no: LineUtils.getLineNumber(line),
                                    tag: name,
                                });
                                return {
                                    kind: ST_ExpressionKind.ScenarioHtml,
                                    content: ExpressionBuilder.stringLiteral(stringArg.text, line),
                                    line,
                                };
                            }
                            case Native.ST_NamespaceScenarioMethod.tableOfContents: {
                                this.output.emitScenarioAction({
                                    action: ST_ScenarioActionKind.HTML,
                                    inner: '[[TABLEOFCONTENTS]]',
                                    line_no: LineUtils.getLineNumber(line),
                                    tag: 'p',
                                });
                                return {
                                    kind: ST_ExpressionKind.ScenarioHtml,
                                    content: ExpressionBuilder.stringLiteral('[[TABLEOFCONTENTS]]', line),
                                    line,
                                };
                            }
                            case Native.ST_NamespaceScenarioMethod.show: {
                                const options = {
                                    html: true,
                                    compile: false,
                                    stripStrings: false,
                                };
                                if (expression.arguments?.[1]) {
                                    const optionsArg = expression.arguments[1];
                                    // Extract options
                                    if (isObjectLiteralExpression(optionsArg)) {
                                        optionsArg.properties.forEach((prop) => {
                                            if (isPropertyAssignment(prop)) {
                                                const propName = this.transpiler.extractName(prop);
                                                switch (propName) {
                                                    case 'html':
                                                        const htmlValue = this.extractExpression(prop.initializer);
                                                        if (
                                                            guards.expression.isLiteralExpr(htmlValue) &&
                                                            guards.literal.isBoolean(htmlValue.literal)
                                                        ) {
                                                            options.html = htmlValue.literal.value;
                                                        } else {
                                                            this.output.emitError(
                                                                prop,
                                                                `Property (${propName}) type is incorrect (It must be a boolean literal).`,
                                                            );
                                                        }
                                                        break;
                                                    case 'compile':
                                                        const compileValue = this.extractExpression(prop.initializer);
                                                        if (
                                                            guards.expression.isLiteralExpr(compileValue) &&
                                                            guards.literal.isBoolean(compileValue.literal)
                                                        ) {
                                                            options.compile = compileValue.literal.value;
                                                        } else {
                                                            this.output.emitError(
                                                                prop,
                                                                `Property (${propName}) type is incorrect (It must be a boolean literal).`,
                                                            );
                                                        }
                                                        break;
                                                    case 'stripStrings':
                                                        const stripStringsValue = this.extractExpression(
                                                            prop.initializer,
                                                        );
                                                        if (
                                                            guards.expression.isLiteralExpr(stripStringsValue) &&
                                                            guards.literal.isBoolean(stripStringsValue.literal)
                                                        ) {
                                                            options.stripStrings = stripStringsValue.literal.value;
                                                        } else {
                                                            this.output.emitError(
                                                                prop,
                                                                `Property (${propName}) type is incorrect (It must be a boolean literal).`,
                                                            );
                                                        }
                                                        break;
                                                    default:
                                                        this.output.emitWarning(
                                                            prop,
                                                            `Unknown property (${propName}).`,
                                                        );
                                                }
                                            } else {
                                                this.output.emitError(
                                                    prop,
                                                    `Expected a property assignment but got (${
                                                        SyntaxKind[prop.kind]
                                                    }).`,
                                                );
                                            }
                                        });
                                    }
                                }
                                this.output.emitScenarioAction({
                                    action: ST_ScenarioActionKind.SHOW,
                                    expression: this.translators.Expression.translateExpression(
                                        this.extractExpression(expression.arguments[0]),
                                    ),
                                    ...options,
                                    line_no: LineUtils.getLineNumber(line),
                                });
                                return {
                                    kind: ST_ExpressionKind.ScenarioShow,
                                    expression: this.extractExpression(expression.arguments[0]),
                                    ...options,
                                    line,
                                };
                            }
                            case Native.ST_NamespaceScenarioMethod.makeSignature: {
                                const content = this.extractExpression(expression.arguments[1], TypeBuilder.bytes());
                                return {
                                    kind: ST_ExpressionKind.ScenarioMakeSignature,
                                    privateKey: this.extractExpression(expression.arguments[0]),
                                    content,
                                    line,
                                };
                            }
                            case Native.ST_NamespaceScenarioMethod.originate: {
                                // Extract contract compilation
                                const contractArg = expression.arguments[0];
                                if (!isNewExpression(contractArg)) {
                                    return this.output.emitError(
                                        contractArg,
                                        `${name} call expects the contract instantiation as argument.`,
                                    );
                                }
                                const contractClassName = this.transpiler.extractName(contractArg.expression);
                                const contractArguments =
                                    contractArg.arguments?.map((arg, index) => {
                                        const argInfo = Object.values(
                                            this.output.classes[contractClassName].constructorArgs,
                                        ).find((a) => a.index === index);
                                        if (argInfo) {
                                            return this.interpreters.Declaration.extractContractArgument(
                                                arg,
                                                argInfo?.type,
                                            );
                                        } else {
                                            return this.output.emitError(
                                                arg,
                                                `Cannot find argument in the  constructor.`,
                                            );
                                        }
                                    }) || [];
                                const options = { show: true, initialBalance: '0' };
                                if (expression.arguments?.[1]) {
                                    const optionsArg = expression.arguments[1];
                                    // Extract options
                                    if (isObjectLiteralExpression(optionsArg)) {
                                        optionsArg.properties.forEach((prop) => {
                                            if (isPropertyAssignment(prop)) {
                                                const propName = this.transpiler.extractName(prop);
                                                switch (propName) {
                                                    case 'show':
                                                        const showValue = this.extractExpression(prop.initializer);
                                                        if (
                                                            guards.expression.isLiteralExpr(showValue) &&
                                                            guards.literal.isBoolean(showValue.literal)
                                                        ) {
                                                            options.show = showValue.literal.value;
                                                        } else {
                                                            this.output.emitError(
                                                                prop,
                                                                `Property (${propName}) type is incorrect (It must be a boolean literal).`,
                                                            );
                                                        }
                                                        break;
                                                    case 'initialBalance':
                                                        const initialBalanceValue = this.extractExpression(
                                                            prop.initializer,
                                                        );
                                                        if (
                                                            guards.expression.isLiteralExpr(initialBalanceValue) &&
                                                            guards.literal.isNumeric(initialBalanceValue.literal)
                                                        ) {
                                                            options.initialBalance = initialBalanceValue.literal.value;
                                                        } else {
                                                            this.output.emitError(
                                                                prop,
                                                                `Property (${propName}) type is incorrect (It must be a numeric literal).`,
                                                            );
                                                        }
                                                        break;
                                                    default:
                                                        this.output.emitWarning(
                                                            prop,
                                                            `Unknown property (${propName}).`,
                                                        );
                                                }
                                            } else {
                                                this.output.emitError(
                                                    prop,
                                                    `Expected a property assignment but got (${
                                                        SyntaxKind[prop.kind]
                                                    }).`,
                                                );
                                            }
                                        });
                                    }
                                }
                                const contractClass = this.output.classes[contractClassName];
                                if (contractArguments.every(guards.expression.hasType)) {
                                    this.output.emitScenarioAction(
                                        this.output.getNewContractAction(
                                            {
                                                name,
                                                className: contractClassName,
                                                args: contractArguments,
                                            },
                                            contractClass.id,
                                            options.show,
                                            options.initialBalance,
                                        ),
                                    );
                                } else {
                                    return this.output.emitError(contractArg, 'Contract arguments are invalid.');
                                }
                                return {
                                    kind: ST_ExpressionKind.ContractOrigination,
                                    contract: contractClass,
                                    line,
                                };
                            }
                            case Native.ST_NamespaceScenarioMethod.testAccount: {
                                // Extract test account seed
                                const seedArg = expression.arguments[0];
                                if (!isStringLiteral(seedArg)) {
                                    return this.output.emitError(seedArg, `${name} call expects a string as argument.`);
                                }
                                return {
                                    kind: ST_ExpressionKind.ScenarioTestAccount,
                                    seed: seedArg.text,
                                    line,
                                };
                            }
                            case Native.ST_NamespaceScenarioMethod.transfer:
                                {
                                    // Extract contract compilation
                                    const entrypointCallArg = expression.arguments[0];
                                    if (!isCallExpression(entrypointCallArg)) {
                                        return this.output.emitError(
                                            entrypointCallArg,
                                            `(${Native.ST_Namespace.Scenario}.${name}) call expects an entrypoint call as first argument.`,
                                        );
                                    }
                                    const entrypointCallExpr = this.extractExpression(entrypointCallArg);
                                    switch (entrypointCallExpr.kind) {
                                        case ST_ExpressionKind.EntryPointCall:
                                            const options: {
                                                level?: ST_Expression;
                                                amount?: ST_Expression;
                                                chainId?: ST_Expression;
                                                sender?: ST_Expression;
                                                source?: ST_Expression;
                                                now?: ST_Expression;
                                                votingPowers?: ST_Expression;
                                                exception?: ST_Expression;
                                                valid?: ST_Expression;
                                                show?: ST_Expression;
                                            } = {};
                                            if (expression.arguments?.[1]) {
                                                // Extract entrypoint call options
                                                const optionsArg = expression.arguments[1];
                                                if (isObjectLiteralExpression(optionsArg)) {
                                                    optionsArg.properties.forEach((prop): void => {
                                                        const propName = this.transpiler.extractName(prop);
                                                        if (isShorthandPropertyAssignment(prop)) {
                                                            options[propName as keyof typeof options] =
                                                                this.extractAccessExpression(prop.name);
                                                        } else if (isPropertyAssignment(prop)) {
                                                            switch (propName) {
                                                                case 'now':
                                                                case 'amount':
                                                                case 'chainId':
                                                                case 'votingPowers':
                                                                case 'valid':
                                                                case 'show':
                                                                case 'exception':
                                                                case 'level':
                                                                    options[propName as keyof typeof options] =
                                                                        this.extractExpression(
                                                                            prop.initializer,
                                                                            Native.EntryPointCallOptionTypes[
                                                                                propName as keyof typeof Native.EntryPointCallOptionTypes
                                                                            ],
                                                                        );
                                                                    break;
                                                                case 'sender':
                                                                    const senderValue = this.extractExpression(
                                                                        prop.initializer,
                                                                    );
                                                                    if (
                                                                        guards.expression.isLiteralExpr(senderValue) &&
                                                                        guards.literal.isString(senderValue.literal)
                                                                    ) {
                                                                        options.sender =
                                                                            ExpressionBuilder.addressLiteral(
                                                                                senderValue.literal.value,
                                                                                line,
                                                                            );
                                                                    } else if (
                                                                        senderValue.kind ===
                                                                        ST_ExpressionKind.ScenarioTestAccountAccess
                                                                    ) {
                                                                        options.sender = senderValue.account;
                                                                    } else if (
                                                                        senderValue.kind ===
                                                                        ST_ExpressionKind.ContractAddressAccess
                                                                    ) {
                                                                        options.sender = senderValue;
                                                                    } else {
                                                                        return this.output.emitError(
                                                                            prop,
                                                                            `Property (${propName}) type is incorrect (It must be a string literal, an implicit account object or an address).`,
                                                                        );
                                                                    }
                                                                    break;
                                                                case 'source':
                                                                    const sourceValue = this.extractExpression(
                                                                        prop.initializer,
                                                                    );
                                                                    if (
                                                                        guards.expression.isLiteralExpr(sourceValue) &&
                                                                        guards.literal.isString(sourceValue.literal)
                                                                    ) {
                                                                        options.source =
                                                                            ExpressionBuilder.addressLiteral(
                                                                                sourceValue.literal.value,
                                                                                line,
                                                                            );
                                                                    } else if (
                                                                        sourceValue.kind ===
                                                                            ST_ExpressionKind.ScenarioTestAccount ||
                                                                        sourceValue.kind ===
                                                                            ST_ExpressionKind.ScenarioTestAccountAccess
                                                                    ) {
                                                                        options.source = sourceValue;
                                                                    } else if (
                                                                        sourceValue.kind ===
                                                                        ST_ExpressionKind.ContractAddressAccess
                                                                    ) {
                                                                        options.sender = sourceValue;
                                                                    } else {
                                                                        return this.output.emitError(
                                                                            prop,
                                                                            `Property (${propName}) type is incorrect (It must be an address).`,
                                                                        );
                                                                    }
                                                                    break;
                                                                default:
                                                                    this.output.emitWarning(
                                                                        prop,
                                                                        `Unknown property (${propName}).`,
                                                                    );
                                                            }
                                                        } else {
                                                            this.output.emitError(
                                                                prop,
                                                                `Expected a property assignment but got (${
                                                                    SyntaxKind[prop.kind]
                                                                }).`,
                                                            );
                                                        }
                                                    });
                                                }
                                            }

                                            const numberArgs = Object.keys(entrypointCallExpr.arguments).length;
                                            const action: ST_ScenarioAction = {
                                                action: ST_ScenarioActionKind.MESSAGE,
                                                line_no: LineUtils.getLineNumber(line),
                                                message: entrypointCallExpr.epName,
                                                id: staticId(
                                                    entrypointCallExpr.contract.id,
                                                    LineUtils.getLineNumber(entrypointCallExpr.line),
                                                ),
                                                params:
                                                    numberArgs > 1
                                                        ? this.translators.Expression.translateExpression({
                                                              kind: ST_ExpressionKind.ObjectLiteralExpression,
                                                              properties: entrypointCallExpr.arguments,
                                                              line,
                                                              type: {
                                                                  type: FrontendType.TRecord,
                                                                  properties: Object.entries(
                                                                      entrypointCallExpr.arguments,
                                                                  ).reduce(
                                                                      (acc, [name, { type }]) => ({
                                                                          ...acc,
                                                                          [name]: type,
                                                                      }),
                                                                      {},
                                                                  ),
                                                              },
                                                          })
                                                        : numberArgs === 1
                                                        ? this.translators.Expression.translateExpression(
                                                              Object.values(entrypointCallExpr.arguments)[0],
                                                          )
                                                        : '(unit)',
                                            };

                                            // Add options to `message` scenario action
                                            Object.keys(options).forEach((key) => {
                                                const option = options[key as keyof typeof options];
                                                if (typeof option !== 'undefined') {
                                                    (action as any)[
                                                        Native.EntryPointCallOptionNames[key as keyof typeof options]
                                                    ] = this.translators.Expression.translateExpression(option);
                                                }
                                            });
                                            this.output.emitScenarioAction(action);
                                            return entrypointCallExpr;
                                    }
                                }
                                break;
                            case Native.ST_NamespaceScenarioMethod.verifyEqual: {
                                // Extract call arguments
                                const leftExpr = this.extractExpression(expression.arguments[0]);
                                const rightExpr = this.extractExpression(expression.arguments[1]);
                                const verifyEqualExpr = ScenarioBuilder.verifyEqual(leftExpr, rightExpr, line);
                                this.output.emitScenarioAction({
                                    action: ST_ScenarioActionKind.VERIFY,
                                    condition: this.translators.Expression.translateExpression(
                                        verifyEqualExpr.condition,
                                    ),
                                    line_no: LineUtils.getLineNumber(line),
                                });
                                return verifyEqualExpr;
                            }
                            case Native.ST_NamespaceScenarioMethod.verify: {
                                // Extract condition expression
                                const conditionArg = this.extractExpression(expression.arguments[0]);
                                this.output.emitScenarioAction({
                                    action: ST_ScenarioActionKind.VERIFY,
                                    condition: this.translators.Expression.translateExpression(conditionArg),
                                    line_no: LineUtils.getLineNumber(line),
                                });
                                return {
                                    kind: ST_ExpressionKind.ScenarioVerify,
                                    condition: conditionArg,
                                    line,
                                };
                            }
                        }
                        break;
                }
                if (namespace in Native.ST_Namespace) {
                    return this.output.emitError(expression, `(${namespace}.${name}) is not implemented.`);
                }
            }
            const expr = this.extractExpression(expression.expression.expression);

            switch (expr.kind) {
                case ST_ExpressionKind.ContractOrigination:
                    const ep = Object.values(expr.contract.entry_points).find(
                        ({ functionRef }) => functionRef === name,
                    );
                    if (ep) {
                        const entrypointCallArgs = this.extractArgExpressions(
                            expression,
                            expr.contract.scope.functions[ep.functionRef].type.inputTypes,
                        );
                        return {
                            kind: ST_ExpressionKind.EntryPointCall,
                            arguments: entrypointCallArgs,
                            epName: ep.name,
                            contract: expr.contract,
                            line,
                        };
                    }

                    return this.output.emitError(
                        expression,
                        `Contract (${expr.contract.name}) cannot access property (${name}).`,
                    );
                case ST_ExpressionKind.StorageAccessExpr:
                    if (
                        guards.expression.hasType(expr) &&
                        expr.type.type === FrontendType.TRecord &&
                        expr.type.properties[name].type === FrontendType.TLambda
                    ) {
                        return {
                            kind: ST_ExpressionKind.LambdaCallExpr,
                            arguments: this.extractExpression(expression.arguments[0]), // Only 1 argument
                            lambda: {
                                kind: ST_ExpressionKind.AttrAccessExpr,
                                attr: name,
                                type: expr.type.properties[name],
                                prev: expr,
                                line,
                            },
                            type: expr.type.properties[name],
                            line,
                        };
                    }
                    if (guards.expression.hasType(expr) && expr.type.type === FrontendType.TLambda) {
                        return {
                            kind: ST_ExpressionKind.LambdaCallExpr,
                            arguments: this.extractExpression(expression.arguments[0]), // Only 1 argument
                            lambda: {
                                kind: ST_ExpressionKind.AttrAccessExpr,
                                type: expr.type,
                                attr: name,
                                prev: expr,
                                line,
                            },
                            type: expr.type,
                            line,
                        };
                    }
            }

            // Resolve function declaration (arguments types are necessary)
            const type = this.interpreters.Type.resolveTypeOfAccessExpression(expr);
            if (
                name in Native.PropertyMethod // @TODO &&
                //Native.MethodsByType[type.type].includes(name as Native.PropertyMethod)
            ) {
                const line = LineUtils.getLineAndCharacter(this.sourceFile, expression);
                const kind = Native.ExpressionByMethod[name as Native.PropertyMethod];
                switch (kind) {
                    case ST_ExpressionKind.IsVariant:
                        switch (name) {
                            case Native.PropertyMethod.isSome:
                                return {
                                    kind,
                                    source: expr,
                                    subject: 'Some',
                                    line,
                                };
                            case Native.PropertyMethod.isVariant:
                                if (isStringLiteral(expression.arguments[0])) {
                                    return {
                                        kind,
                                        source: expr,
                                        subject: expression.arguments[0].text,
                                        line,
                                    };
                                }
                        }
                        break;
                    case ST_ExpressionKind.OpenVariant:
                        let message: ST_Expression | undefined;
                        switch (name) {
                            case Native.PropertyMethod.openSome:
                                if (expression.arguments?.[0]) {
                                    message = this.extractExpression(expression.arguments[0]);
                                }
                                return {
                                    kind,
                                    source: expr,
                                    subject: 'Some',
                                    message,
                                    line,
                                };
                            case Native.PropertyMethod.openVariant:
                                if (expression.arguments?.[1]) {
                                    message = this.extractExpression(expression.arguments[0]);
                                }
                                if (isStringLiteral(expression.arguments[0])) {
                                    return {
                                        kind,
                                        source: expr,
                                        subject: expression.arguments[0].text,
                                        message,
                                        line,
                                    };
                                }
                        }
                        break;
                    case ST_ExpressionKind.AddSeconds:
                        const input = this.extractExpression(expression.arguments[0]);
                        switch (name) {
                            case Native.PropertyMethod.addSeconds:
                                return {
                                    kind,
                                    target: expr,
                                    expression: input,
                                    type: TypeBuilder.timestamp(),
                                    line,
                                };
                            case Native.PropertyMethod.addMinutes:
                                return {
                                    kind,
                                    target: expr,
                                    expression: {
                                        kind: ST_ExpressionKind.BinaryExpr,
                                        left: input,
                                        right: ExpressionBuilder.intLiteral('60', line),
                                        operator: ST_BinaryToken.Mul,
                                        line,
                                    },
                                    type: TypeBuilder.timestamp(),
                                    line,
                                };
                            case Native.PropertyMethod.addHours:
                                return {
                                    kind,
                                    target: expr,
                                    expression: {
                                        kind: ST_ExpressionKind.BinaryExpr,
                                        left: input,
                                        right: ExpressionBuilder.intLiteral('3600', line),
                                        operator: ST_BinaryToken.Mul,
                                        line,
                                    },
                                    type: TypeBuilder.timestamp(),
                                    line,
                                };
                            case Native.PropertyMethod.addDays:
                                return {
                                    kind,
                                    target: expr,
                                    expression: {
                                        kind: ST_ExpressionKind.BinaryExpr,
                                        left: input,
                                        right: ExpressionBuilder.intLiteral('86400', line),
                                        operator: ST_BinaryToken.Mul,
                                        line,
                                    },
                                    type: TypeBuilder.timestamp(),
                                    line,
                                };
                        }

                    case ST_ExpressionKind.Add:
                        if (type.type === FrontendType.TSet) {
                            return {
                                kind: ST_ExpressionKind.UpdateSet,
                                target: expr,
                                expression: this.extractExpression(expression.arguments[0]),
                                add: true,
                                line,
                            };
                        }
                        break;
                    case ST_ExpressionKind.Remove:
                        switch (type.type) {
                            case FrontendType.TSet:
                                return {
                                    kind: ST_ExpressionKind.UpdateSet,
                                    target: expr,
                                    expression: this.extractExpression(expression.arguments[0]),
                                    add: false,
                                    line,
                                };
                            case FrontendType.TBig_map:
                            case FrontendType.TMap:
                                return {
                                    kind: ST_ExpressionKind.DelItem,
                                    source: expr,
                                    subject: this.extractExpression(expression.arguments[0]),
                                    line,
                                };
                        }
                        break;
                    case ST_ExpressionKind.Slice:
                        return {
                            kind,
                            expression: expr,
                            offset: this.extractExpression(expression.arguments[0]),
                            length: this.extractExpression(expression.arguments[1]),
                            line,
                        };
                    case ST_ExpressionKind.BinaryExpr:
                        switch (name) {
                            case Native.PropertyMethod.plus:
                                return {
                                    kind,
                                    left: expr,
                                    right: this.extractExpression(expression.arguments[0]),
                                    operator: ST_BinaryToken.Plus,
                                    line,
                                };
                            case Native.PropertyMethod.multiply:
                                return {
                                    kind,
                                    left: expr,
                                    right: this.extractExpression(expression.arguments[0]),
                                    operator: ST_BinaryToken.MulOverloaded,
                                    line,
                                };
                            case Native.PropertyMethod.concat:
                                return {
                                    kind,
                                    left: expr,
                                    right: this.extractExpression(expression.arguments[0]),
                                    operator: ST_BinaryToken.Plus,
                                    line,
                                };
                            case Native.PropertyMethod.minus:
                                return {
                                    kind,
                                    left: expr,
                                    right: this.extractExpression(expression.arguments[0]),
                                    operator: ST_BinaryToken.Minus,
                                    line,
                                };
                            default:
                                return this.output.emitError(
                                    expression,
                                    `Type ${type.type} doesn't have method (${name}).`,
                                );
                        }
                        break;
                    case ST_ExpressionKind.GetItem: {
                        let inputType: ST_TypeDef = { type: FrontendType.TUnknown };
                        let returnType: ST_TypeDef = { type: FrontendType.TUnknown };
                        switch (type.type) {
                            case FrontendType.TBig_map:
                            case FrontendType.TMap:
                                inputType = type.keyType;
                                returnType = type.valueType;
                        }
                        return {
                            kind,
                            subject: this.extractExpression(expression.arguments[0], inputType),
                            source: expr,
                            defaultValue:
                                expression.arguments.length > 1
                                    ? this.extractExpression(expression.arguments[1])
                                    : undefined,
                            type: returnType,
                            line,
                        };
                    }
                    case ST_ExpressionKind.Contains:
                    case ST_ExpressionKind.DelItem:
                    case ST_ExpressionKind.Push:
                        let subjectType = type;
                        switch (subjectType.type) {
                            case FrontendType.TBig_map:
                            case FrontendType.TMap:
                                subjectType = subjectType.keyType;
                                break;
                            case FrontendType.TList:
                            case FrontendType.TSet:
                                subjectType = subjectType.innerTypes[0];
                        }
                        return {
                            kind,
                            subject: this.extractExpression(expression.arguments[0], subjectType),
                            source: expr,
                            line,
                        };
                    case ST_ExpressionKind.SetItem:
                        return {
                            kind,
                            target: {
                                kind: ST_ExpressionKind.GetItem,
                                subject: this.extractExpression(expression.arguments[0]),
                                source: expr,
                                type: { type: FrontendType.TUnknown },
                                line,
                            },
                            value: this.extractExpression(expression.arguments[1]),
                            line,
                        };
                    case ST_ExpressionKind.Size:
                        return {
                            kind,
                            expression: expr,
                            line,
                        };
                    case ST_ExpressionKind.AsExpression: {
                        const t = type || this.interpreters.Type.checkType(expression);
                        return {
                            kind,
                            expression: this.extractExpression(expression.arguments[0]),
                            type: t,
                            line,
                        };
                    }
                    case ST_ExpressionKind.GetElements:
                    case ST_ExpressionKind.GetKeys:
                    case ST_ExpressionKind.GetValues:
                    case ST_ExpressionKind.GetEntries:
                        return {
                            kind,
                            expression: expr,
                            type: TypeBuilder.list([TypeBuilder.unknown()]),
                            line,
                        };
                    case ST_ExpressionKind.Reverse:
                    case ST_ExpressionKind.ToInt:
                    case ST_ExpressionKind.ToNat:
                    case ST_ExpressionKind.Negate:
                    case ST_ExpressionKind.PairCAR:
                    case ST_ExpressionKind.PairCDR: {
                        return {
                            kind,
                            expression: expr,
                            line,
                        };
                    }
                }
            }
        }
        if (isIdentifier(expression.expression)) {
            const expr = this.lookupIdentifierInScopes(name, line);
            if (expr?.kind === ST_ExpressionKind.LambdaExpression) {
                return {
                    kind: ST_ExpressionKind.LambdaCallExpr,
                    arguments: this.extractExpression(expression.arguments[0]), // Only 1 argument
                    lambda: expr,
                    type: expr.lambda.type,
                    line,
                };
            }
            if (expr?.kind === ST_ExpressionKind.MethodPropAccessExpr) {
                return {
                    kind: ST_ExpressionKind.LambdaCallExpr,
                    arguments: this.extractExpression(expression.arguments[0]), // Only 1 argument
                    lambda: expr,
                    type: expr.property?.type || { type: FrontendType.TUnknown },
                    line,
                };
            }
            if (expr?.kind === ST_ExpressionKind.MethodParamAccessExpr) {
                return {
                    kind: ST_ExpressionKind.LambdaCallExpr,
                    arguments: this.extractExpression(expression.arguments[0]), // Only 1 argument
                    lambda: expr,
                    type: expr.type,
                    line,
                };
            }
        }
        return this.output.emitError(expression, `Cannot resolve call expression (${name}).`);
    };

    extractAccessExpression = (expression: Expression, type?: ST_TypeDef): ST_Expression => {
        const line = LineUtils.getLineAndCharacter(this.sourceFile, expression);
        const name = this.transpiler.extractName(expression);

        if (isIdentifier(expression)) {
            const expr = this.lookupIdentifierInScopes(name, line);
            if (expr) {
                return expr;
            }
            return this.output.emitError(expression, `Cannot locate property with name (${name}).`);
        }

        if (isPropertyAccessExpression(expression)) {
            if (expression.expression.kind === SyntaxKind.ThisKeyword && name === 'storage') {
                // Storage access
                const storage = this.output.currentClass.scope.properties['storage'];
                return {
                    kind: ST_ExpressionKind.StorageAccessExpr,
                    type: storage.type,
                    line,
                };
            }

            if (isIdentifier(expression.expression)) {
                const namespace = this.transpiler.extractName(expression.expression);
                if (namespace in Native.ST_Namespace) {
                    if (name in Native.ST_SpValue) {
                        // Native property
                        return {
                            kind: ST_ExpressionKind.NativePropAccessExpr,
                            prop: Native.ST_KeywordSpValue[name as Native.ST_SpValue],
                            type: Native.TypeOfNativeValue.Sp[name as Native.ST_SpValue],
                            line,
                        };
                    }
                }
            }

            const propName = this.transpiler.extractName(expression.expression);
            const expr = this.extractAccessExpression(expression.expression);

            // Variant access
            if (
                this.output.switchCase?.variant === propName &&
                name === 'value' &&
                guards.expression.hasType(expr) &&
                expr.type.type === FrontendType.TVariant
            ) {
                return {
                    kind: ST_ExpressionKind.VariantAccess,
                    name: this.output.switchCase.accessExpr.name,
                    line,
                };
            }

            if (expr.kind === ST_ExpressionKind.ObjectLiteralExpression) {
                return expr.properties[name];
            }

            if (expr.kind === ST_ExpressionKind.ContractOrigination) {
                switch (name) {
                    case Native.ST_ContractValue.storage:
                        return {
                            kind: ST_ExpressionKind.ContractStorageAccess,
                            contract: expr.contract,
                            line,
                        };
                    case Native.ST_ContractValue.address:
                        return {
                            kind: ST_ExpressionKind.ContractAddressAccess,
                            contract: expr.contract,
                            line,
                        };
                    case Native.ST_ContractValue.baker:
                        return {
                            kind: ST_ExpressionKind.ContractBakerAccess,
                            contract: expr.contract,
                            line,
                        };
                    case Native.ST_ContractValue.balance:
                        return {
                            kind: ST_ExpressionKind.ContractBalanceAccess,
                            contract: expr.contract,
                            line,
                        };
                    case Native.ST_ContractValue.typed:
                        return {
                            kind: ST_ExpressionKind.ContractTypedAccess,
                            contract: expr.contract,
                            epName: '',
                            line,
                        };
                }

                return this.output.emitError(
                    expression,
                    `Contract (${expr.contract.name}) cannot access property (${name}).`,
                );
            }

            // Update test account access expression (from `acc` to `acc.<prop>`)
            if (expr.kind === ST_ExpressionKind.ScenarioTestAccountAccess) {
                return {
                    ...expr,
                    prop: name,
                    line,
                };
            }

            // Update contract entrypoint access expression (in tests), from `contract.typed` to `contract.typed.<entry_point>`.
            if (expr.kind === ST_ExpressionKind.ContractTypedAccess) {
                return {
                    ...expr,
                    epName: name,
                };
            }

            return {
                kind: ST_ExpressionKind.AttrAccessExpr,
                attr: name,
                type: {
                    type: FrontendType.TUnknown,
                },
                prev: expr,
                line,
            };
        }

        if (isCallExpression(expression)) {
            return this.extractCallExpression(expression, type);
        }

        return this.output.emitError(expression, `Cannot resolve access expression (${SyntaxKind[expression.kind]}).`);
    };

    extractBinaryExpression = (expression: BinaryExpression): ST_ExpressionStatement => {
        const operator = this.extractBinaryOperator(expression.operatorToken);
        const leftExpr = this.extractExpression(expression.left);
        const rightExpr = this.extractExpression(expression.right);

        switch (operator) {
            case ST_BinaryToken.Equals:
                if (
                    leftExpr.kind === ST_ExpressionKind.MethodPropAccessExpr &&
                    leftExpr.property &&
                    leftExpr.property.flags === NodeFlags.Const
                ) {
                    return this.output.emitError(
                        expression,
                        `Cannot assign to '${leftExpr.attr}' because it is a constant.`,
                    );
                }
            case ST_BinaryToken.Plus:
            case ST_BinaryToken.Minus:
            case ST_BinaryToken.Mul:
            case ST_BinaryToken.Div:
            case ST_BinaryToken.Mod:
            case ST_BinaryToken.EqualsEquals:
            case ST_BinaryToken.EqualsEqualsEquals:
            case ST_BinaryToken.ExclamationEquals:
            case ST_BinaryToken.ExclamationEqualsEquals:
            case ST_BinaryToken.LessThan:
            case ST_BinaryToken.GreaterThan:
            case ST_BinaryToken.LessThanEquals:
            case ST_BinaryToken.GreaterThanEquals:
            case ST_BinaryToken.And:
            case ST_BinaryToken.Or:
                return {
                    kind: ST_StatementKind.Expression,
                    expression: {
                        kind: ST_ExpressionKind.BinaryExpr,
                        left: leftExpr,
                        operator: operator,
                        right: rightExpr,
                        line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                    },
                    line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                };
            case ST_BinaryToken.PlusEquals:
            case ST_BinaryToken.MinusEquals:
            case ST_BinaryToken.MulEquals:
            case ST_BinaryToken.DivEquals:
                if (
                    leftExpr.kind === ST_ExpressionKind.MethodPropAccessExpr &&
                    leftExpr.property &&
                    leftExpr.property.flags === NodeFlags.Const
                ) {
                    return this.output.emitError(
                        expression,
                        `Cannot assign to '${leftExpr.attr}' because it is a constant.`,
                    );
                }
                const map = {
                    [ST_BinaryToken.PlusEquals]: ST_BinaryToken.Plus,
                    [ST_BinaryToken.MinusEquals]: ST_BinaryToken.Minus,
                    [ST_BinaryToken.MulEquals]: ST_BinaryToken.Mul,
                    [ST_BinaryToken.DivEquals]: ST_BinaryToken.Div,
                };
                return {
                    kind: ST_StatementKind.Expression,
                    expression: {
                        kind: ST_ExpressionKind.BinaryExpr,
                        left: leftExpr,
                        operator: ST_BinaryToken.Equals,
                        right: {
                            kind: ST_ExpressionKind.BinaryExpr,
                            left: leftExpr,
                            operator: map[operator],
                            right: rightExpr,
                            line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                        },
                        line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                    },
                    line: LineUtils.getLineAndCharacter(this.sourceFile, expression),
                };
        }
        return this.output.emitError(
            expression,
            `Unexpected operator (${StringOfBinaryToken[operator]}) of (${SyntaxKind[expression.kind]}).`,
        );
    };

    extractEnumDeclaration = (node: EnumDeclaration): ST_VariableStatement => {
        const type = this.interpreters.Type.checkType(node) as Extract<ST_TypeDef, { kind: FrontendType.TRecord }>;
        return {
            kind: ST_StatementKind.VariableStatement,
            local: !!this.output.declaringMethod,
            decorators: {},
            type,
            name: this.transpiler.extractName(node),
            modifiers: ModifiersUtils.extractModifiers(node),
            flags: NodeFlags.Const,
            expression: this.extractEnumExpression(node, type),
            line: LineUtils.getLineAndCharacter(this.sourceFile, node),
        };
    };

    extractVariableInitializer = (
        node: VariableDeclaration | PropertyDeclaration,
        flags: NodeFlags,
    ): ST_VariableStatement => {
        const name = this.transpiler.extractName(node);
        const type = this.interpreters.Type.checkType(node);
        if (node.initializer) {
            let expr: ST_Expression | undefined;
            // If the initializer is an identifier, it means that the value comes from another property.
            if (isIdentifier(node.initializer)) {
                if (this.output.declaringMethod) {
                    expr = this.extractAccessExpression(node.initializer);
                } else {
                    expr = this.dereferencePropertyDeclaration(name, node.initializer).expression;
                }
            } else {
                expr = this.extractExpression(node.initializer, type);
            }
            const statement: ST_VariableStatement = {
                kind: ST_StatementKind.VariableStatement,
                local: !!this.output.declaringMethod,
                decorators: this.interpreters.Decorator.extractPropertyDecorators(node),
                name,
                type,
                modifiers: ModifiersUtils.extractModifiers(node),
                flags,
                expression: expr,
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            };

            return statement;
        }
        return this.output.emitError(node, `Variable declaration must be initialized.`);
    };

    /**
     * @description Deference type from scope
     * @param {string} propName Property name
     * @param {Node} Identifier Property reference
     * @returns {ST_VariableStatement} Property value
     */
    dereferencePropertyDeclaration = (propName: string, node: Identifier): ST_VariableStatement => {
        const referenceName = this.transpiler.extractName(node);
        const property = this.interpreters.Declaration.dereferenceProperty(referenceName);
        if (!property) {
            return this.output.emitError(node, `Cannot find any reference to property (${referenceName}).`);
        }
        return {
            ...property,
            name: propName,
        };
    };

    extractArgExpressions = (expr: CallExpression, inputTypes: ST_TypeDefs): Record<string, ST_TypedExpression> => {
        const args = expr.arguments;
        if (args.length !== Object.keys(inputTypes).length) {
            return this.output.emitError(expr, `Call expression doesn't match with method signature.`);
        }
        // Validate call arguments against the method signature
        return Object.entries(inputTypes).reduce((prev, [key, type], index) => {
            const argExpr = this.extractExpression(args[index], type);
            if (guards.expression.hasType(argExpr)) {
                if (!CheckerUtils.checkTypes(argExpr.type, type) /* will be falsy if types don't match */) {
                    return this.output.emitError(
                        args[index],
                        `Argument (${key}) is expected to be of type (${PrinterUtils.type.toString(
                            type,
                        )}) but got (${PrinterUtils.type.toString(argExpr.type)}).`,
                    );
                }
            }
            return {
                ...prev,
                [key]: argExpr,
            };
        }, {});
    };

    resolveImpureLambdaStatements = (
        lambdaExpression: Extract<ST_Expression, { kind: ST_ExpressionKind.LambdaExpression }>,
        args: NodeArray<Expression>,
        line: FileLineInfo,
    ): ST_Expression => {
        const param = this.extractExpression(args[0]);
        const storage = this.output.currentClass.scope.properties['storage'];
        const localName = this.output.scope.generateVarID();
        // Emit local variable declaration
        this.output.scope.emitStatement({
            kind: ST_StatementKind.VariableStatement,
            decorators: {},
            flags: NodeFlags.Const,
            modifiers: [],
            name: localName,
            type: { type: FrontendType.TUnknown },
            local: true,
            expression: {
                kind: ST_ExpressionKind.LambdaCallExpr,
                arguments: {
                    kind: ST_ExpressionKind.ObjectLiteralExpression,
                    properties: {
                        in_param: param,
                        in_storage: {
                            kind: ST_ExpressionKind.StorageAccessExpr,
                            type: storage.type,
                            line,
                        },
                    },
                    type: {
                        type: FrontendType.TRecord,
                        properties: {
                            in_param: {
                                type: FrontendType.TUnknown,
                            },
                            in_storage: storage.type,
                        },
                    },
                    line,
                },
                lambda: {
                    kind: ST_ExpressionKind.GlobalLambdaAccessExpr,
                    name: lambdaExpression.lambda.name,
                    line,
                },
                type: lambdaExpression.lambda.type,
                line,
            },
            line,
        });
        // Update the storage state with the result from the lambda expression
        this.output.scope.emitStatement({
            kind: ST_StatementKind.Expression,
            expression: {
                kind: ST_ExpressionKind.BinaryExpr,
                left: {
                    kind: ST_ExpressionKind.StorageAccessExpr,
                    type: storage.type,
                    line,
                },
                operator: ST_BinaryToken.Equals,
                right: {
                    kind: ST_ExpressionKind.AttrAccessExpr,
                    attr: 'storage',
                    type: storage.type,
                    prev: {
                        kind: ST_ExpressionKind.MethodPropAccessExpr,
                        attr: localName,
                        type: {
                            type: FrontendType.TRecord,
                            properties: {
                                storage: storage.type,
                            },
                        },
                        line,
                    },
                    line,
                },
                line,
            },
            line,
        });
        // Include new operations
        this.output.scope.emitStatement({
            kind: ST_StatementKind.ForOfStatement,
            iterator: {
                kind: ST_ExpressionKind.ForIterator,
                name: 'op',
                type: {
                    type: FrontendType.TOperation,
                },
                line,
            },
            expression: {
                kind: ST_ExpressionKind.Reverse,
                expression: {
                    kind: ST_ExpressionKind.AttrAccessExpr,
                    attr: 'operations',
                    type: {
                        type: FrontendType.TList,
                        innerTypes: [{ type: FrontendType.TOperation }],
                    },
                    prev: {
                        kind: ST_ExpressionKind.MethodPropAccessExpr,
                        attr: localName,
                        type: {
                            type: FrontendType.TRecord,
                            properties: {
                                operations: {
                                    type: FrontendType.TList,
                                    innerTypes: [{ type: FrontendType.TOperation }],
                                },
                            },
                        },
                        line,
                    },
                    line,
                },
                line,
            },
            statements: [
                {
                    kind: ST_StatementKind.Expression,
                    expression: {
                        kind: ST_ExpressionKind.Push,
                        source: {
                            kind: ST_ExpressionKind.OperationsAccessExpr,
                            line,
                        },
                        subject: {
                            kind: ST_ExpressionKind.ForIterator,
                            name: 'op',
                            type: {
                                type: FrontendType.TOperation,
                            },
                            line,
                        },
                        line,
                    },
                    line,
                },
            ],
            line,
        });

        return {
            kind: ST_ExpressionKind.AttrAccessExpr,
            attr: 'result',
            type: { type: FrontendType.TUnknown },
            prev: {
                kind: ST_ExpressionKind.MethodPropAccessExpr,
                attr: localName,
                type: { type: FrontendType.TUnknown },
                line,
            },
            line,
        };
    };
}
