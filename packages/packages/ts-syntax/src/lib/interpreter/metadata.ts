import {
    isArrayLiteralExpression,
    isPropertyAccessExpression,
    isPropertyAssignment,
    ObjectLiteralElementLike,
    ObjectLiteralExpression,
    SyntaxKind,
} from 'typescript';
import type { ST_Expression } from '../../@types/expression';
import { ST_ExpressionKind } from '../../enums/expression';
import { ST_LiteralKind } from '../../enums/literal';
import { FrontendType } from '../../enums/type';
import LineUtils from '../utils/Line';
import { InterpreterBase } from './Base';

export default class MetadataInterpreter extends InterpreterBase {
    parseObjectLiteralExpression = (expr: ObjectLiteralExpression): Record<string, ST_Expression> => {
        return expr.properties.reduce((prev, prop) => {
            const propName = this.transpiler.extractName(prop);
            return {
                ...prev,
                [propName]: this.parseElement(prop, propName),
            };
        }, {});
    };

    parseElement = (el: ObjectLiteralElementLike, name: string): ST_Expression => {
        if (isPropertyAssignment(el)) {
            const initializer = el.initializer;
            if (isArrayLiteralExpression(initializer)) {
                if (name === 'views') {
                    // Validations
                    if (
                        initializer.elements.some(
                            (el) => !isPropertyAccessExpression(el) || el.expression.kind !== SyntaxKind.ThisKeyword,
                        )
                    ) {
                        return this.output.emitError(el, 'Invalid metadata view.');
                    }

                    return {
                        kind: ST_ExpressionKind.ArrayLiteralExpression,
                        elements: initializer.elements.map((e): ST_Expression => {
                            const viewName = this.transpiler.extractNamespace(e).reverse()[0];
                            const view = this.output.currentClass.off_chain_view[viewName];
                            const line = LineUtils.getLineAndCharacter(this.sourceFile, e);
                            return {
                                kind: ST_ExpressionKind.LiteralExpr,
                                type: {
                                    type: FrontendType.TString,
                                },
                                literal: {
                                    kind: ST_LiteralKind.String,
                                    value: view.name,
                                    line,
                                },
                                line,
                            };
                        }),
                        type: {
                            type: FrontendType.TList,
                            innerTypes: [{ type: FrontendType.TString }],
                        },
                        line: LineUtils.getLineAndCharacter(this.sourceFile, el),
                    };
                }
            }
            return this.interpreters.Statement.extractExpression(initializer);
        }

        return this.output.emitError(el, `Invalid field ${name} in metadata.`);
    };
}
