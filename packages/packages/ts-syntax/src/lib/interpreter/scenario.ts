import {
    ConciseBody,
    Expression,
    isBlock,
    isCallExpression,
    isIdentifier,
    isPropertyAccessExpression,
    isStringLiteral,
    SyntaxKind,
    isNewExpression,
    isObjectLiteralExpression,
    isPropertyAssignment,
    CallExpression,
    isArrowFunction,
    isArrayLiteralExpression,
} from 'typescript';
import { InterpreterBase } from './Base';

import * as Native from '../../enums/native';
import LineUtils from '../utils/Line';
import { ST_ScenarioActionKind, ST_ScenarioKind } from '../../enums/scenario';
import guards from '../utils/guard';
import { ST_ExpressionKind } from '../../enums/expression';
import type { ST_Flag } from '../../@types/output';
import type { ST_ScenarioExpression } from '../../@types/expression';
import { ST_ScopeKind } from '../../enums/scope';

export default class TestInterpreter extends InterpreterBase {
    visitTestBody = (body: ConciseBody): void => {
        if (isBlock(body)) {
            this.interpreters.Statement.extractStatements(Array.from(body.statements));
        } else if (isCallExpression(body)) {
            this.interpreters.Statement.extractExpression(body);
        } else {
            return this.output.emitError(body, `Invalid test block.`);
        }
    };

    /**
     * Resolve global call expression
     * @param {CallExpression} callExpr Call Expression
     */
    resolveDevExpression = (callExpr: CallExpression): void => {
        const line = LineUtils.getLineAndCharacter(this.sourceFile, callExpr);
        const name = this.transpiler.extractName(callExpr);

        const resolveExpression = (expr: Expression): ST_ScenarioExpression => {
            if (isIdentifier(expr)) {
                const namespace = this.transpiler.extractName(expr);
                if (namespace in Native.ST_Namespace) {
                    // Native module
                    return {
                        kind: ST_ExpressionKind.NativeModuleAccessExpr,
                        module: namespace as Native.ST_Namespace,
                        line,
                    };
                }
            }
            return this.output.emitError(callExpr, `Expression (${expr.kind}) could not be resolved.`);
        };

        if (isPropertyAccessExpression(callExpr.expression)) {
            const expression = resolveExpression(callExpr.expression.expression);
            switch (expression.kind) {
                case ST_ExpressionKind.NativeModuleAccessExpr:
                    switch (expression.module) {
                        case Native.ST_Namespace.Dev:
                            switch (name) {
                                case Native.ST_NativeMethod.Dev.compileContract:
                                    {
                                        // Extract compilation name
                                        const nameArg = callExpr.arguments[0];
                                        if (!isStringLiteral(nameArg)) {
                                            return this.output.emitError(
                                                nameArg,
                                                `${name} call expects a string as first argument.`,
                                            );
                                        }
                                        // Extract contract compilation
                                        const contractArg = callExpr.arguments[1];
                                        if (!isNewExpression(contractArg)) {
                                            return this.output.emitError(
                                                contractArg,
                                                `${name} call expects the contract instantiation as second argument.`,
                                            );
                                        }
                                        const compilationName = nameArg.text;
                                        const contractClassName = this.transpiler.extractName(contractArg.expression);
                                        const contractArguments =
                                            contractArg.arguments?.map((arg, index) => {
                                                const argInfo = Object.values(
                                                    this.output.classes[contractClassName].constructorArgs,
                                                ).find((a) => a.index === index);
                                                if (argInfo) {
                                                    return this.interpreters.Declaration.extractContractArgument(
                                                        arg,
                                                        argInfo?.type,
                                                    );
                                                } else {
                                                    return this.output.emitError(
                                                        arg,
                                                        `Cannot find argument in the  constructor.`,
                                                    );
                                                }
                                            }) || [];
                                        if (contractArguments.every(guards.expression.hasType)) {
                                            this.output.emitCompilationTarget(
                                                compilationName,
                                                contractClassName,
                                                contractArguments,
                                            );
                                        } else {
                                            return this.output.emitError(
                                                contractArg,
                                                'Contract arguments are invalid.',
                                            );
                                        }
                                    }
                                    break;
                                case Native.ST_NativeMethod.Dev.test:
                                    {
                                        // Extract test information
                                        const infoArg = callExpr.arguments[0];
                                        if (!isObjectLiteralExpression(infoArg)) {
                                            return this.output.emitError(
                                                infoArg,
                                                `${name} call expects a object literal as first argument.`,
                                            );
                                        }
                                        const testInfo: {
                                            name?: string;
                                            enabled: boolean;
                                            shortName?: string;
                                            profile?: boolean;
                                            flags: ST_Flag[];
                                        } = {
                                            enabled: true,
                                            flags: [],
                                        };
                                        infoArg.properties.forEach((prop): void => {
                                            if (isPropertyAssignment(prop)) {
                                                const propName = this.transpiler.extractName(prop);
                                                switch (propName) {
                                                    case 'name':
                                                        const nameValue = this.interpreters.Statement.extractExpression(
                                                            prop.initializer,
                                                        );
                                                        if (
                                                            guards.expression.isLiteralExpr(nameValue) &&
                                                            guards.literal.isString(nameValue.literal)
                                                        ) {
                                                            testInfo.name = nameValue.literal.value;
                                                        } else {
                                                            return this.output.emitError(
                                                                prop,
                                                                `Property (${propName}) type is incorrect (It must be a string literal).`,
                                                            );
                                                        }
                                                        break;
                                                    case 'shortName':
                                                        const shortNameValue =
                                                            this.interpreters.Statement.extractExpression(
                                                                prop.initializer,
                                                            );
                                                        if (
                                                            guards.expression.isLiteralExpr(shortNameValue) &&
                                                            guards.literal.isString(shortNameValue.literal)
                                                        ) {
                                                            testInfo.shortName = shortNameValue.literal.value;
                                                        } else {
                                                            this.output.emitError(
                                                                prop,
                                                                `Property (${propName}) type is incorrect (It must be a string literal).`,
                                                            );
                                                        }
                                                        break;
                                                    case 'profile':
                                                        const profileValue =
                                                            this.interpreters.Statement.extractExpression(
                                                                prop.initializer,
                                                            );
                                                        if (
                                                            guards.expression.isLiteralExpr(profileValue) &&
                                                            guards.literal.isBoolean(profileValue.literal)
                                                        ) {
                                                            testInfo.profile = profileValue.literal.value;
                                                        } else {
                                                            this.output.emitError(
                                                                prop,
                                                                `Property (${propName}) type is incorrect (It must be a boolean literal).`,
                                                            );
                                                        }
                                                        break;
                                                    case 'enabled':
                                                        const enabledValue =
                                                            this.interpreters.Statement.extractExpression(
                                                                prop.initializer,
                                                            );
                                                        if (
                                                            guards.expression.isLiteralExpr(enabledValue) &&
                                                            guards.literal.isBoolean(enabledValue.literal)
                                                        ) {
                                                            testInfo.enabled = enabledValue.literal.value;
                                                        } else {
                                                            this.output.emitError(
                                                                prop,
                                                                `Property (${propName}) type is incorrect (It must be a boolean literal).`,
                                                            );
                                                        }
                                                        break;
                                                    case 'flags':
                                                        if (isArrayLiteralExpression(prop.initializer)) {
                                                            testInfo.flags = prop.initializer.elements.reduce<
                                                                ST_Flag[]
                                                            >((state, el) => {
                                                                if (
                                                                    isArrayLiteralExpression(el) &&
                                                                    el.elements.every(isStringLiteral)
                                                                ) {
                                                                    const [name, ...rest] = el.elements;
                                                                    state = [
                                                                        ...state,
                                                                        {
                                                                            name: name.text,
                                                                            args: rest.map(({ text }) => text),
                                                                        },
                                                                    ];
                                                                }
                                                                return state;
                                                            }, []);
                                                        } else {
                                                            this.output.emitError(prop, `Expected a list of flags.`);
                                                        }
                                                        break;
                                                    default:
                                                        this.output.emitError(prop, `Unknown property (${propName}).`);
                                                }
                                            } else {
                                                this.output.emitError(
                                                    prop,
                                                    `Expected a property assignment but got (${
                                                        SyntaxKind[prop.kind]
                                                    }).`,
                                                );
                                            }
                                        });
                                        if (!testInfo.name) {
                                            return this.output.emitError(callExpr, `Tests must have a name.`);
                                        }
                                        // Extract contract compilation
                                        const scenarioArg = callExpr.arguments[1];
                                        if (!isArrowFunction(scenarioArg)) {
                                            return this.output.emitError(
                                                scenarioArg,
                                                `${name} call expects an arrow function as second argument.`,
                                            );
                                        }
                                        this.output.emitScenario({
                                            kind: ST_ScenarioKind.Test,
                                            scenario: testInfo.flags.map((flag) => ({
                                                action: ST_ScenarioActionKind.FLAG,
                                                flag: [flag.name, ...flag.args],
                                                line_no: LineUtils.getLineNumber(
                                                    LineUtils.getLineAndCharacter(this.sourceFile, callExpr),
                                                ),
                                            })),
                                            name: testInfo.name,
                                            longname: testInfo.name,
                                            shortname: testInfo.shortName || testInfo.name,
                                            profile: testInfo.profile || false,
                                            show: testInfo.enabled,
                                        });
                                        this.output.enterScope(ST_ScopeKind.Test);
                                        this.visitTestBody(scenarioArg.body);
                                        // Tear down
                                        this.output.exitScope();
                                    }
                                    break;
                                default:
                                    return this.output.emitError(
                                        callExpr,
                                        `Method (${Native.ST_Namespace.Dev}.${name}) is not implemented.`,
                                    );
                            }
                            break;
                        default:
                            return this.output.emitError(
                                callExpr,
                                `Could not resolve namespace (${expression.module}).`,
                            );
                    }
                    break;
                default:
                    return this.output.emitError(
                        callExpr,
                        `Expression (${expression.kind}) not supported in this context.`,
                    );
            }
        } else {
            return this.output.emitError(callExpr, `Call (${name}) is not supported in this context.`);
        }
    };
}
