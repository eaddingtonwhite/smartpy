import type { SourceFile } from 'typescript';
import type OutputContext from '../OutputContext';
import type Transpiler from '../Transpiler';
import type { Interpreters, Translators } from '../Transpiler';
export class InterpreterBase {
    constructor(public transpiler: Transpiler) {}

    public get translators(): Translators {
        return this.transpiler.translators;
    }

    public get interpreters(): Interpreters {
        return this.transpiler.interpreters;
    }

    public get output(): OutputContext {
        return this.transpiler.output;
    }

    public get sourceFile(): SourceFile {
        return this.transpiler.sourceFile;
    }
}
