import { StringLiteral, Node, NumericLiteral, Expression, isStringLiteral, isNumericLiteral } from 'typescript';

import { SyntaxKind } from 'typescript';

import { FrontendType } from '../../enums/type';
import { InterpreterBase } from './Base';
import LineUtils from '../utils/Line';
import { ST_LiteralKind } from '../../enums/literal';
import Printer from '../utils/printer';
import type { ST_Literal } from '../../@types/literal';
import type { ST_TypeDef } from '../../@types/type';

export default class LiteralInterpreter extends InterpreterBase {
    /**
     * @description Extract literal
     * @param {Node} node
     * @returns {ST_Literal} A literal
     */
    extractLiteral = (typeDef: ST_TypeDef, node: Node): ST_Literal => {
        switch (typeDef.type) {
            case FrontendType.TTimestamp: {
                let value;
                if (isStringLiteral(node)) {
                    value = String((new Date((<StringLiteral>node).text).getTime() / 1000).toFixed(0));
                } else if (isNumericLiteral(node)) {
                    // BigInt is necessary to allow users to specify huge numbers
                    value = BigInt(Number((<NumericLiteral>node).text)).toString();
                } else {
                    return this.output.emitError(node, 'Timestamp format is invalid.');
                }

                return {
                    kind: ST_LiteralKind.Numeric,
                    value,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            }
            case FrontendType.TBls12_381_fr: {
                if (isStringLiteral(node)) {
                    return {
                        kind: ST_LiteralKind.String,
                        value: (<StringLiteral>node).text,
                        line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                    };
                } else if (isNumericLiteral(node)) {
                    // BigInt is necessary to allow users to specify huge numbers
                    return {
                        kind: ST_LiteralKind.Numeric,
                        value: BigInt(Number((<NumericLiteral>node).text)).toString(),
                        line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                    };
                } else {
                    return this.output.emitError(node, 'TBls12_381_fr format is invalid.');
                }
            }
            case FrontendType.TNat:
            case FrontendType.TInt:
            case FrontendType.TIntOrNat:
            case FrontendType.TMutez:
                return {
                    kind: ST_LiteralKind.Numeric,
                    // BigInt is necessary to allow users to specify huge numbers
                    value: BigInt(Number((<NumericLiteral>node).text)).toString(),
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            case FrontendType.TBytes:
            case FrontendType.TString:
            case FrontendType.TAddress:
            case FrontendType.TSignature:
            case FrontendType.TChain_id:
            case FrontendType.TKey:
            case FrontendType.TKey_hash:
            case FrontendType.TBls12_381_g1:
            case FrontendType.TBls12_381_g2:
            case FrontendType.TContract:
                return {
                    kind: ST_LiteralKind.String,
                    value: (<StringLiteral>node).text,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            case FrontendType.TBool:
                if (node.kind === SyntaxKind.FalseKeyword || node.kind === SyntaxKind.TrueKeyword) {
                    return {
                        kind: ST_LiteralKind.Boolean,
                        value: node.kind === SyntaxKind.TrueKeyword,
                        line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                    };
                }
        }
        return this.output.emitError(
            node,
            `Unexpected type (${Printer.type.toString(typeDef)}) with syntax kind (${SyntaxKind[node.kind]}).`,
        );
    };

    extractStringLiteral = (expr: Expression): string => {
        const literal = this.interpreters.Literal.extractLiteral(
            {
                type: FrontendType.TString,
            },
            expr,
        );
        if (literal.kind === ST_LiteralKind.String) {
            return literal.value;
        } else {
            return this.output.emitError(
                expr,
                `Expected literal (${ST_LiteralKind.String}), but got (${ST_LiteralKind[literal.kind]}).`,
            );
        }
    };

    extractBoolLiteral = (expr: Expression): boolean => {
        const literal = this.interpreters.Literal.extractLiteral(
            {
                type: FrontendType.TBool,
            },
            expr,
        );
        if (literal.kind === ST_LiteralKind.Boolean) {
            return literal.value;
        } else {
            return this.output.emitError(
                expr,
                `Expected literal (${ST_LiteralKind.Boolean}), but got (${ST_LiteralKind[literal.kind]}).`,
            );
        }
    };

    extractNumericLiteral = (expr: Expression): string => {
        const literal = this.interpreters.Literal.extractLiteral(
            {
                type: FrontendType.TIntOrNat,
            },
            expr,
        );
        if (literal.kind === ST_LiteralKind.Numeric) {
            return literal.value;
        } else {
            return this.output.emitError(
                expr,
                `Expected literal (${ST_LiteralKind.Numeric}), but got (${ST_LiteralKind[literal.kind]}).`,
            );
        }
    };
}
