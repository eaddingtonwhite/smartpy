import { BinaryOperatorToken, SyntaxKind } from 'typescript';
import { ST_BinaryToken } from '../../enums/expression';

export const isAssignmentBinaryOperatorToken = (o: BinaryOperatorToken): boolean =>
    [
        SyntaxKind.PlusEqualsToken,
        SyntaxKind.MinusEqualsToken,
        SyntaxKind.AsteriskEqualsToken,
        SyntaxKind.SlashEqualsToken,
        SyntaxKind.EqualsToken,
    ].includes(o.kind);

export const isAssignmentBinaryOperator = (o: ST_BinaryToken): boolean =>
    [
        ST_BinaryToken.PlusEquals,
        ST_BinaryToken.MinusEquals,
        ST_BinaryToken.MulEquals,
        ST_BinaryToken.DivEquals,
        ST_BinaryToken.Equals,
    ].includes(o);
