import type { Nullable } from '../../@types/common';
import type { ST_TypeDef } from '../../@types/type';
import { FrontendType } from '../../enums/type';
import ErrorUtils from './Error';
import guards from './guard';
import PrinterUtils from './printer';

/**
 * @description Check implicit type against the explicit type.
 * @param {ST_TypeDef} implicit types
 * @param {ST_TypeDef} explicit type
 * @returns {Nullable<ST_TypeDef>} Return the correct type, or null if the check failed.
 */
const checkTypes = (implicit: ST_TypeDef, explicit: ST_TypeDef): Nullable<ST_TypeDef> => {
    if (guards.type.isUnknown(explicit)) {
        return implicit;
    }
    if (guards.type.isUnknown(implicit)) {
        return explicit;
    }

    // Check simple types for (explicit === implicit)
    switch (implicit.type) {
        case FrontendType.TUnit:
        case FrontendType.TString:
        case FrontendType.TAddress:
        case FrontendType.TSignature:
        case FrontendType.TTimestamp:
        case FrontendType.TKey:
        case FrontendType.TKey_hash:
        case FrontendType.TNat:
        case FrontendType.TInt:
        case FrontendType.TMutez:
        case FrontendType.TBytes:
        case FrontendType.TIntOrNat:
        case FrontendType.TBls12_381_fr:
        case FrontendType.TBls12_381_g1:
        case FrontendType.TBls12_381_g2:
        case FrontendType.TBool:
            if (explicit.type === implicit.type) {
                return explicit;
            }
    }

    switch (implicit.type) {
        case FrontendType.TContract:
            switch (explicit.type) {
                case FrontendType.TContract:
                    if (checkTypes(implicit.inputType, explicit.inputType)) {
                        return explicit;
                    }
            }
            break;
        case FrontendType.TVariant: {
            if (explicit.type === FrontendType.TVariant) {
                for (const key in implicit.properties) {
                    if (!explicit.properties[key] && !checkTypes(implicit.properties[key], explicit.properties[key])) {
                        return ErrorUtils.failWithInfo({
                            msg: `Variant property type doesn't match. Expected (${PrinterUtils.type.toString(
                                implicit.properties[key],
                            )}), but received (${PrinterUtils.type.toString(explicit.properties[key])}).`,
                            line: explicit.line!,
                        });
                    }
                }
                return explicit;
            }
        }
        case FrontendType.TRecord:
            switch (explicit.type) {
                case FrontendType.TRecord:
                    for (const key in implicit.properties) {
                        if (
                            !explicit.properties[key] &&
                            !checkTypes(implicit.properties[key], explicit.properties[key])
                        ) {
                            return ErrorUtils.failWithInfo({
                                msg: `Record property type doesn't match. Expected (${PrinterUtils.type.toString(
                                    implicit.properties[key],
                                )}), but received (${PrinterUtils.type.toString(explicit.properties[key])}).`,
                                line: explicit.line!,
                            });
                        }
                    }
                    return explicit;
            }
            break;
        case FrontendType.TMap:
            switch (explicit.type) {
                case FrontendType.TMap:
                case FrontendType.TBig_map:
                    const keyType = checkTypes(implicit.keyType, explicit.keyType);
                    const valueType = checkTypes(implicit.valueType, explicit.valueType);
                    if (keyType && valueType) {
                        return explicit;
                    }
                    break;
            }
            break;
        case FrontendType.TBig_map:
            switch (explicit.type) {
                case FrontendType.TBig_map:
                    const keyType = checkTypes(implicit.keyType, explicit.keyType);
                    const valueType = checkTypes(implicit.valueType, explicit.valueType);
                    if (keyType && valueType) {
                        return explicit;
                    }
                    break;
            }
            break;
        case FrontendType.TOption:
            switch (explicit.type) {
                case FrontendType.TOption:
                    if (checkTypes(implicit.innerType, explicit.innerType)) {
                        return explicit;
                    }
            }
            break;
        case FrontendType.TTuple:
            switch (explicit.type) {
                case FrontendType.TTuple:
                    if (implicit.innerTypes.every((t, i) => checkTypes(t, explicit.innerTypes[i]))) {
                        return explicit;
                    }
            }
            break;
        case FrontendType.TSet:
            switch (explicit.type) {
                case FrontendType.TSet:
                    const innerType = implicit.innerTypes[0];
                    if (explicit.innerTypes.every((t) => checkTypes(innerType, t))) {
                        return explicit;
                    }
            }
            break;
        case FrontendType.TList:
            switch (explicit.type) {
                case FrontendType.TTuple:
                    if (implicit.innerTypes.every((t, i) => checkTypes(t, explicit.innerTypes[i]))) {
                        return explicit;
                    }
                    break;
                case FrontendType.TSet:
                case FrontendType.TList:
                    const innerType = implicit.innerTypes[0];
                    if (explicit.innerTypes.every((t) => checkTypes(innerType, t))) {
                        return explicit;
                    }
                    break;
                case FrontendType.TBig_map:
                case FrontendType.TMap:
                    {
                        const innerType = implicit.innerTypes[0];
                        if (innerType.type === FrontendType.TUnknown) {
                            return explicit;
                        }
                    }
                    break;
            }
            break;
        case FrontendType.TString:
            switch (explicit.type) {
                case FrontendType.TTimestamp:
                case FrontendType.TBytes:
                case FrontendType.TString:
                case FrontendType.TAddress:
                case FrontendType.TKey_hash:
                case FrontendType.TKey:
                case FrontendType.TBls12_381_fr:
                case FrontendType.TBls12_381_g1:
                case FrontendType.TBls12_381_g2:
                    return explicit;
            }
            break;
        case FrontendType.TNat:
        case FrontendType.TInt:
        case FrontendType.TIntOrNat:
            switch (explicit.type) {
                case FrontendType.TNat:
                case FrontendType.TInt:
                case FrontendType.TMutez:
                case FrontendType.TIntOrNat:
                case FrontendType.TBls12_381_fr:
                    return explicit;
            }
            break;
        case FrontendType.TLambda:
            if (explicit.type === FrontendType.TLambda && checkTypes(implicit.outputType, explicit.outputType)) {
                if (
                    Object.keys(implicit.inputTypes).length > 1 &&
                    explicit.inputTypes['input'].type === FrontendType.TRecord &&
                    Object.entries(explicit.inputTypes['input'].properties).every(
                        ([name, t]) => implicit.inputTypes[name] && checkTypes(implicit.inputTypes[name], t),
                    )
                ) {
                    return implicit;
                } else if (
                    Object.keys(implicit.inputTypes).length === 1 &&
                    checkTypes(implicit.inputTypes[Object.keys(implicit.inputTypes)[0]], explicit.inputTypes['input'])
                ) {
                    return implicit;
                }
            }
            break;
    }

    return null;
};

export const CheckerUtils = {
    checkTypes,
};

export default CheckerUtils;
