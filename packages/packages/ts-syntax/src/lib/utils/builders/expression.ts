import type { FileLineInfo } from '../../../@types/common';
import type { ST_Expression } from '../../../@types/expression';
import type { ST_TypeDef } from '../../../@types/type';
import { ST_BinaryToken, ST_ExpressionKind } from '../../../enums/expression';
import { ST_LiteralKind } from '../../../enums/literal';
import { FrontendType } from '../../../enums/type';
import TypeBuilder from './type';

export const mutezLiteral = (v: string | number, line: FileLineInfo): ST_Expression => ({
    kind: ST_ExpressionKind.LiteralExpr,
    literal: {
        kind: ST_LiteralKind.Numeric,
        value: String(v),
        line,
    },
    type: TypeBuilder.mutez(),
    line,
});

export const addressLiteral = (
    address: string,
    line: FileLineInfo,
): Extract<ST_Expression, { kind: ST_ExpressionKind.LiteralExpr }> => ({
    kind: ST_ExpressionKind.LiteralExpr,
    literal: {
        kind: ST_LiteralKind.String,
        value: address,
        line,
    },
    type: TypeBuilder.address(),
    line,
});

export const boolLiteral = (value: boolean, line: FileLineInfo): ST_Expression => ({
    kind: ST_ExpressionKind.LiteralExpr,
    literal: {
        kind: ST_LiteralKind.Boolean,
        value,
        line,
    },
    type: TypeBuilder.bool(),
    line,
});

export const natLiteral = (value: string, line: FileLineInfo): ST_Expression => ({
    kind: ST_ExpressionKind.LiteralExpr,
    literal: {
        kind: ST_LiteralKind.Numeric,
        value,
        line,
    },
    type: TypeBuilder.nat(),
    line,
});

export const intLiteral = (value: string, line: FileLineInfo): ST_Expression => ({
    kind: ST_ExpressionKind.LiteralExpr,
    literal: {
        kind: ST_LiteralKind.Numeric,
        value,
        line,
    },
    type: TypeBuilder.int(),
    line,
});

export const timestampLiteral = (value: string, line: FileLineInfo): ST_Expression => ({
    kind: ST_ExpressionKind.LiteralExpr,
    literal: {
        kind: ST_LiteralKind.String,
        value,
        line,
    },
    type: TypeBuilder.timestamp(),
    line,
});

export const chainIdLiteral = (value: string, line: FileLineInfo): ST_Expression => ({
    kind: ST_ExpressionKind.LiteralExpr,
    literal: {
        kind: ST_LiteralKind.String,
        value,
        line,
    },
    type: TypeBuilder.chainId(),
    line,
});

export const stringLiteral = (value: string, line: FileLineInfo): ST_Expression => ({
    kind: ST_ExpressionKind.LiteralExpr,
    literal: {
        kind: ST_LiteralKind.String,
        value,
        line,
    },
    type: TypeBuilder.string(),
    line,
});

export const some = (expression: ST_Expression, type: ST_TypeDef, line: FileLineInfo): ST_Expression => ({
    kind: ST_ExpressionKind.Some,
    expression,
    type: {
        type: FrontendType.TOption,
        innerType: type,
        none: false,
        line,
    },
    line,
});

export const pack = (expr: ST_Expression): ST_Expression => ({
    kind: ST_ExpressionKind.Pack,
    expression: expr,
    line: expr.line,
});

export const binaryExpression = (
    leftExpr: ST_Expression,
    rightExpr: ST_Expression,
    operator: ST_BinaryToken,
    line: FileLineInfo,
): ST_Expression => ({
    kind: ST_ExpressionKind.BinaryExpr,
    left: leftExpr,
    right: rightExpr,
    operator,
    line,
});

export const attrAccessExpression = (
    attr: string,
    prev: ST_Expression,
    line: FileLineInfo,
    type?: ST_TypeDef,
): ST_Expression => ({
    kind: ST_ExpressionKind.AttrAccessExpr,
    attr,
    prev,
    type: type || TypeBuilder.unknown(),
    line,
});

export const methodPropAccessExpression = (attr: string, line: FileLineInfo, type?: ST_TypeDef): ST_Expression => ({
    kind: ST_ExpressionKind.MethodPropAccessExpr,
    attr,
    type: type || TypeBuilder.unknown(),
    line,
});

const ExpressionBuilder = {
    mutezLiteral,
    intLiteral,
    addressLiteral,
    boolLiteral,
    stringLiteral,
    natLiteral,
    timestampLiteral,
    chainIdLiteral,
    pack,
    some,
    binaryExpression,
    attrAccessExpression,
    methodPropAccessExpression,
};

export default ExpressionBuilder;
