import {
    isCallExpression,
    isCaseClause,
    isClassDeclaration,
    isDecorator,
    isEnumDeclaration,
    isEnumMember,
    isFunctionDeclaration,
    isIdentifier,
    isInterfaceDeclaration,
    isMethodDeclaration,
    isParameter,
    isPropertyAccessExpression,
    isPropertyAssignment,
    isPropertyDeclaration,
    isPropertySignature,
    isShorthandPropertyAssignment,
    isStringLiteral,
    isTypeAliasDeclaration,
    isTypeReferenceNode,
    isVariableDeclaration,
    Node,
} from 'typescript';
import type { Nullable } from '../../@types/common';

/**
 * @description Extract node name.
 * @param {Node} node
 * @returns {Nullable<string>} node name
 */
const extractName = (node: Node): Nullable<string> => {
    if (isStringLiteral(node)) {
        return node.text;
    }
    if (isIdentifier(node)) {
        return node.escapedText.toString();
    }
    if ((isFunctionDeclaration(node) || isMethodDeclaration(node) || isClassDeclaration(node)) && node.name) {
        return extractName(node.name);
    }
    if (isTypeReferenceNode(node)) {
        return extractName(node.typeName);
    }
    if (isDecorator(node) || isCallExpression(node)) {
        return extractName(node.expression);
    }
    if (isCaseClause(node)) {
        return extractName(node.expression);
    }
    if (
        isParameter(node) ||
        isTypeAliasDeclaration(node) ||
        isInterfaceDeclaration(node) ||
        isPropertySignature(node) ||
        isPropertyAssignment(node) ||
        isPropertyDeclaration(node) ||
        isVariableDeclaration(node) ||
        isPropertyAccessExpression(node) ||
        isShorthandPropertyAssignment(node) ||
        isEnumMember(node) ||
        isEnumDeclaration(node)
    ) {
        return extractName(node.name);
    }

    return null;
};

export const IdentifierUtils = {
    extractName,
};

export default IdentifierUtils;
