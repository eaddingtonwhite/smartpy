import { Transpiler } from './lib/Transpiler';
import typings from './ide.generated';
import type { ST_Scenario } from './@types/scenario';
import type { FileInfo } from './@types/common';

// Expose types at the root
export type { ST_Error } from './lib/utils/Error';

export { setFetchFromDisk, setFetchFromURL } from './lib/utils/file/FileUtils';

const SmartTS = {
    typings,
    Transpiler,
    transpile: (sourceCode: FileInfo): Promise<ST_Scenario[]> => {
        return new Transpiler(sourceCode).transpile();
    },
};

export default SmartTS;
