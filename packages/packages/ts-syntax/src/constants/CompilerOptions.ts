import type { CompilerOptions as CompilerOptionsType } from 'typescript';
import { ScriptTarget, ModuleKind } from 'typescript';

export const CompilerOptions: CompilerOptionsType = {
    experimentalDecorators: true,
    lib: ['lib.d.ts'],
    target: ScriptTarget.ES2020,
    module: ModuleKind.ES2020,
};

export default CompilerOptions;
