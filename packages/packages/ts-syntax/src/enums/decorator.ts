export enum ST_DecoratorKind {
    Contract = 'Contract',
    EntryPoint = 'EntryPoint',
    OffChainView = 'OffChainView',
    MetadataBuilder = 'MetadataBuilder',
    GlobalLambda = 'GlobalLambda',
    Inline = 'Inline',
}

export enum ST_ContractDecoratorProp {
    flags = 'flags',
}

export enum ST_EntryPointDecoratorProp {
    name = 'name',
    lazy = 'lazy',
    lazy_no_code = 'lazy_no_code',
    mock = 'mock',
}

export enum ST_OffChainViewDecoratorProp {
    name = 'name',
    pure = 'pure',
    description = 'description',
}

export enum ST_GlobalLambdaDecoratorProp {
    pure = 'pure',
}
