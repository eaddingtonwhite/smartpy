import path from 'path';
import typescript from '@rollup/plugin-typescript';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import pkg from './package.json';
import nodePolyfills from 'rollup-plugin-polyfill-node';

const config = {
    input: 'src/index.ts',
    output: [
        {
            file: pkg.module,
            inlineDynamicImports: true,
            format: 'es',
            banner: `/*! == Version: ${pkg.version} == */`,
            sourcemap: true,
        },
        {
            file: pkg.main,
            inlineDynamicImports: true,
            format: 'cjs',
            exports: 'named',
            banner: `/*! == Version: ${pkg.version} == */`,
            sourcemap: true,
        },
        // {
        //     name: 'SmartTS',
        //     file: pkg.minified,
        //     format: 'iife',
        //     exports: 'default',
        //     compact: true,
        //     banner: `/* === Version: ${pkg.version} === */`,
        // },
    ],
    plugins: [
        commonjs(),
        nodePolyfills(),
        resolve({ browser: true, preferBuiltins: true }),
        typescript({
            tsconfig: path.resolve(__dirname, './tsconfig.build.json'),
        }),
        terser(),
    ],
};

export default config;
