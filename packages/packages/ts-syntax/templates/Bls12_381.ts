interface TStorage {
    g1: TBls12_381_g1;
    g2: TBls12_381_g2;
    fr: TBls12_381_fr;
    mulResult: TOption<TBls12_381_fr>;
    checkResult: TOption<TBool>;
}

@Contract
export class Bls12_381 {
    constructor(public storage: TStorage) {}

    /**
        ADD: Add two curve points or field elements.

        :: bls12_381_g1 : bls12_381_g1 : 'S -> bls12_381_g1 : 'S
        :: bls12_381_g2 : bls12_381_g2 : 'S -> bls12_381_g2 : 'S
        :: bls12_381_fr : bls12_381_fr : 'S -> bls12_381_fr : 'S
    */
    @EntryPoint
    add(g1: TBls12_381_g1, g2: TBls12_381_g2, fr: TBls12_381_fr): void {
        this.storage.g1 = this.storage.g1.plus(g1);
        this.storage.g2 = this.storage.g2.plus(g2);
        this.storage.fr = this.storage.fr.plus(fr);
    }

    /**
        NEG: Negate a curve point or field element.

        :: bls12_381_g1 : 'S -> bls12_381_g1 : 'S
        :: bls12_381_g2 : 'S -> bls12_381_g2 : 'S
        :: bls12_381_fr : 'S -> bls12_381_fr : 'S
    */
    @EntryPoint
    negate(): void {
        this.storage.g1 = this.storage.g1.negate();
        this.storage.g2 = this.storage.g2.negate();
        this.storage.fr = this.storage.fr.negate();
    }

    /**
        INT: Convert a field element to type int. The returned value is always between 0 (inclusive) and the order of Fr (exclusive).

        :: bls12_381_fr : 'S -> int : 'S
    */
    @EntryPoint
    toInt(): void {
        Sp.verify(
            this.storage.fr.toInt() == 30425446867866594294858714277721586621961856045737725883479188213565831504476,
            'Failed to cast field element Fr to Int',
        );
    }

    /**
        MUL: Multiply a curve point or field element by a scalar field element. Fr
        elements can be built from naturals by multiplying by the unit of Fr using PUSH bls12_381_fr 1; MUL. Note
        that the multiplication will be computed using the natural modulo the order
        of Fr.

        :: bls12_381_g1 : bls12_381_fr : 'S -> bls12_381_g1 : 'S
        :: bls12_381_g2 : bls12_381_fr : 'S -> bls12_381_g2 : 'S
        :: bls12_381_fr : bls12_381_fr : 'S -> bls12_381_fr : 'S
        :: nat : bls12_381_fr : 'S -> bls12_381_fr : 'S
        :: int : bls12_381_fr : 'S -> bls12_381_fr : 'S
        :: bls12_381_fr : nat : 'S -> bls12_381_fr : 'S
        :: bls12_381_fr : int : 'S -> bls12_381_fr : 'S
    */
    @EntryPoint
    mul(pair: TTuple<[TBls12_381_fr, TBls12_381_fr]>): void {
        this.storage.mulResult = Sp.some(pair.fst().multiply(pair.snd()));
        this.storage.g1 = this.storage.g1.multiply(this.storage.fr);
        this.storage.g2 = this.storage.g2.multiply(this.storage.fr);
        this.storage.fr = this.storage.fr.multiply(this.storage.fr);
        this.storage.fr = this.storage.fr.multiply(1);
        this.storage.fr = (2 as TInt).multiply(this.storage.fr);
    }

    /**
        PAIRING_CHECK:
        Verify that the product of pairings of the given list of points is equal to 1 in Fq12. Returns true if the list is empty.
        Can be used to verify if two pairings P1 and P2 are equal by verifying P1 * P2^(-1) = 1.

        :: list (pair bls12_381_g1 bls12_381_g2) : 'S -> bool : 'S
    */
    @EntryPoint
    pairing_check(listOfPairs: TList<TTuple<[TBls12_381_g1, TBls12_381_g2]>>): void {
        this.storage.checkResult = Sp.some(Sp.pairingCheck(listOfPairs));
    }
}

Dev.test({ name: 'BLS12-381' }, () => {
    const c1 = Scenario.originate(
        new Bls12_381({
            g1: '0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1',
            g2: '0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801',
            fr: '0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221',
            mulResult: Sp.none,
            checkResult: Sp.none,
        }),
    );

    c1.add(
        '0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1',
        '0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801',
        '0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221',
    );

    c1.negate();
    c1.toInt();

    c1.mul([
        '0xd30edc8fce6c34442d371da0e24fc3fb83ea957cfea9766c62a531dda98e4b52',
        '0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221',
    ]);

    c1.pairing_check([
        [
            '0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1',
            '0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801',
        ],
        [
            '0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1',
            '0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801',
        ],
    ]);
});

Dev.compileContract(
    'bls12_381_compilation',
    new Bls12_381({
        g1: '0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1',
        g2: '0x13e02b6052719f607dacd3a088274f65596bd0d09920b61ab5da61bbdc7f5049334cf11213945d57e5ac7d055d042b7e024aa2b2f08f0a91260805272dc51051c6e47ad4fa403b02b4510b647ae3d1770bac0326a805bbefd48056c8c121bdb80606c4a02ea734cc32acd2b02bc28b99cb3e287e85a763af267492ab572e99ab3f370d275cec1da1aaa9075ff05f79be0ce5d527727d6e118cc9cdc6da2e351aadfd9baa8cbdd3a76d429a695160d12c923ac9cc3baca289e193548608b82801',
        fr: '0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221',
        mulResult: Sp.none,
        checkResult: Sp.none,
    }),
);
