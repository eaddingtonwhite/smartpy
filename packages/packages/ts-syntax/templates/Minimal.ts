interface StorageSpec {
    value: TNat;
}

@Contract
export class Minimal {
    storage: StorageSpec = {
        value: 1,
    };

    @EntryPoint
    ep(value: TNat): void {
        this.storage.value = value;
    }
}

Dev.test({ name: 'Minimal' }, () => {
    // Originate `Minimal` contract with an initial balance of 1 xtz
    const c1 = Scenario.originate(new Minimal(), { initialBalance: 1000000 });
    Scenario.transfer(c1.ep(1));
});

Dev.compileContract('minimal', new Minimal());
