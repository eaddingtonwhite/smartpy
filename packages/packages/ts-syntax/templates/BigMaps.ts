interface TStorage {
    map: TBig_map<TString, TNat>;
}

@Contract
export class BigMaps {
    storage: TStorage = {
        map: [],
    };

    @EntryPoint
    set(param: { key: TString; value: TNat }): void {
        this.storage.map.set(param.key, param.value);
    }

    @EntryPoint
    remove(key: TString): void {
        const value = this.storage.map.get(key);
        if (this.storage.map.hasKey(key) && value === 1) {
            this.storage.map.remove(key);
        }
    }
}

Dev.test({ name: 'BigMaps' }, () => {
    const c1 = Scenario.originate(new BigMaps());
    Scenario.transfer(c1.set({ key: 'key1', value: 0 }));
    Scenario.transfer(c1.set({ key: 'key2', value: 1 }));
    Scenario.transfer(c1.set({ key: 'key3', value: 2 }));

    Scenario.transfer(c1.remove('key1'));
});

Dev.compileContract('BigMaps_compiled', new BigMaps());
