// Collatz, calling other contracts - Example for illustrative purposes only.
// - Compute the length of the nth Collatz sequence (https://oeis.org/A006577) with on-chain continuations

interface StorageSpec {
    onEven: TAddress;
    onOdd: TAddress;
    counter: TNat;
}

@Contract
export class Collatz {
    constructor(public storage: StorageSpec) {}

    @EntryPoint
    run(param: TNat) {
        if (param > 1) {
            this.storage.counter += 1;
            type tk = TRecord<{ k: TContract<TNat>; param: TNat }, Layout.right_comb>;
            const params = {
                k: Sp.selfEntryPoint('run'),
                param,
            };
            if (param % 2 === 0) {
                this.call(params, Sp.contract<tk>(this.storage.onEven).openSome());
            } else {
                this.call(params, Sp.contract<tk>(this.storage.onOdd).openSome());
            }
        }
    }

    @EntryPoint
    reset() {
        this.storage.counter = 0;
    }

    @Inline
    call(callback: TRecord<{ k: TContract<TNat>; param: TNat }, Layout.right_comb>, contract: TContract<TNat>) {
        Sp.transfer(callback, 0, contract);
    }
}

Dev.test({ name: 'Collatz' }, () => {
    const c1 = Scenario.originate(
        new Collatz({
            onEven: 'KT1QEaMVhcGvnf31cmWN4YWcujfzwhEQqX8c',
            onOdd: 'KT1CbRd63JiSrZsVEJzmXAmA5ACNvpqYTGbM',
            counter: 0,
        }),
    );
    Scenario.transfer(c1.run(1));
    Scenario.transfer(c1.reset());
});

Dev.compileContract(
    'collatz',
    new Collatz({
        onEven: 'KT1QEaMVhcGvnf31cmWN4YWcujfzwhEQqX8c',
        onOdd: 'KT1CbRd63JiSrZsVEJzmXAmA5ACNvpqYTGbM',
        counter: 0,
    }),
);
