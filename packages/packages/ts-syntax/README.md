# Typescript Syntax for SmartML

## Development

### Building

```shell
npm run build
```

### Testing

**Run tests**
```shell
npm run test
```

**Run tests without CLI baselines**
```shell
npm run simple-test
```
