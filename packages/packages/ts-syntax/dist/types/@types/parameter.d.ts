import type { ST_TypeDef } from './type';
export declare type ST_Parameter = {
    name: string;
    type: ST_TypeDef;
};
export declare type ST_Parameters = Record<string, ST_Parameter>;
