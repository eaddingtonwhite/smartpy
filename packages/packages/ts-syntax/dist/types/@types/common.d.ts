import type { LineAndCharacter } from 'typescript';
export declare type Nullable<T> = T | null | undefined;
export interface FileLineInfo extends LineAndCharacter {
    fileName: string;
}
export declare type FileInfo = {
    baseDir?: string;
    name: string;
    code: string;
};
