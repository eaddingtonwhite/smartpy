import type { ArrowFunction, FunctionDeclaration, MethodDeclaration } from 'typescript';
import type { ST_VariableValueKind } from '../enums/statement';
import type { FrontendType } from '../enums/type';
import type { FileLineInfo } from './common';
import type { ST_Property_Decorators } from './decorator';
import type { ST_Expression } from './expression';
import type { ST_Literal } from './literal';
import type { ST_Scope } from './module';
import type { ST_Statements, ST_VariableStatement } from './statement';
import type { ST_TypeDef, ST_TypeDefs } from './type';
export declare type ST_VariableValue = {
    kind: ST_VariableValueKind.Literal;
    literal: ST_Literal;
} | ST_Expression;
export interface ST_FunctionProperty {
    name: string;
    type: Extract<ST_TypeDef, {
        type: FrontendType.TFunction | FrontendType.TLambda;
    }>;
    typeDefs: ST_TypeDefs;
    id: number;
    properties: ST_Properties;
    statements: ST_Statements;
    pure: boolean;
    scope: ST_Scope;
    decorators: ST_Property_Decorators;
    node: FunctionDeclaration | MethodDeclaration | ArrowFunction;
    line: FileLineInfo;
}
export declare type ST_Properties = Record<string, ST_VariableStatement>;
