import type { ST_LiteralKind } from '../enums/literal';
import type { FileLineInfo } from './common';
export declare type ST_Literal = ({
    kind: ST_LiteralKind.Numeric;
    value: string;
} | {
    kind: ST_LiteralKind.String;
    value: string;
} | {
    kind: ST_LiteralKind.Boolean;
    value: boolean;
} | {
    kind: ST_LiteralKind.Unit;
}) & {
    line: FileLineInfo;
};
