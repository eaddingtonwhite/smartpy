import type { ST_ScenarioActionKind, ST_ScenarioKind } from '../enums/scenario';
import type { ST_TypedExpression } from './expression';
export interface ST_Scenario {
    kind: ST_ScenarioKind;
    scenario: ST_ScenarioAction[];
    name: string;
    shortname: string;
    longname: string;
    profile?: boolean;
    show: boolean;
}
export interface ST_CompilationTarget {
    name: string;
    className: string;
    args: ST_TypedExpression[];
}
export declare type ST_ScenarioAction = {
    action: ST_ScenarioActionKind.HTML;
    inner: string;
    line_no: string;
    tag: string;
} | {
    action: ST_ScenarioActionKind.NEW_CONTRACT;
    name: string;
    id: string;
    export: string;
    line_no: string;
    show: boolean;
    accept_unknown_types: boolean;
} | {
    action: ST_ScenarioActionKind.MESSAGE;
    id: string;
    message: string;
    amount?: string;
    sender?: string;
    source?: string;
    level?: string;
    time?: string;
    chain_id?: string;
    voting_powers?: string;
    exception?: string;
    valid?: string;
    line_no: string;
    params: string;
} | {
    action: ST_ScenarioActionKind.SHOW;
    compile: boolean;
    expression: string;
    html: boolean;
    line_no: string;
    stripStrings: boolean;
} | {
    action: ST_ScenarioActionKind.FLAG;
    flag: string[];
    line_no: string;
} | {
    action: ST_ScenarioActionKind.VERIFY;
    condition: string;
    line_no: string;
};
