import type { FileLineInfo } from './common';
import type { ST_Class_Decorators, ST_EntryPoint_Decorator, ST_OffChainView_Decorator, ST_GlobalLambda_Decorator } from './decorator';
import type { ST_Expression } from './expression';
import type { ST_FunctionProperty, ST_Properties } from './property';
import type { ST_TypeDef } from './type';
export declare type ST_Flag = {
    name: string;
    args: string[];
};
export interface ST_Contract {
    name: string;
    classRef: string;
}
export interface ST_ClassScope {
    properties: ST_Properties;
    functions: Record<string, ST_FunctionProperty>;
}
interface ST_FunctionArgument {
    index: number;
    name: string;
    type: ST_TypeDef;
    initializer?: ST_Expression;
}
export interface ST_Class {
    name: string;
    constructorArgs: Record<string, ST_FunctionArgument>;
    id: number;
    line: FileLineInfo;
    scope: ST_ClassScope;
    decorators: ST_Class_Decorators;
    entry_points: Record<string, ST_EntryPoint_Decorator>;
    off_chain_view: Record<string, ST_OffChainView_Decorator>;
    global_lambdas: Record<string, ST_GlobalLambda_Decorator>;
    metadata: Record<string, Record<string, ST_Expression>>;
}
export interface Report {
    msg: string;
    line?: FileLineInfo;
}
export declare type ST_Output = {
    scenario: unknown[];
    shortname: string;
    kind: string;
};
export {};
