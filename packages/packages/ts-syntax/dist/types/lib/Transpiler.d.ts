import { SourceFile, Node, ImportDeclaration } from 'typescript';
import OutputContext from './OutputContext';
import TypeInterpreter from './interpreter/type';
import LiteralInterpreter from './interpreter/literal';
import DecoratorInterpreter from './interpreter/decorator';
import DeclarationInterpreter from './interpreter/declaration';
import StatementInterpreter from './interpreter/statement';
import MetadataInterpreter from './interpreter/metadata';
import InliningInterpreter from './interpreter/inlining';
import type { FileInfo } from '../@types/common';
import type { ST_Scenario } from '../@types/scenario';
import ScenarioInterpreter from './interpreter/scenario';
import ExpressionTranslator from './translator/expression';
import StatementTranslator from './translator/statement';
declare enum InterpreterKind {
    Declaration = "Declaration",
    Decorator = "Decorator",
    Literal = "Literal",
    Statement = "Statement",
    Type = "Type",
    Metadata = "Metadata",
    Inlining = "Inlining",
    Scenario = "Scenario"
}
declare enum TranslatorKind {
    Expression = "Expression",
    Statement = "Statement"
}
export declare type Interpreters = Readonly<{
    [InterpreterKind.Declaration]: DeclarationInterpreter;
    [InterpreterKind.Decorator]: DecoratorInterpreter;
    [InterpreterKind.Literal]: LiteralInterpreter;
    [InterpreterKind.Statement]: StatementInterpreter;
    [InterpreterKind.Type]: TypeInterpreter;
    [InterpreterKind.Metadata]: MetadataInterpreter;
    [InterpreterKind.Inlining]: InliningInterpreter;
    [InterpreterKind.Scenario]: ScenarioInterpreter;
}>;
export declare type Translators = Readonly<{
    [TranslatorKind.Expression]: ExpressionTranslator;
    [TranslatorKind.Statement]: StatementTranslator;
}>;
export declare class Transpiler {
    source: FileInfo;
    sourceFile: SourceFile;
    output: OutputContext;
    interpreters: Interpreters;
    translators: Translators;
    constructor(source: FileInfo);
    transpile: () => Promise<ST_Scenario[]>;
    /**
     * @description Extract node name.
     * @param {Node} node
     * @returns {string} node name
     */
    extractName: (node: Node) => string;
    /**
     * @description Extract expression namespace.
     * @param {Node} node
     * @returns {string[]} namespace path
     */
    extractNamespace: (expr: Node) => string[];
    /**
     * @description Get Imported module name
     * @param {ImportDeclaration} node
     * @returns {string} Module name
     */
    getImportName: (node: ImportDeclaration) => string;
    /**
     * @description Visit import declaration and apply transformations
     * @param {ImportDeclaration} node
     * @returns {@Promise<void>}
     */
    visitImportDeclaration(node: ImportDeclaration): Promise<void>;
}
export default Transpiler;
