import { ObjectLiteralElementLike, ObjectLiteralExpression } from 'typescript';
import type { ST_Expression } from '../../@types/expression';
import { InterpreterBase } from './Base';
export default class MetadataInterpreter extends InterpreterBase {
    parseObjectLiteralExpression: (expr: ObjectLiteralExpression) => Record<string, ST_Expression>;
    parseElement: (el: ObjectLiteralElementLike, name: string) => ST_Expression;
}
