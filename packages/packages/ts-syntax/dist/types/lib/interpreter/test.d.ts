import { ConciseBody, Expression, Node } from 'typescript';
import { InterpreterBase } from './Base';
import type { ST_ScenarioAction, ST_ScenarioExpression } from '../../@types/scenario';
export default class TestInterpreter extends InterpreterBase {
    visitTestBody: ({ name, enabled }: {
        name: string;
        enabled: boolean;
    }, body: ConciseBody) => void;
    visitScenarioAction: (node: Node) => ST_ScenarioAction[];
    resolveExpression: (expression: Expression) => ST_ScenarioExpression;
}
