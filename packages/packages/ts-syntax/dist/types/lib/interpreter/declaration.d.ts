import { Node, ClassDeclaration, PropertyDeclaration, FunctionDeclaration, MethodDeclaration, ArrowFunction, Expression, VariableDeclaration } from 'typescript';
import { InterpreterBase } from './Base';
import type { ST_Expression } from '../../@types/expression';
import type { ST_Parameter } from '../../@types/parameter';
import type { ST_VariableStatement } from '../../@types/statement';
import type { ST_TypeDef } from '../../@types/type';
export default class DeclarationInterpreter extends InterpreterBase {
    visitClassDeclarationNode(node: ClassDeclaration): void;
    visitFunctionDeclarationNode: (node: FunctionDeclaration | MethodDeclaration | PropertyDeclaration | VariableDeclaration) => void;
    visitClassMember: (node: Node) => void;
    extractContractArgument: (expr: Expression, argType: ST_TypeDef) => ST_Expression;
    extractParameter: (node: Node) => ST_Parameter;
    /**
     * @description Deference property from anther property reference
     * @param {Node} Identifier Property reference
     * @returns {Extract<ST_Expression, { kind: ST_ExpressionKind.VariableDeclaration }> | void} Property value
     */
    dereferenceProperty: (name: string) => ST_VariableStatement | void;
    visitFunctionNode: (node: FunctionDeclaration | MethodDeclaration | ArrowFunction, inlined: boolean) => void;
}
