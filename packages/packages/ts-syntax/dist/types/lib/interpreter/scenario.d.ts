import { ConciseBody, CallExpression } from 'typescript';
import { InterpreterBase } from './Base';
export default class TestInterpreter extends InterpreterBase {
    visitTestBody: (body: ConciseBody) => void;
    /**
     * Resolve global call expression
     * @param {CallExpression} callExpr Call Expression
     */
    resolveDevExpression: (callExpr: CallExpression) => void;
}
