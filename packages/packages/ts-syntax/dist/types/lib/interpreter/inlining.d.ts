import { InterpreterBase } from './Base';
import type { ST_Expression } from '../../@types/expression';
import type { ST_Statement } from '../../@types/statement';
import type { FileLineInfo } from '../../@types/common';
import type { ST_FunctionProperty } from '../../@types/property';
export default class InliningInterpreter extends InterpreterBase {
    expandInlineExpression: (expression: ST_Expression, args: Record<string, ST_Expression>) => ST_Expression;
    expandInlineStatement: (statement: ST_Statement, args: Record<string, ST_Expression>) => ST_Statement;
    visitInlineCall: (args: Record<string, ST_Expression>, func: ST_FunctionProperty, line: FileLineInfo) => void;
}
