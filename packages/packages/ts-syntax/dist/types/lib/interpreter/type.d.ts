import { Node, TypeAliasDeclaration, TypeNode, NodeArray, InterfaceDeclaration, CallExpression } from 'typescript';
import type { Nullable } from '../../@types/common';
import type { ST_Expression } from '../../@types/expression';
import type { ST_TypeDef, ST_Layout } from '../../@types/type';
import { Layout } from '../../enums/type';
import { InterpreterBase } from './Base';
export default class TypeInterpreter extends InterpreterBase {
    visitInterfaceDeclaration: (node: InterfaceDeclaration) => void;
    visitTypeAliasDeclaration: (node: TypeAliasDeclaration) => void;
    /**
     * @description Extract type definition from type declaration.
     * @param {TypeNode} node
     * @returns {ST_TypeDef} Type definition
     */
    extractTypeDef: (node: TypeNode) => ST_TypeDef;
    /**
     * @description Check property type against the initializer.
     * @param {Node} node
     * @returns {ST_TypeDef} Type definition
     */
    checkType: (node: Node) => ST_TypeDef;
    private getTypesFromArray;
    private composeFrontendType;
    extractCallReturnType: (baseType: ST_TypeDef, args: NodeArray<Node>) => Nullable<ST_TypeDef>;
    /**
     * @description Get type from access expression
     * @param {string[]} namespace type reference
     * @returns {ST_TypeDef} Type Definition
     */
    dereferenceType: (namespace: string[]) => ST_TypeDef;
    /**
     * @description Get type from access expression
     * @param {string[]} namespace property reference
     * @returns {ST_TypeDef} Type Definition
     */
    dereferencePropertyType: (namespace: string[]) => ST_TypeDef;
    dereferenceTypeOfNativeValue: (namespace: string[]) => ST_TypeDef;
    dereferenceTypeOfNativeMethod: (namespace: string[]) => ST_TypeDef;
    extractLayout: (node: Node) => ST_Layout | Layout;
    resolveTypeOfAccessExpression: (expr: ST_Expression) => ST_TypeDef;
    validateNativeMethodArgs: (methodArgs: (ST_TypeDef & {
        optional: boolean;
    })[], expr: CallExpression) => void;
}
