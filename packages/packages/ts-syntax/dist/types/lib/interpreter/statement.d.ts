import { BinaryOperatorToken, Expression, BinaryExpression, ObjectLiteralExpression, VariableDeclaration, PropertyDeclaration, Identifier, Block, Statement, EnumDeclaration, CallExpression, NodeFlags, NodeArray } from 'typescript';
import { InterpreterBase } from './Base';
import { ST_BinaryToken, ST_ExpressionKind } from '../../enums/expression';
import { FrontendType } from '../../enums/type';
import type { ST_FunctionProperty } from '../../@types/property';
import type { FileLineInfo, Nullable } from '../../@types/common';
import type { ST_Expression, ST_TypedExpression } from '../../@types/expression';
import type { ST_Statement, ST_VariableStatement, ST_ExpressionStatement } from '../../@types/statement';
import type { ST_TypeDef, ST_TypeDefs } from '../../@types/type';
export default class StatementInterpreter extends InterpreterBase {
    visitBlock: (node: Block) => void;
    extractStatements: (statements: Statement[]) => ST_Statement[];
    extractBinaryOperator: (token: BinaryOperatorToken) => ST_BinaryToken;
    extractExpression: (expression: Expression, type?: ST_TypeDef | undefined) => ST_Expression;
    extractFunction: (expression: CallExpression) => ST_FunctionProperty | void;
    extractObjectLiteralExpression: (expression: ObjectLiteralExpression, type: ({
        type: FrontendType.TRecord;
        properties: ST_TypeDefs;
        layout?: import("../../@types/type").ST_Layout | import("../../enums/type").Layout | undefined;
    } & {
        line?: FileLineInfo | undefined;
    }) | ({
        type: FrontendType.TVariant;
        properties: ST_TypeDefs;
        layout?: import("../../@types/type").ST_Layout | import("../../enums/type").Layout | undefined;
    } & {
        line?: FileLineInfo | undefined;
    })) => Extract<ST_Expression, {
        kind: ST_ExpressionKind.ObjectLiteralExpression;
    }>;
    extractEnumExpression: (expression: EnumDeclaration, type: {
        type: FrontendType.TRecord;
        properties: ST_TypeDefs;
        layout?: import("../../@types/type").ST_Layout | import("../../enums/type").Layout | undefined;
    } & {
        line?: FileLineInfo | undefined;
    }) => Extract<ST_Expression, {
        kind: ST_ExpressionKind.ObjectLiteralExpression;
    }>;
    lookupIdentifierInScopes: (identifier: string, line: FileLineInfo) => Nullable<ST_Expression>;
    extractCallExpression: (expression: CallExpression, typeDef?: ST_TypeDef | undefined) => ST_Expression;
    extractAccessExpression: (expression: Expression, type?: ST_TypeDef | undefined) => ST_Expression;
    extractBinaryExpression: (expression: BinaryExpression) => ST_ExpressionStatement;
    extractEnumDeclaration: (node: EnumDeclaration) => ST_VariableStatement;
    extractVariableInitializer: (node: VariableDeclaration | PropertyDeclaration, flags: NodeFlags) => ST_VariableStatement;
    /**
     * @description Deference type from scope
     * @param {string} propName Property name
     * @param {Node} Identifier Property reference
     * @returns {ST_VariableStatement} Property value
     */
    dereferencePropertyDeclaration: (propName: string, node: Identifier) => ST_VariableStatement;
    extractArgExpressions: (expr: CallExpression, inputTypes: ST_TypeDefs) => Record<string, ST_TypedExpression>;
    resolveImpureLambdaStatements: (lambdaExpression: Extract<ST_Expression, {
        kind: ST_ExpressionKind.LambdaExpression;
    }>, args: NodeArray<Expression>, line: FileLineInfo) => ST_Expression;
}
