import { Node, Expression } from 'typescript';
import { InterpreterBase } from './Base';
import type { ST_Literal } from '../../@types/literal';
import type { ST_TypeDef } from '../../@types/type';
export default class LiteralInterpreter extends InterpreterBase {
    /**
     * @description Extract literal
     * @param {Node} node
     * @returns {ST_Literal} A literal
     */
    extractLiteral: (typeDef: ST_TypeDef, node: Node) => ST_Literal;
    extractStringLiteral: (expr: Expression) => string;
    extractBoolLiteral: (expr: Expression) => boolean;
    extractNumericLiteral: (expr: Expression) => string;
}
