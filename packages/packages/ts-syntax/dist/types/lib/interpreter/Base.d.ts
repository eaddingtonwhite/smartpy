import type { SourceFile } from 'typescript';
import type OutputContext from '../OutputContext';
import type Transpiler from '../Transpiler';
import type { Interpreters, Translators } from '../Transpiler';
export declare class InterpreterBase {
    transpiler: Transpiler;
    constructor(transpiler: Transpiler);
    get translators(): Translators;
    get interpreters(): Interpreters;
    get output(): OutputContext;
    get sourceFile(): SourceFile;
}
