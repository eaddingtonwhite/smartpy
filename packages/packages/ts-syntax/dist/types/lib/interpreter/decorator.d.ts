import { Decorator, ClassDeclaration, FunctionLikeDeclaration, PropertyDeclaration, VariableDeclaration, Node, ObjectLiteralExpression } from 'typescript';
import { InterpreterBase } from './Base';
import { ST_DecoratorKind } from '../../enums/decorator';
import type { ST_Class_Decorator, ST_Property_Decorators, ST_GlobalLambda_Decorator, ST_Metadata_Decorator, ST_Contract_Decorator, ST_OffChainView_Decorator, ST_EntryPoint_Decorator } from '../../@types/decorator';
export default class DecoratorInterpreter extends InterpreterBase {
    /**
     * @description Extract class decorators from class declaration.
     * @param {string} className
     * @param {ClassDeclaration} node
     * @returns {STDecorator[]} Class decorators
     */
    extractClassDecorators: (className: string, node: ClassDeclaration) => ST_Class_Decorator[];
    /**
     * @description Extract property decorators.
     * @param {string} className
     * @param {ClassDeclaration} node
     * @returns {STDecorator[]} Property decorators
     */
    extractPropertyDecorators: (node: FunctionLikeDeclaration | PropertyDeclaration | VariableDeclaration) => ST_Property_Decorators;
    /**
     * @description Extract information from @GlobalLambda decorator
     * @param {string} name
     * @param {Decorator} decorator node
     * @returns {ST_OffChainView_Decorator} @GlobalLambda decorator information
     */
    extractGlobalLambdaInfo: (name: string, decorator: Decorator) => ST_GlobalLambda_Decorator;
    /**
     * @description Extract information from @MetadataBuilder decorator
     * @param {string} name property name
     * @param {ObjectLiteralExpression} node node
     * @returns {ST_Metadata_Decorator} @MetadataBuilder decorator information
     */
    extractMetadataInfo: (name: string, node: ObjectLiteralExpression) => ST_Metadata_Decorator;
    /**
     * @description Extract information from @Contract decorator
     * @param {string} className
     * @param {Decorator} decorator node
     * @returns {ST_Decorator} @Contract decorator information
     */
    extractContractInfo: (className: string, decorator: Decorator) => ST_Contract_Decorator;
    /**
     * @description Extract information from @OffChainView decorator
     * @param {string} functionName
     * @param {Decorator} decorator node
     * @returns {ST_OffChainView_Decorator} @OffChainView decorator information
     */
    extractOffChainViewInfo: (functionName: string, decorator: Decorator) => ST_OffChainView_Decorator;
    /**
     * @description Extract information from @EntryPoint decorator
     * @param {string} functionName
     * @param {Decorator} decorator node
     * @returns {ST_Decorator} @EntryPoint decorator information
     */
    extractEntryPointInfo: (functionName: string, decorator: Decorator) => ST_EntryPoint_Decorator;
    hasDecorator: (node: Node, decorator: ST_DecoratorKind) => boolean;
}
