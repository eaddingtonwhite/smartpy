import { ArrowFunction, ClassDeclaration, FunctionDeclaration, ImportDeclaration, MethodDeclaration, Node, SourceFile } from 'typescript';
import type { Nullable } from '../@types/common';
import type { ST_Property_Decorators, ST_Class_Decorator } from '../@types/decorator';
import type { ST_Expression, ST_TypedExpression } from '../@types/expression';
import type { ST_ImportedModule, ST_Module } from '../@types/module';
import type { ST_Class } from '../@types/output';
import type { ST_FunctionProperty, ST_Properties } from '../@types/property';
import type { ST_CompilationTarget, ST_Scenario, ST_ScenarioAction } from '../@types/scenario';
import type { ST_Statement, ST_VariableStatement } from '../@types/statement';
import type { ST_TypeDefs, ST_TypeDef } from '../@types/type';
import { ST_ExpressionKind } from '../enums/expression';
import { ST_Modifier } from '../enums/Modifiers';
import { ST_ScopeKind } from '../enums/scope';
import { FrontendType } from '../enums/type';
import type { Translators } from './Transpiler';
declare class Scope {
    kind: ST_ScopeKind;
    parent?: Scope | undefined;
    iterators: Record<string, Extract<ST_Expression, {
        kind: ST_ExpressionKind.ForIterator;
    }>>;
    properties: ST_Properties;
    statements: ST_Statement[];
    functions: Record<string, ST_FunctionProperty>;
    typeDefs: ST_TypeDefs;
    classes: Record<string, ST_Class>;
    varCounter: number;
    constructor(kind: ST_ScopeKind, parent?: Scope | undefined);
    addIterator: (name: string, expr: Extract<ST_Expression, {
        kind: ST_ExpressionKind.ForIterator;
    }>) => {
        kind: ST_ExpressionKind.ForIterator;
        name: string;
        type: ST_TypeDef;
    } & {
        line: import("../@types/common").FileLineInfo;
    };
    removeIterator: (name: string) => boolean;
    /**
     * @description Records a statement.
     * @param {ST_Statement} statement
     */
    emitStatement: (statement: ST_Statement) => void;
    /**
     * @description Generates a variable identifier.
     * @return {string} Variable identifier.
     */
    generateVarID: () => string;
}
declare class BaseContext {
    sourceFile: SourceFile;
    translators: Translators;
    importing: Nullable<string>;
    declaringClass: Nullable<string>;
    declaringMethod: Nullable<string>;
    switchCase: Nullable<{
        variant: string;
        accessExpr: Extract<ST_Expression, {
            kind: ST_ExpressionKind.VariantAccess;
        }>;
    }>;
    modules: Record<string, ST_ImportedModule>;
    exported: string[];
    scopes: Scope[];
    result: ST_Module;
    constructor(sourceFile: SourceFile, translators: Translators);
    get scope(): Scope;
    /**
     * @description Filter all non exported (classes, properties, type definition) and return the module scope
     */
    get exportedModule(): ST_Module;
    /**
     * @description Add imported module.
     * @param {string} name Module name
     * @param {ST_Module} module Module scope
     * @param {ImportDeclaration} node Import declaration node
     */
    addModule: (name: string, module: ST_Module, node: ImportDeclaration) => void;
    /**
     * @description Getter that returns a frozen context of the imported modules
     */
    get importedModules(): Readonly<Record<string, ST_ImportedModule>>;
    get classes(): Record<string, ST_Class>;
    /**
     * @description Get global properties.
     * @returns {ST_Properties} ROOT properties
     */
    get globalProperties(): ST_Properties;
    /**
     * @description Get Global type definitions.
     * @returns {ST_TypeDefs} ROOT type definitions.
     */
    get globalTypeDefs(): ST_TypeDefs;
    lambdaCounter: number;
    get nextLambdaId(): number;
}
export declare class OutputContext extends BaseContext {
    /**
     * @description Initialize new scope.
     */
    enterScope(kind?: ST_ScopeKind): void;
    /**
     * @description Remove previous scope.
     */
    exitScope(): void;
    /**
     * @description Get properties by class.
     * @param {string} className
     * @returns {ST_Properties} properties
     */
    getClassProperties(className: string): ST_Properties;
    /**
     * @description Get class method
     * @param {string} className
     * @param {string} methodName
     * @returns {ST_FunctionProperty} The method
     */
    getClassMethod(className: string, methodName: string): ST_FunctionProperty;
    /**
     * @description Get current class
     * @returns {ST_Class} class being declared
     */
    get currentClass(): ST_Class;
    /**
     * @description Get current method being interpreted.
     * @returns {ST_FunctionProperty} The method
     */
    get currentMethod(): ST_FunctionProperty;
    /**
     * @description Get current scenario being interpreted.
     * @returns {ST_Scenario} The scenario
     */
    get currentScenario(): ST_Scenario;
    /**
     * @description Record class declaration;
     * @param {ClassDeclaration} node Class declaration node.
     * @returns {string} Class name.
     */
    emitClassDeclaration: (node: ClassDeclaration) => string;
    /**
     * @description Record type definition.
     * @param {string} name Type name
     * @param {ST_TypeDef} typeDef Type specification
     * @param {ST_Modifier[]} modifiers type modifiers (export, etc...)
     */
    emitTypeDef: (name: string, typeDef: ST_TypeDef, modifiers: ST_Modifier[]) => void;
    /**
     * @description Record variable declaration.
     * @param {ST_VariableStatement} variable
     */
    emitVariableDeclaration: (variable: ST_VariableStatement) => void;
    /**
     * @description Record method declaration.
     * @param {string} name method name
     * @param {ST_TypeDef} type method signature
     * @param {ST_Modifier[]} modifiers method modifiers (export, etc...)
     * @param {Node} node function node
     */
    emitFunctionDeclaration: (name: string, type: {
        type: FrontendType.TLambda | FrontendType.TFunction;
        inputTypes: Record<string, ST_TypeDef & {
            index: number;
        }>;
        outputType: ST_TypeDef;
    } & {
        line?: import("../@types/common").FileLineInfo | undefined;
    }, modifiers: ST_Modifier[], node: FunctionDeclaration | MethodDeclaration | ArrowFunction, decorators: ST_Property_Decorators) => void;
    /**
     * @description Record method statement.
     * @param {ST_Statement} statement method statement
     */
    emitFunctionStatement: (statement: ST_Statement) => void;
    /**
     * @description Record class decorator.
     * @param name Class name
     * @param decorator Class decorator
     */
    emitClassDecorator: (className: string, decorator: ST_Class_Decorator) => void;
    /**
     * @description Record property decorator.
     * @param decorator Class decorator
     * @param propertyName Property name
     */
    emitPropertyDecorators: (decorators: ST_Property_Decorators) => void;
    /**
     * @description Record compilation target.
     * @param {string} name compilation target name
     * @param {string} className Contract className
     * @param {ST_Expression[]} args arguments expected by the class constructor
     */
    emitCompilationTarget: (name: string, className: string, args: ST_TypedExpression[]) => void;
    /**
     * @description Emit scenario.
     * @param {ST_Scenario} scenario
     */
    emitScenario: (scenario: ST_Scenario) => void;
    /**
     * @description Emit a scenario action.
     * @param {ST_ScenarioAction} action Scenario Action
     */
    emitScenarioAction: (action: ST_ScenarioAction) => void;
    emitError: (node: Node, msg: string) => never;
    emitWarning: (node: Node, msg: string) => void;
    getResult(): ST_Scenario[];
    exportContract: (contract: ST_Class, storage: ST_Expression & {
        type?: ST_TypeDef;
    }, initialBalance?: string) => string;
    getNewContractAction: (compilation: ST_CompilationTarget, id: number, show?: boolean, initialBalance?: string) => ST_ScenarioAction;
    stringifyFlags(contract: ST_Class): string;
    buildSubEntryPointStatements(func: ST_FunctionProperty): ST_FunctionProperty;
    buildMessages(contract: ST_Class): string;
    buildViews(contract: ST_Class): string;
    buildGlobals(contract: ST_Class): string;
    buildMetadata(contract: ST_Class): string;
    capitalizeBoolean: (bool: boolean) => string;
}
export default OutputContext;
