import type { CompilerHost, CompilerOptions, SourceFile } from 'typescript';
import type { FileInfo } from '../@types/common';
export declare class STCompilerHost implements CompilerHost {
    private sourceFiles;
    constructor(files: FileInfo[]);
    getSourceFile: (fileName: string) => SourceFile;
    getDefaultLibFileName: (defaultLibOptions: CompilerOptions) => string;
    writeFile: () => void;
    getCurrentDirectory: () => string;
    getDirectories: (_: string) => string[];
    fileExists: (fileName: string) => boolean;
    readFile: (fileName: string) => string;
    getCanonicalFileName: (fileName: string) => string;
    useCaseSensitiveFileNames: () => boolean;
    getNewLine: () => string;
    getEnvironmentVariable: () => string;
}
export default CompilerHost;
