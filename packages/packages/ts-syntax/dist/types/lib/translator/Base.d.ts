import type { Translators } from '../Transpiler';
import type Transpiler from '../Transpiler';
export declare class TranslatorBase {
    transpiler: Transpiler;
    constructor(transpiler: Transpiler);
    get translators(): Translators;
}
