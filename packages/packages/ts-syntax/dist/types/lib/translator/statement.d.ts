import type { ST_Statements, ST_Statement } from '../../@types/statement';
import { TranslatorBase } from './Base';
export default class StatementTranslator extends TranslatorBase {
    translateStatements: (statements: ST_Statements) => string;
    translateStatement: (statement: ST_Statement) => string;
}
