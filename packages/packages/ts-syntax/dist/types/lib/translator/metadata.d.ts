import type { ST_Expression } from '../../@types/expression';
declare const MetadataTranslator: {
    translateMetadataObject: (properties: Record<string, ST_Expression>, line: string | number) => string;
};
export default MetadataTranslator;
