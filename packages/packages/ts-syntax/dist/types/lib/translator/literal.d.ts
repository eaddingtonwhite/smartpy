import type { ST_Literal } from '../../@types/literal';
import type { ST_TypeDef } from '../../@types/type';
declare const LiteralTranslator: {
    translateLiteral: (value: ST_Literal, type: ST_TypeDef) => string;
};
export default LiteralTranslator;
