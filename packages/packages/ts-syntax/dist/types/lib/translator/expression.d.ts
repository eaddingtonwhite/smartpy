import { ST_ExpressionKind } from '../../enums/expression';
import { FrontendType } from '../../enums/type';
import type { ST_Expression } from '../../@types/expression';
import type { ST_TypeDef } from '../../@types/type';
import { TranslatorBase } from './Base';
export default class ExpressionTranslator extends TranslatorBase {
    translateExpression: (expression: ST_Expression, type?: ST_TypeDef | undefined) => string;
    translateObjectLiteralExpression: (expression: Extract<ST_Expression, {
        kind: ST_ExpressionKind.ObjectLiteralExpression;
    }>, type: ST_TypeDef) => string;
    translateArrayLiteralExpression: (expression: Extract<ST_Expression, {
        kind: ST_ExpressionKind.ArrayLiteralExpression;
    }>, type: ST_TypeDef) => string;
    translateMapEntry: (elements: ST_Expression[], type: {
        type: FrontendType.TBig_map | FrontendType.TMap;
        keyType: ST_TypeDef;
        valueType: ST_TypeDef;
    } & {
        line?: import("../../@types/common").FileLineInfo | undefined;
    }) => string;
}
