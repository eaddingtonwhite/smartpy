import type { ST_TypeDef } from '../../@types/type';
import { FrontendType } from '../../enums/type';
declare const TypeTranslator: {
    translateType: (typeDef?: ST_TypeDef | undefined) => string;
    translateInputType: (typeDef: Extract<ST_TypeDef, {
        type: FrontendType.TFunction | FrontendType.TLambda;
    }>) => string;
};
export default TypeTranslator;
