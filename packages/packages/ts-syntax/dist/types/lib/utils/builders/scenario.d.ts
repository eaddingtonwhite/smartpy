import type { FileLineInfo } from '../../../@types/common';
import type { ST_Expression } from '../../../@types/expression';
import { ST_ExpressionKind } from '../../../enums/expression';
declare const ScenarioBuilder: {
    verifyEqual: (leftExpr: ST_Expression, rightExpr: ST_Expression, line: FileLineInfo) => Extract<ST_Expression, {
        kind: ST_ExpressionKind.ScenarioVerify;
    }>;
};
export default ScenarioBuilder;
