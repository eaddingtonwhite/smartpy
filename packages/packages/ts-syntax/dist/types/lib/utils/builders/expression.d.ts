import type { FileLineInfo } from '../../../@types/common';
import type { ST_Expression } from '../../../@types/expression';
import type { ST_TypeDef } from '../../../@types/type';
import { ST_BinaryToken, ST_ExpressionKind } from '../../../enums/expression';
export declare const mutezLiteral: (v: string | number, line: FileLineInfo) => ST_Expression;
export declare const addressLiteral: (address: string, line: FileLineInfo) => Extract<ST_Expression, {
    kind: ST_ExpressionKind.LiteralExpr;
}>;
export declare const boolLiteral: (value: boolean, line: FileLineInfo) => ST_Expression;
export declare const natLiteral: (value: string, line: FileLineInfo) => ST_Expression;
export declare const intLiteral: (value: string, line: FileLineInfo) => ST_Expression;
export declare const timestampLiteral: (value: string, line: FileLineInfo) => ST_Expression;
export declare const chainIdLiteral: (value: string, line: FileLineInfo) => ST_Expression;
export declare const stringLiteral: (value: string, line: FileLineInfo) => ST_Expression;
export declare const some: (expression: ST_Expression, type: ST_TypeDef, line: FileLineInfo) => ST_Expression;
export declare const pack: (expr: ST_Expression) => ST_Expression;
export declare const binaryExpression: (leftExpr: ST_Expression, rightExpr: ST_Expression, operator: ST_BinaryToken, line: FileLineInfo) => ST_Expression;
export declare const attrAccessExpression: (attr: string, prev: ST_Expression, line: FileLineInfo, type?: ST_TypeDef | undefined) => ST_Expression;
export declare const methodPropAccessExpression: (attr: string, line: FileLineInfo, type?: ST_TypeDef | undefined) => ST_Expression;
declare const ExpressionBuilder: {
    mutezLiteral: (v: string | number, line: FileLineInfo) => ST_Expression;
    intLiteral: (value: string, line: FileLineInfo) => ST_Expression;
    addressLiteral: (address: string, line: FileLineInfo) => Extract<ST_Expression, {
        kind: ST_ExpressionKind.LiteralExpr;
    }>;
    boolLiteral: (value: boolean, line: FileLineInfo) => ST_Expression;
    stringLiteral: (value: string, line: FileLineInfo) => ST_Expression;
    natLiteral: (value: string, line: FileLineInfo) => ST_Expression;
    timestampLiteral: (value: string, line: FileLineInfo) => ST_Expression;
    chainIdLiteral: (value: string, line: FileLineInfo) => ST_Expression;
    pack: (expr: ST_Expression) => ST_Expression;
    some: (expression: ST_Expression, type: ST_TypeDef, line: FileLineInfo) => ST_Expression;
    binaryExpression: (leftExpr: ST_Expression, rightExpr: ST_Expression, operator: ST_BinaryToken, line: FileLineInfo) => ST_Expression;
    attrAccessExpression: (attr: string, prev: ST_Expression, line: FileLineInfo, type?: ST_TypeDef | undefined) => ST_Expression;
    methodPropAccessExpression: (attr: string, line: FileLineInfo, type?: ST_TypeDef | undefined) => ST_Expression;
};
export default ExpressionBuilder;
