import { ClassDeclaration, EnumDeclaration, FunctionDeclaration, InterfaceDeclaration, MethodDeclaration, PropertyDeclaration, TypeAliasDeclaration, VariableDeclaration, VariableStatement, ParameterDeclaration } from 'typescript';
import { ST_Modifier } from '../../enums/Modifiers';
export declare const ModifiersUtils: {
    extractModifiers: (node: TypeAliasDeclaration | InterfaceDeclaration | ClassDeclaration | FunctionDeclaration | MethodDeclaration | PropertyDeclaration | VariableStatement | VariableDeclaration | EnumDeclaration | ParameterDeclaration) => ST_Modifier[];
};
export default ModifiersUtils;
