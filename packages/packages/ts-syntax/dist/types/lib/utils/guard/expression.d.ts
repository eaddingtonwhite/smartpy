import type { ST_Expression, ST_TypedExpression } from '../../../@types/expression';
import { ST_ExpressionKind } from '../../../enums/expression';
export declare const hasType: (expr: ST_Expression) => expr is ST_TypedExpression;
export declare const isAttrAccessExpr: (expr: ST_Expression) => expr is {
    kind: ST_ExpressionKind.AttrAccessExpr;
    attr: string;
    type: import("../../../@types/type").ST_TypeDef;
    prev: import("../../../@types/common").Nullable<ST_Expression>;
} & {
    line: import("../../../@types/common").FileLineInfo;
};
export declare const isExpression: (arg: {
    kind: string;
}) => arg is ST_Expression;
export declare const isLiteralExpr: (expr: ST_Expression) => expr is {
    kind: ST_ExpressionKind.LiteralExpr;
    type: import("../../../@types/type").ST_TypeDef;
    literal: import("../../../@types/literal").ST_Literal;
} & {
    line: import("../../../@types/common").FileLineInfo;
};
declare const expression: {
    isLiteralExpr: (expr: ST_Expression) => expr is {
        kind: ST_ExpressionKind.LiteralExpr;
        type: import("../../../@types/type").ST_TypeDef;
        literal: import("../../../@types/literal").ST_Literal;
    } & {
        line: import("../../../@types/common").FileLineInfo;
    };
    hasType: (expr: ST_Expression) => expr is ST_TypedExpression;
    isAttrAccessExpr: (expr: ST_Expression) => expr is {
        kind: ST_ExpressionKind.AttrAccessExpr;
        attr: string;
        type: import("../../../@types/type").ST_TypeDef;
        prev: import("../../../@types/common").Nullable<ST_Expression>;
    } & {
        line: import("../../../@types/common").FileLineInfo;
    };
    isExpression: (arg: {
        kind: string;
    }) => arg is ST_Expression;
};
export default expression;
