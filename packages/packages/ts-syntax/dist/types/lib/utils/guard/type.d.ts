import type { ST_TypeDef } from '../../../@types/type';
import { FrontendType } from '../../../enums/type';
export declare const hasProperties: (typeDef: ST_TypeDef) => typeDef is ({
    type: FrontendType.TRecord;
    properties: import("../../../@types/type").ST_TypeDefs;
    layout?: import("../../../@types/type").ST_Layout | import("../../../enums/type").Layout | undefined;
} & {
    line?: import("../../../@types/common").FileLineInfo | undefined;
}) | ({
    type: FrontendType.TVariant;
    properties: import("../../../@types/type").ST_TypeDefs;
    layout?: import("../../../@types/type").ST_Layout | import("../../../enums/type").Layout | undefined;
} & {
    line?: import("../../../@types/common").FileLineInfo | undefined;
});
export declare const isTuple: (typeDef: ST_TypeDef) => typeDef is {
    type: FrontendType.TList | FrontendType.TTuple | FrontendType.TSet;
    innerTypes: ST_TypeDef[];
} & {
    line?: import("../../../@types/common").FileLineInfo | undefined;
};
export declare const hasOneArg: (typeDef: ST_TypeDef) => typeDef is ({
    type: FrontendType.TOption;
    none: boolean;
    innerType: ST_TypeDef;
} & {
    line?: import("../../../@types/common").FileLineInfo | undefined;
}) | ({
    type: FrontendType.TOption;
    innerType: ST_TypeDef;
} & {
    line?: import("../../../@types/common").FileLineInfo | undefined;
});
export declare const isMap: (typeDef: ST_TypeDef) => typeDef is {
    type: FrontendType.TBig_map | FrontendType.TMap;
    keyType: ST_TypeDef;
    valueType: ST_TypeDef;
} & {
    line?: import("../../../@types/common").FileLineInfo | undefined;
};
export declare const isUnknown: (t: ST_TypeDef) => boolean;
export declare const isFunction: (t: ST_TypeDef) => t is {
    type: FrontendType.TLambda | FrontendType.TFunction;
    inputTypes: Record<string, ST_TypeDef & {
        index: number;
    }>;
    outputType: ST_TypeDef;
} & {
    line?: import("../../../@types/common").FileLineInfo | undefined;
};
declare const type: {
    hasProperties: (typeDef: ST_TypeDef) => typeDef is ({
        type: FrontendType.TRecord;
        properties: import("../../../@types/type").ST_TypeDefs;
        layout?: import("../../../@types/type").ST_Layout | import("../../../enums/type").Layout | undefined;
    } & {
        line?: import("../../../@types/common").FileLineInfo | undefined;
    }) | ({
        type: FrontendType.TVariant;
        properties: import("../../../@types/type").ST_TypeDefs;
        layout?: import("../../../@types/type").ST_Layout | import("../../../enums/type").Layout | undefined;
    } & {
        line?: import("../../../@types/common").FileLineInfo | undefined;
    });
    hasOneArg: (typeDef: ST_TypeDef) => typeDef is ({
        type: FrontendType.TOption;
        none: boolean;
        innerType: ST_TypeDef;
    } & {
        line?: import("../../../@types/common").FileLineInfo | undefined;
    }) | ({
        type: FrontendType.TOption;
        innerType: ST_TypeDef;
    } & {
        line?: import("../../../@types/common").FileLineInfo | undefined;
    });
    isTuple: (typeDef: ST_TypeDef) => typeDef is {
        type: FrontendType.TList | FrontendType.TTuple | FrontendType.TSet;
        innerTypes: ST_TypeDef[];
    } & {
        line?: import("../../../@types/common").FileLineInfo | undefined;
    };
    isMap: (typeDef: ST_TypeDef) => typeDef is {
        type: FrontendType.TBig_map | FrontendType.TMap;
        keyType: ST_TypeDef;
        valueType: ST_TypeDef;
    } & {
        line?: import("../../../@types/common").FileLineInfo | undefined;
    };
    isUnknown: (t: ST_TypeDef) => boolean;
    isFunction: (t: ST_TypeDef) => t is {
        type: FrontendType.TLambda | FrontendType.TFunction;
        inputTypes: Record<string, ST_TypeDef & {
            index: number;
        }>;
        outputType: ST_TypeDef;
    } & {
        line?: import("../../../@types/common").FileLineInfo | undefined;
    };
};
export default type;
