import type { ST_Literal } from '../../../@types/literal';
import { ST_LiteralKind } from '../../../enums/literal';
export declare const isString: (l: ST_Literal) => l is {
    kind: ST_LiteralKind.String;
    value: string;
} & {
    line: import("../../../@types/common").FileLineInfo;
};
export declare const isBoolean: (l: ST_Literal) => l is {
    kind: ST_LiteralKind.Boolean;
    value: boolean;
} & {
    line: import("../../../@types/common").FileLineInfo;
};
export declare const isNumeric: (l: ST_Literal) => l is {
    kind: ST_LiteralKind.Numeric;
    value: string;
} & {
    line: import("../../../@types/common").FileLineInfo;
};
declare const literal: {
    isString: (l: ST_Literal) => l is {
        kind: ST_LiteralKind.String;
        value: string;
    } & {
        line: import("../../../@types/common").FileLineInfo;
    };
    isBoolean: (l: ST_Literal) => l is {
        kind: ST_LiteralKind.Boolean;
        value: boolean;
    } & {
        line: import("../../../@types/common").FileLineInfo;
    };
    isNumeric: (l: ST_Literal) => l is {
        kind: ST_LiteralKind.Numeric;
        value: string;
    } & {
        line: import("../../../@types/common").FileLineInfo;
    };
};
export default literal;
