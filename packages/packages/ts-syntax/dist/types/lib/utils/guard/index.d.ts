declare const guards: {
    type: {
        hasProperties: (typeDef: import("../../../@types/type").ST_TypeDef) => typeDef is ({
            type: import("../../../enums/type").FrontendType.TRecord;
            properties: import("../../../@types/type").ST_TypeDefs;
            layout?: import("../../../@types/type").ST_Layout | import("../../../enums/type").Layout | undefined;
        } & {
            line?: import("../../../@types/common").FileLineInfo | undefined;
        }) | ({
            type: import("../../../enums/type").FrontendType.TVariant;
            properties: import("../../../@types/type").ST_TypeDefs;
            layout?: import("../../../@types/type").ST_Layout | import("../../../enums/type").Layout | undefined;
        } & {
            line?: import("../../../@types/common").FileLineInfo | undefined;
        });
        hasOneArg: (typeDef: import("../../../@types/type").ST_TypeDef) => typeDef is ({
            type: import("../../../enums/type").FrontendType.TOption;
            none: boolean;
            innerType: import("../../../@types/type").ST_TypeDef;
        } & {
            line?: import("../../../@types/common").FileLineInfo | undefined;
        }) | ({
            type: import("../../../enums/type").FrontendType.TOption;
            innerType: import("../../../@types/type").ST_TypeDef;
        } & {
            line?: import("../../../@types/common").FileLineInfo | undefined;
        });
        isTuple: (typeDef: import("../../../@types/type").ST_TypeDef) => typeDef is {
            type: import("../../../enums/type").FrontendType.TList | import("../../../enums/type").FrontendType.TTuple | import("../../../enums/type").FrontendType.TSet;
            innerTypes: import("../../../@types/type").ST_TypeDef[];
        } & {
            line?: import("../../../@types/common").FileLineInfo | undefined;
        };
        isMap: (typeDef: import("../../../@types/type").ST_TypeDef) => typeDef is {
            type: import("../../../enums/type").FrontendType.TBig_map | import("../../../enums/type").FrontendType.TMap;
            keyType: import("../../../@types/type").ST_TypeDef;
            valueType: import("../../../@types/type").ST_TypeDef;
        } & {
            line?: import("../../../@types/common").FileLineInfo | undefined;
        };
        isUnknown: (t: import("../../../@types/type").ST_TypeDef) => boolean;
        isFunction: (t: import("../../../@types/type").ST_TypeDef) => t is {
            type: import("../../../enums/type").FrontendType.TLambda | import("../../../enums/type").FrontendType.TFunction;
            inputTypes: Record<string, import("../../../@types/type").ST_TypeDef & {
                index: number;
            }>;
            outputType: import("../../../@types/type").ST_TypeDef;
        } & {
            line?: import("../../../@types/common").FileLineInfo | undefined;
        };
    };
    literal: {
        isString: (l: import("../../../@types/literal").ST_Literal) => l is {
            kind: import("../../../enums/literal").ST_LiteralKind.String;
            value: string;
        } & {
            line: import("../../../@types/common").FileLineInfo;
        };
        isBoolean: (l: import("../../../@types/literal").ST_Literal) => l is {
            kind: import("../../../enums/literal").ST_LiteralKind.Boolean;
            value: boolean;
        } & {
            line: import("../../../@types/common").FileLineInfo;
        };
        isNumeric: (l: import("../../../@types/literal").ST_Literal) => l is {
            kind: import("../../../enums/literal").ST_LiteralKind.Numeric;
            value: string;
        } & {
            line: import("../../../@types/common").FileLineInfo;
        };
    };
    expression: {
        isLiteralExpr: (expr: import("../../../@types/expression").ST_Expression) => expr is {
            kind: import("../../../enums/expression").ST_ExpressionKind.LiteralExpr;
            type: import("../../../@types/type").ST_TypeDef;
            literal: import("../../../@types/literal").ST_Literal;
        } & {
            line: import("../../../@types/common").FileLineInfo;
        };
        hasType: (expr: import("../../../@types/expression").ST_Expression) => expr is import("../../../@types/expression").ST_TypedExpression;
        isAttrAccessExpr: (expr: import("../../../@types/expression").ST_Expression) => expr is {
            kind: import("../../../enums/expression").ST_ExpressionKind.AttrAccessExpr;
            attr: string;
            type: import("../../../@types/type").ST_TypeDef;
            prev: import("../../../@types/common").Nullable<import("../../../@types/expression").ST_Expression>;
        } & {
            line: import("../../../@types/common").FileLineInfo;
        };
        isExpression: (arg: {
            kind: string;
        }) => arg is import("../../../@types/expression").ST_Expression;
    };
};
export default guards;
