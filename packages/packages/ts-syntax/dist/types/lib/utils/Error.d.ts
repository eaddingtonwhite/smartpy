import type { FileLineInfo } from '../../@types/common';
interface ST_ErrorParams {
    msg: string;
    line: FileLineInfo;
    text?: string;
    fileName?: string;
}
export declare const failWith: (msg: string) => never;
export declare class ST_Error extends Error {
    message: string;
    line: FileLineInfo;
    snippet?: string | undefined;
    name: string;
    constructor(message: string, line: FileLineInfo, snippet?: string | undefined);
}
export declare const failWithInfo: (obj: ST_ErrorParams) => never;
declare const ErrorUtils: {
    failWith: (msg: string) => never;
    failWithInfo: (obj: ST_ErrorParams) => never;
};
export default ErrorUtils;
