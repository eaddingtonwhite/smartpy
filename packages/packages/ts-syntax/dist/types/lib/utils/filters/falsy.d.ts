/**
 * @description Returns true if value is falsy
 * @param {unknown} o a value of any type
 * @returns {boolean} true if "o" is falsy, and true otherwise
 */
export declare const isFalsy: (o: unknown) => o is undefined;
/**
 * @description Returns true if value is not falsy
 * @param {unknown} o a value of any type
 * @returns {boolean} true if "o" is not falsy, and false otherwise
 */
export declare const nonFalsy: <T>(o: T) => o is NonNullable<T>;
