import { Node } from 'typescript';
import type { Nullable } from '../../@types/common';
export declare const IdentifierUtils: {
    extractName: (node: Node) => Nullable<string>;
};
export default IdentifierUtils;
