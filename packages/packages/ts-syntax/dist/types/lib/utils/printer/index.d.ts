declare const PrinterUtils: {
    type: {
        toString: (typeDef: import("../../../@types/type").ST_TypeDef) => string;
    };
};
export default PrinterUtils;
