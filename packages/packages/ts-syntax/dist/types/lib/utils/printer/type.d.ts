import type { ST_TypeDef } from '../../../@types/type';
declare const type: {
    toString: (typeDef: ST_TypeDef) => string;
};
export default type;
