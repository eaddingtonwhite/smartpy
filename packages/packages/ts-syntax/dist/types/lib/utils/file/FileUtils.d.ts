export declare function setFetchFromURL(newMethod: (filePath: string) => Promise<string>): void;
export declare function setFetchFromDisk(newMethod: (filePath: string) => Promise<string>): void;
declare const FileUtils: {
    getSourceFromFile: (filePath: string, isURL: boolean, baseDir?: string) => Promise<string>;
};
export default FileUtils;
