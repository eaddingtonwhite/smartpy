import { BinaryOperatorToken } from 'typescript';
import { ST_BinaryToken } from '../../enums/expression';
export declare const isAssignmentBinaryOperatorToken: (o: BinaryOperatorToken) => boolean;
export declare const isAssignmentBinaryOperator: (o: ST_BinaryToken) => boolean;
