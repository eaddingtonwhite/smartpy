import type { Nullable } from '../../@types/common';
import type { ST_TypeDef } from '../../@types/type';
export declare const CheckerUtils: {
    checkTypes: (implicit: ST_TypeDef, explicit: ST_TypeDef) => Nullable<ST_TypeDef>;
};
export default CheckerUtils;
