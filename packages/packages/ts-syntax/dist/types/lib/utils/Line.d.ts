import type { SourceFile, Node } from 'typescript';
import type { FileLineInfo } from '../../@types/common';
declare const LineUtils: {
    getLineAndCharacter: (sourceFile: SourceFile, node: Node) => FileLineInfo;
    getLineNumber: (line?: FileLineInfo | undefined) => string;
    getFileLineSneakCased: (line: FileLineInfo) => string;
};
export default LineUtils;
