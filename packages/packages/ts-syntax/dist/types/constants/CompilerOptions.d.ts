import type { CompilerOptions as CompilerOptionsType } from 'typescript';
export declare const CompilerOptions: CompilerOptionsType;
export default CompilerOptions;
