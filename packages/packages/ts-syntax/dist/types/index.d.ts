import { Transpiler } from './lib/Transpiler';
import type { ST_Scenario } from './@types/scenario';
import type { FileInfo } from './@types/common';
export type { ST_Error } from './lib/utils/Error';
export { setFetchFromDisk, setFetchFromURL } from './lib/utils/file/FileUtils';
declare const SmartTS: {
    typings: string;
    Transpiler: typeof Transpiler;
    transpile: (sourceCode: FileInfo) => Promise<ST_Scenario[]>;
};
export default SmartTS;
