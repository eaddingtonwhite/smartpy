export declare enum ST_FileExtension {
    TS = "ts",
    JSON = "json"
}
