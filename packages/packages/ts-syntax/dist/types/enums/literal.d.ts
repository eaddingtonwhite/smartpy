export declare enum ST_LiteralKind {
    Numeric = "Numeric",
    String = "String",
    Boolean = "Boolean",
    Unit = "Unit"
}
