export declare enum ST_ScenarioActionKind {
    HTML = "html",
    NEW_CONTRACT = "newContract",
    MESSAGE = "message",
    SHOW = "show",
    FLAG = "flag",
    VERIFY = "verify"
}
export declare enum ST_ScenarioKind {
    Compilation = "compilation",
    Test = "test"
}
