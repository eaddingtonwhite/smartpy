export declare enum ST_Modifier {
    Export = "Export",
    Static = "Static",
    Public = "Public",
    Private = "Private"
}
