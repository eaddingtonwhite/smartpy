export declare enum FrontendType {
    TAddress = "TAddress",
    TBig_map = "TBig_map",
    TBls12_381_fr = "TBls12_381_fr",
    TBls12_381_g1 = "TBls12_381_g1",
    TBls12_381_g2 = "TBls12_381_g2",
    TBool = "TBool",
    TBytes = "TBytes",
    TChain_id = "TChain_id",
    TInt = "TInt",
    TKey = "TKey",
    TKey_hash = "TKey_hash",
    TLambda = "TLambda",
    TList = "TList",
    TMap = "TMap",
    TMutez = "TMutez",
    TNat = "TNat",
    TNever = "TNever",
    TOption = "TOption",
    TContract = "TContract",
    TTuple = "TTuple",
    TOperation = "TOperation",
    TSet = "TSet",
    TSignature = "TSignature",
    TString = "TString",
    TTimestamp = "TTimestamp",
    TUnit = "TUnit",
    TRecord = "TRecord",
    TVariant = "TVariant",
    TIntOrNat = "TIntOrNat",
    TFunction = "TFunction",
    TUnknown = "TUnknown"
}
export declare enum BackendType {
    address = "address",
    big_map = "big_map",
    bls12_381_fr = "bls12_381_fr",
    bls12_381_g1 = "bls12_381_g1",
    bls12_381_g2 = "bls12_381_g2",
    bool = "bool",
    bytes = "bytes",
    chain_id = "chain_id",
    int = "int",
    key = "key",
    key_hash = "key_hash",
    lambda = "lambda",
    list = "list",
    map = "map",
    mutez = "mutez",
    nat = "nat",
    never = "never",
    option = "option",
    contract = "contract",
    tuple = "tuple",
    operation = "operation",
    set = "set",
    signature = "signature",
    string = "string",
    timestamp = "timestamp",
    unit = "unit",
    record = "record",
    variant = "variant",
    intOrNat = "intOrNat",
    unknown = "unknown"
}
export declare const backendOfFrontend: Record<FrontendType, BackendType>;
export declare enum Layout {
    right_comb = "right_comb"
}
declare const _default: {
    backendOfFrontend: Record<FrontendType, BackendType>;
    BackendType: typeof BackendType;
    FrontendType: typeof FrontendType;
    Layout: typeof Layout;
};
export default _default;
