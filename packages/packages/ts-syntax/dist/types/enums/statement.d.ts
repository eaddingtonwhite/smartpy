export declare enum ST_StatementKind {
    Expression = "Expression",
    VariableStatement = "VariableStatement",
    IfStatement = "IfStatement",
    ForStatement = "ForStatement",
    ForOfStatement = "ForOfStatement",
    WhileStatement = "WhileStatement",
    SwitchStatement = "SwitchStatement",
    Bind = "Bind",
    Result = "Result"
}
export declare enum ST_VariableValueKind {
    Literal = "Literal",
    PropertyPath = "PropertyPath"
}
export declare enum ST_StatementKeywords {
    forGroup = "forGroup",
    whileBlock = "whileBlock",
    ifBlock = "ifBlock",
    elseBlock = "elseBlock"
}
