export declare enum ST_ScopeKind {
    Test = "Test",
    Global = "Global",
    Class = "Class",
    Method = "Method",
    Generic = "Generic"
}
