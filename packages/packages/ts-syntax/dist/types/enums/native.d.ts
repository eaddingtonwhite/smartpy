import type { ST_TypeDef } from '../@types/type';
import { ST_ExpressionKind } from './expression';
import { FrontendType } from './type';
export declare enum ST_Enum {
    Layout = "Layout"
}
export declare enum ST_Namespace {
    Sp = "Sp",
    Dev = "Dev",
    Scenario = "Scenario"
}
export declare enum ST_NamespaceDevMethod {
    compileContract = "compileContract",
    test = "test"
}
export declare enum ST_NamespaceScenarioMethod {
    h1 = "h1",
    h2 = "h2",
    h3 = "h3",
    h4 = "h4",
    p = "p",
    tableOfContents = "tableOfContents",
    show = "show",
    originate = "originate",
    transfer = "transfer",
    verify = "verify",
    verifyEqual = "verifyEqual",
    testAccount = "testAccount",
    makeSignature = "makeSignature"
}
export declare enum ST_NamespaceSpMethod {
    bigMap = "bigMap",
    some = "some",
    ediv = "ediv",
    never = "never",
    verify = "verify",
    verifyEqual = "verifyEqual",
    failWith = "failWith",
    transfer = "transfer",
    unpack = "unpack",
    pack = "pack",
    variant = "variant",
    selfEntryPoint = "selfEntryPoint",
    contract = "contract",
    toAddress = "toAddress",
    createContract = "createContract",
    createContractOperation = "createContractOperation",
    votingPower = "votingPower",
    setDelegate = "setDelegate",
    implicitAccount = "implicitAccount",
    hashKey = "hashKey",
    concat = "concat",
    pairingCheck = "pairingCheck",
    checkSignature = "checkSignature",
    blake2b = "blake2b",
    sha256 = "sha256",
    sha512 = "sha512",
    sha3 = "sha3",
    keccak = "keccak"
}
export declare enum ST_SpValue {
    unit = "unit",
    none = "none",
    sender = "sender",
    source = "source",
    amount = "amount",
    balance = "balance",
    chainId = "chainId",
    level = "level",
    totalVotingPower = "totalVotingPower",
    now = "now",
    self = "self",
    selfAddress = "selfAddress"
}
export declare const ST_KeywordSpValue: {
    unit: string;
    none: string;
    sender: string;
    source: string;
    amount: string;
    balance: string;
    chainId: string;
    level: string;
    totalVotingPower: string;
    now: string;
    self: string;
    selfAddress: string;
};
export declare const ST_NativeMethod: {
    Sp: typeof ST_NamespaceSpMethod;
    Dev: typeof ST_NamespaceDevMethod;
    Scenario: typeof ST_NamespaceScenarioMethod;
};
export declare const SpValue: Record<ST_Namespace.Sp, Record<ST_SpValue, string>>;
export declare const TypeOfNativeValue: Record<ST_Namespace.Sp, Record<ST_SpValue, ST_TypeDef>>;
export declare const ArgTypesOfSpMethod: Record<ST_NamespaceSpMethod, (ST_TypeDef & {
    optional: boolean;
})[]>;
export declare const ArgTypesOfDevMethod: Record<ST_NamespaceDevMethod, (ST_TypeDef & {
    optional: boolean;
})[]>;
export declare const ArgTypesOfScenarioMethod: Record<ST_NamespaceScenarioMethod, (ST_TypeDef & {
    optional: boolean;
})[]>;
export declare const ReturnTypeOfScenarioMethod: Record<ST_NamespaceScenarioMethod, ST_TypeDef>;
export declare const TypeOfScenarioMethod: Record<ST_NamespaceScenarioMethod, {
    argTypes: (ST_TypeDef & {
        optional: boolean;
    })[];
    outputType: ST_TypeDef;
}>;
export declare const ReturnTypeOfSpMethod: Record<ST_NamespaceSpMethod, ST_TypeDef>;
export declare const ReturnTypeOfNativeMethod: {
    Sp: Record<ST_NamespaceSpMethod, ST_TypeDef>;
    Scenario: Record<ST_NamespaceScenarioMethod, ST_TypeDef>;
};
export declare const ExpressionOfNativeCall: Record<ST_Namespace.Sp, Record<ST_NamespaceSpMethod, ST_ExpressionKind>>;
export declare const NativeCallOfExpression: Record<ST_Namespace.Sp, Record<string, ST_NamespaceSpMethod>>;
export declare const ValueOfEnum: {
    Layout: {
        right_comb: string;
    };
};
export declare enum PropertyMethod {
    hasKey = "hasKey",
    get = "get",
    set = "set",
    remove = "remove",
    size = "size",
    push = "push",
    isSome = "isSome",
    openSome = "openSome",
    fst = "fst",
    snd = "snd",
    plus = "plus",
    minus = "minus",
    negate = "negate",
    toInt = "toInt",
    toNat = "toNat",
    multiply = "multiply",
    concat = "concat",
    slice = "slice",
    add = "add",
    contains = "contains",
    elements = "elements",
    reverse = "reverse",
    keys = "keys",
    values = "values",
    entries = "entries",
    isVariant = "isVariant",
    openVariant = "openVariant",
    addSeconds = "addSeconds",
    addMinutes = "addMinutes",
    addHours = "addHours",
    addDays = "addDays"
}
export declare const MethodsByType: Record<FrontendType, PropertyMethod[]>;
export declare const ExpressionByMethod: Record<PropertyMethod, ST_ExpressionKind>;
export declare const EntryPointCallOptionTypes: {
    level: ST_TypeDef;
    amount: ST_TypeDef;
    chainId: ST_TypeDef;
    sender: undefined;
    source: undefined;
    now: ST_TypeDef;
    votingPowers: ST_TypeDef;
    exception: undefined;
    valid: ST_TypeDef;
    show: ST_TypeDef;
};
export declare const EntryPointCallOptionNames: {
    level: string;
    amount: string;
    chainId: string;
    sender: string;
    source: string;
    now: string;
    votingPowers: string;
    exception: string;
    valid: string;
    show: string;
};
export declare enum ST_ContractValue {
    balance = "balance",
    address = "address",
    baker = "baker",
    storage = "storage",
    typed = "typed"
}
