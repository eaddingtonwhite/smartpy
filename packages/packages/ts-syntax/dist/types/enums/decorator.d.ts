export declare enum ST_DecoratorKind {
    Contract = "Contract",
    EntryPoint = "EntryPoint",
    OffChainView = "OffChainView",
    MetadataBuilder = "MetadataBuilder",
    GlobalLambda = "GlobalLambda",
    Inline = "Inline"
}
export declare enum ST_ContractDecoratorProp {
    flags = "flags"
}
export declare enum ST_EntryPointDecoratorProp {
    name = "name",
    lazy = "lazy",
    lazy_no_code = "lazy_no_code",
    mock = "mock"
}
export declare enum ST_OffChainViewDecoratorProp {
    name = "name",
    pure = "pure",
    description = "description"
}
export declare enum ST_GlobalLambdaDecoratorProp {
    pure = "pure"
}
