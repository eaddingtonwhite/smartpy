interface TStorage {
    storedValue: TNat;
}

type ReplaceParams = {
    value: TNat;
};

type DivideParams = {
    divisor: TNat;
};

@Contract
class StoreValue {
    storage: TStorage = {
        storedValue: 1,
    };

    @EntryPoint
    replace(params: ReplaceParams) {
        this.storage.storedValue = params.value;
    }

    @EntryPoint
    double() {
        this.storage.storedValue *= 2;
    }

    @EntryPoint
    divide(params: DivideParams) {
        this.storage.storedValue /= params.divisor;
    }
}
