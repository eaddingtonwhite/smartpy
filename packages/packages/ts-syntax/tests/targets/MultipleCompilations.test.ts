import Transpiler from '../../src/lib/Transpiler';
import { getCode } from '../loadTemplate';

const templateName = 'MultipleCompilations';

test(templateName, async () => {
    const code = getCode(`./tests/targets/${templateName}`);
    const transpiler = new Transpiler({
        name: 'code.ts',
        code,
    });
    await transpiler.transpile();

    // Snapshot the result
    expect(transpiler.output.result).toMatchSnapshot();

    // Snapshot the S-expression result
    expect(transpiler.output.getResult()).toMatchSnapshot('Transpilation Result');
});
