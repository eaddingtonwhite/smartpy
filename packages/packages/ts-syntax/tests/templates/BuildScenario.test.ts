import * as fs from 'fs';
import { getCode } from '../loadTemplate';
import Transpiler from '../../src/lib/Transpiler';

// Change the following regex to filter the templates being tested
const TEMPLATE_FILTER = /^\w*[.]ts/g;

describe('Build scenarios for templates', () => {
    const files = fs.readdirSync('./templates', { encoding: 'utf-8' });

    files
        .filter((f) => f.match(TEMPLATE_FILTER))
        .map(async (fileName) => {
            test(`Template ${fileName}`, async () => {
                try {
                    const code = getCode(`./templates/${fileName.replace('.ts', '')}`);
                    const transpiler = new Transpiler({
                        baseDir: './templates/',
                        name: fileName,
                        code,
                    });
                    await transpiler.transpile();

                    // Snapshot the S-expression result
                    expect(transpiler.output.getResult()).toMatchSnapshot(`Transpilation Result (${fileName})`);
                } catch (e) {
                    if (process.env.TRACE_ERRORS) {
                        console.trace(e.stderr?.toString() || e);
                    }
                    fail(`Failed to generate scenario for template ${fileName}.\n\n${e} (${e?.snippet || ''})`);
                }
            });
        });
});
