import smartpy as sp

tstorage = sp.TRecord(admins = sp.TSet(sp.TAddress)).layout("admins")
tparameter = sp.TVariant(add = sp.TSet(sp.TAddress), changeAdmins = sp.TRecord(add = sp.TOption(sp.TSet(sp.TAddress)), remove = sp.TOption(sp.TSet(sp.TAddress))).layout(("add", "remove")), remove = sp.TSet(sp.TAddress)).layout(("add", ("changeAdmins", "remove")))
tglobals = { }
tviews = { }
