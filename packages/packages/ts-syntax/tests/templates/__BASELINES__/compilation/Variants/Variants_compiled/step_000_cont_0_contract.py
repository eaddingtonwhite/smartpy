import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(value = sp.TOption(sp.TNat)).layout("value"))
    self.init(value = sp.none)

  @sp.entry_point
  def switchCase1(self, params):
    sp.set_type(params, sp.TVariant(action1 = sp.TString, action2 = sp.TNat).layout(("action1", "action2")))
    with params.match_cases() as arg:
      with arg.match('action1') as action1:
        sp.verify(action1 == 'A String', 'Expected value to be ('A String').')
      with arg.match('action2') as action2:
        sp.verify(action2 == 10, 'Expected value to be (10).')

    sp.if params.is_variant('action2'):
      self.data.value = sp.some(params.open_variant('action2'))

  @sp.entry_point
  def switchCase2(self, params):
    sp.set_type(params, sp.TVariant(action1 = sp.TString, action2 = sp.TNat).layout(("action1", "action2")))
    with params.match_cases() as arg:
      with arg.match('action1') as action1:
        sp.verify(action1 == 'A String', 'Expected value to be ('A String').')
      with arg.match('action2') as action2:
        sp.verify(action2 == 10, 'Expected value to be (10).')

    sp.if params.is_variant('action2'):
      self.data.value = sp.some(params.open_variant('action2'))

  @sp.entry_point
  def switchCase3(self, params):
    sp.set_type(params, sp.TVariant(action1 = sp.TString, action2 = sp.TNat).layout(("action1", "action2")))
    with params.match_cases() as arg:
      with arg.match('action1') as action1:
        sp.verify(action1 == 'A String', 'Expected value to be ('A String').')
      with arg.match('action2') as action2:
        sp.verify(action2 == 10, 'Expected value to be (10).')

    sp.if params.is_variant('action2'):
      self.data.value = sp.some(params.open_variant('action2'))