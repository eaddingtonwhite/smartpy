import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(decrement = sp.TLambda(sp.TNat, sp.TInt), increment = sp.TLambda(sp.TNat, sp.TNat), storedValue = sp.TNat, transformer = sp.TLambda(sp.TNat, sp.TNat)).layout((("decrement", "increment"), ("storedValue", "transformer"))))
    self.init(decrement = lambda(sp.TLambda(sp.TNat, sp.TInt)),
              increment = lambda(sp.TLambda(sp.TNat, sp.TNat)),
              storedValue = 1,
              transformer = lambda(sp.TLambda(sp.TNat, sp.TNat)))

  @sp.entry_point
  def store(self, params):
    sp.set_type(params.value, sp.TNat)
    sp.set_type(params.transformer2, sp.TLambda(sp.TNat, sp.TNat))
    def f6(lparams_6):
      sp.if lparams_6 > 5:
        sp.result(2)
      sp.else:
        sp.result(lparams_6)
    transformer3 = sp.local("transformer3", sp.build_lambda(f6))
    self.data.storedValue = self.data.increment(params.value)
    self.data.storedValue = self.data.transformer(params.value)
    def f0(lparams_0):
      sp.if lparams_0 > 5:
        sp.result(1)
      sp.else:
        sp.result(lparams_0)
    self.data.storedValue = sp.build_lambda(f0)(params.value)
    self.data.storedValue = params.transformer2(params.value)
    self.data.storedValue = transformer3.value(params.value)
    self.data.storedValue = self.transformer4(params.value)
    var_0 = sp.local("var_0", self.transformer5(sp.record(in_param = params.value, in_storage = self.data)))
    self.data = var_0.value.storage
    sp.for op in var_0.value.operations.rev():
      sp.operations().push(op)
    self.data.storedValue = var_0.value.result
    self.data.storedValue = sp.as_nat(self.data.decrement(params.value))