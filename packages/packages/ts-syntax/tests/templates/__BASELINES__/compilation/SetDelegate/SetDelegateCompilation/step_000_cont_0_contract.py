import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(currentDelegate = sp.TOption(sp.TKeyHash), nominatedDelegates = sp.TSet(sp.TKeyHash)).layout(("currentDelegate", "nominatedDelegates")))
    self.init(currentDelegate = sp.some(sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w')),
              nominatedDelegates = sp.set([]))

  @sp.entry_point
  def testDelegate(self, params):
    sp.set_type(params, sp.TKeyHash)
    sp.if sp.some(params) != self.data.currentDelegate:
      sp.set_delegate(sp.some(params))
      sp.set_delegate(self.data.currentDelegate)