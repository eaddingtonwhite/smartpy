import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def ep(self, params):
    sp.set_type(params, sp.TKeyHash)
    sp.verify(sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w') == sp.to_address(sp.implicit_account(params)))