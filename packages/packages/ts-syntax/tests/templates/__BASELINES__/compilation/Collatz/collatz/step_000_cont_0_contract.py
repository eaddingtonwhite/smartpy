import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(counter = sp.TNat, onEven = sp.TAddress, onOdd = sp.TAddress).layout(("counter", ("onEven", "onOdd"))))
    self.init(counter = 0,
              onEven = sp.address('KT1QEaMVhcGvnf31cmWN4YWcujfzwhEQqX8c'),
              onOdd = sp.address('KT1CbRd63JiSrZsVEJzmXAmA5ACNvpqYTGbM'))

  @sp.entry_point
  def run(self, params):
    sp.set_type(params, sp.TNat)
    sp.if params > 1:
      self.data.counter += 1
      params = sp.local("params", sp.record(k = sp.self_entry_point('run'), param = params))
      sp.if (params % 2) == 0:
        sp.transfer(params.value, sp.tez(0), sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), param = sp.TNat).layout(("k", "param")), self.data.onEven).open_some())
      sp.else:
        sp.transfer(params.value, sp.tez(0), sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), param = sp.TNat).layout(("k", "param")), self.data.onOdd).open_some())

  @sp.entry_point
  def reset(self):
    self.data.counter = 0