import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def ep(self, params):
    sp.set_type(params, sp.TKey)
    sp.verify(sp.key_hash('tz1Xv78KMT7cHyVDLi9RmQP1KuWULHDafZdK') == sp.hash_key(params))