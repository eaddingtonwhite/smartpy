import smartpy as sp

tstorage = sp.TInt
tparameter = sp.TUnit
tglobals = { }
tviews = { "getStorageState": ((), sp.TInt), "increment": ((), sp.TInt), "decrement": ((), sp.TInt) }
