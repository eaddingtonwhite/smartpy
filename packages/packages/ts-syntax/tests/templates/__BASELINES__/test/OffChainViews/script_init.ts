@Contract
export class OffChainViews {
    constructor(public storage: TInt = /* Default value */ 1) {}

    @OffChainView({ name: 'getStorageState', description: 'Get Storage State' })
    state = () => {
        return this.storage;
    };

    @OffChainView({ pure: true, description: 'Increment value' })
    increment = () => {
        return this.storage + 1;
    };

    @OffChainView({ pure: true, description: 'Decrement value' })
    decrement = () => {
        return this.storage - 1;
    };

    @MetadataBuilder
    metadata = {
        name: 'SOME CONTRACT',
        decimals: 1,
        views: [this.state, this.increment, this.decrement],
        preferSymbol: true,
        source: {
            tools: ['SmartPy'],
        },
    };
}

Dev.test({ name: 'OffChainViews' }, () => {
    Scenario.originate(new OffChainViews());
});

Dev.compileContract('compilation', new OffChainViews(10));
