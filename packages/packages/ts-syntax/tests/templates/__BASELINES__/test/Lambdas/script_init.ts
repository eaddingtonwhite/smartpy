interface TStorage {
    storedValue: TNat;
    transformer: TLambda<TNat, TNat>;
    increment: TLambda<TNat, TNat>;
    decrement: TLambda<TNat, TInt>;
}

const transformer: TLambda<TNat, TNat> = (value: TNat): TNat => {
    if (value > 5) {
        return 1;
    } else {
        return value;
    }
};

@Contract
export class Lambdas {
    storage: TStorage = {
        storedValue: 1,
        transformer: transformer,
        increment: (value: TNat) => {
            return value + 1;
        },
        decrement: (value: TNat): TInt => value - 1,
    };

    @EntryPoint
    store(value: TNat, transformer2: TLambda<TNat, TNat>): void {
        const transformer3: TLambda<TNat, TNat> = (value: TNat): TNat => {
            if (value > 5) {
                return 2;
            } else {
                return value;
            }
        };
        this.storage.storedValue = this.storage.increment(value);
        this.storage.storedValue = this.storage.transformer(value);
        this.storage.storedValue = transformer(value);
        this.storage.storedValue = transformer2(value);
        this.storage.storedValue = transformer3(value);
        this.storage.storedValue = this.transformer4(value);
        this.storage.storedValue = this.transformer5(value);
        this.storage.storedValue = this.storage.decrement(value) as TNat;
    }

    @GlobalLambda
    transformer4 = (value: TNat): TNat => {
        return value + 5;
    };

    @GlobalLambda({ pure: false })
    transformer5 = (value: TNat): TNat => {
        return value + this.storage.storedValue;
    };
}

Dev.test({ name: 'Lambdas' }, () => {
    const c1 = Scenario.originate(new Lambdas());
    Scenario.transfer(c1.store(1, (v: TNat) => v + 1));
});

Dev.compileContract('compile_contract', new Lambdas());
