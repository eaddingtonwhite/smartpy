import smartpy as sp

tstorage = sp.TRecord(decrement = sp.TLambda(sp.TNat, sp.TInt), increment = sp.TLambda(sp.TNat, sp.TNat), storedValue = sp.TNat, transformer = sp.TLambda(sp.TNat, sp.TNat)).layout((("decrement", "increment"), ("storedValue", "transformer")))
tparameter = sp.TVariant(store = sp.TRecord(transformer2 = sp.TLambda(sp.TNat, sp.TNat), value = sp.TNat).layout(("transformer2", "value"))).layout("store")
tglobals = { "transformer4": sp.TLambda(sp.TNat, sp.TNat), "transformer5": sp.TLambda(sp.TRecord(in_param = sp.TNat, in_storage = sp.TRecord(decrement = sp.TLambda(sp.TNat, sp.TInt), increment = sp.TLambda(sp.TNat, sp.TNat), storedValue = sp.TNat, transformer = sp.TLambda(sp.TNat, sp.TNat)).layout((("decrement", "increment"), ("storedValue", "transformer")))).layout(("in_param", "in_storage")), sp.TRecord(operations = sp.TList(sp.TOperation), result = sp.TNat, storage = sp.TRecord(decrement = sp.TLambda(sp.TNat, sp.TInt), increment = sp.TLambda(sp.TNat, sp.TNat), storedValue = sp.TNat, transformer = sp.TLambda(sp.TNat, sp.TNat)).layout((("decrement", "increment"), ("storedValue", "transformer")))).layout(("operations", ("result", "storage")))) }
tviews = { }
