interface AllTypesStorage {
    address: TAddress;
    big_map: TBig_map<TNat, TBool>;
    bls12_381_fr: TBls12_381_fr;
    bls12_381_g1: TBls12_381_g1;
    bls12_381_g2: TBls12_381_g2;
    bool: TBool;
    bytes: TBytes;
    chain_id: TChain_id;
    contract: TContract<TNat>;
    int: TInt;
    key: TKey;
    key_hash: TKey_hash;
    lambda: TLambda<TNat, TNat>;
    list: TList<TString>;
    map: TMap<TInt, TString>;
    mutez: TMutez;
    nat: TNat;
    option: TOption<TAddress>;
    option2: TOption<TNat>;
    tuple: TTuple<[TNat, TNat, TNat]>;
    // sapling_state n
    // sapling_transaction n
    set: TSet<TNat>;
    signature: TSignature;
    string: TString;
    //ticket cty
    timestamp: TTimestamp;
    unit: TUnit;
    record: TRecord<
        {
            prop1: TInt;
            prop2: TString;
            prop3: TNat;
        },
        ['prop1', 'prop2', 'prop3']
    >;
    variant: TVariant<{ kind: 'A'; value: TString } | { kind: 'B'; value: TNat }, Layout.right_comb>;
}

export const storage: AllTypesStorage = {
    address: 'tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w',
    big_map: [[0, true]],
    bls12_381_fr: 1,
    bls12_381_g1: '0x01',
    bls12_381_g2: '0x00',
    bool: true,
    bytes: '0x00',
    chain_id: '0xeda',
    contract: 'tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w%foo',
    int: 1,
    key: 'edpk',
    key_hash: 'tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w',
    lambda: (value: TNat) => {
        return value + 1;
    },
    list: ['A String'],
    map: [[1, 'Something']],
    mutez: 1,
    nat: 0,
    // operation
    option: Sp.some<TAddress>('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
    option2: Sp.none,
    // or ty1 ty2
    tuple: [0, 1, 3],
    // sapling_state n
    // sapling_transaction n
    set: [1],
    signature: 'sig',
    string: 'A new String',
    // ticket cty
    timestamp: 1571659294,
    unit: Sp.unit,
    record: {
        prop1: 1,
        prop2: '2',
        prop3: 3,
    },
    variant: { kind: 'A', value: 'TEST' },
};

@Contract
class AllTypes {
    constructor(public storage: AllTypesStorage = storage) {}
}

Dev.test({ name: 'AllTypes' }, () => {
    Scenario.originate(new AllTypes(storage));
});

Dev.compileContract('compile_contract', new AllTypes(storage));
