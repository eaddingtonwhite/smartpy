import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(address = sp.TAddress, big_map = sp.TBigMap(sp.TNat, sp.TBool), bls12_381_fr = sp.TBls12_381_fr, bls12_381_g1 = sp.TBls12_381_g1, bls12_381_g2 = sp.TBls12_381_g2, bool = sp.TBool, bytes = sp.TBytes, chain_id = sp.TChainId, contract = sp.TContract(sp.TNat), int = sp.TInt, key = sp.TKey, key_hash = sp.TKeyHash, lambda = sp.TLambda(sp.TNat, sp.TNat), list = sp.TList(sp.TString), map = sp.TMap(sp.TInt, sp.TString), mutez = sp.TMutez, nat = sp.TNat, option = sp.TOption(sp.TAddress), option2 = sp.TOption(sp.TNat), record = sp.TRecord(prop1 = sp.TInt, prop2 = sp.TString, prop3 = sp.TNat).layout((("prop1", "prop2"), "prop3")), set = sp.TSet(sp.TNat), signature = sp.TSignature, string = sp.TString, timestamp = sp.TTimestamp, tuple = sp.TTuple(sp.TNat, sp.TNat, sp.TNat), unit = sp.TUnit, variant = sp.TVariant(A = sp.TString, B = sp.TNat).layout(("A", "B"))).layout((((("address", ("big_map", "bls12_381_fr")), ("bls12_381_g1", ("bls12_381_g2", "bool"))), (("bytes", ("chain_id", "contract")), (("int", "key"), ("key_hash", "lambda")))), ((("list", ("map", "mutez")), (("nat", "option"), ("option2", "record"))), (("set", ("signature", "string")), (("timestamp", "tuple"), ("unit", "variant")))))))
    self.init(address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
              big_map = {0 : True},
              bls12_381_fr = sp.bls12_381_fr('0x01'),
              bls12_381_g1 = sp.bls12_381_g1('0x01'),
              bls12_381_g2 = sp.bls12_381_g2('0x00'),
              bool = True,
              bytes = sp.bytes('0x00'),
              chain_id = sp.chain_id('0x0eda'),
              contract = sp.contract(sp.TNat, sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w%foo')).open_some(),
              int = 1,
              key = sp.key('edpk'),
              key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'),
              lambda = lambda(sp.TLambda(sp.TNat, sp.TNat)),
              list = sp.list(['A String']),
              map = {1 : 'Something'},
              mutez = sp.mutez(1),
              nat = 0,
              option = sp.some(sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w')),
              option2 = sp.none,
              record = sp.record(prop1 = 1, prop2 = '2', prop3 = 3),
              set = sp.set([1]),
              signature = sp.signature('sig'),
              string = 'A new String',
              timestamp = sp.timestamp(1571659294),
              tuple = (0, 1, 3),
              unit = sp.unit,
              variant = A('TEST'))