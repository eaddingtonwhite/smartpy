interface TStorage {
    timestamp: TTimestamp;
}

@Contract
export class Timestamp {
    storage: TStorage = {
        timestamp: '2021-07-26T12:35:28.504Z',
    };

    @EntryPoint
    addTime(): void {
        this.storage.timestamp = this.storage.timestamp.addSeconds(1);
        this.storage.timestamp = this.storage.timestamp.addMinutes(1);
        this.storage.timestamp = this.storage.timestamp.addHours(1);
        this.storage.timestamp = this.storage.timestamp.addDays(1);

        Sp.verify(Sp.now.minus(this.storage.timestamp) > 0, 'Sp.now.minus(this.storage.timestamp) > 0');
    }
}

Dev.test({ name: 'Timestamp' }, () => {
    const c1 = Scenario.originate(new Timestamp());
    Scenario.transfer(c1.addTime(), { now: '2022-07-26T12:35:28.504Z' });
});

Dev.compileContract('Timestamp_compiled', new Timestamp());
