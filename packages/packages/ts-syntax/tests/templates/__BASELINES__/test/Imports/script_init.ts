import * as LocalModule from './Minimal';
import * as RemoteModule from 'https://smartpy.io/typescript/templates/Minimal.ts';

@Contract
class LocalImport extends LocalModule.Minimal {
    @EntryPoint
    setValue(value: TNat) {
        this.storage.value = value;
    }
}

@Contract
class RemoteImport extends RemoteModule.Minimal {
    @EntryPoint
    setValue(value: TNat) {
        this.storage.value = value;
    }
}

Dev.test({ name: 'LocalImport' }, () => {
    const c1 = Scenario.originate(new LocalImport());
    Scenario.transfer(c1.setValue(1));
});

Dev.test({ name: 'RemoteImport' }, () => {
    const c1 = Scenario.originate(new RemoteImport());
    Scenario.transfer(c1.setValue(1));
});

Dev.compileContract('compile_LocalImport', new LocalImport());
Dev.compileContract('compile_RemoteImport', new RemoteImport());
