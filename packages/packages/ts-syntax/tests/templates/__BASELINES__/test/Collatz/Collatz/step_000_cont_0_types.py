import smartpy as sp

tstorage = sp.TRecord(counter = sp.TNat, onEven = sp.TAddress, onOdd = sp.TAddress).layout(("counter", ("onEven", "onOdd")))
tparameter = sp.TVariant(reset = sp.TUnit, run = sp.TNat).layout(("reset", "run"))
tglobals = { }
tviews = { }
