import * as fs from 'fs';
import { execSync } from 'child_process';

const SCRIPT_PATH = '../../../smartpy-cli/SmartPy.sh';
const BASELINES_PATH = './tests/templates/__BASELINES__/test/';

// Change the following regex to filter the templates being tested
const TEMPLATE_FILTER = /^\w*[.]ts/g;

beforeAll(() => {
    if (fs.existsSync(BASELINES_PATH)) {
        fs.rmSync(BASELINES_PATH, { recursive: true });
    }
    fs.mkdirSync(BASELINES_PATH, { recursive: true });
});

describe('Test templates', () => {
    const files = fs.readdirSync('./templates', { encoding: 'utf-8' });

    files
        .filter((f) => f.match(TEMPLATE_FILTER))
        .map(async (fileName) => {
            test(`Template ${fileName}`, async () => {
                try {
                    if (!process.env.SKIP_CLI_BASELINES) {
                        const dir = `${BASELINES_PATH}${fileName.replace('.ts', '')}`;
                        fs.mkdirSync(dir, { recursive: true });
                        execSync(`${SCRIPT_PATH} test ./templates/${fileName} ${dir}`);
                    }
                } catch (e) {
                    if (process.env.TRACE_ERRORS) {
                        console.trace(e.stderr?.toString() || e);
                    }
                    fail(`Failed when testing template ${fileName}.\n\n${e.stdout?.toString() || e}`);
                }
            });
        });
});
