import { FrontendType } from '../../../src/enums/type';
import Transpiler from '../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../ExpectAnyLine';

test(FrontendType.TIntOrNat, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type number_type = number;`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        number_type: {
            line: ExpectAnyLine,
            type: FrontendType.TIntOrNat,
        },
    });
});
