import { FrontendType } from '../../../src/enums/type';
import Transpiler from '../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../ExpectAnyLine';

test(FrontendType.TSignature, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type signature_type = ${FrontendType.TSignature};`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        signature_type: {
            line: ExpectAnyLine,
            type: FrontendType.TSignature,
        },
    });
});
