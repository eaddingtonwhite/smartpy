import { FrontendType } from '../../../../src/enums/type';
import Transpiler from '../../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../../ExpectAnyLine';

describe(`${FrontendType.TRecord}`, () => {
    test('(Simple 1)', async () => {
        const sourceCode = {
            name: 'code.ts',
            code: `​type record__type = ${FrontendType.TRecord};`,
        };
        const transpiler = new Transpiler(sourceCode);
        await transpiler.transpile();

        expect(transpiler.output.globalTypeDefs).toEqual({
            record__type: {
                type: FrontendType.TRecord,
                properties: {},
                layout: [],
                line: ExpectAnyLine,
            },
        });
    });

    test('(Simple 2)', async () => {
        const sourceCode = {
            name: 'code.ts',
            code: `type record__type = {};`,
        };
        const transpiler = new Transpiler(sourceCode);
        await transpiler.transpile();

        expect(transpiler.output.globalTypeDefs).toEqual({
            record__type: {
                type: FrontendType.TRecord,
                line: ExpectAnyLine,
                properties: {},
            },
        });
    });

    test('Untyped property (Must generate errors)', async () => {
        const sourceCode = {
            name: 'code.ts',
            code: `
            ​type record__type = {
                a
            };
            `,
        };
        const transpiler = new Transpiler(sourceCode);

        expect(transpiler.transpile).rejects.toThrow();
    });

    test(`${FrontendType.TRecord} of { a: ${FrontendType.TMap}<${FrontendType.TNat}, ${FrontendType.TInt}> }`, async () => {
        const sourceCode = {
            name: 'code.ts',
            code: `
            ​type record__type = {
                a: ${FrontendType.TMap}<${FrontendType.TNat}, ${FrontendType.TInt}>
            };
            `,
        };
        const transpiler = new Transpiler(sourceCode);
        await transpiler.transpile();

        expect(transpiler.output.globalTypeDefs).toEqual({
            record__type: {
                type: FrontendType.TRecord,
                line: ExpectAnyLine,
                properties: {
                    a: {
                        line: ExpectAnyLine,
                        type: FrontendType.TMap,
                        keyType: {
                            type: FrontendType.TNat,
                            line: ExpectAnyLine,
                        },
                        valueType: {
                            type: FrontendType.TInt,
                            line: ExpectAnyLine,
                        },
                    },
                },
            },
        });
    });
});
