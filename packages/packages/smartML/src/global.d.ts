import type Bls12 from 'tezos-bls12-381';
import type Eztz from './polyfills/eztz';

declare global {
    var eztz: typeof Eztz;
    var smartpyContext: SmartpyContext;
}

interface SmartpyContext {
    Bls12: typeof Bls12;
}
