import { TezosToolkit } from '@taquito/taquito';
import { TransactionOperation } from '@taquito/taquito/dist/types/operations/transaction-operation';
export declare function invokeContractWithParameter(tezos: TezosToolkit, address: string, parameter: object, entrypoint: string, amount?: number): Promise<TransactionOperation>;
declare const _default: {
    invokeContractWithParameter: typeof invokeContractWithParameter;
};
export default _default;
