import { TezosToolkit } from '@taquito/taquito';
declare const _default: {
    getWallet: (tezosNode: string, secretKey: string) => Promise<TezosToolkit>;
};
export default _default;
