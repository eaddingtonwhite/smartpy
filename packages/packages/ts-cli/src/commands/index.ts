import Commander from 'commander';
import * as fs from 'fs';
import SmartTS, { setFetchFromDisk, setFetchFromURL } from '@smartpy/ts-syntax';
import axios from 'axios';

const fetchFromURL = async (filePath: string): Promise<string> => (await axios.get(filePath)).data;

const fetchFromDisk = async (filePath: string): Promise<string> => {
    const fs = await import('fs');
    return fs.readFileSync(filePath, { encoding: 'utf-8', flag: 'r' });
};

setFetchFromURL(fetchFromURL);
setFetchFromDisk(fetchFromDisk);

/**
 * @description Apply file validation
 * @param filePath file (relative or absolute) path
 */
const fileValidation = (filePath: string): string => {
    if (!filePath.endsWith('.ts')) {
        process.stderr.write(`Expected file "${filePath}" to have (.ts) extension.`);
    }
    if (!fs.existsSync(filePath)) {
        process.stderr.write(`File "${filePath}" doesn't exist.`);
    }

    return filePath.split('/').reverse()[0];
};

Commander.command('scenario')
    .requiredOption('--file <path>')
    .requiredOption('--outDir <path>')
    .action(async (args) => {
        const filePath: string = args.file;
        let outDir: string = args.outDir;

        // Remove leading slash
        if (outDir.endsWith('/')) {
            outDir = outDir.slice(0, outDir.lastIndexOf('/'));
        }

        // Apply file validations
        const fileName = fileValidation(filePath);

        try {
            // Read file
            const code = fs.readFileSync(filePath, { encoding: 'utf8', flag: 'r' });

            // Transpile file
            const scenario = await SmartTS.transpile({
                baseDir: filePath.replace(fileName, ''),
                name: fileName,
                code,
            });

            // Write the result into a json file.
            const outputPath = `${outDir}/scenario.json`;
            fs.writeFileSync(outputPath, JSON.stringify(scenario, null, 4), { encoding: 'utf-8', flag: 'w' });

            // Write to the user
            console.log();
            console.log(`Generated (${fileName}) scenario at (${outputPath}).`);
            console.log();
        } catch (e) {
            console.error(e);
        }
    });

export default Commander;
