# Create SmartPy projects

This starter kit is used to bootstrap smartpy projects.

## Bootstrap a project for `typescript` syntax.

### npx
```console
npx create-smartpy-project@latest <project-directory> --typescript
```

### npm
```console
npm init smartpy-project@latest <project-directory> --typescript
```

### yarn
```console
yarn create smartpy-project@latest <project-directory> --typescript
```

## Update boilerplate

### npx
```console
npx create-smartpy-project@latest update --path <project-directory> --typescript
```

### npm
```console
npm exec creat-smartpy-project@latest -- update --path <project-directory> --typescript
```

### yarn
```console
yarn exec create-smartpy-project@latest -- update --path <project-directory> --typescript
```


## Development Info

Edit the boilerplates at: [boilerplates](./boilerplates)

### Package boilerplates
```console
npm run bundle
```
