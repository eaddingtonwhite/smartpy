import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) =>
    createStyles({
        spacingTopTwo: { marginTop: theme.spacing(2) },
        spacingOne: {
            margin: theme.spacing(1),
        },
        spacingTwo: {
            margin: theme.spacing(2),
        },
        noShadow: {
            boxShadow: 'none',
        },
    }),
);
