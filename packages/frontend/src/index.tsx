import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { loader } from '@monaco-editor/react';
import { loadWASM } from 'onigasm'; // peer dependency of 'monaco-textmate'
import darkPlus from './features/editor/themes/darkPlus';
import lightPlus from './features/editor/themes/lightPlus';

import './index.css';
import './features/i18n';
import CircularProgress from './features/loader/components/CircularProgressWithText';
import * as serviceWorker from './serviceWorker';

import { createStore } from './store';
import { getBase } from './utils/url';
import MichelsonLanguage from './features/editor/language/michelson';
import PythonLanguage from './features/editor/language/python';

const { store, persistor, history } = createStore();

const App = React.lazy(() => import('./App'));

// Load monaco static files (IMPORTANT)
loader.config({ paths: { vs: `${getBase()}/static/monaco-editor/0.25.2/min/vs` } });
loader.init().then(async (monaco) => {
    await loadWASM(`${getBase()}/static/wasm/onigasm.wasm`); // See https://www.npmjs.com/package/onigasm#light-it-up
    // monaco's built-in themes aren't powereful enough to handle TM tokens
    // https://github.com/Nishkalkashyap/monaco-vscode-textmate-theme-converter#monaco-vscode-textmate-theme-converter
    monaco.editor.defineTheme('vs-code-dark', darkPlus as any);
    monaco.editor.defineTheme('vs-code-light', lightPlus as any);

    monaco.languages.register({ id: 'ocaml' });
    monaco.languages.register({ id: 'tmPython' });
    monaco.languages.register({ id: 'michelson' });

    monaco.languages.setLanguageConfiguration('michelson', {
        comments: { lineComment: '#', blockComment: ['/*', '*/'] },
    });
    monaco.languages.setLanguageConfiguration('tmPython', {
        comments: { lineComment: '#', blockComment: ['"""', '"""'] },
    });
    monaco.languages.setLanguageConfiguration('ocaml', {
        comments: { blockComment: ['(*', '*)'] },
    });

    // Add michelson auto-completers
    MichelsonLanguage.register(monaco);
    // // Add python auto-completers
    PythonLanguage.register(monaco);

    ReactDOM.render(
        <React.StrictMode>
            <Provider store={store}>
                <PersistGate loading={<CircularProgress loading={true} />} persistor={persistor}>
                    <Suspense fallback={<CircularProgress loading={true} />}>
                        <App history={history} />
                    </Suspense>
                </PersistGate>
            </Provider>
        </React.StrictMode>,
        document.getElementById('root'),
    );
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
