import React from 'react';
import { act } from 'react-dom/test-utils';
import { IDELayout } from 'SmartPyModels';

import Editor from '../../features/mich-ide/views/EditorView';
import renderWithStore from '../test-helpers/renderWithStore';

const props = {
    firstMessage: '',
    htmlOutput: {
        __html: '',
    },
    settings: {
        layout: 'side-by-side' as IDELayout,
    },
    editorRef: React.createRef() as React.RefObject<any>,
    updateContract: (value: string) => null,
    showError: (error: string) => null,
    clearOutputs: () => null,
    contract: '',
};

describe('Michelson Editor Page', () => {
    it('Michelson Editor renders correctly', async () => {
        let container: Element;
        await act(async () => {
            container = renderWithStore(
                <React.Suspense fallback={'...'}>
                    <Editor {...props} />
                </React.Suspense>,
            ).container;

            while (!container.innerHTML.includes('run-code')) {
                // Wait for the content to be available or fail with timeout
                await new Promise((r) => setTimeout(r, 1000));
            }
            expect(container).toMatchSnapshot();
        });
    });
});
