import React from 'react';
import { useDispatch } from 'react-redux';

import { IDEContract } from 'SmartPyModels';

// Material UI
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tooltip from '@material-ui/core/Tooltip';
import Chip from '@material-ui/core/Chip';
import DialogActions from '@material-ui/core/DialogActions';
import Paper from '@material-ui/core/Paper';
// Material UI Icons
import DeleteIcon from '@material-ui/icons/DeleteSweep';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import CompilationIcon from '@material-ui/icons/Description';

// Local Components
import ShareMenuItem from './ShareMenuItem';
import ToolsMenuItem from './ToolsMenuItem';
import PopperWithArrow from './PopperWithArrow';

// Local Hooks
import useCommonStyles from '../../../hooks/useCommonStyles';
import useTranslation from '../../i18n/hooks/useTranslation';
import TestIcon from '../../common/elements/icons/Test';
import { IDENewcomerGuideSteps, SYNTAX } from '../enums/ide';
import { ST_Scenario } from '@smartpy/ts-syntax/dist/types/@types/scenario';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            alignItems: 'center',
            height: 50,
            borderWidth: 1,
            backgroundColor: theme.palette.background.paper,
        },
        gridItem: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
        noMargin: {
            margin: 0,
        },
    }),
);

interface OwnProps {
    clearOutputs: () => void;
    targets?: ({ kind: string; name: string } | ST_Scenario)[];
    downloadOutputPanel: () => void;
    compileContract: (withTests?: boolean) => void;
    runScenario?: (testName: string) => void;
    updateNewcomerGuideStep?: (step: IDENewcomerGuideSteps) => void;
    newcomerGuideStep?: IDENewcomerGuideSteps;
    setVolatileContract: (code: string) => void;
    selectedContract: IDEContract | undefined;
    baseUrl: string;
    templatesMenu: React.ReactElement;
    settingsMenu: React.ReactElement;
    syntax: SYNTAX;
}
const EditorToolBar: React.FC<OwnProps> = (props) => {
    const classes = { ...useStyles(), ...useCommonStyles() };
    const [open, setOpen] = React.useState(false);
    const refs = {
        testsButton: React.useRef<HTMLButtonElement>(null),
        playButton: React.useRef<HTMLButtonElement>(null),
    };
    const onlyIcon = useMediaQuery((theme: Theme) => theme.breakpoints.down('lg'));
    const t = useTranslation();
    const dispatch = useDispatch();

    const {
        clearOutputs,
        targets,
        downloadOutputPanel,
        compileContract,
        runScenario,
        updateNewcomerGuideStep,
        newcomerGuideStep,
        setVolatileContract,
        selectedContract,
        baseUrl,
        templatesMenu,
        settingsMenu,
        syntax,
    } = props;

    const [compileTargets, testTargets, miscTargets] = React.useMemo(
        () =>
            (targets || []).reduce<
                [
                    ({ kind: string; name: string } | ST_Scenario)[],
                    ({ kind: string; name: string } | ST_Scenario)[],
                    { [kind: string]: ({ kind: string; name: string } | ST_Scenario)[] },
                ]
            >(
                (acc, cur) => {
                    if (cur.kind === 'compilation') {
                        acc[0] = [...acc[0], cur];
                    } else if (cur.kind === 'test') {
                        acc[1] = [...acc[1], cur];
                    } else {
                        acc[2] = {
                            ...acc[2],
                            [cur.kind]: [...(acc[2][cur.kind] || []), cur],
                        };
                    }
                    return acc;
                },
                [[], [], {}],
            ),
        [targets],
    );

    const handleToggle = () => {
        setOpen((prevState) => !prevState);
        handleNewcomerInteraction();
    };

    const handleClose = (event: React.MouseEvent<EventTarget>) => {
        if (refs.testsButton.current && refs.testsButton.current.contains(event.target as HTMLElement)) {
            return;
        }
        setOpen(false);
    };

    const handleNewcomerInteraction = () => {
        if (updateNewcomerGuideStep) {
            dispatch(updateNewcomerGuideStep(IDENewcomerGuideSteps.NONE));
        }
    };
    return (
        <div className={classes.root}>
            <Grid container justifyContent="space-between" alignItems="center">
                <Grid item xs={12} sm={6} className={classes.gridItem}>
                    <ButtonGroup disableElevation>
                        <Tooltip title={t('ide.toolBar.runCode') as string} aria-label="run-code" placement="left">
                            <Button
                                ref={refs.playButton}
                                onClick={() => compileContract(true)}
                                endIcon={onlyIcon ? null : <PlayArrowIcon />}
                                variant="contained"
                            >
                                {onlyIcon ? <PlayArrowIcon /> : 'Run'}
                            </Button>
                        </Tooltip>
                        {targets ? (
                            <Button
                                endIcon={<ArrowDropDownIcon />}
                                variant="outlined"
                                disabled={targets.length === 0}
                                ref={refs.testsButton}
                                aria-controls="tests-menu"
                                aria-haspopup="true"
                                onClick={handleToggle}
                            >
                                Tests
                            </Button>
                        ) : null}
                        <Tooltip
                            title={t('ide.toolBar.clearOutputs') as string}
                            aria-label="clear-outputs"
                            placement="right"
                        >
                            <Button onClick={clearOutputs} variant="outlined">
                                <DeleteIcon />
                            </Button>
                        </Tooltip>
                    </ButtonGroup>
                </Grid>
                <Grid item xs={12} sm={6} className={classes.gridItem}>
                    <Grid container spacing={2} justifyContent="center" className={classes.noMargin}>
                        <Grid item>{templatesMenu}</Grid>
                        <Grid item>
                            <ToolsMenuItem
                                selectedContract={selectedContract}
                                setVolatileContract={setVolatileContract}
                                onlyIcon={onlyIcon}
                                downloadOutputPanel={downloadOutputPanel}
                                syntax={syntax}
                            />
                        </Grid>
                        <Grid item>
                            <ShareMenuItem onlyIcon={onlyIcon} code={selectedContract?.code} baseUrl={baseUrl} />
                        </Grid>
                        <Grid item>{settingsMenu}</Grid>
                    </Grid>
                </Grid>
            </Grid>
            {/* Tests menu */}
            <Menu
                anchorEl={refs.testsButton.current}
                open={Boolean(open)}
                onClose={handleClose}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                {compileTargets.length > 0 ? (
                    <div>
                        <MenuItem disabled>
                            <ListItemText primary={t('ide.output.compilations')} />
                        </MenuItem>
                        {compileTargets.map(({ name }, i) => (
                            <MenuItem key={i} onClick={() => runScenario && runScenario(name)}>
                                <ListItemIcon>
                                    <CompilationIcon />
                                </ListItemIcon>
                                <ListItemText primary={name} />
                            </MenuItem>
                        ))}
                    </div>
                ) : null}
                {testTargets.length > 0 ? (
                    <div>
                        <MenuItem disabled>
                            <ListItemText primary={t('ide.output.tests')} />
                        </MenuItem>
                        {testTargets.map(({ name }, i) => (
                            <MenuItem key={i} onClick={() => runScenario && runScenario(name)}>
                                <ListItemIcon>
                                    <TestIcon />
                                </ListItemIcon>
                                <ListItemText primary={name} />
                            </MenuItem>
                        ))}
                    </div>
                ) : null}
                {Object.entries(miscTargets).map(([kind, list]) => (
                    <div key={kind}>
                        <MenuItem disabled>
                            <ListItemText primary={kind} />
                        </MenuItem>
                        {list.map(({ name }, i) => (
                            <MenuItem key={i} onClick={() => runScenario && runScenario(name)}>
                                <ListItemIcon>
                                    <TestIcon />
                                </ListItemIcon>
                                <ListItemText primary={name} />
                            </MenuItem>
                        ))}
                    </div>
                ))}
            </Menu>
            {/* Play Button Guide Step (Used in newcomer mode) */}
            <PopperWithArrow
                open={newcomerGuideStep === IDENewcomerGuideSteps.RUN_CODE}
                withGlowAnimation
                anchorEl={refs.playButton.current}
            >
                <Paper>
                    <Chip
                        className={classes.spacingTwo}
                        variant="outlined"
                        label={t('ide.newcomer.runCodeStep.popperChip')}
                    />
                    <DialogActions>
                        <Button fullWidth variant="contained" onClick={handleNewcomerInteraction}>
                            {t('common.understood')}
                        </Button>
                    </DialogActions>
                </Paper>
            </PopperWithArrow>
        </div>
    );
};

export default EditorToolBar;
