import React from 'react';

// Material UI
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
// Material Icons
import EditIcon from '@material-ui/icons/Edit';
import ViewListIcon from '@material-ui/icons/ViewList';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

// Local utils
import { getElapsedTime } from '../../../utils/time';
// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';
// Local Components
import EditContractDialog from './EditContractDialog';
import { IDEContract } from 'SmartPyModels';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        list: {
            overflowY: 'auto',
            maxHeight: 300,
            backgroundColor: theme.palette.background.default,
            borderRadius: 5,
            marginTop: 10,
            marginBottom: 20,
        },
        emptyListText: {
            display: 'flex',
            justifyContent: 'center',
        },
        contractName: {
            color: 'peru',
        },
    }),
);

interface OwnProps {
    onlyIcon: boolean;
    contracts: IDEContract[];
    handleSelectContract: (contractId: string) => void;
    handleDeleteContract: (contractId: string) => void;
    handleUpdateContract: (contractId: string, name: string) => void;
}

const StoredContractsDialog: React.FC<OwnProps> = ({
    onlyIcon,
    contracts,
    handleDeleteContract,
    handleSelectContract,
    handleUpdateContract,
}) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [editContract, setEditContract] = React.useState(null as IDEContract | null);
    const t = useTranslation();

    const handleEditOpen = (contract: IDEContract) => {
        contract && setEditContract(contract);
    };

    const handleEditClose = () => {
        setEditContract(null);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <React.Fragment>
            <Tooltip
                title={t('ide.contractManagement.listDialog.listStoredContracts') as string}
                aria-label="list-contracts"
                placement="bottom"
            >
                <Button onClick={handleClickOpen} variant="outlined">
                    {onlyIcon ? <ViewListIcon /> : t('ide.contractManagement.listDialog.listStoredContracts')}
                </Button>
            </Tooltip>
            <Dialog fullWidth open={open} onClose={handleClose}>
                <DialogTitle>{t('ide.contractManagement.listDialog.title')}</DialogTitle>
                <DialogContent dividers>
                    <Typography variant="caption">{t('ide.contractManagement.listDialog.clickToUse')}</Typography>
                    <List className={classes.list}>
                        {contracts.length === 0 ? (
                            <div className={classes.emptyListText}>
                                <Typography variant="overline">
                                    {t('ide.contractManagement.listDialog.emptyList')}
                                </Typography>
                            </div>
                        ) : null}
                        {contracts.map(({ id, name, updatedAt }, index) => (
                            <ListItem
                                key={id}
                                button
                                divider={index !== contracts.length - 1}
                                onClick={() => id && handleSelectContract(id)}
                            >
                                <ListItemText
                                    primary={
                                        <div>
                                            {t('ide.contractManagement.listDialog.contractName')}
                                            <span className={classes.contractName}>{name}</span>
                                        </div>
                                    }
                                    secondary={t('ide.contractManagement.listDialog.elapsedTime', {
                                        elapsedTime: getElapsedTime(updatedAt as string),
                                    })}
                                />
                                <ListItemSecondaryAction>
                                    <Tooltip
                                        title={t('ide.contractManagement.editContract') as string}
                                        aria-label="edit-contract"
                                        placement="left"
                                    >
                                        <IconButton
                                            edge="end"
                                            aria-label="edit"
                                            onClick={() => handleEditOpen(contracts[index])}
                                            color="primary"
                                        >
                                            <EditIcon />
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip
                                        title={t('ide.contractManagement.deleteContract') as string}
                                        aria-label="delete-contract"
                                        placement="left"
                                    >
                                        <IconButton
                                            edge="end"
                                            aria-label="delete"
                                            onClick={() => id && handleDeleteContract(id)}
                                        >
                                            <DeleteForeverIcon color="error" />
                                        </IconButton>
                                    </Tooltip>
                                </ListItemSecondaryAction>
                            </ListItem>
                        ))}
                    </List>
                </DialogContent>
            </Dialog>
            {editContract ? (
                <EditContractDialog
                    open
                    contract={editContract}
                    handleDeleteContract={handleDeleteContract}
                    handleUpdateContract={handleUpdateContract}
                    handleClose={handleEditClose}
                />
            ) : null}
        </React.Fragment>
    );
};

export default StoredContractsDialog;
