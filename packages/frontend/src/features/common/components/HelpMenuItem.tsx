import React from 'react';

// Material UI
import MenuItem from '@material-ui/core/MenuItem';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';

// Local Components
import HelpMenuFab from '../../common/components/HelpMenuFab';

// Local Utils
import { getBase } from '../../../utils/url';
import useTranslation from '../../i18n/hooks/useTranslation';

const HelpMenuItem: React.FC = () => {
    const t = useTranslation();

    return (
        <>
            <HelpMenuFab>
                <MenuItem component={Link} href={`${getBase()}/docs`} target="_blank">
                    <Typography variant="inherit">{t('ide.helpMenu.openReferenceManual')}</Typography>
                </MenuItem>
                <MenuItem component={Link} href={`${getBase()}/docs/cli`} target="_blank">
                    <Typography variant="inherit">{t('ide.helpMenu.installSmartpyCli')}</Typography>
                </MenuItem>
                <MenuItem component={Link} href={`${getBase()}/docs/releases`} target="_blank">
                    <Typography variant="inherit">{t('ide.helpMenu.viewReleases')}</Typography>
                </MenuItem>
            </HelpMenuFab>
        </>
    );
};

export default HelpMenuItem;
