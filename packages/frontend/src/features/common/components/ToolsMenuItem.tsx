import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ImportFromDisk from '@material-ui/icons/InsertDriveFile';
import Download from '@material-ui/icons/GetApp';

import useTranslation from '../../i18n/hooks/useTranslation';

// Local Elements
import ToolsButton from '../../common/elements/ToolsButton';
// Local Components
import FileImporter from '../../common/components/FileImporter';
// Local Utils
import { downloadFile } from '../../../utils/file';
// Local Services
import toast from '../../../services/toast';
import { IDEContract } from 'SmartPyModels';
import { SYNTAX } from '../enums/ide';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        listItemIcon: {
            minWidth: 30,
        },
    }),
);

interface OwnProps {
    onlyIcon: boolean;
    downloadOutputPanel: () => void;
    selectedContract: IDEContract | undefined;
    setVolatileContract: (code: string) => void;
    syntax: SYNTAX;
}

const ToolsMenuItem: React.FC<OwnProps> = (props) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [fileImporterOpen, setFileImporterOpen] = React.useState(false);
    const anchorRef = React.useRef<HTMLButtonElement>(null);
    const dispatch = useDispatch();
    const t = useTranslation();

    const { onlyIcon, downloadOutputPanel, selectedContract, setVolatileContract, syntax } = props;

    const extension = React.useMemo((): string => {
        if (syntax === SYNTAX.PYTHON) {
            return '.py';
        }
        if (syntax === SYNTAX.TYPESCRIPT) {
            return '.ts';
        }
        if (syntax === SYNTAX.MICHELSON) {
            return '.tz';
        }
        return '.unknown';
    }, [syntax]);

    const acceptExpression = React.useMemo(() => {
        if (syntax === SYNTAX.PYTHON) {
            return '.py, text/x-python';
        }
        if (syntax === SYNTAX.TYPESCRIPT) {
            return '.ts, text/x-typescript';
        }
        if (syntax === SYNTAX.MICHELSON) {
            return '.tz';
        }
        return '.*';
    }, [syntax]);

    const handleToggle = () => {
        setOpen((prevState) => !prevState);
    };

    const handleClose = (event: React.MouseEvent<EventTarget>) => {
        if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
            return;
        }
        setOpen(false);
    };

    const handleFileImporterOpen = () => {
        setFileImporterOpen(true);
    };

    const handleFileImporterClose = () => {
        setFileImporterOpen(false);
    };

    const handleFileImporterComplete = async (file: File) => {
        const reader = new FileReader();

        const fail = () => {
            toast.error(t('common.couldNotLoadFile'));
        };

        reader.onabort = fail;
        reader.onerror = fail;
        reader.onload = () => {
            dispatch(setVolatileContract(reader.result as string));
            handleFileImporterClose();
        };
        reader.readAsText(file);
    };

    return (
        <React.Fragment>
            <ToolsButton
                ref={anchorRef}
                aria-controls="tools-menu"
                aria-haspopup="true"
                onClick={handleToggle}
                onlyIcon={onlyIcon}
            />
            <Menu
                anchorEl={anchorRef.current}
                keepMounted
                open={Boolean(open)}
                onClose={handleClose}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                <MenuItem onClick={handleFileImporterOpen}>
                    <ListItemIcon className={classes.listItemIcon}>
                        <ImportFromDisk />
                    </ListItemIcon>
                    <ListItemText primary="Load Contract From Disk" />
                </MenuItem>
                <MenuItem
                    disabled={!selectedContract}
                    onClick={() =>
                        downloadFile(`${selectedContract?.name}${extension}`, selectedContract?.code as string)
                    }
                >
                    <ListItemIcon className={classes.listItemIcon}>
                        <Download />
                    </ListItemIcon>
                    <ListItemText primary="Download Contract to Disk" />
                </MenuItem>
                <MenuItem onClick={downloadOutputPanel}>
                    <ListItemIcon className={classes.listItemIcon}>
                        <Download />
                    </ListItemIcon>
                    <ListItemText primary="Download Output Panel" />
                </MenuItem>
            </Menu>
            <FileImporter
                open={fileImporterOpen}
                onClose={handleFileImporterClose}
                onComplete={handleFileImporterComplete}
                accept={acceptExpression}
            />
        </React.Fragment>
    );
};

export default ToolsMenuItem;
