import httpRequester from '../../../services/httpRequester';

import { downloadFile, getFileContent } from '../../../utils/file';
import { getBase } from '../../../utils/url';
import { getAllTemplates as getPythonTemplates } from '../../py-ide/constants/templates';
import { getAllTemplates as getTypescriptTemplates } from '../../ts-ide/constants/templates';

/**
 * Get template code.
 * @param {string} templateName - Template name.
 * @return {Promise<string | void>} A promise with the template code, or null if the template doesn't exist.
 */
export const getTemplateCode = async (templateName: string): Promise<string> => {
    const isPythonTemplate = templateName.endsWith('.py');
    const template = isPythonTemplate
        ? getPythonTemplates()[templateName]?.fileName
        : getTypescriptTemplates()[templateName]?.fileName;

    return getFileContent(`${getBase()}${isPythonTemplate ? '' : '/typescript'}/templates/${template || templateName}`);
};

export const downloadOutputPanel = async (
    contractName = 'output-panel',
    isDark: boolean,
    outputPanel: HTMLDivElement,
) => {
    let html = outputPanel.innerHTML;
    if (outputPanel.children[0]?.className.match('MuiAlert')) {
        html = Array.from(outputPanel.children)
            .slice(1)
            .map((c) => c.innerHTML)
            .join('');
    }
    const output = html.replace(/\/static/g, `${window.location.origin}/static`);

    let styles = await (await httpRequester.get(`${window.location.origin}/static/css/smart.css`)).data;
    styles += await (await httpRequester.get(`${window.location.origin}/static/css/typography.css`)).data;
    styles = styles.replace(/\/static/g, `${window.location.origin}/static`);
    const script = await (await httpRequester.get(`${window.location.origin}/static/js/smart.js`)).data;
    downloadFile(
        `${contractName}.html`,
        `<html>
            <head>
                <style>${styles}</style>
                <script>${script}</script>
            </head>
            <body>
                <div id="outputPanel">${output}</div>
            </body>
        </html>`,
    );
};
