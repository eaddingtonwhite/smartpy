import React from 'react';
// Material UI
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import ToolsIcon from '@material-ui/icons/Build';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

interface OwnProps {
    onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
    onlyIcon: boolean;
}

export default React.forwardRef<HTMLButtonElement, OwnProps>(({ onlyIcon, ...props }, ref) => (
    <Tooltip title="Tools">
        <Button
            startIcon={onlyIcon ? null : <ToolsIcon />}
            color="primary"
            variant="contained"
            aria-label="tools"
            endIcon={onlyIcon ? null : <ArrowDropDownIcon />}
            {...{ ...props, ref }}
        >
            {onlyIcon ? <ToolsIcon /> : 'Tools'}
        </Button>
    </Tooltip>
));
