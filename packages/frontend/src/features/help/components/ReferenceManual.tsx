import React from 'react';

// Material UI
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import CliIcon from '@material-ui/icons/LastPage';
import FileIcon from '@material-ui/icons/Description';
import ReleasesIcon from '@material-ui/icons/LibraryBooks';
import MenuBookIcon from '@material-ui/icons/MenuBook';

// Local Components
import SelectionOfTemplates from '../../common/components/SelectionOfTemplates';
import CodeBlock from '../../common/components/CodeBlock';

// Local Services
import logger from '../../../services/logger';

// Local Utils
import { getFileContent } from '../../../utils/file';
import { getBase } from '../../../utils/url';

// State Management
import useTranslation from '../../i18n/hooks/useTranslation';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            margin: 20,
        },
        card: {
            borderWidth: 1,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.main,
            boxShadow: `5px 5px 0 0 ${theme.palette.primary.main}`,
        },
        title: {
            color: theme.palette.primary.main,
            fontWeight: 'bold',
        },
        description: {
            color: theme.palette.primary.light,
        },
        marginBottom: {
            marginBottom: 20,
        },
        arrow: {
            display: 'flex',
            justifyContent: 'center',
        },
        innerDescription: {
            fontWeight: 'bold',
            margin: 10,
        },
        list: {
            width: '100%',
            backgroundColor: theme.palette.background.default,
            padding: 0,
        },
        link: {
            color: theme.palette.text.primary,
            textDecoration: 'none',
            border: 'none',
            outline: 'none',
            cursor: 'pointer',
            transition: '0.3s',
            margin: 0,
        },
    }),
);

const ReferenceManual = () => {
    const classes = useStyles();
    const t = useTranslation();
    const [selectedFile, setSelectedFile] = React.useState(null as string | null);

    const handleDialogClose = () => setSelectedFile(null);

    const showFile = async (fileName: string) => {
        getFileContent(`${getBase()}/static/python/${fileName}`)
            .then((content) => {
                setSelectedFile(content);
            })
            .catch((err) => logger.error(err));
    };

    return (
        <div className={classes.root} id="introduction">
            <Typography variant="h4" className={`${classes.title} ${classes.marginBottom}`}>
                {t('help.referenceManual')}
            </Typography>
            <Typography gutterBottom variant="body1" className={`${classes.description} ${classes.marginBottom}`}>
                {t('help.referenceManualSubTitle')}
                <Chip
                    clickable
                    href={`${getBase()}/docs`}
                    target="_blank"
                    component={Link}
                    icon={<MenuBookIcon />}
                    variant="outlined"
                    size="small"
                    label={t('help.referenceManual')}
                />
            </Typography>
            <Chip
                clickable
                href={`${getBase()}/docs/releases`}
                target="_blank"
                component={Link}
                icon={<ReleasesIcon />}
                variant="outlined"
                label={t('help.releases')}
                className={classes.marginBottom}
            />
            <Divider variant="middle" orientation="vertical" />
            <Chip
                clickable
                href={`${getBase()}/docs/cli`}
                target="_blank"
                component={Link}
                icon={<CliIcon />}
                variant="outlined"
                label={t('help.installCLI')}
                className={classes.marginBottom}
            />

            {/* A selection with some templates */}
            <SelectionOfTemplates />

            <Typography variant="h6" className={`${classes.title} ${classes.marginBottom}`}>
                {t('help.internalModulesHeader')}
            </Typography>
            <Typography gutterBottom variant="body1" className={`${classes.description} ${classes.marginBottom}`}>
                {t('help.internalModulesSubHeader')}
            </Typography>
            <Grid container spacing={2} alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <Paper className={`${classes.card} ${classes.marginBottom}`}>
                        <List className={classes.list}>
                            <ListItem dense divider button onClick={() => showFile('smartpy.py')}>
                                <ListItemAvatar>
                                    <FileIcon fontSize="large" />
                                </ListItemAvatar>
                                <ListItemText primary="smartpy.py" secondary={t('help.smartpyModuleDescription')} />
                            </ListItem>
                            <ListItem dense button onClick={() => showFile('smartpyio.py')}>
                                <ListItemAvatar>
                                    <FileIcon fontSize="large" />
                                </ListItemAvatar>
                                <ListItemText primary="smartpyio.py" secondary={t('help.smartpyioModuleDescription')} />
                            </ListItem>
                        </List>
                    </Paper>
                </Grid>
            </Grid>

            <Typography gutterBottom variant="body1" className={`${classes.description} ${classes.marginBottom}`}>
                {t('help.smartpyReliesOn')}
                <Link href="https://gitlab.com/SmartPy/smartpy" target="_blank">
                    {t('help.openSourceDistribution')}
                </Link>
            </Typography>

            <Dialog fullWidth maxWidth="lg" open={!!selectedFile} onClose={handleDialogClose}>
                <DialogTitle>{t('help.moduleCode')}</DialogTitle>
                <DialogContent dividers>
                    <CodeBlock language="python" showLineNumbers withCopy={false} text={selectedFile || ''} />
                </DialogContent>
                <DialogActions>
                    <Button color="primary" onClick={handleDialogClose}>
                        {t('common.close')}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default ReferenceManual;
