import React from 'react';
import { useDispatch } from 'react-redux';
import { KeyCode, KeyMod } from 'monaco-editor/esm/vs/editor/editor.api';
import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';
import type { OnMount } from '@monaco-editor/react';

// Material UI
import { makeStyles, createStyles, Theme, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';

// State Management
import { IDESettings } from 'SmartPyModels';
import actions, { setVolatileContract, updateNewcomerGuideStep } from '../actions';
import { useNewcomerGuideStep, useSelectedContract } from '../selectors';

// Local Components
import EditorToolBar from '../../common/components/EditorToolBar';
import OutputPanel from '../../common/components/OutputPanel';
import ContractManagement from '../containers/ContractManagement';
import Editor from '../../editor/components/Editor';

// Local Utils
import { evalRun } from '../utils/EditorUtils';
import { evalTest } from '../utils/EditorUtils';
import toast from '../../../services/toast';
import useTranslation from '../../i18n/hooks/useTranslation';
import { downloadOutputPanel } from '../../common/utils/IDEUtils';
import SettingsMenuItem from '../components/SettingsMenuItem';
import TemplatesMenuItem from '../components/TemplatesMenuItem';
import { SYNTAX } from '../../common/enums/ide';
import Logger from '../../../services/logger';

const useStyles = (stacked: boolean) =>
    makeStyles((theme: Theme) =>
        createStyles({
            root: {
                display: 'flex',
                flexDirection: 'column',
                flexGrow: 1,
            },
            editorSection: {
                display: 'flex',
                flexGrow: 1,
                borderTopWidth: 2,
                borderTopStyle: 'solid',
                borderTopColor: theme.palette.primary.light,
                // screen height - navBar - toolBar (percentage doesn't work here)
                height: 'calc(100vh - 80px - 60px)',
            },
            outputPanel: {
                background: theme.palette.background.paper,
                position: 'relative',
                height: stacked ? '40%' : '100%',
                padding: 15,
                overflowY: 'auto',
            },
            borderLeft: {
                borderLeftWidth: 2,
                borderLeftStyle: 'solid',
                borderLeftColor: theme.palette.primary.light,
            },
            borderTop: {
                borderTopWidth: 2,
                borderTopStyle: 'solid',
                borderTopColor: theme.palette.primary.light,
            },
            editor: {
                display: 'flex',
                flexDirection: 'column',
                height: stacked ? '60%' : '100%',
            },
            hidden: {
                display: 'none',
            },
        }),
    );

interface OwnProps {
    clearOutputs: () => void;
    editorRef: React.MutableRefObject<editor.IStandaloneCodeEditor>;
    htmlOutput: { __html: string };
    settings: IDESettings;
    targets: { kind: string; name: string }[];
    showError: (error: string) => void;
}

const EditorView: React.FC<OwnProps> = (props) => {
    const classes = useStyles(props.settings.layout === 'stacked')();
    const outputPanelRef = React.useRef(null as unknown as HTMLDivElement);
    const theme = useTheme();
    const dispatch = useDispatch();
    const contract = useSelectedContract();
    const newcomerGuideStep = useNewcomerGuideStep();
    const t = useTranslation();
    const { targets, clearOutputs, editorRef, htmlOutput, settings, showError } = props;
    const downMd = useMediaQuery((theme: Theme) => theme.breakpoints.down('xs'));
    const [compiling, setCompiling] = React.useState(false);

    const getGridSm = () => (settings.layout === 'side-by-side' ? 6 : 12);
    const getBorderClass = () =>
        settings.layout !== 'side-by-side' || downMd ? classes.borderTop : classes.borderLeft;

    const downloadOutput = () => {
        if (contract && htmlOutput.__html) {
            downloadOutputPanel(contract.name, theme.palette.mode === 'dark', outputPanelRef.current);
        } else {
            toast.error(t('ide.errors.outputEmpty'));
        }
    };

    const onInput = (code = '') => {
        if (editorRef.current) {
            if (contract?.id && code !== contract?.code) {
                dispatch(
                    actions.updateContract({
                        ...contract,
                        code,
                    }),
                );
            } else if (!contract?.id) {
                dispatch(
                    actions.addContract(
                        {
                            code,
                            shared: false,
                        },
                        false,
                    ),
                );
            }
        }
    };

    const compileContract = async (withTests = true) => {
        setCompiling(true);
        try {
            await new Promise((r) => setTimeout(r, 500));
            await evalRun(withTests);
        } catch (error) {
            showError(error);
        }
        setCompiling(false);
    };

    const runScenario = async (testName: string) => {
        setCompiling(true);
        try {
            await evalTest(testName);
        } catch (error) {
            showError(error);
        }
        setCompiling(false);
    };

    const handleEditorDidMount: OnMount = (editor) => {
        if (!editor) {
            return Logger.error('Monaco Editor could not load, please notify the maintainer.');
        }
        editorRef.current = editor;
        editor.focus();
    };

    /**
     * This method returns the editor
     */
    const showEditor = () => {
        return (
            <Grid
                item
                xs={getGridSm()}
                className={`${classes.editor} ${settings.layout === 'output-only' ? classes.hidden : ''}`}
            >
                <ContractManagement />
                <Editor
                    onMount={handleEditorDidMount}
                    language="tmPython"
                    value={contract?.code || ''}
                    options={{
                        automaticLayout: true,
                        selectOnLineNumbers: true,
                        fontSize: settings.fontSize,
                    }}
                    actions={[
                        {
                            // Unique identifier
                            id: 'run-code',
                            // A label of the action that will be presented to the user.
                            label: 'Run Code',
                            keybindings: [KeyMod.CtrlCmd | KeyCode.Enter],
                            // Custom section
                            contextMenuGroupId: 'utils',
                            contextMenuOrder: 1,
                            run: () => compileContract(),
                        },
                        {
                            // Unique identifier
                            id: 'run-code-no-tests',
                            // A label of the action that will be presented to the user.
                            label: 'Run Code Without Tests',
                            keybindings: [KeyMod.Shift | KeyMod.CtrlCmd | KeyCode.Enter],
                            // Custom section
                            contextMenuGroupId: 'utils',
                            contextMenuOrder: 2,
                            run: () => compileContract(false),
                        },
                    ]}
                    onChange={onInput}
                />
            </Grid>
        );
    };

    return (
        <div className={classes.root}>
            <EditorToolBar
                targets={targets}
                clearOutputs={clearOutputs}
                downloadOutputPanel={downloadOutput}
                compileContract={compileContract}
                runScenario={runScenario}
                selectedContract={contract}
                baseUrl={`${window.location.origin}${process.env.PUBLIC_URL}/ide`}
                settingsMenu={<SettingsMenuItem />}
                templatesMenu={<TemplatesMenuItem />}
                updateNewcomerGuideStep={updateNewcomerGuideStep}
                setVolatileContract={setVolatileContract}
                newcomerGuideStep={newcomerGuideStep}
                syntax={SYNTAX.PYTHON}
            />
            <div className={classes.editorSection}>
                <Grid container justifyContent="center">
                    {showEditor()}
                    <Grid
                        item
                        xs={getGridSm()}
                        className={`${classes.outputPanel} ${getBorderClass()} ${
                            settings.layout === 'editor-only' ? classes.hidden : ''
                        }`}
                        ref={outputPanelRef}
                    >
                        <OutputPanel output={htmlOutput} isContractCompiling={compiling} />
                    </Grid>
                </Grid>
            </div>
        </div>
    );
};

export default EditorView;
