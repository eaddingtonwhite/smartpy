import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import { makeStyles, alpha, createStyles, Theme, useTheme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import InputBase from '@material-ui/core/InputBase';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import Chip from '@material-ui/core/Chip';
import SearchIcon from '@material-ui/icons/Search';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';
import ShareOutlinedIcon from '@material-ui/icons/ShareOutlined';

// Local Services
import toast from '../../../services/toast';
// Local Components
import Loader from '../../loader/components/CircularProgressWithText';
// State Management
import actions from '../actions';
import { useFavoriteTemplates } from '../selectors';
import useTranslation from '../../i18n/hooks/useTranslation';
// Local Constants
import { TemplateProps, Templates } from '../constants/templates';
// Local Utils
import { StringMatchers } from '../../../utils/matchers';
import { copyToClipboard } from '../../../utils/clipboard';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        secondaryText: {
            color: theme.palette.mode === 'dark' ? 'rgba(255, 255, 255, 0.54)' : 'rgba(0, 0, 0, 0.54)',
        },
        appBar: {
            marginBottom: 20,
        },
        list: {
            width: '100%',
            maxHeight: 400,
            overflowY: 'auto',
        },
        item: {
            display: 'flex',
            width: '100%',
        },
        favoriteButton: {
            width: 48,
            height: 48,
            alignSelf: 'center',
        },
        itemContainer: {
            width: '100%',
        },
        title: {
            flexGrow: 1,
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'block',
            },
        },
        search: {
            position: 'relative',
            display: 'flex',
            alignItems: 'center',
            paddingLeft: 5,
            borderRadius: theme.shape.borderRadius,
            backgroundColor: alpha(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: alpha(theme.palette.common.white, 0.25),
            },
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(1),
                width: 'auto',
            },
        },
        searchIcon: {
            alignSelf: 'center',
        },
        inputRoot: {
            color: 'inherit',
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                width: '12ch',
                '&:focus': {
                    width: '20ch',
                },
            },
        },
        emptyListText: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: 20,
        },
        accordion: {
            marginBottom: 10,
            border: '1px solid rgba(0, 123, 255, 0.5)',
        },
        sectionSummary: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
    }),
);

interface OwnProps {
    title: string;
    templates: Templates;
}

const TemplateContracts: React.FC<OwnProps> = ({ templates, title }) => {
    const classes = useStyles();
    const theme = useTheme();
    const [expanded, setExpanded] = React.useState('');
    const [search, setSearch] = React.useState('');
    const [loading, setLoading] = React.useState(true);
    const favoriteTemplates = useFavoriteTemplates();
    const dispatch = useDispatch();
    const t = useTranslation();

    const handleListItemClick = async (fileName: string) => {
        dispatch(actions.loadTemplate(fileName));
    };

    const handleTemplateShare = (fileName: string) => {
        copyToClipboard(`${window.location.origin}/ide?template=${fileName}`);
        toast.info('The template url was copied!');
    };

    const handleFavoriteToggle = (template: string) => {
        dispatch(actions.toggleFavoriteTemplate(template));
    };

    const filteredTemplates = React.useMemo(() => {
        setLoading(true);
        const filtered = Object.keys(templates).reduce<Templates>((state, section) => {
            const sectionTemplates = templates[section].reduce<TemplateProps[]>((state, template) => {
                if (
                    StringMatchers.includesInsensitive(template.description, search) ||
                    StringMatchers.includesInsensitive(template.name, search)
                ) {
                    state.push(template);
                }
                return state;
            }, []);
            return {
                ...state,
                [section]: sectionTemplates,
            };
        }, {});
        setLoading(false);
        return filtered;
    }, [search, templates]);

    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value);
    };

    const getTemplatesList = (templates: TemplateProps[]) => {
        return templates.length === 0 ? (
            <div className={classes.emptyListText}>
                <Typography variant="overline">{t('ide.templatesMenu.emptyList')}</Typography>
            </div>
        ) : (
            <List className={classes.list}>
                {templates.map((template) => (
                    <div key={template.name} className={classes.item}>
                        <IconButton
                            key={template.fileName}
                            className={classes.favoriteButton}
                            onClick={() => handleFavoriteToggle(template.fileName)}
                        >
                            {favoriteTemplates.includes(template.fileName) ? <StarIcon /> : <StarBorderIcon />}
                        </IconButton>
                        <ListItem
                            button
                            onClick={() => handleListItemClick(template.fileName)}
                            classes={{ container: classes.itemContainer }}
                        >
                            <div>
                                <ListItemText primary={template.name} />
                                {template.description ? (
                                    <div
                                        className={classes.secondaryText}
                                        dangerouslySetInnerHTML={{
                                            __html: template.description,
                                        }}
                                    />
                                ) : null}
                            </div>
                            <ListItemSecondaryAction onClick={() => handleTemplateShare(template.fileName)}>
                                <Tooltip title="Share Template" aria-label="share-template" placement="left">
                                    <IconButton edge="end">
                                        <ShareOutlinedIcon />
                                    </IconButton>
                                </Tooltip>
                            </ListItemSecondaryAction>
                        </ListItem>
                    </div>
                ))}
            </List>
        );
    };

    const TemplatesComponent: React.FC<{ templates: Templates }> = ({ templates }) => {
        const sections = Object.keys(templates);
        if (sections.length === 1) {
            return getTemplatesList(templates[sections[0]]);
        }
        return (
            <div>
                {sections.map((section) => (
                    <MuiAccordion
                        key={section}
                        square
                        expanded={expanded === section}
                        onChange={() => setExpanded((state) => (state === section ? '' : section))}
                        className={classes.accordion}
                    >
                        <MuiAccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            classes={{ content: classes.sectionSummary }}
                        >
                            <Typography variant="h6">{section}</Typography>
                            <Chip variant="outlined" size="small" label={templates[section].length} />
                        </MuiAccordionSummary>
                        <Divider />
                        <MuiAccordionDetails>{getTemplatesList(templates[section])}</MuiAccordionDetails>
                    </MuiAccordion>
                ))}
            </div>
        );
    };

    return (
        <React.Fragment>
            <AppBar
                position="static"
                color={theme.palette.mode === 'dark' ? 'default' : 'primary'}
                className={classes.appBar}
            >
                <Toolbar>
                    <Typography className={classes.title} variant="h6" noWrap>
                        {title}
                    </Typography>
                    <div className={classes.search}>
                        <SearchIcon className={classes.searchIcon} />
                        <InputBase
                            defaultValue={search}
                            onChange={handleSearch}
                            placeholder={`${t('common.search')}…`}
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>
                </Toolbar>
            </AppBar>

            {loading ? <Loader loading={true} margin={50} /> : <TemplatesComponent templates={filteredTemplates} />}
        </React.Fragment>
    );
};

export default TemplateContracts;
