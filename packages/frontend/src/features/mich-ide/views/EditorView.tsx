import React from 'react';
import { useDispatch } from 'react-redux';
import { KeyCode, KeyMod } from 'monaco-editor/esm/vs/editor/editor.api';
import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';
import type { OnMount } from '@monaco-editor/react';

// Material UI
import { makeStyles, createStyles, Theme, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Alert from '@material-ui/core/Alert';
import Grid from '@material-ui/core/Grid';

// State Management
import { MichelsonIDESettings } from 'SmartPyModels';
import { useSelectedContract } from '../selectors';

// Local Components
import OutputPanel from '../../common/components/OutputPanel';
import ContractManagement from '../containers/ContractManagement';
// Local Utils
import { evalCode } from '../utils/EditorUtils';
import toast from '../../../services/toast';
import useTranslation from '../../i18n/hooks/useTranslation';
import { downloadOutputPanel } from '../../common/utils/IDEUtils';
import Logger from '../../../services/logger';
import Editor from '../../editor/components/Editor';
import EditorToolBar from '../../common/components/EditorToolBar';
import SettingsMenuItem from '../components/SettingsMenuItem';
import TemplatesMenuItem from '../components/TemplatesMenuItem';
import { SYNTAX } from '../../common/enums/ide';
import { addContract, setVolatileContract, updateContract } from '../actions';
import debounce from '../../../utils/debounce';

const debouncer = debounce(1000);

const useStyles = (stacked: boolean) =>
    makeStyles((theme: Theme) =>
        createStyles({
            root: {
                display: 'flex',
                flexDirection: 'column',
                flexGrow: 1,
            },
            editorSection: {
                display: 'flex',
                flexGrow: 1,
                borderTopWidth: 2,
                borderTopStyle: 'solid',
                borderTopColor: theme.palette.primary.light,
                // screen height - navBar - toolBar (percentage doesn't work here)
                height: 'calc(100vh - 80px - 60px)',
            },
            outputPanel: {
                background: theme.palette.background.paper,
                position: 'relative',
                height: stacked ? '40%' : '100%',
                padding: 15,
                overflowY: 'auto',
            },
            borderLeft: {
                borderLeftWidth: 2,
                borderLeftStyle: 'solid',
                borderLeftColor: theme.palette.primary.light,
            },
            borderTop: {
                borderTopWidth: 2,
                borderTopStyle: 'solid',
                borderTopColor: theme.palette.primary.light,
            },
            editor: {
                display: 'flex',
                flexDirection: 'column',
                height: stacked ? '60%' : '100%',
            },
            hidden: {
                display: 'none',
            },
            errorMessage: {
                marginBottom: 10,
            },
        }),
    );

interface OwnProps {
    clearOutputs: () => void;
    editorRef: React.MutableRefObject<editor.IStandaloneCodeEditor>;
    htmlOutput: { __html: string };
    settings: MichelsonIDESettings;
    showError: (error: string) => void;
    firstMessage: string;
}

const EditorView: React.FC<OwnProps> = (props) => {
    const classes = useStyles(props.settings.layout === 'stacked')();
    const outputPanelRef = React.useRef(null as unknown as HTMLDivElement);
    const theme = useTheme();
    const dispatch = useDispatch();
    const contract = useSelectedContract();
    const t = useTranslation();
    const { clearOutputs, editorRef, htmlOutput, settings, showError, firstMessage } = props;
    const downMd = useMediaQuery((theme: Theme) => theme.breakpoints.down('xs'));
    const [compiling, setCompiling] = React.useState(false);

    const getGridSm = () => (settings.layout === 'side-by-side' ? 6 : 12);
    const getBorderClass = () =>
        settings.layout !== 'side-by-side' || downMd ? classes.borderTop : classes.borderLeft;

    const downloadOutput = () => {
        if (contract && htmlOutput.__html) {
            downloadOutputPanel(contract.name, theme.palette.mode === 'dark', outputPanelRef.current);
        } else {
            toast.error(t('ide.errors.outputEmpty'));
        }
    };

    const onInput = (code = '') => {
        if (editorRef.current) {
            if (contract?.id && code !== contract?.code) {
                dispatch(
                    updateContract({
                        ...contract,
                        code,
                    }),
                );
            } else if (!contract?.id) {
                dispatch(
                    addContract(
                        {
                            code,
                            shared: false,
                        },
                        false,
                    ),
                );
            }
            settings.electricEvaluation && compileContract();
        }
    };

    const compileContract = React.useCallback(
        async () =>
            debouncer(() => {
                const code = editorRef.current?.getValue();
                if (code) {
                    setCompiling(true);
                    try {
                        evalCode(t, code, settings.protocol);
                    } catch (error) {
                        showError(error);
                    }
                    setCompiling(false);
                }
            }),
        [editorRef, settings.protocol, showError, t],
    );

    const handleEditorDidMount: OnMount = React.useCallback(
        (editor) => {
            if (!editor) {
                return Logger.error('Monaco Editor could not load, please notify the maintainer.');
            }
            editorRef.current = editor;
            editor.focus();

            window.editor = editor;
            compileContract();
        },
        [compileContract, editorRef],
    );

    /**
     * This method returns the editor
     */
    const showEditor = () => {
        return (
            <Grid
                item
                xs={getGridSm()}
                className={`${classes.editor} ${settings.layout === 'output-only' ? classes.hidden : ''}`}
            >
                <ContractManagement onResize={() => null} />
                <div style={{ display: 'flex', flexGrow: 1 }}>
                    <Editor
                        onMount={handleEditorDidMount}
                        language="michelson"
                        value={contract?.code || ''}
                        options={{
                            automaticLayout: true,
                            selectOnLineNumbers: true,
                            tabSize: 4,
                            fontSize: settings.fontSize,
                        }}
                        actions={[
                            {
                                // An unique identifier of the contributed action.
                                id: 'run-code',
                                // A label of the action that will be presented to the user.
                                label: 'Run Code',
                                // An optional array of keybindings for the action.
                                keybindings: [KeyMod.CtrlCmd | KeyCode.Enter],

                                contextMenuGroupId: 'utils',

                                contextMenuOrder: 1.5,
                                run: compileContract,
                            },
                        ]}
                        onChange={onInput}
                    />
                </div>
            </Grid>
        );
    };

    return (
        <div className={classes.root}>
            <EditorToolBar
                clearOutputs={clearOutputs}
                downloadOutputPanel={downloadOutput}
                compileContract={compileContract}
                selectedContract={contract}
                baseUrl={`${window.location.origin}${process.env.PUBLIC_URL}/michelson`}
                settingsMenu={<SettingsMenuItem />}
                templatesMenu={<TemplatesMenuItem />}
                setVolatileContract={setVolatileContract}
                syntax={SYNTAX.MICHELSON}
            />
            <div className={classes.editorSection}>
                <Grid container justifyContent="center">
                    {showEditor()}
                    <Grid
                        item
                        xs={getGridSm()}
                        className={`${classes.outputPanel} ${getBorderClass()} ${
                            settings.layout === 'editor-only' ? classes.hidden : ''
                        }`}
                        ref={outputPanelRef}
                    >
                        <Alert
                            variant="outlined"
                            severity={firstMessage ? 'error' : 'info'}
                            className={classes.errorMessage}
                        >
                            {firstMessage || t('michelsonIde.noErrors')}
                        </Alert>
                        <div>
                            <OutputPanel output={htmlOutput} isContractCompiling={compiling} shouldUpdateTabs={true} />
                        </div>
                    </Grid>
                </Grid>
            </div>
        </div>
    );
};

export default EditorView;
