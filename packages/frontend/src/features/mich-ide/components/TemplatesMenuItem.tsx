import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { Theme } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';

// Local Elements
import TemplatesButton from '../../common/elements/TemplatesButton';

// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';
import { templates } from '../constants/templates';
import actions from '../actions';

const TemplatesMenuItem: React.FC = () => {
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef<HTMLButtonElement>(null);
    const onlyIcon = useMediaQuery((theme: Theme) => theme.breakpoints.down('lg'));
    const t = useTranslation();
    const dispatch = useDispatch();

    const handleToggle = () => {
        setOpen((prevState) => !prevState);
    };

    const handleClose = (event: React.MouseEvent<EventTarget>) => {
        if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
            return;
        }
        setOpen(false);
    };

    const setVolatileContract = (code: string) => {
        dispatch(actions.setVolatileContract(code));
    };

    return (
        <React.Fragment>
            <TemplatesButton
                ref={anchorRef}
                label={t('ide.templatesMenu.label')}
                aria-controls="templates-menu"
                aria-haspopup="true"
                onClick={handleToggle}
                onlyIcon={onlyIcon}
            />
            <Menu
                anchorEl={anchorRef.current}
                keepMounted
                open={Boolean(open)}
                onClose={handleClose}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                {templates.map(({ name, code }) => (
                    <MenuItem key={name} onClick={() => setVolatileContract(code)}>
                        <ListItemText primary={name} />
                    </MenuItem>
                ))}
            </Menu>
        </React.Fragment>
    );
};

export default TemplatesMenuItem;
