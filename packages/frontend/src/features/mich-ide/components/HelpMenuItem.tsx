import React from 'react';

// Material UI
import MenuItem from '@material-ui/core/MenuItem';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';

// Local Components
import HelpMenuFab from '../../common/components/HelpMenuFab';

// Local Utils
import useTranslation from '../../i18n/hooks/useTranslation';

const HelpMenuItem: React.FC = () => {
    const t = useTranslation();
    return (
        <>
            <HelpMenuFab>
                <MenuItem component={Link} href="https://tezos.gitlab.io/michelson-reference" target="_blank">
                    <Typography variant="inherit">
                        {t('michelsonIde.helpMenu.michelsonReferenceByNomadicLabs')}
                    </Typography>
                </MenuItem>
            </HelpMenuFab>
        </>
    );
};

export default HelpMenuItem;
