import React from 'react';
import { Route, RouteProps } from 'react-router';

import Wallet from '../../wallet/views/Wallet';
import Nodes from '../../nodes/views/Nodes';
import Help from '../../help/views/Help';
import RouteWithNavigation from '../containers/RouteWithNavigation';
import NotFound from '../../not-found/views/NotFound';

interface SmartPyRoute {
    title: string;
    Route: typeof Route | typeof RouteWithNavigation;
    routeProps: RouteProps;
    Component: React.FC<any>;
}

export default [
    // Root
    {
        title: 'Home - SmartPy',
        Route: Route,
        routeProps: {
            exact: true,
            path: '/',
        },
        Component: React.lazy(() => import('../../home/views/Home')),
    },
    // IDE
    {
        title: 'Editor - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/ide/:cid?' },
        Component: React.lazy(() => import('../../py-ide/containers/Editor')),
    },
    // Typescript IDE
    {
        title: 'Editor - SmartTS',
        Route: RouteWithNavigation,
        routeProps: { path: '/ts-ide/:cid?' },
        Component: React.lazy(() => import('../../ts-ide/containers/Editor')),
    },
    // Ocaml IDE
    {
        title: 'Editor - SmartML',
        Route: RouteWithNavigation,
        routeProps: { path: '/ml-ide/:cid?' },
        Component: React.lazy(() => import('../../ml-ide/containers/Editor')),
    },
    // Origination
    {
        title: 'Origination - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/origination' },
        Component: React.lazy(() => import('../../origination/containers/Origination')),
    },
    // Michelson Helper
    {
        title: 'Michelson - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/michelson' },
        Component: React.lazy(() => import('../../mich-ide/containers/MichelsonEditor')),
    },
    // Help
    {
        title: 'Help - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/help' },
        Component: Help,
    },
    // Wallet
    {
        title: 'Wallet - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/wallet' },
        Component: Wallet,
    },
    // Nodes Monitor
    {
        title: 'Nodes - SmartPy',
        Route: RouteWithNavigation,
        routeProps: { path: '/nodes' },
        Component: Nodes,
    },
    // Not Found Page
    {
        title: 'Nodes - Not Found',
        Route: RouteWithNavigation,
        routeProps: { path: '*' },
        Component: NotFound,
    },
] as SmartPyRoute[];
