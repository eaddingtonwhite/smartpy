import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter, RouteComponentProps } from 'react-router-dom';
// Material UI
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';
import CodeIcon from '@material-ui/icons/Code';
import Search from '@material-ui/icons/Search';
import ContactSupport from '@material-ui/icons/ContactSupport';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ButtonGroup from '@material-ui/core/ButtonGroup';

// Local Icons
import TezosWallet from '../../common/elements/icons/TezosWallet';
// Local Elements
import DarkLightSwitch from '../../theme/components/DarkLightSwitch';
import RouterButton from '../elements/RouterButton';
import Logo from '../../common/elements/Logo';

// Local Utils
import { version } from '../../../../package.json';
import { getBase } from '../../../utils/url';
import TypescriptIcon from '../../common/elements/icons/TypeScript';
import PythonIcon from '../../common/elements/icons/Python';
import OcamlIcon from '../../common/elements/icons/Ocaml';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appBar: {
            position: 'relative',
            height: 80,
            backgroundColor: 'transparent',
            boxShadow: 'none',
        },
        logoSection: {
            flexGrow: 1,
            display: 'flex',
            padding: 20,
        },
        middleSection: {
            flexGrow: 1,
            display: 'flex',
            padding: 20,
        },
        gridMenu: {
            width: 'auto',
        },
        paper: {
            display: 'flex',
            backgroundColor: theme.palette.background.default,
            borderBottomWidth: 2,
            borderBottomStyle: 'solid',
            borderBottomColor: theme.palette.primary.light,
        },
        button: {
            minWidth: 140,
            backgroundColor: theme.palette.background.paper,
            borderWidth: 2,
            '&:hover': {
                borderWidth: 2,
            },
        },
        buttonGroupItem: {
            backgroundColor: theme.palette.background.paper,
            '&:not(:last-child)': {
                marginLeft: 0,
                border: '1px solid rgba(0, 123, 255, 0.5)',
            },
        },
        groupedOutlinedHorizontal: {
            borderColor: 'inherit',
        },
        groupedOutlinedDisabled: {
            borderWidth: 2,
            '&:disabled': {
                borderWidth: 2,
            },
        },
        lowOpacify: {
            opacity: 0.6,
        },
        activeButton: {
            backgroundColor: theme.palette.background.paper,
            color: theme.palette.mode === 'dark' ? '#66aaff' : '#0160cc',
        },
        logoParagraph: {
            margin: '-10px 0 0 40px',
            fontVariant: 'small-caps',
            fontFamily: 'Verdana, sans-serif',
            color: theme.palette.mode === 'dark' ? '#66aacc' : '#01608c',
        },
        logoLink: {
            textDecoration: 'none',
        },
        version: {
            position: 'absolute',
            left: 200,
            top: 28,
            fontSize: '8pt',
            color: theme.palette.mode === 'dark' ? '#FFF' : '#000',
            textDecoration: 'none',
        },
    }),
);

const menuButtons = [
    {
        component: RouterButton,
        props: {
            fullWidth: true,
            variant: 'outlined',
            size: 'large',
            startIcon: <CodeIcon />,
            to: '/ide',
            match: /.*(ide)|(michelson).*/,
        } as any,
        text: 'Editor',
    },
    {
        component: Button,
        props: {
            fullWidth: true,
            href: `${getBase()}/explorer.html`,
            variant: 'outlined',
            size: 'large',
            startIcon: <Search />,
            match: /.*(explorer.html).*/,
        } as any,
        text: 'Explorer',
    },
    {
        component: Button,
        props: {
            fullWidth: true,
            href: `${getBase()}/wallet.html`,
            variant: 'outlined',
            size: 'large',
            startIcon: <TezosWallet />,
            match: /.*(wallet.html).*/,
        } as any,
        text: 'Wallet',
    },
    {
        component: RouterButton,
        props: {
            fullWidth: true,
            variant: 'outlined',
            size: 'large',
            startIcon: <ContactSupport />,
            to: '/help',
            match: /.*(\/help).*/,
        } as any,
        text: 'Help',
    },
];

const Navigation: React.FC<RouteComponentProps> = (props) => {
    const showWideMenu = useMediaQuery('(min-width:1300px)');
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const classes = useStyles();

    const {
        location: { pathname },
    } = props;

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const wideMenuItems = (
        <>
            {menuButtons.map((item, index) => {
                item.props.className = classes.button;
                if (item.props.match && pathname.match(item.props.match)) {
                    item.props.className = `${item.props.className} ${classes.activeButton}`;
                }
                return (
                    <Grid item key={index}>
                        <item.component {...item.props}>{item.text}</item.component>
                    </Grid>
                );
            })}
        </>
    );

    const mobileMenuItems = menuButtons.map((item, index) => {
        item.props.className = classes.button;
        if (item.props.match && pathname.match(item.props.match)) {
            item.props.className = `${item.props.className} ${classes.activeButton}`;
        }
        return (
            <MenuItem key={index}>
                <item.component {...item.props}>{item.text}</item.component>
            </MenuItem>
        );
    });

    const middleSection = React.useMemo(() => {
        const isPythonIDE = pathname.includes('/ide');
        const isTypescriptIDE = pathname.includes('/ts-ide');
        const isOcamlIDE = pathname.includes('/ml-ide');
        const isMichelsonIDE = pathname.includes('/michelson');
        if (isPythonIDE || isTypescriptIDE || isMichelsonIDE || isOcamlIDE) {
            return (
                <div className={classes.middleSection} aria-label="outlined button group">
                    <DarkLightSwitch />
                    <ButtonGroup variant="outlined">
                        <RouterButton
                            fullWidth
                            variant="outlined"
                            size="large"
                            to="/ide"
                            className={isPythonIDE ? '' : classes.lowOpacify}
                            classes={{ root: classes.buttonGroupItem }}
                        >
                            <PythonIcon />
                        </RouterButton>
                        <RouterButton
                            fullWidth
                            variant="outlined"
                            size="large"
                            to="/ts-ide"
                            className={isTypescriptIDE ? '' : classes.lowOpacify}
                            classes={{ root: classes.buttonGroupItem }}
                        >
                            <TypescriptIcon />
                        </RouterButton>
                        {process.env.NODE_ENV === 'development' ? (
                            <RouterButton
                                fullWidth
                                variant="outlined"
                                size="large"
                                to="/ml-ide"
                                className={isOcamlIDE ? '' : classes.lowOpacify}
                                classes={{ root: classes.buttonGroupItem }}
                            >
                                <OcamlIcon />
                            </RouterButton>
                        ) : null}
                        <RouterButton
                            fullWidth
                            variant="outlined"
                            size="large"
                            to="/michelson"
                            className={isMichelsonIDE ? '' : classes.lowOpacify}
                            classes={{ root: classes.buttonGroupItem }}
                        >
                            <img alt="Michelson Editor" src={`${getBase()}/static/img/michelson_logo.png`} width={24} />
                        </RouterButton>
                    </ButtonGroup>
                </div>
            );
        }

        return <DarkLightSwitch />;
    }, [classes.buttonGroupItem, classes.lowOpacify, classes.middleSection, pathname]);

    return (
        <React.Fragment>
            <AppBar className={classes.appBar}>
                <Toolbar className={classes.paper}>
                    <div className={classes.logoSection}>
                        <Link to="/" className={classes.logoLink}>
                            <Logo />
                            <p className={classes.logoParagraph}>by Smart Chain Arena</p>
                        </Link>

                        <Typography
                            variant="caption"
                            className={classes.version}
                            component="a"
                            href={`${getBase()}/docs/releases`}
                        >
                            {version}
                        </Typography>
                    </div>
                    {middleSection}
                    {showWideMenu ? (
                        <Grid spacing={1} container className={classes.gridMenu}>
                            {wideMenuItems}
                        </Grid>
                    ) : (
                        <React.Fragment>
                            <IconButton
                                aria-label="menu"
                                aria-controls="menu"
                                aria-haspopup="true"
                                onClick={handleClick}
                            >
                                <MenuIcon fontSize="large" />
                            </IconButton>
                            <Menu
                                id="mobile-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                            >
                                {mobileMenuItems}
                            </Menu>
                        </React.Fragment>
                    )}
                </Toolbar>
            </AppBar>
        </React.Fragment>
    );
};

export default withRouter(Navigation);
