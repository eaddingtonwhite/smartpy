import React from 'react';
// Material UI
import { makeStyles, createStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

import SmartPyIcon from '../../common/elements/icons/SmartPy';
import { Typography } from '@material-ui/core';

const useStyles = (margin = 0, size: number) =>
    makeStyles(() =>
        createStyles({
            root: {
                position: 'relative',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                flexGrow: 1,
                margin,
                height: '100%',
            },
            container: {
                position: 'relative',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                margin,
            },
            icon: {
                position: 'absolute',
                width: size,
                height: size,
                opacity: 0.8,
            },
        }),
    );

interface OwnProps {
    loading?: boolean;
    msg?: string;
    margin?: number;
    size?: number;
}

const CircularProgressWithText: React.FC<OwnProps> = ({ msg, loading = true, margin, size = 200 }) => {
    const classes = useStyles(margin, size / 2)();

    return loading ? (
        <div className={classes.root}>
            <div className={classes.container}>
                <CircularProgress size={size} />
                <SmartPyIcon className={classes.icon} />
            </div>
            {msg ? <Typography variant="caption">{msg}</Typography> : null}
        </div>
    ) : null;
};

export default CircularProgressWithText;
