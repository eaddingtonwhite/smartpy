import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Menu from '@material-ui/core/Menu';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Switch from '@material-ui/core/Switch';

import { IDELayout } from 'SmartPyModels';
import { useSettings } from '../selectors/setting';
import actions from '../actions';

// Local Hooks
import useTranslation from '../../i18n/hooks/useTranslation';

// Local Elements
import SettingsButton from '../../common/elements/SettingsButton';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        menuRoot: {
            width: 230,
            margin: theme.spacing(1),
        },
        select: {
            padding: 12,
            minWidth: 90,
        },
        menuItem: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            margin: theme.spacing(1),
        },
        settingSection: {
            fontWeight: 'bold',
        },
    }),
);

const SettingsMenuItem: React.FC = () => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef<HTMLButtonElement>(null);
    const settings = useSettings();
    const dispatch = useDispatch();
    const t = useTranslation();

    const onlyIcon = useMediaQuery((theme: Theme) => theme.breakpoints.down('lg'));

    const handleToggle = () => {
        setOpen((prevState) => !prevState);
    };

    const handleClose = (event: React.MouseEvent<EventTarget>) => {
        if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
            return;
        }
        setOpen(false);
    };

    const handleLayoutChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        dispatch(actions.updateSettings({ layout: event.target.value as IDELayout }));
    };

    const handleFontSizeChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        dispatch(actions.updateSettings({ fontSize: Number(event.target.value) }));
    };

    const handleNewcomersModeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(actions.updateSettings({ newcomersMode: event.target.checked }));
    };

    return (
        <React.Fragment>
            <SettingsButton
                ref={anchorRef}
                aria-controls="settings-menu"
                aria-haspopup="true"
                onClick={handleToggle}
                onlyIcon={onlyIcon}
            />
            <Menu
                anchorEl={anchorRef.current}
                keepMounted
                open={Boolean(open)}
                onClose={handleClose}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                <div className={classes.menuRoot}>
                    <Typography variant="overline" gutterBottom className={classes.settingSection}>
                        UI
                    </Typography>
                    <Divider />
                    <div className={classes.menuItem}>
                        <Typography variant="caption">{t('ide.settings.layout')}</Typography>
                        <Select
                            variant="filled"
                            value={settings.layout}
                            classes={{ filled: classes.select }}
                            onChange={handleLayoutChange}
                        >
                            <MenuItem value="side-by-side">{t('ide.settings.sideBySide')}</MenuItem>
                            <MenuItem value="stacked">{t('ide.settings.stacked')}</MenuItem>
                            <MenuItem value="editor-only">{t('ide.settings.editorOnly')}</MenuItem>
                            <MenuItem value="output-only">{t('ide.settings.outputOnly')}</MenuItem>
                        </Select>
                    </div>
                    <div className={classes.menuItem}>
                        <Typography variant="caption">{t('ide.settings.fontSize')}</Typography>
                        <Select
                            variant="filled"
                            value={settings.fontSize || 14}
                            classes={{ filled: classes.select }}
                            onChange={handleFontSizeChange}
                        >
                            <MenuItem value="12">12px</MenuItem>
                            <MenuItem value="14">14px</MenuItem>
                            <MenuItem value="16">16px</MenuItem>
                        </Select>
                    </div>
                    <Typography variant="overline" gutterBottom className={classes.settingSection}>
                        {t('ide.settings.helpers')}
                    </Typography>
                    <Divider />
                    <div className={classes.menuItem}>
                        <Typography variant="caption">{t('ide.settings.newcomersMode')}</Typography>
                        <Switch
                            color="primary"
                            checked={settings.newcomersMode}
                            onChange={handleNewcomersModeChange}
                            name="newComersMode"
                            inputProps={{ 'aria-label': t('ide.settings.newcomersMode') }}
                        />
                    </div>
                </div>
            </Menu>
        </React.Fragment>
    );
};

export default SettingsMenuItem;
