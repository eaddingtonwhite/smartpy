export interface TemplateProps {
    fileName: string;
    name: string;
    description?: string;
}

export type Templates = {
    [section: string]: TemplateProps[];
};

export type TemplatesObj = {
    [template: string]: TemplateProps;
};

export const syntaxTemplates: Templates = {
    'Syntax Examples': [],
    'Templates per Type': [],
    'Misc Features': [],
};

export const contractTemplates: Templates = {
    'Token Contracts': [],
    'Simple Examples': [],
};

export const getAllTemplates = (): TemplatesObj => {
    const availableContractTemplates = Object.keys(contractTemplates).reduce(
        (state, key) => [...state, ...contractTemplates[key]],
        [] as TemplateProps[],
    );
    const availableSyntaxTemplates = Object.keys(syntaxTemplates).reduce(
        (state, key) => [...state, ...syntaxTemplates[key]],
        [] as TemplateProps[],
    );

    return [...availableContractTemplates, ...availableSyntaxTemplates].reduce(
        (state, template) => ({ ...state, [template.fileName]: template }),
        {},
    );
};
