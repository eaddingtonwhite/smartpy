import pako from 'pako';

import { copyToClipboard } from '../../../utils/clipboard';
import toast from '../../../services/toast';

/**
 * Evaluate the code in the editor
 *
 * @param {string} code source code
 * @param {boolean} withTests - When true, the evaluation will run with tests.
 */
export const evalRun = async (code: string, withTests = true): Promise<{ kind: string; name: string }[]> => {
    return window.smartmlCtx.call('runSmartMLScript', code);
};

/**
 * Evaluate a specific test.
 *
 * @param {string} testName - Name of the Tests to evaluate.
 */
export const evalTest = async (testName: string) => {
    try {
        window.evalTest(testName);
    } catch (error) {
        window.showTraceback(error, String(error));
    }
};

export const getEmbeddedLink = (contract: string) => {
    const encoded = btoa(pako.deflate(contract, { to: 'string' }))
        .replace(/\+/g, '@')
        .replace(/\//g, '_')
        .replace(/=/g, '-');
    return `${window.location.origin}/ts-ide?code=${encoded}`;
};

export const handleTemplateShare = (fileName: string) => {
    copyToClipboard(`${window.location.origin}/ts-ide/?template=${fileName}`);
    toast.info('The template url was copied!');
};
