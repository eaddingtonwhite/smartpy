declare module 'SmartPyModels' {
    export type AccountInformation = {
        isLoading?: boolean;
        revealRequired?: boolean;
        revealing?: boolean;
        activating?: boolean;
        activationRequired?: boolean;
        errors?: string;
        source?: import('./constants/sources').AccountSource;
        pkh?: string;
        balance?: number;
        network?: string;
    };
    export type NetworkInformation = {
        network?: string;
        rpc: string;
    };
}

declare module 'SmartPyWalletTypes' {
    export type WalletStatus = {
        temple: boolean;
    };
    export type ScriptParams = {
        code: string;
        storage: string;
    };
    export type OperationCostsEstimation = {
        fee: number;
        gasLimit: number;
        storageLimit: number;
    };
    export type OriginationResults = Promise<{
        hash: string;
        results: import('conseiljs').AlphaOperationContentsAndResult[];
        contractAddress?: string;
        monitor: import('rxjs').Observable<number>;
    }>;
    export type OriginationInformation = {
        bytes: string;
        blockHash: string;
        operationContents: import('conseiljs').Origination;
        blake2bHash: string;
        send: () => Promise<OriginationResults>;
    };
}
