declare module 'SmartPyModels' {
    export type IDELayout = 'side-by-side' | 'stacked' | 'editor-only' | 'output-only';
    export type IDEContract = {
        id?: string;
        name?: string;
        code?: string;
        shared?: boolean;
        updatedAt?: string;
    };
    export type IDESettings = {
        layout?: IDELayout;
        newcomersMode?: boolean;
        fontSize?: number;
    };
}
