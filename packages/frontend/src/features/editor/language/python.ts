import * as Monaco from 'monaco-editor/esm/vs/editor/editor.api';
import { languages } from 'monaco-editor/esm/vs/editor/editor.api';
import pythonCompleters from './completers/python';

const completers = pythonCompleters.map(buildCompleter);

class PythonLanguage {
    static register(monaco: typeof Monaco) {
        // Register a completion item provider for the language
        // Register new completionItemProvider on Monaco's language import
        monaco.languages.registerCompletionItemProvider('tmPython', {
            provideCompletionItems: (model, position) => {
                const word = model.getWordUntilPosition(position);

                const range = {
                    startLineNumber: position.lineNumber,
                    endLineNumber: position.lineNumber,
                    startColumn: word.startColumn,
                    endColumn: word.endColumn,
                };
                const textUntilPosition = model.getValueInRange({
                    startLineNumber: 1,
                    startColumn: 1,
                    endLineNumber: position.lineNumber,
                    endColumn: position.column,
                });
                const match = textUntilPosition.match(/(sp)[.]?\w*$/g);

                if (match?.[0]) {
                    return {
                        suggestions: completers.reduce<languages.CompletionItem[]>(
                            (acc, cur) => (cur.sortText?.includes(match[0]) ? [...acc, { ...cur, range }] : acc),
                            [],
                        ),
                    };
                }
            },
        });
    }
}

export default PythonLanguage;

function buildCompleter({
    label,
    detail,
    caption,
}: {
    label: string;
    detail: string;
    caption: string;
}): Omit<languages.CompletionItem, 'range'> {
    let kind = Monaco.languages.CompletionItemKind.Unit;

    if (detail.includes('[SmartPy type]')) {
        kind = Monaco.languages.CompletionItemKind.TypeParameter;
    }
    if (detail.includes('[SmartPy constant]')) {
        kind = Monaco.languages.CompletionItemKind.Variable;
    }
    if (detail.includes('[SmartPy function]')) {
        kind = Monaco.languages.CompletionItemKind.Function;
    }
    if (detail.includes('[SmartPy decorator]')) {
        kind = Monaco.languages.CompletionItemKind.Event;
    }
    if (detail.includes('[SmartPy literal]')) {
        kind = Monaco.languages.CompletionItemKind.Field;
    }
    return {
        label,
        detail,
        sortText: caption,
        insertText: label,
        kind,
    };
}
