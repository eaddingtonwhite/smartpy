import React from 'react';
import MonacoEditor from '@monaco-editor/react';
import type { EditorProps, OnMount } from '@monaco-editor/react';
import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';

import { Registry } from 'monaco-textmate'; // peer dependency
import { wireTmGrammars } from 'monaco-editor-textmate';
import selectors from '../../../store/selectors';

// React Ace IDE
import AceEditor from 'react-ace';
import 'ace-builds/src-min-noconflict/mode-python';
import 'ace-builds/src-min-noconflict/mode-typescript';
import 'ace-builds/src-min-noconflict/theme-chrome';
import 'ace-builds/src-min-noconflict/theme-monokai';

const registry = new Registry({
    getGrammarDefinition: async (scopeName) => {
        if (scopeName.match(/.*.python/g)) {
            const pythonGrammar = await import('../grammars/MagicPython.tmLanguage.json');
            return {
                format: 'json',
                content: pythonGrammar.default,
            };
        }
        if (scopeName.match(/.*.ocaml/g)) {
            const ocamlGrammar = await import('../grammars/ocaml.tmLanguage.json');
            return {
                format: 'json',
                content: ocamlGrammar.default,
            };
        }
        if (scopeName.match(/.*.typescript/g)) {
            const typescriptGrammar = await import('../grammars/typescript.tmLanguage.json');
            return {
                format: 'json',
                content: typescriptGrammar.default,
            };
        }
        if (scopeName.match(/.*.michelson/g)) {
            const michelsonGrammar = await import('../grammars/michelson.tmLanguage.json');
            return {
                format: 'json',
                content: michelsonGrammar.default,
            };
        }
        return console.error('This should never happen.', scopeName) as never;
    },
});

interface OwnProps {
    actions?: editor.IActionDescriptor[];
}

const Editor: React.FC<EditorProps & OwnProps> = ({ actions = [], ...props }) => {
    const isDarkMode = selectors.theme.useThemeMode();

    const onMount: OnMount = React.useCallback(
        async (editor, monaco) => {
            // monaco "language id's" to TextMate scopeNames
            const grammars = new Map();
            grammars.set('tmPython', 'source.python');
            grammars.set('ocaml', 'source.ocaml');
            grammars.set('michelson', 'source.michelson');
            grammars.set('typescript', 'source.typescript');

            await wireTmGrammars(monaco, registry, grammars, editor);

            // Add Actions
            actions.forEach((action) => {
                editor.addAction(action);
            });
            // Propagate
            props.onMount?.(editor, monaco);
            window.editor = editor;
        },
        [actions, props],
    );

    return (
        <div style={{ display: 'flex', flexGrow: 1, overflow: 'hidden' }}>
            {/webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? (
                <AceEditor
                    debounceChangePeriod={1000}
                    mode={props.language}
                    theme={isDarkMode ? 'monokai' : 'chrome'}
                    onLoad={(editor) => {
                        (props.onMount as any)?.(editor);
                    }}
                    setOptions={{
                        enableBasicAutocompletion: true,
                        enableLiveAutocompletion: false,
                        showLineNumbers: true,
                        tabSize: 4,
                        useWorker: true,
                        autoScrollEditorIntoView: true,
                        showPrintMargin: false,
                        useSoftTabs: true,
                        showGutter: true,
                        wrap: true,
                        highlightActiveLine: true,
                    }}
                    value={props.value}
                    editorProps={{ $blockScrolling: true }}
                    width="100%"
                    height="auto"
                    onChange={props.onChange as any}
                />
            ) : (
                <MonacoEditor
                    {...{
                        ...props,
                        onMount,
                        theme: isDarkMode ? 'vs-code-dark' : 'vs-code-light',
                        options: {
                            ...(props.options || {}),
                            quickSuggestions: false,
                        },
                    }}
                />
            )}
        </div>
    );
};

export default Editor;
