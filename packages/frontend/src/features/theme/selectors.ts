import { useSelector } from 'react-redux';
import { RootState } from 'SmartPyTypes';

export const useThemeMode = () => useSelector<RootState, boolean>((state: RootState) => state.theme.darkMode);
