import React from 'react';
import { Link } from 'react-router-dom';

// Material UI
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import { SvgIconProps } from '@material-ui/core/SvgIcon';
import Chip from '@material-ui/core/Chip';
import Divider from '@material-ui/core/Divider';

// Local Components
import CodeBlock from '../../common/components/CodeBlock';
import SelectionOfTemplates from '../../common/components/SelectionOfTemplates';

// Local Constants
import samples from '../constants/samples';
import { getBase } from '../../../utils/url';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            margin: 20,
        },
        card: {
            borderWidth: 1,
            borderStyle: 'solid',
            borderColor: theme.palette.primary.main,
            boxShadow: `5px 5px 0 0 ${theme.palette.primary.main}`,
            marginBottom: 20,
        },
        title: {
            color: theme.palette.primary.main,
            fontWeight: 'bold',
            margin: 20,
        },
        description: {
            color: theme.palette.primary.light,
            margin: 20,
        },
        exampleGrid: {
            marginBottom: 20,
        },
        arrow: {
            display: 'flex',
            justifyContent: 'center',
        },
        description2: {
            fontWeight: 'bold',
            marginBottom: 20,
        },
        innerDescription: {
            fontWeight: 'bold',
            margin: 10,
        },
        list: {
            width: '100%',
            backgroundColor: theme.palette.background.default,
            padding: 0,
        },
    }),
);

const ArrowIcon = (props: SvgIconProps) => {
    const theme = useTheme();
    const isDown = useMediaQuery(theme.breakpoints.up('lg'));

    return isDown ? <ArrowForwardIcon {...props} /> : <ArrowDownwardIcon {...props} />;
};

const Introduction: React.FC = () => {
    const classes = useStyles();
    return (
        <div className={classes.root} id="introduction">
            <Typography variant="h4" className={classes.title}>
                Introduction
            </Typography>
            <Typography gutterBottom variant="h6" className={classes.description}>
                The SmartPy language is available through a Python library for building and analyzing Tezos smart
                contracts.
            </Typography>
            <Typography gutterBottom variant="body1" className={classes.description}>
                It comes with various tools: a compiler that generates Michelson code, a simulation engine, a contract
                explorer, etc.
            </Typography>
            <Grid container spacing={2} alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <Paper className={classes.card}>
                        <Typography variant="h6" className={classes.title}>
                            Contract Example
                        </Typography>
                        <Typography gutterBottom variant="body1" className={classes.description}>
                            This is a very simple contract called &quot;StoreValue&quot; which stores some
                            &quot;value&quot; and enables its users to either replace it by calling the replace entry
                            point or double it by calling double.
                        </Typography>
                        <Grid container alignItems="center" justifyContent="center" className={classes.exampleGrid}>
                            <Grid item xs={11} lg={5}>
                                <CodeBlock language="python" square={false} showLineNumbers text={samples.code} />
                            </Grid>
                            <Grid item xs={11} lg={1}>
                                <div className={classes.arrow}>
                                    <ArrowIcon fontSize="large" color="primary" />
                                </div>
                            </Grid>
                            <Grid item xs={11} lg={5}>
                                <CodeBlock language="perl" square={false} showLineNumbers text={samples.michelson} />
                            </Grid>
                        </Grid>
                    </Paper>
                    <SelectionOfTemplates>
                        <Divider />
                        <Typography variant="body1" align="justify" color="textPrimary" className={classes.description}>
                            The documentation, including our{' '}
                            <Chip
                                label="Reference Manual"
                                color="primary"
                                size="small"
                                variant="outlined"
                                clickable
                                component="a"
                                href={`${getBase()}/docs`}
                            />
                            , is accessible through the{' '}
                            <Chip
                                label="Help"
                                color="primary"
                                size="small"
                                variant="outlined"
                                clickable
                                component={Link}
                                to="/help"
                            />{' '}
                            section.
                        </Typography>
                    </SelectionOfTemplates>
                </Grid>
            </Grid>
        </div>
    );
};

export default Introduction;
