import { Network } from '../constants/networks';
import httpRequester from '../services/httpRequester';
import logger from '../services/logger';

/**
 * @description Get the RPC network.
 * @param {string} rpc - RPC address ( e.g. https://mainnet.smartpy.io )
 * @return {string} One of the following networks [MAINNET, EDONET, FLORENCENET]
 */
export const getRpcNetwork = async (rpc: string) => {
    const {
        network_version: { chain_name },
    } = await httpRequester
        .get(`${rpc}/version`)
        .then(({ data }) => data)
        .catch(async (e) => {
            logger.warn(e);
            return {
                network_version: await httpRequester.get(`${rpc}/network/version`),
            };
        });

    const network = chain_name.split('_')[1];
    if (!Object.keys(Network).includes(network)) {
        logger.error(`Unknown network: ${network}.`);
    }

    return network;
};
