const WorkerPlugin = require('worker-plugin');
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');

const routes = ['ide$', 'michelson$', 'nodes$', 'wallet$'];

module.exports = {
    webpack: {
        plugins: [new WorkerPlugin(), new MonacoWebpackPlugin({ languages: [] })],
        configure: {
            module: {
                rules: [
                    {
                        test: /\.wasm$/,
                        type: 'javascript/auto',
                    },
                ],
            },
        },
    },
    devServer: (devServerConfig) => ({
        ...devServerConfig,
        historyApiFallback: true,
        proxy: routes.reduce(
            (state, route) => ({
                ...state,
                [`/${route}`]: {
                    target: `${process.env.HTTPS === 'true' ? 'https' : 'http'}://[::1]:${process.env.PORT || 3000}`,
                    pathRewrite: { [`^/${route}`]: '' },
                    secure: false,
                    changeOrigin: true,
                },
            }),
            {},
        ),
    }),
};
