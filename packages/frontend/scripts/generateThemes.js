const { convertTheme } = require('monaco-vscode-textmate-theme-converter'); // cjs module
const fs = require('fs');

const MONACO_DIR = './ide-themes/monaco';

const inTheme = {
    /** json */
};
const outTheme = convertTheme(inTheme);
fs.writeFileSync(`${MONACO_DIR}/dark.json`, JSON.stringify(outTheme, null, 4), { encoding: 'utf-8' });
