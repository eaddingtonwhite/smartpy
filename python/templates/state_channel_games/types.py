import smartpy as sp

class BytesModel:
    t_init_input = sp.TBytes
    t_game_state = sp.TBytes
    t_move_data  = sp.TBytes

class Types:
    def __init__(self, model = BytesModel):
        self.t_init_input   = model.t_init_input
        self.t_game_state   = model.t_game_state
        self.t_move_data    = model.t_move_data

        self.t_nonce          = sp.TString
        self.t_model_id       = sp.TBytes
        self.t_token_map      = sp.TMap(sp.TNat, sp.TNat)
        self.t_game_bonds     = sp.TMap(sp.TInt, self.t_token_map)
        self.t_players_addr   = sp.TMap(sp.TInt, sp.TAddress)
        self.t_metadata       = sp.TMap(sp.TString, sp.TBytes)

        self.t_transfer       = sp.TRecord(
            sender = sp.TInt,
            receiver = sp.TInt,
            bonds = self.t_token_map
        )
        self.t_outcome        = sp.TVariant(
            game_finished        = sp.TString,
            player_inactive      = sp.TInt,
            player_double_played = sp.TInt
        ).layout(("game_finished", ("player_double_played", "player_inactive")))

        self.t_settlements  = sp.TMap(self.t_outcome, sp.TList(self.t_transfer))

        # Games #

        self.t_player = sp.TRecord(addr = sp.TAddress, pk = sp.TKey)

        self.t_constants = sp.TRecord(
            bonds                   = self.t_game_bonds,
            channel_id              = sp.TBytes,
            game_nonce              = self.t_nonce,
            model_id                = self.t_model_id,
            play_delay              = sp.TInt,
            players_addr            = self.t_players_addr,
            settlements             = self.t_settlements
        ).right_comb()

        self.t_current = sp.TRecord(
            move_nb = sp.TNat,
            player  = sp.TInt,
            outcome = sp.TOption(self.t_outcome),
        ).right_comb()

        self.t_timeouts = sp.TMap(sp.TInt, sp.TTimestamp)

        self.t_game = sp.TRecord(
            addr_players = sp.TMap(sp.TAddress, sp.TInt),
            constants    = self.t_constants,
            current      = self.t_current,
            init_input   = self.t_init_input,
            state        = sp.TOption(self.t_game_state),
            settled      = sp.TBool,
            timeouts     = self.t_timeouts,
            metadata     = self.t_metadata
        ).right_comb()

        # Models

        self.model_outcomes   = sp.TList(sp.TString)

        self.t_apply_input = sp.TRecord(
            move_data = self.t_move_data,
            move_nb   = sp.TNat,
            player    = sp.TInt,
            state     = self.t_game_state,
        ).right_comb()

        self.t_apply_result = sp.TPair(
            self.t_game_state,
            sp.TOption(sp.TString),
        )

        self.t_init   = sp.TLambda(self.t_init_input, self.t_game_state)
        self.t_apply_ = sp.TLambda(self.t_apply_input, self.t_apply_result)

        self.t_model = sp.TRecord(
            init     = self.t_init,
            apply_   = self.t_apply_,
            outcomes = self.model_outcomes,
            metadata = self.t_metadata
        ).right_comb()

        self.t_model_lambdas = sp.TRecord(
            apply_   = self.t_apply_,
            init     = self.t_init,
            outcomes = self.model_outcomes,
        ).right_comb()

        self.t_model_wrap = sp.TRecord(
            name  = sp.TString,
            model = sp.TBytes
        ).right_comb()

        # Others

        self.t_fa2_transfer = sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs")))
