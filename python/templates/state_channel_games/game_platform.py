# State channel based game platform templates (work in progress)

import smartpy as sp

# N simultaneous games of M different models with code in a big_map of lambda

types = sp.io.import_template("state_channel_games/types.py").Types()

t_init_type = sp.TRecord(
    admins = sp.TSet(sp.TAddress),
    channels = sp.TBigMap(
        sp.TBytes,
        sp.TRecord(
            closed   = sp.TBool,
            nonce    = types.t_nonce,
            players  = sp.TMap(
                sp.TAddress,
                sp.TRecord(
                    bonds    = types.t_token_map,
                    pk       = sp.TKey,
                    withdraw = sp.TOption(
                        sp.TRecord(
                            challenge        = sp.TSet(sp.TBytes),
                            challenge_tokens = sp.TMap(sp.TNat, sp.TInt),
                            timeout          = sp.TTimestamp,
                            tokens           = types.t_token_map,
                        )
                    ),
                    withdraw_id = sp.TNat
                )
            ),
            withdraw_delay = sp.TInt,
        ),
    ),
    games = sp.TBigMap(
        sp.TBytes,
        types.t_game
    ),
    ledger = sp.TBigMap(
        sp.TPair(sp.TAddress,sp.TNat),
        sp.TRecord(balance = sp.TNat)
    ),
    models = sp.TBigMap(
        types.t_model_id,
        types.t_model
    ),
    metadata = sp.TBigMap(sp.TString, sp.TBytes),
    token_metadata = sp.TBigMap(sp.TNat, sp.TMap(sp.TString, sp.TBytes)),
    token_permissions = sp.TBigMap(sp.TNat, sp.TMap(sp.TString, sp.TBytes))
)

class Contract_Administration:
    @sp.entry_point
    def remove_admins(self, admins):
        sp.verify(self.data.admins.contains(sp.sender), "Platform_NotAdmin")
        sp.verify(~admins.contains(sp.sender), "Platform_CannotRemoveSelf")
        with sp.for_("admin", admins.elements()) as admin:
            self.data.admins.remove(admin)

    @sp.entry_point
    def add_admins(self, admins):
        sp.verify(self.data.admins.contains(sp.sender), "Platform_NotAdmin")
        with sp.for_("admin", admins.elements()) as admin:
            self.data.admins.add(admin)

class GamePlatform_OffChainViews:
    @sp.offchain_view(pure = True)
    def offchain_init(self, params):
        """Compute the init state from a model and init params."""
        sp.result(self.data.models[params.model_id].init(params.init_params))

    @sp.offchain_view(pure = True)
    def offchain_new_game(self, params):
        """Return a new game structure according to init params and model"""
        game_id = compute_game_id(params.constants)
        sp.set_type(params.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
        sp.result(self.new_game_(game_id, params.constants, params.params, params.signatures))

    @sp.offchain_view(pure = True)
    def offchain_init_game(self, params):
        """Initialize and return a game computed by the specified model or by the platform model if none"""
        game = sp.local('game', params.game).value

        sp.set_type(params.model, sp.TOption(types.t_model))
        sp.verify(~game.state.is_some(), "Platform_GameAlreadyInitialized")
        with sp.if_(params.model.is_some()):
            sp.result(params.model.open_some().init(game.init_input))
        with sp.else_():
            sp.verify(self.data.models.contains(params.model_id), message = sp.pair("Platform_ModelID_not_found", params.model_id))
            sp.result(self.data.models[params.model_id].init(game.init_input))

    @sp.offchain_view(pure = True)
    def offchain_check_move(self, params):
        """Check a move performed by another party."""
        game_id   = params.game_id
        game      = sp.local('game', params.game).value
        sender    = params.sender
        move_data = params.move_data
        move_nb   = params.move_nb
        new_state = params.new_state
        new_current  = params.new_current
        signature = params.signature
        sp.set_type(game.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))

        sp.verify(~game.settled, "Platform_GameSettled")
        self.game_play_(game, move_nb, move_data, sender)

        sp.verify(new_state == game.state.open_some("Plateform_GameNotInitialized"))
        sp.verify(new_current  == game.current)

        to_sign = action_new_state(game_id, game.current, new_state)
        pk = self.data.channels[game.constants.channel_id].players[sender].pk
        sp.verify(sp.check_signature(pk, signature, to_sign), message = "Platform_badSig")

        sp.result("OK")

    @sp.offchain_view(pure = True)
    def offchain_apply_move(self, params):
        """Compute a new state from a move."""
        game      = params.game
        move_nb   = params.move_nb
        move_data = params.move_data
        sender    = params.sender
        sp.verify(~game.settled, "Platform_GameSettled")
        self.check_move_current(game.current, game.constants, sender, move_nb)
        params = sp.record(
            move_data = move_data,
            move_nb   = move_nb,
            player    = game.current.player,
            state     = game.state.open_some("Plateform_GameNotInitialized")
        )
        params = sp.set_type_expr(params, types.t_apply_input)
        sp.result(self.data.models[game.constants.model_id].apply_(params))

    @sp.offchain_view(pure = True)
    def offchain_play(self, params):
        """Simulate a play from a determined sender, determined move_nb and move_data for a determined game."""
        sp.verify(params.game.current.outcome == sp.none, "Platform_NotRunning")
        sp.set_type(params.game.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
        game = sp.local('game', params.game).value
        sp.verify(~game.settled, "Platform_GameSettled")
        sp.result(self.game_play_(game, params.move_nb, params.move_data, params.sender))

class GamePlatform(sp.Contract,
                   Contract_Administration,
                   GamePlatform_OffChainViews):
    def __init__(self, admins, self_addr = None):
        list_of_views = [self.offchain_check_move, self.offchain_apply_move,
                         self.offchain_init, self.offchain_new_game, self.offchain_play]
        self.init_type(t_init_type)
        self.init(
            admins            = admins,
            channels          = sp.big_map(),
            games             = sp.big_map(),
            ledger            = sp.big_map(),
            models            = sp.big_map(),
            metadata          = sp.big_map(),
            token_metadata    = sp.big_map(),
            token_permissions = sp.big_map({0: {"type": sp.pack("native")}}),
        )
        metadata_base = {
            "version": "alpha 0.1",
            "description" : (
                "Example of a state channel based game platform templates (work in progress)."
            ),
            "interfaces": ["TZIP-016"],
            "authors": [
                "SmartPy <https://smartpy.io>"
            ],
            "homepage": "https://smartpy.io",
            "views": list_of_views,
            "source": {
                "tools": ["SmartPy"],
                "location": "https://gitlab.com/SmartPy/smartpy/-/blob/master/python/templates/state_channel_games/game_platform.py"
            },
        }
        self.init_metadata("metadata_base", metadata_base)
        self.self_addr = self_addr

    def self_address(self):
        if self.self_addr is None:
            return sp.self_address
        else:
            return self.self_addr

    # Methods #

    def get_player(self, constants, player_id):
        sp.set_type(constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
        player = self.data.channels[constants.channel_id].players[constants.players_addr[player_id]]
        player = sp.local('player', player).value
        return player

    def get_running_game(self, game_id, verify_running = True):
        game = self.data.games.get(game_id, message = sp.pair("Platform_GameNotFound: ", game_id))
        game = sp.local('game', game).value
        if verify_running:
            sp.verify(game.current.outcome == sp.none, "Platform_NotRunning")
        return game

    def get_opened_channel(self, channel_id, local = True):
        channel = self.data.channels.get(channel_id, message = sp.pair("Platform_ChannelNotFound: ", channel_id))
        if local:
            channel = sp.local('channel', channel).value
        sp.verify(~channel.closed, "Platform_ChannelClosed")
        return channel

    def get_model(self, model_id):
        model = self.data.models.get(model_id, message = sp.pair("Platform_ModelNotFound: ", model_id))
        model = sp.local('model', model).value
        return model

    def get_token_metadata(self, token):
        token_metadata = self.data.token_metadata.get(token, message = sp.pair("Platform_TokenNotFound", sp.record(token = token)))
        token_metadata = sp.local('token_metadata', token_metadata).value
        return token_metadata

    def get_token_permissions(self, token):
        token_permissions = self.data.token_permissions.get(token, message = sp.pair("Platform_TokenNotFound", sp.record(token = token)))
        token_permissions = sp.local('token_permissions', token_permissions).value
        return token_permissions

    def compute_timeouts(self, game):
        """ update timeout of player if other player is starving"""
        with sp.if_(game.timeouts.contains(game.current.player)):
            game.timeouts[game.current.player] = sp.now.add_seconds(game.constants.play_delay)
        return game

    def increase_token_supply(self, token, amount):
        token_permissions = self.get_token_permissions(token)
        supply = sp.local('supply', sp.nat(0))
        with sp.if_(token_permissions.contains("supply")):
            supply.value = sp.unpack(token_permissions["supply"], sp.TNat).open_some()
        supply.value += amount
        with sp.if_(token_permissions.contains("max_supply")):
            max_supply = sp.unpack(token_permissions["max_supply"], sp.TNat).open_some()
            sp.verify(supply.value <= max_supply, message = sp.pair("Platform_TokenSupplyExceed", sp.record(token = token, amount = amount)))
        self.data.token_permissions[token]["supply"] = sp.pack(supply.value)

    def decrease_token_supply(self, token, amount):
        token_permissions = self.get_token_permissions(token)
        sp.verify(token_permissions.contains("supply"), message = sp.pair("Platform_NoSupply", sp.record(token = token)))
        supply = sp.unpack(self.data.token_permissions[token]["supply"]).open_some()
        self.data.token_permissions[token]["supply"] = sp.pack(
            sp.as_nat(supply - amount, message = sp.pair("Platform_NotEnoughtTokenSupply", sp.record(token = token, amount = amount)))
        )

    def platform_transfer(self, game_id, players_addr, players, transfer, allowed_mint_model, allowed_mint_game):
        """ Perform token transfers between the platform and the receiver

            Update the allowed_mint local
        """
        receiver_addr = sp.compute(players_addr[transfer.receiver])
        receiver = sp.local('receiver', players[receiver_addr]).value
        with sp.for_("tokens", transfer.bonds.items()) as token:
            allowed = allowed_mint_game.get(token.key, 0)
            delta = sp.local('delta', allowed - token.value)
            allowed_mint_game[token.key] = sp.eif(delta.value > 0, sp.as_nat(delta.value), 0)
            with sp.if_(delta.value < 0):
                allowed_model = allowed_mint_model.get(token.key, 0)
                allowed_mint_model[token.key] = sp.as_nat(
                    sp.to_int(allowed_model) + delta.value,
                    sp.pair("Platform_NotAllowedMinting", sp.record(game_id = game_id, token = token.key, amount = token.value))
            )
            self.increase_token_supply(token.key, token.value)
            receiver.bonds[token.key] = receiver.bonds.get(token.key, 0) + token.value
        players[receiver_addr] = receiver

    def sender_transfer(self, players_addr, players, transfer):
        """ Perform token transfers between two players"""
        def bond_transfer(sender, sender_addr, receiver, token, amount):
            with sp.if_(~(amount == 0)):
                error_message = sp.pair("Platform_NotEnoughToken:", sp.record(sender = sender_addr, token = token, amount = amount))
                sp.verify(sender.bonds.contains(token), message = error_message)
                sender.bonds[token] = sp.as_nat(sender.bonds[token] - amount, message = error_message)
                with sp.if_(sender.bonds[token] == 0):
                    del sender.bonds[token]
                receiver.bonds[token] = receiver.bonds.get(token, 0) + amount

        # We don't need to do anything if sender == receiver
        sender_addr   = sp.compute(players_addr[transfer.sender])
        receiver_addr = sp.compute(players_addr[transfer.receiver])
        with sp.if_(~(sender_addr == receiver_addr)):
            sender   = sp.local('sender',   players[sender_addr]).value
            receiver = sp.local('receiver', players[receiver_addr]).value
            with sp.for_("bond", transfer.bonds.items()) as bond:
                bond_transfer(sender, sender_addr, receiver, bond.key, bond.value)
            players[sender_addr]   = sender
            players[receiver_addr] = receiver

    def receive_tokens(self, token):
        """ Receive token from outside

            If token_id is 0: the transaction amount must equal the amount given in params
            Else: the transfer is performed by a call to the corresponding
                  external contract address and token_id listed in the token_permissions
        """
        with sp.if_(token.key == 0):
            sp.verify(
                sp.amount == sp.utils.nat_to_mutez(token.value),
                message = sp.pair("Platform_WrongMutezAmount_Expected", sp.utils.nat_to_mutez(token.value))
            )
        with sp.else_():
            token_permissions = sp.compute(self.data.token_permissions.get(token.key, message = sp.pair("Platform_TokenNotFound", token.key)))
            sp.verify(token_permissions.contains("type"), message = sp.pair("Platform_TokenTypeNotSetup", token.key))
            token_type = sp.compute(sp.unpack(token_permissions["type"], sp.TString).open_some())
            with sp.if_(token_type == "FA2"):
                # FA2 transfer
                sp.verify(token_permissions.contains("fa2_address"), message = sp.pair("Platform_Token_NotFA2Setup", token.value))
                fa2_address = sp.unpack(token_permissions["fa2_address"], sp.TAddress).open_some()
                fa2_token_id = sp.unpack(token_permissions["fa2_token_id"], sp.TNat).open_some()
                fa2_contract = sp.contract(types.t_fa2_transfer, fa2_address, entry_point = "transfer")
                arg = [sp.record(
                    from_ = sp.sender,
                    txs = sp.list([sp.record(
                        to_      = sp.self_address,
                        token_id = fa2_token_id,
                        amount   = token.value
                    )])
                )]
                sp.transfer(arg, sp.tez(0), fa2_contract.open_some("Plateform_FA2Unreacheable"))
            with sp.else_():
                sp.verify(token_type == "FREE", message = sp.pair("Platform_TokenCannotBeRecived", token.key))
                # Avoid a gas lock attack
                sp.verify(token.value < 10**18, message = sp.pair("Platform_AmountExceeded", token.key))
            self.increase_token_supply(token.key, token.value)

    def check_move_current(self, current, constants, sender, move_nb):
        """ Checks game is Running + sender is current player + move is current_move """
        sp.set_type(constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
        sp.verify(sender == constants.players_addr[current.player], message="Platform_GameWrongPlayer")
        sp.verify(current.move_nb == move_nb, message = "Platform_Wrongmove_nb")
        sp.verify(current.outcome == sp.none, "Platform_GameNotRunning")

    ###############
    # Entrypoints #
    ###############

    @sp.entry_point
    def new_model(self, name, model):
        sp.set_type(name, sp.TString)
        model_id = sp.compute(sp.blake2b(model))
        with sp.if_(~self.data.models.contains(model_id)):
            unpacked = sp.compute(sp.unpack(model, types.t_model_lambdas).open_some("Platform_ModelUnpackFailure"))
            self.data.models[model_id] = sp.record(
                apply_   = unpacked.apply_,
                init     = unpacked.init,
                outcomes = unpacked.outcomes,
                metadata = sp.map({"name": sp.pack(name)}, tkey = sp.TString, tvalue = sp.TBytes)
            )

    @sp.entry_point
    def new_channel(self, players, withdraw_delay, nonce):
        channel_id = sp.blake2b(sp.pack((self.self_address(), players, nonce)))
        sp.verify(~self.data.channels.contains(channel_id), message = "Platform_ChannelAlreadyExists")
        sp.verify(sp.len(players) == 2, message = "Platform_Only2PlayersAllowed")
        players_map = sp.local('players_map', sp.map()).value
        with sp.for_("player", players.items()) as player:
            players_map[player.key] = sp.record(
                bonds       = {},
                pk          = player.value,
                withdraw    = sp.none,
                withdraw_id = 0,
            )
        self.data.channels[channel_id] = sp.record(
            closed         = False,
            nonce          = nonce,
            players        = players_map,
            withdraw_delay = withdraw_delay,
        )

    # Game #

    def new_game_(self, game_id, constants, init_input, signatures):
        sp.set_type(constants, types.t_constants)
        sp.set_type(init_input, types.t_init_input)
        sp.set_type(signatures, sp.TMap(sp.TKey, sp.TSignature))
        sp.verify(~self.data.games.contains(game_id), message = "Platform_GameAlreadyExists")
        sp.verify(sp.len(constants.players_addr) == 2, message = "Platform_Only2PlayersAllowed")
        channel = self.get_opened_channel(constants.channel_id)

        addr_players = sp.local('addr_players', sp.map()).value
        with sp.for_("addr_player", constants.players_addr.items()) as addr_player:
            addr_players[addr_player.value] = addr_player.key
            sp.verify(channel.players.contains(addr_player.value), message = "Platform_GamePlayerNotInChannel")

        with sp.for_('player', channel.players.values()) as player:
            sp.verify(signatures.contains(player.pk), "Platform_MissingSig")
            sp.verify(
                sp.check_signature(player.pk, signatures[player.pk], action_new_game(constants, init_input)),
                message = sp.pair("Platform_BadSig", sp.record(player = player.pk, signature = signatures[player.pk]))
            )

        return sp.set_type_expr(
            sp.record(
            addr_players = addr_players,
            constants    = constants,
            current      = sp.record(
                move_nb        = 0,
                player         = 1,
                outcome        = sp.none
                ),
            init_input   = init_input,
            settled      = False,
            state        = sp.none,
            timeouts     = sp.map(),
            metadata     = sp.map()
            ), types.t_game
        )

    @sp.entry_point
    def new_game(self, constants, params, signatures):
        game_id = compute_game_id(constants)
        self.data.games[game_id] = self.new_game_(game_id, constants, params, signatures)

    def game_play_(self, game, move_nb, move_data, sender):
        sp.set_type(move_nb, sp.TNat)
        sp.set_type(move_data, types.t_move_data)
        self.check_move_current(game.current, game.constants, sender, move_nb)
        model = self.get_model(game.constants.model_id)
        with sp.if_(~game.state.is_some()):
            game.state = sp.some(model.init(game.init_input))
        params = sp.record(
            move_data = move_data,
            move_nb   = game.current.move_nb,
            player    = game.current.player,
            state     = game.state.open_some()
        )
        apply_result = sp.compute(model.apply_(params))
        game.current = sp.record(
            move_nb        = game.current.move_nb +1,
            player         =  3 - game.current.player,
            outcome        = sp.eif(sp.snd(apply_result).is_some(), sp.some(sp.variant("game_finished", sp.snd(apply_result).open_some())), sp.none),
        )
        game.state = sp.some(sp.fst(apply_result))
        return game

    @sp.entry_point
    def game_play(self, game_id, move_nb, move_data):
        game = self.get_running_game(game_id)
        self.game_play_(game, move_nb, move_data, sp.sender)
        self.data.games[game_id] = self.compute_timeouts(game)

    @sp.entry_point
    def game_set_outcome(self, game_id, outcome, timeout, signatures):
        sp.set_type(signatures, sp.TMap(sp.TKey, sp.TSignature))
        sp.verify(sp.now <= timeout, message = "Platform_OutcomeTimedout")
        game = self.get_running_game(game_id)
        to_sign = sp.compute(action_new_outcome(game_id, outcome, timeout))
        channel = self.get_opened_channel(game.constants.channel_id, local = False)
        players = sp.local('players', channel.players).value
        with sp.for_('player', players.values()) as player:
            sp.verify(signatures.contains(player.pk), "Platform_MissingSig")
            sp.verify(sp.check_signature(player.pk, signatures[player.pk], to_sign), message = "Platform_badSig")
        game.current.outcome = sp.some(sp.variant("game_finished", outcome))
        self.data.games[game_id] = game

    @sp.entry_point
    def game_set_state(self, game_id, new_current, new_state, signatures):
        """ Update game state if new_current.nbMoves > game.nbMoves."""
        sp.set_type(signatures, sp.TMap(sp.TKey, sp.TSignature))
        game = self.get_running_game(game_id)
        sp.verify(new_current.move_nb > game.current.move_nb, "Platform_OutdatedMoveNB")
        to_sign = sp.compute(action_new_state(game_id, new_current, new_state))
        channel = self.get_opened_channel(game.constants.channel_id)
        with sp.for_('player', channel.players.values()) as player:
            sp.verify(
                sp.check_signature(player.pk, signatures[player.pk], to_sign),
                message = sp.pair("Platform_badSig", player.pk)
            )

        game.current  = new_current
        game.state = sp.some(new_state)
        self.data.games[game_id] = self.compute_timeouts(game)

    @sp.entry_point
    def game_settle(self, game_id):
        """ Set game to settled and update channel bonds depending on outcome.

            3 Types of outcome exists:
                - `player_double_played`: set by `double_signed`
                                              or `double_signed_offchain`
                - `player_inactive`: set by `starved` entrypoint
                - `game_finished`: set by `apply_` lambda after a `play`
                                    or by the current.outcome of `game_set_state`

            Outcome value of `player_double_played` and `player_inactive`
            represents the id of the player associated with the fault.
            Outcome value of `game_finished` is freely defined by the game.

            Transfers are defined by the game constant `settlements`.
            Each outcome and outcome value is associated with a list of transfers.

            No transfer occurs if the outcome and outcome value aren't in `settlements`.

            Transfer decreases bonds of the sender and increases bonds of the receiver.

            If the sender doesn't own enough to perform the transfer, the WHOLE settle FAILS.
        """
        game = self.get_running_game(game_id, verify_running = False)
        model_id = game.constants.model_id
        model = sp.local('model',
            sp.eif(
                self.data.models.contains(model_id),
                sp.some(self.data.models[model_id]),
                sp.none
            )
        ).value
        sp.verify(~game.settled, message = "Plateform_GameSettled")

        allowed_mint_model = sp.local("allowed_mint_model", {})
        allowed_mint_game  = sp.local("allowed_mint_game", {})
        with sp.if_(model.is_some()):
            with sp.if_(model.open_some().metadata.contains("allowed_mint")):
                allowed_mint_model.value = sp.unpack(model.open_some().metadata["allowed_mint"], types.t_token_map).open_some()
        with sp.if_(game.metadata.contains("allowed_mint")):
            allowed_mint_game.value = sp.unpack(game.metadata["allowed_mint"], types.t_token_map).open_some()
            # Allowed mint is not updated in the game storage after all the transfers
            # That's not needed because the game is settled.

        # Don't forget to check if outcome is not none
        with sp.if_(game.constants.settlements.contains(game.current.outcome.open_some("Plateform_GameIsntOver"))):
            transfers = sp.compute(game.constants.settlements[game.current.outcome.open_some()])
            players = self.get_opened_channel(game.constants.channel_id).players
            with sp.for_("transfer", transfers) as transfer:
                with sp.if_(transfer.sender == 0):
                    self.platform_transfer(game_id, game.constants.players_addr, players, transfer, allowed_mint_model.value, allowed_mint_game.value)
                with sp.else_():
                    self.sender_transfer(game.constants.players_addr, players, transfer)

            self.data.channels[game.constants.channel_id].players = players

        game.settled = True
        self.data.games[game_id] = game
        with sp.if_(sp.len(allowed_mint_model.value) > 0):
            self.data.models[game.constants.model_id].metadata["allowed_mint"] = sp.pack(allowed_mint_model.value)
        with sp.else_():
            with sp.if_(model.is_some()):
                del self.data.models[game.constants.model_id].metadata["allowed_mint"]

    # Dispute #

    @sp.entry_point
    def dispute_starving(self, game_id, flag):
        """ If flag is True, the player indicates that the other player
            no longer has his confidence.
            The latter must play on chain until he removes the flag"""
        game = self.get_running_game(game_id)
        player_id = game.addr_players[sp.sender]
        with sp.if_(flag):
            game.timeouts[3-player_id] = sp.now.add_seconds(game.constants.play_delay)
        with sp.else_():
            del game.timeouts[3-player_id]
        self.data.games[game_id] = game

    @sp.entry_point
    def dispute_starved(self, game_id, player_id):
        game = self.get_running_game(game_id)
        sp.verify(game.timeouts.contains(player_id), message = "Platform_NoTimeoutSetup")
        sp.verify(sp.now > game.timeouts[player_id], message = "Platform_NotTimedOut")
        game.current.outcome = sp.some(sp.variant("player_inactive", player_id))
        self.data.games[game_id] = game

    @sp.entry_point
    def dispute_double_signed(self, game_id, player_id, statement1, statement2):
        # The verification that both signatures come from the same game_id is implicit
        # Because game_id is included in the new_state signature
        # Same thing with game current : player_id and move_nb
        sp.set_type(statement1, sp.TRecord(state = types.t_game_state, sig = sp.TSignature))
        sp.set_type(statement2, sp.TOption(sp.TRecord(current = types.t_current, state = types.t_game_state, sig = sp.TSignature)))
        game = self.get_running_game(game_id)
        player = self.get_player(game.constants, player_id)

        with sp.if_(statement2.is_none()):
            # Verification of 1st statement
            to_sign = action_new_state(game_id, game.current, statement1.state)
            sp.verify(sp.check_signature(player.pk, statement1.sig, to_sign), message = "Platform_badSig")
            # Comparing with onchain current state
            sp.verify(~(statement1.state == game.state.open_some("Plateform_GameNotInitialized")), message = "Platform_NotDifferentStates")
        with sp.else_():
            statement2 = sp.local('statement2', statement2.open_some()).value
            # Verification of 1st statement
            to_sign1 = action_new_state(game_id, statement2.current, statement1.state)
            sp.verify(sp.check_signature(player.pk, statement1.sig, to_sign1), message = "Platform_badSig")
            # Verification of 2nd statement
            to_sign2 = action_new_state(game_id, statement2.current, statement2.state)
            sp.verify(sp.check_signature(player.pk, statement2.sig, to_sign2), message = "Platform_badSig")

            # Comparing statement1 with statement2
            sp.verify(~(statement1.state == statement2.state), message = "Platform_NotSameStates")

        game.current.outcome = sp.some(sp.variant("player_double_played", player_id))
        self.data.games[game_id] = game

    # Channel #

    @sp.entry_point(lazify = True)
    def channel_push_bonds(self, player_addr, channel_id, bonds):
        channel = self.get_opened_channel(channel_id, local = False)
        player = channel.players.get(player_addr, message = "Platform_NotChannelPlayer")
        player = sp.local('player', player).value
        with sp.for_("token", bonds.items()) as token:
            self.receive_tokens(token)
            with sp.if_(player.bonds.contains(token.key)):
                player.bonds[token.key] += token.value
            with sp.else_():
                player.bonds[token.key] = token.value
        self.data.channels[channel_id].players[player_addr] = player

    @sp.entry_point
    def withdraw_request(self, channel_id, tokens):
        channel = self.get_opened_channel(channel_id)
        player = channel.players.get(sp.sender, message = "Platform_SenderNotInChannel")
        player = sp.local("player", player).value
        with sp.if_(player.withdraw.is_some()):
            sp.verify(sp.now >= player.withdraw.open_some().timeout, message = "Platform_ChallengeDelayNotOver")
        player.withdraw_id += 1
        player.withdraw = sp.some(
            sp.record(
                challenge        = sp.set(),
                challenge_tokens = sp.map(),
                timeout          = sp.now.add_seconds(channel.withdraw_delay),
                tokens           = tokens,
            )
        )
        channel.players[sp.sender] = player
        self.data.channels[channel_id] = channel

    @sp.entry_point
    def withdraw_challenge(self, channel_id, withdrawer, game_ids):
        """ A withdraw can be challenged by giving a set of game_id.
            The challenge will be valid if the total bonds of the non settled game exceed the withdraw.
        """
        sp.set_type(game_ids, sp.TSet(sp.TBytes))
        channel = self.get_opened_channel(channel_id)
        sp.verify(channel.players.contains(sp.sender), "Platform_SenderNotInChannel")
        sp.verify(channel.players.contains(withdrawer), "Platform_WithdrawerNotInChannel")
        sp.verify(channel.players[withdrawer].withdraw.is_some(), "Platform_NoWithdrawRequestOpened")
        withdraw = sp.local('withdraw', channel.players[withdrawer].withdraw.open_some()).value
        with sp.for_('game_id', game_ids.elements()) as game_id:
            game = self.get_running_game(game_id, verify_running = False)
            sp.verify(game.constants.channel_id == channel_id, "Platform_ChannelIdMismatch")
            with sp.if_(game.settled):
                sp.verify(withdraw.challenge.contains(game_id), "Platform_GameSettled")
                withdraw.challenge.remove(game_id)
                minus_tokens(withdraw.challenge_tokens, game.constants.bonds[game.addr_players[withdrawer]])
            with sp.else_():
                with sp.if_(~withdraw.challenge.contains(game_id)):
                    withdraw.challenge.add(game_id)
                    add_tokens(withdraw.challenge_tokens, game.constants.bonds[game.addr_players[withdrawer]])
        with sp.if_(~(withdrawer==sp.sender)):
            withdraw.timeout = sp.now
        channel.players[withdrawer].withdraw = sp.some(withdraw)
        self.data.channels[channel_id] = channel

    @sp.entry_point
    def withdraw_finalise(self, channel_id):
        """ A withdraw challenge can be answered if both of the following conditions are met:
            - the `timeout` timed out
            - challenge_tokens doesn't contain positive value
        """
        channel = self.get_opened_channel(channel_id)
        sp.verify(channel.players.contains(sp.sender), "Platform_SenderNotInChannel")
        player = sp.local('player', channel.players[sp.sender]).value
        sp.verify(player.withdraw.is_some(), "Platform_NoWithdrawOpened")
        withdraw = sp.local('withdraw', player.withdraw.open_some()).value
        sp.verify(sp.now >= withdraw.timeout, message = "Platform_ChallengeDelayNotOver")

        with sp.for_('token', withdraw.challenge_tokens.items()) as token:
            with sp.if_((token.value > 0) & player.bonds.contains(token.key)):
                sp.verify((player.bonds[token.key]-withdraw.tokens[token.key]) > token.value, "Platform_TokenChallenged")

        # Transfer withdraw
        with sp.for_('token', withdraw.tokens.items()) as token:
            with sp.if_(token.value > 0):
                sp.verify(player.bonds.contains(token.key), message = "Platform_NotEnoughTokens")
                player.bonds[token.key] = sp.as_nat(player.bonds[token.key] - token.value, message = "Platform_NotEnoughTokens")
                with sp.if_(self.data.ledger.contains((sp.sender, token.key))):
                    self.data.ledger[(sp.sender, token.key)] = sp.record(balance =
                        token.value + self.data.ledger[(sp.sender, token.key)].balance
                    )
                with sp.else_():
                    self.data.ledger[(sp.sender, token.key)] = sp.record(balance = token.value)

        player.withdraw = sp.none
        channel.players[sp.sender] = player
        self.data.channels[channel_id] = channel

    @sp.entry_point
    def withdraw_cancel(self, channel_id):
        """Cancel a running withdraw_request after it timedout"""
        channel = self.get_opened_channel(channel_id)
        sp.verify(channel.players.contains(sp.sender), "Platform_SenderNotInChannel")
        player = sp.local('player', channel.players[sp.sender]).value
        sp.verify(player.withdraw.is_some(), "Platform_NoWithdrawOpened")
        sp.verify(sp.now >= player.withdraw.open_some().timeout, message = "Platform_ChallengeDelayNotOver")
        channel.players[sp.sender].withdraw = sp.none
        self.data.channels[channel_id] = channel

    # Ledger

    @sp.entry_point(lazify = True)
    def withdraw_ledger(self, receiver, tokens):
        """ Transfer tokens to outside

            If token_id is 0: transfer in tez
            Else: the transfer is performed by a call to the corresponding
                  external contract address and token_id listed in the token_permissions
        """
        sp.set_type(tokens, types.t_token_map)
        with sp.for_("token", tokens.items()) as token:
            token_key = (sp.sender, token.key)

            # Performing Transfer
            with sp.if_(token.key == 0):
                # Transfer of tez
                sp.send(receiver, sp.utils.nat_to_mutez(token.value))
            with sp.else_():
                # FA2 transfer
                token_permissions = sp.compute(self.data.token_permissions.get(token.key, message = sp.pair("Platform_TokenNotFound", sp.record(token = token.key))))
                sp.verify(token_permissions.contains("type"), message = sp.pair("Platform_TokenTypeNotSetup", token.key))
                token_type = sp.compute(sp.unpack(token_permissions["type"], sp.TString).open_some())
                sp.verify(token_type == "FA2", message = sp.pair("Platform_TokenCannotBeWithdrawn", token.key))
                sp.verify(token_permissions.contains("fa2_address"), message = sp.pair("Platform_TokenFA2NotSetup", token.key))
                fa2_address = sp.unpack(token_permissions["fa2_address"], sp.TAddress).open_some()
                fa2_token_id = sp.unpack(token_permissions["fa2_token_id"], sp.TNat).open_some()
                fa2_contract = sp.contract(types.t_fa2_transfer, fa2_address, entry_point = "transfer")
                arg = sp.list([sp.record(
                    from_ = self.self_address(),
                    txs = sp.list([sp.record(
                        to_      = sp.sender,
                        token_id = fa2_token_id,
                        amount   = token.value
                    )])
                )])
                sp.transfer(arg, sp.tez(0), fa2_contract.open_some("Plateform_FA2Unreacheable"))
                self.decrease_token_supply(token.key, token.value)

            # Write the transfer in ledger
            transfer_err = sp.pair("Platform_NotEnoughtToken", sp.record(token_id = token.key))
            sp.verify(self.data.ledger.contains(token_key) | (token.value == 0), message = transfer_err)
            self.data.ledger[token_key].balance = sp.as_nat(self.data.ledger[token_key].balance - token.value, message = transfer_err)

    @sp.entry_point
    def ledger_to_bonds(self, channel_id, receiver, tokens):
        channel = self.get_opened_channel(channel_id)
        to = channel.players.get(receiver, message = "Platform_ReceiverNotChannelPlayer")
        to = sp.local('to', to).value
        with sp.for_("token", tokens.items()) as token:
            with sp.if_(token.value > 0):
                sp.verify(self.data.ledger.contains((sp.sender, token.key)), message = "Platform_NotEnoughToken")
                self.data.ledger[(sp.sender, token.key)] = sp.record(balance = sp.as_nat(
                    self.data.ledger[(sp.sender, token.key)].balance - token.value,
                    message = "Platform_NotEnoughToken"
                ))
                with sp.if_(to.bonds.contains(token.key)):
                    to.bonds[token.key] += token.value
                with sp.else_():
                    to.bonds[token.key] = token.value
        self.data.channels[channel_id].players[receiver] = to

    # Admin #

    def update_metadata(self, metadata_map, key, metadata):
        sp.set_type(metadata, sp.TOption(sp.TBytes))
        sp.set_type(key, sp.TString)
        with sp.if_(metadata.is_some()):
            metadata_map[key] = metadata.open_some()
        with sp.else_():
            del metadata_map[key]

    @sp.entry_point
    def set_token_metadata(self, token_id, metadata):
        sp.verify(self.data.admins.contains(sp.sender), "Platform_NotAdmin")
        self.data.token_metadata[token_id] = metadata

    @sp.entry_point
    def set_token_permissions(self, token_id, metadata):
        sp.verify(self.data.admins.contains(sp.sender), "Platform_NotAdmin")
        self.data.token_permissions[token_id] = metadata

    @sp.entry_point
    def update_global_metadata(self, key, metadata):
        sp.verify(self.data.admins.contains(sp.sender), "Platform_NotAdmin")
        self.update_metadata(self.data.metadata, key, metadata)

    @sp.entry_point
    def update_model_metadata(self, model_id, key, metadata):
        sp.verify(self.data.admins.contains(sp.sender), "Platform_NotAdmin")
        sp.verify(self.data.models.contains(model_id), message = "Platform_ModelNotFound")
        self.update_metadata(self.data.models[model_id].metadata, key, metadata)

    @sp.entry_point
    def update_game_metadata(self, game_id, key, metadata):
        sp.verify(self.data.admins.contains(sp.sender), "Platform_NotAdmin")
        sp.verify(self.data.games.contains(game_id), message = "Platform_GameNotFound")
        self.update_metadata(self.data.games[game_id].metadata, key, metadata)

    # @sp.entry_point
    # def admin_update_entrypoints(self, push_bonds, withdraw_ledger):
    #     # TODO: params as option
    #     sp.verify(self.data.admins.contains(sp.sender), "Platform_NotAdmin")
    #     sp.set_entry_point("push_bonds", push_bonds.open_some())
    #     sp.set_entry_point("withdraw_ledger", withdraw_ledger.open_some())

# Utils #

def compute_game_id(constants):
    return sp.blake2b(sp.pack(
        sp.pair(constants.channel_id, sp.pair(constants.model_id, constants.game_nonce))
    ))

def action_new_game(constants, init_input):
    constants  = sp.set_type_expr(constants, types.t_constants)
    init_input = sp.set_type_expr(init_input, types.t_init_input)
    return sp.pack(("New Game", constants, init_input))

def action_new_state(game_id, new_current, new_state):
    game_id    = sp.set_type_expr(game_id, sp.TBytes)
    new_current   = sp.set_type_expr(new_current, types.t_current)
    new_state  = sp.set_type_expr(new_state, sp.TBytes)
    return sp.pack(("New State", game_id, new_current, new_state))

def action_new_outcome(game_id, new_outcome, timeout):
    game_id     = sp.set_type_expr(game_id, sp.TBytes)
    new_outcome = sp.set_type_expr(new_outcome, sp.TString)
    return sp.pack(("New Outcome", game_id, new_outcome, timeout))

def transfer(sender, receiver, bonds):
    return sp.record(sender = sender, receiver = receiver, bonds = bonds)

def minus_tokens(a, b):
    sp.set_type(a, sp.TMap(sp.TNat, sp.TInt))
    sp.set_type(b, types.t_token_map)
    with sp.for_('token', b.items()) as token:
        with sp.if_(a.contains(token.key)):
            amount = sp.compute(a[token.key] - sp.to_int(b[token.key]))
            with sp.if_( amount == 0):
                del a[token.key]
            with sp.else_():
                a[token.key] = amount
        with sp.else_():
            a[token.key] = 0 - sp.to_int(b[token.key])

def add_tokens(a, b):
    sp.set_type(a, sp.TMap(sp.TNat, sp.TInt))
    sp.set_type(b, types.t_token_map)
    with sp.for_('token', b.items()) as token:
        with sp.if_(a.contains(token.key)):
            a[token.key] +=  sp.to_int(b[token.key])
        with sp.else_():
            amount = sp.to_int(b[token.key])
            with sp.if_(~(amount == 0)):
                a[token.key] = amount


# Test utils #

def outcome(gamePlatform, game_id):
        return gamePlatform.data.games[game_id].current.outcome.open_some()

def finished_outcome(gamePlatform, game_id):
    return outcome(gamePlatform, game_id).open_variant('game_finished')

def player_bonds(gamePlatform, channel_id, player_addr):
    return gamePlatform.data.channels[channel_id].players[player_addr].bonds


sp.add_compilation_target("platform", GamePlatform(sp.set([sp.address("tz1_GAMEPLATFORM_ADMIN")])), flags = ["no-simplify-via-michel", "no-decompile"])
