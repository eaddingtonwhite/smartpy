import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';
import globals from 'rollup-plugin-node-globals';
import pkg from './package.json';

export default {
  input: 'index.js',
  output: [
    {
      name: pkg.module,
      file: `${pkg.directories.dist}/${pkg.name}.min.js`,
      format: 'iife',
      plugins: [terser()],
      compact: true,
    },
  ],
  plugins: [resolve({ browser: true }), globals()],
};
