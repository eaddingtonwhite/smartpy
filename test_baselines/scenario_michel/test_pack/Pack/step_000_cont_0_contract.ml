open Smartml

module Contract = struct

  let%entry_point pack_lambda_record self () =
    verify (unpack (pack sp.build_lambda(lambda lparams_0: {a = 1; b = lparams_0 + 2})) sp.TLambda(int, sp.TRecord(a = int, b = int).layout(("a", "b"))).open_some()(100).a = 1);
    verify (unpack (pack sp.build_lambda(lambda lparams_1: {a = lparams_1 + 1})) sp.TLambda(int, sp.TRecord(a = int).layout("a")).open_some()(100).a = 101)

  let%entry_point pack_lambda_variant self () =
    verify (unpack (pack sp.build_lambda(lambda lparams_2: (variant('a', lparams_2 + 1) : sp.TVariant(a = int, b = int).layout(("a", "b"))))) sp.TLambda(int, sp.TVariant(a = int, b = int).layout(("a", "b"))).open_some()(100).open_variant('a') = 101);
    verify (unpack (pack sp.build_lambda(lambda lparams_3: variant('a', lparams_3 + 1))) sp.TLambda(int, sp.TVariant(a = int).layout("a")).open_some()(100).open_variant('a') = 101)

  let%entry_point pack_lambdas self () =
    def f4(lparams_4):
      y = sp.local("y", 0);
      r = sp.local("r", "A");
      if lparams_4 = 0 then
        if lparams_4 = y.value then
          r.value <- "B";
      r.value
    f1 = sp.local("f1", sp.build_lambda(f4), sp.TLambda(int, string));
    p1 = sp.local("p1", pack f1.value);
    f2 = sp.local("f2", unpack p1.value sp.TLambda(int, string).open_some(), sp.TLambda(int, string));
    p2 = sp.local("p2", pack f2.value);
    verify (p1.value = p2.value);
    verify (f1.value(0) = f2.value(0));
    verify (f1.value(1) = f2.value(1));
    verify (f1.value(2) = f2.value(2))

  let%entry_point pack_lambdas2 self () =
    f1 = sp.local("f1", sp.build_lambda(lambda lparams_5: lparams_5 > 0), sp.TLambda(int, bool));
    p1 = sp.local("p1", pack f1.value);
    f2 = sp.local("f2", unpack p1.value sp.TLambda(int, bool).open_some(), sp.TLambda(int, bool));
    verify f2.value(1)

  let%entry_point run self () =
    verify ((pack True) = bytes "0x05030a");
    verify ((pack (True : bool)) = (pack (True : bool)));
    verify ((pack False) = bytes "0x050303");
    verify ((pack (False : bool)) = (pack (False : bool)));
    verify ((pack ()) = bytes "0x05030b");
    verify ((pack (() : unit)) = (pack (() : unit)));
    verify ((pack sp.tez(0)) = bytes "0x050000");
    verify ((pack (sp.tez(0) : mutez)) = (pack (sp.tez(0) : mutez)));
    verify ((pack sp.mutez(1)) = bytes "0x050001");
    verify ((pack (sp.mutez(1) : mutez)) = (pack (sp.mutez(1) : mutez)));
    verify ((pack sp.mutez(2)) = bytes "0x050002");
    verify ((pack (sp.mutez(2) : mutez)) = (pack (sp.mutez(2) : mutez)));
    verify ((pack sp.mutez(3)) = bytes "0x050003");
    verify ((pack (sp.mutez(3) : mutez)) = (pack (sp.mutez(3) : mutez)));
    verify ((pack sp.mutez(10)) = bytes "0x05000a");
    verify ((pack (sp.mutez(10) : mutez)) = (pack (sp.mutez(10) : mutez)));
    verify ((pack sp.mutez(10000)) = bytes "0x0500909c01");
    verify ((pack (sp.mutez(10000) : mutez)) = (pack (sp.mutez(10000) : mutez)));
    verify ((pack sp.tez(1)) = bytes "0x050080897a");
    verify ((pack (sp.tez(1) : mutez)) = (pack (sp.tez(1) : mutez)));
    verify ((pack sp.tez(1000000)) = bytes "0x050080c0a8ca9a3a");
    verify ((pack (sp.tez(1000000) : mutez)) = (pack (sp.tez(1000000) : mutez)));
    verify ((pack 0) = bytes "0x050000");
    verify ((pack (0 : nat)) = (pack (0 : nat)));
    verify ((pack 1) = bytes "0x050001");
    verify ((pack (1 : nat)) = (pack (1 : nat)));
    verify ((pack 2) = bytes "0x050002");
    verify ((pack (2 : nat)) = (pack (2 : nat)));
    verify ((pack 3) = bytes "0x050003");
    verify ((pack (3 : nat)) = (pack (3 : nat)));
    verify ((pack 10) = bytes "0x05000a");
    verify ((pack (10 : nat)) = (pack (10 : nat)));
    verify ((pack 10000) = bytes "0x0500909c01");
    verify ((pack (10000 : nat)) = (pack (10000 : nat)));
    verify ((pack 1000000) = bytes "0x050080897a");
    verify ((pack (1000000 : nat)) = (pack (1000000 : nat)));
    verify ((pack 1000000000000) = bytes "0x050080c0a8ca9a3a");
    verify ((pack (1000000000000 : nat)) = (pack (1000000000000 : nat)));
    verify ((pack 0) = bytes "0x050000");
    verify ((pack (0 : int)) = (pack (0 : int)));
    verify ((pack 1) = bytes "0x050001");
    verify ((pack (1 : int)) = (pack (1 : int)));
    verify ((pack 2) = bytes "0x050002");
    verify ((pack (2 : int)) = (pack (2 : int)));
    verify ((pack 3) = bytes "0x050003");
    verify ((pack (3 : int)) = (pack (3 : int)));
    verify ((pack 10) = bytes "0x05000a");
    verify ((pack (10 : int)) = (pack (10 : int)));
    verify ((pack 10000) = bytes "0x0500909c01");
    verify ((pack (10000 : int)) = (pack (10000 : int)));
    verify ((pack 1000000) = bytes "0x050080897a");
    verify ((pack (1000000 : int)) = (pack (1000000 : int)));
    verify ((pack 1000000000000) = bytes "0x050080c0a8ca9a3a");
    verify ((pack (1000000000000 : int)) = (pack (1000000000000 : int)));
    verify ((pack 0) = bytes "0x050000");
    verify ((pack (0 : int)) = (pack (0 : int)));
    verify ((pack -1) = bytes "0x050041");
    verify ((pack (-1 : int)) = (pack (-1 : int)));
    verify ((pack -2) = bytes "0x050042");
    verify ((pack (-2 : int)) = (pack (-2 : int)))

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [pack_lambda_record; pack_lambda_variant; pack_lambdas; pack_lambdas2; run]
end