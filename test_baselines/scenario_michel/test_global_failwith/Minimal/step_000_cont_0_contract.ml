open Smartml

module Contract = struct

  let%entry_point entry_point_1 self () =
    aaa = sp.local("aaa", self.f(""))

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [entry_point_1]
end