open Smartml

module Contract = struct

  let%entry_point toInt self params =
    self.data.fr <- some (to_int params)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(fr = int option).layout("fr")
      ~storage:[%expr
                 {fr = sp.none}]
      [toInt]
end