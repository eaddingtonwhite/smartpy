open Smartml

module Contract = struct

  let%entry_point play self params =
    verify ((self.data.winner = 0) & (not self.data.draw));
    verify ((params.f.i >= 0) & (params.f.i < 8));
    verify ((params.f.j >= 0) & (params.f.j < 8));
    verify ((params.t.i >= 0) & (params.t.i < 8));
    verify ((params.t.j >= 0) & (params.t.j < 8));
    verify ((sign self.data.deck[params.f.i][params.f.j]) = self.data.nextPlayer);
    verify ((sign self.data.deck[params.t.i][params.t.j]) <> self.data.nextPlayer);
    if (abs self.data.deck[params.f.i][params.f.j]) = 1 then
      if ((params.t.j - params.f.j) <> 0) & ((sign self.data.deck[params.t.i][params.t.j]) = 0) then
        verify ((abs (params.t.j - params.f.j)) = 1);
        if self.data.previousPawnMove.is_some() then
          verify (self.data.previousPawnMove.open_some().f.i = self.data.previousPawnMove.open_some().t.i);
          verify (self.data.previousPawnMove.open_some().f.j = (self.data.previousPawnMove.open_some().t.j - (2 * self.data.nextPlayer)));
      self.data.previousPawnMove <- some {f = params.f; t = params.t}
    else
      self.data.previousPawnMove <- none;
    if (abs self.data.deck[params.f.i][params.f.j]) = 5 then
      verify ((((params.t.j - params.f.j) = 0) | ((params.t.i - params.f.i) = 0)) | ((abs (params.t.j - params.f.j)) = (abs (params.t.i - params.f.i))));
      sp.for k in sp.range(1, (to_int (max (abs (params.t.j - params.f.j)) (abs (params.t.i - params.f.i)))) - 1):
        verify (self.data.deck[params.f.i + (k * (sign (params.t.i - params.f.i)))][params.f.j + (k * (sign (params.t.j - params.f.j)))] = 0);
    if (abs self.data.deck[params.f.i][params.f.j]) = 6 then
      verify (((abs (params.t.j - params.f.j)) <= 1) & ((abs (params.t.i - params.f.i)) <= 1));
      king = sp.local("king", self.data.nextPlayer);
      self.data.kings[self.data.nextPlayer].moved <- True;
      self.data.kings[self.data.nextPlayer].i <- params.t.i;
      self.data.kings[self.data.nextPlayer].j <- params.t.j;
    if (abs self.data.deck[params.f.i][params.f.j]) = 2 then
      verify (((params.t.j - params.f.j) = 0) | ((params.t.i - params.f.i) = 0));
      sp.for k in sp.range(1, (to_int (max (abs (params.t.j - params.f.j)) (abs (params.t.i - params.f.i)))) - 1):
        verify (self.data.deck[params.f.i + (k * (sign (params.t.i - params.f.i)))][params.f.j + (k * (sign (params.t.j - params.f.j)))] = 0);
    if (abs self.data.deck[params.f.i][params.f.j]) = 4 then
      verify ((abs (params.t.j - params.f.j)) = (abs (params.t.i - params.f.i)));
      sp.for k in sp.range(1, (to_int (max (abs (params.t.j - params.f.j)) (abs (params.t.i - params.f.i)))) - 1):
        verify (self.data.deck[params.f.i + (k * (sign (params.t.i - params.f.i)))][params.f.j + (k * (sign (params.t.j - params.f.j)))] = 0);
    if (abs self.data.deck[params.f.i][params.f.j]) = 3 then
      verify ((abs ((params.t.j - params.f.j) * (params.t.i - params.f.i))) = 2);
    self.data.deck[params.t.i][params.t.j] <- self.data.deck[params.f.i][params.f.j];
    self.data.deck[params.f.i][params.f.j] <- 0;
    self.data.nextPlayer <- - self.data.nextPlayer

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(check = bool, deck = map(int, map(int, int)), draw = bool, kings = map(int, sp.TRecord(i = int, j = int, moved = bool).layout(("i", ("j", "moved")))), nextPlayer = int, previousPawnMove = sp.TRecord(f = sp.TRecord(i = int, j = int).layout(("i", "j")), t = sp.TRecord(i = int, j = int).layout(("i", "j"))).layout(("f", "t")) option, winner = intOrNat).layout(("check", ("deck", ("draw", ("kings", ("nextPlayer", ("previousPawnMove", "winner")))))))
      ~storage:[%expr
                 {check = False,
                  deck = {0 : {0 : 2, 1 : 3, 2 : 4, 3 : 5, 4 : 6, 5 : 4, 6 : 3, 7 : 2}, 1 : {0 : 1, 1 : 1, 2 : 1, 3 : 1, 4 : 1, 5 : 1, 6 : 1, 7 : 1}, 2 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 3 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 4 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 5 : {0 : 0, 1 : 0, 2 : 0, 3 : 0, 4 : 0, 5 : 0, 6 : 0, 7 : 0}, 6 : {0 : -1, 1 : -1, 2 : -1, 3 : -1, 4 : -1, 5 : -1, 6 : -1, 7 : -1}, 7 : {0 : -2, 1 : -3, 2 : -4, 3 : -5, 4 : -6, 5 : -4, 6 : -3, 7 : -2}},
                  draw = False,
                  kings = {-1 : {i = 7; j = 4; moved = False}, 1 : {i = 0; j = 4; moved = False}},
                  nextPlayer = 1,
                  previousPawnMove = sp.none,
                  winner = 0}]
      [play]
end