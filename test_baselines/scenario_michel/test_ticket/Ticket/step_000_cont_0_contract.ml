open Smartml

module Contract = struct

  let%entry_point auto_call self () =
    ticket_9 = sp.local("ticket_9", ticket 1 43);
    sp.transfer(ticket_9.value, sp.tez(0), sp.self_entry_point('run'))

  let%entry_point run self params =
    set_type (params : int ticket);
    ticket_test_ticket_14_data, ticket_test_ticket_14_copy = sp.match_tuple(read_ticket_raw params, "ticket_test_ticket_14_data", "ticket_test_ticket_14_copy")
    ticket_test_ticket_14_ticketer, ticket_test_ticket_14_content, ticket_test_ticket_14_amount = sp.match_tuple(ticket_test_ticket_14_data, "ticket_test_ticket_14_ticketer", "ticket_test_ticket_14_content", "ticket_test_ticket_14_amount")
    ticket_15 = sp.local("ticket_15", ticket "abc" 42);
    self.data.y <- some ticket_15.value;
    ticket1_test_ticket_16, ticket2_test_ticket_16 = sp.match_tuple(split_ticket_raw ticket_test_ticket_14_copy (ticket_test_ticket_14_amount // 3, as_nat (ticket_test_ticket_14_amount - (ticket_test_ticket_14_amount // 3))).open_some(), "ticket1_test_ticket_16", "ticket2_test_ticket_16")
    self.data.x <- some join_tickets_raw (ticket2_test_ticket_16, ticket1_test_ticket_16).open_some()

  let%entry_point run2 self params =
    set_type (params : sp.TRecord(t = int ticket, x = int).layout(("t", "x")));
    x_test_ticket_22, t_test_ticket_22 = sp.match_record(params, "x", "t")
    verify (x_test_ticket_22 = 42);
    ticket_test_ticket_24_data, ticket_test_ticket_24_copy = sp.match_tuple(read_ticket_raw t_test_ticket_22, "ticket_test_ticket_24_data", "ticket_test_ticket_24_copy")
    ticket_test_ticket_24_ticketer, ticket_test_ticket_24_content, ticket_test_ticket_24_amount = sp.match_tuple(ticket_test_ticket_24_data, "ticket_test_ticket_24_ticketer", "ticket_test_ticket_24_content", "ticket_test_ticket_24_amount")
    ticket_25 = sp.local("ticket_25", ticket "abc" 42);
    self.data.y <- some ticket_25.value;
    ticket1_test_ticket_26, ticket2_test_ticket_26 = sp.match_tuple(split_ticket_raw ticket_test_ticket_24_copy (ticket_test_ticket_24_amount // 3, as_nat (ticket_test_ticket_24_amount - (ticket_test_ticket_24_amount // 3))).open_some(), "ticket1_test_ticket_26", "ticket2_test_ticket_26")
    self.data.x <- some join_tickets_raw (ticket2_test_ticket_26, ticket1_test_ticket_26).open_some()

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = int ticket option, y = string ticket option).layout(("x", "y"))
      ~storage:[%expr
                 {x = sp.none,
                  y = sp.none}]
      [auto_call; run; run2]
end