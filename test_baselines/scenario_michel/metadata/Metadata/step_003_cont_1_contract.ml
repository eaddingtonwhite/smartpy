open Smartml

module Contract = struct

  let%entry_point a_lazy_entry_point self () =
    ()

  let%entry_point change_metadata self params =
    self.data.metadata[""] <- params

  let%entry_point incr self () =
    self.data.x += 1

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(metadata = bigMap(string, bytes), x = intOrNat).layout(("metadata", "x"))
      ~storage:[%expr
                 {metadata = {'' : sp.bytes('0x697066733a2f2f516d65394c3479365a76507751746169734e4754554537566a55375052746e4a4673384e6a4e797a744533644754')},
                  x = 1}]
      [a_lazy_entry_point; change_metadata; incr]
end