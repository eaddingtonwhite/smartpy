import smartpy as sp

tstorage = sp.TRecord(metadata = sp.TBigMap(sp.TString, sp.TBytes), x = sp.TIntOrNat).layout(("metadata", "x"))
tparameter = sp.TVariant(a_lazy_entry_point = sp.TUnit, change_metadata = sp.TBytes, incr = sp.TUnit).layout(("a_lazy_entry_point", ("change_metadata", "incr")))
tglobals = { }
tviews = { "big_fail": ((), sp.TUnknown()), "big_fail2": ((), sp.TBool), "get_cst": ((), sp.TIntOrNat), "get_storage": ((), sp.TIntOrNat), "get_x": (sp.TIntOrNat, sp.TRecord(a = sp.TIntOrNat, b = sp.TIntOrNat).layout(("a", "b"))) }
