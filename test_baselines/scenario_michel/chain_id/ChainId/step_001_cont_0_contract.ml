open Smartml

module Contract = struct

  let%entry_point test self () =
    self.data.chainId <- some chain_id

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(chainId = chainId option).layout("chainId")
      ~storage:[%expr
                 {chainId = sp.none}]
      [test]
end