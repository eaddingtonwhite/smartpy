open Smartml

module Contract = struct

  let%entry_point addBalanceCounterparty self () =
    verify (self.data.balanceCounterparty = sp.tez(0));
    verify (amount = self.data.fromCounterparty);
    self.data.balanceCounterparty <- self.data.fromCounterparty

  let%entry_point addBalanceOwner self () =
    verify (self.data.balanceOwner = sp.tez(0));
    verify (amount = self.data.fromOwner);
    self.data.balanceOwner <- self.data.fromOwner

  let%entry_point claimCounterparty self params =
    verify (now < self.data.epoch);
    verify (self.data.hashedSecret = (blake2b params.secret));
    verify (sender = self.data.counterparty);
    sp.send(self.data.counterparty, self.data.balanceOwner + self.data.balanceCounterparty);
    self.data.balanceOwner <- sp.tez(0);
    self.data.balanceCounterparty <- sp.tez(0)

  let%entry_point claimOwner self () =
    verify (self.data.epoch < now);
    verify (sender = self.data.owner);
    sp.send(self.data.owner, self.data.balanceOwner + self.data.balanceCounterparty);
    self.data.balanceOwner <- sp.tez(0);
    self.data.balanceCounterparty <- sp.tez(0)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(balanceCounterparty = mutez, balanceOwner = mutez, counterparty = address, epoch = timestamp, fromCounterparty = mutez, fromOwner = mutez, hashedSecret = bytes, owner = address).layout(("balanceCounterparty", ("balanceOwner", ("counterparty", ("epoch", ("fromCounterparty", ("fromOwner", ("hashedSecret", "owner"))))))))
      ~storage:[%expr
                 {balanceCounterparty = sp.tez(0),
                  balanceOwner = sp.tez(0),
                  counterparty = sp.address('tz0FakeBob'),
                  epoch = sp.timestamp(123),
                  fromCounterparty = sp.tez(4),
                  fromOwner = sp.tez(50),
                  hashedSecret = sp.bytes('0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e'),
                  owner = sp.address('tz0FakeAlice')}]
      [addBalanceCounterparty; addBalanceOwner; claimCounterparty; claimOwner]
end