open Smartml

module Contract = struct

  let%entry_point bar self () =
    self.data.board <- self.zero;
    sp.for k in sp.list([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]):
      self.data.board[5][k] <- 1

  let%entry_point glider self () =
    self.data.board <- self.zero;
    self.data.board[3][3] <- 1;
    self.data.board[4][4] <- 1;
    self.data.board[5][2] <- 1;
    self.data.board[5][3] <- 1;
    self.data.board[5][4] <- 1

  let%entry_point reset self () =
    self.data.board <- self.zero

  let%entry_point run self () =
    next = sp.local("next", self.zero);
    sp.for i in sp.list([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]):
      sp.for j in sp.list([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]):
        sum = sp.local("sum", 0);
        sp.for k in sp.range(-1, 2):
          if ((i + k) >= 0) & ((i + k) < 10) then
            sp.for l in sp.range(-1, 2):
              if ((j + l) >= 0) & ((j + l) < 10) then
                if (k <> 0) | (l <> 0) then
                  sum.value += self.data.board[i + k][j + l];
        if self.data.board[i][j] = 0 then
          if sum.value = 3 then
            next.value[i][j] <- 1
        else
          if (sum.value >= 2) & (sum.value <= 3) then
            next.value[i][j] <- 1;
    self.data.board <- next.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(board = map(int, map(int, intOrNat))).layout("board")
      ~storage:[%expr
                 {board = {}}]
      [bar; glider; reset; run]
end