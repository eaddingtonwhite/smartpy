open Smartml

module Contract = struct

  let%entry_point comparisons self () =
    verify (self.data.i <= 123);
    verify ((2 + self.data.i) = 12);
    verify ((2 + self.data.i) <> 1234);
    verify ((self.data.i + 4) <> 123);
    verify ((self.data.i - 5) < 123);
    verify ((7 - self.data.i) < 123);
    verify (self.data.i > 3);
    verify ((4 * self.data.i) > 3);
    verify ((self.data.i * 5) > 3);
    verify (self.data.i >= 3);
    verify (self.data.i > 3);
    verify (self.data.i >= 3);
    verify (self.data.i < 3000);
    verify (self.data.i <= 3000);
    verify (self.data.b & True);
    if self.data.i > 3 then
      verify (self.data.i > 4)

  let%entry_point iterations self () =
    x = sp.local("x", self.data.i);
    x.value <- 0;
    x.value <- 1;
    x.value <- 2;
    x.value <- 3;
    x.value <- 4;
    sp.for i in sp.range(0, 5):
      x.value <- i;
    self.data.i <- to_int self.data.n;
    self.data.i <- 5;
    sp.while self.data.i <= 42:
      self.data.i += 2;
    if self.data.i <= 123 then
      x.value <- 12;
      self.data.i += x.value
    else
      x.value <- 5;
      self.data.i <- x.value

  let%entry_point localVariable self () =
    x = sp.local("x", self.data.i);
    x.value *= 2;
    self.data.i <- 10;
    self.data.i <- x.value

  let%entry_point myMessageName4 self () =
    sp.for x in self.data.m.items():
      self.data.i += x.key * x.value

  let%entry_point myMessageName5 self () =
    sp.for x in self.data.m.keys():
      self.data.i += 2 * x

  let%entry_point myMessageName6 self params =
    sp.for x in self.data.m.values():
      self.data.i += 3 * x;
    self.data.aaa.remove(2);
    self.data.aaa.add(12);
    self.data.abc.push(some 16);
    self.data.abca[0] <- some 16;
    if self.data.aaa.contains(12) then
      self.data.m[42] <- 43;
    self.data.aaa.add(as_nat (- params))

  let%entry_point someComputations self params =
    verify (self.data.i <= 123);
    self.data.i += params.y;
    self.data.acb <- params.x;
    self.data.i <- 100;
    self.data.i -= 1

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(aaa = sp.TSet(nat), abc = intOrNat option list, abca = map(intOrNat, intOrNat option), acb = string, b = bool, ddd = intOrNat list, f = bool, h = bytes, i = int, m = map(int, int), n = nat, pkh = keyHash, s = string, toto = string).layout(("aaa", ("abc", ("abca", ("acb", ("b", ("ddd", ("f", ("h", ("i", ("m", ("n", ("pkh", ("s", "toto"))))))))))))))
      ~storage:[%expr
                 {aaa = sp.set([1, 2, 3]),
                  abc = sp.list([sp.some(123), sp.none]),
                  abca = {0 : sp.some(123), 1 : sp.none},
                  acb = 'toto',
                  b = True,
                  ddd = sp.list([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]),
                  f = False,
                  h = sp.bytes('0x0000ab112233aa'),
                  i = 7,
                  m = {},
                  n = 123,
                  pkh = sp.key_hash('tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk'),
                  s = 'abc',
                  toto = 'ABC'}]
      [comparisons; iterations; localVariable; myMessageName4; myMessageName5; myMessageName6; someComputations]
end