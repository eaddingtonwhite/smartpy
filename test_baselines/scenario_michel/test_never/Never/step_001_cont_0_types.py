import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat).layout("x")
tparameter = sp.TVariant(entry_point_1 = sp.TVariant(A = sp.TIntOrNat, B = sp.TNever).layout(("A", "B"))).layout("entry_point_1")
tglobals = { }
tviews = { }
