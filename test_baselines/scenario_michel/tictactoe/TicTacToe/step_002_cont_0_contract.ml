open Smartml

module Contract = struct

  let%entry_point play self params =
    verify ((self.data.winner = 0) & (not self.data.draw));
    verify ((params.i >= 0) & (params.i < 3));
    verify ((params.j >= 0) & (params.j < 3));
    verify (params.move = self.data.nextPlayer);
    verify (self.data.deck[params.i][params.j] = 0);
    self.data.deck[params.i][params.j] <- params.move;
    self.data.nbMoves += 1;
    self.data.nextPlayer <- 3 - self.data.nextPlayer;
    if ((self.data.deck[params.i][0] <> 0) & (self.data.deck[params.i][0] = self.data.deck[params.i][1])) & (self.data.deck[params.i][0] = self.data.deck[params.i][2]) then
      self.data.winner <- self.data.deck[params.i][0];
    if ((self.data.deck[0][params.j] <> 0) & (self.data.deck[0][params.j] = self.data.deck[1][params.j])) & (self.data.deck[0][params.j] = self.data.deck[2][params.j]) then
      self.data.winner <- self.data.deck[0][params.j];
    if ((self.data.deck[0][0] <> 0) & (self.data.deck[0][0] = self.data.deck[1][1])) & (self.data.deck[0][0] = self.data.deck[2][2]) then
      self.data.winner <- self.data.deck[0][0];
    if ((self.data.deck[0][2] <> 0) & (self.data.deck[0][2] = self.data.deck[1][1])) & (self.data.deck[0][2] = self.data.deck[2][0]) then
      self.data.winner <- self.data.deck[0][2];
    if (self.data.nbMoves = 9) & (self.data.winner = 0) then
      self.data.draw <- True

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(deck = map(intOrNat, map(intOrNat, int)), draw = bool, nbMoves = intOrNat, nextPlayer = int, winner = int).layout(("deck", ("draw", ("nbMoves", ("nextPlayer", "winner")))))
      ~storage:[%expr
                 {deck = {0 : {0 : 0, 1 : 0, 2 : 0}, 1 : {0 : 0, 1 : 0, 2 : 0}, 2 : {0 : 0, 1 : 0, 2 : 0}},
                  draw = False,
                  nbMoves = 0,
                  nextPlayer = 1,
                  winner = 0}]
      [play]
end