import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(s = sp.TString, value = sp.TIntOrNat).layout(("s", "value")))
    self.init(s = '',
              value = 0)

  @sp.entry_point
  def arith(self):
    sp.verify(sp.michelson("ADD")(1, 2) == 3)
    sp.verify(sp.michelson("ADD")(1, -2) == -1)
    sp.verify(sp.michelson("SUB")(1, -2) == 3)
    sp.verify(sp.michelson("SUB")(1, 2) == -1)

  @sp.entry_point
  def concat1(self):
    sp.verify(sp.michelson("CONCAT")(sp.list(['a', 'b', 'c'])) == 'abc')

  @sp.entry_point
  def concat2(self):
    sp.verify(sp.michelson("CONCAT")('a', 'b') == 'ab')

  @sp.entry_point
  def lambdas(self):
    sp.verify(sp.michelson("PUSH (lambda int int) {DUP; ADD}; SWAP; EXEC;")(100) == 200)
    sp.verify(sp.michelson("PAIR; PUSH (lambda (pair int int) int) { UNPAIR; PUSH int 10; MUL; ADD;}; SWAP; EXEC;")(2, 5) == 25)
    sp.verify(sp.michelson("PAIR 3; PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; SWAP; EXEC;")(2, 5, 7) == 257)
    sp.verify(sp.michelson("PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; PUSH int 2; APPLY; PUSH int 5; APPLY; PUSH int 7; EXEC;")() == 257)

  @sp.entry_point
  def overflow_add(self):
    compute_inlineMichelson_36 = sp.local("compute_inlineMichelson_36", sp.michelson("ADD")(sp.mutez(9223372036854775807), sp.mutez(1)))

  @sp.entry_point
  def overflow_mul(self):
    compute_inlineMichelson_41 = sp.local("compute_inlineMichelson_41", sp.michelson("MUL")(sp.mutez(4611686018427387904), 2))

  @sp.entry_point
  def prim0(self):
    sp.verify(sp.michelson("UNIT")() == sp.unit)
    sp.verify(sp.michelson("NONE unit")() == sp.none)
    sp.verify(sp.michelson("PUSH int 2; PUSH int 1; PAIR")() == (1, 2))
    sp.verify(sp.len(sp.michelson("NIL unit")()) == 0)
    sp.verify(sp.len(sp.michelson("EMPTY_SET unit")()) == 0)
    sp.verify(sp.len(sp.michelson("EMPTY_MAP unit unit")()) == 0)

  @sp.entry_point
  def seq(self):
    sp.verify(sp.michelson("DIP {SWAP}; ADD; MUL; DUP; MUL;")(15, 16, 17) == 262144)

  @sp.entry_point
  def test_operations(self):
    sp.for op in sp.list([sp.michelson("NONE key_hash; SET_DELEGATE;")()]):
      sp.operations().push(op)
    match_pair_inlineMichelson_89_fst, match_pair_inlineMichelson_89_snd = sp.match_tuple(sp.michelson("PUSH int 123; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter int; storage int; code { UNPAIR; ADD; NIL operation; PAIR } }; PAIR")(), "match_pair_inlineMichelson_89_fst", "match_pair_inlineMichelson_89_snd")
    sp.for op in sp.list([sp.fst(sp.michelson("PUSH int 123; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter int; storage int; code { UNPAIR; ADD; NIL operation; PAIR } }; PAIR")())]):
      sp.operations().push(op)

  @sp.entry_point
  def underflow(self):
    compute_inlineMichelson_46 = sp.local("compute_inlineMichelson_46", sp.michelson("SUB")(sp.tez(0), sp.mutez(1)))