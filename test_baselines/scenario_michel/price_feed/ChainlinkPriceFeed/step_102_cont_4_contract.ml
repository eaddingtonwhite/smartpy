open Smartml

module Contract = struct

  let%entry_point cancelProposal self params =
    verify (self.data.addrVoterId.contains(sender)) "MultisignAdmin_VoterUnknown";
    verify ((self.data.proposals.contains(self.data.addrVoterId[sender])) & (self.data.proposals[self.data.addrVoterId[sender]].id = params)) "MultisignAdmin_ProposalUnknown";
    self.data.proposals[self.data.addrVoterId[sender]].canceled <- True

  let%entry_point getLastProposal self params =
    sp.transfer(self.data.proposals[fst params], sp.tez(0), sp.contract(sp.TRecord(batchs = sp.TVariant(selfAdmin = sp.TVariant(changeQuorum = nat, changeTarget = address, changeTimeout = int, changeVoters = sp.TRecord(added = sp.TRecord(addr = address, publicKey = key).layout(("addr", "publicKey")) list, removed = sp.TSet(address)).layout(("added", "removed"))).layout((("changeQuorum", "changeTarget"), ("changeTimeout", "changeVoters"))) list, targetAdmin = sp.TVariant(changeActive = bool, changeAdmin = address, changeOracles = sp.TRecord(added = (address * sp.TRecord(adminAddress = address, endingRound = nat option, startingRound = nat).layout(("adminAddress", ("endingRound", "startingRound")))) list, removed = address list).layout(("added", "removed")), updateFutureRounds = sp.TRecord(maxSubmissions = nat, minSubmissions = nat, oraclePayment = nat, restartDelay = nat, timeout = nat).layout(("maxSubmissions", ("minSubmissions", ("oraclePayment", ("restartDelay", "timeout")))))).layout((("changeActive", "changeAdmin"), ("changeOracles", "updateFutureRounds"))) list).layout(("selfAdmin", "targetAdmin")) list, canceled = bool, id = nat, nay = sp.TSet(nat), startedAt = timestamp, yay = sp.TSet(nat)).layout(("batchs", ("canceled", ("id", ("nay", ("startedAt", "yay")))))), snd params).open_some(message = "MultisignAdmin_WrongCallbackInterface"))

  let%entry_point getParams self params =
    sp.transfer({quorum = self.data.quorum; target = self.data.target; timeout = self.data.timeout}, sp.tez(0), params)

  let%entry_point multiVote self params =
    set_type (params : sp.TRecord(initiatorId = nat, proposalId = nat, votes = sp.TRecord(signature = signature, voterId = nat, yay = bool).layout(("signature", ("voterId", "yay"))) list).layout(("initiatorId", ("proposalId", "votes"))) list);
    sp.for proposalVotes in params:
      sp.for vote in proposalVotes.votes:
        verify (self.data.voters.contains(vote.voterId)) "MultisignAdmin_VoterUnknown";
        verify sp.check_signature(self.data.voters[vote.voterId].publicKey, vote.signature, pack (self_address, (proposalVotes.initiatorId, proposalVotes.proposalId))) "MultisignAdmin_Badsig";
        y5 = sp.local("y5", self.registerVote({in_param = {initiatorId = proposalVotes.initiatorId; proposalId = proposalVotes.proposalId; voterId = vote.voterId; yay = vote.yay}; in_storage = self.data}));
        self.data <- y5.value.storage;
        sp.for op in y5.value.operations.rev():
          operations().push(op);
      if ((len self.data.proposals[proposalVotes.initiatorId].nay) + (len self.data.proposals[proposalVotes.initiatorId].yay)) >= self.data.quorum then
        if (len self.data.proposals[proposalVotes.initiatorId].nay) >= (len self.data.proposals[proposalVotes.initiatorId].yay) then
          self.data.proposals[proposalVotes.initiatorId].canceled <- True
        else
          y6 = sp.local("y6", self.onVoted({in_param = self.data.proposals[proposalVotes.initiatorId]; in_storage = self.data}));
          self.data <- y6.value.storage;
          sp.for op in y6.value.operations.rev():
            operations().push(op)

  let%entry_point newProposal self params =
    verify (self.data.addrVoterId.contains(sender)) "MultisignAdmin_VoterUnknown";
    self.data.voters[self.data.addrVoterId[sender]].lastProposalId += 1;
    self.data.proposals[self.data.addrVoterId[sender]] <- {batchs = params; canceled = False; id = self.data.voters[self.data.addrVoterId[sender]].lastProposalId; nay = sp.set([]); startedAt = now; yay = sp.set([self.data.addrVoterId[sender]])};
    if self.data.quorum < 2 then
      y7 = sp.local("y7", self.onVoted({in_param = self.data.proposals[self.data.addrVoterId[sender]]; in_storage = self.data}));
      self.data <- y7.value.storage;
      sp.for op in y7.value.operations.rev():
        operations().push(op)

  let%entry_point vote self params =
    verify (self.data.addrVoterId.contains(sender)) "MultisignAdmin_VoterUnknown";
    sp.for vote in params:
      y8 = sp.local("y8", self.registerVote({in_param = {initiatorId = vote.initiatorId; proposalId = vote.proposalId; voterId = self.data.addrVoterId[sender]; yay = vote.yay}; in_storage = self.data}));
      self.data <- y8.value.storage;
      sp.for op in y8.value.operations.rev():
        operations().push(op);
      if ((len self.data.proposals[vote.initiatorId].nay) + (len self.data.proposals[vote.initiatorId].yay)) >= self.data.quorum then
        if (len self.data.proposals[vote.initiatorId].nay) >= (len self.data.proposals[vote.initiatorId].yay) then
          self.data.proposals[vote.initiatorId].canceled <- True
        else
          y9 = sp.local("y9", self.onVoted({in_param = self.data.proposals[vote.initiatorId]; in_storage = self.data}));
          self.data <- y9.value.storage;
          sp.for op in y9.value.operations.rev():
            operations().push(op)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(addrVoterId = bigMap(address, nat), keyVoterId = bigMap(key, nat), lastVoteTimestamp = timestamp, lastVoterId = nat, metadata = bigMap(string, bytes), nbVoters = nat, proposals = bigMap(nat, sp.TRecord(batchs = sp.TVariant(selfAdmin = sp.TVariant(changeQuorum = nat, changeTarget = address, changeTimeout = int, changeVoters = sp.TRecord(added = sp.TRecord(addr = address, publicKey = key).layout(("addr", "publicKey")) list, removed = sp.TSet(address)).layout(("added", "removed"))).layout((("changeQuorum", "changeTarget"), ("changeTimeout", "changeVoters"))) list, targetAdmin = sp.TVariant(changeActive = bool, changeAdmin = address, changeOracles = sp.TRecord(added = (address * sp.TRecord(adminAddress = address, endingRound = nat option, startingRound = nat).layout(("adminAddress", ("endingRound", "startingRound")))) list, removed = address list).layout(("added", "removed")), updateFutureRounds = sp.TRecord(maxSubmissions = nat, minSubmissions = nat, oraclePayment = nat, restartDelay = nat, timeout = nat).layout(("maxSubmissions", ("minSubmissions", ("oraclePayment", ("restartDelay", "timeout")))))).layout((("changeActive", "changeAdmin"), ("changeOracles", "updateFutureRounds"))) list).layout(("selfAdmin", "targetAdmin")) list, canceled = bool, id = nat, nay = sp.TSet(nat), startedAt = timestamp, yay = sp.TSet(nat)).layout(("batchs", ("canceled", ("id", ("nay", ("startedAt", "yay"))))))), quorum = nat, target = address option, timeout = int, voters = bigMap(nat, sp.TRecord(addr = address, lastProposalId = nat, publicKey = key).layout(("addr", ("lastProposalId", "publicKey"))))).layout(("addrVoterId", ("keyVoterId", ("lastVoteTimestamp", ("lastVoterId", ("metadata", ("nbVoters", ("proposals", ("quorum", ("target", ("timeout", "voters")))))))))))
      ~storage:[%expr
                 {addrVoterId = {sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a') : 0},
                  keyVoterId = {sp.key('edpkuZ7ERiU5B8knLqQsVMH86j9RLMUyHyL665oCXDkPQxF7HGqSeJ') : 0},
                  lastVoteTimestamp = sp.timestamp(0),
                  lastVoterId = 0,
                  metadata = {'' : sp.bytes('0x68747470733a2f2f636c6f7564666c6172652d697066732e636f6d2f697066732f516d57474c57783470475a4272564639427a313270415433413544756e77336f394e4d414b5676574b3531436679')},
                  nbVoters = 1,
                  proposals = {},
                  quorum = 1,
                  target = sp.none,
                  timeout = 5,
                  voters = {0 : {addr = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'); lastProposalId = 0; publicKey = sp.key('edpkuZ7ERiU5B8knLqQsVMH86j9RLMUyHyL665oCXDkPQxF7HGqSeJ')}}}]
      [cancelProposal; getLastProposal; getParams; multiVote; newProposal; vote]
end