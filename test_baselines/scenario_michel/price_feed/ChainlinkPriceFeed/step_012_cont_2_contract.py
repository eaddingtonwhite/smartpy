import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(active = sp.TBool, admin = sp.TAddress, aggregator = sp.TOption(sp.TAddress)).layout(("active", ("admin", "aggregator"))))
    self.init(active = True,
              admin = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'),
              aggregator = sp.some(sp.address('KT1CfuSjCcunNQ5qCCro2Kc74uivnor9d8ba%latestRoundData')))

  @sp.entry_point
  def administrate(self, params):
    sp.verify(sp.sender == self.data.admin, 'Proxy_NotAdmin')
    sp.set_type(params, sp.TList(sp.TVariant(changeActive = sp.TBool, changeAdmin = sp.TAddress, changeAggregator = sp.TAddress).layout(("changeActive", ("changeAdmin", "changeAggregator")))))
    sp.for action in params:
      with action.match_cases() as arg:
        with arg.match('changeActive') as changeActive:
          self.data.active = changeActive
        with arg.match('changeAdmin') as changeAdmin:
          self.data.admin = changeAdmin
        with arg.match('changeAggregator') as changeAggregator:
          self.data.aggregator = sp.some(changeAggregator)


  @sp.entry_point
  def decimals(self, params):
    sp.transfer(params, sp.tez(0), sp.contract(sp.TPair(sp.TUnit, sp.TAddress), self.data.aggregator.open_some(message = 'Proxy_AggregatorNotConfigured'), entry_point='decimals').open_some(message = 'Proxy_InvalidParametersInDecimalsView'))

  @sp.entry_point
  def description(self, params):
    sp.transfer(params, sp.tez(0), sp.contract(sp.TPair(sp.TUnit, sp.TAddress), self.data.aggregator.open_some(message = 'Proxy_AggregatorNotConfigured'), entry_point='description').open_some(message = 'Proxy_InvalidParametersInDescriptionView'))

  @sp.entry_point
  def latestRoundData(self, params):
    sp.transfer(params, sp.tez(0), sp.contract(sp.TPair(sp.TUnit, sp.TAddress), self.data.aggregator.open_some(message = 'Proxy_AggregatorNotConfigured'), entry_point='latestRoundData').open_some(message = 'Proxy_InvalidParametersInLatestRoundDataView'))

  @sp.entry_point
  def version(self, params):
    sp.transfer(params, sp.tez(0), sp.contract(sp.TPair(sp.TUnit, sp.TAddress), self.data.aggregator.open_some(message = 'Proxy_AggregatorNotConfigured'), entry_point='version').open_some(message = 'Proxy_InvalidParametersInVersionView'))