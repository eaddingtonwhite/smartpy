open Smartml

module Contract = struct

  let%entry_point getLatestRoundData self () =
    sp.transfer(sp.self_entry_point_address('setLatestRoundData'), sp.tez(0), sp.contract(address, self.data.proxy, entry_point='latestRoundData').open_some(message = "Wrong Interface: Could not resolve proxy latestRoundData entry-point."))

  let%entry_point setLatestRoundData self params =
    set_type (params : sp.TRecord(answer = nat, answeredInRound = nat, roundId = nat, startedAt = timestamp, updatedAt = timestamp).layout(("answer", ("answeredInRound", ("roundId", ("startedAt", "updatedAt"))))));
    verify (sender = self.data.proxy);
    self.data.latestRoundData <- some params

  let%entry_point setup self params =
    verify (sender = self.data.admin);
    self.data.admin <- params.admin;
    self.data.proxy <- params.proxy

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(admin = address, latestRoundData = sp.TRecord(answer = nat, answeredInRound = nat, roundId = nat, startedAt = timestamp, updatedAt = timestamp).layout(("answer", ("answeredInRound", ("roundId", ("startedAt", "updatedAt"))))) option, proxy = address).layout(("admin", ("latestRoundData", "proxy")))
      ~storage:[%expr
                 {admin = sp.address('tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a'),
                  latestRoundData = sp.none,
                  proxy = sp.address('KT1PG6uK91ymZYVtjnRXv2mEdFYSH6P6uJhC')}]
      [getLatestRoundData; setLatestRoundData; setup]
end