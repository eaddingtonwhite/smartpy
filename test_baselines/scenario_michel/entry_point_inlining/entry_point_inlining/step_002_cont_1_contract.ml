open Smartml

module Contract = struct

  let%entry_point entry_point_1 self () =
    self.data.x += 1012;
    self.data.x += 3;
    self.data.x += 15

  let%entry_point f self params =
    self.data.x += params + 1000;
    self.data.x += 3

  let%entry_point g self params =
    self.data.x += params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = intOrNat).layout("x")
      ~storage:[%expr
                 {x = 12}]
      [entry_point_1; f; g]
end