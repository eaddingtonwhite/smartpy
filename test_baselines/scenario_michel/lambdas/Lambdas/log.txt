Comment...
 h1: Lambdas
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_1_pre_michelson.michel 573
 -> (Pair 0 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 0))))
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_1_storage.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_1_storage.json 13
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_1_sizes.csv 2
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_1_storage.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_1_types.py 7
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_1_contract.tz 705
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_1_contract.json 743
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_1_contract.py 103
 => test_baselines/scenario_michel/lambdas/Lambdas/step_001_cont_1_contract.ml 99
 => test_baselines/scenario_michel/lambdas/Lambdas/step_002_cont_1_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_002_cont_1_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_002_cont_1_params.json 1
Executing f(sp.record())...
 -> (Pair 0 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 13))))
 => test_baselines/scenario_michel/lambdas/Lambdas/step_003_cont_1_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_003_cont_1_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_003_cont_1_params.json 1
Executing flambda(sp.record())...
 -> (Pair 0 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 1555371))))
 => test_baselines/scenario_michel/lambdas/Lambdas/step_004_cont_1_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_004_cont_1_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_004_cont_1_params.json 1
Executing i(sp.record())...
 -> (Pair 0 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 36))))
 => test_baselines/scenario_michel/lambdas/Lambdas/step_005_cont_1_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_005_cont_1_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_005_cont_1_params.json 1
Executing comp_test(sp.record())...
 -> (Pair 5 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 36))))
 => test_baselines/scenario_michel/lambdas/Lambdas/step_006_cont_1_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_006_cont_1_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_006_cont_1_params.json 1
Executing abs_test(5)...
 -> (Pair 5 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 36))))
Verifying sp.contract_data(1).abcd == 5...
 OK
 => test_baselines/scenario_michel/lambdas/Lambdas/step_008_cont_1_params.py 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_008_cont_1_params.tz 1
 => test_baselines/scenario_michel/lambdas/Lambdas/step_008_cont_1_params.json 1
Executing abs_test(-42)...
 -> (Pair 42 (Pair { PUSH int 1; ADD } (Pair None (Pair (Some 42) 36))))
Verifying sp.contract_data(1).abcd == 42...
 OK
