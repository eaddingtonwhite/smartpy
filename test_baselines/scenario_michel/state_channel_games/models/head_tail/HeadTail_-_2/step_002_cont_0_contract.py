import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(apply_ = sp.TLambda(sp.TRecord(move_data = sp.TNat, move_nb = sp.TNat, player = sp.TInt, state = sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes, secret1 = sp.TOption(sp.TNat), secret2 = sp.TOption(sp.TNat)).layout(("hash1", ("hash2", ("secret1", "secret2"))))).layout(("move_data", ("move_nb", ("player", "state")))), sp.TPair(sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes, secret1 = sp.TOption(sp.TNat), secret2 = sp.TOption(sp.TNat)).layout(("hash1", ("hash2", ("secret1", "secret2")))), sp.TOption(sp.TBounded(['player_1_won', 'player_2_won'], t=sp.TString)))), constants = sp.TRecord(channel_id = sp.TBytes, game_nonce = sp.TString, loser = sp.TMutez, model_id = sp.TBytes, player1 = sp.TRecord(addr = sp.TAddress, pk = sp.TKey).layout(("addr", "pk")), player2 = sp.TRecord(addr = sp.TAddress, pk = sp.TKey).layout(("addr", "pk")), winner = sp.TMutez).layout(("channel_id", ("game_nonce", ("loser", ("model_id", ("player1", ("player2", "winner"))))))), current = sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TString), player = sp.TInt).layout(("move_nb", ("outcome", "player"))), state = sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes, secret1 = sp.TOption(sp.TNat), secret2 = sp.TOption(sp.TNat)).layout(("hash1", ("hash2", ("secret1", "secret2"))))).layout(("apply_", ("constants", ("current", "state")))))
    self.init(apply_ = lambda(sp.TLambda(sp.TRecord(move_data = sp.TNat, move_nb = sp.TNat, player = sp.TInt, state = sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes, secret1 = sp.TOption(sp.TNat), secret2 = sp.TOption(sp.TNat)).layout(("hash1", ("hash2", ("secret1", "secret2"))))).layout(("move_data", ("move_nb", ("player", "state")))), sp.TPair(sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes, secret1 = sp.TOption(sp.TNat), secret2 = sp.TOption(sp.TNat)).layout(("hash1", ("hash2", ("secret1", "secret2")))), sp.TOption(sp.TBounded(['player_1_won', 'player_2_won'], t=sp.TString))))),
              constants = sp.record(channel_id = sp.bytes('0x01'), game_nonce = '', loser = sp.tez(0), model_id = sp.bytes('0x'), player1 = sp.record(addr = sp.address('tz0Fakeplayer1'), pk = sp.key('edpkFakeplayer1')), player2 = sp.record(addr = sp.address('tz0Fakeplayer2'), pk = sp.key('edpkFakeplayer2')), winner = sp.tez(0)),
              current = sp.record(move_nb = 0, outcome = sp.none, player = 1),
              state = sp.record(hash1 = sp.bytes('0x64fabcca0db8d1beeaed6aa9a387fbed25054c08783d649f886a87873d47b00f'), hash2 = sp.bytes('0x1f462ac2296c296987b861db5b5dd6d921b84d031312e80a3a0d5a3ceb1ac5f3'), secret1 = sp.none, secret2 = sp.none))

  @sp.entry_point
  def play(self, params):
    sp.set_type(params.move_nb, sp.TNat)
    sp.set_type(params.move_data, sp.TNat)
    sp.if self.data.current.player == 1:
      sp.verify(sp.sender == self.data.constants.player1.addr, 'Game_WrongPlayer')
    sp.else:
      sp.verify(sp.sender == self.data.constants.player2.addr, 'Game_WrongPlayer')
    sp.verify(self.data.current.outcome.is_variant('None'))
    sp.verify(self.data.current.move_nb == params.move_nb)
    compute_game_tester_40 = sp.local("compute_game_tester_40", self.data.apply_(sp.record(move_data = params.move_data, move_nb = self.data.current.move_nb, player = self.data.current.player, state = self.data.state)))
    match_pair_game_tester_50_fst, match_pair_game_tester_50_snd = sp.match_tuple(compute_game_tester_40.value, "match_pair_game_tester_50_fst", "match_pair_game_tester_50_snd")
    sp.set_type(match_pair_game_tester_50_snd, sp.TOption(sp.TBounded(['player_1_won', 'player_2_won'], t=sp.TString)))
    with match_pair_game_tester_50_snd.match_cases() as arg:
      with arg.match('Some') as Some:
        self.data.current.outcome = sp.some(sp.unbound(Some))
      with arg.match('None') as None:
        self.data.current.outcome = sp.none

    self.data.current.move_nb += 1
    self.data.current.player = 3 - self.data.current.player
    self.data.state = match_pair_game_tester_50_fst