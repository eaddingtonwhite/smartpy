open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    sp.send(sp.address('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr'), sp.tez(1))

  let%entry_point ep2 self () =
    ()

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [ep1; ep2]
end