open Smartml

module Contract = struct

  let%entry_point ep self params =
    if params = True then
      sp.send(sp.address('tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4'), sp.tez(2))

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [ep]
end