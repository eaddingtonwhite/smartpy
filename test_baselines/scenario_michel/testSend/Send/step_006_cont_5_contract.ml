open Smartml

module Contract = struct

  let%entry_point ep self () =
    sp.transfer(-42, sp.tez(0), sp.contract(int, sp.address('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr'), entry_point='myEntryPoint').open_some())

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [ep]
end