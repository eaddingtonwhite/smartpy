open Smartml

module Contract = struct

  let%entry_point ep self () =
    sp.send(sp.address('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr'), sp.tez(1))

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [ep]
end