import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(addrVoterId = sp.TBigMap(sp.TAddress, sp.TNat), keyVoterId = sp.TBigMap(sp.TKey, sp.TNat), lastVoteTimestamp = sp.TTimestamp, lastVoterId = sp.TNat, metadata = sp.TBigMap(sp.TString, sp.TBytes), nbVoters = sp.TNat, proposals = sp.TBigMap(sp.TNat, sp.TRecord(batchs = sp.TList(sp.TVariant(selfAdmin = sp.TList(sp.TVariant(changeQuorum = sp.TNat, changeTarget = sp.TAddress, changeTimeout = sp.TInt, changeVoters = sp.TRecord(added = sp.TList(sp.TRecord(addr = sp.TAddress, publicKey = sp.TKey).layout(("addr", "publicKey"))), removed = sp.TSet(sp.TAddress)).layout(("added", "removed"))).layout((("changeQuorum", "changeTarget"), ("changeTimeout", "changeVoters")))), targetAdmin = sp.TList(sp.TVariant(setActive = sp.TBool, setAdmin = sp.TAddress).layout(("setActive", "setAdmin")))).layout(("selfAdmin", "targetAdmin"))), canceled = sp.TBool, id = sp.TNat, nay = sp.TSet(sp.TNat), startedAt = sp.TTimestamp, yay = sp.TSet(sp.TNat)).layout(("batchs", ("canceled", ("id", ("nay", ("startedAt", "yay"))))))), quorum = sp.TNat, target = sp.TOption(sp.TAddress), timeout = sp.TInt, voters = sp.TBigMap(sp.TNat, sp.TRecord(addr = sp.TAddress, lastProposalId = sp.TNat, publicKey = sp.TKey).layout(("addr", ("lastProposalId", "publicKey"))))).layout(("addrVoterId", ("keyVoterId", ("lastVoteTimestamp", ("lastVoterId", ("metadata", ("nbVoters", ("proposals", ("quorum", ("target", ("timeout", "voters"))))))))))))
    self.init(addrVoterId = {sp.address('tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT') : 0, sp.address('tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR') : 1},
              keyVoterId = {sp.key('edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd') : 0, sp.key('edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe') : 1},
              lastVoteTimestamp = sp.timestamp(0),
              lastVoterId = 1,
              metadata = {'' : sp.bytes('0x68747470733a2f2f636c6f7564666c6172652d697066732e636f6d2f697066732f516d596e55326a7771345855346a775831526d75314547666d79684d687044437050704846356a396a4639793177')},
              nbVoters = 2,
              proposals = {},
              quorum = 1,
              target = sp.none,
              timeout = 5,
              voters = {0 : sp.record(addr = sp.address('tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT'), lastProposalId = 0, publicKey = sp.key('edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd')), 1 : sp.record(addr = sp.address('tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR'), lastProposalId = 0, publicKey = sp.key('edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe'))})

  @sp.entry_point
  def cancelProposal(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), 'MultisignAdmin_VoterUnknown')
    sp.verify((self.data.proposals.contains(self.data.addrVoterId[sp.sender])) & (self.data.proposals[self.data.addrVoterId[sp.sender]].id == params), 'MultisignAdmin_ProposalUnknown')
    self.data.proposals[self.data.addrVoterId[sp.sender]].canceled = True

  @sp.entry_point
  def getLastProposal(self, params):
    sp.transfer(self.data.proposals[sp.fst(params)], sp.tez(0), sp.contract(sp.TRecord(batchs = sp.TList(sp.TVariant(selfAdmin = sp.TList(sp.TVariant(changeQuorum = sp.TNat, changeTarget = sp.TAddress, changeTimeout = sp.TInt, changeVoters = sp.TRecord(added = sp.TList(sp.TRecord(addr = sp.TAddress, publicKey = sp.TKey).layout(("addr", "publicKey"))), removed = sp.TSet(sp.TAddress)).layout(("added", "removed"))).layout((("changeQuorum", "changeTarget"), ("changeTimeout", "changeVoters")))), targetAdmin = sp.TList(sp.TVariant(setActive = sp.TBool, setAdmin = sp.TAddress).layout(("setActive", "setAdmin")))).layout(("selfAdmin", "targetAdmin"))), canceled = sp.TBool, id = sp.TNat, nay = sp.TSet(sp.TNat), startedAt = sp.TTimestamp, yay = sp.TSet(sp.TNat)).layout(("batchs", ("canceled", ("id", ("nay", ("startedAt", "yay")))))), sp.snd(params)).open_some(message = 'MultisignAdmin_WrongCallbackInterface'))

  @sp.entry_point
  def getParams(self, params):
    sp.transfer(sp.record(quorum = self.data.quorum, target = self.data.target, timeout = self.data.timeout), sp.tez(0), params)

  @sp.entry_point
  def multiVote(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(initiatorId = sp.TNat, proposalId = sp.TNat, votes = sp.TList(sp.TRecord(signature = sp.TSignature, voterId = sp.TNat, yay = sp.TBool).layout(("signature", ("voterId", "yay"))))).layout(("initiatorId", ("proposalId", "votes")))))
    sp.for proposalVotes in params:
      sp.for vote in proposalVotes.votes:
        sp.verify(self.data.voters.contains(vote.voterId), 'MultisignAdmin_VoterUnknown')
        sp.verify(sp.check_signature(self.data.voters[vote.voterId].publicKey, vote.signature, sp.pack((sp.self_address, (proposalVotes.initiatorId, proposalVotes.proposalId)))), 'MultisignAdmin_Badsig')
        y10 = sp.local("y10", self.registerVote(sp.record(in_param = sp.record(initiatorId = proposalVotes.initiatorId, proposalId = proposalVotes.proposalId, voterId = vote.voterId, yay = vote.yay), in_storage = self.data)))
        self.data = y10.value.storage
        sp.for op in y10.value.operations.rev():
          sp.operations().push(op)
      sp.if (sp.len(self.data.proposals[proposalVotes.initiatorId].nay) + sp.len(self.data.proposals[proposalVotes.initiatorId].yay)) >= self.data.quorum:
        sp.if sp.len(self.data.proposals[proposalVotes.initiatorId].nay) >= sp.len(self.data.proposals[proposalVotes.initiatorId].yay):
          self.data.proposals[proposalVotes.initiatorId].canceled = True
        sp.else:
          y11 = sp.local("y11", self.onVoted(sp.record(in_param = self.data.proposals[proposalVotes.initiatorId], in_storage = self.data)))
          self.data = y11.value.storage
          sp.for op in y11.value.operations.rev():
            sp.operations().push(op)

  @sp.entry_point
  def newProposal(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), 'MultisignAdmin_VoterUnknown')
    self.data.voters[self.data.addrVoterId[sp.sender]].lastProposalId += 1
    self.data.proposals[self.data.addrVoterId[sp.sender]] = sp.record(batchs = params, canceled = False, id = self.data.voters[self.data.addrVoterId[sp.sender]].lastProposalId, nay = sp.set([]), startedAt = sp.now, yay = sp.set([self.data.addrVoterId[sp.sender]]))
    sp.if self.data.quorum < 2:
      y12 = sp.local("y12", self.onVoted(sp.record(in_param = self.data.proposals[self.data.addrVoterId[sp.sender]], in_storage = self.data)))
      self.data = y12.value.storage
      sp.for op in y12.value.operations.rev():
        sp.operations().push(op)

  @sp.entry_point
  def vote(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), 'MultisignAdmin_VoterUnknown')
    sp.for vote in params:
      y13 = sp.local("y13", self.registerVote(sp.record(in_param = sp.record(initiatorId = vote.initiatorId, proposalId = vote.proposalId, voterId = self.data.addrVoterId[sp.sender], yay = vote.yay), in_storage = self.data)))
      self.data = y13.value.storage
      sp.for op in y13.value.operations.rev():
        sp.operations().push(op)
      sp.if (sp.len(self.data.proposals[vote.initiatorId].nay) + sp.len(self.data.proposals[vote.initiatorId].yay)) >= self.data.quorum:
        sp.if sp.len(self.data.proposals[vote.initiatorId].nay) >= sp.len(self.data.proposals[vote.initiatorId].yay):
          self.data.proposals[vote.initiatorId].canceled = True
        sp.else:
          y14 = sp.local("y14", self.onVoted(sp.record(in_param = self.data.proposals[vote.initiatorId], in_storage = self.data)))
          self.data = y14.value.storage
          sp.for op in y14.value.operations.rev():
            sp.operations().push(op)