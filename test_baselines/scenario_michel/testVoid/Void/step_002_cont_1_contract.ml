open Smartml

module Contract = struct

  let%entry_point ep self () =
    ()

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [ep]
end