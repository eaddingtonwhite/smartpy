open Smartml

module Contract = struct

  let%entry_point vote self params =
    set_type (params.vote : string);
    self.data.votes.push({sender = sender; vote = params.vote})

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(votes = sp.TRecord(sender = address, vote = string).layout(("sender", "vote")) list).layout("votes")
      ~storage:[%expr
                 {votes = sp.list([])}]
      [vote]
end