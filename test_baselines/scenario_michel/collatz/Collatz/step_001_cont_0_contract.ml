open Smartml

module Contract = struct

  let%entry_point run self params =
    sp.transfer(params.x // 2, sp.tez(0), params.k)

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [run]
end