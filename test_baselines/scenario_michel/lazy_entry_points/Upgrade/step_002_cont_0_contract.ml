open Smartml

module Contract = struct

  let%entry_point bounce self params =
    set_type (params : address)

  let%entry_point check_bounce self () =
    verify sp.has_entry_point("bounce")

  let%entry_point modify_x self params =
    self.data.x <- params

  let%entry_point take_ticket self params =
    set_type (params : int ticket)

  let%entry_point update_bounce self params =
    with sp.set_entry_point("bounce", params):
<br>;
    sp.send(sender, sp.tez(1))

  let%entry_point update_modify_x self params =
    with sp.set_entry_point("modify_x", params):
<br>

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = int, y = bool).layout(("x", "y"))
      ~storage:[%expr
                 {x = 1,
                  y = True}]
      [bounce; check_bounce; modify_x; take_ticket; update_bounce; update_modify_x]
end