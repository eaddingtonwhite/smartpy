open Smartml

module Contract = struct

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = intOrNat).layout("x")
      ~storage:[%expr
                 {x = 2}]
      []
end