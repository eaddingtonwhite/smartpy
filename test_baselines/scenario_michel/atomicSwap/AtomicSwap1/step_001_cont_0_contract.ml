open Smartml

module Contract = struct

  let%entry_point allSigned self () =
    verify (self.data.notional <> sp.tez(0));
    verify (self.data.owner = sender);
    sp.send(self.data.counterparty, self.data.notional);
    self.data.notional <- sp.tez(0)

  let%entry_point cancelSwap self () =
    verify (self.data.notional <> sp.tez(0));
    verify (self.data.owner = sender);
    verify (self.data.epoch < now);
    sp.send(self.data.owner, self.data.notional);
    self.data.notional <- sp.tez(0)

  let%entry_point knownSecret self params =
    verify (self.data.notional <> sp.tez(0));
    verify (self.data.counterparty = sender);
    verify (self.data.hashedSecret = (blake2b params.secret));
    sp.send(self.data.counterparty, self.data.notional);
    self.data.notional <- sp.tez(0)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(counterparty = address, epoch = timestamp, hashedSecret = bytes, notional = mutez, owner = address).layout(("counterparty", ("epoch", ("hashedSecret", ("notional", "owner")))))
      ~storage:[%expr
                 {counterparty = sp.address('tz0FakeRobert'),
                  epoch = sp.timestamp(50),
                  hashedSecret = sp.bytes('0x1f98c84caf714d00ede5d23142bc166d84f8cd42adc18be22c3d47453853ea49'),
                  notional = sp.mutez(12),
                  owner = sp.address('tz0FakeAlice')}]
      [allSigned; cancelSwap; knownSecret]
end