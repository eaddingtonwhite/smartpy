open Smartml

module Contract = struct

  let%entry_point call_f self params =
    let%var __s1 = self.data.x + params in
    self.data.x <- __s1.value;
    let%var __s2 = self.data.x + 5 in
    self.data.x <- __s2.value;
    let%var __s3 = self.data.x + 32 in
    self.data.x <- __s3.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = intOrNat, y = intOrNat).layout(("x", "y"))
      ~storage:[%expr
                 {x = 12,
                  y = 0}]
      [call_f]
end