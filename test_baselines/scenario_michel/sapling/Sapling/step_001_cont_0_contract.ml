open Smartml

module Contract = struct

  let%entry_point entry_point_1 self params =
    set_type (params.s : 15 saplingState);
    self.data.y <- some sp.sapling_verify_update(params.s, params.t)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(h = sp.TLambda(sp.TRecord(a = 12 saplingState, b = 12 saplingTransaction).layout(("a", "b")), (int * 12 saplingState) option), x = intOrNat, y = (int * 15 saplingState) option option, z = sp.TLambda(intOrNat, (intOrNat * 8 saplingState))).layout(("h", ("x", ("y", "z"))))
      ~storage:[%expr
                 {h = lambda(sp.TLambda(sp.TRecord(a = 12 saplingState, b = 12 saplingTransaction).layout(("a", "b")), (int * 12 saplingState) option)),
                  x = 12,
                  y = sp.none,
                  z = lambda(sp.TLambda(intOrNat, (intOrNat * 8 saplingState)))}]
      [entry_point_1]
end