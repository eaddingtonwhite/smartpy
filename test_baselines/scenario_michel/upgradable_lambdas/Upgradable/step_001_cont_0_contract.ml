open Smartml

module Contract = struct

  let%entry_point calc self params =
    self.data.value <- self.data.logic(params)

  let%entry_point updateLogic self params =
    self.data.logic <- params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(logic = sp.TLambda(bytes, nat), value = nat).layout(("logic", "value"))
      ~storage:[%expr
                 {logic = lambda(sp.TLambda(bytes, nat)),
                  value = 0}]
      [calc; updateLogic]
end