open Smartml

module Contract = struct

  let%entry_point f self () =
    y2 = sp.local("y2", self.a({in_param = 5; in_storage = self.data}));
    self.data <- y2.value.storage;
    sp.for op in y2.value.operations.rev():
      operations().push(op);
    y3 = sp.local("y3", self.a({in_param = 10; in_storage = self.data}));
    self.data <- y3.value.storage;
    sp.for op in y3.value.operations.rev():
      operations().push(op);
    self.data.z <- y2.value.result + y3.value.result

  let%entry_point g self () =
    y4 = sp.local("y4", self.a({in_param = 6; in_storage = self.data}));
    self.data <- y4.value.storage;
    sp.for op in y4.value.operations.rev():
      operations().push(op);
    self.data.z <- y4.value.result

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = nat, y = string, z = nat).layout(("x", ("y", "z")))
      ~storage:[%expr
                 {x = 2,
                  y = 'aaa',
                  z = 0}]
      [f; g]
end