import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TNat, y = sp.TString, z = sp.TNat).layout(("x", ("y", "z"))))
    self.init(x = 2,
              y = 'aaa',
              z = 0)

  @sp.entry_point
  def f(self):
    y2 = sp.local("y2", self.a(sp.record(in_param = 5, in_storage = self.data)))
    self.data = y2.value.storage
    sp.for op in y2.value.operations.rev():
      sp.operations().push(op)
    y3 = sp.local("y3", self.a(sp.record(in_param = 10, in_storage = self.data)))
    self.data = y3.value.storage
    sp.for op in y3.value.operations.rev():
      sp.operations().push(op)
    self.data.z = y2.value.result + y3.value.result

  @sp.entry_point
  def g(self):
    y4 = sp.local("y4", self.a(sp.record(in_param = 6, in_storage = self.data)))
    self.data = y4.value.storage
    sp.for op in y4.value.operations.rev():
      sp.operations().push(op)
    self.data.z = y4.value.result