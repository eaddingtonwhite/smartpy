open Smartml

module Contract = struct

  let%entry_point mul_int_fr self params =
    match_pair_mul_bls12_381_int_fr_17_fst, match_pair_mul_bls12_381_int_fr_17_snd = sp.match_tuple(params, "match_pair_mul_bls12_381_int_fr_17_fst", "match_pair_mul_bls12_381_int_fr_17_snd")
    self.data.fr <- some (mul match_pair_mul_bls12_381_int_fr_17_fst match_pair_mul_bls12_381_int_fr_17_snd)

  let%entry_point mul_nat_fr self params =
    match_pair_mul_bls12_381_int_fr_30_fst, match_pair_mul_bls12_381_int_fr_30_snd = sp.match_tuple(params, "match_pair_mul_bls12_381_int_fr_30_fst", "match_pair_mul_bls12_381_int_fr_30_snd")
    self.data.fr <- some (mul match_pair_mul_bls12_381_int_fr_30_fst match_pair_mul_bls12_381_int_fr_30_snd)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(fr = bls12_381_fr option).layout("fr")
      ~storage:[%expr
                 {fr = sp.none}]
      [mul_int_fr; mul_nat_fr]
end