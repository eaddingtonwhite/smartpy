open Smartml

module Contract = struct

  let%entry_point breed self params =
    verify (params.parent1 <> params.parent2);
    if False then
      verify (sp.tez(0) < self.data.kitties[params.parent1].borrowPrice);
      verify (self.data.kitties[params.parent1].borrowPrice < params.borrowPrice);
      verify (amount = params.borrowPrice);
      sp.send(self.data.kitties[params.parent1].owner, params.borrowPrice);
    verify (self.data.kitties[params.parent1].auction < now);
    verify (self.data.kitties[params.parent1].hatching < now);
    if self.data.kitties[params.parent2].owner <> sender then
      verify (sp.tez(0) < self.data.kitties[params.parent2].borrowPrice);
      verify (self.data.kitties[params.parent2].borrowPrice < params.borrowPrice);
      verify (amount = params.borrowPrice);
      sp.send(self.data.kitties[params.parent2].owner, params.borrowPrice);
    verify (self.data.kitties[params.parent2].auction < now);
    verify (self.data.kitties[params.parent2].hatching < now);
    self.data.kitties[params.parent1].hatching <- add_seconds now 100;
    self.data.kitties[params.parent2].hatching <- add_seconds now 100;
    self.data.kitties[params.kittyId] <- {auction = timestamp 0; borrowPrice = sp.tez(0); generation = 1 + (max self.data.kitties[params.parent1].generation self.data.kitties[params.parent2].generation); hatching = add_seconds now 100; isNew = False; kittyId = params.kittyId; owner = sender; price = sp.tez(0)}

  let%entry_point build self params =
    verify (self.data.creator = sender);
    verify params.kitty.isNew;
    set_type (params.kitty.kittyId : int);
    self.data.kitties[params.kitty.kittyId] <- params.kitty

  let%entry_point buy self params =
    verify (sp.tez(0) < self.data.kitties[params.kittyId].price);
    verify (self.data.kitties[params.kittyId].price <= params.price);
    verify (amount = params.price);
    sp.send(self.data.kitties[params.kittyId].owner, params.price);
    self.data.kitties[params.kittyId].owner <- sender;
    if self.data.kitties[params.kittyId].isNew then
      self.data.kitties[params.kittyId].isNew <- False;
      self.data.kitties[params.kittyId].auction <- add_seconds now 10;
    verify (now <= self.data.kitties[params.kittyId].auction);
    if now <= self.data.kitties[params.kittyId].auction then
      self.data.kitties[params.kittyId].price <- params.price + sp.mutez(1)

  let%entry_point lend self params =
    verify (sp.tez(0) <= params.price);
    if False then
      verify (sp.tez(0) < self.data.kitties[params.kittyId].borrowPrice);
      verify (self.data.kitties[params.kittyId].borrowPrice < params.borrowPrice);
      verify (amount = params.borrowPrice);
      sp.send(self.data.kitties[params.kittyId].owner, params.borrowPrice);
    verify (self.data.kitties[params.kittyId].auction < now);
    verify (self.data.kitties[params.kittyId].hatching < now);
    self.data.kitties[params.kittyId].borrowPrice <- params.price

  let%entry_point sell self params =
    verify (sp.tez(0) <= params.price);
    if False then
      verify (sp.tez(0) < self.data.kitties[params.kittyId].borrowPrice);
      verify (self.data.kitties[params.kittyId].borrowPrice < params.borrowPrice);
      verify (amount = params.borrowPrice);
      sp.send(self.data.kitties[params.kittyId].owner, params.borrowPrice);
    verify (self.data.kitties[params.kittyId].auction < now);
    verify (self.data.kitties[params.kittyId].hatching < now);
    self.data.kitties[params.kittyId].price <- params.price

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(creator = address, kitties = map(int, sp.TRecord(auction = timestamp, borrowPrice = mutez, generation = intOrNat, hatching = timestamp, isNew = bool, kittyId = int, owner = address, price = mutez).layout(("auction", ("borrowPrice", ("generation", ("hatching", ("isNew", ("kittyId", ("owner", "price")))))))))).layout(("creator", "kitties"))
      ~storage:[%expr
                 {creator = sp.address('tz0FakeCreator'),
                  kitties = {}}]
      [breed; build; buy; lend; sell]
end