open Smartml

module Contract = struct

  let%entry_point receive_balances self params =
    set_type (params : sp.TRecord(balance = nat, request = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id"))).layout(("request", "balance")) list);
    self.data.last_sum <- 0;
    sp.for resp in params:
      self.data.last_sum += resp.balance

  let%entry_point reinit self () =
    self.data.last_sum <- 0

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(last_sum = nat, operator_support = bool).layout(("last_sum", "operator_support"))
      ~storage:[%expr
                 {last_sum = 0,
                  operator_support = False}]
      [receive_balances; reinit]
end