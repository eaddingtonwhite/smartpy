open Smartml

module Contract = struct

  let%entry_point concatenating self params =
    self.data.s0 <- some (concat params.s);
    self.data.b0 <- some (concat sp.list([params.b0, params.b1, concat params.sb]))

  let%entry_point concatenating2 self params =
    self.data.s0 <- some (params.s1 + params.s2);
    self.data.b0 <- some (params.b1 + params.b2)

  let%entry_point slicing self params =
    self.data.s0 <- sp.slice(params.s, 2, 5);
    self.data.b0 <- sp.slice(params.b, 1, 2);
    self.data.l0 <- len params.s;
    with self.data.s0.match('Some') as Some:
      self.data.l0 += len Some;
    self.data.l1 <- len params.b

  let%entry_point test_nat_of_string self params =
    res = sp.local("res", 0);
    sp.for idx in sp.range(0, len params):
      res.value <- (10 * res.value) + {"0" : 0, "1" : 1, "2" : 2, "3" : 3, "4" : 4, "5" : 5, "6" : 6, "7" : 7, "8" : 8, "9" : 9}[sp.slice(params, idx, 1).open_some()];
    self.data.nat_of_string <- res.value

  let%entry_point test_split self params =
    prev_idx = sp.local("prev_idx", 0);
    res = sp.local("res", sp.list([]));
    sp.for idx in sp.range(0, len params):
      if sp.slice(params, idx, 1).open_some() = "," then
        res.value.push(sp.slice(params, prev_idx.value, as_nat (idx - prev_idx.value)).open_some());
        prev_idx.value <- idx + 1;
    if (len params) > 0 then
      res.value.push(sp.slice(params, prev_idx.value, as_nat ((len params) - prev_idx.value)).open_some());
    self.data.split <- res.value.rev()

  let%entry_point test_string_of_nat self params =
    x = sp.local("x", params);
    res = sp.local("res", sp.list([]));
    if x.value = 0 then
      res.value.push("0");
    sp.while x.value > 0:
      res.value.push({0 : "0", 1 : "1", 2 : "2", 3 : "3", 4 : "4", 5 : "5", 6 : "6", 7 : "7", 8 : "8", 9 : "9"}[x.value % 10]);
      x.value //= 10;
    self.data.string_of_nat <- concat res.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(b0 = bytes option, l0 = nat, l1 = nat, nat_of_string = intOrNat, s0 = string option, split = string list, string_of_nat = string).layout(("b0", ("l0", ("l1", ("nat_of_string", ("s0", ("split", "string_of_nat")))))))
      ~storage:[%expr
                 {b0 = sp.some(sp.bytes('0xaa')),
                  l0 = 0,
                  l1 = 0,
                  nat_of_string = 0,
                  s0 = sp.some('hello'),
                  split = sp.list([]),
                  string_of_nat = ''}]
      [concatenating; concatenating2; slicing; test_nat_of_string; test_split; test_string_of_nat]
end