open Smartml

module Contract = struct

  let%entry_point compute self params =
    def f6(lparams_6):
      result = sp.local("result", 1);
      sp.for i in sp.range(0, fst lparams_6):
        result.value *= snd lparams_6;
      result.value
    bin_ops = sp.local("bin_ops", {"+" : sp.build_lambda(lambda lparams_3: (fst lparams_3) + (snd lparams_3)), "*" : sp.build_lambda(lambda lparams_4: (fst lparams_4) * (snd lparams_4)), "-" : sp.build_lambda(lambda lparams_5: (snd lparams_5) - (fst lparams_5)), "^" : sp.build_lambda(f6)});
    elements = sp.local("elements", self.string_split({s = params; sep = " "}));
    stack = sp.local("stack", sp.list([]), int list);
    formulas = sp.local("formulas", sp.list([]));
    sp.for element in elements.value:
      if element <> "" then
        if bin_ops.value.contains(element) then
          with sp.match_cons(stack.value) as match_cons_67:
            with sp.match_cons(match_cons_67.tail) as match_cons_68:
              stack.value <- match_cons_68.tail;
              stack.value.push(bin_ops.value[element]((match_cons_67.head, match_cons_68.head)))
            else:
              failwith "Bad formula: too many operators"
          else:
            failwith "Bad formula: too many operators";
          with sp.match_cons(formulas.value) as match_cons_75:
            with sp.match_cons(match_cons_75.tail) as match_cons_76:
              formulas.value <- match_cons_76.tail;
              formulas.value.push(concat sp.list(["(", match_cons_76.head, element, match_cons_75.head, ")"]))
            else:
              ()
          else:
            ()
        else
          stack.value.push(self.nat_of_string(element));
          formulas.value.push(element);
    self.data.formula <- params;
    self.data.operations <- elements.value;
    length = sp.local("length", len stack.value);
    verify (length.value = 1) ("Bad stack at the end of the computation. Length = " + self.string_of_nat(length.value), stack.value);
    with sp.match_cons(stack.value) as match_cons_86:
      with sp.match_cons(formulas.value) as match_cons_87:
        self.data.result <- match_cons_86.head;
        if match_cons_86.head >= 0 then
          self.data.summary <- (match_cons_87.head + " = ") + self.string_of_nat(as_nat match_cons_86.head)
        else
          self.data.summary <- (match_cons_87.head + " = -") + self.string_of_nat(as_nat (- match_cons_86.head))
      else:
        ()
    else:
      ()

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(Help = string list, formula = string, operations = string list, result = int, summary = string).layout(("Help", ("formula", ("operations", ("result", "summary")))))
      ~storage:[%expr
                 {Help = sp.list(['SmartPy Reverse Polish Calculator Template', 'Operations: +, -, *, ^', 'Example: 2 3 4 * - 3 ^']),
                  formula = '',
                  operations = sp.list([]),
                  result = 0,
                  summary = ''}]
      [compute]
end