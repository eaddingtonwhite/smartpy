open Smartml

module Contract = struct

  let%entry_point add self params =
    self.data.value <- params.x + params.y

  let%entry_point factorial self params =
    self.data.value <- 1;
    sp.for y in sp.range(1, params + 1):
      self.data.value *= y

  let%entry_point log2 self params =
    self.data.value <- 0;
    y = sp.local("y", params);
    sp.while y.value > 1:
      self.data.value += 1;
      y.value //= 2

  let%entry_point multiply self params =
    self.data.value <- params.x * params.y

  let%entry_point other self params =
    compute_test_exception_optimization_47 = sp.local("compute_test_exception_optimization_47", {1 : 2, 2 : 3});
    self.data.value <- compute_test_exception_optimization_47.value[params];
    self.data.value += compute_test_exception_optimization_47.value.get(params, default_value = 12);
    self.data.value += compute_test_exception_optimization_47.value.get(params, message = "not here")

  let%entry_point square self params =
    self.data.value <- params * params

  let%entry_point squareRoot self params =
    verify (params >= 0);
    y = sp.local("y", params);
    sp.while (y.value * y.value) > params:
      y.value <- ((params // y.value) + y.value) // 2;
    verify (((y.value * y.value) <= params) & (params < ((y.value + 1) * (y.value + 1))));
    self.data.value <- y.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(value = nat).layout("value")
      ~storage:[%expr
                 {value = 0}]
      [add; factorial; log2; multiply; other; square; squareRoot]
end