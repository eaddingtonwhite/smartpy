import smartpy as sp

class Mul__Bls12_381_fr_fr(sp.Contract):
    def __init__(self):
        self.init(fr = sp.none)

    """
    MUL: Multiply a curve point or field element by a scalar field element. Fr
    elements can be built from naturals by multiplying by the unit of Fr using PUSH bls12_381_fr 1; MUL. Note
    that the multiplication will be computed using the natural modulo the order
    of Fr.

    :: bls12_381_fr : bls12_381_fr : 'S -> bls12_381_fr : 'S
    """
    @sp.entry_point
    def mul(self, pair):
        self.data.fr = sp.some(sp.mul(sp.fst(pair), sp.snd(pair)))

@sp.add_test(name = "Mul__Bls12_381_g2_fr")
def test():
    c1 = Mul__Bls12_381_fr_fr();

    scenario = sp.test_scenario()
    scenario += c1

    c1.mul(
        sp.pair(
            sp.bls12_381_fr("0xd30edc8fce6c34442d371da0e24fc3fb83ea957cfea9766c62a531dda98e4b52"),
            sp.bls12_381_fr("0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221")
        )
    );
