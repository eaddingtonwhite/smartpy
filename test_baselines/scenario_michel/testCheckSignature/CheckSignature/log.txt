Comment...
 h1: Check Signature
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_001_cont_0_pre_michelson.michel 44
 -> (Pair "edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG" (Pair 0 "Hello World"))
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_001_cont_0_storage.tz 1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_001_cont_0_storage.json 1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_001_cont_0_storage.py 1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_001_cont_0_types.py 7
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_001_cont_0_contract.tz 44
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_001_cont_0_contract.json 75
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_001_cont_0_contract.py 14
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_001_cont_0_contract.ml 18
Comment...
 h2: Successful Call
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_003_cont_0_params.py 1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_003_cont_0_params.tz 1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_003_cont_0_params.json 1
Executing setCurrentValue(sp.record(newValue = 'should work', userSignature = sp.to_constant(sp.make_signature(secret_key = sp.to_constant(sp.test_account("Alice").secret_key), message = sp.pack(sp.record(c = 0, n = 'should work', o = 'Hello World')), message_format = 'Raw'))))...
 -> (Pair "edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG" (Pair 1 "should work"))
Comment...
 h2: Replay Attack
Comment...
 p: Trying to reuse the same signature is blocked by the value of the counter.
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_006_cont_0_params.py 1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_006_cont_0_params.tz 1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_006_cont_0_params.json 1
Executing setCurrentValue(sp.record(newValue = 'should work', userSignature = sp.to_constant(sp.make_signature(secret_key = sp.to_constant(sp.test_account("Alice").secret_key), message = sp.pack(sp.record(c = 0, n = 'should work', o = 'Hello World')), message_format = 'Raw'))))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.check_signature(self.data.bossPublicKey, params.userSignature, sp.pack(sp.record(c = self.data.counter, n = params.newValue, o = self.data.currentValue))) : sp.TBool) (python/templates/testCheckSignature.py, line 11)
 (python/templates/testCheckSignature.py, line 11)
Comment...
 h2: Signature From Wrong Secret Key
Comment...
 p: Signing the right thing from a different secret-key.
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_009_cont_0_params.py 1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_009_cont_0_params.tz 1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_009_cont_0_params.json 4
Executing setCurrentValue(sp.record(newValue = 'Hello again World', userSignature = sp.to_constant(sp.make_signature(secret_key = sp.to_constant(sp.test_account("Robert").secret_key), message = sp.pack(sp.record(c = 1, n = 'Hello again World', o = 'should work')), message_format = 'Raw'))))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.check_signature(self.data.bossPublicKey, params.userSignature, sp.pack(sp.record(c = self.data.counter, n = params.newValue, o = self.data.currentValue))) : sp.TBool) (python/templates/testCheckSignature.py, line 11)
 (python/templates/testCheckSignature.py, line 11)
Comment...
 h2: Second Successful Call
Comment...
 p: Showing that the previous call failed <b>because</b> of the secret-key (signing same bytes).
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_012_cont_0_params.py 1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_012_cont_0_params.tz 1
 => test_baselines/scenario_michel/testCheckSignature/CheckSignature/step_012_cont_0_params.json 4
Executing setCurrentValue(sp.record(newValue = 'Hello again World', userSignature = sp.to_constant(sp.make_signature(secret_key = sp.to_constant(sp.test_account("Alice").secret_key), message = sp.pack(sp.record(c = 1, n = 'Hello again World', o = 'should work')), message_format = 'Raw'))))...
 -> (Pair "edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG" (Pair 2 "Hello again World"))
