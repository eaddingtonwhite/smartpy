open Smartml

module Contract = struct

  let%entry_point balance_of self params =
    verify (not self.data.paused) "FA2_PAUSED";
    set_type (params : sp.TRecord(callback = sp.TRecord(balance = nat, request = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id"))).layout(("request", "balance")) list contract, requests = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id")) list).layout(("requests", "callback")));
    def f0(lparams_0):
      verify (self.data.token_metadata.contains(lparams_0.token_id)) "FA2_TOKEN_UNDEFINED";
      if self.data.ledger.contains((lparams_0.owner : address)) then
        {request = {owner = (lparams_0.owner : address); token_id = (lparams_0.token_id : nat)}; balance = self.data.ledger[(lparams_0.owner : address)].balance}
      else
        {request = {owner = (lparams_0.owner : address); token_id = (lparams_0.token_id : nat)}; balance = 0}
    responses = sp.local("responses", params.requests.map(sp.build_lambda(f0)));
    sp.transfer(responses.value, sp.tez(0), (params.callback : sp.TRecord(balance = nat, request = sp.TRecord(owner = address, token_id = nat).layout(("owner", "token_id"))).layout(("request", "balance")) list contract))

  let%entry_point mint self params =
    verify (sender = self.data.administrator) "FA2_NOT_ADMIN";
    verify (params.token_id = 0) "single-asset: token-id <> 0";
    verify (self.data.all_tokens = params.token_id) "Token-IDs should be consecutive";
    self.data.all_tokens <- max self.data.all_tokens (params.token_id + 1);
    if self.data.ledger.contains((params.address : address)) then
      self.data.ledger[(params.address : address)].balance += params.amount
    else
      self.data.ledger[(params.address : address)] <- {balance = params.amount};
    if self.data.token_metadata.contains(params.token_id) then
      self.data.total_supply[params.token_id] <- params.amount
    else
      self.data.token_metadata[params.token_id] <- {token_id = params.token_id; token_info = params.metadata};
      self.data.total_supply[params.token_id] <- params.amount

  let%entry_point proxy self params =
    set_type (params : sp.TRecord(amount = nat, escrow = address, request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket).layout(("amount", ("escrow", "request"))));
    escrow_oracle_553, request_oracle_553, amount_oracle_553 = sp.match_record(params, "escrow", "request", "amount")
    verify (not self.data.paused) "FA2_PAUSED";
    set_type (sp.list([{from_ = sender; txs = sp.list([{to_ = escrow_oracle_553; token_id = 0; amount = amount_oracle_553}])}]) : sp.TRecord(from_ = address, txs = sp.TRecord(amount = nat, to_ = address, token_id = nat).layout(("to_", ("token_id", "amount"))) list).layout(("from_", "txs")) list);
    sp.for transfer in sp.list([{from_ = sender; txs = sp.list([{amount = amount_oracle_553; to_ = escrow_oracle_553; token_id = 0}])}]):
      sp.for tx in transfer.txs:
        verify (tx.token_id = 0) "single-asset: token-id <> 0";
        verify ((((sender = self.data.administrator) | (transfer.from_ = sender)) | (self.data.operators.contains(({owner = transfer.from_; operator = sender; token_id = tx.token_id} : sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id"))))))) | (sender = self_address)) "FA2_NOT_OPERATOR";
        verify (self.data.token_metadata.contains(tx.token_id)) "FA2_TOKEN_UNDEFINED";
        if tx.amount > 0 then
          verify (self.data.ledger[(transfer.from_ : address)].balance >= tx.amount) "FA2_INSUFFICIENT_BALANCE";
          self.data.ledger[(transfer.from_ : address)].balance <- as_nat (self.data.ledger[(transfer.from_ : address)].balance - tx.amount);
          if self.data.ledger.contains((tx.to_ : address)) then
            self.data.ledger[(tx.to_ : address)].balance += tx.amount
          else
            self.data.ledger[(tx.to_ : address)] <- {balance = tx.amount};
    sp.transfer({amount = amount_oracle_553; request = request_oracle_553; sender = sender}, sp.tez(0), sp.contract(sp.TRecord(amount = nat, request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, sender = address).layout(("amount", ("request", "sender"))), escrow_oracle_553, entry_point='send_request').open_some(message = "TokenWrongEscrowInterface"))

  let%entry_point set_administrator self params =
    verify (sender = self.data.administrator) "FA2_NOT_ADMIN";
    self.data.administrator <- params

  let%entry_point set_metadata self params =
    verify (sender = self.data.administrator) "FA2_NOT_ADMIN";
    self.data.metadata[params.k] <- params.v

  let%entry_point set_pause self params =
    verify (sender = self.data.administrator) "FA2_NOT_ADMIN";
    self.data.paused <- params

  let%entry_point transfer self params =
    verify (not self.data.paused) "FA2_PAUSED";
    set_type (params : sp.TRecord(from_ = address, txs = sp.TRecord(amount = nat, to_ = address, token_id = nat).layout(("to_", ("token_id", "amount"))) list).layout(("from_", "txs")) list);
    sp.for transfer in params:
      sp.for tx in transfer.txs:
        verify (tx.token_id = 0) "single-asset: token-id <> 0";
        verify ((((sender = self.data.administrator) | (transfer.from_ = sender)) | (self.data.operators.contains(({owner = transfer.from_; operator = sender; token_id = tx.token_id} : sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id"))))))) | (sender = self_address)) "FA2_NOT_OPERATOR";
        verify (self.data.token_metadata.contains(tx.token_id)) "FA2_TOKEN_UNDEFINED";
        if tx.amount > 0 then
          verify (self.data.ledger[(transfer.from_ : address)].balance >= tx.amount) "FA2_INSUFFICIENT_BALANCE";
          self.data.ledger[(transfer.from_ : address)].balance <- as_nat (self.data.ledger[(transfer.from_ : address)].balance - tx.amount);
          if self.data.ledger.contains((tx.to_ : address)) then
            self.data.ledger[(tx.to_ : address)].balance += tx.amount
          else
            self.data.ledger[(tx.to_ : address)] <- {balance = tx.amount}

  let%entry_point update_operators self params =
    set_type (params : sp.TVariant(add_operator = sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator")) list);
    sp.for update in params:
      with update.match_cases() as arg:
        with arg.match('add_operator') as add_operator:
          verify ((add_operator.owner = sender) | (sender = self.data.administrator)) "FA2_NOT_ADMIN_OR_OPERATOR";
          self.data.operators[({owner = add_operator.owner; operator = add_operator.operator; token_id = add_operator.token_id} : sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id"))))] <- ()
        with arg.match('remove_operator') as remove_operator:
          verify ((remove_operator.owner = sender) | (sender = self.data.administrator)) "FA2_NOT_ADMIN_OR_OPERATOR";
          del self.data.operators[({owner = remove_operator.owner; operator = remove_operator.operator; token_id = remove_operator.token_id} : sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id"))))]


  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(administrator = address, all_tokens = nat, ledger = bigMap(address, sp.TRecord(balance = nat).layout("balance")), metadata = bigMap(string, bytes), operators = bigMap(sp.TRecord(operator = address, owner = address, token_id = nat).layout(("owner", ("operator", "token_id"))), unit), paused = bool, token_metadata = bigMap(nat, sp.TRecord(token_id = nat, token_info = map(string, bytes)).layout(("token_id", "token_info"))), total_supply = bigMap(nat, nat)).layout(("administrator", ("all_tokens", ("ledger", ("metadata", ("operators", ("paused", ("token_metadata", "total_supply"))))))))
      ~storage:[%expr
                 {administrator = sp.address('tz0FakeAdministrator'),
                  all_tokens = 0,
                  ledger = {},
                  metadata = {'' : sp.bytes('0x')},
                  operators = {},
                  paused = False,
                  token_metadata = {},
                  total_supply = {}}]
      [balance_of; mint; proxy; set_administrator; set_metadata; set_pause; transfer; update_operators]
end