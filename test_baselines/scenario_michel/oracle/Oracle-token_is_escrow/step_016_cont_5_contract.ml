open Smartml

module Contract = struct

  let%entry_point set_value self params =
    verify (sender = self.data.escrow) "ReceiverNotEscrow";
    set_type (params : sp.TRecord(request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, result = sp.TRecord(client = address, client_request_id = nat, result = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))), tag = string).layout(("client", ("client_request_id", ("result", "tag")))) ticket).layout(("request", "result")));
    request_oracle_495, result_oracle_495 = sp.match_record(params, "request", "result")
    ticket_oracle_497_data, ticket_oracle_497_copy = sp.match_tuple(read_ticket_raw request_oracle_495, "ticket_oracle_497_data", "ticket_oracle_497_copy")
    ticket_oracle_497_ticketer, ticket_oracle_497_content, ticket_oracle_497_amount = sp.match_tuple(ticket_oracle_497_data, "ticket_oracle_497_ticketer", "ticket_oracle_497_content", "ticket_oracle_497_amount")
    ticket_oracle_498_data, ticket_oracle_498_copy = sp.match_tuple(read_ticket_raw result_oracle_495, "ticket_oracle_498_data", "ticket_oracle_498_copy")
    ticket_oracle_498_ticketer, ticket_oracle_498_content, ticket_oracle_498_amount = sp.match_tuple(ticket_oracle_498_data, "ticket_oracle_498_ticketer", "ticket_oracle_498_content", "ticket_oracle_498_amount")
    verify (ticket_oracle_497_ticketer = self.data.requester) "ReceiverBadRequester";
    verify (ticket_oracle_497_content.client_request_id > self.data.last_request_id) "ReceiverBadRequestId";
    self.data.last_request_id <- ticket_oracle_497_content.client_request_id;
    self.data.value <- some ticket_oracle_498_content.result.open_variant('int')

  let%entry_point setup self params =
    verify (sender = self.data.admin) "ReceiverNotAdmin";
    self.data.requester <- params.requester;
    self.data.escrow <- params.escrow

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(admin = address, escrow = address, last_request_id = nat, requester = address, value = int option).layout(("admin", ("escrow", ("last_request_id", ("requester", "value")))))
      ~storage:[%expr
                 {admin = sp.address('tz0FakeReceiver1 admin'),
                  escrow = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
                  last_request_id = 0,
                  requester = sp.address('KT1Tezooo3zzSmartPyzzSTATiCzzzseJjWC'),
                  value = sp.none}]
      [set_value; setup]
end