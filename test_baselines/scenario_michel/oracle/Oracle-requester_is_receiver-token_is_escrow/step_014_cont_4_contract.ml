open Smartml

module Contract = struct

  let%entry_point cancel_value self params =
    verify (sender = self.data.admin) "RequesterNotAdmin";
    sp.transfer({client_request_id = as_nat (self.data.next_request_id - 1); force = params.force; oracle = self.data.oracle}, sp.tez(0), sp.contract(sp.TRecord(client_request_id = nat, force = bool, oracle = address).layout(("client_request_id", ("force", "oracle"))), self.data.escrow, entry_point='cancel_request').open_some())

  let%entry_point request_value self params =
    verify (sender = self.data.admin) "RequesterNotAdmin";
    ticket_431 = sp.local("ticket_431", ticket {cancel_timeout = add_seconds now (params.cancel_timeout_minutes * 60); client_request_id = self.data.next_request_id; fulfill_timeout = add_seconds now (params.fulfill_timeout_minutes * 60); job_id = self.data.job_id; oracle = self.data.oracle; parameters = params.parameters; tag = "OracleRequest"; target = sp.self_entry_point_address('set_value')} 1);
    sp.transfer({amount = params.amount; request = ticket_431.value}, sp.tez(0), sp.contract(sp.TRecord(amount = nat, request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket).layout(("amount", "request")), self.data.escrow, entry_point='send_request').open_some());
    self.data.next_request_id += 1

  let%entry_point set_value self params =
    verify (sender = self.data.escrow) "ReceiverNotEscrow";
    set_type (params : sp.TRecord(request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, result = sp.TRecord(client = address, client_request_id = nat, result = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))), tag = string).layout(("client", ("client_request_id", ("result", "tag")))) ticket).layout(("request", "result")));
    request_oracle_495, result_oracle_495 = sp.match_record(params, "request", "result")
    ticket_oracle_497_data, ticket_oracle_497_copy = sp.match_tuple(read_ticket_raw request_oracle_495, "ticket_oracle_497_data", "ticket_oracle_497_copy")
    ticket_oracle_497_ticketer, ticket_oracle_497_content, ticket_oracle_497_amount = sp.match_tuple(ticket_oracle_497_data, "ticket_oracle_497_ticketer", "ticket_oracle_497_content", "ticket_oracle_497_amount")
    ticket_oracle_498_data, ticket_oracle_498_copy = sp.match_tuple(read_ticket_raw result_oracle_495, "ticket_oracle_498_data", "ticket_oracle_498_copy")
    ticket_oracle_498_ticketer, ticket_oracle_498_content, ticket_oracle_498_amount = sp.match_tuple(ticket_oracle_498_data, "ticket_oracle_498_ticketer", "ticket_oracle_498_content", "ticket_oracle_498_amount")
    verify (ticket_oracle_497_ticketer = self_address) "ReceiverBadRequester";
    verify (ticket_oracle_497_content.client_request_id = (as_nat (self.data.next_request_id - 1))) "ReceiverBadRequester";
    self.data.value <- some ticket_oracle_498_content.result.open_variant('int')

  let%entry_point setup self params =
    verify (sender = self.data.admin) "RequesterNotAdmin";
    self.data.admin <- params.admin;
    self.data.escrow <- params.escrow;
    self.data.oracle <- params.oracle;
    self.data.job_id <- params.job_id;
    self.data.token <- params.token

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(admin = address, escrow = address, job_id = bytes, next_request_id = nat, oracle = address, token = address, value = int option).layout(("admin", ("escrow", ("job_id", ("next_request_id", ("oracle", ("token", "value")))))))
      ~storage:[%expr
                 {admin = sp.address('tz0FakeRequester2 admin'),
                  escrow = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
                  job_id = sp.bytes('0x0001'),
                  next_request_id = 1,
                  oracle = sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H'),
                  token = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'),
                  value = sp.none}]
      [cancel_value; request_value; set_value; setup]
end