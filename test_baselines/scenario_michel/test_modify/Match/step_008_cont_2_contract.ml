open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    with sp.modify(self.data, "x") as "x":
      x.value + 1

  let init storage =
    Basics.build_contract
      ~tstorage:intOrNat
      ~storage:[%expr 42]
      [ep1]
end