import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat).layout(("x", "y")))
    self.init(x = 12,
              y = 0)