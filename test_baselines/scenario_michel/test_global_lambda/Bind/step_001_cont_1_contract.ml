open Smartml

module Contract = struct

  let%entry_point check self params =
    let%var __s1 = params.params * 2 in
    verify (__s1.value = params.result)

  let%entry_point test self params =
    let%var __s2 = params * 2 in
    self.data.x <- some __s2.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = intOrNat option).layout("x")
      ~storage:[%expr
                 {x = sp.none}]
      [check; test]
end