open Smartml

module Contract = struct

  let%entry_point change self params =
    self.data.x <- params.x;
    self.data.y <- params.y

  let%entry_point go_x self () =
    self.data.x += 1;
    self.data.y -= 1

  let%entry_point go_y self () =
    self.data.x -= 1;
    self.data.y += 1

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = int, y = int).layout(("x", "y"))
      ~storage:[%expr
                 {x = 0,
                  y = 0}]
      [change; go_x; go_y]
end