open Smartml

module Contract = struct

  let%entry_point ep self params =
    set_type (params : sp.TVariant(V1 = sp.TRecord(a = bool, b = string).layout(("b", "a")), V2 = nat).layout(("V2", "V1")) list)

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [ep]
end