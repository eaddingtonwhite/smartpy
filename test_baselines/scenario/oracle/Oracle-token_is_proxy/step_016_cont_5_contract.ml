open Smartml

module Contract = struct

  let%entry_point cancel_value self params =
    verify (sender = self.data.admin) "RequesterNotAdmin";
    sp.transfer({client_request_id = as_nat (self.data.next_request_id - 1); force = params.force; oracle = self.data.oracle}, sp.tez(0), sp.contract(sp.TRecord(client_request_id = nat, force = bool, oracle = address).layout(("client_request_id", ("force", "oracle"))), self.data.escrow, entry_point='cancel_request').open_some())

  let%entry_point request_value self params =
    verify (sender = self.data.admin) "RequesterNotAdmin";
    ticket_431 = sp.local("ticket_431", ticket {cancel_timeout = add_seconds now (params.cancel_timeout_minutes * 60); client_request_id = self.data.next_request_id; fulfill_timeout = add_seconds now (params.fulfill_timeout_minutes * 60); job_id = self.data.job_id; oracle = self.data.oracle; parameters = params.parameters; tag = "OracleRequest"; target = to_address sp.contract(sp.TRecord(request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket, result = sp.TRecord(client = address, client_request_id = nat, result = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))), tag = string).layout(("client", ("client_request_id", ("result", "tag")))) ticket).layout(("request", "result")), self.data.receiver.open_some(message = "RequesterTargetUnknown"), entry_point='set_value').open_some(message = "RequesterTargetUnknown")} 1);
    sp.transfer({amount = params.amount; escrow = self.data.escrow; request = ticket_431.value}, sp.tez(0), sp.contract(sp.TRecord(amount = nat, escrow = address, request = sp.TRecord(cancel_timeout = timestamp, client_request_id = nat, fulfill_timeout = timestamp, job_id = bytes, oracle = address, parameters = sp.TVariant(bytes = bytes, int = int, string = string).layout(("bytes", ("int", "string"))) option, tag = string, target = address).layout(("cancel_timeout", ("client_request_id", ("fulfill_timeout", ("job_id", ("oracle", ("parameters", ("tag", "target")))))))) ticket).layout(("amount", ("escrow", "request"))), self.data.token, entry_point='proxy').open_some());
    self.data.next_request_id += 1

  let%entry_point set_receiver self params =
    verify (sender = self.data.admin) "RequesterNotAdmin";
    self.data.receiver <- some params

  let%entry_point setup self params =
    verify (sender = self.data.admin) "RequesterNotAdmin";
    self.data.admin <- params.admin;
    self.data.escrow <- params.escrow;
    self.data.oracle <- params.oracle;
    self.data.job_id <- params.job_id;
    self.data.token <- params.token

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(admin = address, escrow = address, job_id = bytes, next_request_id = nat, oracle = address, receiver = address option, token = address).layout(("admin", ("escrow", ("job_id", ("next_request_id", ("oracle", ("receiver", "token")))))))
      ~storage:[%expr
                 {admin = sp.address('tz0FakeRequester2 admin'),
                  escrow = sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H'),
                  job_id = sp.bytes('0x0001'),
                  next_request_id = 1,
                  oracle = sp.address('KT1Tezooo3zzSmartPyzzSTATiCzzzseJjWC'),
                  receiver = sp.none,
                  token = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')}]
      [cancel_value; request_value; set_receiver; setup]
end