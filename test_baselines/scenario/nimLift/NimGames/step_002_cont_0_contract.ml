open Smartml

module Contract = struct

  let%entry_point build self params =
    rangeMap = sp.local("rangeMap", {});
    sp.for i in sp.range(1, params.size + 1):
      rangeMap.value[i - 1] <- i;
    self.data.games[self.data.nbGames] <- {bound = params.bound; claimed = False; deck = rangeMap.value; lastWins = False; nextPlayer = 1; size = params.size; winner = 0};
    self.data.nbGames += 1

  let%entry_point claim self params =
    verify ((sum self.data.games[params.gameId].deck.values()) = 0);
    self.data.games[params.gameId].claimed <- True;
    if self.data.games[params.gameId].lastWins then
      self.data.games[params.gameId].winner <- 3 - self.data.games[params.gameId].nextPlayer
    else
      self.data.games[params.gameId].winner <- self.data.games[params.gameId].nextPlayer

  let%entry_point remove self params =
    verify (params.cell >= 0);
    verify (params.cell < self.data.games[params.gameId].size);
    verify (params.k >= 1);
    verify (params.k <= self.data.games[params.gameId].bound);
    verify (params.k <= self.data.games[params.gameId].deck[params.cell]);
    self.data.games[params.gameId].deck[params.cell] -= params.k;
    self.data.games[params.gameId].nextPlayer <- 3 - self.data.games[params.gameId].nextPlayer

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(games = map(intOrNat, sp.TRecord(bound = int, claimed = bool, deck = map(int, int), lastWins = bool, nextPlayer = int, size = int, winner = int).layout(("bound", ("claimed", ("deck", ("lastWins", ("nextPlayer", ("size", "winner")))))))), nbGames = intOrNat).layout(("games", "nbGames"))
      ~storage:[%expr
                 {games = {},
                  nbGames = 0}]
      [build; claim; remove]
end