open Smartml

module Contract = struct

  let%entry_point f self params =
    self.data.a <- 2 * params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(a = intOrNat, b = bool).layout(("a", "b"))
      ~storage:[%expr
                 {a = 12,
                  b = True}]
      [f]
end