open Smartml

module Contract = struct

  let%entry_point ep self () =
    k = sp.local("k", "abc");
    with sp.match_record(self.data.m[k.value], "data") as data:
      k.value <- "xyz" + k.value;
    self.data.out <- k.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(m = map(string, sp.TRecord(a = intOrNat, b = intOrNat).layout(("a", "b"))), out = string).layout(("m", "out"))
      ~storage:[%expr
                 {m = {'abc' : {a = 10; b = 20}},
                  out = 'z'}]
      [ep]
end