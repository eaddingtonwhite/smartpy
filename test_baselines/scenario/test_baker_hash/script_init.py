import smartpy as sp

class Delegating(sp.Contract):
    @sp.entry_point
    def delegate(self, baker):
        sp.set_delegate(baker)

@sp.add_test(name = "Delegating Pre Florence")
def preFlorenceTest():
    voting_powers = {
        sp.key_hash("tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4"): 0,
    }
    preFlorenceScenario = sp.test_scenario()
    preFlorenceScenario.add_flag("protocol", "edo")
    preFlorenceScenario.h1("PreFlorence")
    pre = Delegating()
    preFlorenceScenario += pre
    pre.delegate(sp.some(sp.key_hash("tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4"))).run(voting_powers = voting_powers)

@sp.add_test(name = "Delegating Post Florence")
def postFlorenceTest():
    voting_powers = {
        sp.baker_hash("SG1jfZeHRzeWAM1T4zrwunEyUpwWc82D4tbv"): 0,
    }
    postFlorenceScenario = sp.test_scenario()
    postFlorenceScenario.add_flag("protocol", "baking_accounts")
    postFlorenceScenario.h1("Baking Accounts")
    post = Delegating()
    postFlorenceScenario += post
    post.delegate(sp.some(sp.baker_hash("SG1jfZeHRzeWAM1T4zrwunEyUpwWc82D4tbv"))).run(voting_powers = voting_powers)

sp.add_compilation_target("set_delegate_pre_florence", Delegating(), flags = [("protocol", "edo")])
sp.add_compilation_target("set_delegate_post_florence", Delegating(), flags = [("protocol", "baking_accounts")])
