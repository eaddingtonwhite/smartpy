open Smartml

module Contract = struct

  let%entry_point compute self params =
    set_type (params.params : nat);
    let%var __s5 = params.data.token_metadata[params.params] in
    self.data.result <- some __s5.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(result = sp.TRecord(token_id = nat, token_info = map(string, bytes)).layout(("token_id", "token_info")) option).layout("result")
      ~storage:[%expr
                 {result = sp.none}]
      [compute]
end