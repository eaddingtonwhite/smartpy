open Smartml

module Contract = struct

  let%entry_point target self params =
    self.data.last <- some params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(last = address option).layout("last")
      ~storage:[%expr
                 {last = sp.none}]
      [target]
end