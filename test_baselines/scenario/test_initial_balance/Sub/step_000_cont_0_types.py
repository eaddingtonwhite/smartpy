import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat).layout("x")
tparameter = sp.TUnit
tglobals = { "sub": sp.TLambda(sp.TRecord(in_storage = sp.TRecord(x = sp.TIntOrNat).layout("x")).layout("in_storage"), sp.TRecord(operations = sp.TList(sp.TOperation), result = sp.TUnit, storage = sp.TRecord(x = sp.TIntOrNat).layout("x")).layout(("operations", ("result", "storage")))) }
tviews = { }
