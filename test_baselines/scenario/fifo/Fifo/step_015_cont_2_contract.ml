open Smartml

module Contract = struct

  let%entry_point pop self () =
    verify (self.data.fif.first < self.data.fif.last);
    del self.data.fif.saved[self.data.fif.first];
    self.data.fif.first += 1

  let%entry_point popQ self () =
    verify (self.data.queue.first < self.data.queue.last);
    del self.data.queue.saved[self.data.queue.first];
    self.data.queue.first += 1

  let%entry_point push self params =
    self.data.fif.last += 1;
    self.data.fif.saved[self.data.fif.last] <- params;
    self.data.queue.last += 1;
    self.data.queue.saved[self.data.queue.last] <- params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(fif = sp.TRecord(first = int, last = int, saved = map(int, intOrNat)).layout(("first", ("last", "saved"))), queue = sp.TRecord(first = int, last = int, saved = map(int, intOrNat)).layout(("first", ("last", "saved")))).layout(("fif", "queue"))
      ~storage:[%expr
                 {fif = {first = 0; last = -1; saved = {}},
                  queue = {first = 0; last = -1; saved = {}}}]
      [pop; popQ; push]
end