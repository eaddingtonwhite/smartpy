open Smartml

module Contract = struct

  let%entry_point cancelProposal self params =
    verify (self.data.addrVoterId.contains(sender)) "MultisignAdmin_VoterUnknown";
    verify ((self.data.proposals.contains(self.data.addrVoterId[sender])) & (self.data.proposals[self.data.addrVoterId[sender]].id = params)) "MultisignAdmin_ProposalUnknown";
    self.data.proposals[self.data.addrVoterId[sender]].canceled <- True

  let%entry_point getLastProposal self params =
    sp.transfer(self.data.proposals[fst params], sp.tez(0), sp.contract(sp.TRecord(batchs = sp.TVariant(selfAdmin = sp.TVariant(changeQuorum = nat, changeTarget = address, changeTimeout = int, changeVoters = sp.TRecord(added = sp.TRecord(addr = address, publicKey = key).layout(("addr", "publicKey")) list, removed = sp.TSet(address)).layout(("added", "removed"))).layout((("changeQuorum", "changeTarget"), ("changeTimeout", "changeVoters"))) list, targetAdmin = sp.TVariant(setActive = bool, setAdmin = address).layout(("setActive", "setAdmin")) list).layout(("selfAdmin", "targetAdmin")) list, canceled = bool, id = nat, nay = sp.TSet(nat), startedAt = timestamp, yay = sp.TSet(nat)).layout(("batchs", ("canceled", ("id", ("nay", ("startedAt", "yay")))))), snd params).open_some(message = "MultisignAdmin_WrongCallbackInterface"))

  let%entry_point getParams self params =
    sp.transfer({quorum = self.data.quorum; target = self.data.target; timeout = self.data.timeout}, sp.tez(0), params)

  let%entry_point multiVote self params =
    set_type (params : sp.TRecord(initiatorId = nat, proposalId = nat, votes = sp.TRecord(signature = signature, voterId = nat, yay = bool).layout(("signature", ("voterId", "yay"))) list).layout(("initiatorId", ("proposalId", "votes"))) list);
    sp.for proposalVotes in params:
      sp.for vote in proposalVotes.votes:
        verify (self.data.voters.contains(vote.voterId)) "MultisignAdmin_VoterUnknown";
        verify sp.check_signature(self.data.voters[vote.voterId].publicKey, vote.signature, pack (self_address, (proposalVotes.initiatorId, proposalVotes.proposalId))) "MultisignAdmin_Badsig";
        y10 = sp.local("y10", self.registerVote({in_param = {initiatorId = proposalVotes.initiatorId; proposalId = proposalVotes.proposalId; voterId = vote.voterId; yay = vote.yay}; in_storage = self.data}));
        self.data <- y10.value.storage;
        sp.for op in y10.value.operations.rev():
          operations().push(op);
      if ((len self.data.proposals[proposalVotes.initiatorId].nay) + (len self.data.proposals[proposalVotes.initiatorId].yay)) >= self.data.quorum then
        if (len self.data.proposals[proposalVotes.initiatorId].nay) >= (len self.data.proposals[proposalVotes.initiatorId].yay) then
          self.data.proposals[proposalVotes.initiatorId].canceled <- True
        else
          y11 = sp.local("y11", self.onVoted({in_param = self.data.proposals[proposalVotes.initiatorId]; in_storage = self.data}));
          self.data <- y11.value.storage;
          sp.for op in y11.value.operations.rev():
            operations().push(op)

  let%entry_point newProposal self params =
    verify (self.data.addrVoterId.contains(sender)) "MultisignAdmin_VoterUnknown";
    self.data.voters[self.data.addrVoterId[sender]].lastProposalId += 1;
    self.data.proposals[self.data.addrVoterId[sender]] <- {batchs = params; canceled = False; id = self.data.voters[self.data.addrVoterId[sender]].lastProposalId; nay = sp.set([]); startedAt = now; yay = sp.set([self.data.addrVoterId[sender]])};
    if self.data.quorum < 2 then
      y12 = sp.local("y12", self.onVoted({in_param = self.data.proposals[self.data.addrVoterId[sender]]; in_storage = self.data}));
      self.data <- y12.value.storage;
      sp.for op in y12.value.operations.rev():
        operations().push(op)

  let%entry_point vote self params =
    verify (self.data.addrVoterId.contains(sender)) "MultisignAdmin_VoterUnknown";
    sp.for vote in params:
      y13 = sp.local("y13", self.registerVote({in_param = {initiatorId = vote.initiatorId; proposalId = vote.proposalId; voterId = self.data.addrVoterId[sender]; yay = vote.yay}; in_storage = self.data}));
      self.data <- y13.value.storage;
      sp.for op in y13.value.operations.rev():
        operations().push(op);
      if ((len self.data.proposals[vote.initiatorId].nay) + (len self.data.proposals[vote.initiatorId].yay)) >= self.data.quorum then
        if (len self.data.proposals[vote.initiatorId].nay) >= (len self.data.proposals[vote.initiatorId].yay) then
          self.data.proposals[vote.initiatorId].canceled <- True
        else
          y14 = sp.local("y14", self.onVoted({in_param = self.data.proposals[vote.initiatorId]; in_storage = self.data}));
          self.data <- y14.value.storage;
          sp.for op in y14.value.operations.rev():
            operations().push(op)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(addrVoterId = bigMap(address, nat), keyVoterId = bigMap(key, nat), lastVoteTimestamp = timestamp, lastVoterId = nat, metadata = bigMap(string, bytes), nbVoters = nat, proposals = bigMap(nat, sp.TRecord(batchs = sp.TVariant(selfAdmin = sp.TVariant(changeQuorum = nat, changeTarget = address, changeTimeout = int, changeVoters = sp.TRecord(added = sp.TRecord(addr = address, publicKey = key).layout(("addr", "publicKey")) list, removed = sp.TSet(address)).layout(("added", "removed"))).layout((("changeQuorum", "changeTarget"), ("changeTimeout", "changeVoters"))) list, targetAdmin = sp.TVariant(setActive = bool, setAdmin = address).layout(("setActive", "setAdmin")) list).layout(("selfAdmin", "targetAdmin")) list, canceled = bool, id = nat, nay = sp.TSet(nat), startedAt = timestamp, yay = sp.TSet(nat)).layout(("batchs", ("canceled", ("id", ("nay", ("startedAt", "yay"))))))), quorum = nat, target = address option, timeout = int, voters = bigMap(nat, sp.TRecord(addr = address, lastProposalId = nat, publicKey = key).layout(("addr", ("lastProposalId", "publicKey"))))).layout(("addrVoterId", ("keyVoterId", ("lastVoteTimestamp", ("lastVoterId", ("metadata", ("nbVoters", ("proposals", ("quorum", ("target", ("timeout", "voters")))))))))))
      ~storage:[%expr
                 {addrVoterId = {sp.address('tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT') : 0, sp.address('tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR') : 1},
                  keyVoterId = {sp.key('edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd') : 0, sp.key('edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe') : 1},
                  lastVoteTimestamp = sp.timestamp(0),
                  lastVoterId = 1,
                  metadata = {'' : sp.bytes('0x68747470733a2f2f636c6f7564666c6172652d697066732e636f6d2f697066732f516d596e55326a7771345855346a775831526d75314547666d79684d687044437050704846356a396a4639793177')},
                  nbVoters = 2,
                  proposals = {},
                  quorum = 1,
                  target = sp.none,
                  timeout = 5,
                  voters = {0 : {addr = sp.address('tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT'); lastProposalId = 0; publicKey = sp.key('edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd')}, 1 : {addr = sp.address('tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR'); lastProposalId = 0; publicKey = sp.key('edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe')}}}]
      [cancelProposal; getLastProposal; getParams; multiVote; newProposal; vote]
end