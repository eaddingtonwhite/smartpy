open Smartml

module Contract = struct

  let%entry_point myEntryPoint self params =
    verify (self.data.x <= 100);
    self.data.x += params;
    self.data.y <- 12345;
    self.data.y += params + 2;
    self.data.x += 10

  let%entry_point myEntryPoint2 self () =
    ()

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = intOrNat, y = intOrNat, z = intOrNat).layout(("x", ("y", "z")))
      ~storage:[%expr
                 {x = 1,
                  y = 2,
                  z = 42}]
      [myEntryPoint; myEntryPoint2]
end