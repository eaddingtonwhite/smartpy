import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(chainId = sp.TOption(sp.TChainId)).layout("chainId"))
    self.init(chainId = sp.none)

  @sp.entry_point
  def test(self):
    self.data.chainId = sp.some(sp.chain_id)