open Smartml

module Contract = struct

  let%entry_point test self params =
    self.data.a <- some {l = params.l; lr = params.l.rev(); mi = params.m.items(); mir = params.m.rev_items(); mk = params.m.keys(); mkr = params.m.rev_keys(); mv = params.m.values(); mvr = params.m.rev_values(); s = params.s.elements(); sr = params.s.rev_elements()};
    self.data.b <- sum params.l;
    self.data.c <- concat params.m.keys();
    self.data.d <- sum params.s.rev_elements();
    self.data.e <- "";
    sp.for x in params.m.values():
      if snd x then
        self.data.e += fst x;
    sp.for i in sp.range(0, 5):
      self.data.f.push(i * i);
    self.data.g <- sp.range(1, 12)

  let%entry_point test_match self params =
    with sp.match_cons(params) as match_cons_52:
      self.data.head <- match_cons_52.head;
      self.data.tail <- match_cons_52.tail
    else:
      self.data.head <- "abc"

  let%entry_point test_match2 self params =
    with sp.match_cons(params) as match_cons_60:
      with sp.match_cons(match_cons_60.tail) as match_cons_61:
        self.data.head <- match_cons_60.head + match_cons_61.head;
        self.data.tail <- match_cons_61.tail
      else:
        ()
    else:
      self.data.head <- "abc"

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(a = sp.TRecord(l = intOrNat list, lr = intOrNat list, mi = sp.TRecord(key = string, value = (string * bool)).layout(("key", "value")) list, mir = sp.TRecord(key = string, value = (string * bool)).layout(("key", "value")) list, mk = string list, mkr = string list, mv = (string * bool) list, mvr = (string * bool) list, s = intOrNat list, sr = intOrNat list).layout(("l", ("lr", ("mi", ("mir", ("mk", ("mkr", ("mv", ("mvr", ("s", "sr")))))))))) option, b = intOrNat, c = string, d = intOrNat, e = string, f = intOrNat list, g = intOrNat list, head = string, tail = string list).layout(("a", ("b", ("c", ("d", ("e", ("f", ("g", ("head", "tail")))))))))
      ~storage:[%expr
                 {a = sp.none,
                  b = 0,
                  c = '',
                  d = 0,
                  e = '',
                  f = sp.list([]),
                  g = sp.list([]),
                  head = 'no head',
                  tail = sp.list(['no tail'])}]
      [test; test_match; test_match2]
end