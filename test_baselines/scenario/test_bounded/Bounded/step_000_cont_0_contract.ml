open Smartml

module Contract = struct

  let%entry_point ep self () =
    self.data.x <- "abc"

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = sp.TBounded(['abc', 'def'], t=string)).layout("x")
      ~storage:[%expr
                 {x = 'abc'}]
      [ep]
end