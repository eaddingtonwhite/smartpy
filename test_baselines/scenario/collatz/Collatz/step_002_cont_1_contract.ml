open Smartml

module Contract = struct

  let%entry_point run self params =
    set_type (params.x : nat);
    sp.transfer((3 * params.x) + 1, sp.tez(0), params.k)

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [run]
end