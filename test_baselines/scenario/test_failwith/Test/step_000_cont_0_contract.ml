open Smartml

module Contract = struct

  let%entry_point ep self () =
    def f0(lparams_0):
      if lparams_0 = 0 then
        failwith "zero"
      else
        1
    self.data.f <- some sp.build_lambda(f0)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(f = sp.TLambda(intOrNat, intOrNat) option).layout("f")
      ~storage:[%expr
                 {f = sp.none}]
      [ep]
end