open Smartml

module Contract = struct

  let%entry_point welcome self params =
    verify (self.data.Left <= 123);
    self.data.Left += params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(Left = intOrNat, Right = intOrNat).layout(("Left", "Right"))
      ~storage:[%expr
                 {Left = 1,
                  Right = 12}]
      [welcome]
end