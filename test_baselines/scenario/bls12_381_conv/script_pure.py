import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self, **kargs):
        self.init(**kargs)

    @sp.entry_point
    def entry_point_1(self, params):
        self.data.fr = sp.some(sp.mul(sp.fst(sp.ediv(params, sp.mutez(1)).open_some()), sp.bls12_381_fr("0x01")))

@sp.add_test(name = "Minimal")
def test():
    scenario = sp.test_scenario()
    scenario.h1("Minimal")
    c1 = MyContract(fr=sp.none)
    scenario += c1
