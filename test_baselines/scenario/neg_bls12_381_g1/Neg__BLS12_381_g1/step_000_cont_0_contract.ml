open Smartml

module Contract = struct

  let%entry_point negate self params =
    set_type (params.g1 : bls12_381_g1);
    self.data.g1 <- some (- params.g1)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(g1 = bls12_381_g1 option).layout("g1")
      ~storage:[%expr
                 {g1 = sp.none}]
      [negate]
end