open Smartml

module Contract = struct

  let%entry_point swap self () =
    x = sp.local("x", self.data.a.x1);
    self.data.a.x1 <- self.data.a.x2;
    self.data.a.x2 <- x.value;
    self.data.b.x1 <- self.data.b.x2;
    self.data.b.x2 *= 2

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(a = sp.TRecord(x1 = int, x2 = int).layout(("x1", "x2")), b = sp.TRecord(x1 = int, x2 = int).layout(("x1", "x2"))).layout(("a", "b"))
      ~storage:[%expr
                 {a = {x1 = -1; x2 = -2},
                  b = {x1 = -3; x2 = -4}}]
      [swap]
end