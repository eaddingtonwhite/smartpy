import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TOption(sp.TIntOrNat)).layout("x"))
    self.init(x = sp.none)

  @sp.entry_point
  def push(self, params):
    self.data.x = sp.some(params)