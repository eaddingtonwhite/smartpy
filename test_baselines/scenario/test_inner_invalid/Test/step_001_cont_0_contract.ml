open Smartml

module Contract = struct

  let%entry_point compute self params =
    sp.transfer(params, sp.tez(0), sp.self_entry_point('run'))

  let%entry_point reset self () =
    self.data.counter <- 0

  let%entry_point run self params =
    verify ((self.data.counter * params) <> 9);
    if params > 1 then
      self.data.counter += 1;
      sp.transfer(params - 1, sp.tez(0), sp.self_entry_point('run'))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(counter = int).layout("counter")
      ~storage:[%expr
                 {counter = 0}]
      [compute; reset; run]
end