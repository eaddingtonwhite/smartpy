open Smartml

module Contract = struct

  let%entry_point myEntryPoint self params =
    verify (self.data.myParameter1 <= 123);
    self.data.myParameter1 += params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(myParameter1 = intOrNat, myParameter2 = intOrNat).layout(("myParameter1", "myParameter2"))
      ~storage:[%expr
                 {myParameter1 = 1,
                  myParameter2 = 151}]
      [myEntryPoint]
end