open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    with sp.match_record(self.data, "data") as data:
      match_pair_test_ticket_71_fst, match_pair_test_ticket_71_snd = sp.match_tuple(sp.get_and_update(data.m, 42, none), "match_pair_test_ticket_71_fst", "match_pair_test_ticket_71_snd")
      data.m <- match_pair_test_ticket_71_snd;
      data.x <- 0

  let%entry_point ep2 self params =
    with sp.match_record(self.data, "data") as data:
      ticket_78 = sp.local("ticket_78", ticket "a" 1);
      ticket_79 = sp.local("ticket_79", ticket "b" 2);
      sp.transfer((ticket_78.value, ticket_79.value), sp.tez(0), params)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(m = map(int, int ticket), x = int).layout(("m", "x"))
      [ep1; ep2]
end