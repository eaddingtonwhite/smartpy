open Smartml

module Contract = struct

  let%entry_point run self () =
    with sp.match_record(self.data, "data") as data:
      ticket_test_ticket_49_data, ticket_test_ticket_49_copy = sp.match_tuple(read_ticket_raw data.t, "ticket_test_ticket_49_data", "ticket_test_ticket_49_copy")
      ticket_test_ticket_49_ticketer, ticket_test_ticket_49_content, ticket_test_ticket_49_amount = sp.match_tuple(ticket_test_ticket_49_data, "ticket_test_ticket_49_ticketer", "ticket_test_ticket_49_content", "ticket_test_ticket_49_amount")
      verify (ticket_test_ticket_49_content = 42);
      ticket1_test_ticket_51, ticket2_test_ticket_51 = sp.match_tuple(split_ticket_raw ticket_test_ticket_49_copy (ticket_test_ticket_49_amount // 2, ticket_test_ticket_49_amount // 2).open_some(), "ticket1_test_ticket_51", "ticket2_test_ticket_51")
      data.t <- join_tickets_raw (ticket2_test_ticket_51, ticket1_test_ticket_51).open_some()

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(t = nat ticket, x = int).layout(("t", "x"))
      [run]
end