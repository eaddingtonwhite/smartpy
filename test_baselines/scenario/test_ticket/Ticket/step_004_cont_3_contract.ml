open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    with sp.match_record(self.data, "data") as data:
      match_pair_test_ticket_61_fst, match_pair_test_ticket_61_snd = sp.match_tuple(sp.get_and_update(data.m, 42, none), "match_pair_test_ticket_61_fst", "match_pair_test_ticket_61_snd")
      data.m <- match_pair_test_ticket_61_snd

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(m = map(int, int ticket)).layout("m")
      [ep1]
end