open Smartml

module Contract = struct

  let%entry_point a self params =
    self.data <- params

  let%entry_point b self params =
    self.data.address <- params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(address = int option).layout("address")
      [a; b]
end