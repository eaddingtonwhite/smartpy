open Smartml

module Contract = struct

  let%entry_point test self params =
    verify ((fst ediv amount sp.mutez(1).open_some(message = ())) = params);
    verify ((mul params sp.mutez(1)) = amount)

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [test]
end