open Smartml

module Contract = struct

  let%entry_point pack_lambda_record self () =
    verify (unpack (pack sp.build_lambda(lambda lparams_21: {a = 1; b = lparams_21 + 2})) sp.TLambda(int, sp.TRecord(a = int, b = int).layout(("a", "b"))).open_some()(100).a = 1);
    verify (unpack (pack sp.build_lambda(lambda lparams_22: {a = lparams_22 + 1})) sp.TLambda(int, sp.TRecord(a = int).layout("a")).open_some()(100).a = 101)

  let%entry_point pack_lambda_variant self () =
    verify (unpack (pack sp.build_lambda(lambda lparams_23: (variant('a', lparams_23 + 1) : sp.TVariant(a = int, b = int).layout(("a", "b"))))) sp.TLambda(int, sp.TVariant(a = int, b = int).layout(("a", "b"))).open_some()(100).open_variant('a') = 101);
    verify (unpack (pack sp.build_lambda(lambda lparams_24: variant('a', lparams_24 + 1))) sp.TLambda(int, sp.TVariant(a = int).layout("a")).open_some()(100).open_variant('a') = 101)

  let%entry_point pack_lambdas self () =
    def f25(lparams_25):
      y = sp.local("y", 0);
      r = sp.local("r", "A");
      if lparams_25 = 0 then
        if lparams_25 = y.value then
          r.value <- "B";
      r.value
    f1 = sp.local("f1", sp.build_lambda(f25), sp.TLambda(int, string));
    p1 = sp.local("p1", pack f1.value);
    f2 = sp.local("f2", unpack p1.value sp.TLambda(int, string).open_some(), sp.TLambda(int, string));
    p2 = sp.local("p2", pack f2.value);
    verify (p1.value = p2.value);
    verify (f1.value(0) = f2.value(0));
    verify (f1.value(1) = f2.value(1));
    verify (f1.value(2) = f2.value(2))

  let%entry_point pack_lambdas2 self () =
    f1 = sp.local("f1", sp.build_lambda(lambda lparams_26: lparams_26 > 0), sp.TLambda(int, bool));
    p1 = sp.local("p1", pack f1.value);
    f2 = sp.local("f2", unpack p1.value sp.TLambda(int, bool).open_some(), sp.TLambda(int, bool));
    verify f2.value(1)

  let%entry_point run self () =
    verify ((pack baker_hash "SG1jfZeHRzeWAM1T4zrwunEyUpwWc82D4tbv") = bytes "0x050a00000014dd577f8a5cd09a4a1e62241676ffaf4bc0c5b87f")

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [pack_lambda_record; pack_lambda_variant; pack_lambdas; pack_lambdas2; run]
end