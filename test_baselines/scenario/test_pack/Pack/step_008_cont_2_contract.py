import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def pack_lambda_record(self):
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda lparams_21: sp.record(a = 1, b = lparams_21 + 2))), sp.TLambda(sp.TInt, sp.TRecord(a = sp.TInt, b = sp.TInt).layout(("a", "b")))).open_some()(100).a == 1)
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda lparams_22: sp.record(a = lparams_22 + 1))), sp.TLambda(sp.TInt, sp.TRecord(a = sp.TInt).layout("a"))).open_some()(100).a == 101)

  @sp.entry_point
  def pack_lambda_variant(self):
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda lparams_23: sp.set_type_expr(variant('a', lparams_23 + 1), sp.TVariant(a = sp.TInt, b = sp.TInt).layout(("a", "b"))))), sp.TLambda(sp.TInt, sp.TVariant(a = sp.TInt, b = sp.TInt).layout(("a", "b")))).open_some()(100).open_variant('a') == 101)
    sp.verify(sp.unpack(sp.pack(sp.build_lambda(lambda lparams_24: variant('a', lparams_24 + 1))), sp.TLambda(sp.TInt, sp.TVariant(a = sp.TInt).layout("a"))).open_some()(100).open_variant('a') == 101)

  @sp.entry_point
  def pack_lambdas(self):
    def f25(lparams_25):
      y = sp.local("y", 0)
      r = sp.local("r", 'A')
      sp.if lparams_25 == 0:
        sp.if lparams_25 == y.value:
          r.value = 'B'
      sp.result(r.value)
    f1 = sp.local("f1", sp.build_lambda(f25), sp.TLambda(sp.TInt, sp.TString))
    p1 = sp.local("p1", sp.pack(f1.value))
    f2 = sp.local("f2", sp.unpack(p1.value, sp.TLambda(sp.TInt, sp.TString)).open_some(), sp.TLambda(sp.TInt, sp.TString))
    p2 = sp.local("p2", sp.pack(f2.value))
    sp.verify(p1.value == p2.value)
    sp.verify(f1.value(0) == f2.value(0))
    sp.verify(f1.value(1) == f2.value(1))
    sp.verify(f1.value(2) == f2.value(2))

  @sp.entry_point
  def pack_lambdas2(self):
    f1 = sp.local("f1", sp.build_lambda(lambda lparams_26: lparams_26 > 0), sp.TLambda(sp.TInt, sp.TBool))
    p1 = sp.local("p1", sp.pack(f1.value))
    f2 = sp.local("f2", sp.unpack(p1.value, sp.TLambda(sp.TInt, sp.TBool)).open_some(), sp.TLambda(sp.TInt, sp.TBool))
    sp.verify(f2.value(1))

  @sp.entry_point
  def run(self):
    sp.verify(sp.pack(sp.baker_hash('SG1jfZeHRzeWAM1T4zrwunEyUpwWc82D4tbv')) == sp.bytes('0x050a00000014dd577f8a5cd09a4a1e62241676ffaf4bc0c5b87f'))