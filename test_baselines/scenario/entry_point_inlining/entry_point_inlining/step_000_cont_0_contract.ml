open Smartml

module Contract = struct

  let%entry_point entry_point_1 self () =
    self.data.x += 12;
    self.data.x += 15

  let%entry_point f self params =
    self.data.x += params

  let%entry_point g self params =
    self.data.x += params

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x = intOrNat).layout("x")
      ~storage:[%expr
                 {x = 11}]
      [entry_point_1; f; g]
end