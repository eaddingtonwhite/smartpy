open Smartml

module Contract = struct

  let%entry_point myEntryPoint self params =
    self.data.a += params.x;
    self.data.b += params.y

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(a = int, b = nat).layout(("a", "b"))
      ~storage:[%expr
                 {a = 12,
                  b = 16}]
      [myEntryPoint]
end