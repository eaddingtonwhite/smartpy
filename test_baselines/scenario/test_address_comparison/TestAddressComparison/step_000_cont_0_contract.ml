open Smartml

module Contract = struct

  let%entry_point test self params =
    verify ((sp.slice(pack (fst params : address), 6, 22)) = (sp.slice(pack (snd params : address), 6, 22)))

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [test]
end