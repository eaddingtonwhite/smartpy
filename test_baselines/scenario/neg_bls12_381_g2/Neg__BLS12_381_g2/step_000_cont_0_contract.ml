open Smartml

module Contract = struct

  let%entry_point negate self params =
    set_type (params.g2 : bls12_381_g2);
    self.data.g2 <- some (- params.g2)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(g2 = bls12_381_g2 option).layout("g2")
      ~storage:[%expr
                 {g2 = sp.none}]
      [negate]
end