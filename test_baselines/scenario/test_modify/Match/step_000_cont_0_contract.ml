open Smartml

module Contract = struct

  let%entry_point ep1 self () =
    set_type (self.data.x2 : sp.TRecord(a = nat, b = int, c = bool, d = string).layout(("a", ("b", ("c", "d")))));
    set_type (self.data.x3 : sp.TRecord(a = nat, b = int, c = bool, d = string).layout(((("a", "b"), "c"), "d")));
    set_type (self.data.x4 : sp.TRecord(a = nat, b = int, c = bool, d = string).layout((("a", "b"), ("c", "d"))));
    with sp.match_record(self.data.x1, "modify_record_test_modify_20") as modify_record_test_modify_20:
      verify ((abs modify_record_test_modify_20.b) = (modify_record_test_modify_20.a + 1));
    with sp.match_record(self.data.x2, "modify_record_test_modify_22") as modify_record_test_modify_22:
      verify ((abs modify_record_test_modify_22.b) = (modify_record_test_modify_22.a + 1));
      modify_record_test_modify_22.d <- "xyz";
    with sp.match_record(self.data.x3, "modify_record_test_modify_25") as modify_record_test_modify_25:
      verify ((abs modify_record_test_modify_25.b) = (modify_record_test_modify_25.a + 1));
      modify_record_test_modify_25.d <- "xyz";
    with sp.match_record(self.data.x4, "modify_record_test_modify_28") as modify_record_test_modify_28:
      verify ((abs modify_record_test_modify_28.b) = (modify_record_test_modify_28.a + 1));
      modify_record_test_modify_28.d <- "xyz"

  let%entry_point ep2 self () =
    with sp.match_record(self.data.x1, "modify_record_test_modify_34") as modify_record_test_modify_34:
      verify ((abs modify_record_test_modify_34.b) = (modify_record_test_modify_34.a + 1));
      modify_record_test_modify_34.d <- "xyz"

  let%entry_point ep3 self () =
    with sp.match_tuple(self.data.y, "a", "b", "c", "d") as a, b, c, d:
      verify ((abs b.value) = (a.value + 1));
      d.value <- "xyz";
      (a.value, b.value, c.value, d.value)

  let%entry_point ep4 self () =
    with sp.match_record(self.data.x1, "modify_record_test_modify_48") as modify_record_test_modify_48:
      ()

  let%entry_point ep5 self params =
    with sp.match_record(self.data.x1, "modify_record_test_modify_60") as modify_record_test_modify_60:
      sp.send(params, sp.tez(0));
      modify_record_test_modify_60.d <- "xyz";
    self.data.x1.a += 5

  let%entry_point ep6 self () =
    with sp.match_record(self.data.z, "modify_record_test_modify_67") as modify_record_test_modify_67:
      with sp.match_record(modify_record_test_modify_67.d, "modify_record_test_modify_68") as modify_record_test_modify_68:
        modify_record_test_modify_67.b <- 100;
        modify_record_test_modify_68.e <- 2;
        modify_record_test_modify_68.f <- "y"

  let%entry_point ep7 self () =
    verify (self.data.z.d.e = 2);
    with sp.match_record(self.data.z.d, "modify_record_test_modify_76") as modify_record_test_modify_76:
      verify (modify_record_test_modify_76.e = 2);
      modify_record_test_modify_76.e <- 3;
      modify_record_test_modify_76.f <- "z";
      verify (modify_record_test_modify_76.e = 3);
      modify_record_test_modify_76.e <- 4;
      verify (modify_record_test_modify_76.e = 4);
    verify (self.data.z.d.e = 4)

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(x1 = sp.TRecord(a = nat, b = int, c = bool, d = string).layout(("a", ("b", ("c", "d")))), x2 = sp.TRecord(a = nat, b = int, c = bool, d = string).layout(("a", ("b", ("c", "d")))), x3 = sp.TRecord(a = nat, b = int, c = bool, d = string).layout(((("a", "b"), "c"), "d")), x4 = sp.TRecord(a = nat, b = int, c = bool, d = string).layout((("a", "b"), ("c", "d"))), y = sp.TTuple(nat, int, bool, string), z = sp.TRecord(a = nat, b = int, c = bool, d = sp.TRecord(e = intOrNat, f = string).layout(("e", "f"))).layout(("a", ("b", ("c", "d"))))).layout(("x1", ("x2", ("x3", ("x4", ("y", "z"))))))
      ~storage:[%expr
                 {x1 = {a = 0; b = 1; c = True; d = 'abc'},
                  x2 = {a = 0; b = 1; c = True; d = 'abc'},
                  x3 = {a = 0; b = 1; c = True; d = 'abc'},
                  x4 = {a = 0; b = 1; c = True; d = 'abc'},
                  y = (0, 1, True, 'abc'),
                  z = {a = 0; b = 1; c = True; d = {e = 1; f = 'x'}}}]
      [ep1; ep2; ep3; ep4; ep5; ep6; ep7]
end