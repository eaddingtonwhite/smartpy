import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x1 = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(("a", ("b", ("c", "d")))), x2 = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(("a", ("b", ("c", "d")))), x3 = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(((("a", "b"), "c"), "d")), x4 = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout((("a", "b"), ("c", "d"))), y = sp.TTuple(sp.TNat, sp.TInt, sp.TBool, sp.TString), z = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TRecord(e = sp.TIntOrNat, f = sp.TString).layout(("e", "f"))).layout(("a", ("b", ("c", "d"))))).layout(("x1", ("x2", ("x3", ("x4", ("y", "z")))))))
    self.init(x1 = sp.record(a = 0, b = 1, c = True, d = 'abc'),
              x2 = sp.record(a = 0, b = 1, c = True, d = 'abc'),
              x3 = sp.record(a = 0, b = 1, c = True, d = 'abc'),
              x4 = sp.record(a = 0, b = 1, c = True, d = 'abc'),
              y = (0, 1, True, 'abc'),
              z = sp.record(a = 0, b = 1, c = True, d = sp.record(e = 1, f = 'x')))

  @sp.entry_point
  def ep1(self):
    sp.set_type(self.data.x2, sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(("a", ("b", ("c", "d")))))
    sp.set_type(self.data.x3, sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(((("a", "b"), "c"), "d")))
    sp.set_type(self.data.x4, sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout((("a", "b"), ("c", "d"))))
    with sp.match_record(self.data.x1, "modify_record_test_modify_20") as modify_record_test_modify_20:
      sp.verify((abs(modify_record_test_modify_20.b)) == (modify_record_test_modify_20.a + 1))
    with sp.match_record(self.data.x2, "modify_record_test_modify_22") as modify_record_test_modify_22:
      sp.verify((abs(modify_record_test_modify_22.b)) == (modify_record_test_modify_22.a + 1))
      modify_record_test_modify_22.d = 'xyz'
    with sp.match_record(self.data.x3, "modify_record_test_modify_25") as modify_record_test_modify_25:
      sp.verify((abs(modify_record_test_modify_25.b)) == (modify_record_test_modify_25.a + 1))
      modify_record_test_modify_25.d = 'xyz'
    with sp.match_record(self.data.x4, "modify_record_test_modify_28") as modify_record_test_modify_28:
      sp.verify((abs(modify_record_test_modify_28.b)) == (modify_record_test_modify_28.a + 1))
      modify_record_test_modify_28.d = 'xyz'

  @sp.entry_point
  def ep2(self):
    with sp.match_record(self.data.x1, "modify_record_test_modify_34") as modify_record_test_modify_34:
      sp.verify((abs(modify_record_test_modify_34.b)) == (modify_record_test_modify_34.a + 1))
      modify_record_test_modify_34.d = 'xyz'

  @sp.entry_point
  def ep3(self):
    with sp.match_tuple(self.data.y, "a", "b", "c", "d") as a, b, c, d:
      sp.verify((abs(b.value)) == (a.value + 1))
      d.value = 'xyz'
      sp.result((a.value, b.value, c.value, d.value))

  @sp.entry_point
  def ep4(self):
    with sp.match_record(self.data.x1, "modify_record_test_modify_48") as modify_record_test_modify_48:
      pass

  @sp.entry_point
  def ep5(self, params):
    with sp.match_record(self.data.x1, "modify_record_test_modify_60") as modify_record_test_modify_60:
      sp.send(params, sp.tez(0))
      modify_record_test_modify_60.d = 'xyz'
    self.data.x1.a += 5

  @sp.entry_point
  def ep6(self):
    with sp.match_record(self.data.z, "modify_record_test_modify_67") as modify_record_test_modify_67:
      with sp.match_record(modify_record_test_modify_67.d, "modify_record_test_modify_68") as modify_record_test_modify_68:
        modify_record_test_modify_67.b = 100
        modify_record_test_modify_68.e = 2
        modify_record_test_modify_68.f = 'y'

  @sp.entry_point
  def ep7(self):
    sp.verify(self.data.z.d.e == 2)
    with sp.match_record(self.data.z.d, "modify_record_test_modify_76") as modify_record_test_modify_76:
      sp.verify(modify_record_test_modify_76.e == 2)
      modify_record_test_modify_76.e = 3
      modify_record_test_modify_76.f = 'z'
      sp.verify(modify_record_test_modify_76.e == 3)
      modify_record_test_modify_76.e = 4
      sp.verify(modify_record_test_modify_76.e == 4)
    sp.verify(self.data.z.d.e == 4)