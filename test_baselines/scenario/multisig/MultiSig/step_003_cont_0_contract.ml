open Smartml

module Contract = struct

  let%entry_point build self params =
    self.data.multisigs[self.data.nbMultisigs] <- params.contract;
    self.data.nbMultisigs += 1

  let%entry_point sign self params =
    verify (params.id = sender);
    set_type (params.contractName : string);
    verify (params.contractName = self.data.multisigs[params.contractId].name);
    set_type (self.data.multisigs[params.contractId].weight : int);
    set_type (self.data.multisigs[params.contractId].groupsOK : int);
    sp.for group in self.data.multisigs[params.contractId].groups:
      sp.for participant in group.participants:
        if participant.id = params.id then
          verify (not participant.hasVoted);
          participant.hasVoted <- True;
          set_type (group.weight : int);
          group.weight += participant.weight;
          group.voters += 1;
          if ((not group.ok) & (group.thresholdVoters <= group.voters)) & (group.thresholdWeight <= group.weight) then
            group.ok <- True;
            self.data.multisigs[params.contractId].weight += group.contractWeight;
            self.data.multisigs[params.contractId].groupsOK += 1;
            if ((not self.data.multisigs[params.contractId].ok) & (self.data.multisigs[params.contractId].thresholdGroupsOK <= self.data.multisigs[params.contractId].groupsOK)) & (self.data.multisigs[params.contractId].thresholdWeight <= self.data.multisigs[params.contractId].weight) then
              self.data.multisigs[params.contractId].ok <- True

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(multisigs = map(intOrNat, sp.TRecord(groups = sp.TRecord(contractWeight = int, ok = bool, participants = sp.TRecord(hasVoted = bool, id = address, weight = int).layout(("hasVoted", ("id", "weight"))) list, thresholdVoters = intOrNat, thresholdWeight = int, voters = intOrNat, weight = int).layout(("contractWeight", ("ok", ("participants", ("thresholdVoters", ("thresholdWeight", ("voters", "weight"))))))) list, groupsOK = int, name = string, ok = bool, thresholdGroupsOK = int, thresholdWeight = int, weight = int).layout(("groups", ("groupsOK", ("name", ("ok", ("thresholdGroupsOK", ("thresholdWeight", "weight")))))))), nbMultisigs = intOrNat).layout(("multisigs", "nbMultisigs"))
      ~storage:[%expr
                 {multisigs = {},
                  nbMultisigs = 0}]
      [build; sign]
end