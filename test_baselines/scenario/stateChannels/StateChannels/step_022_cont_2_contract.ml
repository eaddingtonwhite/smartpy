open Smartml

module Contract = struct

  let%entry_point channelAccuseDoubleMove self params =
    verify self.data.active;
    self.data.active <- False;
    verify (params.msg1.seq = params.msg2.seq);
    set_type (params.msg1.seq : int);
    verify ((pack params.msg1) <> (pack params.msg2));
    if params.party = 1 then
      verify sp.check_signature(self.data.party1.pk, params.sig1, pack {id = self.data.id; name = "state"; seq = params.msg1.seq; state = params.msg1.state});
      verify sp.check_signature(self.data.party1.pk, params.sig2, pack {id = self.data.id; name = "state"; seq = params.msg2.seq; state = params.msg2.state});
      sp.send(self.data.party2.address, self.data.party1.bond + self.data.party2.bond)
    else
      verify sp.check_signature(self.data.party2.pk, params.sig1, pack {id = self.data.id; name = "state"; seq = params.msg1.seq; state = params.msg1.state});
      verify sp.check_signature(self.data.party2.pk, params.sig2, pack {id = self.data.id; name = "state"; seq = params.msg2.seq; state = params.msg2.state});
      sp.send(self.data.party1.address, self.data.party1.bond + self.data.party2.bond);
    set_type (self.data.baseState : sp.TRecord(claimed = bool, deck = map(intOrNat, int), nextPlayer = int, size = intOrNat, winner = int).layout(("claimed", ("deck", ("nextPlayer", ("size", "winner"))))));
    set_type (params.msg1.state : sp.TRecord(claimed = bool, deck = map(intOrNat, int), nextPlayer = int, size = intOrNat, winner = int).layout(("claimed", ("deck", ("nextPlayer", ("size", "winner"))))));
    set_type (params.msg2.state : sp.TRecord(claimed = bool, deck = map(intOrNat, int), nextPlayer = int, size = intOrNat, winner = int).layout(("claimed", ("deck", ("nextPlayer", ("size", "winner"))))))

  let%entry_point channelNewState self params =
    verify self.data.active;
    verify (self.data.seq < params.msg.seq);
    self.data.seq <- params.msg.seq;
    verify sp.check_signature(self.data.party1.pk, params.sig1, pack {id = self.data.id; name = "state"; seq = params.msg.seq; state = params.msg.state});
    verify sp.check_signature(self.data.party2.pk, params.sig2, pack {id = self.data.id; name = "state"; seq = params.msg.seq; state = params.msg.state});
    self.data.baseState <- params.msg.state

  let%entry_point channelRenounce self params =
    verify self.data.active;
    self.data.active <- False;
    if params.party = 1 then
      sp.send(self.data.party2.address, (self.data.party1.bond + self.data.party2.bond) - self.data.party1.looserClaim);
      sp.send(self.data.party1.address, self.data.party1.looserClaim)
    else
      verify sp.check_signature(self.data.party2.pk, params.sig.open_some(), pack {id = self.data.id; name = "renounce"});
      sp.send(self.data.party1.address, (self.data.party1.bond + self.data.party2.bond) - self.data.party2.looserClaim);
      sp.send(self.data.party2.address, self.data.party2.looserClaim)

  let%entry_point channelSetBond self params =
    verify self.data.active;
    if params.party = 1 then
      verify (not self.data.party1.hasBond);
      verify (self.data.party1.bond = amount);
      self.data.party1.hasBond <- True
    else
      verify (not self.data.party2.hasBond);
      verify (self.data.party2.bond = amount);
      self.data.party2.hasBond <- True

  let%entry_point claim self params =
    verify self.data.active;
    self.data.seq += 1;
    verify ((sum self.data.baseState.deck.values()) = 0);
    verify (not self.data.baseState.claimed);
    self.data.baseState.claimed <- True;
    self.data.baseState.winner <- self.data.baseState.nextPlayer;
    if self.data.baseState.winner <> 0 then
      if self.data.baseState.winner = 1 then
        self.data.active <- False;
        sp.send(self.data.party1.address, (self.data.party1.bond + self.data.party2.bond) - self.data.party2.looserClaim);
        sp.send(self.data.party2.address, self.data.party2.looserClaim)
      else
        self.data.active <- False;
        sp.send(self.data.party2.address, (self.data.party1.bond + self.data.party2.bond) - self.data.party1.looserClaim);
        sp.send(self.data.party1.address, self.data.party1.looserClaim);
    verify (params.sub.winner = self.data.baseState.winner);
    if params.party = 1 then
      ()
    else
      verify sp.check_signature(self.data.party2.pk, params.sig.open_some(), pack {id = self.data.id; name = "state"; seq = self.data.seq; state = self.data.baseState});
    self.data.nextParty <- 3 - self.data.nextParty

  let%entry_point remove self params =
    verify self.data.active;
    verify (self.data.nextParty = params.party);
    self.data.seq += 1;
    verify (params.sub.cell >= 0);
    verify (params.sub.cell < self.data.baseState.size);
    verify (params.sub.k >= 1);
    verify (params.sub.k <= 2);
    verify (params.sub.k <= self.data.baseState.deck[params.sub.cell]);
    self.data.baseState.deck[params.sub.cell] -= params.sub.k;
    self.data.baseState.nextPlayer <- 3 - self.data.baseState.nextPlayer;
    if params.party = 1 then
      ()
    else
      verify sp.check_signature(self.data.party2.pk, params.sig.open_some(), pack {id = self.data.id; name = "state"; seq = self.data.seq; state = self.data.baseState});
    self.data.nextParty <- 3 - self.data.nextParty

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(active = bool, baseState = sp.TRecord(claimed = bool, deck = map(intOrNat, int), nextPlayer = int, size = intOrNat, winner = int).layout(("claimed", ("deck", ("nextPlayer", ("size", "winner"))))), id = string, nextParty = int, party1 = sp.TRecord(address = address, bond = mutez, hasBond = bool, looserClaim = mutez, pk = key).layout(("address", ("bond", ("hasBond", ("looserClaim", "pk"))))), party2 = sp.TRecord(address = address, bond = mutez, hasBond = bool, looserClaim = mutez, pk = key).layout(("address", ("bond", ("hasBond", ("looserClaim", "pk"))))), seq = intOrNat).layout(("active", ("baseState", ("id", ("nextParty", ("party1", ("party2", "seq")))))))
      ~storage:[%expr
                 {active = True,
                  baseState = {claimed = False; deck = {0 : 1, 1 : 2, 2 : 3, 3 : 4, 4 : 5}; nextPlayer = 1; size = 5; winner = 0},
                  id = '1234',
                  nextParty = 1,
                  party1 = {address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'); bond = sp.tez(12); hasBond = False; looserClaim = sp.tez(2); pk = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG')},
                  party2 = {address = sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'); bond = sp.tez(15); hasBond = False; looserClaim = sp.tez(3); pk = sp.key('edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT')},
                  seq = 0}]
      [channelAccuseDoubleMove; channelNewState; channelRenounce; channelSetBond; claim; remove]
end