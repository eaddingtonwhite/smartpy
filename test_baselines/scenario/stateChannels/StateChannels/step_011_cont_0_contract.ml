open Smartml

module Contract = struct

  let%entry_point claim self params =
    verify ((sum self.data.deck.values()) = 0);
    verify (not self.data.claimed);
    self.data.claimed <- True;
    self.data.winner <- self.data.nextPlayer;
    verify (params.winner = self.data.winner)

  let%entry_point remove self params =
    verify (params.cell >= 0);
    verify (params.cell < self.data.size);
    verify (params.k >= 1);
    verify (params.k <= 2);
    verify (params.k <= self.data.deck[params.cell]);
    self.data.deck[params.cell] -= params.k;
    self.data.nextPlayer <- 3 - self.data.nextPlayer

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(claimed = bool, deck = map(intOrNat, int), nextPlayer = int, size = intOrNat, winner = int).layout(("claimed", ("deck", ("nextPlayer", ("size", "winner")))))
      ~storage:[%expr
                 {claimed = False,
                  deck = {0 : 1, 1 : 2, 2 : 3, 3 : 4, 4 : 5},
                  nextPlayer = 1,
                  size = 5,
                  winner = 0}]
      [claim; remove]
end