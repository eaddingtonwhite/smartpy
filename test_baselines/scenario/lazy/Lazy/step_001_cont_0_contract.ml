open Smartml

module Contract = struct

  let%entry_point myEntryPoint self params =
    verify (params.x <= 123) self.get_error((self.data, 0));
    verify (params.y <= 123) self.get_error((self.data, 1));
    verify (params.z <= 123) self.get_error((self.data, 2));
    self.data.x += (params.x + params.y) + params.z

  let%entry_point myEntryPoint2 self () =
    self.data.s <- self.get_error((self.data, 0)) + self.get_error((self.data, 3))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(bb = bigMap(bool, int), msg = bigMap(nat, string), s = string, x = intOrNat).layout(("bb", ("msg", ("s", "x"))))
      ~storage:[%expr
                 {bb = {},
                  msg = {0 : 'Bad compare', 1 : 'Bad compare2', 2 : 'Bad compare3', 3 : 'abcdefg'},
                  s = '',
                  x = 0}]
      [myEntryPoint; myEntryPoint2]
end