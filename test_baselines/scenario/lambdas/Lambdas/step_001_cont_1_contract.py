import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(abcd = sp.TInt, f = sp.TLambda(sp.TIntOrNat, sp.TIntOrNat), fff = sp.TOption(sp.TLambda(sp.TNat, sp.TNat)), ggg = sp.TOption(sp.TIntOrNat), value = sp.TNat).layout(("abcd", ("f", ("fff", ("ggg", "value"))))))
    self.init(abcd = 0,
              f = lambda(sp.TLambda(sp.TIntOrNat, sp.TIntOrNat)),
              fff = sp.none,
              ggg = sp.some(42),
              value = 0)

  @sp.entry_point
  def abs_test(self, params):
    self.data.abcd = self.abs(params)

  @sp.entry_point
  def comp_test(self):
    self.data.abcd = self.comp(sp.record(f = sp.build_lambda(lambda lparams_7: lparams_7 + 3), x = 2))

  @sp.entry_point
  def f(self):
    toto = sp.local("toto", sp.build_lambda(lambda lparams_8: sp.fst(lparams_8) + sp.snd(lparams_8)))
    titi = sp.local("titi", toto.value.apply(5))
    self.data.value = titi.value(8)

  @sp.entry_point
  def flambda(self):
    self.data.value = self.flam(self.flam(15)) + self.square_root(12345)

  @sp.entry_point
  def h(self):
    def f9(lparams_9):
      sp.verify(lparams_9 >= 0)
      y = sp.local("y", lparams_9)
      sp.while (y.value * y.value) > lparams_9:
        y.value = ((lparams_9 // y.value) + y.value) // 2
      sp.verify(((y.value * y.value) <= lparams_9) & (lparams_9 < ((y.value + 1) * (y.value + 1))))
      sp.result(y.value)
    self.data.fff = sp.some(sp.build_lambda(f9))

  @sp.entry_point
  def hh(self, params):
    self.data.value = self.data.fff.open_some()(params)

  @sp.entry_point
  def i(self):
    def f10(lparams_10):
      sp.verify(lparams_10 >= 0)
    ch1 = sp.local("ch1", sp.build_lambda(f10))
    def f11(lparams_11):
      sp.verify(lparams_11 >= 0)
      sp.result(lparams_11 - 2)
    ch2 = sp.local("ch2", sp.build_lambda(f11))
    def f12(lparams_12):
      sp.verify(lparams_12 >= 0)
      sp.result(True)
    ch3 = sp.local("ch3", sp.build_lambda(f12))
    def f13(lparams_13):
      def f14(lparams_14):
        sp.verify(lparams_14 >= 0)
        sp.result(False)
      ch3b = sp.local("ch3b", sp.build_lambda(f14))
      sp.verify(lparams_13 >= 0)
      sp.result(3 * lparams_13)
    ch4 = sp.local("ch4", sp.build_lambda(f13))
    self.data.value = ch4.value(12)
    y2 = sp.local("y2", self.not_pure(sp.record(in_param = sp.unit, in_storage = self.data)))
    self.data = y2.value.storage
    sp.for op in y2.value.operations.rev():
      sp.operations().push(op)
    sp.verify(y2.value.result == self.data.value)

  @sp.entry_point
  def operation_creation(self):
    def f15(lparams_15):
      __operations__ = sp.local("__operations__", sp.list([]), sp.TList(sp.TOperation))
      create_contract_lambdas_101 = sp.local("create_contract_lambdas_101", create contract ...)
      sp.operations().push(create_contract_lambdas_101.value.operation)
      create_contract_lambdas_102 = sp.local("create_contract_lambdas_102", create contract ...)
      sp.operations().push(create_contract_lambdas_102.value.operation)
      sp.result(sp.operations())
    f = sp.local("f", sp.build_lambda(f15))
    sp.for op in f.value(12345001):
      sp.operations().push(op)
    sp.for op in f.value(12345002):
      sp.operations().push(op)

  @sp.entry_point
  def operation_creation_result(self):
    def f16(lparams_16):
      __operations__ = sp.local("__operations__", sp.list([]), sp.TList(sp.TOperation))
      create_contract_lambdas_110 = sp.local("create_contract_lambdas_110", create contract ...)
      sp.operations().push(create_contract_lambdas_110.value.operation)
      __s3 = sp.local("__s3", 4)
      sp.result((sp.operations(), __s3.value))
    f = sp.local("f", sp.build_lambda(f16))
    x = sp.local("x", f.value(12345001))
    y = sp.local("y", f.value(12345002))
    sp.for op in sp.fst(x.value):
      sp.operations().push(op)
    sp.for op in sp.fst(y.value):
      sp.operations().push(op)
    sum = sp.local("sum", sp.snd(x.value) + sp.snd(y.value))