import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(check = sp.TOption(sp.TBool)).layout("check"))
    self.init(check = sp.none)

  @sp.entry_point
  def pairing_check(self, params):
    self.data.check = sp.some(sp.pairing_check(params))