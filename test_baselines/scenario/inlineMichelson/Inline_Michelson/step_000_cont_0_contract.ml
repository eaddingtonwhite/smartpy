open Smartml

module Contract = struct

  let%entry_point arith self () =
    verify (sp.michelson("ADD")(1, 2) = 3);
    verify (sp.michelson("ADD")(1, -2) = -1);
    verify (sp.michelson("SUB")(1, -2) = 3);
    verify (sp.michelson("SUB")(1, 2) = -1)

  let%entry_point concat1 self () =
    verify (sp.michelson("CONCAT")(sp.list(["a", "b", "c"])) = "abc")

  let%entry_point concat2 self () =
    verify (sp.michelson("CONCAT")("a", "b") = "ab")

  let%entry_point lambdas self () =
    verify (sp.michelson("PUSH (lambda int int) {DUP; ADD}; SWAP; EXEC;")(100) = 200);
    verify (sp.michelson("PAIR; PUSH (lambda (pair int int) int) { UNPAIR; PUSH int 10; MUL; ADD;}; SWAP; EXEC;")(2, 5) = 25);
    verify (sp.michelson("PAIR 3; PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; SWAP; EXEC;")(2, 5, 7) = 257);
    verify (sp.michelson("PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; PUSH int 2; APPLY; PUSH int 5; APPLY; PUSH int 7; EXEC;")() = 257)

  let%entry_point overflow_add self () =
    compute_inlineMichelson_36 = sp.local("compute_inlineMichelson_36", sp.michelson("ADD")(sp.mutez(9223372036854775807), sp.mutez(1)))

  let%entry_point overflow_mul self () =
    compute_inlineMichelson_41 = sp.local("compute_inlineMichelson_41", sp.michelson("MUL")(sp.mutez(4611686018427387904), 2))

  let%entry_point prim0 self () =
    verify (sp.michelson("UNIT")() = ());
    verify (sp.michelson("NONE unit")() = none);
    verify (sp.michelson("PUSH int 2; PUSH int 1; PAIR")() = (1, 2));
    verify ((len sp.michelson("NIL unit")()) = 0);
    verify ((len sp.michelson("EMPTY_SET unit")()) = 0);
    verify ((len sp.michelson("EMPTY_MAP unit unit")()) = 0)

  let%entry_point seq self () =
    verify (sp.michelson("DIP {SWAP}; ADD; MUL; DUP; MUL;")(15, 16, 17) = 262144)

  let%entry_point test_operations self () =
    sp.for op in sp.list([sp.michelson("NONE key_hash; SET_DELEGATE;")()]):
      operations().push(op);
    match_pair_inlineMichelson_89_fst, match_pair_inlineMichelson_89_snd = sp.match_tuple(sp.michelson("PUSH int 123; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter int; storage int; code { UNPAIR; ADD; NIL operation; PAIR } }; PAIR")(), "match_pair_inlineMichelson_89_fst", "match_pair_inlineMichelson_89_snd")
    sp.for op in sp.list([fst sp.michelson("PUSH int 123; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter int; storage int; code { UNPAIR; ADD; NIL operation; PAIR } }; PAIR")()]):
      operations().push(op)

  let%entry_point underflow self () =
    compute_inlineMichelson_46 = sp.local("compute_inlineMichelson_46", sp.michelson("SUB")(sp.tez(0), sp.mutez(1)))

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(s = string, value = intOrNat).layout(("s", "value"))
      ~storage:[%expr
                 {s = '',
                  value = 0}]
      [arith; concat1; concat2; lambdas; overflow_add; overflow_mul; prim0; seq; test_operations; underflow]
end