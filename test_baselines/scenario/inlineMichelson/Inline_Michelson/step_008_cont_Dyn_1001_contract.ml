open Smartml

module Contract = struct

  let%entry_point main self () =
    ()

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr 123]
      [main]
end