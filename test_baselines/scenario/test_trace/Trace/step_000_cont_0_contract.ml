open Smartml

module Contract = struct

  let%entry_point f self params =
    trace (params * 2);
    trace "double"

  let init storage =
    Basics.build_contract
      ~tstorage:unit
      ~storage:[%expr sp.unit]
      [f]
end