open Smartml

module Contract = struct

  let%entry_point play self params =
    set_type (params.move_nb : nat);
    set_type (params.move_data : nat);
    if self.data.current.player = 1 then
      verify (sender = self.data.constants.player1.addr) "Game_WrongPlayer"
    else
      verify (sender = self.data.constants.player2.addr) "Game_WrongPlayer";
    verify self.data.current.outcome.is_variant('None');
    verify (self.data.current.move_nb = params.move_nb);
    compute_game_tester_40 = sp.local("compute_game_tester_40", self.data.apply_({move_data = params.move_data; move_nb = self.data.current.move_nb; player = self.data.current.player; state = self.data.state}));
    match_pair_game_tester_50_fst, match_pair_game_tester_50_snd = sp.match_tuple(compute_game_tester_40.value, "match_pair_game_tester_50_fst", "match_pair_game_tester_50_snd")
    set_type (match_pair_game_tester_50_snd : sp.TBounded(['player_1_won', 'player_2_won'], t=string) option);
    with match_pair_game_tester_50_snd.match_cases() as arg:
      with arg.match('Some') as Some:
        self.data.current.outcome <- some (unbound Some)
      with arg.match('None') as None:
        self.data.current.outcome <- none
;
    self.data.current.move_nb += 1;
    self.data.current.player <- 3 - self.data.current.player;
    self.data.state <- match_pair_game_tester_50_fst

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(apply_ = sp.TLambda(sp.TRecord(move_data = nat, move_nb = nat, player = int, state = sp.TRecord(hash1 = bytes, hash2 = bytes, secret1 = nat option, secret2 = nat option).layout(("hash1", ("hash2", ("secret1", "secret2"))))).layout(("move_data", ("move_nb", ("player", "state")))), (sp.TRecord(hash1 = bytes, hash2 = bytes, secret1 = nat option, secret2 = nat option).layout(("hash1", ("hash2", ("secret1", "secret2")))) * sp.TBounded(['player_1_won', 'player_2_won'], t=string) option)), constants = sp.TRecord(channel_id = bytes, game_nonce = string, loser = mutez, model_id = bytes, player1 = sp.TRecord(addr = address, pk = key).layout(("addr", "pk")), player2 = sp.TRecord(addr = address, pk = key).layout(("addr", "pk")), winner = mutez).layout(("channel_id", ("game_nonce", ("loser", ("model_id", ("player1", ("player2", "winner"))))))), current = sp.TRecord(move_nb = nat, outcome = string option, player = int).layout(("move_nb", ("outcome", "player"))), state = sp.TRecord(hash1 = bytes, hash2 = bytes, secret1 = nat option, secret2 = nat option).layout(("hash1", ("hash2", ("secret1", "secret2"))))).layout(("apply_", ("constants", ("current", "state"))))
      ~storage:[%expr
                 {apply_ = lambda(sp.TLambda(sp.TRecord(move_data = nat, move_nb = nat, player = int, state = sp.TRecord(hash1 = bytes, hash2 = bytes, secret1 = nat option, secret2 = nat option).layout(("hash1", ("hash2", ("secret1", "secret2"))))).layout(("move_data", ("move_nb", ("player", "state")))), (sp.TRecord(hash1 = bytes, hash2 = bytes, secret1 = nat option, secret2 = nat option).layout(("hash1", ("hash2", ("secret1", "secret2")))) * sp.TBounded(['player_1_won', 'player_2_won'], t=string) option))),
                  constants = {channel_id = sp.bytes('0x01'); game_nonce = ''; loser = sp.tez(0); model_id = sp.bytes('0x'); player1 = {addr = sp.address('tz0Fakeplayer1'); pk = sp.key('edpkFakeplayer1')}; player2 = {addr = sp.address('tz0Fakeplayer2'); pk = sp.key('edpkFakeplayer2')}; winner = sp.tez(0)},
                  current = {move_nb = 0; outcome = sp.none; player = 1},
                  state = {hash1 = sp.bytes('0x64fabcca0db8d1beeaed6aa9a387fbed25054c08783d649f886a87873d47b00f'); hash2 = sp.bytes('0x1f462ac2296c296987b861db5b5dd6d921b84d031312e80a3a0d5a3ceb1ac5f3'); secret1 = sp.none; secret2 = sp.none}}]
      [play]
end