import smartpy as sp

GameTester = sp.io.import_template("state_channel_games/game_tester.py").GameTester

class HeadTail:
    def __init__(self):
        self.name = "Head and Tail"
        self.t_init_input = sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes)
        self.t_game_state = sp.TRecord(hash1 = sp.TBytes, hash2 = sp.TBytes, secret1 = sp.TOption(sp.TNat), secret2 = sp.TOption(sp.TNat))
        self.t_move_data  = sp.TNat
        self.t_outcome    = ["player_1_won", "player_2_won"]

    def apply_(self, move_data, move_nb, player, state):
        state = sp.local('state', state)
        outcome = sp.local('outcome', sp.none)
        with sp.if_(player == 1):
            sp.verify(sp.blake2b(sp.pack(move_data)) == state.value.hash1, "Invalid Secret")
            state.value.secret1 = sp.some(move_data)
        with sp.if_(player == 2):
            sp.verify(sp.blake2b(sp.pack(move_data)) == state.value.hash2, "Invalid Secret")
            state.value.secret2 = sp.some(move_data)
        with sp.if_(state.value.secret1.is_some() & state.value.secret2.is_some()):
            res = state.value.secret1.open_some() +  state.value.secret2.open_some()
            with sp.if_(res % 2 == 0):
                outcome.value = sp.some(sp.bounded("player_1_won"))
            with sp.else_():
                outcome.value = sp.some(sp.bounded("player_2_won"))

        sp.result(
            sp.pair(
                state.value,
                outcome.value,
            )
        )

    def init(self, params):
        sp.result(
            sp.record(
                hash1=params.hash1,
                hash2=params.hash2,
                secret1=sp.none,
                secret2=sp.none)
        )

# Tests
if "templates" not in __name__:

    player1 = sp.test_account('player1')
    player2 = sp.test_account('player2')
    p1 = sp.record(addr = player1.address, pk = player1.public_key)
    p2 = sp.record(addr = player2.address, pk = player2.public_key)

    @sp.add_test(name="HeadTail - 1")
    def test():
        scenario = sp.test_scenario()
        scenario.h1("HeadTail")
        scenario.h2("Contract")

        secret1 = 1234
        secret2 = 4568
        hash1 = sp.to_constant(sp.blake2b(sp.pack(secret1)))
        hash2 = sp.to_constant(sp.blake2b(sp.pack(secret2)))

        c1 = GameTester(HeadTail(), p1, p2, sp.record(hash1=hash1, hash2=hash2))
        scenario += c1
        scenario.h2("A sequence of interactions with a winner")
        c1.play(move_data = 42, move_nb = 0).run(sender = player1, valid=False)
        c1.play(move_data = 1234, move_nb = 0).run(sender = player1)
        c1.play(move_data = 4568, move_nb = 1).run(sender = player2)
        scenario.verify(c1.data.current.outcome == sp.some("player_1_won"))

    @sp.add_test(name="HeadTail - 2")
    def test():
        scenario = sp.test_scenario()
        scenario.h1("HeadTail")
        scenario.h2("Contract")

        secret1 = 1234
        secret2 = 4567
        hash1 = sp.to_constant(sp.blake2b(sp.pack(secret1)))
        hash2 = sp.to_constant(sp.blake2b(sp.pack(secret2)))

        c1 = GameTester(HeadTail(), p1, p2, sp.record(hash1=hash1, hash2=hash2))
        scenario += c1
        scenario.h2("A sequence of interactions with a winner")
        c1.play(move_data = 1234, move_nb = 0).run(sender = player1)
        c1.play(move_data = 4567, move_nb = 1).run(sender = player2)
        scenario.verify(c1.data.current.outcome == sp.some("player_2_won"))