import smartpy as sp

gp           = sp.io.import_template("state_channel_games/game_platform.py")

if "templates" not in __name__:
    admin1 = sp.test_account('admin1')
    admin2 = sp.test_account('admin2')
    notAdmin = sp.test_account('notAdmin')

    @sp.add_test(name="GamePlatform Change Admin")
    def test():
        sc = sp.test_scenario()

        sc.table_of_contents()
        sc.h1("Change Admin")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin1.address]))
        sc += c1

        sc.h2("Not admin trying to add admin")
        c1.add_admins(sp.set([admin2.address])).run(sender = admin2, valid = False, exception = "Platform_NotAdmin")

        sc.h2("Add new admin")
        c1.add_admins(sp.set([admin2.address])).run(sender = admin1)
        sc.verify_equal(c1.data.admins, sp.set([admin1.address, admin2.address]))

        sc.h2("Admin trying to remove itself")
        c1.remove_admins(sp.set([admin1.address])).run(sender = admin1, valid = False, exception = "Platform_CannotRemoveSelf")

        sc.h2("Non Admin trying to remove admin")
        c1.remove_admins(sp.set([admin1.address])).run(sender = notAdmin, valid = False, exception = "Platform_NotAdmin")

        sc.h2("Remove old admin")
        c1.remove_admins(sp.set([admin1.address])).run(sender = admin2)
        sc.verify_equal(c1.data.admins, sp.set([admin2.address]))
