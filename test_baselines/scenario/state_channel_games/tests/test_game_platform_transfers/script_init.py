import smartpy as sp

gp         = sp.io.import_template("state_channel_games/game_platform.py")
Transfer   = sp.io.import_template("state_channel_games/models/transfer.py").Transfer
model_wrap = sp.io.import_template("state_channel_games/model_wrap.py")
FA2        = sp.io.import_template("FA2.py")

def transfer_settlements(
        p1_to_p2 = {}, p2_to_p1 = {},
        p1_to_p1 = {}, p2_to_p2 = {},
        p0_to_p1 = {}, p0_to_p2 = {}
    ):
    transfers = []
    if len(p1_to_p2) > 0:
        transfers.append(gp.transfer(1, 2, p1_to_p2))
    if len(p2_to_p1) > 0:
        transfers.append(gp.transfer(2, 1, p2_to_p1))
    if len(p1_to_p1) > 0:
        transfers.append(gp.transfer(1, 1, p1_to_p1))
    if len(p2_to_p2) > 0:
        transfers.append(gp.transfer(2, 2, p2_to_p2))
    if len(p0_to_p1) > 0:
        transfers.append(gp.transfer(0, 1, p0_to_p1))
    if len(p0_to_p2) > 0:
        transfers.append(gp.transfer(0, 2, p0_to_p2))
    settlements = sp.map({
        sp.variant("player_double_played", 1)      : transfers,
        sp.variant("player_double_played", 2)      : transfers,
        sp.variant("player_inactive",      1)      : transfers,
        sp.variant("player_inactive",      2)      : transfers,
        sp.variant("game_finished", "transferred")  : transfers
    })
    bonds = {1: p1_to_p2, 2: p2_to_p1}
    sp.set_type_expr(bonds, gp.types.t_game_bonds)
    sp.set_type_expr(settlements, gp.types.t_constants)
    return settlements, bonds

def free_token_permissions():
    return sp.map({
        "max_supply": sp.pack(1_000_000_000),
        "type": sp.pack("FREE"),
    })

def make_signatures(p1, p2, x):
    sig1 = sp.make_signature(p1.secret_key, x)
    sig2 = sp.make_signature(p2.secret_key, x)
    return sp.map({p1.public_key: sig1, p2.public_key: sig2})

if "templates" not in __name__:
    admin = sp.test_account('admin')
    player1 = sp.test_account('player1')
    player2 = sp.test_account('player2')
    players = {player1.address: player1.public_key, player2.address: player2.public_key}

    transfer = Transfer()
    play_delay = 3600 * 24

    def new_game_sigs(constants, params = sp.unit):
        new_game = gp.action_new_game(constants, sp.pack(params))
        return make_signatures(player1, player2, new_game)

    @sp.add_test(name="Simple transfers")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Transfers")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin.address]))
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.new_model(model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        ###########
        #  Test 1 #
        ###########
        sc.h2("Simple transfer")
        game_num = 1
        sc.h3("Player 1 will transfer 10 to Player 2")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)
        sc.h3("Player 1 push {0: 10}")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 10}).run(sender = player1, amount = sp.mutez(10))

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(0))
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(0))
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[0] == 10)
        sc.p("P2 received 10 tokens 0")

        ############
        #  Test 2  #
        ############
        sc.h2("Transfer back")
        game_num += 1
        sc.h3("P1 and P2 inverted. Player 1 will transfer 10 to Player 2")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("P2 Instanciate new game : Transfer")
        constants = sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player2.address, 2:player1.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements)
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player2)

        sc.h3("(Onchain) play transfer")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player2)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(0))
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[0] == 10)
        c1.game_settle(game_id).run(sender = player1)
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(0))
        sc.p("P2 (player 1 of previous game) received 10 tokens 0")

        ###########
        #  Test 3 #
        ###########
        sc.h2("Multiple transfers")
        game_num += 1
        sc.h3("Admin setup free tokens")
        c1.set_token_permissions(token_id = 1, metadata = free_token_permissions()).run(sender = admin)
        c1.set_token_permissions(token_id = 2, metadata = free_token_permissions()).run(sender = admin)

        sc.h3("Player 1 transfers 10 token_1 to Player 2 and 10 token_2 ")
        settlements, bonds = transfer_settlements(p1_to_p2 = {1:10, 2:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)
        sc.h3("Player 1 push {1: 10, 2: 10}")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {1: 10, 2: 10}).run(sender = player1)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[1] == 10)
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[2] == 10)
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(1))
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(2))
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(1))
        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(2))
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[1] == 10)
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[2] == 10)
        sc.p("P1 received 10 token_1 and 10 token_2")

        ###########
        #  Test 4 #
        ###########
        sc.h2("Self transfer")
        game_num += 1
        sc.h3("Player 1 transfers 5 tokens to himself")
        settlements, bonds = transfer_settlements(p1_to_p1 = {0:5})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.p("P1 tokens didn't changed")

        ###########
        #  Test 5 #
        ###########
        sc.h2("Transfer in both direction")
        game_num += 1
        sc.h3("Player 1 transfers 5 tokens 0 to player 2, 2 tokens to himself, Player 2 transfer 4 tokens 0 to player 1")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:5}, p1_to_p1 = {0:2}, p2_to_p1 = {0: 4})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("Player 2 push {0: 10}")
        c1.channel_push_bonds(player_addr = player2.address, channel_id = channel_id, bonds = {0: 10}).run(sender = player2, amount = sp.mutez(10))

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Player 2 settle the game")
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[0] == 10)
        c1.game_settle(game_id).run(sender = player2)
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 9)
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[0] == 11)
        sc.p("P1 lost 1 token, P2 won 1 token")

        ###########
        #  Test 6 #
        ###########
        sc.h2("Simple withdraw")
        sc.h3("Player 1 request withdraw 5 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 5})).run(sender = player1)
        sc.p("Player 1 channel bonds")
        sc.show(c1.data.channels[channel_id].players[player1.address])
        sc.h3("Player 2 request leave empty challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([])).run(sender = player2)
        sc.h3("Player 1 finalize withdraw request")
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 9)
        c1.withdraw_finalise(channel_id).run(sender = player1)
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 4)
        sc.verify(c1.data.ledger[(player1.address, 0)].balance == 5)

    @sp.add_test(name="Withdraw challenges")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Withdraw challenges")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin.address]))
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.new_model(model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Player 1 push {0: 10}")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 10}).run(sender = player1, amount = sp.mutez(10))

        sc.h2("P1 Instanciate new game : Transfer")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:5})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        ###########
        #  Test 1 #
        ###########
        sc.h2("Challenged withdraw")
        sc.h3("Player 1 request withdraw 8 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 8})).run(sender = player1)
        sc.h3("Player 2 challenge by pushing a game_id")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id])).run(sender = player2)
        sc.h3("Player 1 can't finalize withdraw")
        c1.withdraw_finalise(channel_id).run(sender = player1, valid = False, exception = "Platform_TokenChallenged")
        sc.h3("Player 1 proposes an abort outcome for running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id, "aborted", sp.timestamp(3600 * 24))
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.game_set_outcome(game_id = game_id, outcome = "aborted", timeout = sp.timestamp(3600 * 24), signatures = outcome_sigs).run(sender = player1)
        c1.game_settle(game_id).run(sender = player1)
        sc.h3("Player 1 resolve challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id])).run(sender = player1)
        sc.h3("Player 1 finalize withdraw request")
        c1.withdraw_finalise(channel_id).run(sender = player1)

        ###########
        #  Test 2 #
        ###########
        sc.h2("Multi challenged withdraw")
        sc.h3("Admin setup free tokens")
        c1.set_token_permissions(token_id = 1, metadata = free_token_permissions()).run(sender = admin)
        sc.p("3 running transfers: 1 with low money, 2 with high money.<br/>\
              Player 1 resolves game 1 but it's not sufficient to withdraw and then resolve game 2, that become sufficient to withdraw")
        sc.h2("Player 1 push {1: 100}")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {1: 100}).run(sender = player1)
        sc.h2("Instanciate new transfers games")
        sc.h3("Game 1: 5 tokens 1")
        settlements, bonds = transfer_settlements(p1_to_p2 = {1:5})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game_1", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id_1 = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)
        sc.h3("Game 2: 40 tokens 1")
        settlements, bonds = transfer_settlements(p1_to_p2 = {1:40})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game_2", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id_2 = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)
        sc.h3("Game 3: 40 tokens 1")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game_3", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id_3 = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Player 1 request withdraw 50 tokens 1")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({1: 50})).run(sender = player1)
        sc.h3("Player 2 challenge by pushing 3 game ids")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id_1, game_id_2, game_id_3])).run(sender = player2)
        sc.h3("Player 1 can't finalize withdraw")
        c1.withdraw_finalise(channel_id).run(sender = player1, valid = False, exception = "Platform_TokenChallenged")

        sc.h3("Player 1 proposes an abort outcome for game_id_1 running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id_1, "aborted", sp.timestamp(3600 * 24))
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.game_set_outcome(game_id = game_id_1, outcome = "aborted", timeout = sp.timestamp(3600 * 24), signatures = outcome_sigs).run(sender = player1)
        c1.game_settle(game_id_1).run(sender = player1)
        sc.h3("Player 1 tries to resolve challenge but still doesn't have resolved enough challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id_1])).run(sender = player1)
        c1.withdraw_finalise(channel_id).run(sender = player1, valid = False, exception = "Platform_TokenChallenged")

        sc.h3("Player 1 proposes an abort outcome for game_id_2 running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id_2, "aborted", sp.timestamp(3600 * 24))
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.game_set_outcome(game_id = game_id_2, outcome = "aborted", timeout = sp.timestamp(3600 * 24), signatures = outcome_sigs).run(sender = player1)
        c1.game_settle(game_id_2).run(sender = player1)
        sc.h3("Player 1 resolves challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id_2])).run(sender = player1)
        sc.h3("Player 1 finalizes withdraw request")
        c1.withdraw_finalise(channel_id).run(sender = player1)

    @sp.add_test(name="Withdraw timeout and not enough tokens")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Expected transfer errors")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin.address]))
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        c1.new_model(model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Player 1 push {0: 10}")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 10}).run(sender = player1, amount = sp.mutez(10))

        sc.h2("Not enough tokens")
        sc.h3("Player 1 request withdraw 50 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 50})).run(sender = player1)
        sc.h3("Player 1 fails to withdraw 50 tokens")
        c1.withdraw_finalise(channel_id).run(sender = player1, now = sp.timestamp(3600 * 24 + 1), valid = False, exception = "Platform_NotEnoughTokens")

        sc.h3("Player 1 request withdraw 5 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 5})).run(sender = player1, now = sp.timestamp(3600 * 24 + 1))
        sc.h3("Player 1 can't finalize before timeout")
        c1.withdraw_finalise(channel_id).run(sender = player1, valid = False, exception = "Platform_ChallengeDelayNotOver")
        sc.h3("Player 1 finalize after timeout")
        c1.withdraw_finalise(channel_id).run(sender = player1, now = sp.timestamp(3600 * 24 * 2 + 2))
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 5)
        sc.verify(c1.data.ledger[(player1.address, 0)].balance == 5)

    @sp.add_test(name="Settled game Challenge")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Settled game Challenge")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin.address]))
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.new_model(model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Player 1 and 2 push {0: 10}")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 10}).run(sender = player1, amount = sp.mutez(10))

        sc.h2("P1 Instanciate new game : Transfer")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:100})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Player 1 proposes an abort outcome for game_id running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id, "aborted", sp.timestamp(3600 * 24))
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.game_set_outcome(game_id = game_id, outcome = "aborted", timeout = sp.timestamp(3600 * 24), signatures = outcome_sigs).run(sender = player1)
        c1.game_settle(game_id).run(sender = player1)

        sc.h2("Player 2 tries to challenge with a settled game")
        sc.h3("Player 1 request withdraw 5 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 5})).run(sender = player1)
        sc.h3("Player 2 fails to push settled game id as a challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id])).run(
            sender = player2, valid = False, exception = "Platform_GameSettled")

    @sp.add_test(name="Expected errors")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Expected transfer errors")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin.address]))
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.new_model(model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h3("P1 Instanciate new game : Transfer")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game1", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Player 1 plays")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)

        sc.h2("(fail) Settle: Don't own token")
        error_message = sp.pair("Platform_NotEnoughToken:", sp.record(sender = player1.address, token = 0, amount = 10))
        sp.set_type_expr(error_message, sp.TPair(sp.TString, sp.TRecord(sender = sp.TAddress, token = sp.TNat, amount = sp.TNat)))
        c1.game_settle(game_id).run(sender = player1, valid = False, exception = error_message)

        sc.h2("Push 5 tokens 0")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 5}).run(sender = player1, amount = sp.mutez(5))

        sc.h2("fail) Settle: No enough tokens")
        error_message = sp.pair("Platform_NotEnoughToken:", sp.record(sender = player1.address, token = 0, amount = 10))
        sp.set_type_expr(error_message, sp.TPair(sp.TString, sp.TRecord(sender = sp.TAddress, token = sp.TNat, amount = sp.TNat, bond = gp.types.t_token_map)))
        c1.game_settle(game_id).run(sender = player1, valid = False, exception = error_message)

        sc.h2("Not enought tez for bonds")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 5}).run(
            sender = player1, amount = sp.mutez(0), valid = False, exception = sp.pair("Platform_WrongMutezAmount_Expected", sp.utils.nat_to_mutez(5))
        )

        sc.h2("Too much tez for bonds")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 5}).run(
            sender = player1, amount = sp.mutez(0), valid = False, exception = sp.pair("Platform_WrongMutezAmount_Expected", sp.utils.nat_to_mutez(5))
        )

        sc.h2("Trying to push bonds for not configured token")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {404: 5}).run(
            sender = player1, amount = sp.mutez(0), valid = False, exception = sp.pair("Platform_TokenNotFound", 404)
        )

    @sp.add_test(name="Platform tokens transfers")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Transfers")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin.address]))
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.new_model(model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("New token: Reputation")
        token = sp.map({
            "name": sp.utils.bytes_of_string("Reputation token"),
            "decimals": sp.utils.bytes_of_string("%d" % 0),
            "symbol": sp.utils.bytes_of_string("R"),
        })
        c1.set_token_metadata(token_id = 42, metadata = token).run(sender = admin)
        token = sp.map({
            "type": sp.pack("INTERNAL"),
        })
        c1.set_token_permissions(token_id = 42, metadata = token).run(sender = admin)

        sc.h2("Simple transfer")
        game_num = 1
        sc.h3("Platform will mint 10 tokens 0 to Player 2")
        settlements, bonds = transfer_settlements(p0_to_p2 = {42:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Admin allows reputation transfer in game metadata")
        c1.update_game_metadata(game_id = game_id, key = "allowed_mint", metadata = sp.some(sp.pack(sp.map(l = {sp.nat(42): sp.nat(10)})))).run(sender = admin)

        sc.h3("Play transfer")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(42))
        sc.verify(~c1.data.token_permissions[42].contains("supply"))
        c1.game_settle(game_id).run(sender = player1)
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[42] == 10)
        sc.verify(sp.unpack(c1.data.token_permissions[42]["supply"], sp.TNat).open_some() == 10)
        sc.p("P2 received 10 tokens 0")

        sc.h2("Admin allow transfer model to mint Reputation token")
        c1.update_model_metadata(model_id = model_id, key = "allowed_mint", metadata = sp.some(sp.pack(sp.map(l = {sp.nat(42): sp.nat(15)})))).run(sender = admin)

        game_num += 1
        sc.h2("Simple transfer that mint Reputation token")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                         game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Play transfer")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[42] == 10)
        sc.verify(sp.unpack(c1.data.token_permissions[42]["supply"], sp.TNat).open_some() == 10)
        sc.verify(sp.unpack(c1.data.models[model_id].metadata["allowed_mint"], sp.TMap(sp.TNat, sp.TNat)).open_some()[42] == 15)
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[42] == 20)
        sc.verify(sp.unpack(c1.data.token_permissions[42]["supply"], sp.TNat).open_some() == 20)
        sc.verify(sp.unpack(c1.data.models[model_id].metadata["allowed_mint"], sp.TMap(sp.TNat, sp.TNat)).open_some()[42] == 5)
        sc.p("P2 received 10 tokens 0")

        game_num += 1
        sc.h2("Simple transfer that mint Reputation token")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                         game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Play transfer")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game cannot be completed because the model is not allowed to mint more")
        c1.game_settle(game_id).run(sender = player1, valid = False)

        sc.h3("Admin allows enough reputation transfer in game metadata")
        c1.update_game_metadata(game_id = game_id, key = "allowed_mint", metadata = sp.some(sp.pack(sp.map(l = {sp.nat(42): sp.nat(6)})))).run(sender = admin)
        sc.h3("Settle game")
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[42] == 20)
        sc.verify(sp.unpack(c1.data.token_permissions[42]["supply"], sp.TNat).open_some() == 20)
        sc.verify(sp.unpack(c1.data.models[model_id].metadata["allowed_mint"], sp.TMap(sp.TNat, sp.TNat)).open_some()[42] == 5)
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[42] == 30)
        sc.verify(sp.unpack(c1.data.token_permissions[42]["supply"], sp.TNat).open_some() == 30)
        sc.verify(sp.unpack(c1.data.models[model_id].metadata["allowed_mint"], sp.TMap(sp.TNat, sp.TNat)).open_some()[42] == 1)
        sc.p("P2 received 10 tokens 42")

    @sp.add_test(name="Platform tokens transfers errors")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Transfers")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin.address]))
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.new_model(model).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Simple transfer")
        sc.h3("Platform will mint 10 tokens 42 to Player 2")
        settlements, bonds = transfer_settlements(p0_to_p2 = {42:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game1", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        sc.show(game_id)
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Game transfer allowance not configured")
        c1.game_settle(game_id).run(
            sender = player1, valid = False,
            exception =  sp.pair('Platform_NotAllowedMinting', sp.record(amount = 10, game_id = game_id, token = 42))
        )

        sc.h3("Game transfer allowance not configured")
        c1.game_settle(game_id).run(
            sender = player1, valid = False,
            exception = sp.pair("Platform_NotAllowedMinting", sp.record(game_id = game_id, token = 42, amount = 10))
        )

        sc.h3("(Onchain) admin allows reputation transfer in metadata")
        c1.update_game_metadata(game_id = game_id, key = "allowed_mint", metadata = sp.some(sp.pack(sp.map(l = {sp.nat(42): sp.nat(5)})))).run(sender = admin)

        sc.h3("Game transfer allowance is not enough")
        c1.game_settle(game_id).run(
            sender = player1, valid = False,
            exception = sp.pair("Platform_NotAllowedMinting", sp.record(game_id = game_id, token = 42, amount = 10))
        )

        sc.h3("(Onchain) admin set allowed reputation transfer to 10")
        c1.update_game_metadata(game_id = game_id, key = "allowed_mint", metadata = sp.some(sp.pack(sp.map(l = {sp.nat(42): sp.nat(10)})))).run(sender = admin)

        sc.h3("Token doesnt exist")
        c1.game_settle(game_id).run(
            sender = player1, valid = False,
            exception = sp.pair("Platform_TokenNotFound", sp.record(token = 42))
        )

        sc.h2("New token: Reputation")
        token = sp.map({
            "name": sp.utils.bytes_of_string("Reputation token"),
            "decimals": sp.utils.bytes_of_string("%d" % 0),
            "symbol": sp.utils.bytes_of_string("R"),
        })
        c1.set_token_metadata(token_id = 42, metadata = token).run(sender = admin)
        token = sp.map({
            "type": sp.pack("INTERNAL"),
            "max_supply": sp.pack(5)
        })
        c1.set_token_permissions(token_id = 42, metadata = token).run(sender = admin)

        sc.h3("Token supply exceed")
        c1.game_settle(game_id).run(
            sender = player1, valid = False,
            exception = sp.pair("Platform_TokenSupplyExceed", sp.record(token = 42, amount = 10))
        )

        sc.h2("Increase token reputation supply")
        token = sp.map({
            "max_supply": sp.pack(10)
        })
        c1.set_token_permissions(token_id = 42, metadata = token).run(sender = admin)

        sc.h3("Valid transfer")
        c1.game_settle(game_id).run(sender = player1)

class DummyFA2(FA2.FA2, FA2.FA2_mint):
    pass

    @sp.add_test(name="FA2 tokens")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        FA2_admin = sp.test_account("FA2_admin")
        sc.h1("Transfers")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin.address]))
        sc += c1
        sc.h2("FA2")
        dummyToken = DummyFA2(
            FA2.FA2_config(single_asset = False),
            admin = FA2_admin.address,
            metadata = sp.utils.metadata_of_url("https://example.com")
        )
        sc += dummyToken
        sc.h2("Initial minting")
        dummy_md = FA2.FA2.make_metadata(
            name     = "Dummy FA2",
            decimals = 0,
            symbol   = "DFA2" )
        dummyToken.mint(
            address  = FA2_admin.address,
            token_id = 0,
            amount   = 100_000_000_000,
            metadata = dummy_md
        ).run(sender = FA2_admin)
        sc.h2("FA2_admin transfer 100 tokens to player1")
        dummyToken.transfer([dummyToken.batch_transfer.item(
            from_ = FA2_admin.address,
            txs = [ sp.record(to_ = player1.address, amount = 100, token_id = 0)])
        ]).run(sender = FA2_admin)
        sc.h2("GamePlatform admin registers FA2 token 0 on platform as token 1")
        c1.set_token_metadata(
            token_id = 1,
            metadata = {
                "name"        : sp.utils.bytes_of_string("Wrapped Dummy FA2"),
                "decimals"    : sp.utils.bytes_of_string("%d" % 0),
                "symbol"      : sp.utils.bytes_of_string("WDFA2"),
            }
        ).run(sender = admin)
        c1.set_token_permissions(
            token_id = 1,
            metadata = {
                "type"        : sp.pack("FA2"),
                "max_supply"  : sp.pack(100_000_000_000),
                "fa2_address" : sp.pack(dummyToken.address),
                "fa2_token_id": sp.pack(0),
            }
        ).run(sender = admin)

        sc.h2("Player1 set platform as operator")
        dummyToken.update_operators([
                sp.variant("add_operator", dummyToken.operator_param.make(
                    owner = player1.address,
                    operator = c1.address,
                    token_id = 0)),
        ]).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams).run(sender = player1)

        sc.h2("Player1 push dummy FA2 tokens as bonds")
        sc.verify(dummyToken.data.ledger[dummyToken.ledger_key.make(player1.address, 0)].balance == 100)
        sc.verify(~dummyToken.data.ledger.contains(dummyToken.ledger_key.make(c1.address, 0)))
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {1: 100}).run(sender = player1)
        sc.verify(dummyToken.data.ledger[dummyToken.ledger_key.make(player1.address, 0)].balance == 0)
        sc.verify(dummyToken.data.ledger[dummyToken.ledger_key.make(c1.address, 0)].balance == 100)

        sc.h2("Withdraw")
        sc.h3("Player 1 request withdraw 50 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({1: 50})).run(sender = player1)
        sc.h3("Player 2 request leave empty challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([])).run(sender = player2)
        sc.h3("Player 1 finalize withdraw request")
        sc.verify_equal(gp.player_bonds(c1, channel_id, player1.address)[1], 100)
        sc.verify(~c1.data.ledger.contains((player1.address, 1)))
        c1.withdraw_finalise(channel_id).run(sender = player1)
        sc.verify_equal(gp.player_bonds(c1, channel_id, player1.address)[1], 50)
        sc.verify_equal(c1.data.ledger[(player1.address, 1)].balance, 50)
        sc.h3("Player 1 withdraw_ledger")
        c1.withdraw_ledger(receiver = player1.address, tokens = {1: 25}).run(sender = player1)
        sc.verify_equal(c1.data.ledger[(player1.address, 1)].balance, 25)
        sc.verify_equal(dummyToken.data.ledger[dummyToken.ledger_key.make(player1.address, 0)].balance, 25)
        sc.verify_equal(dummyToken.data.ledger[dummyToken.ledger_key.make(c1.address, 0)].balance, 75)
