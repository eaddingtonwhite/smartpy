import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(result = sp.TOption(sp.TRecord(addr_players = sp.TMap(sp.TAddress, sp.TInt), constants = sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), current = sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive")))), player = sp.TInt).layout(("move_nb", ("outcome", "player"))), init_input = sp.TBytes, metadata = sp.TMap(sp.TString, sp.TBytes), settled = sp.TBool, state = sp.TOption(sp.TBytes), timeouts = sp.TMap(sp.TInt, sp.TTimestamp)).layout(("addr_players", ("constants", ("current", ("init_input", ("metadata", ("settled", ("state", "timeouts")))))))))).layout("result"))
    self.init(result = sp.none)

  @sp.entry_point
  def compute(self, params):
    sp.verify(params.params.game.current.outcome == sp.none, 'Platform_NotRunning')
    sp.set_type(params.params.game.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    game = sp.local("game", params.params.game)
    sp.verify(~ game.value.settled, 'Platform_GameSettled')
    sp.set_type(params.params.move_nb, sp.TNat)
    sp.set_type(params.params.move_data, sp.TBytes)
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(params.params.sender == game.value.constants.players_addr[game.value.current.player], 'Platform_GameWrongPlayer')
    sp.verify(game.value.current.move_nb == params.params.move_nb, 'Platform_Wrongmove_nb')
    sp.verify(game.value.current.outcome == sp.none, 'Platform_GameNotRunning')
    model = sp.local("model", params.data.models.get(game.value.constants.model_id, message = ('Platform_ModelNotFound: ', game.value.constants.model_id)))
    sp.if ~ game.value.state.is_some():
      game.value.state = sp.some(model.value.init(game.value.init_input))
    compute_game_platform_429 = sp.local("compute_game_platform_429", model.value.apply_(sp.record(move_data = params.params.move_data, move_nb = game.value.current.move_nb, player = game.value.current.player, state = game.value.state.open_some())))
    game.value.current = sp.record(move_nb = game.value.current.move_nb + 1, outcome = sp.eif(sp.snd(compute_game_platform_429.value).is_some(), sp.some(variant('game_finished', sp.snd(compute_game_platform_429.value).open_some())), sp.none), player = 3 - game.value.current.player)
    game.value.state = sp.some(sp.fst(compute_game_platform_429.value))
    __s4 = sp.local("__s4", game.value)
    self.data.result = sp.some(__s4.value)