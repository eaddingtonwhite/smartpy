open Smartml

module Contract = struct

  let%entry_point compute self params =
    game = sp.local("game", params.params.game);
    set_type (game.value.constants.players_addr : map(int, address));
    verify (not game.value.settled) "Platform_GameSettled";
    set_type (params.params.move_nb : nat);
    set_type (params.params.move_data : bytes);
    set_type (game.value.constants.players_addr : map(int, address));
    verify (params.params.sender = game.value.constants.players_addr[game.value.current.player]) "Platform_GameWrongPlayer";
    verify (game.value.current.move_nb = params.params.move_nb) "Platform_Wrongmove_nb";
    verify (game.value.current.outcome = none) "Platform_GameNotRunning";
    model = sp.local("model", params.data.models.get(game.value.constants.model_id, message = ("Platform_ModelNotFound: ", game.value.constants.model_id)));
    if not game.value.state.is_some() then
      game.value.state <- some model.value.init(game.value.init_input);
    compute_game_platform_429 = sp.local("compute_game_platform_429", model.value.apply_({move_data = params.params.move_data; move_nb = game.value.current.move_nb; player = game.value.current.player; state = game.value.state.open_some()}));
    game.value.current <- {move_nb = game.value.current.move_nb + 1; outcome = sp.eif((snd compute_game_platform_429.value).is_some(), some variant('game_finished', snd compute_game_platform_429.value.open_some()), none); player = 3 - game.value.current.player};
    game.value.state <- some (fst compute_game_platform_429.value);
    verify (params.params.new_state = game.value.state.open_some(message = "Plateform_GameNotInitialized"));
    verify (params.params.new_current = game.value.current);
    verify sp.check_signature(params.data.channels[game.value.constants.channel_id].players[params.params.sender].pk, params.params.signature, pack ("New State", (params.params.game_id : bytes), (game.value.current : sp.TRecord(move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option, player = int).layout(("move_nb", ("outcome", "player")))), (params.params.new_state : bytes))) "Platform_badSig";
    let%var __s7 = "OK" in
    self.data.result <- some __s7.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(result = string option).layout("result")
      ~storage:[%expr
                 {result = sp.none}]
      [compute]
end