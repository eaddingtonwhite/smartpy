import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(result = sp.TOption(sp.TString)).layout("result"))
    self.init(result = sp.none)

  @sp.entry_point
  def compute(self, params):
    game = sp.local("game", params.params.game)
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(~ game.value.settled, 'Platform_GameSettled')
    sp.set_type(params.params.move_nb, sp.TNat)
    sp.set_type(params.params.move_data, sp.TBytes)
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(params.params.sender == game.value.constants.players_addr[game.value.current.player], 'Platform_GameWrongPlayer')
    sp.verify(game.value.current.move_nb == params.params.move_nb, 'Platform_Wrongmove_nb')
    sp.verify(game.value.current.outcome == sp.none, 'Platform_GameNotRunning')
    model = sp.local("model", params.data.models.get(game.value.constants.model_id, message = ('Platform_ModelNotFound: ', game.value.constants.model_id)))
    sp.if ~ game.value.state.is_some():
      game.value.state = sp.some(model.value.init(game.value.init_input))
    compute_game_platform_429 = sp.local("compute_game_platform_429", model.value.apply_(sp.record(move_data = params.params.move_data, move_nb = game.value.current.move_nb, player = game.value.current.player, state = game.value.state.open_some())))
    game.value.current = sp.record(move_nb = game.value.current.move_nb + 1, outcome = sp.eif(sp.snd(compute_game_platform_429.value).is_some(), sp.some(variant('game_finished', sp.snd(compute_game_platform_429.value).open_some())), sp.none), player = 3 - game.value.current.player)
    game.value.state = sp.some(sp.fst(compute_game_platform_429.value))
    sp.verify(params.params.new_state == game.value.state.open_some(message = 'Plateform_GameNotInitialized'))
    sp.verify(params.params.new_current == game.value.current)
    sp.verify(sp.check_signature(params.data.channels[game.value.constants.channel_id].players[params.params.sender].pk, params.params.signature, sp.pack(('New State', sp.set_type_expr(params.params.game_id, sp.TBytes), sp.set_type_expr(game.value.current, sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive")))), player = sp.TInt).layout(("move_nb", ("outcome", "player")))), sp.set_type_expr(params.params.new_state, sp.TBytes)))), 'Platform_badSig')
    __s7 = sp.local("__s7", 'OK')
    self.data.result = sp.some(__s7.value)