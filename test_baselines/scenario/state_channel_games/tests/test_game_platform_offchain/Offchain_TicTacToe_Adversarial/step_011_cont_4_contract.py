import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(result = sp.TOption(sp.TPair(sp.TBytes, sp.TOption(sp.TString)))).layout("result"))
    self.init(result = sp.none)

  @sp.entry_point
  def compute(self, params):
    sp.verify(~ params.params.game.settled, 'Platform_GameSettled')
    sp.set_type(params.params.game.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(params.params.sender == params.params.game.constants.players_addr[params.params.game.current.player], 'Platform_GameWrongPlayer')
    sp.verify(params.params.game.current.move_nb == params.params.move_nb, 'Platform_Wrongmove_nb')
    sp.verify(params.params.game.current.outcome == sp.none, 'Platform_GameNotRunning')
    __s8 = sp.local("__s8", params.data.models[params.params.game.constants.model_id].apply_(sp.set_type_expr(sp.record(move_data = params.params.move_data, move_nb = params.params.move_nb, player = params.params.game.current.player, state = params.params.game.state.open_some(message = 'Plateform_GameNotInitialized')), sp.TRecord(move_data = sp.TBytes, move_nb = sp.TNat, player = sp.TInt, state = sp.TBytes).layout(("move_data", ("move_nb", ("player", "state")))))))
    self.data.result = sp.some(__s8.value)