open Smartml

module Contract = struct

  let%entry_point compute self params =
    verify (not params.params.game.settled) "Platform_GameSettled";
    set_type (params.params.game.constants.players_addr : map(int, address));
    verify (params.params.sender = params.params.game.constants.players_addr[params.params.game.current.player]) "Platform_GameWrongPlayer";
    verify (params.params.game.current.move_nb = params.params.move_nb) "Platform_Wrongmove_nb";
    verify (params.params.game.current.outcome = none) "Platform_GameNotRunning";
    let%var __s8 = params.data.models[params.params.game.constants.model_id].apply_(({move_data = params.params.move_data; move_nb = params.params.move_nb; player = params.params.game.current.player; state = params.params.game.state.open_some(message = "Plateform_GameNotInitialized")} : sp.TRecord(move_data = bytes, move_nb = nat, player = int, state = bytes).layout(("move_data", ("move_nb", ("player", "state")))))) in
    self.data.result <- some __s8.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(result = (bytes * string option) option).layout("result")
      ~storage:[%expr
                 {result = sp.none}]
      [compute]
end