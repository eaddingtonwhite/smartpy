import smartpy as sp

gp           = sp.io.import_template("state_channel_games/game_platform.py")
Tictactoe    = sp.io.import_template("state_channel_games/models/tictactoe.py").Tictactoe
Nim          = sp.io.import_template("state_channel_games/models/nim.py").Nim
model_wrap   = sp.io.import_template("state_channel_games/model_wrap.py")

class TestView(sp.Contract):
    def __init__(self, c, f):
        self.c = c
        self.f = f.f
        self.init(result = sp.none)

    @sp.entry_point
    def compute(self, data, params):
        self.c.data = data
        b = sp.bind_block()
        with b:
            self.f(self.c, params)
        self.data.result = sp.some(b.value)

def make_signatures(p1, p2, x):
    sig1 = sp.make_signature(p1.secret_key, x)
    sig2 = sp.make_signature(p2.secret_key, x)
    return sp.map({p1.public_key: sig1, p2.public_key: sig2})

if "templates" not in __name__:
    admin1 = sp.test_account('admin1')
    player1 = sp.test_account('player1')
    player2 = sp.test_account('player2')
    players = {player1.address: player1.public_key, player2.address: player2.public_key}

    def new_game_sigs(constants, params = sp.unit):
        new_game = gp.action_new_game(constants, sp.pack(params))
        return make_signatures(player1, player2, new_game)

    tictactoe = Tictactoe()
    nim       = Nim()
    play_delay = 3600 * 24
    settlements = sp.map({
        sp.variant("player_inactive",      1)      : [gp.transfer(1, 2, {0: 15})],
        sp.variant("player_inactive",      2)      : [gp.transfer(2, 1, {0: 15})],
        sp.variant("player_double_played", 1)      : [gp.transfer(1, 2, {0: 20})],
        sp.variant("player_double_played", 2)      : [gp.transfer(2, 1, {0: 20})],
        sp.variant("game_finished", "player_1_won"): [gp.transfer(2, 1, {0: 10})],
        sp.variant("game_finished", "player_2_won"): [gp.transfer(1, 2, {0: 10})],
    })

    @sp.add_test(name="Offchain_TicTacToe_Win")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        def get_state(testView):
            return sc.compute(testView.data.result.open_some())
        sc.h1("Offchain - Tic-Tac-Toe - Win")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin1.address]))
        sc += c1
        sc.h2("Offchain new_game")
        offchain_new_game = TestView(c1, c1.offchain_new_game)
        sc += offchain_new_game
        sc.h2("Offchain play")
        offchain_play = TestView(c1, c1.offchain_play)
        sc += offchain_play

        sc.h2("New model: TicTacToe")
        model = sc.compute(model_wrap.model_wrap(tictactoe))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.new_model(model).run(sender = player1)
        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 5)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)
        sc.h2("Push bonds")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 20}).run(sender = player1, amount = sp.mutez(20))
        c1.channel_push_bonds(player_addr = player2.address, channel_id = channel_id, bonds = {0: 20}).run(sender = player2, amount = sp.mutez(20))

        sc.h3("New game")
        constants = sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game1", play_delay = play_delay, settlements = settlements, bonds = {1:{0:10}, 2:{0:10}})
        game_id = sc.compute(gp.compute_game_id(constants))
        sc.h3("Calculate game data for new game")
        offchain_new_game.compute(sp.record(data = c1.data, params = sp.record(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)))).run(sender = player1)
        game = get_state(offchain_new_game)

        sc.h3("Move 0")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(sp.record(i = 1, j = 1)), move_nb = 0, sender = player1.address))).run(sender = player1)
        game_state_0 = get_state(offchain_play)
        sc.h3("Unpack game state")
        sc.show(sp.unpack(game_state_0.state.open_some(), tictactoe.t_game_state))

        new_state = gp.action_new_state(game_id, game_state_0.current, game_state_0.state.open_some())

        state_0_signatures = make_signatures(player1, player2, new_state)
        sc.h3("Signatures")
        sc.show(state_0_signatures)

        sc.h3("Move 1")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game_state_0, move_data = sp.pack(sp.record(i = 1, j = 2)), move_nb = 1, sender = player2.address))).run(sender = player2)
        game_state_1 = get_state(offchain_play)
        state_1_signatures = make_signatures(player1, player2, gp.action_new_state(game_id, game_state_1.current, game_state_1.state.open_some()))

        sc.h3("Move 2 to 4")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game_state_1, move_data = sp.pack(sp.record(i = 2, j = 1)), move_nb = 2, sender = player1.address))).run(sender = player1)
        game = get_state(offchain_play)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(sp.record(i = 2, j = 2)), move_nb = 3, sender = player2.address))).run(sender = player2)
        game = get_state(offchain_play)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(sp.record(i = 0, j = 1)), move_nb = 4, sender = player1.address))).run(sender = player1)
        game = get_state(offchain_play)
        state_4_signatures = make_signatures(player1, player2, gp.action_new_state(game_id, game.current, game.state.open_some()))

        # ---- Pushing onchain

        sc.h2("Pushing onchain step by step")
        sc.h3("New game")
        c1.new_game(game_id = game_id, constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)
        sc.h3("Update state 1")
        c1.game_set_state(game_id = game_id, new_current = game_state_0.current, new_state = game_state_0.state.open_some(), signatures = state_0_signatures).run(sender = player1)
        sc.h3("Update state 2")
        c1.game_set_state(game_id = game_id, new_current = game_state_1.current, new_state = game_state_1.state.open_some(), signatures = state_1_signatures).run(sender = player2)
        sc.h3("Update state 4")
        c1.game_set_state(game_id = game_id, new_current = game.current, new_state = game.state.open_some(), signatures = state_4_signatures).run(sender = player2)
        sc.verify(gp.finished_outcome(c1, game_id) == "player_1_won")
        sc.p("Player1 has won")

    @sp.add_test(name="Offchain_TicTacToe_Adversarial")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        def get_state(testView):
            return sc.compute(testView.data.result.open_some())
        sc.h1("Offchain - Tic-Tac-Toe - Adversarial Draw")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin1.address]))
        sc += c1
        sc.h2("Offchain new_game")
        offchain_new_game = TestView(c1, c1.offchain_new_game)
        sc += offchain_new_game
        sc.h2("Offchain play")
        offchain_play = TestView(c1, c1.offchain_play)
        sc += offchain_play
        sc.h2("Offchain Check move")
        offchain_check_move = TestView(c1, c1.offchain_check_move)
        sc += offchain_check_move
        sc.h2("Offchain Apply move")
        offchain_apply_move = TestView(c1, c1.offchain_apply_move)
        sc += offchain_apply_move

        sc.h2("New model: TicTacToe")
        model = sc.compute(model_wrap.model_wrap(tictactoe))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.new_model(model).run(sender = player1)
        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 5)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)
        sc.h2("Push bonds")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 20}).run(sender = player1, amount = sp.mutez(20))
        c1.channel_push_bonds(player_addr = player2.address, channel_id = channel_id, bonds = {0: 20}).run(sender = player2, amount = sp.mutez(20))

        sc.h2("(Offchain) New game")
        constants = sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game1", play_delay = play_delay, settlements = settlements, bonds = {1:{0:10}, 2:{0:10}})
        game_id = sc.compute(gp.compute_game_id(constants))
        offchain_new_game.compute(sp.record(data = c1.data, params = sp.record(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)))).run(sender = player1)
        game = get_state(offchain_new_game)

        sc.h3("(Offchain) Move 0")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(sp.record(i = 1, j = 1)), move_nb = 0, sender = player1.address))).run(sender = player1)
        game_state_0 = get_state(offchain_play)
        new_state = gp.action_new_state(game_id, game_state_0.current, game_state_0.state.open_some())
        sig1 = sp.make_signature(player1.secret_key, new_state)

        ###################################
        # Test 1: Go onchain and starving #
        ###################################
        sc.h2("Go onchain and starving")
        sc.p("Player 2 refuses to sign move 1")
        sc.h3("Player 1 forces player 2 to play")
        sc.h4("Push new game")
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)
        sc.h4("Push its move")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.record(i = 1, j = 1)), move_nb = 0).run(sender = player1)
        sc.h4("Player1 starving")
        c1.dispute_starving(game_id = game_id, flag = True).run(sender = player1)

        ###########################
        # Test 3: new_game replay #
        ###########################
        sc.h2("(fail) New game replay")
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player2, valid = False, exception = "Platform_GameAlreadyExists")
        #############################
        # Test 4: new_game override #
        #############################
        sc.h2("(fail) New game override")
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player2, valid = False, exception = "Platform_GameAlreadyExists")
        #################################
        # Test 5: Override other's move #
        #################################
        sc.h2("(fail) Other's move override")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.record(i = 1, j = 1)), move_nb = 0).run(sender = player2, valid = False, exception = "Platform_Wrongmove_nb")
        #######################
        # Test 6: Settle game #
        #######################
        sc.h2("(fail) Settle game")
        c1.game_settle(game_id).run(sender = player2, valid = False, exception = "Plateform_GameIsntOver")
        #######################################
        # Test 7: Fake new outcome signatures #
        #######################################
        sc.h2("(fail) Push outcome with fake signature")
        new_outcome = gp.action_new_outcome(game_id, "player_2_won", sp.timestamp(3600 * 24))
        sigs = make_signatures(player2, player2, new_outcome)
        c1.game_set_outcome(game_id = game_id, outcome = "player_2_won", timeout = sp.timestamp(3600 * 24), signatures = sigs).run(sender = player2, valid = False, exception = "Platform_MissingSig")
        ###################################
        # Test 8: Fake double sign report #
        ###################################
        sc.h2("Player2 tries to report player 1 double sign")
        statement1 = sp.record(state = game_state_0.state.open_some(), sig = sig1)
        c1.dispute_double_signed(game_id = game_id, player_id = 1, statement1 = statement1, statement2 = sp.none).run(sender = player2, valid = False, exception = "Platform_NotDifferentStates")

        sc.h2("(Onchain) move 1")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.record(i = 1, j = 2)), move_nb = 1).run(sender = player2)
        ###########################
        # Test 9: Remove starving #
        ###########################
        sc.h2("Player1 remove starving")
        c1.dispute_starving(game_id = game_id, flag = False).run(sender = player1)

        sc.h2("(Offchain) Move 2")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = c1.data.games[game_id], move_data = sp.pack(sp.record(i = 2, j = 1)), move_nb = 2, sender = player1.address))).run(sender = player1)
        game_state_2 = get_state(offchain_play)
        new_state = gp.action_new_state(game_id, game.current, game.state.open_some())

        sc.h2("(Offchain) Player 2 sends a wrong move")
        sc.h3("(Offchain) Player 2 tries to craft an invalid move")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game_state_2, move_data = sp.pack(sp.record(i = 1, j = 1)), move_nb = 3, sender = player2.address))).run(sender = player2, valid = False, exception = "move on a non empty cell")
        sc.h3("(Offchain) Player 2 prepares move 3")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game_state_2, move_data = sp.pack(sp.record(i = 2, j = 2)), move_nb = 3, sender = player2.address))).run(sender = player2)
        fake_game = get_state(offchain_play)
        sc.h3("(Offchain) Player 2 craft an invalid signed move and send it to player1")
        player2_move_data = sp.pack(sp.record(i = 1, j = 1))
        fake_state = sp.pack(sp.utils.matrix([[0, 0, 0], [0, 1, 0], [0, 0, 0]]))
        fake_game_state = gp.action_new_state(game_id, fake_game.current, sp.pack(sp.utils.matrix([[0, 0, 0], [0, 1, 0], [0, 0, 0]])))
        sig_fake_state = sp.make_signature(player2.secret_key, fake_game_state)
        sc.show(sp.record(fake_state = fake_state, sig_fake_state = sig_fake_state))

        ###############################
        # Test 10: Offchain check move #
        ###############################
        sc.h3("(Offchain) Player 1 discovers that it doesn't pass offchain_check_move")
        offchain_check_move.compute(
            sp.record(data = c1.data, params =
                sp.record(game_id = game_id, signature = sig_fake_state,
                          move_nb = 3, move_data = player2_move_data, sender = player2.address,
                          game = game_state_2, new_current = fake_game.current, new_state = fake_game.state.open_some())
            )
        ).run(sender = player1, valid = False)

        ################################
        # Test 11: Offchain apply move #
        ################################
        sc.h3("(Offchain) Player 1 tries to find the regular state for player2's move but it's an error")
        offchain_apply_move.compute(sp.record(data = c1.data, params = sp.record(game = game_state_2, sender = player2.address, move_nb = 3, move_data = player2_move_data))).run(sender = player1, valid = False, exception = "move on a non empty cell")

        sc.h2("(Offchain) Move 3")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game_state_2, move_data = sp.pack(sp.record(i = 2, j = 2)), move_nb = 3, sender = player2.address))).run(sender = player2)
        game_state_3 = get_state(offchain_play)
        new_state = gp.action_new_state(game_id, game_state_3.current, game_state_3.state.open_some())
        state_3_signatures = make_signatures(player1, player2, new_state)

        ##########################################
        # Test 12: Claim starved without timeout #
        ##########################################
        sc.h2("(fail) claim starved without timeout")
        c1.dispute_starved(game_id = game_id, player_id = 1).run(sender = player2, valid = False, exception = "Platform_NoTimeoutSetup")

        ##########################################
        # Test 13: Claim starved without timeout #
        ##########################################
        sc.h2("(fail) claim starved before timeout")
        sc.h3("Player 2 sets starving")
        c1.dispute_starving(game_id = game_id, flag = True).run(sender = player2)
        sc.h3("Player 2 claims starving before timeout")
        c1.dispute_starved(game_id = game_id, player_id = 1).run(sender = player2, valid = False, exception = "Platform_NotTimedOut")

        sc.h2("(Offchain) Move 4")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game_state_3, move_data = sp.pack(sp.record(i = 0, j = 0)), move_nb = 4, sender = player1.address))).run(sender = player1)
        game = get_state(offchain_play)
        new_state = gp.action_new_state(game_id, game.current, game.state.open_some())

        sc.h2("(Offchain) Player 2 tries to send a new move 3")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game_state_2, move_data = sp.pack(sp.record(i = 0, j = 1)), move_nb = 3, sender = player2.address))).run(sender = player2)
        game = get_state(offchain_play)
        new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sig_2 = sp.make_signature(player2.secret_key, new_state)

        ##########################
        # Test 14: Double signed #
        ##########################
        sc.h2("(Onchain) Player 1 report double signed")
        statement1 = sp.record(state = game_state_3.state.open_some(), sig = state_3_signatures[player2.public_key])
        statement2 = sp.record(current = game.current, state = game.state.open_some(), sig = sig_2)
        c1.dispute_double_signed(game_id = game_id, player_id = 2, statement1 = statement1, statement2 = sp.some(statement2)).run(sender = player1)
        sc.verify(gp.outcome(c1, game_id) == sp.variant("player_double_played", 2))

        ###############################
        # Test 15: New channel replay #
        ###############################
        sc.h2("(fail) Player 2 tries to recreate channel")
        c1.new_channel(channelParams).run(sender = player2, valid = False, exception = "Platform_ChannelAlreadyExists")

        ##########################
        # Test 16: Double settle #
        ##########################
        sc.h2("(Onchain) Player 1 settle the game")
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(c1.data.games[game_id].settled)
        sc.h2("(Onchain) Player 1 fails to double settle the game")
        c1.game_settle(game_id).run(sender = player1, valid = False, exception = "Plateform_GameSettled")

    @sp.add_test(name="game_set_outcome")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        def get_state(testView):
            return sc.compute(testView.data.result.open_some())
        sc.h1("Set outcome")
        sc.h2("Contract")
        c1 = gp.GamePlatform(sp.set([admin1.address]))
        sc += c1
        sc.h2("Offchain new_game")
        offchain_new_game = TestView(c1, c1.offchain_new_game)
        sc += offchain_new_game

        sc.h2("New model: TicTacToe")
        model = sc.compute(model_wrap.model_wrap(tictactoe))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.new_model(model).run(sender = player1)
        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 5)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)
        sc.h2("Push bonds")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 20}).run(sender = player1, amount = sp.mutez(20))
        c1.channel_push_bonds(player_addr = player2.address, channel_id = channel_id, bonds = {0: 20}).run(sender = player2, amount = sp.mutez(20))

        sc.h2("New game")
        constants = sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game1", play_delay = play_delay, settlements = settlements, bonds = {1:{0:10}, 2:{0:10}})
        game_id = sc.compute(gp.compute_game_id(constants))
        sc.h3("Calculate game data for new game")
        offchain_new_game.compute(sp.record(data = c1.data, params = sp.record(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)))).run(sender = player1)
        game = get_state(offchain_new_game)

        sc.h2("Vote for outcome proposal")
        outcome_timeout = sp.timestamp(3600 * 24)
        proposed_outcome = gp.action_new_outcome(game_id, "draw", outcome_timeout)
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        sc.show(proposed_outcome)
        sc.h3("Signatures")
        sc.show(outcome_sigs)

        sc.h2("Pushing onchain")
        c1.new_game(game_id = game_id, constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)
        sc.h3("(Expected failure) outcome timed out")
        c1.game_set_outcome(game_id = game_id, outcome = "draw", timeout = outcome_timeout, signatures = outcome_sigs).run(
            sender = player1, now = sp.timestamp(3600 * 48), valid = False, exception = "Platform_OutcomeTimedout")
        sc.h3("valid outcome timeout")
        c1.game_set_outcome(game_id = game_id, outcome = "draw", timeout = outcome_timeout, signatures = outcome_sigs).run(sender = player1, now = sp.timestamp(0))
        sc.verify(gp.finished_outcome(c1, game_id) == "draw")
