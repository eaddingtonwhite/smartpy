open Smartml

module Contract = struct

  let%entry_point add_admins self params =
    verify (self.data.admins.contains(sender)) "Platform_NotAdmin";
    sp.for admin in params.elements():
      self.data.admins.add(admin)

  let%entry_point channel_push_bonds self params =
    verify (not self.data.channels.get(params.channel_id, message = ("Platform_ChannelNotFound: ", params.channel_id)).closed) "Platform_ChannelClosed";
    player = sp.local("player", self.data.channels.get(params.channel_id, message = ("Platform_ChannelNotFound: ", params.channel_id)).players.get(params.player_addr, message = "Platform_NotChannelPlayer"));
    sp.for token in params.bonds.items():
      if token.key = 0 then
        verify (amount = (mul token.value sp.mutez(1))) ("Platform_WrongMutezAmount_Expected", mul token.value sp.mutez(1))
      else
        compute_game_platform_304 = sp.local("compute_game_platform_304", self.data.token_permissions.get(token.key, message = ("Platform_TokenNotFound", token.key)));
        verify (compute_game_platform_304.value.contains("type")) ("Platform_TokenTypeNotSetup", token.key);
        compute_game_platform_306 = sp.local("compute_game_platform_306", unpack compute_game_platform_304.value["type"] string.open_some());
        if compute_game_platform_306.value = "FA2" then
          verify (compute_game_platform_304.value.contains("fa2_address")) ("Platform_Token_NotFA2Setup", token.value);
          sp.transfer(sp.list([{from_ = sender; txs = sp.list([{to_ = self_address; token_id = unpack compute_game_platform_304.value["fa2_token_id"] nat.open_some(); amount = token.value}])}]), sp.tez(0), sp.contract(sp.TRecord(from_ = address, txs = sp.TRecord(amount = nat, to_ = address, token_id = nat).layout(("to_", ("token_id", "amount"))) list).layout(("from_", "txs")) list, unpack compute_game_platform_304.value["fa2_address"] address.open_some(), entry_point='transfer').open_some(message = "Plateform_FA2Unreacheable"))
        else
          verify (compute_game_platform_306.value = "FREE") ("Platform_TokenCannotBeRecived", token.key);
          verify (token.value < 1000000000000000000) ("Platform_AmountExceeded", token.key);
        token_permissions = sp.local("token_permissions", self.data.token_permissions.get(token.key, message = ("Platform_TokenNotFound", {token = token.key})));
        supply = sp.local("supply", 0);
        if token_permissions.value.contains("supply") then
          supply.value <- unpack token_permissions.value["supply"] nat.open_some();
        supply.value += token.value;
        if token_permissions.value.contains("max_supply") then
          verify (supply.value <= unpack token_permissions.value["max_supply"] nat.open_some()) ("Platform_TokenSupplyExceed", {amount = token.value; token = token.key});
        self.data.token_permissions[token.key]["supply"] <- pack supply.value;
      if player.value.bonds.contains(token.key) then
        player.value.bonds[token.key] += token.value
      else
        player.value.bonds[token.key] <- token.value;
    self.data.channels[params.channel_id].players[params.player_addr] <- player.value

  let%entry_point dispute_double_signed self params =
    set_type (params.statement1 : sp.TRecord(sig = signature, state = bytes).layout(("sig", "state")));
    set_type (params.statement2 : sp.TRecord(current = sp.TRecord(move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option, player = int).layout(("move_nb", ("outcome", "player"))), sig = signature, state = bytes).layout(("current", ("sig", "state"))) option);
    game = sp.local("game", self.data.games.get(params.game_id, message = ("Platform_GameNotFound: ", params.game_id)));
    verify (game.value.current.outcome = none) "Platform_NotRunning";
    set_type (game.value.constants.players_addr : map(int, address));
    player = sp.local("player", self.data.channels[game.value.constants.channel_id].players[game.value.constants.players_addr[params.player_id]]);
    if params.statement2.is_variant('None') then
      verify sp.check_signature(player.value.pk, params.statement1.sig, pack ("New State", (params.game_id : bytes), (game.value.current : sp.TRecord(move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option, player = int).layout(("move_nb", ("outcome", "player")))), (params.statement1.state : bytes))) "Platform_badSig";
      verify (not (params.statement1.state = game.value.state.open_some(message = "Plateform_GameNotInitialized"))) "Platform_NotDifferentStates"
    else
      statement2 = sp.local("statement2", params.statement2.open_some());
      verify sp.check_signature(player.value.pk, params.statement1.sig, pack ("New State", (params.game_id : bytes), (statement2.value.current : sp.TRecord(move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option, player = int).layout(("move_nb", ("outcome", "player")))), (params.statement1.state : bytes))) "Platform_badSig";
      verify sp.check_signature(player.value.pk, statement2.value.sig, pack ("New State", (params.game_id : bytes), (statement2.value.current : sp.TRecord(move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option, player = int).layout(("move_nb", ("outcome", "player")))), (statement2.value.state : bytes))) "Platform_badSig";
      verify (not (params.statement1.state = statement2.value.state)) "Platform_NotSameStates";
    game.value.current.outcome <- some variant('player_double_played', params.player_id);
    self.data.games[params.game_id] <- game.value

  let%entry_point dispute_starved self params =
    game = sp.local("game", self.data.games.get(params.game_id, message = ("Platform_GameNotFound: ", params.game_id)));
    verify (game.value.current.outcome = none) "Platform_NotRunning";
    verify (game.value.timeouts.contains(params.player_id)) "Platform_NoTimeoutSetup";
    verify (now > game.value.timeouts[params.player_id]) "Platform_NotTimedOut";
    game.value.current.outcome <- some variant('player_inactive', params.player_id);
    self.data.games[params.game_id] <- game.value

  let%entry_point dispute_starving self params =
    game = sp.local("game", self.data.games.get(params.game_id, message = ("Platform_GameNotFound: ", params.game_id)));
    verify (game.value.current.outcome = none) "Platform_NotRunning";
    if params.flag then
      game.value.timeouts[3 - game.value.addr_players[sender]] <- add_seconds now game.value.constants.play_delay
    else
      del game.value.timeouts[3 - game.value.addr_players[sender]];
    self.data.games[params.game_id] <- game.value

  let%entry_point game_play self params =
    game = sp.local("game", self.data.games.get(params.game_id, message = ("Platform_GameNotFound: ", params.game_id)));
    verify (game.value.current.outcome = none) "Platform_NotRunning";
    set_type (params.move_nb : nat);
    set_type (params.move_data : bytes);
    set_type (game.value.constants.players_addr : map(int, address));
    verify (sender = game.value.constants.players_addr[game.value.current.player]) "Platform_GameWrongPlayer";
    verify (game.value.current.move_nb = params.move_nb) "Platform_Wrongmove_nb";
    verify (game.value.current.outcome = none) "Platform_GameNotRunning";
    model = sp.local("model", self.data.models.get(game.value.constants.model_id, message = ("Platform_ModelNotFound: ", game.value.constants.model_id)));
    if not game.value.state.is_some() then
      game.value.state <- some model.value.init(game.value.init_input);
    compute_game_platform_429 = sp.local("compute_game_platform_429", model.value.apply_({move_data = params.move_data; move_nb = game.value.current.move_nb; player = game.value.current.player; state = game.value.state.open_some()}));
    game.value.current <- {move_nb = game.value.current.move_nb + 1; outcome = sp.eif((snd compute_game_platform_429.value).is_some(), some variant('game_finished', snd compute_game_platform_429.value.open_some()), none); player = 3 - game.value.current.player};
    game.value.state <- some (fst compute_game_platform_429.value);
    if game.value.timeouts.contains(game.value.current.player) then
      game.value.timeouts[game.value.current.player] <- add_seconds now game.value.constants.play_delay;
    self.data.games[params.game_id] <- game.value

  let%entry_point game_set_outcome self params =
    set_type (params.signatures : map(key, signature));
    verify (now <= params.timeout) "Platform_OutcomeTimedout";
    game = sp.local("game", self.data.games.get(params.game_id, message = ("Platform_GameNotFound: ", params.game_id)));
    verify (game.value.current.outcome = none) "Platform_NotRunning";
    compute_game_platform_449 = sp.local("compute_game_platform_449", pack ("New Outcome", (params.game_id : bytes), (params.outcome : string), params.timeout));
    verify (not self.data.channels.get(game.value.constants.channel_id, message = ("Platform_ChannelNotFound: ", game.value.constants.channel_id)).closed) "Platform_ChannelClosed";
    players = sp.local("players", self.data.channels.get(game.value.constants.channel_id, message = ("Platform_ChannelNotFound: ", game.value.constants.channel_id)).players);
    sp.for player in players.value.values():
      verify (params.signatures.contains(player.pk)) "Platform_MissingSig";
      verify sp.check_signature(player.pk, params.signatures[player.pk], compute_game_platform_449.value) "Platform_badSig";
    game.value.current.outcome <- some variant('game_finished', params.outcome);
    self.data.games[params.game_id] <- game.value

  let%entry_point game_set_state self params =
    set_type (params.signatures : map(key, signature));
    game = sp.local("game", self.data.games.get(params.game_id, message = ("Platform_GameNotFound: ", params.game_id)));
    verify (game.value.current.outcome = none) "Platform_NotRunning";
    verify (params.new_current.move_nb > game.value.current.move_nb) "Platform_OutdatedMoveNB";
    compute_game_platform_464 = sp.local("compute_game_platform_464", pack ("New State", (params.game_id : bytes), (params.new_current : sp.TRecord(move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option, player = int).layout(("move_nb", ("outcome", "player")))), (params.new_state : bytes)));
    channel = sp.local("channel", self.data.channels.get(game.value.constants.channel_id, message = ("Platform_ChannelNotFound: ", game.value.constants.channel_id)));
    verify (not channel.value.closed) "Platform_ChannelClosed";
    sp.for player in channel.value.players.values():
      verify sp.check_signature(player.pk, params.signatures[player.pk], compute_game_platform_464.value) ("Platform_badSig", player.pk);
    game.value.current <- params.new_current;
    game.value.state <- some params.new_state;
    if game.value.timeouts.contains(game.value.current.player) then
      game.value.timeouts[game.value.current.player] <- add_seconds now game.value.constants.play_delay;
    self.data.games[params.game_id] <- game.value

  let%entry_point game_settle self params =
    game = sp.local("game", self.data.games.get(params, message = ("Platform_GameNotFound: ", params)));
    model = sp.local("model", sp.eif(self.data.models.contains(game.value.constants.model_id), some self.data.models[game.value.constants.model_id], none));
    verify (not game.value.settled) "Plateform_GameSettled";
    allowed_mint_model = sp.local("allowed_mint_model", {});
    allowed_mint_game = sp.local("allowed_mint_game", {});
    if model.value.is_some() then
      if model.value.open_some().metadata.contains("allowed_mint") then
        allowed_mint_model.value <- unpack model.value.open_some().metadata["allowed_mint"] map(nat, nat).open_some();
    if game.value.metadata.contains("allowed_mint") then
      allowed_mint_game.value <- unpack game.value.metadata["allowed_mint"] map(nat, nat).open_some();
    if game.value.constants.settlements.contains(game.value.current.outcome.open_some(message = "Plateform_GameIsntOver")) then
      compute_game_platform_523 = sp.local("compute_game_platform_523", game.value.constants.settlements[game.value.current.outcome.open_some()]);
      channel = sp.local("channel", self.data.channels.get(game.value.constants.channel_id, message = ("Platform_ChannelNotFound: ", game.value.constants.channel_id)));
      verify (not channel.value.closed) "Platform_ChannelClosed";
      sp.for transfer in compute_game_platform_523.value:
        if transfer.sender = 0 then
          compute_game_platform_253 = sp.local("compute_game_platform_253", game.value.constants.players_addr[transfer.receiver]);
          receiver = sp.local("receiver", channel.value.players[compute_game_platform_253.value]);
          sp.for tokens in transfer.bonds.items():
            delta = sp.local("delta", allowed_mint_game.value.get(tokens.key, default_value = 0) - tokens.value);
            allowed_mint_game.value[tokens.key] <- sp.eif(delta.value > 0, as_nat delta.value, 0);
            if delta.value < 0 then
              allowed_mint_model.value[tokens.key] <- sp.as_nat((to_int allowed_mint_model.value.get(tokens.key, default_value = 0)) + delta.value, message = ("Platform_NotAllowedMinting", {amount = tokens.value; game_id = params; token = tokens.key}));
            token_permissions = sp.local("token_permissions", self.data.token_permissions.get(tokens.key, message = ("Platform_TokenNotFound", {token = tokens.key})));
            supply = sp.local("supply", 0);
            if token_permissions.value.contains("supply") then
              supply.value <- unpack token_permissions.value["supply"] nat.open_some();
            supply.value += tokens.value;
            if token_permissions.value.contains("max_supply") then
              verify (supply.value <= unpack token_permissions.value["max_supply"] nat.open_some()) ("Platform_TokenSupplyExceed", {amount = tokens.value; token = tokens.key});
            self.data.token_permissions[tokens.key]["supply"] <- pack supply.value;
            receiver.value.bonds[tokens.key] <- receiver.value.bonds.get(tokens.key, default_value = 0) + tokens.value;
          channel.value.players[compute_game_platform_253.value] <- receiver.value
        else
          compute_game_platform_281 = sp.local("compute_game_platform_281", game.value.constants.players_addr[transfer.sender]);
          compute_game_platform_282 = sp.local("compute_game_platform_282", game.value.constants.players_addr[transfer.receiver]);
          if not (compute_game_platform_281.value = compute_game_platform_282.value) then
            sender = sp.local("sender", channel.value.players[compute_game_platform_281.value]);
            receiver = sp.local("receiver", channel.value.players[compute_game_platform_282.value]);
            sp.for bond in transfer.bonds.items():
              if not (bond.value = 0) then
                verify (sender.value.bonds.contains(bond.key)) ("Platform_NotEnoughToken:", {amount = bond.value; sender = compute_game_platform_281.value; token = bond.key});
                sender.value.bonds[bond.key] <- sp.as_nat(sender.value.bonds[bond.key] - bond.value, message = ("Platform_NotEnoughToken:", {amount = bond.value; sender = compute_game_platform_281.value; token = bond.key}));
                if sender.value.bonds[bond.key] = 0 then
                  del sender.value.bonds[bond.key];
                receiver.value.bonds[bond.key] <- receiver.value.bonds.get(bond.key, default_value = 0) + bond.value;
            channel.value.players[compute_game_platform_281.value] <- sender.value;
            channel.value.players[compute_game_platform_282.value] <- receiver.value;
      self.data.channels[game.value.constants.channel_id].players <- channel.value.players;
    game.value.settled <- True;
    self.data.games[params] <- game.value;
    if (len allowed_mint_model.value) > 0 then
      self.data.models[game.value.constants.model_id].metadata["allowed_mint"] <- pack allowed_mint_model.value
    else
      if model.value.is_some() then
        del self.data.models[game.value.constants.model_id].metadata["allowed_mint"]

  let%entry_point ledger_to_bonds self params =
    channel = sp.local("channel", self.data.channels.get(params.channel_id, message = ("Platform_ChannelNotFound: ", params.channel_id)));
    verify (not channel.value.closed) "Platform_ChannelClosed";
    to = sp.local("to", channel.value.players.get(params.receiver, message = "Platform_ReceiverNotChannelPlayer"));
    sp.for token in params.tokens.items():
      if token.value > 0 then
        verify (self.data.ledger.contains((sender, token.key))) "Platform_NotEnoughToken";
        self.data.ledger[(sender, token.key)] <- {balance = sp.as_nat(self.data.ledger[(sender, token.key)].balance - token.value, message = "Platform_NotEnoughToken")};
        if to.value.bonds.contains(token.key) then
          to.value.bonds[token.key] += token.value
        else
          to.value.bonds[token.key] <- token.value;
    self.data.channels[params.channel_id].players[params.receiver] <- to.value

  let%entry_point new_channel self params =
    verify (not (self.data.channels.contains(blake2b (pack (self_address, params.players, params.nonce))))) "Platform_ChannelAlreadyExists";
    verify ((len params.players) = 2) "Platform_Only2PlayersAllowed";
    players_map = sp.local("players_map", {});
    sp.for player in params.players.items():
      players_map.value[player.key] <- {bonds = {}; pk = player.value; withdraw = none; withdraw_id = 0};
    self.data.channels[blake2b (pack (self_address, params.players, params.nonce))] <- {closed = False; nonce = params.nonce; players = players_map.value; withdraw_delay = params.withdraw_delay}

  let%entry_point new_game self params =
    set_type (params.constants : sp.TRecord(bonds = map(int, map(nat, nat)), channel_id = bytes, game_nonce = string, model_id = bytes, play_delay = int, players_addr = map(int, address), settlements = map(sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TRecord(bonds = map(nat, nat), receiver = int, sender = int).layout(("bonds", ("receiver", "sender"))) list)).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))));
    set_type (params.params : bytes);
    set_type (params.signatures : map(key, signature));
    verify (not (self.data.games.contains(blake2b (pack (params.constants.channel_id, (params.constants.model_id, params.constants.game_nonce)))))) "Platform_GameAlreadyExists";
    verify ((len params.constants.players_addr) = 2) "Platform_Only2PlayersAllowed";
    channel = sp.local("channel", self.data.channels.get(params.constants.channel_id, message = ("Platform_ChannelNotFound: ", params.constants.channel_id)));
    verify (not channel.value.closed) "Platform_ChannelClosed";
    addr_players = sp.local("addr_players", {});
    sp.for addr_player in params.constants.players_addr.items():
      addr_players.value[addr_player.value] <- addr_player.key;
      verify (channel.value.players.contains(addr_player.value)) "Platform_GamePlayerNotInChannel";
    sp.for player in channel.value.players.values():
      verify (params.signatures.contains(player.pk)) "Platform_MissingSig";
      verify sp.check_signature(player.pk, params.signatures[player.pk], pack ("New Game", (params.constants : sp.TRecord(bonds = map(int, map(nat, nat)), channel_id = bytes, game_nonce = string, model_id = bytes, play_delay = int, players_addr = map(int, address), settlements = map(sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TRecord(bonds = map(nat, nat), receiver = int, sender = int).layout(("bonds", ("receiver", "sender"))) list)).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements")))))))), (params.params : bytes))) ("Platform_BadSig", {player = player.pk; signature = params.signatures[player.pk]});
    self.data.games[blake2b (pack (params.constants.channel_id, (params.constants.model_id, params.constants.game_nonce)))] <- ({addr_players = addr_players.value; constants = params.constants; current = {move_nb = 0; outcome = none; player = 1}; init_input = params.params; metadata = {}; settled = False; state = none; timeouts = {}} : sp.TRecord(addr_players = map(address, int), constants = sp.TRecord(bonds = map(int, map(nat, nat)), channel_id = bytes, game_nonce = string, model_id = bytes, play_delay = int, players_addr = map(int, address), settlements = map(sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TRecord(bonds = map(nat, nat), receiver = int, sender = int).layout(("bonds", ("receiver", "sender"))) list)).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), current = sp.TRecord(move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option, player = int).layout(("move_nb", ("outcome", "player"))), init_input = bytes, metadata = map(string, bytes), settled = bool, state = bytes option, timeouts = map(int, timestamp)).layout(("addr_players", ("constants", ("current", ("init_input", ("metadata", ("settled", ("state", "timeouts")))))))))

  let%entry_point new_model self params =
    set_type (params.name : string);
    compute_game_platform_342 = sp.local("compute_game_platform_342", blake2b params.model);
    if not (self.data.models.contains(compute_game_platform_342.value)) then
      compute_game_platform_344 = sp.local("compute_game_platform_344", unpack params.model sp.TRecord(apply_ = sp.TLambda(sp.TRecord(move_data = bytes, move_nb = nat, player = int, state = bytes).layout(("move_data", ("move_nb", ("player", "state")))), (bytes * string option)), init = sp.TLambda(bytes, bytes), outcomes = string list).layout(("apply_", ("init", "outcomes"))).open_some(message = "Platform_ModelUnpackFailure"));
      self.data.models[compute_game_platform_342.value] <- {apply_ = compute_game_platform_344.value.apply_; init = compute_game_platform_344.value.init; metadata = ({"name" : pack params.name} : map(string, bytes)); outcomes = compute_game_platform_344.value.outcomes}

  let%entry_point remove_admins self params =
    verify (self.data.admins.contains(sender)) "Platform_NotAdmin";
    verify (not (params.contains(sender))) "Platform_CannotRemoveSelf";
    sp.for admin in params.elements():
      self.data.admins.remove(admin)

  let%entry_point set_token_metadata self params =
    verify (self.data.admins.contains(sender)) "Platform_NotAdmin";
    self.data.token_metadata[params.token_id] <- params.metadata

  let%entry_point set_token_permissions self params =
    verify (self.data.admins.contains(sender)) "Platform_NotAdmin";
    self.data.token_permissions[params.token_id] <- params.metadata

  let%entry_point update_game_metadata self params =
    verify (self.data.admins.contains(sender)) "Platform_NotAdmin";
    verify (self.data.games.contains(params.game_id)) "Platform_GameNotFound";
    set_type (params.metadata : bytes option);
    set_type (params.key : string);
    if params.metadata.is_some() then
      self.data.games[params.game_id].metadata[params.key] <- params.metadata.open_some()
    else
      del self.data.games[params.game_id].metadata[params.key]

  let%entry_point update_global_metadata self params =
    verify (self.data.admins.contains(sender)) "Platform_NotAdmin";
    set_type (params.metadata : bytes option);
    set_type (params.key : string);
    if params.metadata.is_some() then
      self.data.metadata[params.key] <- params.metadata.open_some()
    else
      del self.data.metadata[params.key]

  let%entry_point update_model_metadata self params =
    verify (self.data.admins.contains(sender)) "Platform_NotAdmin";
    verify (self.data.models.contains(params.model_id)) "Platform_ModelNotFound";
    set_type (params.metadata : bytes option);
    set_type (params.key : string);
    if params.metadata.is_some() then
      self.data.models[params.model_id].metadata[params.key] <- params.metadata.open_some()
    else
      del self.data.models[params.model_id].metadata[params.key]

  let%entry_point withdraw_cancel self params =
    channel = sp.local("channel", self.data.channels.get(params, message = ("Platform_ChannelNotFound: ", params)));
    verify (not channel.value.closed) "Platform_ChannelClosed";
    verify (channel.value.players.contains(sender)) "Platform_SenderNotInChannel";
    player = sp.local("player", channel.value.players[sender]);
    verify player.value.withdraw.is_some() "Platform_NoWithdrawOpened";
    verify (now >= player.value.withdraw.open_some().timeout) "Platform_ChallengeDelayNotOver";
    channel.value.players[sender].withdraw <- none;
    self.data.channels[params] <- channel.value

  let%entry_point withdraw_challenge self params =
    set_type (params.game_ids : sp.TSet(bytes));
    channel = sp.local("channel", self.data.channels.get(params.channel_id, message = ("Platform_ChannelNotFound: ", params.channel_id)));
    verify (not channel.value.closed) "Platform_ChannelClosed";
    verify (channel.value.players.contains(sender)) "Platform_SenderNotInChannel";
    verify (channel.value.players.contains(params.withdrawer)) "Platform_WithdrawerNotInChannel";
    verify channel.value.players[params.withdrawer].withdraw.is_some() "Platform_NoWithdrawRequestOpened";
    withdraw = sp.local("withdraw", channel.value.players[params.withdrawer].withdraw.open_some());
    sp.for game_id in params.game_ids.elements():
      game = sp.local("game", self.data.games.get(game_id, message = ("Platform_GameNotFound: ", game_id)));
      verify (game.value.constants.channel_id = params.channel_id) "Platform_ChannelIdMismatch";
      if game.value.settled then
        verify (withdraw.value.challenge.contains(game_id)) "Platform_GameSettled";
        withdraw.value.challenge.remove(game_id);
        set_type (withdraw.value.challenge_tokens : map(nat, int));
        set_type (game.value.constants.bonds[game.value.addr_players[params.withdrawer]] : map(nat, nat));
        sp.for token in game.value.constants.bonds[game.value.addr_players[params.withdrawer]].items():
          if withdraw.value.challenge_tokens.contains(token.key) then
            compute_game_platform_837 = sp.local("compute_game_platform_837", withdraw.value.challenge_tokens[token.key] - (to_int game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]));
            if compute_game_platform_837.value = 0 then
              del withdraw.value.challenge_tokens[token.key]
            else
              withdraw.value.challenge_tokens[token.key] <- compute_game_platform_837.value
          else
            withdraw.value.challenge_tokens[token.key] <- 0 - (to_int game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key])
      else
        if not (withdraw.value.challenge.contains(game_id)) then
          withdraw.value.challenge.add(game_id);
          set_type (withdraw.value.challenge_tokens : map(nat, int));
          set_type (game.value.constants.bonds[game.value.addr_players[params.withdrawer]] : map(nat, nat));
          sp.for token in game.value.constants.bonds[game.value.addr_players[params.withdrawer]].items():
            if withdraw.value.challenge_tokens.contains(token.key) then
              withdraw.value.challenge_tokens[token.key] += to_int game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]
            else
              if not ((to_int game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]) = 0) then
                withdraw.value.challenge_tokens[token.key] <- to_int game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key];
    if not (params.withdrawer = sender) then
      withdraw.value.timeout <- now;
    channel.value.players[params.withdrawer].withdraw <- some withdraw.value;
    self.data.channels[params.channel_id] <- channel.value

  let%entry_point withdraw_finalise self params =
    channel = sp.local("channel", self.data.channels.get(params, message = ("Platform_ChannelNotFound: ", params)));
    verify (not channel.value.closed) "Platform_ChannelClosed";
    verify (channel.value.players.contains(sender)) "Platform_SenderNotInChannel";
    player = sp.local("player", channel.value.players[sender]);
    verify player.value.withdraw.is_some() "Platform_NoWithdrawOpened";
    withdraw = sp.local("withdraw", player.value.withdraw.open_some());
    verify (now >= withdraw.value.timeout) "Platform_ChallengeDelayNotOver";
    sp.for token in withdraw.value.challenge_tokens.items():
      if (token.value > 0) & (player.value.bonds.contains(token.key)) then
        verify ((player.value.bonds[token.key] - withdraw.value.tokens[token.key]) > token.value) "Platform_TokenChallenged";
    sp.for token in withdraw.value.tokens.items():
      if token.value > 0 then
        verify (player.value.bonds.contains(token.key)) "Platform_NotEnoughTokens";
        player.value.bonds[token.key] <- sp.as_nat(player.value.bonds[token.key] - token.value, message = "Platform_NotEnoughTokens");
        if self.data.ledger.contains((sender, token.key)) then
          self.data.ledger[(sender, token.key)] <- {balance = token.value + self.data.ledger[(sender, token.key)].balance}
        else
          self.data.ledger[(sender, token.key)] <- {balance = token.value};
    player.value.withdraw <- none;
    channel.value.players[sender] <- player.value;
    self.data.channels[params] <- channel.value

  let%entry_point withdraw_ledger self params =
    set_type (params.tokens : map(nat, nat));
    sp.for token in params.tokens.items():
      if token.key = 0 then
        sp.send(params.receiver, mul token.value sp.mutez(1))
      else
        compute_game_platform_720 = sp.local("compute_game_platform_720", self.data.token_permissions.get(token.key, message = ("Platform_TokenNotFound", {token = token.key})));
        verify (compute_game_platform_720.value.contains("type")) ("Platform_TokenTypeNotSetup", token.key);
        compute_game_platform_722 = sp.local("compute_game_platform_722", unpack compute_game_platform_720.value["type"] string.open_some());
        verify (compute_game_platform_722.value = "FA2") ("Platform_TokenCannotBeWithdrawn", token.key);
        verify (compute_game_platform_720.value.contains("fa2_address")) ("Platform_TokenFA2NotSetup", token.key);
        sp.transfer(sp.list([{from_ = self_address; txs = sp.list([{to_ = sender; token_id = unpack compute_game_platform_720.value["fa2_token_id"] nat.open_some(); amount = token.value}])}]), sp.tez(0), sp.contract(sp.TRecord(from_ = address, txs = sp.TRecord(amount = nat, to_ = address, token_id = nat).layout(("to_", ("token_id", "amount"))) list).layout(("from_", "txs")) list, unpack compute_game_platform_720.value["fa2_address"] address.open_some(), entry_point='transfer').open_some(message = "Plateform_FA2Unreacheable"));
        token_permissions = sp.local("token_permissions", self.data.token_permissions.get(token.key, message = ("Platform_TokenNotFound", {token = token.key})));
        verify (token_permissions.value.contains("supply")) ("Platform_NoSupply", {token = token.key});
        self.data.token_permissions[token.key]["supply"] <- pack sp.as_nat(unpack self.data.token_permissions[token.key]["supply"] nat.open_some() - token.value, message = ("Platform_NotEnoughtTokenSupply", {amount = token.value; token = token.key}));
      verify ((self.data.ledger.contains((sender, token.key))) | (token.value = 0)) ("Platform_NotEnoughtToken", {token_id = token.key});
      self.data.ledger[(sender, token.key)].balance <- sp.as_nat(self.data.ledger[(sender, token.key)].balance - token.value, message = ("Platform_NotEnoughtToken", {token_id = token.key}))

  let%entry_point withdraw_request self params =
    channel = sp.local("channel", self.data.channels.get(params.channel_id, message = ("Platform_ChannelNotFound: ", params.channel_id)));
    verify (not channel.value.closed) "Platform_ChannelClosed";
    player = sp.local("player", channel.value.players.get(sender, message = "Platform_SenderNotInChannel"));
    if player.value.withdraw.is_some() then
      verify (now >= player.value.withdraw.open_some().timeout) "Platform_ChallengeDelayNotOver";
    player.value.withdraw_id += 1;
    player.value.withdraw <- some {challenge = sp.set([]); challenge_tokens = {}; timeout = add_seconds now channel.value.withdraw_delay; tokens = params.tokens};
    channel.value.players[sender] <- player.value;
    self.data.channels[params.channel_id] <- channel.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(admins = sp.TSet(address), channels = bigMap(bytes, sp.TRecord(closed = bool, nonce = string, players = map(address, sp.TRecord(bonds = map(nat, nat), pk = key, withdraw = sp.TRecord(challenge = sp.TSet(bytes), challenge_tokens = map(nat, int), timeout = timestamp, tokens = map(nat, nat)).layout(("challenge", ("challenge_tokens", ("timeout", "tokens")))) option, withdraw_id = nat).layout(("bonds", ("pk", ("withdraw", "withdraw_id"))))), withdraw_delay = int).layout(("closed", ("nonce", ("players", "withdraw_delay"))))), games = bigMap(bytes, sp.TRecord(addr_players = map(address, int), constants = sp.TRecord(bonds = map(int, map(nat, nat)), channel_id = bytes, game_nonce = string, model_id = bytes, play_delay = int, players_addr = map(int, address), settlements = map(sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TRecord(bonds = map(nat, nat), receiver = int, sender = int).layout(("bonds", ("receiver", "sender"))) list)).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), current = sp.TRecord(move_nb = nat, outcome = sp.TVariant(game_finished = string, player_double_played = int, player_inactive = int).layout(("game_finished", ("player_double_played", "player_inactive"))) option, player = int).layout(("move_nb", ("outcome", "player"))), init_input = bytes, metadata = map(string, bytes), settled = bool, state = bytes option, timeouts = map(int, timestamp)).layout(("addr_players", ("constants", ("current", ("init_input", ("metadata", ("settled", ("state", "timeouts"))))))))), ledger = bigMap((address * nat), sp.TRecord(balance = nat).layout("balance")), metadata = bigMap(string, bytes), models = bigMap(bytes, sp.TRecord(apply_ = sp.TLambda(sp.TRecord(move_data = bytes, move_nb = nat, player = int, state = bytes).layout(("move_data", ("move_nb", ("player", "state")))), (bytes * string option)), init = sp.TLambda(bytes, bytes), metadata = map(string, bytes), outcomes = string list).layout(("apply_", ("init", ("metadata", "outcomes"))))), token_metadata = bigMap(nat, map(string, bytes)), token_permissions = bigMap(nat, map(string, bytes))).layout(("admins", ("channels", ("games", ("ledger", ("metadata", ("models", ("token_metadata", "token_permissions"))))))))
      ~storage:[%expr
                 {admins = sp.set([sp.address('tz1edhNSiBfAcXGs3QXfmgZyrTfNW15rxKNP')]),
                  channels = {},
                  games = {},
                  ledger = {},
                  metadata = {},
                  models = {},
                  token_metadata = {},
                  token_permissions = {0 : {'type' : sp.bytes('0x0501000000066e6174697665')}}}]
      [add_admins; channel_push_bonds; dispute_double_signed; dispute_starved; dispute_starving; game_play; game_set_outcome; game_set_state; game_settle; ledger_to_bonds; new_channel; new_game; new_model; remove_admins; set_token_metadata; set_token_permissions; update_game_metadata; update_global_metadata; update_model_metadata; withdraw_cancel; withdraw_challenge; withdraw_finalise; withdraw_ledger; withdraw_request]
end