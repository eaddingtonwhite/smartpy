import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admins = sp.TSet(sp.TAddress), channels = sp.TBigMap(sp.TBytes, sp.TRecord(closed = sp.TBool, nonce = sp.TString, players = sp.TMap(sp.TAddress, sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), pk = sp.TKey, withdraw = sp.TOption(sp.TRecord(challenge = sp.TSet(sp.TBytes), challenge_tokens = sp.TMap(sp.TNat, sp.TInt), timeout = sp.TTimestamp, tokens = sp.TMap(sp.TNat, sp.TNat)).layout(("challenge", ("challenge_tokens", ("timeout", "tokens"))))), withdraw_id = sp.TNat).layout(("bonds", ("pk", ("withdraw", "withdraw_id"))))), withdraw_delay = sp.TInt).layout(("closed", ("nonce", ("players", "withdraw_delay"))))), games = sp.TBigMap(sp.TBytes, sp.TRecord(addr_players = sp.TMap(sp.TAddress, sp.TInt), constants = sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), current = sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive")))), player = sp.TInt).layout(("move_nb", ("outcome", "player"))), init_input = sp.TBytes, metadata = sp.TMap(sp.TString, sp.TBytes), settled = sp.TBool, state = sp.TOption(sp.TBytes), timeouts = sp.TMap(sp.TInt, sp.TTimestamp)).layout(("addr_players", ("constants", ("current", ("init_input", ("metadata", ("settled", ("state", "timeouts"))))))))), ledger = sp.TBigMap(sp.TPair(sp.TAddress, sp.TNat), sp.TRecord(balance = sp.TNat).layout("balance")), metadata = sp.TBigMap(sp.TString, sp.TBytes), models = sp.TBigMap(sp.TBytes, sp.TRecord(apply_ = sp.TLambda(sp.TRecord(move_data = sp.TBytes, move_nb = sp.TNat, player = sp.TInt, state = sp.TBytes).layout(("move_data", ("move_nb", ("player", "state")))), sp.TPair(sp.TBytes, sp.TOption(sp.TString))), init = sp.TLambda(sp.TBytes, sp.TBytes), metadata = sp.TMap(sp.TString, sp.TBytes), outcomes = sp.TList(sp.TString)).layout(("apply_", ("init", ("metadata", "outcomes"))))), token_metadata = sp.TBigMap(sp.TNat, sp.TMap(sp.TString, sp.TBytes)), token_permissions = sp.TBigMap(sp.TNat, sp.TMap(sp.TString, sp.TBytes))).layout(("admins", ("channels", ("games", ("ledger", ("metadata", ("models", ("token_metadata", "token_permissions")))))))))
    self.init(admins = sp.set([sp.address('tz1edhNSiBfAcXGs3QXfmgZyrTfNW15rxKNP')]),
              channels = {},
              games = {},
              ledger = {},
              metadata = {},
              models = {},
              token_metadata = {},
              token_permissions = {0 : {'type' : sp.bytes('0x0501000000066e6174697665')}})

  @sp.entry_point
  def add_admins(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    sp.for admin in params.elements():
      self.data.admins.add(admin)

  @sp.entry_point
  def channel_push_bonds(self, params):
    sp.verify(~ self.data.channels.get(params.channel_id, message = ('Platform_ChannelNotFound: ', params.channel_id)).closed, 'Platform_ChannelClosed')
    player = sp.local("player", self.data.channels.get(params.channel_id, message = ('Platform_ChannelNotFound: ', params.channel_id)).players.get(params.player_addr, message = 'Platform_NotChannelPlayer'))
    sp.for token in params.bonds.items():
      sp.if token.key == 0:
        sp.verify(sp.amount == sp.mul(token.value, sp.mutez(1)), ('Platform_WrongMutezAmount_Expected', sp.mul(token.value, sp.mutez(1))))
      sp.else:
        compute_game_platform_304 = sp.local("compute_game_platform_304", self.data.token_permissions.get(token.key, message = ('Platform_TokenNotFound', token.key)))
        sp.verify(compute_game_platform_304.value.contains('type'), ('Platform_TokenTypeNotSetup', token.key))
        compute_game_platform_306 = sp.local("compute_game_platform_306", sp.unpack(compute_game_platform_304.value['type'], sp.TString).open_some())
        sp.if compute_game_platform_306.value == 'FA2':
          sp.verify(compute_game_platform_304.value.contains('fa2_address'), ('Platform_Token_NotFA2Setup', token.value))
          sp.transfer(sp.list([sp.record(from_ = sp.sender, txs = sp.list([sp.record(to_ = sp.self_address, token_id = sp.unpack(compute_game_platform_304.value['fa2_token_id'], sp.TNat).open_some(), amount = token.value)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), sp.unpack(compute_game_platform_304.value['fa2_address'], sp.TAddress).open_some(), entry_point='transfer').open_some(message = 'Plateform_FA2Unreacheable'))
        sp.else:
          sp.verify(compute_game_platform_306.value == 'FREE', ('Platform_TokenCannotBeRecived', token.key))
          sp.verify(token.value < 1000000000000000000, ('Platform_AmountExceeded', token.key))
        token_permissions = sp.local("token_permissions", self.data.token_permissions.get(token.key, message = ('Platform_TokenNotFound', sp.record(token = token.key))))
        supply = sp.local("supply", 0)
        sp.if token_permissions.value.contains('supply'):
          supply.value = sp.unpack(token_permissions.value['supply'], sp.TNat).open_some()
        supply.value += token.value
        sp.if token_permissions.value.contains('max_supply'):
          sp.verify(supply.value <= sp.unpack(token_permissions.value['max_supply'], sp.TNat).open_some(), ('Platform_TokenSupplyExceed', sp.record(amount = token.value, token = token.key)))
        self.data.token_permissions[token.key]['supply'] = sp.pack(supply.value)
      sp.if player.value.bonds.contains(token.key):
        player.value.bonds[token.key] += token.value
      sp.else:
        player.value.bonds[token.key] = token.value
    self.data.channels[params.channel_id].players[params.player_addr] = player.value

  @sp.entry_point
  def dispute_double_signed(self, params):
    sp.set_type(params.statement1, sp.TRecord(sig = sp.TSignature, state = sp.TBytes).layout(("sig", "state")))
    sp.set_type(params.statement2, sp.TOption(sp.TRecord(current = sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive")))), player = sp.TInt).layout(("move_nb", ("outcome", "player"))), sig = sp.TSignature, state = sp.TBytes).layout(("current", ("sig", "state")))))
    game = sp.local("game", self.data.games.get(params.game_id, message = ('Platform_GameNotFound: ', params.game_id)))
    sp.verify(game.value.current.outcome == sp.none, 'Platform_NotRunning')
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    player = sp.local("player", self.data.channels[game.value.constants.channel_id].players[game.value.constants.players_addr[params.player_id]])
    sp.if params.statement2.is_variant('None'):
      sp.verify(sp.check_signature(player.value.pk, params.statement1.sig, sp.pack(('New State', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(game.value.current, sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive")))), player = sp.TInt).layout(("move_nb", ("outcome", "player")))), sp.set_type_expr(params.statement1.state, sp.TBytes)))), 'Platform_badSig')
      sp.verify(~ (params.statement1.state == game.value.state.open_some(message = 'Plateform_GameNotInitialized')), 'Platform_NotDifferentStates')
    sp.else:
      statement2 = sp.local("statement2", params.statement2.open_some())
      sp.verify(sp.check_signature(player.value.pk, params.statement1.sig, sp.pack(('New State', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(statement2.value.current, sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive")))), player = sp.TInt).layout(("move_nb", ("outcome", "player")))), sp.set_type_expr(params.statement1.state, sp.TBytes)))), 'Platform_badSig')
      sp.verify(sp.check_signature(player.value.pk, statement2.value.sig, sp.pack(('New State', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(statement2.value.current, sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive")))), player = sp.TInt).layout(("move_nb", ("outcome", "player")))), sp.set_type_expr(statement2.value.state, sp.TBytes)))), 'Platform_badSig')
      sp.verify(~ (params.statement1.state == statement2.value.state), 'Platform_NotSameStates')
    game.value.current.outcome = sp.some(variant('player_double_played', params.player_id))
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def dispute_starved(self, params):
    game = sp.local("game", self.data.games.get(params.game_id, message = ('Platform_GameNotFound: ', params.game_id)))
    sp.verify(game.value.current.outcome == sp.none, 'Platform_NotRunning')
    sp.verify(game.value.timeouts.contains(params.player_id), 'Platform_NoTimeoutSetup')
    sp.verify(sp.now > game.value.timeouts[params.player_id], 'Platform_NotTimedOut')
    game.value.current.outcome = sp.some(variant('player_inactive', params.player_id))
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def dispute_starving(self, params):
    game = sp.local("game", self.data.games.get(params.game_id, message = ('Platform_GameNotFound: ', params.game_id)))
    sp.verify(game.value.current.outcome == sp.none, 'Platform_NotRunning')
    sp.if params.flag:
      game.value.timeouts[3 - game.value.addr_players[sp.sender]] = sp.add_seconds(sp.now, game.value.constants.play_delay)
    sp.else:
      del game.value.timeouts[3 - game.value.addr_players[sp.sender]]
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def game_play(self, params):
    game = sp.local("game", self.data.games.get(params.game_id, message = ('Platform_GameNotFound: ', params.game_id)))
    sp.verify(game.value.current.outcome == sp.none, 'Platform_NotRunning')
    sp.set_type(params.move_nb, sp.TNat)
    sp.set_type(params.move_data, sp.TBytes)
    sp.set_type(game.value.constants.players_addr, sp.TMap(sp.TInt, sp.TAddress))
    sp.verify(sp.sender == game.value.constants.players_addr[game.value.current.player], 'Platform_GameWrongPlayer')
    sp.verify(game.value.current.move_nb == params.move_nb, 'Platform_Wrongmove_nb')
    sp.verify(game.value.current.outcome == sp.none, 'Platform_GameNotRunning')
    model = sp.local("model", self.data.models.get(game.value.constants.model_id, message = ('Platform_ModelNotFound: ', game.value.constants.model_id)))
    sp.if ~ game.value.state.is_some():
      game.value.state = sp.some(model.value.init(game.value.init_input))
    compute_game_platform_429 = sp.local("compute_game_platform_429", model.value.apply_(sp.record(move_data = params.move_data, move_nb = game.value.current.move_nb, player = game.value.current.player, state = game.value.state.open_some())))
    game.value.current = sp.record(move_nb = game.value.current.move_nb + 1, outcome = sp.eif(sp.snd(compute_game_platform_429.value).is_some(), sp.some(variant('game_finished', sp.snd(compute_game_platform_429.value).open_some())), sp.none), player = 3 - game.value.current.player)
    game.value.state = sp.some(sp.fst(compute_game_platform_429.value))
    sp.if game.value.timeouts.contains(game.value.current.player):
      game.value.timeouts[game.value.current.player] = sp.add_seconds(sp.now, game.value.constants.play_delay)
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def game_set_outcome(self, params):
    sp.set_type(params.signatures, sp.TMap(sp.TKey, sp.TSignature))
    sp.verify(sp.now <= params.timeout, 'Platform_OutcomeTimedout')
    game = sp.local("game", self.data.games.get(params.game_id, message = ('Platform_GameNotFound: ', params.game_id)))
    sp.verify(game.value.current.outcome == sp.none, 'Platform_NotRunning')
    compute_game_platform_449 = sp.local("compute_game_platform_449", sp.pack(('New Outcome', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(params.outcome, sp.TString), params.timeout)))
    sp.verify(~ self.data.channels.get(game.value.constants.channel_id, message = ('Platform_ChannelNotFound: ', game.value.constants.channel_id)).closed, 'Platform_ChannelClosed')
    players = sp.local("players", self.data.channels.get(game.value.constants.channel_id, message = ('Platform_ChannelNotFound: ', game.value.constants.channel_id)).players)
    sp.for player in players.value.values():
      sp.verify(params.signatures.contains(player.pk), 'Platform_MissingSig')
      sp.verify(sp.check_signature(player.pk, params.signatures[player.pk], compute_game_platform_449.value), 'Platform_badSig')
    game.value.current.outcome = sp.some(variant('game_finished', params.outcome))
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def game_set_state(self, params):
    sp.set_type(params.signatures, sp.TMap(sp.TKey, sp.TSignature))
    game = sp.local("game", self.data.games.get(params.game_id, message = ('Platform_GameNotFound: ', params.game_id)))
    sp.verify(game.value.current.outcome == sp.none, 'Platform_NotRunning')
    sp.verify(params.new_current.move_nb > game.value.current.move_nb, 'Platform_OutdatedMoveNB')
    compute_game_platform_464 = sp.local("compute_game_platform_464", sp.pack(('New State', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(params.new_current, sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive")))), player = sp.TInt).layout(("move_nb", ("outcome", "player")))), sp.set_type_expr(params.new_state, sp.TBytes))))
    channel = sp.local("channel", self.data.channels.get(game.value.constants.channel_id, message = ('Platform_ChannelNotFound: ', game.value.constants.channel_id)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.for player in channel.value.players.values():
      sp.verify(sp.check_signature(player.pk, params.signatures[player.pk], compute_game_platform_464.value), ('Platform_badSig', player.pk))
    game.value.current = params.new_current
    game.value.state = sp.some(params.new_state)
    sp.if game.value.timeouts.contains(game.value.current.player):
      game.value.timeouts[game.value.current.player] = sp.add_seconds(sp.now, game.value.constants.play_delay)
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def game_settle(self, params):
    game = sp.local("game", self.data.games.get(params, message = ('Platform_GameNotFound: ', params)))
    model = sp.local("model", sp.eif(self.data.models.contains(game.value.constants.model_id), sp.some(self.data.models[game.value.constants.model_id]), sp.none))
    sp.verify(~ game.value.settled, 'Plateform_GameSettled')
    allowed_mint_model = sp.local("allowed_mint_model", {})
    allowed_mint_game = sp.local("allowed_mint_game", {})
    sp.if model.value.is_some():
      sp.if model.value.open_some().metadata.contains('allowed_mint'):
        allowed_mint_model.value = sp.unpack(model.value.open_some().metadata['allowed_mint'], sp.TMap(sp.TNat, sp.TNat)).open_some()
    sp.if game.value.metadata.contains('allowed_mint'):
      allowed_mint_game.value = sp.unpack(game.value.metadata['allowed_mint'], sp.TMap(sp.TNat, sp.TNat)).open_some()
    sp.if game.value.constants.settlements.contains(game.value.current.outcome.open_some(message = 'Plateform_GameIsntOver')):
      compute_game_platform_523 = sp.local("compute_game_platform_523", game.value.constants.settlements[game.value.current.outcome.open_some()])
      channel = sp.local("channel", self.data.channels.get(game.value.constants.channel_id, message = ('Platform_ChannelNotFound: ', game.value.constants.channel_id)))
      sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
      sp.for transfer in compute_game_platform_523.value:
        sp.if transfer.sender == 0:
          compute_game_platform_253 = sp.local("compute_game_platform_253", game.value.constants.players_addr[transfer.receiver])
          receiver = sp.local("receiver", channel.value.players[compute_game_platform_253.value])
          sp.for tokens in transfer.bonds.items():
            delta = sp.local("delta", allowed_mint_game.value.get(tokens.key, default_value = 0) - tokens.value)
            allowed_mint_game.value[tokens.key] = sp.eif(delta.value > 0, sp.as_nat(delta.value), 0)
            sp.if delta.value < 0:
              allowed_mint_model.value[tokens.key] = sp.as_nat(sp.to_int(allowed_mint_model.value.get(tokens.key, default_value = 0)) + delta.value, message = ('Platform_NotAllowedMinting', sp.record(amount = tokens.value, game_id = params, token = tokens.key)))
            token_permissions = sp.local("token_permissions", self.data.token_permissions.get(tokens.key, message = ('Platform_TokenNotFound', sp.record(token = tokens.key))))
            supply = sp.local("supply", 0)
            sp.if token_permissions.value.contains('supply'):
              supply.value = sp.unpack(token_permissions.value['supply'], sp.TNat).open_some()
            supply.value += tokens.value
            sp.if token_permissions.value.contains('max_supply'):
              sp.verify(supply.value <= sp.unpack(token_permissions.value['max_supply'], sp.TNat).open_some(), ('Platform_TokenSupplyExceed', sp.record(amount = tokens.value, token = tokens.key)))
            self.data.token_permissions[tokens.key]['supply'] = sp.pack(supply.value)
            receiver.value.bonds[tokens.key] = receiver.value.bonds.get(tokens.key, default_value = 0) + tokens.value
          channel.value.players[compute_game_platform_253.value] = receiver.value
        sp.else:
          compute_game_platform_281 = sp.local("compute_game_platform_281", game.value.constants.players_addr[transfer.sender])
          compute_game_platform_282 = sp.local("compute_game_platform_282", game.value.constants.players_addr[transfer.receiver])
          sp.if ~ (compute_game_platform_281.value == compute_game_platform_282.value):
            sender = sp.local("sender", channel.value.players[compute_game_platform_281.value])
            receiver = sp.local("receiver", channel.value.players[compute_game_platform_282.value])
            sp.for bond in transfer.bonds.items():
              sp.if ~ (bond.value == 0):
                sp.verify(sender.value.bonds.contains(bond.key), ('Platform_NotEnoughToken:', sp.record(amount = bond.value, sender = compute_game_platform_281.value, token = bond.key)))
                sender.value.bonds[bond.key] = sp.as_nat(sender.value.bonds[bond.key] - bond.value, message = ('Platform_NotEnoughToken:', sp.record(amount = bond.value, sender = compute_game_platform_281.value, token = bond.key)))
                sp.if sender.value.bonds[bond.key] == 0:
                  del sender.value.bonds[bond.key]
                receiver.value.bonds[bond.key] = receiver.value.bonds.get(bond.key, default_value = 0) + bond.value
            channel.value.players[compute_game_platform_281.value] = sender.value
            channel.value.players[compute_game_platform_282.value] = receiver.value
      self.data.channels[game.value.constants.channel_id].players = channel.value.players
    game.value.settled = True
    self.data.games[params] = game.value
    sp.if sp.len(allowed_mint_model.value) > 0:
      self.data.models[game.value.constants.model_id].metadata['allowed_mint'] = sp.pack(allowed_mint_model.value)
    sp.else:
      sp.if model.value.is_some():
        del self.data.models[game.value.constants.model_id].metadata['allowed_mint']

  @sp.entry_point
  def ledger_to_bonds(self, params):
    channel = sp.local("channel", self.data.channels.get(params.channel_id, message = ('Platform_ChannelNotFound: ', params.channel_id)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    to = sp.local("to", channel.value.players.get(params.receiver, message = 'Platform_ReceiverNotChannelPlayer'))
    sp.for token in params.tokens.items():
      sp.if token.value > 0:
        sp.verify(self.data.ledger.contains((sp.sender, token.key)), 'Platform_NotEnoughToken')
        self.data.ledger[(sp.sender, token.key)] = sp.record(balance = sp.as_nat(self.data.ledger[(sp.sender, token.key)].balance - token.value, message = 'Platform_NotEnoughToken'))
        sp.if to.value.bonds.contains(token.key):
          to.value.bonds[token.key] += token.value
        sp.else:
          to.value.bonds[token.key] = token.value
    self.data.channels[params.channel_id].players[params.receiver] = to.value

  @sp.entry_point
  def new_channel(self, params):
    sp.verify(~ (self.data.channels.contains(sp.blake2b(sp.pack((sp.self_address, params.players, params.nonce))))), 'Platform_ChannelAlreadyExists')
    sp.verify(sp.len(params.players) == 2, 'Platform_Only2PlayersAllowed')
    players_map = sp.local("players_map", {})
    sp.for player in params.players.items():
      players_map.value[player.key] = sp.record(bonds = {}, pk = player.value, withdraw = sp.none, withdraw_id = 0)
    self.data.channels[sp.blake2b(sp.pack((sp.self_address, params.players, params.nonce)))] = sp.record(closed = False, nonce = params.nonce, players = players_map.value, withdraw_delay = params.withdraw_delay)

  @sp.entry_point
  def new_game(self, params):
    sp.set_type(params.constants, sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))))
    sp.set_type(params.params, sp.TBytes)
    sp.set_type(params.signatures, sp.TMap(sp.TKey, sp.TSignature))
    sp.verify(~ (self.data.games.contains(sp.blake2b(sp.pack((params.constants.channel_id, (params.constants.model_id, params.constants.game_nonce)))))), 'Platform_GameAlreadyExists')
    sp.verify(sp.len(params.constants.players_addr) == 2, 'Platform_Only2PlayersAllowed')
    channel = sp.local("channel", self.data.channels.get(params.constants.channel_id, message = ('Platform_ChannelNotFound: ', params.constants.channel_id)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    addr_players = sp.local("addr_players", {})
    sp.for addr_player in params.constants.players_addr.items():
      addr_players.value[addr_player.value] = addr_player.key
      sp.verify(channel.value.players.contains(addr_player.value), 'Platform_GamePlayerNotInChannel')
    sp.for player in channel.value.players.values():
      sp.verify(params.signatures.contains(player.pk), 'Platform_MissingSig')
      sp.verify(sp.check_signature(player.pk, params.signatures[player.pk], sp.pack(('New Game', sp.set_type_expr(params.constants, sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements")))))))), sp.set_type_expr(params.params, sp.TBytes)))), ('Platform_BadSig', sp.record(player = player.pk, signature = params.signatures[player.pk])))
    self.data.games[sp.blake2b(sp.pack((params.constants.channel_id, (params.constants.model_id, params.constants.game_nonce))))] = sp.set_type_expr(sp.record(addr_players = addr_players.value, constants = params.constants, current = sp.record(move_nb = 0, outcome = sp.none, player = 1), init_input = params.params, metadata = {}, settled = False, state = sp.none, timeouts = {}), sp.TRecord(addr_players = sp.TMap(sp.TAddress, sp.TInt), constants = sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive"))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), current = sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_finished", ("player_double_played", "player_inactive")))), player = sp.TInt).layout(("move_nb", ("outcome", "player"))), init_input = sp.TBytes, metadata = sp.TMap(sp.TString, sp.TBytes), settled = sp.TBool, state = sp.TOption(sp.TBytes), timeouts = sp.TMap(sp.TInt, sp.TTimestamp)).layout(("addr_players", ("constants", ("current", ("init_input", ("metadata", ("settled", ("state", "timeouts")))))))))

  @sp.entry_point
  def new_model(self, params):
    sp.set_type(params.name, sp.TString)
    compute_game_platform_342 = sp.local("compute_game_platform_342", sp.blake2b(params.model))
    sp.if ~ (self.data.models.contains(compute_game_platform_342.value)):
      compute_game_platform_344 = sp.local("compute_game_platform_344", sp.unpack(params.model, sp.TRecord(apply_ = sp.TLambda(sp.TRecord(move_data = sp.TBytes, move_nb = sp.TNat, player = sp.TInt, state = sp.TBytes).layout(("move_data", ("move_nb", ("player", "state")))), sp.TPair(sp.TBytes, sp.TOption(sp.TString))), init = sp.TLambda(sp.TBytes, sp.TBytes), outcomes = sp.TList(sp.TString)).layout(("apply_", ("init", "outcomes")))).open_some(message = 'Platform_ModelUnpackFailure'))
      self.data.models[compute_game_platform_342.value] = sp.record(apply_ = compute_game_platform_344.value.apply_, init = compute_game_platform_344.value.init, metadata = sp.set_type_expr({'name' : sp.pack(params.name)}, sp.TMap(sp.TString, sp.TBytes)), outcomes = compute_game_platform_344.value.outcomes)

  @sp.entry_point
  def remove_admins(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    sp.verify(~ (params.contains(sp.sender)), 'Platform_CannotRemoveSelf')
    sp.for admin in params.elements():
      self.data.admins.remove(admin)

  @sp.entry_point
  def set_token_metadata(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    self.data.token_metadata[params.token_id] = params.metadata

  @sp.entry_point
  def set_token_permissions(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    self.data.token_permissions[params.token_id] = params.metadata

  @sp.entry_point
  def update_game_metadata(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    sp.verify(self.data.games.contains(params.game_id), 'Platform_GameNotFound')
    sp.set_type(params.metadata, sp.TOption(sp.TBytes))
    sp.set_type(params.key, sp.TString)
    sp.if params.metadata.is_some():
      self.data.games[params.game_id].metadata[params.key] = params.metadata.open_some()
    sp.else:
      del self.data.games[params.game_id].metadata[params.key]

  @sp.entry_point
  def update_global_metadata(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    sp.set_type(params.metadata, sp.TOption(sp.TBytes))
    sp.set_type(params.key, sp.TString)
    sp.if params.metadata.is_some():
      self.data.metadata[params.key] = params.metadata.open_some()
    sp.else:
      del self.data.metadata[params.key]

  @sp.entry_point
  def update_model_metadata(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    sp.verify(self.data.models.contains(params.model_id), 'Platform_ModelNotFound')
    sp.set_type(params.metadata, sp.TOption(sp.TBytes))
    sp.set_type(params.key, sp.TString)
    sp.if params.metadata.is_some():
      self.data.models[params.model_id].metadata[params.key] = params.metadata.open_some()
    sp.else:
      del self.data.models[params.model_id].metadata[params.key]

  @sp.entry_point
  def withdraw_cancel(self, params):
    channel = sp.local("channel", self.data.channels.get(params, message = ('Platform_ChannelNotFound: ', params)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(channel.value.players.contains(sp.sender), 'Platform_SenderNotInChannel')
    player = sp.local("player", channel.value.players[sp.sender])
    sp.verify(player.value.withdraw.is_some(), 'Platform_NoWithdrawOpened')
    sp.verify(sp.now >= player.value.withdraw.open_some().timeout, 'Platform_ChallengeDelayNotOver')
    channel.value.players[sp.sender].withdraw = sp.none
    self.data.channels[params] = channel.value

  @sp.entry_point
  def withdraw_challenge(self, params):
    sp.set_type(params.game_ids, sp.TSet(sp.TBytes))
    channel = sp.local("channel", self.data.channels.get(params.channel_id, message = ('Platform_ChannelNotFound: ', params.channel_id)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(channel.value.players.contains(sp.sender), 'Platform_SenderNotInChannel')
    sp.verify(channel.value.players.contains(params.withdrawer), 'Platform_WithdrawerNotInChannel')
    sp.verify(channel.value.players[params.withdrawer].withdraw.is_some(), 'Platform_NoWithdrawRequestOpened')
    withdraw = sp.local("withdraw", channel.value.players[params.withdrawer].withdraw.open_some())
    sp.for game_id in params.game_ids.elements():
      game = sp.local("game", self.data.games.get(game_id, message = ('Platform_GameNotFound: ', game_id)))
      sp.verify(game.value.constants.channel_id == params.channel_id, 'Platform_ChannelIdMismatch')
      sp.if game.value.settled:
        sp.verify(withdraw.value.challenge.contains(game_id), 'Platform_GameSettled')
        withdraw.value.challenge.remove(game_id)
        sp.set_type(withdraw.value.challenge_tokens, sp.TMap(sp.TNat, sp.TInt))
        sp.set_type(game.value.constants.bonds[game.value.addr_players[params.withdrawer]], sp.TMap(sp.TNat, sp.TNat))
        sp.for token in game.value.constants.bonds[game.value.addr_players[params.withdrawer]].items():
          sp.if withdraw.value.challenge_tokens.contains(token.key):
            compute_game_platform_837 = sp.local("compute_game_platform_837", withdraw.value.challenge_tokens[token.key] - sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]))
            sp.if compute_game_platform_837.value == 0:
              del withdraw.value.challenge_tokens[token.key]
            sp.else:
              withdraw.value.challenge_tokens[token.key] = compute_game_platform_837.value
          sp.else:
            withdraw.value.challenge_tokens[token.key] = 0 - sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key])
      sp.else:
        sp.if ~ (withdraw.value.challenge.contains(game_id)):
          withdraw.value.challenge.add(game_id)
          sp.set_type(withdraw.value.challenge_tokens, sp.TMap(sp.TNat, sp.TInt))
          sp.set_type(game.value.constants.bonds[game.value.addr_players[params.withdrawer]], sp.TMap(sp.TNat, sp.TNat))
          sp.for token in game.value.constants.bonds[game.value.addr_players[params.withdrawer]].items():
            sp.if withdraw.value.challenge_tokens.contains(token.key):
              withdraw.value.challenge_tokens[token.key] += sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key])
            sp.else:
              sp.if ~ (sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]) == 0):
                withdraw.value.challenge_tokens[token.key] = sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key])
    sp.if ~ (params.withdrawer == sp.sender):
      withdraw.value.timeout = sp.now
    channel.value.players[params.withdrawer].withdraw = sp.some(withdraw.value)
    self.data.channels[params.channel_id] = channel.value

  @sp.entry_point
  def withdraw_finalise(self, params):
    channel = sp.local("channel", self.data.channels.get(params, message = ('Platform_ChannelNotFound: ', params)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(channel.value.players.contains(sp.sender), 'Platform_SenderNotInChannel')
    player = sp.local("player", channel.value.players[sp.sender])
    sp.verify(player.value.withdraw.is_some(), 'Platform_NoWithdrawOpened')
    withdraw = sp.local("withdraw", player.value.withdraw.open_some())
    sp.verify(sp.now >= withdraw.value.timeout, 'Platform_ChallengeDelayNotOver')
    sp.for token in withdraw.value.challenge_tokens.items():
      sp.if (token.value > 0) & (player.value.bonds.contains(token.key)):
        sp.verify((player.value.bonds[token.key] - withdraw.value.tokens[token.key]) > token.value, 'Platform_TokenChallenged')
    sp.for token in withdraw.value.tokens.items():
      sp.if token.value > 0:
        sp.verify(player.value.bonds.contains(token.key), 'Platform_NotEnoughTokens')
        player.value.bonds[token.key] = sp.as_nat(player.value.bonds[token.key] - token.value, message = 'Platform_NotEnoughTokens')
        sp.if self.data.ledger.contains((sp.sender, token.key)):
          self.data.ledger[(sp.sender, token.key)] = sp.record(balance = token.value + self.data.ledger[(sp.sender, token.key)].balance)
        sp.else:
          self.data.ledger[(sp.sender, token.key)] = sp.record(balance = token.value)
    player.value.withdraw = sp.none
    channel.value.players[sp.sender] = player.value
    self.data.channels[params] = channel.value

  @sp.entry_point
  def withdraw_ledger(self, params):
    sp.set_type(params.tokens, sp.TMap(sp.TNat, sp.TNat))
    sp.for token in params.tokens.items():
      sp.if token.key == 0:
        sp.send(params.receiver, sp.mul(token.value, sp.mutez(1)))
      sp.else:
        compute_game_platform_720 = sp.local("compute_game_platform_720", self.data.token_permissions.get(token.key, message = ('Platform_TokenNotFound', sp.record(token = token.key))))
        sp.verify(compute_game_platform_720.value.contains('type'), ('Platform_TokenTypeNotSetup', token.key))
        compute_game_platform_722 = sp.local("compute_game_platform_722", sp.unpack(compute_game_platform_720.value['type'], sp.TString).open_some())
        sp.verify(compute_game_platform_722.value == 'FA2', ('Platform_TokenCannotBeWithdrawn', token.key))
        sp.verify(compute_game_platform_720.value.contains('fa2_address'), ('Platform_TokenFA2NotSetup', token.key))
        sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = sp.list([sp.record(to_ = sp.sender, token_id = sp.unpack(compute_game_platform_720.value['fa2_token_id'], sp.TNat).open_some(), amount = token.value)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), sp.unpack(compute_game_platform_720.value['fa2_address'], sp.TAddress).open_some(), entry_point='transfer').open_some(message = 'Plateform_FA2Unreacheable'))
        token_permissions = sp.local("token_permissions", self.data.token_permissions.get(token.key, message = ('Platform_TokenNotFound', sp.record(token = token.key))))
        sp.verify(token_permissions.value.contains('supply'), ('Platform_NoSupply', sp.record(token = token.key)))
        self.data.token_permissions[token.key]['supply'] = sp.pack(sp.as_nat(sp.unpack(self.data.token_permissions[token.key]['supply'], sp.TNat).open_some() - token.value, message = ('Platform_NotEnoughtTokenSupply', sp.record(amount = token.value, token = token.key))))
      sp.verify((self.data.ledger.contains((sp.sender, token.key))) | (token.value == 0), ('Platform_NotEnoughtToken', sp.record(token_id = token.key)))
      self.data.ledger[(sp.sender, token.key)].balance = sp.as_nat(self.data.ledger[(sp.sender, token.key)].balance - token.value, message = ('Platform_NotEnoughtToken', sp.record(token_id = token.key)))

  @sp.entry_point
  def withdraw_request(self, params):
    channel = sp.local("channel", self.data.channels.get(params.channel_id, message = ('Platform_ChannelNotFound: ', params.channel_id)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    player = sp.local("player", channel.value.players.get(sp.sender, message = 'Platform_SenderNotInChannel'))
    sp.if player.value.withdraw.is_some():
      sp.verify(sp.now >= player.value.withdraw.open_some().timeout, 'Platform_ChallengeDelayNotOver')
    player.value.withdraw_id += 1
    player.value.withdraw = sp.some(sp.record(challenge = sp.set([]), challenge_tokens = {}, timeout = sp.add_seconds(sp.now, channel.value.withdraw_delay), tokens = params.tokens))
    channel.value.players[sp.sender] = player.value
    self.data.channels[params.channel_id] = channel.value