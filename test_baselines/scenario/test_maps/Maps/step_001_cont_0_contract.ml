open Smartml

module Contract = struct

  let%entry_point test_get_and_update self () =
    match_pair_test_maps_46_fst, match_pair_test_maps_46_snd = sp.match_tuple(sp.get_and_update(self.data.m, 1, some "one"), "match_pair_test_maps_46_fst", "match_pair_test_maps_46_snd")
    self.data.z <- match_pair_test_maps_46_fst

  let%entry_point test_map_get self params =
    set_type (params : map(int, string));
    self.data.x <- params[12]

  let%entry_point test_map_get2 self params =
    set_type (params : map(int, string));
    self.data.x <- params[12]

  let%entry_point test_map_get_default_values self params =
    set_type (params : map(int, string));
    self.data.x <- params.get(12, default_value = "abc")

  let%entry_point test_map_get_missing_value self params =
    set_type (params : map(int, string));
    self.data.x <- params.get(12, message = "missing 12")

  let%entry_point test_map_get_missing_value2 self params =
    set_type (params : map(int, string));
    self.data.x <- params.get(12, message = 1234)

  let%entry_point test_map_get_opt self params =
    set_type (params : map(int, string));
    self.data.y <- params.get_opt(12)

  let%entry_point test_update_map self () =
    self.data.m[1] <- "one"

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(m = map(intOrNat, string), x = string, y = string option, z = string option).layout(("m", ("x", ("y", "z"))))
      ~storage:[%expr
                 {m = {},
                  x = 'a',
                  y = sp.none,
                  z = sp.some('na')}]
      [test_get_and_update; test_map_get; test_map_get2; test_map_get_default_values; test_map_get_missing_value; test_map_get_missing_value2; test_map_get_opt; test_update_map]
end