open Smartml

module Contract = struct

  let%entry_point log self params =
    verify (params <> 0);
    y = sp.local("y", 0);
    x = sp.local("x", params);
    sp.while x.value < 65536:
      x.value <- x.value << 1;
      y.value -= 65536;
    sp.while x.value >= 131072:
      x.value <- x.value >> 1;
      y.value += 65536;
    b = sp.local("b", 32768);
    sp.while b.value > 0:
      x.value <- (x.value * x.value) >> 16;
      if x.value > 131072 then
        x.value <- x.value >> 1;
        y.value += to_int b.value;
      b.value <- b.value >> 1;
    self.data.value <- y.value

  let init storage =
    Basics.build_contract
      ~tstorage:sp.TRecord(value = int).layout("value")
      ~storage:[%expr
                 {value = 0}]
      [log]
end