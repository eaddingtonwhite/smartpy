import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(addrVoterId = sp.TBigMap(sp.TAddress, sp.TNat), keyVoterId = sp.TBigMap(sp.TKey, sp.TNat), lastVoteTimestamp = sp.TTimestamp, lastVoterId = sp.TNat, metadata = sp.TBigMap(sp.TString, sp.TBytes), nbVoters = sp.TNat, proposals = sp.TBigMap(sp.TNat, sp.TRecord(batchs = sp.TList(sp.TVariant(selfAdmin = sp.TList(sp.TVariant(changeQuorum = sp.TNat, changeTarget = sp.TAddress, changeTimeout = sp.TInt, changeVoters = sp.TRecord(added = sp.TList(sp.TRecord(addr = sp.TAddress, publicKey = sp.TKey).layout(("addr", "publicKey"))), removed = sp.TSet(sp.TAddress)).layout(("added", "removed"))).layout((("changeQuorum", "changeTarget"), ("changeTimeout", "changeVoters")))), targetAdmin = sp.TList(sp.TVariant(changeActive = sp.TBool, changeAdmin = sp.TAddress, changeOracles = sp.TRecord(added = sp.TList(sp.TPair(sp.TAddress, sp.TRecord(adminAddress = sp.TAddress, endingRound = sp.TOption(sp.TNat), startingRound = sp.TNat).layout(("adminAddress", ("endingRound", "startingRound"))))), removed = sp.TList(sp.TAddress)).layout(("added", "removed")), updateFutureRounds = sp.TRecord(maxSubmissions = sp.TNat, minSubmissions = sp.TNat, oraclePayment = sp.TNat, restartDelay = sp.TNat, timeout = sp.TNat).layout((("maxSubmissions", "minSubmissions"), ("oraclePayment", ("restartDelay", "timeout"))))).layout((("changeActive", "changeAdmin"), ("changeOracles", "updateFutureRounds"))))).layout(("selfAdmin", "targetAdmin"))), canceled = sp.TBool, id = sp.TNat, nay = sp.TSet(sp.TNat), startedAt = sp.TTimestamp, yay = sp.TSet(sp.TNat)).layout((("batchs", ("canceled", "id")), ("nay", ("startedAt", "yay"))))), quorum = sp.TNat, target = sp.TOption(sp.TAddress), timeout = sp.TInt, voters = sp.TBigMap(sp.TNat, sp.TRecord(addr = sp.TAddress, lastProposalId = sp.TNat, publicKey = sp.TKey).layout(("addr", ("lastProposalId", "publicKey"))))).layout(((("addrVoterId", "keyVoterId"), ("lastVoteTimestamp", ("lastVoterId", "metadata"))), (("nbVoters", ("proposals", "quorum")), ("target", ("timeout", "voters"))))))
    self.init(addrVoterId = {sp.address('KT1_SIGNER1_ADDRESS') : 0},
              keyVoterId = {sp.key('KT1_SIGNER1_KEY') : 0},
              lastVoteTimestamp = sp.timestamp(0),
              lastVoterId = 0,
              metadata = {'' : sp.bytes('0x68747470733a2f2f636c6f7564666c6172652d697066732e636f6d2f697066732f516d57474c57783470475a4272564639427a313270415433413544756e77336f394e4d414b5676574b3531436679')},
              nbVoters = 1,
              proposals = {},
              quorum = 1,
              target = sp.none,
              timeout = 5,
              voters = {0 : sp.record(addr = sp.address('KT1_SIGNER1_ADDRESS'), lastProposalId = 0, publicKey = sp.key('KT1_SIGNER1_KEY'))})

  @sp.entry_point
  def cancelProposal(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), 'MultisignAdmin_VoterUnknown')
    sp.verify((self.data.proposals.contains(self.data.addrVoterId[sp.sender])) & (self.data.proposals[self.data.addrVoterId[sp.sender]].id == params), 'MultisignAdmin_ProposalUnknown')
    self.data.proposals[self.data.addrVoterId[sp.sender]].canceled = True

  @sp.entry_point
  def getLastProposal(self, params):
    sp.transfer(self.data.proposals[sp.fst(params)], sp.tez(0), sp.contract(sp.TRecord(batchs = sp.TList(sp.TVariant(selfAdmin = sp.TList(sp.TVariant(changeQuorum = sp.TNat, changeTarget = sp.TAddress, changeTimeout = sp.TInt, changeVoters = sp.TRecord(added = sp.TList(sp.TRecord(addr = sp.TAddress, publicKey = sp.TKey).layout(("addr", "publicKey"))), removed = sp.TSet(sp.TAddress)).layout(("added", "removed"))).layout((("changeQuorum", "changeTarget"), ("changeTimeout", "changeVoters")))), targetAdmin = sp.TList(sp.TVariant(changeActive = sp.TBool, changeAdmin = sp.TAddress, changeOracles = sp.TRecord(added = sp.TList(sp.TPair(sp.TAddress, sp.TRecord(adminAddress = sp.TAddress, endingRound = sp.TOption(sp.TNat), startingRound = sp.TNat).layout(("adminAddress", ("endingRound", "startingRound"))))), removed = sp.TList(sp.TAddress)).layout(("added", "removed")), updateFutureRounds = sp.TRecord(maxSubmissions = sp.TNat, minSubmissions = sp.TNat, oraclePayment = sp.TNat, restartDelay = sp.TNat, timeout = sp.TNat).layout((("maxSubmissions", "minSubmissions"), ("oraclePayment", ("restartDelay", "timeout"))))).layout((("changeActive", "changeAdmin"), ("changeOracles", "updateFutureRounds"))))).layout(("selfAdmin", "targetAdmin"))), canceled = sp.TBool, id = sp.TNat, nay = sp.TSet(sp.TNat), startedAt = sp.TTimestamp, yay = sp.TSet(sp.TNat)).layout((("batchs", ("canceled", "id")), ("nay", ("startedAt", "yay")))), sp.snd(params)).open_some(message = 'MultisignAdmin_WrongCallbackInterface'))

  @sp.entry_point
  def getParams(self, params):
    sp.transfer(sp.record(quorum = self.data.quorum, target = self.data.target, timeout = self.data.timeout), sp.tez(0), params)

  @sp.entry_point
  def multiVote(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(initiatorId = sp.TNat, proposalId = sp.TNat, votes = sp.TList(sp.TRecord(signature = sp.TSignature, voterId = sp.TNat, yay = sp.TBool).layout(("signature", ("voterId", "yay"))))).layout(("initiatorId", ("proposalId", "votes")))))
    sp.for proposalVotes in params:
      sp.for vote in proposalVotes.votes:
        sp.verify(self.data.voters.contains(vote.voterId), 'MultisignAdmin_VoterUnknown')
        sp.verify(sp.check_signature(self.data.voters[vote.voterId].publicKey, vote.signature, sp.pack((sp.self_address, (proposalVotes.initiatorId, proposalVotes.proposalId)))), 'MultisignAdmin_Badsig')
        y14 = sp.local("y14", self.registerVote(sp.record(in_param = sp.record(initiatorId = proposalVotes.initiatorId, proposalId = proposalVotes.proposalId, voterId = vote.voterId, yay = vote.yay), in_storage = self.data)))
        self.data = y14.value.storage
        sp.for op in y14.value.operations.rev():
          sp.operations().push(op)
      sp.if (sp.len(self.data.proposals[proposalVotes.initiatorId].nay) + sp.len(self.data.proposals[proposalVotes.initiatorId].yay)) >= self.data.quorum:
        sp.if sp.len(self.data.proposals[proposalVotes.initiatorId].nay) >= sp.len(self.data.proposals[proposalVotes.initiatorId].yay):
          self.data.proposals[proposalVotes.initiatorId].canceled = True
        sp.else:
          y15 = sp.local("y15", self.onVoted(sp.record(in_param = self.data.proposals[proposalVotes.initiatorId], in_storage = self.data)))
          self.data = y15.value.storage
          sp.for op in y15.value.operations.rev():
            sp.operations().push(op)

  @sp.entry_point
  def newProposal(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), 'MultisignAdmin_VoterUnknown')
    self.data.voters[self.data.addrVoterId[sp.sender]].lastProposalId += 1
    self.data.proposals[self.data.addrVoterId[sp.sender]] = sp.record(batchs = params, canceled = False, id = self.data.voters[self.data.addrVoterId[sp.sender]].lastProposalId, nay = sp.set([]), startedAt = sp.now, yay = sp.set([self.data.addrVoterId[sp.sender]]))
    sp.if self.data.quorum < 2:
      y16 = sp.local("y16", self.onVoted(sp.record(in_param = self.data.proposals[self.data.addrVoterId[sp.sender]], in_storage = self.data)))
      self.data = y16.value.storage
      sp.for op in y16.value.operations.rev():
        sp.operations().push(op)

  @sp.entry_point
  def vote(self, params):
    sp.verify(self.data.addrVoterId.contains(sp.sender), 'MultisignAdmin_VoterUnknown')
    sp.for vote in params:
      y17 = sp.local("y17", self.registerVote(sp.record(in_param = sp.record(initiatorId = vote.initiatorId, proposalId = vote.proposalId, voterId = self.data.addrVoterId[sp.sender], yay = vote.yay), in_storage = self.data)))
      self.data = y17.value.storage
      sp.for op in y17.value.operations.rev():
        sp.operations().push(op)
      sp.if (sp.len(self.data.proposals[vote.initiatorId].nay) + sp.len(self.data.proposals[vote.initiatorId].yay)) >= self.data.quorum:
        sp.if sp.len(self.data.proposals[vote.initiatorId].nay) >= sp.len(self.data.proposals[vote.initiatorId].yay):
          self.data.proposals[vote.initiatorId].canceled = True
        sp.else:
          y18 = sp.local("y18", self.onVoted(sp.record(in_param = self.data.proposals[vote.initiatorId], in_storage = self.data)))
          self.data = y18.value.storage
          sp.for op in y18.value.operations.rev():
            sp.operations().push(op)