import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)

  @sp.entry_point
  def bounce(self, params):
    sp.set_type(params, sp.TUnit)
    with sp.contract(sp.TUnit, sp.source).match_cases() as arg:
      with arg.match('None') as _l0:
        sp.failwith(11)
      with arg.match('Some') as s35:
        x12 = sp.local("x12", (sp.cons(sp.transfer_operation(sp.unit, sp.amount, s35), sp.list([])), self.data))
        s44 = sp.local("s44", sp.fst(x12.value))
        s45 = sp.local("s45", sp.snd(x12.value))
        sp.operations() = s44.value
        self.data = s45.value


  @sp.entry_point
  def bounce2(self, params):
    sp.set_type(params, sp.TUnit)
    with sp.contract(sp.TUnit, sp.source).match_cases() as arg:
      with arg.match('None') as _l16:
        sp.failwith(15)
      with arg.match('Some') as s9:
        with sp.contract(sp.TUnit, sp.source).match_cases() as arg:
          with arg.match('None') as _l20:
            sp.failwith(16)
          with arg.match('Some') as s20:
            x27 = sp.local("x27", sp.amount - sp.tez(1))
            x41 = sp.local("x41", (sp.cons(sp.transfer_operation(sp.unit, x27.value, s20), sp.cons(sp.transfer_operation(sp.unit, sp.tez(1), s9), sp.list([]))), self.data))
            s44 = sp.local("s44", sp.fst(x41.value))
            s45 = sp.local("s45", sp.snd(x41.value))
            sp.operations() = s44.value
            self.data = s45.value



@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
