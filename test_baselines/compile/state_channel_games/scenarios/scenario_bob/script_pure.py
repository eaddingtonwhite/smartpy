"""
    A realistic scenario where Alice and Opponent play tictactoe on the gamePlatform.

    They exchange their addresses, public_key, and the running game via an
    unknown medium, possibly public medium.

    Alice scenario is a bit more complex. She originates a gamePlatform
    contract on the interpreter. This way she can verify what would occur if
    every exchange would be pushed onchain.

    Bob is doing something lighter: he only uses the `TestView` template
    and `verify` function of the scenario.
    Bob doesn't verify opponnent countersign of his moves.

    In a perfect world, only steps marked as ONCHAIN need to be pushed on-chain.
    Everything else only exists on the interpreter.
"""

import smartpy as sp

gp         = sp.io.import_template("state_channel_games/game_platform.py")
Tictactoe  = sp.io.import_template("state_channel_games/models/tictactoe.py").Tictactoe
model_wrap = sp.io.import_template("state_channel_games/model_wrap.py")

class TestView(sp.Contract):
    def __init__(self, c, f):
        self.c = c
        self.f = f.f
        self.init(result = sp.none)

    @sp.entry_point
    def compute(self, data, params):
        self.c.data = data
        b = sp.bind_block()
        with b:
            self.f(self.c, params)
        self.data.result = sp.some(b.value)

if "templates" not in __name__:
    me = sp.record(
        address    = sp.address("tz1bQx5gL9WLBsc9qyxKj7wGiit78A7frBmK"),
        public_key = sp.key("edpkv3regPfkqjzWuD29P4tq1zkKokRQnGGpg5UMMvvXnCkXNoRqY2"),
        secret_key = sp.secret_key("edskRtPDyFcuEVeZ2tK7xu2jKGFUgvfvLP7gWB8QvFmBv84Z9MhErHps65MNr1RdL5Qh3Uii6CDhymhoPFEee3TykXPpaWPUWU"),
    )
    opponent = sp.record(
        address    = sp.address("tz1dtXJpDg4e8tYcGE5b7dzStfvHGyVZivaY"),
        public_key = sp.key("edpkuTgJuK2tK5pixWA27bMZ3JHybicPr6Uy6ang9GWbGvFV4eAPXb"),
    )
    players = {me.address: me.public_key, opponent.address: opponent.public_key}
    admin = me

    tictactoe = Tictactoe()
    play_delay = 3600 * 24

    @sp.add_test(name="Bob")
    def test():
        data = {}
        sc = sp.test_scenario()
        sc.table_of_contents()
        def get_state(testView):
            return sc.compute(testView.data.result.open_some())
        sc.h1("Transfers")
        sc.h2("Contract")
        platform_addr = sp.address("KT1LX9dx1MMSBbG3seX7DHC7kd2tuKUmwyZs")
        c1 = gp.GamePlatform(admins = sp.set([admin.address]), self_addr = platform_addr)
        c1.address = platform_addr
        sc += c1

        # -- New channel --
        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "1", withdraw_delay = 60 * 10)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        # -- Push bonds --
        sc.h2("ONCHAIN: Push bonds {0: 100_000_000}")
        c1.channel_push_bonds(player_addr = me.address, channel_id = channel_id, bonds = {0: 100*10**6}).run(sender = me.address, amount = sp.tez(100))
        sc.h2("Push opponent bonds {0: 50_000_000}")
        c1.channel_push_bonds(player_addr = opponent.address, channel_id = channel_id, bonds = {0: 50*10**6}).run(sender = opponent.address, amount = sp.tez(50))

        # -- Offchain views --
        sc.h2("Offchain view")
        sc.h3("Offchain new_game")
        offchain_new_game = TestView(c1, c1.offchain_new_game)
        sc += offchain_new_game
        sc.h3("Offchain play")
        offchain_play = TestView(c1, c1.offchain_play)
        sc += offchain_play

        # -- New model --
        sc.h2("New model: TicTacToe")
        model = model_wrap.model_wrap(tictactoe)
        model = sc.compute(model)
        model_id = sc.compute(model_wrap.model_id(model))
        sc.verify_equal(model_id, sp.bytes("0xfee5d250816b1e1fc0518f7cdf3fe498332634f7ec3cb99aea0b456d6d5cc794"))
        c1.new_model(model).run(sender = me.address)
        sc.verify(c1.data.models.contains(model_id))

        # -- Game constants --
        settlements = sp.map({
            sp.variant("player_inactive",      1)      : [gp.transfer(1, 2, {0: 15_000000})],
            sp.variant("player_inactive",      2)      : [gp.transfer(2, 1, {0: 15_000000})],
            sp.variant("player_double_played", 1)      : [gp.transfer(1, 2, {0: 20_000000})],
            sp.variant("player_double_played", 2)      : [gp.transfer(2, 1, {0: 20_000000})],
            sp.variant("game_finished", "player_1_won"): [gp.transfer(2, 1, {0: 10_000000})],
            sp.variant("game_finished", "player_2_won"): [gp.transfer(1, 2, {0: 10_000000})],
        })
        constants = sp.record(model_id = model_id, channel_id = channel_id, players_addr = {2:me.address, 1:opponent.address},
                              game_nonce = "game2", play_delay = play_delay, settlements = settlements, bonds = {1:{0:20*10**6}, 2:{0:20*10**6}})
        game_id = gp.compute_game_id(constants)
        sc.show(game_id)

        # -- New game --
        sc.h2("New game")
        new_game_action = gp.action_new_game(constants, sp.pack(sp.unit))
        my_new_game_sig = sc.compute(sp.make_signature(me.secret_key, new_game_action))
        sc.show(my_new_game_sig)

        # Replace by opponent signature
        opponent_new_game_sig = sp.signature('edsigtacNiZEmvuPcLn1qHfCpGaX7B37W5qphQxfQaAaPXZLRipQxbhL29VxyGED3oCZ8VgdwoiAkchDFfgMPZrgsg9DLPDcqWi')
        new_game_signatures = sp.map({me.public_key: my_new_game_sig, opponent.public_key: opponent_new_game_sig})

        offchain_new_game.compute(sp.record(data = c1.data, params = sp.record(constants = constants, params = sp.pack(sp.unit), signatures = new_game_signatures))).run(sender = me.address)
        game = get_state(offchain_new_game)
        sc.show(game)

        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_signatures).run(sender = me.address)

        # -- Move 0 --
        move_nb = 0
        sc.h2(f"Move {move_nb}")

        # Replace by opponent move
        move_data = sp.record(i = 1, j = 1)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = 0, sender = opponent.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        # Replace by opponent signature
        opponent_state_sig = sp.signature('edsigu2du13SzYeqwWiSWWtEtkgpU3QGSySkV8Kdhue8zRXdWUBp5JPRLcJvR2EDX65BiW11QeUtVfnsRvZc87Ly4dgrrLREW53')

        sc.verify(sp.check_signature(
            opponent.public_key,
            opponent_state_sig,
            action_new_state
        ))

        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        # -- Move 1 --
        move_nb = 1
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 0, j = 0)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = me.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: my_state_sig})

        # -- Move 2 --
        move_nb = 2
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 0, j = 1)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = opponent.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        opponent_state_sig = sp.signature('edsigteu5Qog57XB4vfuysmc1zZZAN4mUPNdrAnqhNfyzgMHfWt9h5Lxu23g3iC5uRDM3ptutR2xxEyxtvDsifJkuKH2AZNHG37')

        sc.verify(sp.check_signature(
            opponent.public_key,
            opponent_state_sig,
            action_new_state
        ))

        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        # -- Move 3 --
        move_nb = 3
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 2, j = 1)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = me.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: my_state_sig})

        # -- Move 4 --
        move_nb = 4
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 2, j = 0)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = opponent.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig 4")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        opponent_state_sig = sp.signature('edsigtbtHb5ZwZaAE4Dq5PSdoKKRwZqR9FDwRsLJXtkL8K9RXYXALVLQ2DoUWVKH24ciJtUfeP2JPZHmh26H2SHL9LjkM74g78b')

        sc.verify(sp.check_signature(
            opponent.public_key,
            opponent_state_sig,
            action_new_state
        ))

        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        # -- Move 5 --
        move_nb = 5
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 0, j = 2)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = me.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig 5")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: my_state_sig})

        # -- Move 6 --
        move_nb = 6
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 1, j = 2)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = opponent.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig 6")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        opponent_state_sig = sp.signature('edsigti4cCdVvaeVtAn7XwqJSmBDJLjYmjYMX9BzsHPY91UzrrmH1m6B1Myy3Ht9qtceGtQjcHZUzqTsGqZVaFZ5e2iLZprnqK7')

        sc.verify(sp.check_signature(
            opponent.public_key,
            opponent_state_sig,
            action_new_state
        ))

        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        # -- Move 7 --
        move_nb = 7
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 2, j = 2)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = me.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig 7")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: my_state_sig})

        # -- Move 8 --
        move_nb = 8
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 1, j = 0)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = opponent.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig 8")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        opponent_state_sig = sp.signature('edsigtjRU3f8nGdYj31aYNtAuXEGEj7oqBbzYxfkVdEFd8BY1fhdFV6NnERJyzafX5kw36fxMotkJ5MfkytaVgmqmLnXxB1GLhX')

        sc.verify(sp.check_signature(
            opponent.public_key,
            opponent_state_sig,
            action_new_state
        ))

        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})
