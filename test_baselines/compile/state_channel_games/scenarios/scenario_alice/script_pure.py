"""
    A realistic scenario where Alice and Opponent play tictactoe on the gamePlatform.

    They exchange their addresses, public_key, and the running game via an
    unknown medium, possibly public medium.

    Alice scenario is a bit more complex. She originates a gamePlatform
    contract on the interpreter. This way she can verify what would occur if
    every exchange would be pushed onchain.

    Bob is doing something lighter: he only uses the `TestView` template
    and `verify` function of the scenario.
    Bob doesn't verify opponnent countersign of his moves.

    In a perfect world, only steps marked as ONCHAIN need to be pushed on-chain.
    Everything else only exists on the interpreter.
"""
import smartpy as sp

gp         = sp.io.import_template("state_channel_games/game_platform.py")
Tictactoe  = sp.io.import_template("state_channel_games/models/tictactoe.py").Tictactoe
model_wrap = sp.io.import_template("state_channel_games/model_wrap.py")

class TestView(sp.Contract):
    def __init__(self, c, f):
        self.c = c
        self.f = f.f
        self.init(result = sp.none)

    @sp.entry_point
    def compute(self, data, params):
        self.c.data = data
        b = sp.bind_block()
        with b:
            self.f(self.c, params)
        self.data.result = sp.some(b.value)

if "templates" not in __name__:
    me = sp.record(
        address    = sp.address("tz1dtXJpDg4e8tYcGE5b7dzStfvHGyVZivaY"),
        public_key = sp.key("edpkuTgJuK2tK5pixWA27bMZ3JHybicPr6Uy6ang9GWbGvFV4eAPXb"),
        secret_key = sp.secret_key("edskRhyHJmHpNZagdYET8zkQob6whvb8tJ2tEp2r3ugx7LzH4kEHRgp3tSiBZLw445CdLjXsasDGt4G2DR4b8RUr5shnF4DZDf"),
    )
    opponent = sp.record(
        address    = sp.address("tz1bQx5gL9WLBsc9qyxKj7wGiit78A7frBmK"),
        public_key = sp.key("edpkv3regPfkqjzWuD29P4tq1zkKokRQnGGpg5UMMvvXnCkXNoRqY2"),
    )
    players = {me.address: me.public_key, opponent.address: opponent.public_key}
    admin = me

    tictactoe = Tictactoe()
    play_delay = 3600 * 24

    @sp.add_test(name="Alice")
    def test():
        data = {}
        sc = sp.test_scenario()
        sc.table_of_contents()
        def get_state(testView):
            return sc.compute(testView.data.result.open_some())
        sc.h1("Transfers")
        sc.h2("Contract")
        platform_addr = sp.address("KT1LX9dx1MMSBbG3seX7DHC7kd2tuKUmwyZs")
        c1 = gp.GamePlatform(admins = sp.set([admin.address]), self_addr = platform_addr)
        c1.address = platform_addr
        sc += c1
        # -- Offchain views --
        sc.h2("Offchain view")
        sc.h3("Offchain new_game")
        offchain_new_game = TestView(c1, c1.offchain_new_game)
        sc += offchain_new_game
        sc.h3("Offchain play")
        offchain_play = TestView(c1, c1.offchain_play)
        sc += offchain_play

        # -- New channel --
        sc.h2("ONCHAIN: New channel")
        channelParams = sp.record(players = players, nonce = "1", withdraw_delay = 60 * 10)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)
        sc.p('Channel id. Transmit this to Opponent:')
        sc.show(channel_id)

        # -- Push bonds --
        sc.h2("ONCHAIN: Push bonds {0: 50_000_000}")
        c1.channel_push_bonds(player_addr = me.address, channel_id = channel_id, bonds = {0: 50*10**6}).run(sender = me.address, amount = sp.tez(50))
        sc.h2("Push opponent bonds {0: 100}")
        c1.channel_push_bonds(player_addr = opponent.address, channel_id = channel_id, bonds = {0: 100*10**6}).run(sender = opponent.address, amount = sp.tez(100))

        # -- New model --
        sc.h2("New model: TicTacToe")
        model = model_wrap.model_wrap(tictactoe)
        model = sc.compute(model)
        model_id = sc.compute(model_wrap.model_id(model))
        sc.verify_equal(model_id, sp.bytes("0xfee5d250816b1e1fc0518f7cdf3fe498332634f7ec3cb99aea0b456d6d5cc794"))
        c1.new_model(model).run(sender = me.address)

        # -- Game constants --
        settlements = sp.map({
            sp.variant("player_inactive",      1)      : [gp.transfer(1, 2, {0: 15_000000})],
            sp.variant("player_inactive",      2)      : [gp.transfer(2, 1, {0: 15_000000})],
            sp.variant("player_double_played", 1)      : [gp.transfer(1, 2, {0: 20_000000})],
            sp.variant("player_double_played", 2)      : [gp.transfer(2, 1, {0: 20_000000})],
            sp.variant("game_finished", "player_1_won"): [gp.transfer(2, 1, {0: 10_000000})],
            sp.variant("game_finished", "player_2_won"): [gp.transfer(1, 2, {0: 10_000000})],
        })
        constants = sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:me.address, 2:opponent.address},
                              game_nonce = "game2", play_delay = play_delay, settlements = settlements, bonds = {1:{0:20*10**6}, 2:{0:20*10**6}})
        game_id = gp.compute_game_id(constants)
        sc.show(game_id)

        # -- New game --
        sc.h2("New game")
        new_game_action = gp.action_new_game(constants, sp.pack(sp.unit))
        sc.p('New game action.')
        sc.show(new_game_action)
        my_new_game_sig = sc.compute(sp.make_signature(me.secret_key, new_game_action))
        sc.p('New game signature.<br/>Transmit this to Opponent:')
        sc.show(my_new_game_sig)

        # Replace by opponent signature
        opponent_new_game_sig = sp.signature('edsigtohUPNjFagQ3p2bUZHtndJfyNTPuCai1hKuFiuK8wd1yB1KtLJq7JN8E6APzDuNcsFQZ2cFZcT1c19ncdmtA78WWhgsm3D')
        new_game_signatures = sp.map({me.public_key: my_new_game_sig, opponent.public_key: opponent_new_game_sig})

        offchain_new_game.compute(sp.record(data = c1.data, params = sp.record(constants = constants, params = sp.pack(sp.unit), signatures = new_game_signatures))).run(sender = me.address)
        game = get_state(offchain_new_game)
        sc.show(game)

        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_signatures).run(sender = me.address)

        # -- Move 0 --
        move_nb = 0
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 1, j = 1)
        sc.p('Move data. Transmit this to Opponent:')
        sc.show(move_data)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = 0, sender = me.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig. Transmit this to Opponent:")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        # Replace by opponent signature
        opponent_state_sig = sp.signature('edsigtdZCPbRjtvsuyz51UJhg55gwPU1jxJMXDfbaWDmmfXLtUpAMkU9WMwmC9p5ZsMhReaPdzNTetVvD388E4ZBsoJUGHHruZk')
        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        sc.h3("push move 0 onchain")
        c1.game_set_state(
            game_id = game_id,
            new_current = data[move_nb][0].current,
            new_state = data[move_nb][0].state.open_some(),
            signatures = data[move_nb][2]
        ).run(sender = me.address)

        # -- Move 1 --
        move_nb += 1
        sc.h2(f"Move {move_nb}")

        # Replace by opponent move
        move_data = sp.record(i = 0, j = 0)
        sc.show(move_data)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = opponent.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig. Transmit this to Opponent:")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        # Replace by opponent signature
        opponent_state_sig = sp.signature('edsigtZWR51RJM5pEywDDJwuFW7V8VwK4SwZXRSwHnbZMpUrgioMCe1T7BGm2ammoRun1c2ME4BxX2CCUSwNwJWg5T7QxXjc6Aq')
        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        sc.h3("push move onchain")
        c1.game_set_state(
            game_id = game_id,
            new_current = data[move_nb][0].current,
            new_state = data[move_nb][0].state.open_some(),
            signatures = data[move_nb][2]
        ).run(sender = me.address)

        # -- Move 2 --
        move_nb += 1
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 0, j = 1)
        sc.p('Move data. Transmit this to Opponent:')
        sc.show(move_data)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = me.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig. Transmit this to Opponent:")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        # Replace by opponent signature
        opponent_state_sig = sp.signature('edsigtZbnZR4SfNrds1B4x4316no4g7wz8eZdvLG7pwCiwHGUnBvFaHEGrZHkqi85jeoy1nyz3cnMxr2AWwNqZGMFqw8WHA9E4g')
        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        sc.h3("push move onchain")
        c1.game_set_state(
            game_id = game_id,
            new_current = data[move_nb][0].current,
            new_state = data[move_nb][0].state.open_some(),
            signatures = data[move_nb][2]
        ).run(sender = me.address)

        # -- Move 3 --
        move_nb += 1
        sc.h2(f"Move {move_nb}")

        # Replace by opponent move
        move_data = sp.record(i = 2, j = 1)
        sc.show(move_data)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = opponent.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig. Transmit this to Opponent:")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        opponent_state_sig = sp.signature('edsigtYLCLNanFAg9JbgGMGQcoX8zNJTGsCCRTTSGjPd1yKoWWUr1rUwfoGE1cAFcjYYa3ZWSMMGygMM47kNNE3hq27J4oLZbS7')
        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        sc.h3("push move onchain")
        c1.game_set_state(
            game_id = game_id,
            new_current = data[move_nb][0].current,
            new_state = data[move_nb][0].state.open_some(),
            signatures = data[move_nb][2]
        ).run(sender = me.address)

        # -- Move 4 --
        move_nb += 1
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 2, j = 0)
        sc.p('Move data. Transmit this to Opponent:')
        sc.show(move_data)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = me.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig. Transmit this to Opponent:")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        opponent_state_sig = sp.signature('edsigtv5WsDw1Pp463UorQaJhe1hQuJbvzUdUZDiLPuJzewarER6kJ9EGY66onYXXEdpMgSiuYDHc8JrEExpkY1vEvMCMRN8WDy')
        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        sc.h3("push move onchain")
        c1.game_set_state(
            game_id = game_id,
            new_current = data[move_nb][0].current,
            new_state = data[move_nb][0].state.open_some(),
            signatures = data[move_nb][2]
        ).run(sender = me.address)

        # -- Move 5 --
        move_nb += 1
        sc.h2(f"Move {move_nb}")

        # Replace by opponent move
        move_data = sp.record(i = 0, j = 2)
        sc.show(move_data)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = opponent.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig. Transmit this to Opponent:")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        opponent_state_sig = sp.signature('edsigtqbpKh1VrUAbWi8TUUFKWk33u92FDDhQW1cN9mP7HRpCaFgdjYMYUY3DtdJB7Ur3jhAofU4kWmhD9ULohhPYrjizgcfDGp')
        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        sc.h3("push move onchain")
        c1.game_set_state(
            game_id = game_id,
            new_current = data[move_nb][0].current,
            new_state = data[move_nb][0].state.open_some(),
            signatures = data[move_nb][2]
        ).run(sender = me.address)

        # -- Move 6 --
        move_nb += 1
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 1, j = 2)
        sc.p('Move data. Transmit this to Opponent:')
        sc.show(move_data)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = me.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig. Transmit this to Opponent:")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        opponent_state_sig = sp.signature('edsigtyXCmUFg3SHvSDpt9rDiiHS8MPoeu6emALYyn3F5B2hY4QE7FRviE7cJLKtbq8LGttfUUiJXnRxTDpqf8XgheT9jBhmNrR')
        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        sc.h3("push move onchain")
        c1.game_set_state(
            game_id = game_id,
            new_current = data[move_nb][0].current,
            new_state = data[move_nb][0].state.open_some(),
            signatures = data[move_nb][2]
        ).run(sender = me.address)

        # -- Move 7 --
        move_nb += 1
        sc.h2(f"Move {move_nb}")

        # Replace by opponent move
        move_data = sp.record(i = 2, j = 2)
        sc.show(move_data)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = opponent.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig. Transmit this to Opponent:")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        opponent_state_sig = sp.signature('edsigttMtBSM5Dj3vntVJjpsreMJamboQ5xRnqjo6s4PDLCCQeYdqeSr8EKUELpwHAumMJELSNYnbNT66Ss521tJUQ9jwqWT3L8')
        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        sc.h3("push move onchain")
        c1.game_set_state(
            game_id = game_id,
            new_current = data[move_nb][0].current,
            new_state = data[move_nb][0].state.open_some(),
            signatures = data[move_nb][2]
        ).run(sender = me.address)

        # -- Move 8 --
        move_nb += 1
        sc.h2(f"Move {move_nb}")

        move_data = sp.record(i = 1, j = 0)
        sc.p('Move data. Transmit this to Opponent:')
        sc.show(move_data)
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(move_data), move_nb = move_nb, sender = me.address))).run(sender = me.address)
        game = get_state(offchain_play)
        sc.p('Action new state')
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        sc.show(action_new_state)
        sc.p("My state sig. Transmit this to Opponent:")
        my_state_sig = sp.make_signature(me.secret_key, action_new_state)
        sc.show(my_state_sig)

        opponent_state_sig = sp.signature('edsigtnUSdPUnSwXJCr5xXakhjypzML5PBbTwEi729U87XFzZuXvZNTojzGADdb513YdEWDbM5UWGbWovoYvnYcaA9C2pdWYbnL')
        data[move_nb] = (game, action_new_state, {me.public_key: my_state_sig, opponent.public_key: opponent_state_sig})

        sc.h2("ONCHAIN: push last state onchain")
        c1.game_set_state(
            game_id = game_id,
            new_current = data[move_nb][0].current,
            new_state = data[move_nb][0].state.open_some(),
            signatures = data[move_nb][2]
        ).run(sender = me.address)

        # -- Settle --
        sc.h2(f"ONCHAIN: Settle")
        c1.game_settle(game_id).run(sender = me.address)
