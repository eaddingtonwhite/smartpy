import smartpy as sp

gp         = sp.io.import_template("state_channel_games/game_platform.py")
Tictactoe  = sp.io.import_template("state_channel_games/models/tictactoe.py").Tictactoe
Transfer   = sp.io.import_template("state_channel_games/models/transfer.py").Transfer
model_wrap = sp.io.import_template("state_channel_games/model_wrap.py")

class TestView(sp.Contract):
    def __init__(self, c, f):
        self.c = c
        self.f = f.f
        self.init(result = sp.none)

    @sp.entry_point
    def compute(self, data, params):
        self.c.data = data
        b = sp.bind_block()
        with b:
            self.f(self.c, params)
        self.data.result = sp.some(b.value)

def make_signatures(p1, p2, x):
    sig1 = sp.make_signature(p1.secret_key, x)
    sig2 = sp.make_signature(p2.secret_key, x)
    return sp.map({p1.public_key: sig1, p2.public_key: sig2})

def transfer_settlements(p1_to_p2 = {}, p2_to_p1 = {}):
    transfers = []
    if len(p1_to_p2) > 0:
        transfers.append(gp.transfer(1, 2, p1_to_p2))
    if len(p2_to_p1) > 0:
        transfers.append(gp.transfer(2, 1, p2_to_p1))
    settlements = sp.map({
        sp.variant("player_double_played", 1)      : transfers,
        sp.variant("player_double_played", 2)      : transfers,
        sp.variant("player_inactive",      1)      : transfers,
        sp.variant("player_inactive",      2)      : transfers,
        sp.variant("game_finished", "transferred")  : transfers
    })
    bonds = {1: p1_to_p2, 2: p2_to_p1}
    sp.set_type_expr(bonds, gp.types.t_game_bonds)
    sp.set_type_expr(settlements, gp.types.t_constants)
    return settlements, bonds

if "templates" not in __name__:
    player1 = sp.record(
        address    = sp.address("tz1Tj7Re9TYbyD6TNPzBYgd4WZuubWX7vSMh"),
        public_key = sp.key("edpktyqfYnZz5jSTS2Udpta2U2PEevDySmrV4hb9yZP8NmogRWsS3v"),
        secret_key = sp.secret_key("edskS1C2Ycj7peyjPt8Rwy1jQHD6QDFuaxNVEUJ4a9KhEcwi9oNmYtMqiEG3CTWbfwHXsimSw1VFw6ZQhvjXy2J1qJrMa93csb"),
    )
    player2 = sp.record(
        address    = sp.address("tz1Nj4mX3b6simUzdJ1ouNMbJ2SgMR1S2uHQ"),
        public_key = sp.key("edpku8WQYTYtBqLML1GgTja2t8r9A1BrnbAABqMg1w1rMkexLU287S"),
        secret_key = sp.secret_key("edskSA3qCs5NQEzugUtwRUZuft51AuhyLYLUnfxm5r6uatoJPXTTJrmBEFXgn3QDLPHMxAmzsAB2nxD1m8GYfh2gTqZgMcAwLN"),
    )
    players = {player1.address: player1.public_key, player2.address: player2.public_key}
    admin = sp.record(
        address    = sp.address("tz1Tj7Re9TYbyD6TNPzBYgd4WZuubWX7vSMh"),
        public_key = sp.key("edpktyqfYnZz5jSTS2Udpta2U2PEevDySmrV4hb9yZP8NmogRWsS3v"),
        secret_key = sp.secret_key("edskS1C2Ycj7peyjPt8Rwy1jQHD6QDFuaxNVEUJ4a9KhEcwi9oNmYtMqiEG3CTWbfwHXsimSw1VFw6ZQhvjXy2J1qJrMa93csb"),
    )

    tictactoe = Tictactoe()
    transfer = Transfer()
    play_delay = 3600 * 24

    def new_game_sigs(constants, params = sp.unit):
        new_game = gp.action_new_game(constants, sp.pack(params))
        return make_signatures(player1, player2, new_game)

    def action_state_sigs(game_id, game):
        action_new_state = gp.action_new_state(game_id, game.current, game.state.open_some())
        signatures = make_signatures(player1, player2, action_new_state)
        return (game, action_new_state, signatures)

    @sp.add_test(name="Testnet Minimal")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Transfers")
        sc.h2("Contract")
        contract_addr = sp.address("KT1L6rXYMPQnJBrRKYkx5XtWYTM8pp8Dyadg")
        c1 = gp.GamePlatform(admins = sp.set([admin.address]), self_addr = contract_addr)
        c1.address = contract_addr
        sc += c1
        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.new_model(model).run(sender = player1.address)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Transfer")
        sc.h3("Player 1 will transfer 10 to Player 2")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)
        sc.h3("Player 1 push {0: 10}")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 10}).run(sender = player1.address, amount = sp.mutez(10))

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game1", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1.address)

        sc.h3("(Onchain) play transfer")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.unit), move_nb = 0).run(sender = player1.address)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(0))
        c1.game_settle(game_id).run(sender = player1.address)
        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(0))
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[0] == 10)
        sc.p("P2 received 10 tokens 0")

    @sp.add_test(name="Testnet Advanced")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        def get_state(testView):
            return sc.compute(testView.data.result.open_some())
        sc.h1("Transfers")
        sc.h2("Contract")
        contract_addr = sp.address("KT1V8pmoHLq6obLb3q7D6XfordUA5G1K7xkb")
        c1 = gp.GamePlatform(admins = sp.set([admin.address]), self_addr = contract_addr)
        c1.address = contract_addr
        sc += c1
        sc.h2("Offchain new_game")
        offchain_new_game = TestView(c1, c1.offchain_new_game)
        sc += offchain_new_game
        sc.h2("Offchain play")
        offchain_play = TestView(c1, c1.offchain_play)
        sc += offchain_play

        sc.h2("New model: TicTacToe")
        model = model_wrap.model_wrap(tictactoe)
        model = sc.compute(model)
        model_id = sc.compute(model_wrap.model_id(model))
        sc.show(model_id)
        sc.verify_equal(model_id, sp.bytes("0xfee5d250816b1e1fc0518f7cdf3fe498332634f7ec3cb99aea0b456d6d5cc794"))

        c1.new_model(model).run(sender = player1.address)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Player 1 push {0: 20}")
        c1.channel_push_bonds(player_addr = player1.address, channel_id = channel_id, bonds = {0: 20}).run(sender = player1.address, amount = sp.mutez(20))
        sc.h2("Player 2 push {0: 20}")
        c1.channel_push_bonds(player_addr = player2.address, channel_id = channel_id, bonds = {0: 20}).run(sender = player2.address, amount = sp.mutez(20))

        sc.h2("Tictactoe")
        sc.h3("New game")

        data = {}
        move_nb = 0

        settlements = sp.map({
            sp.variant("player_inactive",      1)      : [gp.transfer(1, 2, {0: 15})],
            sp.variant("player_inactive",      2)      : [gp.transfer(2, 1, {0: 15})],
            sp.variant("player_double_played", 1)      : [gp.transfer(1, 2, {0: 20})],
            sp.variant("player_double_played", 2)      : [gp.transfer(2, 1, {0: 20})],
            sp.variant("game_finished", "player_1_won"): [gp.transfer(2, 1, {0: 10})],
            sp.variant("game_finished", "player_2_won"): [gp.transfer(1, 2, {0: 10})],
        })
        constants = sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game2", play_delay = play_delay, settlements = settlements, bonds = {1:{0:20}, 2:{0:20}})
        game_id = sc.compute(gp.compute_game_id(constants))
        sc.h3("Calculate game data for new game")
        new_game_signatures = sc.compute(new_game_sigs(constants))
        offchain_new_game.compute(sp.record(data = c1.data, params = sp.record(constants = constants, params = sp.pack(sp.unit), signatures = new_game_signatures))).run(sender = player1.address)
        game = get_state(offchain_new_game)

        sc.h3("Move 0")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = game, move_data = sp.pack(sp.record(i = 1, j = 1)), move_nb = 0, sender = player1.address))).run(sender = player1.address)
        data[move_nb] = action_state_sigs(game_id, get_state(offchain_play))
        move_nb += 1
        sc.h3("Move 1")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = data[move_nb-1][0], move_data = sp.pack(sp.record(i = 1, j = 2)), move_nb = 1, sender = player2.address))).run(sender = player2.address)
        data[move_nb] = action_state_sigs(game_id, get_state(offchain_play))
        move_nb += 1
        sc.h3("Move 2")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = data[move_nb-1][0], move_data = sp.pack(sp.record(i = 2, j = 1)), move_nb = 2, sender = player1.address))).run(sender = player1.address)
        data[move_nb] = action_state_sigs(game_id, get_state(offchain_play))
        move_nb += 1
        sc.h3("Move 3")
        offchain_play.compute(sp.record(data = c1.data, params = sp.record(game = data[move_nb-1][0], move_data = sp.pack(sp.record(i = 2, j = 2)), move_nb = 3, sender = player2.address))).run(sender = player2.address)
        data[move_nb] = action_state_sigs(game_id, get_state(offchain_play))
        move_nb += 1

        #Onchain
        sc.h2("Player2 want the last move to be played onchain")
        sc.h3("Player2 push new_game")
        c1.new_game(constants = constants, params = sp.pack(sp.unit), signatures = new_game_signatures).run(sender = player2.address)
        sc.h3("Player2 update state to last move")
        c1.game_set_state(game_id = game_id, new_current = data[move_nb-1][0].current, new_state = data[move_nb-1][0].state.open_some(), signatures = data[move_nb-1][2]).run(sender = player2.address)
        sc.h3("Player2 starving")
        c1.dispute_starving(game_id = game_id, flag = True).run(sender = player2.address)
        sc.h2("Player1 plays last move onchain")
        c1.game_play(game_id = game_id, move_data = sp.pack(sp.record(i = 0, j = 1)), move_nb = 4).run(sender = player1.address)

        sc.h2("Player1 settle the game")
        c1.game_settle(game_id).run(sender = player1.address)

        # Withdraw
        sc.h2("Player 1 request withdraw 5 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 5})).run(sender = player1.address)
        sc.h2("Player 2 request leave empty challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set({})).run(sender = player2.address)
        sc.h2("Player 1 finalize withdraw request")
        c1.withdraw_finalise(channel_id).run(sender = player1.address)
