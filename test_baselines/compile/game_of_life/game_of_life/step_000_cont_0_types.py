import smartpy as sp

tstorage = sp.TRecord(board = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TIntOrNat))).layout("board")
tparameter = sp.TVariant(bar = sp.TUnit, glider = sp.TUnit, reset = sp.TUnit, run = sp.TUnit).layout((("bar", "glider"), ("reset", "run")))
tglobals = { "zero": sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TIntOrNat)) }
tviews = { }
