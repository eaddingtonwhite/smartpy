import smartpy as sp

tstorage = sp.TRecord(s = sp.TString, value = sp.TIntOrNat).layout(("s", "value"))
tparameter = sp.TVariant(arith = sp.TUnit, concat1 = sp.TUnit, concat2 = sp.TUnit, lambdas = sp.TUnit, overflow_add = sp.TUnit, overflow_mul = sp.TUnit, prim0 = sp.TUnit, seq = sp.TUnit, test_operations = sp.TUnit, underflow = sp.TUnit).layout(((("arith", "concat1"), ("concat2", ("lambdas", "overflow_add"))), (("overflow_mul", "prim0"), ("seq", ("test_operations", "underflow")))))
tglobals = { }
tviews = { }
