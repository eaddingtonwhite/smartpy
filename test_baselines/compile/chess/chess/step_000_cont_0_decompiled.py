import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(check = sp.TBool, deck = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), draw = sp.TBool, kings = sp.TMap(sp.TInt, sp.TPair(sp.TInt, sp.TPair(sp.TInt, sp.TBool))), nextPlayer = sp.TInt, previousPawnMove = sp.TOption(sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TPair(sp.TInt, sp.TInt))), winner = sp.TInt).layout((("check", ("deck", "draw")), (("kings", "nextPlayer"), ("previousPawnMove", "winner")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TPair(sp.TInt, sp.TInt)))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: let [s22; s23; s24] =
          if Eq(Compare(__storage.winner, 0))
          then
            [ Not(__storage.draw)
            ; __parameter
            ; Pair
                ( Pair(__storage.check, Pair(__storage.deck, __storage.draw))
                , Pair
                    ( Pair(__storage.kings, __storage.nextPlayer)
                    , Pair(__storage.previousPawnMove, __storage.winner)
                    )
                )
            ]
          else
            [ False
            ; __parameter
            ; Pair
                ( Pair(__storage.check, Pair(__storage.deck, __storage.draw))
                , Pair
                    ( Pair(__storage.kings, __storage.nextPlayer)
                    , Pair(__storage.previousPawnMove, __storage.winner)
                    )
                )
            ]
        if s22
        then
          let [s45; s46; s47] =
            if Ge(Compare(Car(Car(s23)), 0))
            then
              [ Gt(Compare(8, Car(Car(s23)))); s23; s24 ]
            else
              [ False; s23; s24 ]
          if s45
          then
            let [s68; s69; s70] =
              if Ge(Compare(Cdr(Car(s46)), 0))
              then
                [ Gt(Compare(8, Cdr(Car(s46)))); s46; s47 ]
              else
                [ False; s46; s47 ]
            if s68
            then
              let [s89; s90; s91] =
                if Ge(Compare(Car(Cdr(s69)), 0))
                then
                  [ Gt(Compare(8, Car(Cdr(s69)))); s69; s70 ]
                else
                  [ False; s69; s70 ]
              if s89
              then
                let [s110; s111; s112] =
                  if Ge(Compare(Cdr(Cdr(s90)), 0))
                  then
                    [ Gt(Compare(8, Cdr(Cdr(s90)))); s90; s91 ]
                  else
                    [ False; s90; s91 ]
                if s110
                then
                  match Get(Car(Car(s111)), Car(Cdr(Car(s112)))) with
                  | None _ -> Failwith(51)
                  | Some s141 ->
                      match Get(Cdr(Car(s111)), s141) with
                      | None _ -> Failwith(51)
                      | Some s156 ->
                          if Eq(Compare(Compare(s156, 0), Cdr(Car(Cdr(s112)))))
                          then
                            match Get(Car(Cdr(s111)), Car(Cdr(Car(s112)))) with
                            | None _ -> Failwith(52)
                            | Some s193 ->
                                match Get(Cdr(Cdr(s111)), s193) with
                                | None _ -> Failwith(52)
                                | Some s207 ->
                                    if Neq(Compare
                                             ( Compare(s207, 0)
                                             , Cdr(Car(Cdr(s112)))
                                             ))
                                    then
                                      match Get
                                              ( Car(Car(s111))
                                              , Car(Cdr(Car(s112)))
                                              ) with
                                      | None _ -> Failwith(53)
                                      | Some s234 ->
                                          match Get(Cdr(Car(s111)), s234) with
                                          | None _ -> Failwith(53)
                                          | Some s247 ->
                                              let [s455; s456] =
                                                if Eq(Compare
                                                        ( Abs(s247)
                                                        , 1#nat
                                                        ))
                                                then
                                                  let [s323; s324; s325] =
                                                    if Neq(Compare
                                                             ( Sub
                                                                 ( Cdr(Cdr(s111))
                                                                 , Cdr(Car(s111))
                                                                 )
                                                             , 0
                                                             ))
                                                    then
                                                      match Get
                                                              ( Car(Cdr(s111))
                                                              , Car(Cdr(Car(s112)))
                                                              ) with
                                                      | None _ ->
                                                          Failwith(55)
                                                      | Some s299 ->
                                                          match Get
                                                                  ( Cdr(Cdr(s111))
                                                                  , s299
                                                                  ) with
                                                          | None _ ->
                                                              Failwith(55)
                                                          | Some s313 ->
                                                              [ Eq(Compare
                                                                    ( 
                                                                   Compare
                                                                    ( s313
                                                                    , 0
                                                                    )
                                                                    , 0
                                                                    ))
                                                              ; s111
                                                              ; s112
                                                              ]
                                                          end
                                                      end
                                                    else
                                                      [ False; s111; s112 ]
                                                  let [s436; s437] =
                                                    if s323
                                                    then
                                                      if Eq(Compare
                                                              ( Abs(Sub
                                                                    ( Cdr(Cdr(s324))
                                                                    , Cdr(Car(s324))
                                                                    ))
                                                              , 1#nat
                                                              ))
                                                      then
                                                        let [s434; s435] =
                                                          match Car(Cdr(Cdr(s325))) with
                                                          | None _ ->
                                                              [ s324; s325 ]
                                                          | Some _ ->
                                                              match Car(Cdr(Cdr(s325))) with
                                                              | None _ ->
                                                                  Failwith(58)
                                                              | Some s368 ->
                                                                  match Car(Cdr(Cdr(s325))) with
                                                                  | None _ ->
                                                                    Failwith(58)
                                                                  | Some s379 ->
                                                                    if Eq(
                                                                    Compare
                                                                    ( Car(Car(s379))
                                                                    , Car(Cdr(s368))
                                                                    ))
                                                                    then
                                                                    match Car(Cdr(Cdr(s325))) with
                                                                    | None _ ->
                                                                    Failwith(58)
                                                                    | Some s408 ->
                                                                    match Car(Cdr(Cdr(s325))) with
                                                                    | None _ ->
                                                                    Failwith(58)
                                                                    | Some s421 ->
                                                                    if Eq(
                                                                    Compare
                                                                    ( Cdr(Car(s421))
                                                                    , 
                                                                    Sub
                                                                    ( Cdr(Cdr(s408))
                                                                    , 
                                                                    Mul
                                                                    ( 2
                                                                    , Cdr(Car(Cdr(s325)))
                                                                    )
                                                                    )
                                                                    ))
                                                                    then
                                                                    [ s324
                                                                    ; s325
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: self.data.previousPawnMove.open_some().f.j == (self.data.previousPawnMove.open_some().t.j - (2 * self.data.nextPlayer))")
                                                                    end
                                                                    end
                                                                    else
                                                                    Failwith("WrongCondition: self.data.previousPawnMove.open_some().f.i == self.data.previousPawnMove.open_some().t.i")
                                                                  end
                                                              end
                                                          end
                                                        [ s434; s435 ]
                                                      else
                                                        Failwith("WrongCondition: (abs(params.t.j - params.f.j)) == 1")
                                                    else
                                                      [ s324; s325 ]
                                                  [ s436
                                                  ; let [x2388; x2389] =
                                                      Unpair(2, s437)
                                                    Pair
                                                      ( x2388
                                                      , let [x2398; x2399] =
                                                          Unpair(2, x2389)
                                                        Pair
                                                          ( x2398
                                                          , let [x2402; x2403
                                                                  ] =
                                                              Unpair(2, x2399)
                                                            let _ = x2402
                                                            Pair
                                                              ( Some_(
                                                            Pair
                                                              ( Car(s436)
                                                              , Cdr(s436)
                                                              ))
                                                              , x2403
                                                              )
                                                          )
                                                      )
                                                  ]
                                                else
                                                  [ s111
                                                  ; let [x2386; x2387] =
                                                      Unpair(2, s112)
                                                    Pair
                                                      ( x2386
                                                      , let [x2396; x2397] =
                                                          Unpair(2, x2387)
                                                        Pair
                                                          ( x2396
                                                          , let [x2400; x2401
                                                                  ] =
                                                              Unpair(2, x2397)
                                                            let _ = x2400
                                                            Pair
                                                              ( None<{ (int ; int) ; (int ; int) }>
                                                              , x2401
                                                              )
                                                          )
                                                      )
                                                  ]
                                              match Get
                                                      ( Car(Car(s455))
                                                      , Car(Cdr(Car(s456)))
                                                      ) with
                                              | None _ -> Failwith(65)
                                              | Some s471 ->
                                                  match Get
                                                          ( Cdr(Car(s455))
                                                          , s471
                                                          ) with
                                                  | None _ -> Failwith(65)
                                                  | Some s484 ->
                                                      let [s787; s788] =
                                                        if Eq(Compare
                                                                ( Abs(s484)
                                                                , 5#nat
                                                                ))
                                                        then
                                                          let [s532; s533;
                                                                s534] =
                                                            if Eq(Compare
                                                                    ( 
                                                                  Sub
                                                                    ( Cdr(Cdr(s455))
                                                                    , Cdr(Car(s455))
                                                                    )
                                                                    , 0
                                                                    ))
                                                            then
                                                              [ True
                                                              ; s455
                                                              ; s456
                                                              ]
                                                            else
                                                              [ Eq(Compare
                                                                    ( 
                                                                   Sub
                                                                    ( Car(Cdr(s455))
                                                                    , Car(Car(s455))
                                                                    )
                                                                    , 0
                                                                    ))
                                                              ; s455
                                                              ; s456
                                                              ]
                                                          let [s569; s570;
                                                                s571] =
                                                            if s532
                                                            then
                                                              [ True
                                                              ; s533
                                                              ; s534
                                                              ]
                                                            else
                                                              [ Eq(Compare
                                                                    ( Abs(
                                                                   Sub
                                                                    ( Cdr(Cdr(s533))
                                                                    , Cdr(Car(s533))
                                                                    ))
                                                                    , Abs(
                                                                   Sub
                                                                    ( Car(Cdr(s533))
                                                                    , Car(Car(s533))
                                                                    ))
                                                                    ))
                                                              ; s533
                                                              ; s534
                                                              ]
                                                          if s569
                                                          then
                                                            let x594 =
                                                              Abs(Sub
                                                                    ( Cdr(Cdr(s570))
                                                                    , Cdr(Car(s570))
                                                                    ))
                                                            let x617 =
                                                              Abs(Sub
                                                                    ( Car(Cdr(s570))
                                                                    , Car(Car(s570))
                                                                    ))
                                                            let [s633; s634;
                                                                  s635; s636] =
                                                              if Le(Compare
                                                                    ( x617
                                                                    , x594
                                                                    ))
                                                              then
                                                                [ x594
                                                                ; 1
                                                                ; s570
                                                                ; s571
                                                                ]
                                                              else
                                                                [ x617
                                                                ; 1
                                                                ; s570
                                                                ; s571
                                                                ]
                                                            let x645 =
                                                              Sub
                                                                ( Int(s633)
                                                                , s634
                                                                )
                                                            let [_] =
                                                              loop [ Gt(
                                                                   Compare
                                                                    ( x645
                                                                    , 1
                                                                    ))
                                                                   ; 1
                                                                   ]
                                                              step s649 ->
                                                                match 
                                                                Get
                                                                  ( Add
                                                                    ( Car(Car(s635))
                                                                    , 
                                                                    Mul
                                                                    ( s649
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Car(Cdr(s635))
                                                                    , Car(Car(s635))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                  , Car(Cdr(Car(s636)))
                                                                  ) with
                                                                | None _ ->
                                                                    Failwith(70)
                                                                | Some s705 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Cdr(Car(s635))
                                                                    , 
                                                                    Mul
                                                                    ( s649
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Cdr(Cdr(s635))
                                                                    , Cdr(Car(s635))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , s705
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(70)
                                                                    | Some s756 ->
                                                                    if Eq(
                                                                    Compare
                                                                    ( s756
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    let x774 =
                                                                    Add
                                                                    ( 1
                                                                    , s649
                                                                    )
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x645
                                                                    , x774
                                                                    ))
                                                                    ; x774
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: self.data.deck[params.f.i + (k * sp.sign(params.t.i - params.f.i))][params.f.j + (k * sp.sign(params.t.j - params.f.j))] == 0")
                                                                    end
                                                                end
                                                            [ s635; s636 ]
                                                          else
                                                            Failwith("WrongCondition: (((params.t.j - params.f.j) == 0) | ((params.t.i - params.f.i) == 0)) | ((abs(params.t.j - params.f.j)) == (abs(params.t.i - params.f.i)))")
                                                        else
                                                          [ s455; s456 ]
                                                      match Get
                                                              ( Car(Car(s787))
                                                              , Car(Cdr(Car(s788)))
                                                              ) with
                                                      | None _ ->
                                                          Failwith(71)
                                                      | Some s803 ->
                                                          match Get
                                                                  ( Cdr(Car(s787))
                                                                  , s803
                                                                  ) with
                                                          | None _ ->
                                                              Failwith(71)
                                                          | Some s816 ->
                                                              let [s1042;
                                                                    s1043] =
                                                                if Eq(
                                                                Compare
                                                                  ( Abs(s816)
                                                                  , 6#nat
                                                                  ))
                                                                then
                                                                  let 
                                                                  [s866;
                                                                    s867;
                                                                    s868] =
                                                                    if Le(
                                                                    Compare
                                                                    ( Abs(
                                                                    Sub
                                                                    ( Cdr(Cdr(s787))
                                                                    , Cdr(Car(s787))
                                                                    ))
                                                                    , 1#nat
                                                                    ))
                                                                    then
                                                                    [ Le(
                                                                    Compare
                                                                    ( Abs(
                                                                    Sub
                                                                    ( Car(Cdr(s787))
                                                                    , Car(Car(s787))
                                                                    ))
                                                                    , 1#nat
                                                                    ))
                                                                    ; s787
                                                                    ; s788
                                                                    ]
                                                                    else
                                                                    [ False
                                                                    ; s787
                                                                    ; s788
                                                                    ]
                                                                  if s866
                                                                  then
                                                                    let x899 =
                                                                    Cdr(Car(Cdr(s868)))
                                                                    match 
                                                                    Get
                                                                    ( x899
                                                                    , Car(Car(Cdr(s868)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(74)
                                                                    | Some s904 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(Cdr(s868)))
                                                                    , Update(x899, Some_(
                                                                    let 
                                                                    [x2384;
                                                                    x2385] =
                                                                    Unpair(2, s904)
                                                                    Pair
                                                                    ( x2384
                                                                    , 
                                                                    let 
                                                                    [x2394;
                                                                    x2395] =
                                                                    Unpair(2, x2385)
                                                                    Pair
                                                                    ( x2394
                                                                    , 
                                                                    let _ =
                                                                    x2395
                                                                    True
                                                                    )
                                                                    )), Car(Car(Cdr(s868))))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(75)
                                                                    | Some s954 ->
                                                                    let s958 =
                                                                    Update(x899, Some_(
                                                                    let 
                                                                    [x2382;
                                                                    x2383] =
                                                                    Unpair(2, s904)
                                                                    Pair
                                                                    ( x2382
                                                                    , 
                                                                    let 
                                                                    [x2392;
                                                                    x2393] =
                                                                    Unpair(2, x2383)
                                                                    Pair
                                                                    ( x2392
                                                                    , 
                                                                    let _ =
                                                                    x2393
                                                                    True
                                                                    )
                                                                    )), Car(Car(Cdr(s868))))
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(Cdr(s868)))
                                                                    , Update(Cdr(Car(Cdr(s868))), Some_(
                                                                    let 
                                                                    [x2380;
                                                                    x2381] =
                                                                    Unpair(2, s954)
                                                                    let _ =
                                                                    x2380
                                                                    Pair
                                                                    ( Car(Cdr(s867))
                                                                    , x2381
                                                                    )), s958)
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(76)
                                                                    | Some s1012 ->
                                                                    [ s867
                                                                    ; 
                                                                    Pair
                                                                    ( Car(s868)
                                                                    , 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Update(Cdr(Car(Cdr(s868))), Some_(
                                                                    let 
                                                                    [x2378;
                                                                    x2379] =
                                                                    Unpair(2, s1012)
                                                                    Pair
                                                                    ( x2378
                                                                    , 
                                                                    let 
                                                                    [x2390;
                                                                    x2391] =
                                                                    Unpair(2, x2379)
                                                                    let _ =
                                                                    x2390
                                                                    Pair
                                                                    ( Cdr(Cdr(s867))
                                                                    , x2391
                                                                    )
                                                                    )), Update(Cdr(Car(Cdr(s868))), Some_(
                                                                    let 
                                                                    [x2376;
                                                                    x2377] =
                                                                    Unpair(2, s954)
                                                                    let _ =
                                                                    x2376
                                                                    Pair
                                                                    ( Car(Cdr(s867))
                                                                    , x2377
                                                                    )), s958))
                                                                    , Cdr(Car(Cdr(s868)))
                                                                    )
                                                                    , Cdr(Cdr(s868))
                                                                    )
                                                                    )
                                                                    ]
                                                                    end
                                                                    end
                                                                    end
                                                                  else
                                                                    Failwith("WrongCondition: ((abs(params.t.j - params.f.j)) <= 1) & ((abs(params.t.i - params.f.i)) <= 1)")
                                                                else
                                                                  [ s787
                                                                  ; s788
                                                                  ]
                                                              match Get
                                                                    ( Car(Car(s1042))
                                                                    , Car(Cdr(Car(s1043)))
                                                                    ) with
                                                              | None _ ->
                                                                  Failwith(77)
                                                              | Some s1058 ->
                                                                  match 
                                                                  Get
                                                                    ( Cdr(Car(s1042))
                                                                    , s1058
                                                                    ) with
                                                                  | None _ ->
                                                                    Failwith(77)
                                                                  | Some s1071 ->
                                                                    let 
                                                                    [s1337;
                                                                    s1338] =
                                                                    if Eq(
                                                                    Compare
                                                                    ( Abs(s1071)
                                                                    , 2#nat
                                                                    ))
                                                                    then
                                                                    let 
                                                                    [s1119;
                                                                    s1120;
                                                                    s1121] =
                                                                    if Eq(
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Cdr(Cdr(s1042))
                                                                    , Cdr(Car(s1042))
                                                                    )
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    [ True
                                                                    ; s1042
                                                                    ; s1043
                                                                    ]
                                                                    else
                                                                    [ Eq(
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Car(Cdr(s1042))
                                                                    , Car(Car(s1042))
                                                                    )
                                                                    , 0
                                                                    ))
                                                                    ; s1042
                                                                    ; s1043
                                                                    ]
                                                                    if s1119
                                                                    then
                                                                    let x1144 =
                                                                    Abs(
                                                                    Sub
                                                                    ( Cdr(Cdr(s1120))
                                                                    , Cdr(Car(s1120))
                                                                    ))
                                                                    let x1167 =
                                                                    Abs(
                                                                    Sub
                                                                    ( Car(Cdr(s1120))
                                                                    , Car(Car(s1120))
                                                                    ))
                                                                    let 
                                                                    [s1183;
                                                                    s1184;
                                                                    s1185;
                                                                    s1186] =
                                                                    if Le(
                                                                    Compare
                                                                    ( x1167
                                                                    , x1144
                                                                    ))
                                                                    then
                                                                    [ x1144
                                                                    ; 1
                                                                    ; s1120
                                                                    ; s1121
                                                                    ]
                                                                    else
                                                                    [ x1167
                                                                    ; 1
                                                                    ; s1120
                                                                    ; s1121
                                                                    ]
                                                                    let x1195 =
                                                                    Sub
                                                                    ( Int(s1183)
                                                                    , s1184
                                                                    )
                                                                    let 
                                                                    [_] =
                                                                    loop 
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1195
                                                                    , 1
                                                                    ))
                                                                    ; 1
                                                                    ]
                                                                    step s1199 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Car(Car(s1185))
                                                                    , 
                                                                    Mul
                                                                    ( s1199
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Car(Cdr(s1185))
                                                                    , Car(Car(s1185))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , Car(Cdr(Car(s1186)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(82)
                                                                    | Some s1255 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Cdr(Car(s1185))
                                                                    , 
                                                                    Mul
                                                                    ( s1199
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Cdr(Cdr(s1185))
                                                                    , Cdr(Car(s1185))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , s1255
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(82)
                                                                    | Some s1306 ->
                                                                    if Eq(
                                                                    Compare
                                                                    ( s1306
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    let x1324 =
                                                                    Add
                                                                    ( 1
                                                                    , s1199
                                                                    )
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1195
                                                                    , x1324
                                                                    ))
                                                                    ; x1324
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: self.data.deck[params.f.i + (k * sp.sign(params.t.i - params.f.i))][params.f.j + (k * sp.sign(params.t.j - params.f.j))] == 0")
                                                                    end
                                                                    end
                                                                    [ s1185
                                                                    ; s1186
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: ((params.t.j - params.f.j) == 0) | ((params.t.i - params.f.i) == 0)")
                                                                    else
                                                                    [ s1042
                                                                    ; s1043
                                                                    ]
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1337))
                                                                    , Car(Cdr(Car(s1338)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(83)
                                                                    | Some s1353 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s1337))
                                                                    , s1353
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(83)
                                                                    | Some s1366 ->
                                                                    let 
                                                                    [s1623;
                                                                    s1624] =
                                                                    if Eq(
                                                                    Compare
                                                                    ( Abs(s1366)
                                                                    , 4#nat
                                                                    ))
                                                                    then
                                                                    if Eq(
                                                                    Compare
                                                                    ( Abs(
                                                                    Sub
                                                                    ( Cdr(Cdr(s1337))
                                                                    , Cdr(Car(s1337))
                                                                    ))
                                                                    , Abs(
                                                                    Sub
                                                                    ( Car(Cdr(s1337))
                                                                    , Car(Car(s1337))
                                                                    ))
                                                                    ))
                                                                    then
                                                                    let x1430 =
                                                                    Abs(
                                                                    Sub
                                                                    ( Cdr(Cdr(s1337))
                                                                    , Cdr(Car(s1337))
                                                                    ))
                                                                    let x1453 =
                                                                    Abs(
                                                                    Sub
                                                                    ( Car(Cdr(s1337))
                                                                    , Car(Car(s1337))
                                                                    ))
                                                                    let 
                                                                    [s1469;
                                                                    s1470;
                                                                    s1471;
                                                                    s1472] =
                                                                    if Le(
                                                                    Compare
                                                                    ( x1453
                                                                    , x1430
                                                                    ))
                                                                    then
                                                                    [ x1430
                                                                    ; 1
                                                                    ; s1337
                                                                    ; s1338
                                                                    ]
                                                                    else
                                                                    [ x1453
                                                                    ; 1
                                                                    ; s1337
                                                                    ; s1338
                                                                    ]
                                                                    let x1481 =
                                                                    Sub
                                                                    ( Int(s1469)
                                                                    , s1470
                                                                    )
                                                                    let 
                                                                    [_] =
                                                                    loop 
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1481
                                                                    , 1
                                                                    ))
                                                                    ; 1
                                                                    ]
                                                                    step s1485 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Car(Car(s1471))
                                                                    , 
                                                                    Mul
                                                                    ( s1485
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Car(Cdr(s1471))
                                                                    , Car(Car(s1471))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , Car(Cdr(Car(s1472)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(88)
                                                                    | Some s1541 ->
                                                                    match 
                                                                    Get
                                                                    ( 
                                                                    Add
                                                                    ( Cdr(Car(s1471))
                                                                    , 
                                                                    Mul
                                                                    ( s1485
                                                                    , 
                                                                    Compare
                                                                    ( 
                                                                    Sub
                                                                    ( Cdr(Cdr(s1471))
                                                                    , Cdr(Car(s1471))
                                                                    )
                                                                    , 0
                                                                    )
                                                                    )
                                                                    )
                                                                    , s1541
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(88)
                                                                    | Some s1592 ->
                                                                    if Eq(
                                                                    Compare
                                                                    ( s1592
                                                                    , 0
                                                                    ))
                                                                    then
                                                                    let x1610 =
                                                                    Add
                                                                    ( 1
                                                                    , s1485
                                                                    )
                                                                    [ Gt(
                                                                    Compare
                                                                    ( x1481
                                                                    , x1610
                                                                    ))
                                                                    ; x1610
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: self.data.deck[params.f.i + (k * sp.sign(params.t.i - params.f.i))][params.f.j + (k * sp.sign(params.t.j - params.f.j))] == 0")
                                                                    end
                                                                    end
                                                                    [ s1471
                                                                    ; s1472
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: (abs(params.t.j - params.f.j)) == (abs(params.t.i - params.f.i))")
                                                                    else
                                                                    [ s1337
                                                                    ; s1338
                                                                    ]
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1623))
                                                                    , Car(Cdr(Car(s1624)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(89)
                                                                    | Some s1639 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s1623))
                                                                    , s1639
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(89)
                                                                    | Some s1652 ->
                                                                    let 
                                                                    [s1703;
                                                                    s1704] =
                                                                    if Eq(
                                                                    Compare
                                                                    ( Abs(s1652)
                                                                    , 3#nat
                                                                    ))
                                                                    then
                                                                    if Eq(
                                                                    Compare
                                                                    ( Abs(
                                                                    Mul
                                                                    ( 
                                                                    Sub
                                                                    ( Cdr(Cdr(s1623))
                                                                    , Cdr(Car(s1623))
                                                                    )
                                                                    , 
                                                                    Sub
                                                                    ( Car(Cdr(s1623))
                                                                    , Car(Car(s1623))
                                                                    )
                                                                    ))
                                                                    , 2#nat
                                                                    ))
                                                                    then
                                                                    [ s1623
                                                                    ; s1624
                                                                    ]
                                                                    else
                                                                    Failwith("WrongCondition: (abs((params.t.j - params.f.j) * (params.t.i - params.f.i))) == 2")
                                                                    else
                                                                    [ s1623
                                                                    ; s1624
                                                                    ]
                                                                    let x1731 =
                                                                    Car(Cdr(s1703))
                                                                    match 
                                                                    Get
                                                                    ( x1731
                                                                    , Car(Cdr(Car(s1704)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(91)
                                                                    | Some s1736 ->
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1703))
                                                                    , Car(Cdr(Car(s1704)))
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(91)
                                                                    | Some s1768 ->
                                                                    match 
                                                                    Get
                                                                    ( Cdr(Car(s1703))
                                                                    , s1768
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(91)
                                                                    | Some s1790 ->
                                                                    let x1817 =
                                                                    Update(x1731, Some_(Update(Cdr(Cdr(s1703)), Some_(s1790), s1736)), Car(Cdr(Car(s1704))))
                                                                    match 
                                                                    Get
                                                                    ( Car(Car(s1703))
                                                                    , x1817
                                                                    ) with
                                                                    | None _ ->
                                                                    Failwith(92)
                                                                    | Some s1833 ->
                                                                    Pair
                                                                    ( Nil<operation>
                                                                    , record_of_tree(..., 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(s1704))
                                                                    , 
                                                                    Pair
                                                                    ( Update(Car(Car(s1703)), Some_(Update(Cdr(Car(s1703)), Some_(0), s1833)), x1817)
                                                                    , Cdr(Cdr(Car(s1704)))
                                                                    )
                                                                    )
                                                                    , 
                                                                    Pair
                                                                    ( 
                                                                    Pair
                                                                    ( Car(Car(Cdr(s1704)))
                                                                    , Neg(Cdr(Car(Cdr(s1704))))
                                                                    )
                                                                    , Cdr(Cdr(s1704))
                                                                    )
                                                                    ))
                                                                    )
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                    end
                                                                  end
                                                              end
                                                          end
                                                      end
                                                  end
                                              end
                                          end
                                      end
                                    else
                                      Failwith("WrongCondition: sp.sign(self.data.deck[params.t.i][params.t.j]) != self.data.nextPlayer")
                                end
                            end
                          else
                            Failwith("WrongCondition: sp.sign(self.data.deck[params.f.i][params.f.j]) == self.data.nextPlayer")
                      end
                  end
                else
                  Failwith("WrongCondition: (params.t.j >= 0) & (params.t.j < 8)")
              else
                Failwith("WrongCondition: (params.t.i >= 0) & (params.t.i < 8)")
            else
              Failwith("WrongCondition: (params.f.j >= 0) & (params.f.j < 8)")
          else
            Failwith("WrongCondition: (params.f.i >= 0) & (params.f.i < 8)")
        else
          Failwith("WrongCondition: (self.data.winner == 0) & (~ self.data.draw)")]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
