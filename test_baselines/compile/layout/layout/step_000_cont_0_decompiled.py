import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TInt, b = sp.TNat).layout(("b", "a")))

  @sp.entry_point
  def run_record(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TPair(sp.TInt, sp.TBool)))

  @sp.entry_point
  def run_record_2(self, params):
    sp.set_type(params, sp.TPair(sp.TInt, sp.TPair(sp.TString, sp.TPair(sp.TBool, sp.TNat))))

  @sp.entry_point
  def run_type_record(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TInt))

  @sp.entry_point
  def run_variant(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TString, Right = sp.TVariant(Left = sp.TInt, Right = sp.TBool).layout(("Left", "Right"))).layout(("Left", "Right")))

  @sp.entry_point
  def run_type_variant(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TInt, Right = sp.TString).layout(("Left", "Right")))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
