import smartpy as sp

tstorage = sp.TRecord(a = sp.TIntOrNat, b = sp.TNat).layout(("b", "a"))
tparameter = sp.TVariant(run_record = sp.TRecord(a = sp.TInt, b = sp.TString, c = sp.TBool).layout(("b", ("a", "c"))), run_record_2 = sp.TRecord(a = sp.TInt, b = sp.TString, c = sp.TBool, d = sp.TNat).layout(("a", ("b", ("c", "d")))), run_type_record = sp.TRecord(a = sp.TInt, b = sp.TString).layout(("b", "a")), run_type_variant = sp.TVariant(d = sp.TString, e = sp.TInt).layout(("e", "d")), run_variant = sp.TVariant(a = sp.TInt, b = sp.TString, c = sp.TBool).layout(("b", ("a", "c")))).layout((("run_record", "run_record_2"), ("run_type_record", ("run_type_variant", "run_variant"))))
tglobals = { }
tviews = { }
