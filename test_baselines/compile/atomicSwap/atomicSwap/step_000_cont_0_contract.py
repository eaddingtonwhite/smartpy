import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(counterparty = sp.TAddress, epoch = sp.TTimestamp, hashedSecret = sp.TBytes, notional = sp.TMutez, owner = sp.TAddress).layout((("counterparty", "epoch"), ("hashedSecret", ("notional", "owner")))))
    self.init(counterparty = sp.address('tz1c3GoMtLJgd3pyodoG7BpHdXgbKYVfnK3N'),
              epoch = sp.timestamp(50),
              hashedSecret = sp.bytes('0x307839304336453830343039324344304138324531443132413043324538324238384544'),
              notional = sp.mutez(12),
              owner = sp.address('tz1c3GoMtLJgd3pyodoG7BpHdXgbKYVfnK3N'))

  @sp.entry_point
  def allSigned(self):
    sp.verify(self.data.notional != sp.tez(0))
    sp.verify(self.data.owner == sp.sender)
    sp.send(self.data.counterparty, self.data.notional)
    self.data.notional = sp.tez(0)

  @sp.entry_point
  def cancelSwap(self):
    sp.verify(self.data.notional != sp.tez(0))
    sp.verify(self.data.owner == sp.sender)
    sp.verify(self.data.epoch < sp.now)
    sp.send(self.data.owner, self.data.notional)
    self.data.notional = sp.tez(0)

  @sp.entry_point
  def knownSecret(self, params):
    sp.verify(self.data.notional != sp.tez(0))
    sp.verify(self.data.counterparty == sp.sender)
    sp.verify(self.data.hashedSecret == sp.blake2b(params.secret))
    sp.send(self.data.counterparty, self.data.notional)
    self.data.notional = sp.tez(0)