import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TNat, y = sp.TString, z = sp.TNat).layout(("x", ("y", "z"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right")))
    def f-1(lparams_-1):
      sp.failwith('[Error: to_cmd: (fun (x1 : { nat ; (nat ; (string ; nat)) }) : { list(operation) ; (nat ; (nat ; (string ; nat))) } ->
match Contract("tz0Fakealice"#address, unit) with
| None _ -> Failwith(9)
| Some s15 ->
    let x29 = Mul(Car(x1), 1000000#mutez)
    match Contract("tz0Fakebob"#address, unit) with
    | None _ -> Failwith(10)
    | Some s35 ->
        let x54 =
          let [x286; x287] = Unpair(2, Cdr(x1))
          let _ = x286
          Pair(Add(1#nat, Car(Cdr(x1))), x287)
        Pair
          ( Cons
              ( Transfer_tokens(Unit, 2000000#mutez, s35)
              , Cons(Transfer_tokens(Unit, x29, s15), Nil<operation>)
              )
          , Pair(Mul(Car(x1), Car(x54)), x54)
          )
    end
end)]')
    sp.failwith(sp.build_lambda(f-1)(sp.unit))

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
