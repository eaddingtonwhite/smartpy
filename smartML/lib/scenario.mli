(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics

type loaded_scenario =
  { scenario : tscenario
  ; scenario_state : Basics.scenario_state
  ; warnings : smart_except list list }

val load_from_string :
     primitives:(module Primitives.Primitives)
  -> Config.t
  -> Yojson.Basic.t
  -> loaded_scenario

val check_close_scenario : Config.t -> scenario -> loaded_scenario

val register :
     ?flags:Config.flag list
  -> group:string
  -> name:string
  -> kind:string
  -> action list
  -> unit

val get : string -> (Config.t -> loaded_scenario) list
