(* Copyright 2019-2021 Smart Chain Arena LLC. *)

val intType : bool Unknown.t -> [> `Int | `Nat | `Unknown ]

val mtype_of_type : ?with_annots:unit -> Type.t -> Michelson.mtype
