(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils

type sapling_test_state =
  { test : bool
  ; memo : int
  ; elements : (string * Bigint.t) list }
[@@deriving show, eq, ord]

type sapling_test_transaction =
  { source : string option
  ; target : string option
  ; amount : Bigint.t
  ; memo : int }
[@@deriving show, eq, ord]

type t = private
  | Unit
  | Bool                     of bool
  | Int                      of
      { i : Bigint.t
      ; is_nat : bool Unknown.t }
  | String                   of string
  | Bytes                    of string
  | Chain_id                 of string
  | Timestamp                of Bigint.t
  | Mutez                    of Bigint.t
  | Address                  of string * string option (* <- entry-point *)
  | Key                      of string
  | Secret_key               of string
  | Key_hash                 of string
  | Baker_hash               of string
  | Signature                of string
  | Sapling_test_state       of sapling_test_state
  | Sapling_test_transaction of sapling_test_transaction
  | Bls12_381_g1             of string
  | Bls12_381_g2             of string
  | Bls12_381_fr             of string
[@@deriving eq, ord, show]

val unit : t

val bool : bool -> t

val int : Bigint.t -> t

val nat : Bigint.t -> t

val intOrNat : bool Unknown.t -> Bigint.t -> t

val small_int : int -> t

val small_nat : int -> t

val string : string -> t

val bytes : string -> t

val chain_id : string -> t

val timestamp : Bigint.t -> t

val mutez : Bigint.t -> t

val address : ?entry_point:string -> string -> t

val key : string -> t

val secret_key : string -> t

val key_hash : string -> t

val baker_hash : string -> t

val signature : string -> t

val sapling_test_state : int -> (string * Bigint.t) list -> t

val sapling_state_real : int -> t

val sapling_test_transaction :
  int -> string option -> string option -> Bigint.t -> t

val unBool : t -> bool option

val unInt : t -> Bigint.t option

val unAddress : t -> (string * string option) option

val bls12_381_g1 : string -> t

val bls12_381_g2 : string -> t

val bls12_381_fr : string -> t
