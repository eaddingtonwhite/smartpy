(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Michelson

val compile_contract :
     ?storage:literal
  -> ?lazy_entry_points:literal
  -> config:Config.t
  -> Michel.Typing.checked_precontract
  -> Michelson.tcontract
