(* Copyright 2019-2021 Smart Chain Arena LLC. *)

(** {1 Contract manipulation and evaluation.} *)

type t = Basics.instance [@@deriving show]

(** {2 Website Implementation} *)

val execMessageInner :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:Basics.scenario_state
  -> title:string
  -> execMessageClass:string
  -> context:Interpreter.context
  -> initContract:t
  -> channel:string
  -> params:Basics.value
  -> string Basics.Execution.exec_message

(** Evaluate a contract call with {!eval} and output an HTML result. *)
