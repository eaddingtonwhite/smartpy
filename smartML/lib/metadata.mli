(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Basics

val for_contract :
  config:Config.t -> instance -> (string * Utils.Misc.json) list
