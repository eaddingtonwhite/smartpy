(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils
open Smartml
open Utils.Misc
open Html

let show_full_contract
    ~config tstorage tparameter mtparameter mtstorage storage code =
  let code = Michelson.Of_micheline.instruction code in
  let storage = Michelson.Of_micheline.literal storage in
  let contract =
    Michelson.typecheck_contract
      ~strict_dup:false
      { tparameter = mtparameter
      ; tstorage = mtstorage
      ; lazy_entry_points = None
      ; storage = Some storage
      ; code }
  in
  let simplified_contract =
    Michelson_rewriter.(
      run_on_tcontract (simplify ~config:Config.default) contract)
  in
  let module Printer = (val Printer.get config : Printer.Printer) in
  let types =
    tabs
      ~global_id:1
      "Contract:"
      [ tab
          ~active:()
          "Storage Type"
          (Raw (Printer.type_to_string ~options:Printer.Options.html tstorage))
      ; tab
          "Parameter Type"
          (Raw (Printer.type_to_string ~options:Printer.Options.html tparameter))
      ; tab
          "Michelson"
          (Html.michelson_html
             ~title:"Michelson"
             ~lazy_tabs:true
             ~id:"0"
             ~simplified_contract
             contract)
        (* ; tab
         *     "SmartPy"
         *     ~lazy_tab:
         *       ( lazy
         *         (Raw
         *            (Printf.sprintf
         *               "Decompilation is experimental!<br><div \
         *                class='white-space-pre'>%s</div>"
         *               (Lazy.force decompiled))) )
         *     (Raw "") *)
        (* TODO Show correct balance. *) ]
  in
  SmartDom.setText "types" (render types)

let show_operation
    getParameter
    getStorage
    (operation, details, params, storage, errors, json, is_receiver) =
  let module Printer = ( val Printer.get_by_language Config.SmartPy
                           : Printer.Printer )
  in
  let params =
    match params with
    | `Error error -> error
    | `OK (branch, parameters) ->
        let params =
          Base.String.fold
            ~init:(Micheline.parse parameters)
            ~f:(fun params c ->
              if c = 'L' then Micheline.left params else Micheline.right params)
            (Base.String.rev branch)
        in
        ( try
            let params = getParameter params in
            Printer.value_to_string ~options:Printer.Options.html params
          with
        | exn -> Printer.exception_to_string true exn )
  in
  let storage =
    match storage with
    | `Error error -> error
    | `OK storage ->
      ( try
          let storage_value = getStorage (Micheline.parse storage) in
          Printer.value_to_string ~options:Printer.Options.html storage_value
        with
      | exn -> Printer.exception_to_string true exn )
  in
  let errors =
    match errors with
    | None -> ""
    | Some errors -> Printf.sprintf "<h4>Errors</h4>%s" errors
  in
  let full_data =
    Printf.sprintf
      "<button class='centertextbutton' onClick='popupJson(\"Operation \
       Details\", %s)'>View Full Data</button>"
      (Base.String.substr_replace_all
         (Yojson.Basic.to_string json)
         ~pattern:"'"
         ~with_:"&apos")
  in
  if is_receiver
  then
    Printf.sprintf
      "<div \
       class='operation'><h4>Operation</h4>%s%s<h4>Details</h4>%s<h4>Parameters</h4>%s<h4>Storage</h4>%s%s</div>"
      operation
      full_data
      details
      params
      storage
      errors
  else
    Printf.sprintf
      "<div class='operation'><h4>Outbound Internal \
       Operation</h4>%s%s<h4>Details</h4>%s%s</div>"
      operation
      full_data
      details
      errors

(* TODO go through Michel types *)
let type_of_mtype ?wrap =
  let open Type in
  let f ?annot_type:_ ?annot_variable:_ mt ~wrap =
    let t =
      match (mt : _ Michelson.mtype_f) with
      | MT0 T_unit -> unit
      | MT0 T_bool -> bool
      | MT0 T_nat -> nat ()
      | MT0 T_int -> int ()
      | MT0 T_mutez -> token
      | MT0 T_string -> string
      | MT0 T_bytes -> bytes
      | MT0 T_timestamp -> timestamp
      | MT0 T_address -> address
      | MT0 T_key -> key
      | MT0 T_key_hash -> key_hash
      | MT0 T_baker_hash -> baker_hash
      | MT0 T_signature -> signature
      | MT0 T_operation -> operation
      | MT0 (T_sapling_state {memo}) -> sapling_state (Some memo)
      | MT0 (T_sapling_transaction {memo}) -> sapling_transaction (Some memo)
      | MT0 T_never -> never
      | MT0 T_bls12_381_g1 -> bls12_381_g1
      | MT0 T_bls12_381_g2 -> bls12_381_g2
      | MT0 T_bls12_381_fr -> bls12_381_fr
      | MT1 (T_option, t) -> option (t ~wrap:None)
      | MT1 (T_list, t) -> list (t ~wrap:None)
      | MT1 (T_ticket, t) -> ticket (t ~wrap:None)
      | MT1 (T_set, telement) -> set ~telement:(telement ~wrap:None)
      | MT1 (T_contract, t) -> contract (t ~wrap:None)
      | MT2 (T_pair {annot_fst; annot_snd}, fst, snd) ->
          let fst = fst ~wrap:(Option.map (Control.pair `Record) annot_fst) in
          let snd = snd ~wrap:(Option.map (Control.pair `Record) annot_snd) in
          let mk xl yl xs ys =
            record (Unknown.value (Binary_tree.node xl yl)) (xs @ ys)
          in
          ( match (Type.getRepr fst, Type.getRepr snd) with
          | TRecord {layout = xl; row = xs}, TRecord {layout = yl; row = ys} ->
            (* TODO If names clash, stick to a pair/or. *)
            ( match (Unknown.get xl, Unknown.get yl) with
            | Some xl, Some yl -> mk xl yl xs ys
            | _ -> pair fst snd )
          | _ -> pair fst snd )
      | MT2 (T_or {annot_left; annot_right}, left, right) ->
          let left =
            left ~wrap:(Option.map (Control.pair `Variant) annot_left)
          in
          let right =
            right ~wrap:(Option.map (Control.pair `Variant) annot_right)
          in
          ( match (Type.getRepr left, Type.getRepr right) with
          | TVariant {layout = xl; row = xs}, TVariant {layout = yl; row = ys}
            ->
            ( match (Unknown.get xl, Unknown.get yl) with
            | Some xl, Some yl ->
                variant (Unknown.value (Binary_tree.node xl yl)) (xs @ ys)
            | _ -> tor left right )
          | _ -> tor left right )
      | MT2 (T_lambda, t, u) -> lambda (t ~wrap:None) (u ~wrap:None)
      | MT2 (T_map, tkey, tvalue) ->
          let tkey = tkey ~wrap:None in
          let tvalue = tvalue ~wrap:None in
          map ~big:false ~tkey ~tvalue
      | MT2 (T_big_map, tkey, tvalue) ->
          let tkey = tkey ~wrap:None in
          let tvalue = tvalue ~wrap:None in
          map ~big:true ~tkey ~tvalue
      | MT0 T_chain_id -> chain_id
      | MT_var s -> failwith ("type_of_mtype: MT_var " ^ s)
    in
    match wrap with
    | Some (`Record, a) -> record (Unknown.value (Layout.leaf a a)) [(a, t)]
    | Some (`Variant, a) -> variant (Unknown.value (Layout.leaf a a)) [(a, t)]
    | None -> t
  in
  Michelson.cata_mtype ~wrap f

let messageBuilder
    ~config ~address init_storage tstorage tparameter code operations =
  let mtstorage = Michelson.Of_micheline.mtype tstorage in
  let tstorage = type_of_mtype mtstorage in
  let pp_mich t i =
    Michelson.display_instr
      (Typing.mtype_of_type ~with_annots:() t)
      (Michelson.Of_micheline.instruction i)
  in
  let getStorage = Value.of_micheline ~config ~pp_mich tstorage in
  let storage = getStorage init_storage in
  let addresses =
    Value.project_literals
      (function
        | Literal.Address (address, _) -> Some address
        | Literal.Key_hash a -> Some a
        | _ -> None)
      storage
  in
  let mtparameter, parameterAnnot =
    Michelson.Of_micheline.mtype_annotated tparameter
  in
  let tparameter =
    type_of_mtype
      ?wrap:(Option.map (fun t -> (`Variant, t)) parameterAnnot)
      mtparameter
  in
  let getParameter = Value.of_micheline ~config ~pp_mich tparameter in
  let editor =
    let name = "contractId" in
    let accountName = "accountId" in
    let buttonText = "Build Transaction Parameters" in
    let output = nextOutputGuiId () in
    let id = nextInputGuiId () ^ "." in
    let nextId = Value.nextId id in
    let input = Value_gui.inputGuiR ~nextId tparameter in
    Printf.sprintf
      "<div class='simulationBuilder'><form><button type='button' \
       class='explorer_button' onClick=\"cleanMessages();t = \
       smartmlCtx.call_exn_handler('importType', '%s'); if (t) \
       smartmlCtx.call_exn_handler('buildTransfer', '%s', '%s', %s.value, \
       %s.value, t)\">%s</button><br>%s</form>\n\
       <div id='%s'></div></div>"
      (Export.export_type tparameter)
      id
      output
      name
      accountName
      buttonText
      input.gui
      output
  in
  let custom_metadata, tzcomet =
    let open Basics in
    let show_metadata k v =
      match v.v with
      | Literal (Literal.String s) ->
          let explore =
            if Base.String.is_prefix s ~prefix:"http://"
               || Base.String.is_prefix s ~prefix:"https://"
            then Printf.sprintf "<a href='%s' target=_blank>Follow link</a>" s
            else if Base.String.is_prefix s ~prefix:"ipfs://"
            then
              let s = Base.String.drop_prefix s 7 in
              Printf.sprintf
                "<a href='https://gateway.ipfs.io/ipfs/%s' \
                 target=_blank>Follow IPFS link</a>"
                s
            else ""
          in
          [`V k; `V v; `T explore]
      | _ -> [`V k; `V v; `T ""]
    in
    match Value.get_field_opt "metadata" storage with
    | Some {v = Map (_, _, _, [])} -> ([], true)
    | Some {v = Map (_, _, _, l)} ->
        (List.map (fun (k, v) -> show_metadata k v) l, true)
    | _ ->
      ( match Value.get_field_opt "metaData" storage with
      | Some {v = Map (_, _, _, l)} ->
          (List.map (fun (k, v) -> show_metadata k v) l, true)
      | _ -> ([], false) )
  in
  let module Printer = ( val Printer.get_by_language Config.SmartPy
                           : Printer.Printer )
  in
  let custom_metadata =
    let viewer =
      Printf.sprintf
        "<a \
         href='https://tqtezos.github.io/TZComet/#/explorer%%3Fexplorer-input%%3D%s&go=true' \
         target=_blank>Explore with TZComet</a>"
        address
    in
    match custom_metadata with
    | [] -> if tzcomet then Printf.sprintf "<h4>Metadata</h4>%s" viewer else ""
    | _ ->
        Printf.sprintf
          "<h4>Metadata</h4>%s<div id='storageMetaDataDiv'>%s</div>"
          (if tzcomet then viewer else "")
          (Printer.html_of_record_list
             (["Name"; "Metadata"; "Explore"], custom_metadata)
             (function
               | `V s ->
                   Printer.value_to_string
                     ~noEmptyList:true
                     ~options:Printer.Options.htmlStripStrings
                     s
               | `T t -> t))
  in
  let addresses =
    match addresses with
    | [] -> ""
    | _ ->
        let addresses =
          List.map
            (fun (address, path) ->
              let explore =
                let s = address in
                let s =
                  if String.contains s '%'
                  then String.sub s 0 (String.index s '%')
                  else s
                in
                let tzkt =
                  Printf.sprintf
                    "<a href='https://tzkt.io/%s/operations' \
                     target=_blank>TzKT</a>"
                    s
                in
                if Base.String.is_prefix s ~prefix:"tz"
                then tzkt
                else
                  Printf.sprintf
                    "<a href='explorer.html?address=%s'>SmartPy Explorer</a>, \
                     <a href='#' \
                     onClick='window.open(\"https://better-call.dev/search?text=%s\")'>Better \
                     Call Dev</a>, %s"
                    s
                    s
                    tzkt
              in
              [ `V (Value.string (String.concat "." (List.rev path)))
              ; `V (Value.literal (Literal.address address))
              ; `T explore ])
            addresses
        in
        Printf.sprintf
          "<h4>Addresses</h4><div id='storageAddressesDiv'>%s</div>"
          (Printer.html_of_record_list
             (["Path"; "Address"; "Explore"], addresses)
             (function
               | `V s ->
                   Printer.value_to_string
                     ~noEmptyList:true
                     ~options:Printer.Options.htmlStripStrings
                     s
               | `T t -> t))
  in
  SmartDom.setText
    "storageDiv"
    (Printf.sprintf
       "<h3>Storage</h3><div id='storageDivInternal'>%s</div>%s%s"
       (Printer.value_to_string ~options:Printer.Options.html storage)
       addresses
       custom_metadata);
  SmartDom.setText "messageBuilder" editor;
  show_full_contract
    ~config
    tstorage
    tparameter
    (mtparameter, parameterAnnot)
    mtstorage
    init_storage
    code;
  let operations =
    String.concat
      "<hr>"
      (List.rev_map (show_operation getParameter getStorage) operations)
  in
  SmartDom.setText "operationsList" operations

let parseTZStatAPI ~config address operation_ =
  let operation = json_getter operation_ in
  let module M = (val operation : JsonGetter) in
  let string x = Value.string (Yojson.Basic.to_string (M.get x)) in
  let bool x = Value.bool (M.bool x) in
  let get_protect field =
    try M.string field with
    | _ -> Printf.sprintf "Missing field '%s'" field
  in
  let receiver = get_protect "receiver" in
  let is_receiver = address = receiver in
  let mainlines =
    let fields =
      [ ("date and time", fun _ -> Value.string (M.string "time"))
      ; ("sender", fun x -> Value.address (M.string x))
      ; ( "receiver"
        , fun x ->
            try Value.address (M.string x) with
            | _ -> Value.string receiver )
      ; ("block", fun x -> Value.address (M.string x))
      ; ("hash", fun x -> Value.address (M.string x)) ]
    in
    List.map (fun (field, f) -> (String.capitalize_ascii field, f field)) fields
  in
  let sublines =
    let fields =
      [ ("status", fun x -> Value.string (M.string x))
      ; ("is_success", bool)
      ; ("height", string)
      ; ("cycle", string)
      ; ("counter", string)
      ; ("gas_limit", string)
      ; ("gas_used", string)
      ; ("gas_price", string)
      ; ("storage_limit", string)
      ; ("storage_size", string)
      ; ("storage_paid", string)
      ; ("volume", string)
      ; ("fee", string)
      ; ("reward", string)
      ; ("deposit", string)
      ; ("burned", string) ]
    in
    List.map (fun (field, f) -> (String.capitalize_ascii field, f field)) fields
  in
  let module Printer = (val Printer.get config : Printer.Printer) in
  let metaData =
    Printer.value_to_string
      ~options:Printer.Options.htmlStripStrings
      (Value.record_comb mainlines)
  in
  let details =
    Printer.value_to_string
      ~options:Printer.Options.htmlStripStrings
      (Value.record_comb sublines)
  in
  let parameters =
    let parameters = M.get "parameters" in
    match parameters with
    | `Null -> `Error "No parameters"
    | _ ->
        let module M = (val json_getter parameters : JsonGetter) in
        `OK (M.string "branch", M.get "prim")
  in
  let storage =
    match M.get "storage" with
    | `Null -> `Error "No storage"
    | storage ->
        let module M = (val json_getter storage : JsonGetter) in
        ( try `OK (M.get "prim") with
        | exn -> `Error (Printer.exception_to_string true exn) )
  in
  let errors =
    match M.get "errors" with
    | `Null -> None
    | errors -> Some (Yojson.Basic.to_string errors)
  in
  (metaData, details, parameters, storage, errors, operation_, is_receiver)

let explorerShowContractInfos ~config address balance =
  let module Printer = (val Printer.get config : Printer.Printer) in
  let lines =
    [ ("Address", Value.address address)
    ; ("Balance", Value.mutez (Big_int.big_int_of_string balance)) ]
  in
  SmartDom.setText
    "contractDataDiv"
    (Printer.value_to_string
       ~options:Printer.Options.html
       (Value.record_comb lines))

let explore ~config ~address ~json ~operations =
  let json = json_getter (Yojson.Basic.from_string json) in
  let module M = (val json : JsonGetter) in
  let open M in
  explorerShowContractInfos ~config address (string "balance");
  let script = json_sub json "script" in
  let module Script = (val script : JsonGetter) in
  if Script.null
  then
    SmartDom.setText
      "messageBuilder"
      "<span style='color:grey;'>&mdash; no contract code &mdash;</span>"
  else
    let code = Script.get "code" in
    let storage = Script.get "storage" in
    let tparameter, tstorage, code =
      match code with
      | `List
          [ `Assoc [("prim", `String "parameter"); ("args", `List [parameter])]
          ; `Assoc [("prim", `String "storage"); ("args", `List [storage])]
          ; `Assoc [("prim", `String "code"); ("args", `List [code])] ] ->
          (parameter, storage, code)
      | `Assoc _ -> assert false
      | _ -> assert false
    in
    let operations =
      match operations with
      | "" -> []
      | _ ->
        ( match Yojson.Basic.from_string operations with
        | `List operations ->
            List.map (parseTZStatAPI ~config address) operations
        | _ -> assert false )
    in
    let parse = Micheline.parse in
    messageBuilder
      ~config
      ~address
      (parse storage)
      (parse tstorage)
      (parse tparameter)
      (parse code)
      operations
