(* Copyright 2019-2021 Smart Chain Arena LLC. *)

include Smartml.Primitives.Primitives

val test : (module Smartml.Primitives.Primitives) -> unit
