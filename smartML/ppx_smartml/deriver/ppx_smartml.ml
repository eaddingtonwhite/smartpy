(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Ppxlib

exception Err of location * string

let () =
  let open Location.Error in
  register_error_of_exn (function
      | Err (loc, msg) -> Some (make ~loc msg ~sub:[])
      | _ -> None)

let err loc = Printf.ksprintf (fun msg -> raise (Err (loc, msg)))

let err_unknown_identifier loc x = err loc "Unknown identifier: %s" x

let rec to_list loc = function
  | [] -> [%expr []]
  | a :: l -> [%expr [%e a] :: [%e to_list loc l]]

let ast_string s = Ast_helper.Exp.constant (Pconst_string (s, None))

let ast_integer i =
  Ast_helper.Exp.constant (Pconst_integer (string_of_int i, None))

let expr_of_lid lid =
  match lid.txt with
  | Lident s -> ast_string s
  | _ -> assert false

let faildesc pexp_desc =
  match pexp_desc with
  | Pexp_record _ -> failwith "Pexp_record"
  | Pexp_field _ -> failwith "Pexp_field"
  | Pexp_ident _ -> failwith "Pexp_ident"
  | Pexp_setfield _ -> failwith "Pexp_setfield"
  | Pexp_apply _ -> failwith "Pexp_apply"
  | Pexp_sequence _ -> failwith "Pexp_sequence"
  | Pexp_constant _ -> failwith "Pexp_constant"
  | Pexp_let _ -> failwith "Pexp_let"
  | Pexp_function _ -> failwith "Pexp_function"
  | Pexp_fun _ -> failwith "Pexp_fun"
  | Pexp_match _ -> failwith "Pexp_match"
  | Pexp_try _ -> failwith "Pexp_try"
  | Pexp_tuple _ -> failwith "Pexp_tuple"
  | Pexp_construct _ -> failwith "Pexp_construct"
  | Pexp_variant _ -> failwith "Pexp_variant"
  | Pexp_array _ -> failwith "Pexp_array"
  | Pexp_ifthenelse _ -> failwith "Pexp_ifthenelse"
  | Pexp_while _ -> failwith "Pexp_while"
  | Pexp_for _ -> failwith "Pexp_for"
  | Pexp_constraint _ -> failwith "Pexp_constraint"
  | Pexp_coerce _ -> failwith "Pexp_coerce"
  | Pexp_send _ -> failwith "Pexp_send"
  | Pexp_new _ -> failwith "Pexp_new"
  | Pexp_setinstvar _ -> failwith "Pexp_setinstvar"
  | Pexp_override _ -> failwith "Pexp_override"
  | Pexp_letmodule _ -> failwith "Pexp_letmodule"
  | Pexp_letexception _ -> failwith "Pexp_letexception"
  | Pexp_assert _ -> failwith "Pexp_assert"
  | Pexp_lazy _ -> failwith "Pexp_lazy"
  | Pexp_poly _ -> failwith "Pexp_poly"
  | Pexp_object _ -> failwith "Pexp_object"
  | Pexp_newtype _ -> failwith "Pexp_newtype"
  | Pexp_pack _ -> failwith "Pexp_pack"
  | Pexp_open _ -> failwith "Pexp_open"
  | Pexp_letop _ -> failwith "Pexp_letop"
  | Pexp_extension _ -> failwith "Pexp_extension"
  | Pexp_unreachable -> failwith "Pexp_unreachable"

let failtdesc = function
  | Ptyp_any -> failwith "failtdesc Ptyp_any"
  | Ptyp_var _ -> failwith "failtdesc Ptyp_var"
  | Ptyp_arrow _ -> failwith "failtdesc Ptyp_arrow"
  | Ptyp_tuple _ -> failwith "failtdesc Ptyp_tuple"
  | Ptyp_constr _ -> failwith "failtdesc Ptyp_constr"
  | Ptyp_object _ -> failwith "failtdesc Ptyp_object"
  | Ptyp_class _ -> failwith "failtdesc Ptyp_class"
  | Ptyp_alias _ -> failwith "failtdesc Ptyp_alias"
  | Ptyp_variant _ -> failwith "failtdesc Ptyp_variant"
  | Ptyp_poly _ -> failwith "failtdesc Ptyp_poly"
  | Ptyp_package _ -> failwith "failtdesc Ptyp_package"
  | Ptyp_extension _ -> failwith "failtdesc Ptyp_extension"

type ('a, 'b) env =
  { ctxt : 'a
  ; actions : 'b
  ; contract_static_ids : (string, int) Hashtbl.t
  ; next_id : unit -> int }

let line_no_of_loc loc =
  to_list loc [[%expr "", [%e ast_integer loc.loc_start.pos_lnum]]]

let rec parse_expr ?env =
  let contract_id var =
    match env with
    | None -> None
    | Some env -> Hashtbl.find_opt env.contract_static_ids var
  in
  let rec parse_expr expr =
    let loc = expr.pexp_loc in
    let line_no = line_no_of_loc loc in
    match expr with
    | [%expr ()] -> [%expr Expr.cst ~line_no:[%e line_no] Literal.unit]
    | [%expr true] -> [%expr Expr.cst ~line_no:[%e line_no] (Literal.bool true)]
    | [%expr false] ->
        [%expr Expr.cst ~line_no:[%e line_no] (Literal.bool false)]
    | {pexp_desc = Pexp_constant (Pconst_integer (i, _)); _} ->
        [%expr
          Expr.cst
            ~line_no:[%e line_no]
            (Literal.intOrNat
               (Unknown.unknown ())
               (Big_int.big_int_of_string [%e ast_string i]))]
    | {pexp_desc = Pexp_constant (Pconst_string (_, _) as i); _} ->
        [%expr
          Expr.cst
            ~line_no:[%e line_no]
            (Literal.string [%e Ast_helper.Exp.constant i])]
    | {pexp_desc = Pexp_apply (e, args); _} -> apply ~loc e args
    | [%expr self.data] -> [%expr Expr.storage ~line_no:[%e line_no]]
    | {pexp_desc = Pexp_field (expr, attribute); _} ->
        let contract_id =
          match expr.pexp_desc with
          | Pexp_ident {txt = Lident s; _} -> contract_id s
          | _ -> None
        in
        begin
          match contract_id with
          | None ->
              [%expr
                Expr.attr
                  ~line_no:[%e line_no]
                  [%e parse_expr expr]
                  [%e expr_of_lid attribute]]
          | Some id ->
            ( match attribute.txt with
            | Lident "data" ->
                [%expr
                  Expr.contract_data
                    ~line_no:[%e line_no]
                    (Ids.C_static {static_id = [%e ast_integer id]})]
            | _ -> assert false )
        end
    | {pexp_desc = Pexp_record (fields, None (*record with*)); _} ->
        let fields =
          List.map
            (fun (name, field) ->
              [%expr [%e expr_of_lid name], [%e parse_expr field]])
            fields
        in
        let fields = to_list loc fields in
        [%expr Expr.record ~line_no:[%e line_no] [%e fields]]
    | {pexp_desc = Pexp_ident lid; _} ->
        let expr_or_lid = function
          | Lident "sender" -> [%expr Expr.sender ~line_no:[%e line_no]]
          | Lident "source" -> [%expr Expr.source ~line_no:[%e line_no]]
          | Lident "balance" -> [%expr Expr.balance ~line_no:[%e line_no]]
          | Lident "amount" -> [%expr Expr.amount ~line_no:[%e line_no]]
          | Lident "level" -> [%expr Expr.level ~line_no:[%e line_no]]
          | Lident "now" -> [%expr Expr.now ~line_no:[%e line_no]]
          | Lident "chain_id" -> [%expr Expr.chain_id ~line_no:[%e line_no]]
          | Lident "params" -> [%expr Expr.params ~line_no:[%e line_no]]
          | Lident "total_voting_power" ->
              [%expr Expr.total_voting_power ~line_no:[%e line_no]]
          | Lident s ->
            begin
              match contract_id s with
              | None ->
                  [%expr Expr.local ~line_no:[%e line_no] [%e ast_string s]]
              | Some var ->
                  [%expr
                    Expr.local
                      ~line_no:[%e line_no]
                      [%e ast_string (s ^ string_of_int var)]]
            end
          | Ldot _ -> assert false
          | Lapply _ -> assert false
        in
        expr_or_lid lid.txt
    (*  | [%expr self.data <- [%e? _]] -> failwith "set data"*)
    | {pexp_desc = Pexp_extension ({txt = "e"; _}, PStr [{pstr_desc; _}]); _} ->
      begin
        match pstr_desc with
        | Pstr_eval (e, []) -> e
        | _ -> assert false
      end
    | {pexp_desc; _} -> faildesc pexp_desc
  and apply ~loc e args =
    let line_no = line_no_of_loc loc in
    match (e, args) with
    | {pexp_desc = Pexp_ident {txt = Lident ident; _}; _}, [(Nolabel, e1)] ->
        let f =
          match ident with
          | "~-" -> [%expr Expr.negE]
          | "not" -> [%expr Expr.notE]
          | "abs" -> [%expr Expr.absE]
          | "implicit_account" -> [%expr Expr.implicit_account]
          | "is_nat" -> [%expr Expr.is_nat]
          | "left" -> [%expr Expr.left]
          | "right" -> [%expr Expr.right]
          | "sign" -> [%expr Expr.sign]
          | "some" -> [%expr Expr.some]
          | "sum" -> [%expr Expr.sum]
          | "to_int" -> [%expr Expr.to_int]
          | "voting_power" -> [%expr Expr.voting_power]
          | _ -> err_unknown_identifier e.pexp_loc ident
        in
        [%expr [%e f] ~line_no:[%e line_no] [%e parse_expr e1]]
    | ( {pexp_desc = Pexp_ident {txt = Lident ident; _}; _}
      , [(Nolabel, e1); (Nolabel, e2)] ) ->
        let f =
          match ident with
          | "=" -> [%expr Expr.eq]
          | "<>" -> [%expr Expr.neq]
          | "*" -> [%expr Expr.mul]
          | ">" -> [%expr Expr.gt]
          | "=>" -> [%expr Expr.gt]
          | "<" -> [%expr Expr.lt]
          | "<=" -> [%expr Expr.le]
          | "/" -> [%expr Expr.div]
          | "%" -> [%expr Expr.e_mod]
          | "ediv" -> [%expr Expr.ediv]
          | "+" -> [%expr Expr.add]
          | "-" -> [%expr Expr.sub]
          | "||" -> [%expr Expr.b_or]
          | "&&" -> [%expr Expr.b_and]
          | "mul" -> [%expr Expr.mul_overloaded]
          | "min" -> [%expr Expr.min]
          | "max" -> [%expr Expr.max]
          (* val xor : bin_expr *)
          | _ -> err_unknown_identifier e.pexp_loc ident
        in
        [%expr
          [%e f] ~line_no:[%e line_no] [%e parse_expr e1] [%e parse_expr e2]]
    | _ -> assert false
  in
  parse_expr

and parse_type =
  let parse_type (typ : core_type) =
    let loc = typ.ptyp_loc in
    let _line_no = line_no_of_loc loc in
    match typ.ptyp_desc with
    | Ptyp_constr ({txt = Lident cst_type; _}, []) ->
      begin
        match cst_type with
        | "bool" -> [%expr Type.bool]
        | "int" -> [%expr Type.int ()]
        | "nat" -> [%expr Type.nat ()]
        | "intOrNat" -> [%expr Type.intOrNat ()]
        | "string" -> [%expr Type.string]
        | "bytes" -> [%expr Type.bytes]
        | "chain_id" -> [%expr Type.chain_id]
        | "mutez" -> [%expr Type.token]
        | "timestamp" -> [%expr Type.timestamp]
        | "address" -> [%expr Type.address]
        | "key_hash" -> [%expr Type.key_hash]
        | "baker_hash" -> [%expr Type.baker_hash]
        | "key" -> [%expr Type.key]
        | "signature" -> [%expr Type.signature]
        | "operation" -> [%expr Type.operation]
        | "bls12_381_g1" -> [%expr Type.bls12_381_g1]
        | "bls12_381_g2" -> [%expr Type.bls12_381_g2]
        | "bls12_381_fr" -> [%expr Type.bls12_381_fr]
        | "never" -> [%expr Type.never]
        | cst_type -> failwith ("Type format error atom " ^ cst_type)
      end
    | _ -> assert false
    (*faildesc typ.pexp_desc*)
  in
  parse_type

and parse_command ?env =
  let parse_expr = parse_expr ?env in
  let rec parse_command command =
    let loc = command.pexp_loc in
    let line_no = line_no_of_loc loc in
    match command with
    | [%expr ()] -> [%expr Command.seq ~line_no:[%e line_no] []]
    | [%expr
        [%e? e1];
        [%e? e2]] ->
        let e1 = parse_command e1 in
        let e2 = parse_command e2 in
        [%expr Command.seq ~line_no:[%e line_no] [[%e e1]; [%e e2]]]
    | [%expr self.data <- [%e? e]] ->
        [%expr
          Command.set
            ~line_no:[%e line_no]
            (Expr.storage ~line_no:[%e line_no])
            [%e parse_expr e]]
    | {pexp_desc = Pexp_setfield (e1, attribute, e2); _} ->
        [%expr
          Command.set
            ~line_no:[%e line_no]
            (Expr.attr
               ~line_no:[%e line_no]
               [%e parse_expr e1]
               [%e expr_of_lid attribute])
            [%e parse_expr e2]]
    | [%expr
        while [%e? cond] do
          [%e? command]
        done] ->
        [%expr
          Command.whileLoop
            ~line_no:[%e line_no]
            [%e parse_expr cond]
            [%e parse_command command]]
    | [%expr List.iter (fun [%p? p] -> [%e? command]) [%e? e]] ->
        let p =
          match p.ppat_desc with
          | Ppat_var {txt = x; _} -> x
          | Ppat_any -> "_"
          | _ -> assert false
        in
        [%expr
          Command.forGroup
            ~line_no:[%e line_no]
            [%e ast_string p]
            [%e parse_expr e]
            [%e parse_command command]]
    | [%expr List.iter [%e? command] [%e? e]] ->
        [%expr
          Command.forGroup
            ~line_no:[%e line_no]
            "a"
            [%e parse_expr e]
            [%e parse_command command]]
    | [%expr failwith [%e? e]] ->
        [%expr Command.sp_failwith ~line_no:[%e line_no] [%e parse_expr e]]
    | [%expr if [%e? cond] then [%e? then_]] ->
        [%expr
          Command.ifte
            ~line_no:[%e line_no]
            [%e parse_expr cond]
            [%e parse_command then_]
            (Command.seq ~line_no:[%e line_no] [])]
    | [%expr if [%e? cond] then [%e? then_] else [%e? else_]] ->
        [%expr
          Command.ifte
            ~line_no:[%e line_no]
            [%e parse_expr cond]
            [%e parse_command then_]
            [%e parse_command else_]]
    | {pexp_desc = Pexp_ident {txt = Lident ident; _}; _} -> apply ~loc ident []
    | { pexp_desc =
          Pexp_apply ({pexp_desc = Pexp_ident {txt = Lident ident; _}; _}, args)
      ; _ } ->
        apply ~loc ident args
    | {pexp_desc = Pexp_match (e, cases); _} ->
        let parse_case = function
          | { pc_lhs = {ppat_desc = Ppat_construct ({txt = Lident v; _}, arg); _}
            ; pc_guard = None
            ; pc_rhs } ->
              let command = parse_command pc_rhs in
              let arg =
                match arg with
                | Some {ppat_desc = Ppat_any; _} | None -> "_no_arg"
                | Some {ppat_desc = Ppat_var {txt = arg; _}; _} -> arg
                | _ -> assert false
              in
              [%expr [%e ast_string v], [%e ast_string arg], [%e command]]
          | _ -> failwith "no guard allowed"
        in
        let cases = List.map parse_case cases in
        [%expr
          Command.mk_match
            ~line_no:[%e line_no]
            [%e parse_expr e]
            [%e to_list loc cases]]
    | _ -> faildesc command.pexp_desc
  and apply ~loc ident (args : _ list) =
    let single_commands =
      let set_type ident args loc =
        let err () =
          err
            loc
            "SmartML command syntax error %s ...\n\
             Usage: %s (<expression> : <type>)"
            ident
            ident
        in
        match args with
        | [(Nolabel, e)] ->
          begin
            match e with
            | [%expr ([%e? e] : [%t? t])] ->
                let line_no = line_no_of_loc loc in
                [%expr
                  Command.set_type
                    ~line_no:[%e line_no]
                    [%e parse_expr e]
                    [%e parse_type t]]
            | _ -> err ()
          end
        | _ -> err ()
      in
      let mono (f : _ -> _) ident args =
        match args with
        | [(Nolabel, e)] -> fun loc -> f loc e
        | _ ->
            fun loc ->
              err
                loc
                "SmartML command syntax error %s ...\nUsage: %s <expression>"
                ident
                ident
      in
      let bin (f : _ -> _) ident args =
        match args with
        | [(Nolabel, e1); (Nolabel, e2)] -> fun loc -> f loc e1 e2
        | _ ->
            fun loc ->
              err
                loc
                "SmartML command syntax error %s ...\n\
                 Usage: %s <expression> <expression>"
                ident
                ident
      in
      [ ( "never"
        , mono (fun loc e ->
              let line_no = line_no_of_loc loc in
              [%expr Command.never ~line_no:[%e line_no] [%e parse_expr e]]) )
      ; ( "verify"
        , bin (fun loc e1 e2 ->
              let line_no = line_no_of_loc loc in
              [%expr
                Command.verify
                  ~line_no:[%e line_no]
                  [%e parse_expr e1]
                  (Some [%e parse_expr e2])]) )
      ; ("set_type", set_type) ]
    in
    match List.assoc_opt ident single_commands with
    | Some command -> command ident args loc
    | _ ->
        err
          loc
          "Unknown command %s\nSupported commands are: %s."
          ident
          (String.concat ", " (List.map fst single_commands))
  in
  parse_command

module Entry_points = struct
  let expand_entry_point ~ctxt recflag value_bindings =
    let on_value_binding binding =
      match binding.pvb_pat.ppat_desc with
      | Ppat_var name ->
          let code =
            let t, e =
              match binding.pvb_expr with
              | [%expr fun self params -> [%e? e]] -> (`Present, e)
              | [%expr fun self () -> [%e? e]] -> (`Absent, e)
              | [%expr fun self (params : [%t? t]) -> [%e? e]] ->
                  (`Annotated t, e)
              | _ -> assert false
            in
            let tparameter_ep_explicit =
              let loc =
                Ppxlib.Expansion_context.Extension.extension_point_loc ctxt
              in
              match t with
              | `Present -> [%expr `Present]
              | `Absent -> [%expr `Absent]
              | `Annotated {ptyp_desc = Ptyp_constr ({txt = Lident t; _}, _); _}
                ->
                  [%expr
                    `Annotated
                      [%e
                        Ast_helper.Exp.ident {txt = Lident ("gen_t_" ^ t); loc}]]
              | `Annotated {ptyp_desc = t; _} -> failtdesc t
            in
            let loc =
              Ppxlib.Expansion_context.Extension.extension_point_loc ctxt
            in
            let line_no = line_no_of_loc loc in
            [%expr
              Basics.
                { channel = [%e ast_string name.txt]
                ; tparameter_ep = [%e tparameter_ep_explicit]
                ; originate = true
                ; lazify = Some false
                ; lazy_no_code = Some false
                ; line_no = [%e line_no]
                ; body = [%e parse_command e]
                ; tparameter_ep_derived = U }]
          in
          let code =
            { binding with
              (* pvb_pat =
               *   { binding.pvb_pat with
               *     ppat_desc = Ppat_var {name with txt = "gen_" ^ name.txt} } *)
              pvb_expr = code }
          in
          [code]
      | _ -> assert false
    in
    let value_bindings =
      List.map on_value_binding value_bindings |> List.concat
    in
    Ppxlib.Ast_helper.Str.value recflag value_bindings
end

module Scenarios = struct
  let new_contract ?name ~loc ~env e =
    let id = env.next_id () in
    Option.iter
      (fun name -> Hashtbl.replace env.contract_static_ids name id)
      name;
    let line_no = line_no_of_loc loc in
    [%expr
      let id = Ids.C_static {static_id = [%e ast_integer id]} in
      Basics.New_contract
        { id
        ; contract = [%e e]
        ; line_no = [%e line_no]
        ; accept_unknown_types = false
        ; show = true
        ; address = Basics.address_of_contract_id ~html:false id None }]

  let add_html ~loc ~env:_ tag e =
    let line_no = line_no_of_loc loc in
    [%expr
      Basics.Html
        {tag = [%e ast_string tag]; inner = [%e e]; line_no = [%e line_no]}]

  let verify ~loc ~env condition =
    let line_no = line_no_of_loc loc in
    [%expr
      Basics.Verify
        {condition = [%e parse_expr ~env condition]; line_no = [%e line_no]}]

  let message ~loc ~env ~options e1 params =
    let supported_options = ["valid"] in
    let check_supported loc k =
      if not (List.mem k supported_options)
      then
        err
          loc
          "Unsupported option ~%s in contract all.\nSupported options: %s."
          k
          (String.concat ", " supported_options)
    in
    let options =
      List.map
        (function
          | (Labelled k : arg_label), v ->
              check_supported loc k;
              (k, v)
          | (Optional k : arg_label), _v ->
              check_supported loc k;
              err
                loc
                "Unsupported option usage ?%s in contract call; please use ~%s."
                k
                k
          | _ -> assert false)
        options
    in
    let line_no = line_no_of_loc loc in
    let id, message =
      match e1 with
      | { pexp_desc =
            Pexp_field
              ( {pexp_desc = Pexp_ident {txt = Lident id; _}; _}
              , {txt = Lident message; _} )
        ; _ } ->
          (id, message)
      | _ -> assert false
    in
    let id =
      match Hashtbl.find_opt env.contract_static_ids id with
      | None -> err loc "Missing contract id %S" id
      | Some s -> s
    in
    let valid =
      match List.assoc_opt "valid" options with
      | None -> [%expr Expr.cst ~line_no:[%e line_no] (Literal.bool true)]
      | Some valid -> parse_expr ~env valid
    in
    [%expr
      Basics.Message
        { id = Ids.C_static {static_id = [%e ast_integer id]}
        ; valid = [%e valid]
        ; exception_ = None
        ; params = [%e parse_expr ~env params]
        ; line_no = [%e line_no]
        ; title = ""
        ; messageClass = ""
        ; sender = None
        ; source = None
        ; chain_id = None
        ; time = None
        ; amount =
            Expr.cst ~line_no:[%e line_no] (Literal.mutez Big_int.zero_big_int)
        ; level = None
        ; voting_powers =
            Expr.build_map ~line_no:[%e line_no] ~big:false ~entries:[]
        ; message = [%e ast_string message]
        ; show = true
        ; export = true }]

  let show ~loc ~env expr =
    let line_no = line_no_of_loc loc in
    [%expr
      Basics.Show
        { expression = [%e parse_expr ~env expr]
        ; html = true
        ; stripStrings = false
        ; compile = true
        ; line_no = [%e line_no] }]

  let single_actions =
    let add_action action ~env = env.actions := action :: !(env.actions) in
    let mono (f : loc:_ -> env:_ -> _ -> _) ident args =
      match args with
      | [(Nolabel, e)] -> fun loc env -> (add_action ~env (f e ~loc ~env) : unit)
      | _ ->
          fun loc ->
            err
              loc
              "Scenario syntax error %s ...\nUsage: %s <expression>"
              ident
              ident
    in
    let call _ args =
      let args, options =
        List.partition
          (function
            | Nolabel, _ -> true
            | _ -> false)
          args
      in
      let args = List.map snd args in
      match args with
      | [e1; params] ->
          fun loc env -> add_action ~env (message ~loc ~env ~options e1 params)
      | _ -> assert false
    in
    [ ("h1", mono (add_html "h1"))
    ; ("h2", mono (add_html "h2"))
    ; ("h3", mono (add_html "h3"))
    ; ("h4", mono (add_html "h4"))
    ; ("p", mono (add_html "p"))
    ; ("show", mono show)
    ; ("verify", mono verify)
    ; ("register_contract", mono (new_contract ?name:None))
    ; ("call", call) ]

  let parse_actions env expr =
    let rec parse_actions expr =
      let loc = expr.pexp_loc in
      let add_action action =
        env.actions := action ~loc ~env :: !(env.actions)
      in
      match expr with
      | {pexp_desc = Pexp_ident {txt = Lident ident; _}; _} ->
          apply ~loc ident []
      | { pexp_desc =
            Pexp_apply
              ({pexp_desc = Pexp_ident {txt = Lident ident; _}; _}, args)
        ; _ } ->
          apply ~loc ident args
      | [%expr
          let [%p? p] = register_contract [%e? e1] in
          [%e? e]] ->
          let name =
            match p.ppat_desc with
            | Ppat_var {txt = v; _} -> v
            | _ -> assert false
          in
          add_action (new_contract ~name e1);
          parse_actions e
      | [%expr
          [%e? e1];
          [%e? e2]] ->
          parse_actions e1;
          parse_actions e2
      | _ -> faildesc expr.pexp_desc
    and apply ~loc ident (args : _ list) =
      match List.assoc_opt ident single_actions with
      | Some action -> action ident args loc env
      | _ ->
          err
            loc
            "Unknown action %s\nSupported actions are: %s."
            ident
            (String.concat ", " (List.map fst single_actions))
    in
    parse_actions expr

  let parse_actions ~ctxt expression =
    let ids = ref (-1) in
    let next_id () =
      incr ids;
      !ids
    in
    let env =
      {ctxt; actions = ref []; contract_static_ids = Hashtbl.create 5; next_id}
    in
    parse_actions env expression;
    let loc = Ppxlib.Expansion_context.Extension.extension_point_loc ctxt in
    to_list loc (List.rev !(env.actions))

  let expand_scenario ~scenario_kind ~ctxt recflag value_bindings =
    let on_value_binding binding =
      match binding.pvb_pat.ppat_desc with
      | Ppat_var name ->
          let code =
            let actions = parse_actions ~ctxt binding.pvb_expr in
            let loc =
              Ppxlib.Expansion_context.Extension.extension_point_loc ctxt
            in
            [%expr
              Basics.
                { shortname = [%e ast_string name.txt]
                ; actions = [%e actions]
                ; flags = []
                ; kind = {kind = [%e ast_string scenario_kind]} }]
          in
          let code = {binding with pvb_expr = code} in
          [code]
      | _ -> assert false
    in
    let value_bindings =
      List.map on_value_binding value_bindings |> List.concat
    in
    Ppxlib.Ast_helper.Str.value recflag value_bindings
end

let parse_expr_ ~ctxt:_ expr = parse_expr expr

let expr_expansion =
  Extension.V3.declare
    "expr"
    Extension.Context.expression
    Ast_pattern.(single_expr_payload __)
    parse_expr_

let () =
  Ppxlib.Driver.register_transformation ~extensions:[expr_expansion] "expr"

let expr_expansion =
  Extension.V3.declare
    "actions"
    Extension.Context.expression
    Ast_pattern.(single_expr_payload __)
    Scenarios.parse_actions

let () =
  Ppxlib.Driver.register_transformation ~extensions:[expr_expansion] "actions"

let command_expansion =
  let parse_command ~ctxt:_ e = parse_command e in
  Extension.V3.declare
    "command"
    Extension.Context.expression
    Ast_pattern.(single_expr_payload __)
    parse_command

let () =
  Ppxlib.Driver.register_transformation
    ~extensions:[command_expansion]
    "command"

let entry_point_expansion =
  Extension.V3.declare
    "entry_point"
    Extension.Context.structure_item
    Ast_pattern.(pstr (pstr_value __ __ ^:: nil))
    Entry_points.expand_entry_point

let () =
  Ppxlib.Driver.register_transformation
    ~extensions:[entry_point_expansion]
    "entry_point"

let smartml_test_expansion =
  Extension.V3.declare
    "smartml_test"
    Extension.Context.structure_item
    Ast_pattern.(pstr (pstr_value __ __ ^:: nil))
    (Scenarios.expand_scenario ~scenario_kind:"test")

let () =
  Ppxlib.Driver.register_transformation
    ~extensions:[smartml_test_expansion]
    "smartml_test"

(* https://ppxlib.readthedocs.io/_/downloads/en/latest/pdf/ *)
