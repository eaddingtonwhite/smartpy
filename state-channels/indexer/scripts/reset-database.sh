#!/bin/sh

DB_NAME=tezplorer
DB_SCHEMA_FILE=$(realpath $(dirname $0))/../docker/data/database.sql

# Clear old schema
psql -h localhost -d $DB_NAME -U postgres -p 5432 <<EOF
DROP SCHEMA public CASCADE;
CREATE SCHEMA public;
EOF

# Create schema
psql -h localhost -d $DB_NAME -U postgres -p 5432 -a -q -f $DB_SCHEMA_FILE 1>/dev/null
