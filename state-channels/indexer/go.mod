module github.com/romarq/tezplorer

go 1.14

require (
	blockwatch.cc/tzgo v0.9.9
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/labstack/echo/v4 v4.2.0 // indirect
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
	github.com/swaggo/echo-swagger v1.1.0 // indirect
	github.com/swaggo/swag v1.7.0 // indirect
	github.com/tidwall/gjson v1.8.1
	github.com/tidwall/pretty v1.2.0 // indirect
	go.uber.org/zap v1.16.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v2 v2.4.0
)
