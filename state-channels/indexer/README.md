# Tezplorer
Tezos Block Explorer

```shell
# Install and compile
go mod vendor
make

# Start containers
make build-docker
make start-docker
```
