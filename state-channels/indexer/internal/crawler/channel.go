package crawler

import (
	"database/sql"
	"encoding/json"
	"time"

	"github.com/lib/pq"
	"github.com/pkg/errors"
	"github.com/romarq/tezplorer/pkg/utils"
)

type channelPlayerBond struct {
	TokenID int64
	Amount  int64
}

type channelPlayerWithdraw struct {
	Challenge       []string
	ChallengeTokens map[int64]int64
	Timeout         int64
	Tokens          map[int64]int64
}

type channelPlayer struct {
	Bonds         []channelPlayerBond
	PublicKeyHash string
	PublicKey     string
	Withdraw      channelPlayerWithdraw
	WithdrawID    int64
}

type channel struct {
	Key   string
	Value struct {
		Closed        bool
		Nonce         string
		WithdrawDelay int64
		Participants  map[string]channelPlayer
	}
}

func (c *Crawler) updateChannelParticipantBonds(channelPlayerID int64, bonds []channelPlayerBond) error {
	for _, bond := range bonds {
		var exists bool
		err := c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM channel_participant_bond WHERE channel_participant_id = $1 AND bond_id = $2)", channelPlayerID, bond.TokenID).Scan(&exists)
		if err != nil {
			return errors.Wrap(err, "Could not query table (channel_participant_bond).")
		}

		if exists {
			// Preparing channel_participant_bond update
			updateChannelParticipantBond, err := c.DB.Prepare(`UPDATE channel_participant_bond SET bond_id = $1, amount = $2 WHERE channel_participant_id = $3`)
			if err != nil {
				return errors.Wrap(err, "Could not prepare update statement for table (channel_participant_bond).")
			}

			_, err = updateChannelParticipantBond.Exec(
				bond.TokenID,
				bond.Amount,
				channelPlayerID,
			)
			if err != nil {
				return err
			}
		} else {
			err = c.insertChannelParticipantBond(channelPlayerID, bond)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (c *Crawler) insertChannelParticipantBond(channelPlayerID int64, bond channelPlayerBond) error {
	// Preparing channel_participant_bond insertion
	insertChannelParticipantBond, err := c.DB.Prepare(
		`INSERT INTO channel_participant_bond(
		channel_participant_id,
		bond_id,
		amount
	) VALUES($1, $2, $3)`)
	if err != nil {
		return errors.Wrap(err, "Could not prepare insert statement for table (channel_participant_bond).")
	}

	_, err = insertChannelParticipantBond.Exec(
		channelPlayerID,
		bond.TokenID,
		bond.Amount,
	)
	if err != nil {
		return errors.Wrap(err, "Could not insert on table (channel_participant_bond).")
	}

	return nil
}

func (c *Crawler) updateChannelParticipants(channelID string, participants map[string]channelPlayer) error {
	for _, participant := range participants {
		var id int64
		err := c.DB.QueryRow("SELECT id FROM channel_participant WHERE channel_id = $1 AND public_key_hash = $2", channelID, participant.PublicKeyHash).Scan(&id)
		if err != nil && err != sql.ErrNoRows {
			return errors.Wrap(err, "Could not query table (channel_participant).")
		}

		// Add channel if it doesn't exist
		if id == 0 {
			id, err = c.insertChannelParticipant(channelID, participant)
			if err != nil {
				return errors.Wrap(err, "Could not insert on table (channel_participant).")
			}
		} else {
			// Preparing channel_participant update
			updateParticipantChannel, err := c.DB.Prepare(`
				UPDATE channel_participant
				SET withdraw_id = $1, challenge = $2, challenge_tokens = $3, timeout = $4, tokens = $5
				WHERE id = $6
			`)
			if err != nil {
				return errors.Wrap(err, "Could not prepare update statement for table (channel_participant).")
			}

			challengeTokens, err := json.MarshalIndent(participant.Withdraw.ChallengeTokens, "", "")
			if err != nil {
				return err
			}
			tokens, err := json.MarshalIndent(participant.Withdraw.Tokens, "", "")
			if err != nil {
				return err
			}

			_, err = updateParticipantChannel.Exec(
				participant.WithdrawID,
				pq.Array(participant.Withdraw.Challenge),
				challengeTokens,
				participant.Withdraw.Timeout,
				tokens,
				id,
			)
			if err != nil {
				return err
			}
		}

		err = c.updateChannelParticipantBonds(id, participant.Bonds)
		if err != nil {
			return errors.Wrap(err, "Could not update table (channel_participant_bond).")
		}
	}

	return nil
}

func (c *Crawler) insertChannelParticipant(channelID string, participant channelPlayer) (int64, error) {
	// Preparing channel_participant insertion
	insertChannelParticipant, err := c.DB.Prepare(
		`INSERT INTO channel_participant(
			channel_id,
			public_key_hash,
			public_key,
			withdraw_id
		) VALUES($1, $2, $3, $4) RETURNING id`)
	if err != nil {
		return 0, errors.Wrap(err, "Could not prepare insert statement for table (channel_participant).")
	}

	var id int64
	err = insertChannelParticipant.QueryRow(
		channelID,
		participant.PublicKeyHash,
		participant.PublicKey,
		participant.WithdrawID,
	).Scan(&id)

	return id, err
}

func (c *Crawler) updateChannel(channel channel, timestamp time.Time) error {
	var exists bool
	err := c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM channel WHERE id = $1)", channel.Key).Scan(&exists)
	if err != nil {
		return errors.Wrap(err, "Could not query table (channel).")
	}

	if exists {
		// Preparing channel update
		updateChannel, err := c.DB.Prepare(`UPDATE channel SET closed = $1, updated_at = $2 WHERE id = $3`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare update statement for table (channel).")
		}

		_, err = updateChannel.Exec(
			channel.Value.Closed,
			timestamp,
			channel.Key,
		)
		if err != nil {
			return errors.Wrap(err, "Could not update table (channel).")
		}
	} else {

		// Preparing channel insertion
		insertChannel, err := c.DB.Prepare(`INSERT INTO channel(
			id,
			closed,
			nonce,
			withdraw_delay,
			updated_at,
			created_at
		) VALUES($1, $2, $3, $4, $5, $6)`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare insert statement for table (channel).")
		}

		_, err = insertChannel.Exec(
			channel.Key,
			channel.Value.Closed,
			channel.Value.Nonce,
			channel.Value.WithdrawDelay,
			timestamp,
			timestamp,
		)
		if err != nil {
			return errors.Wrap(err, "Could not insert on table (channel).")
		}
	}

	return c.updateChannelParticipants(channel.Key, channel.Value.Participants)
}

func parseChannel(entry michelsonMapValue) (channel, error) {
	channel := channel{}

	// Get channel identifier
	key, ok := entry.Key.(string)
	if !ok {
		return channel, errors.Errorf("Unexpected channel key.")
	}
	channel.Key = key

	// Get channel information

	valueMap, ok := entry.Value.(map[string]interface{})
	if !ok {
		return channel, errors.Errorf("Unexpected channel value.")
	}

	// Get channel closed state
	closed, ok := valueMap["closed"].(bool)
	if !ok {
		return channel, errors.Errorf("Could not get (closed) field from channel.")
	}
	channel.Value.Closed = closed

	// Get channel nonce state
	nonce, ok := valueMap["nonce"].(string)
	if !ok {
		return channel, errors.Errorf("Could not get (nonce) field from channel.")
	}
	channel.Value.Nonce = nonce

	// Get channel withdraw_delay state
	withdrawDelay, ok := valueMap["withdraw_delay"].(int64)
	if !ok {
		return channel, errors.Errorf("Could not get (withdraw_delay) field from channel.")
	}
	channel.Value.WithdrawDelay = withdrawDelay

	// Get channel players state
	players, ok := valueMap["players"].([]michelsonMapValue)
	if !ok {
		return channel, errors.Errorf("Could not get (players) field from channel: %s", utils.PrettifyJSON(valueMap))
	}

	channel.Value.Participants = make(map[string]channelPlayer)
	for _, playerMap := range players {
		address, ok := playerMap.Key.(string)
		if !ok {
			return channel, errors.Errorf("Unexpected channel player address.")
		}
		valueMap, ok := playerMap.Value.(map[string]interface{})
		if !ok {
			return channel, errors.Errorf("Unexpected channel player value.")
		}

		// Get player withdraw_id
		withdrawID, ok := valueMap["withdraw_id"].(int64)
		if !ok {
			return channel, errors.Errorf("Could not get (withdraw_id) field from channel player.")
		}
		// Get player public key
		publicKey, ok := valueMap["pk"].(string)
		if !ok {
			return channel, errors.Errorf("Could not get (pk) field from channel player.")
		}

		// Get player bonds
		bonds, ok := valueMap["bonds"].([]michelsonMapValue)
		if !ok {
			return channel, errors.Errorf("Could not get (bonds) field from channel player.")
		}

		playerBonds := make([]channelPlayerBond, len(bonds))
		for _, bondMap := range bonds {
			tokenID, ok := bondMap.Key.(int64)
			if !ok {
				return channel, errors.Errorf("Unexpected tokenID in player bond.")
			}

			amount, ok := bondMap.Value.(int64)
			if !ok {
				return channel, errors.Errorf("Unexpected amount in player bond.")
			}

			playerBonds = append(playerBonds, channelPlayerBond{
				TokenID: tokenID,
				Amount:  amount,
			})
		}

		// Get player withdraw
		var withdraw channelPlayerWithdraw
		withdrawObj, ok := valueMap["withdraw"].(map[string]interface{})
		if ok {
			// Get player withdraw challenge
			withdrawChallenge, ok := withdrawObj["challenge"].([]string)
			if !ok {
				return channel, errors.Errorf("Could not get (challenge) field from channel player withdraw.")
			}

			// Get player withdraw timeout
			timeout, ok := withdrawObj["timeout"].(int64)
			if !ok {
				return channel, errors.Errorf("Could not get (timeout) field from channel player withdraw.")
			}

			// Get player withdraw challenge tokens
			challengeTokens, ok := withdrawObj["challenge_tokens"].([]michelsonMapValue)
			if !ok {
				return channel, errors.Errorf("Could not get (challenge_tokens) field from channel player withdraw.")
			}
			withdrawChallengeTokens := make(map[int64]int64)
			for _, challengeToken := range challengeTokens {
				tokenID, ok := challengeToken.Key.(int64)
				if !ok {
					return channel, errors.Errorf("Could not get challange token identifier from channel player withdraw token.")
				}
				tokenAmount, ok := challengeToken.Value.(int64)
				if !ok {
					return channel, errors.Errorf("Could not get challange token amount from channel player withdraw token.")
				}

				withdrawChallengeTokens[tokenID] = tokenAmount
			}

			// Get player withdraw tokens
			tokens, ok := withdrawObj["tokens"].([]michelsonMapValue)
			if !ok {
				return channel, errors.Errorf("Could not get (challenge_tokens) field from channel player withdraw.")
			}
			withdrawTokens := make(map[int64]int64)
			for _, token := range tokens {
				tokenID, ok := token.Key.(int64)
				if !ok {
					return channel, errors.Errorf("Could not get token identifier from channel player withdraw token.")
				}
				tokenAmount, ok := token.Value.(int64)
				if !ok {
					return channel, errors.Errorf("Could not get token amount from channel player withdraw token.")
				}

				withdrawTokens[tokenID] = tokenAmount
			}

			withdraw = channelPlayerWithdraw{
				Challenge:       withdrawChallenge,
				ChallengeTokens: withdrawChallengeTokens,
				Timeout:         timeout,
				Tokens:          withdrawTokens,
			}
		}

		channel.Value.Participants[address] = channelPlayer{
			Withdraw:      withdraw,
			WithdrawID:    withdrawID,
			PublicKey:     publicKey,
			PublicKeyHash: address,
			Bonds:         playerBonds,
		}
	}

	return channel, nil
}
