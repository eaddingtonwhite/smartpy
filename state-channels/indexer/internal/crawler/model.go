package crawler

import (
	"encoding/json"
	"time"

	"github.com/pkg/errors"
)

type model_type struct {
	ID   string
	Name string
}

type bigMapModelDiff struct {
	Key   string `json:"Key"`
	Value struct {
		Metadata []struct {
			Key   string `json:"Key"`
			Value string `json:"Value"`
		} `json:"metadata"`
	} `json:"Value"`
}

func (c *Crawler) updateModel(model model_type, timestamp time.Time) error {
	var exists bool
	err := c.DB.QueryRow("SELECT EXISTS(SELECT 1 FROM model WHERE id = $1)", model.ID).Scan(&exists)
	if err != nil {
		return errors.Wrap(err, "Could not query table (model).")
	}

	if exists {
		// Preparing model update
		updateModel, err := c.DB.Prepare(`UPDATE model SET name = $1, updated_at = $2 WHERE id = $3`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare update statement for table (model).")
		}

		_, err = updateModel.Exec(
			model.Name,
			timestamp,
			model.ID,
		)
		if err != nil {
			return errors.Wrap(err, "Could not update table (model).")
		}
	} else {

		// Preparing model insertion
		insertModel, err := c.DB.Prepare(`INSERT INTO model(
			id,
			name,
			updated_at,
			created_at
		) VALUES($1, $2, $3, $4)`)
		if err != nil {
			return errors.Wrap(err, "Could not prepare insert statement for table (model).")
		}

		_, err = insertModel.Exec(
			model.ID,
			model.Name,
			timestamp,
			timestamp,
		)
		if err != nil {
			return errors.Wrap(err, "Could not insert on table (model).")
		}
	}

	return nil
}

func parseModel(entry michelsonMapValue) (model_type, error) {
	model := model_type{}
	bigMapModelDiff, err := unmarshalModelDiff(entry)
	if err != nil {
		return model, err
	}

	// Get model information
	model.ID = bigMapModelDiff.Key
	for _, entry := range bigMapModelDiff.Value.Metadata {
		switch entry.Key {
		case "name":
			model.Name = entry.Value
		}
	}

	return model, nil
}

func unmarshalModelDiff(entry michelsonMapValue) (bigMapModelDiff, error) {
	var modelDiff bigMapModelDiff
	bytes, err := json.Marshal(entry)
	if err != nil {
		return modelDiff, err
	}
	err = json.Unmarshal(bytes, &modelDiff)
	if err != nil {
		return modelDiff, err
	}

	return modelDiff, nil
}
