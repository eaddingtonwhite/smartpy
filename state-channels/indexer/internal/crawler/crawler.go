package crawler

import (
	"database/sql"
	"fmt"
	"time"

	"blockwatch.cc/tzgo/micheline"
	"github.com/lib/pq"
	"github.com/pkg/errors"
	LOG "github.com/romarq/tezplorer/internal/logger"
	"github.com/romarq/tezplorer/internal/monitor"

	IndexerConfig "github.com/romarq/tezplorer/internal/config/indexer"
	RPC "github.com/romarq/tezplorer/internal/rpc"
	"github.com/romarq/tezplorer/pkg/utils"
)

type ChainState struct {
	ChainID    string
	BlockHash  string
	BlockLevel int64
}

type ContractState struct {
	Admins          []string
	ChannelsID      int64
	ModelsID        int64
	GamesID         int64
	TokenMetadataID int64
}

type Crawler struct {
	ChainState    ChainState
	ContractState ContractState
	DB            *sql.DB
	RPC           *RPC.RPC
	Config        IndexerConfig.Config
}

func (c *Crawler) fetchChainState() error {
	err := c.DB.QueryRow("SELECT chain_id, l_block_level, l_block_hash FROM state LIMIT 1").Scan(
		&c.ChainState.ChainID,
		&c.ChainState.BlockLevel,
		&c.ChainState.BlockHash,
	)

	if err == sql.ErrNoRows {
		header, err := c.RPC.GetBlockHeaderByLevel(c.Config.Contracts.Level - 1)
		if err != nil {
			return err
		}
		// Store state
		err = c.addState(header)
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	}

	return nil
}

func (c *Crawler) addState(block *RPC.BlockHeader) error {
	if block.ChainId == nil {
		return fmt.Errorf("ChainId is nil")
	}
	if block.Hash == nil {
		return fmt.Errorf("BlockHash is nil")
	}
	chainId := block.ChainId.String()
	blockHash := block.Hash.String()

	insertStmt, err := c.DB.Prepare(`INSERT INTO state(chain_id, l_block_level, l_block_hash) VALUES($1, $2, $3)`)
	if err != nil {
		return errors.Wrap(err, "Could not prepate insert statement for table state.")
	}

	_, err = insertStmt.Exec(
		chainId,
		block.Level,
		blockHash,
	)
	if err != nil {
		return errors.Wrap(err, "Could not insert state.")
	}

	c.ChainState.ChainID = chainId
	c.ChainState.BlockLevel = block.Level
	c.ChainState.BlockHash = blockHash

	return nil
}

func (c *Crawler) updateState(block *RPC.Block) error {
	if c.ChainState.ChainID != block.ChainId.String() {
		return errors.Errorf("Chain identifier mismatch: '%s' != '%s'", c.ChainState.ChainID, block.ChainId.String())
	}
	if c.ChainState.BlockHash != block.Header.Predecessor.String() {
		return errors.Errorf("Predecessor unexpected, expected '%s' but received '%s'", c.ChainState.BlockHash, block.Header.Predecessor.String())
	}
	chainId := block.ChainId.String()
	blockHash := block.Hash.String()

	updateStmt, err := c.DB.Prepare(`UPDATE state SET l_block_level = $1, l_block_hash = $2 WHERE chain_id = $3`)
	if err != nil {
		return errors.Wrap(err, "Could not prepate update statement for table state.")
	}

	_, err = updateStmt.Exec(
		block.Header.Level,
		blockHash,
		chainId,
	)
	if err != nil {
		return errors.Wrap(err, "Could not update state.")
	}

	c.ChainState.BlockLevel = block.Header.Level
	c.ChainState.BlockHash = blockHash

	return nil
}

func (c *Crawler) fetchContractState() error {
	platformScript, err := c.RPC.GetContractScriptAtLevel(c.Config.Contracts.Platform.Address, c.Config.Contracts.Level)
	if err != nil {
		return errors.Wrap(err, "Could not fetch platform contract script.")
	}
	platformStorage := micheline.NewValue(platformScript.StorageType(), platformScript.Storage)

	ledgerScript, err := c.RPC.GetContractScriptAtLevel(c.Config.Contracts.Ledger.Address, c.Config.Contracts.Level)
	if err != nil {
		return errors.Wrap(err, "Could not fetch ledger contract script.")
	}

	var admins []string
	if adminsRaw, exist := platformStorage.GetValue("admins"); exist {
		arr, ok := adminsRaw.([]interface{})
		if !ok {
			return errors.Errorf("Invalid storage, expected field (admins) to contain a value of type ([]interface {}) but received %T.", adminsRaw)
		}
		admins = make([]string, len(arr))
		for i, v := range arr {
			admins[i] = fmt.Sprint(v)
		}
	} else {
		return errors.Errorf("Invalid storage, expected to find a field named (admins).")
	}

	ledgerBigMaps := ledgerScript.BigmapsByName()
	tokenMetadataId, exist := ledgerBigMaps["token_metadata"]
	if !exist {
		return errors.Errorf("Invalid storage, expected to find a field named (token_metadata).")
	}
	platformBigMaps := platformScript.BigmapsByName()
	channelsId, exist := platformBigMaps["channels"]
	if !exist {
		return errors.Errorf("Invalid storage, expected to find a field named (channels).")
	}
	modelsId, exist := platformBigMaps["models"]
	if !exist {
		return errors.Errorf("Invalid storage, expected to find a field named (models).")
	}
	gamesId, exist := platformBigMaps["games"]
	if !exist {
		return errors.Errorf("Invalid storage, expected to find a field named (games).")
	}

	// Preparing contract state insertion
	insertContractState, err := c.DB.Prepare(
		`INSERT INTO contract_state(
			admins,
			channels,
			models,
			games
		) VALUES($1, $2, $3, $4)`)
	if err != nil {
		return errors.Wrap(err, "Could not prepare contract state insertion.")
	}

	_, err = insertContractState.Exec(
		pq.Array(admins),
		channelsId,
		modelsId,
		gamesId,
	)
	if err != nil {
		return errors.Wrap(err, "Could not insert contract state.")
	}

	c.ContractState.Admins = admins
	c.ContractState.ChannelsID = channelsId
	c.ContractState.ModelsID = modelsId
	c.ContractState.GamesID = gamesId
	c.ContractState.TokenMetadataID = tokenMetadataId

	return nil
}

func (c *Crawler) addBlockOperations(block *RPC.Block) error {

	for _, operations := range block.Operations {
		for _, operation := range operations {
			for _, content := range operation.Contents {
				switch content.OpKind() {
				case RPC.OpTypeTransaction:
					transaction := content.(*RPC.Transaction)
					for _, bigMapEl := range transaction.Metadata.Result.BigmapDiff {
						switch transaction.Destination.String() {
						case c.Config.Contracts.Platform.Address:
							LOG.Debug("Updating platform contract state at level (%d)", block.Header.Level)
							switch bigMapEl.Id {
							case c.ContractState.ChannelsID:
								switch bigMapEl.Action {
								case micheline.DiffActionUpdate:
									values, err := c.extractBigMapDiff(c.ContractState.ChannelsID, "%channels", transaction.Destination.String(), transaction.Metadata.Result.BigmapDiff)
									if err != nil {
										return errors.Wrap(err, "Could not extract (channels) big map difference")
									}

									fmt.Println(utils.PrettifyJSON(values))

									channel, err := parseChannel(values)
									if err != nil {
										return errors.Wrap(err, "Could not parse channel entry")
									}

									fmt.Println(utils.PrettifyJSON(channel))

									err = c.updateChannel(channel, block.Header.Timestamp)
									if err != nil {
										return errors.Wrap(err, "Could not update channel table")
									}
								default:
									return fmt.Errorf("unexpected action (%s) on channels big_map", bigMapEl.Action.String())
								}
							case c.ContractState.ModelsID:
								switch bigMapEl.Action {
								case micheline.DiffActionUpdate:
									values, err := c.extractBigMapDiff(c.ContractState.ModelsID, "%models", transaction.Destination.String(), transaction.Metadata.Result.BigmapDiff)
									if err != nil {
										return errors.Wrap(err, "Could not extract (models) big map difference")
									}

									fmt.Println(utils.PrettifyJSON(values))

									model, err := parseModel(values)
									if err != nil {
										return errors.Wrap(err, "Could not parse model entry")
									}

									err = c.updateModel(model, block.Header.Timestamp)
									if err != nil {
										return errors.Wrap(err, "Could not update model table")
									}
								default:
									return fmt.Errorf("unexpected action (%s) on models big_map", bigMapEl.Action.String())
								}
							case c.ContractState.GamesID:
								switch bigMapEl.Action {
								case micheline.DiffActionUpdate:
									values, err := c.extractBigMapDiff(c.ContractState.GamesID, "%games", transaction.Destination.String(), transaction.Metadata.Result.BigmapDiff)
									if err != nil {
										return errors.Wrap(err, "Could not extract (games) big map difference")
									}

									fmt.Println(utils.PrettifyJSON(values))

									game, err := parseGame(values)
									if err != nil {
										return errors.Wrap(err, "Could not parse game entry")
									}

									fmt.Println(utils.PrettifyJSON(game))

									err = c.updateGame(game, block.Header.Timestamp)
									if err != nil {
										return errors.Wrap(err, "Could not update game table")
									}

								default:
									return fmt.Errorf("unexpected action (%s) on games big_map", bigMapEl.Action.String())
								}
							default:
								LOG.Debug("Discarding big_map (%d) update.", bigMapEl.Id)
							}
						case c.Config.Contracts.Ledger.Address:
							switch bigMapEl.Id {
							case c.ContractState.TokenMetadataID:
								switch bigMapEl.Action {
								case micheline.DiffActionUpdate:
									values, err := c.extractBigMapDiff(c.ContractState.TokenMetadataID, "%token_metadata", transaction.Destination.String(), transaction.Metadata.Result.BigmapDiff)
									if err != nil {
										return errors.Wrap(err, "Could not extract (channels) big map difference")
									}

									fmt.Println(utils.PrettifyJSON(values))

									metadata, err := parseMetadata(values)
									if err != nil {
										return errors.Wrap(err, "Could not parse metadata entry")
									}

									fmt.Println(utils.PrettifyJSON(metadata))

									err = c.updateMetadata(metadata, block.Header.Timestamp)
									if err != nil {
										return errors.Wrap(err, "Could not update metadata table")
									}
								default:
									return fmt.Errorf("unexpected action (%s) on metadata big_map", bigMapEl.Action.String())
								}
							}
						}
					}
				}
			}
		}
	}

	return nil
}

func (c *Crawler) monitorHeads() error {
	heads := make(chan RPC.BlockHeader)

	blockMonitor := monitor.New(c.Config.RPC.URL + "/monitor/heads/main")
	go blockMonitor.MonitorHeads(heads)

	defer close(heads)

	for {
		head, ok := <-heads
		if !ok {
			return fmt.Errorf("Failed to monitor heads.")
		}

		if c.ChainState.BlockLevel >= int64(head.Level) {
			LOG.Debug("Level %d already processed, skipping...", head.Level)
			continue
		}

		LOG.Debug("Processing new block %s...", head.Hash)

		err := c.addBlock(head.Level)
		if err != nil {
			LOG.Debug("%v", err)
			return err
		}
	}
}

func (c *Crawler) addBlock(level int64) error {
	if c.ChainState.BlockLevel+1 != level {
		return errors.Errorf("Level too far, expected %d but received %d", c.ChainState.BlockLevel+1, level)
	}

	block, err := c.RPC.GetBlockByLevel(level)
	if err != nil {
		return errors.Wrap(err, "Failed to fetch block by level")
	}

	LOG.Info("Processing block: %d...", level)
	err = c.addBlockOperations(block)
	if err != nil {
		return err
	}

	err = c.updateState(block)
	if err != nil {
		return err
	}

	return nil
}

func (c *Crawler) synchronize() error {
	// This flag is turned [ON] when an error occurs and reset
	// to [OFF] at the begining of the loop after waiting a few seconds
	retry := false
	for {
		if retry {
			time.Sleep(5 * time.Second)
			retry = false
		}

		block, err := c.RPC.GetHeadBlockHeader()
		if err != nil {
			LOG.Debug("Could not get head block header: %v", err)
			retry = true
			continue
		}

		if c.ChainState.BlockLevel == block.Level {
			LOG.Info("Nonitoring new blocks...")
			err = c.monitorHeads()
			if err != nil {
				LOG.Debug("Failed when listening for new blocks: %v", err)
				retry = true
				continue
			}
		}

		LOG.Debug("Synchronyzing (%d of %d)", c.ChainState.BlockLevel, block.Level)
		for c.ChainState.BlockLevel < block.Level {
			err = c.addBlock(c.ChainState.BlockLevel + 1)
			if err != nil {
				LOG.Debug("%v", err)
				retry = true
				continue
			}

			// If the synchronization process completed go back to the beginning of the loop
			if c.ChainState.BlockLevel == block.Level {
				continue
			}
		}

	}
}

// Initialize the tezplorer crawler
func Initialize(rpc *RPC.RPC, db *sql.DB, configuration IndexerConfig.Config) Crawler {
	crawler := Crawler{
		DB:            db,
		RPC:           rpc,
		Config:        configuration,
		ChainState:    ChainState{},
		ContractState: ContractState{},
	}
	// Fetch current state
	err := crawler.fetchChainState()
	if err != nil {
		LOG.Error("Failed to fetch chain state. %v", err)
	}

	err = crawler.fetchContractState()
	if err != nil {
		LOG.Error("Failed to fetch contract state. %v", err)
	}

	err = crawler.synchronize()
	if err != nil {
		LOG.Error("Failed to update chain indexing. %v", err)
	}

	return crawler
}
