package database

import (
	"database/sql"

	_ "github.com/lib/pq"
)

// New - Initialize a new database connection
func New(connectionURL string) (*sql.DB, error) {
	// Connect to PostgreSQL Database
	db, err := sql.Open("postgres", connectionURL)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return db, err
}
