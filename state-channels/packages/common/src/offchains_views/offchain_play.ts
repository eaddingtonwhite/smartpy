export const offchain_play = {
    code: [
        {
            prim: 'CAR',
        },
        [
            {
                prim: 'UNPAIR',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'DIG',
                args: [
                    {
                        int: '2',
                    },
                ],
            },
            {
                prim: 'DROP',
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '5',
                    },
                ],
            },
            {
                prim: 'NONE',
                args: [
                    {
                        prim: 'string',
                    },
                ],
            },
            {
                prim: 'COMPARE',
            },
            {
                prim: 'EQ',
            },
            {
                prim: 'IF',
                args: [
                    [],
                    [
                        {
                            prim: 'PUSH',
                            args: [
                                {
                                    prim: 'string',
                                },
                                {
                                    string: 'Platform_NotRunning',
                                },
                            ],
                        },
                        {
                            prim: 'FAILWITH',
                        },
                    ],
                ],
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '5',
                    },
                ],
            },
            {
                prim: 'IF',
                args: [
                    [
                        {
                            prim: 'PUSH',
                            args: [
                                {
                                    prim: 'string',
                                },
                                {
                                    string: 'Platform_GameSettled',
                                },
                            ],
                        },
                        {
                            prim: 'FAILWITH',
                        },
                    ],
                    [],
                ],
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'CDR',
            },
            {
                prim: 'SWAP',
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'DUG',
                args: [
                    {
                        int: '2',
                    },
                ],
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '6',
                    },
                ],
            },
            {
                prim: 'GET',
            },
            {
                prim: 'IF_NONE',
                args: [
                    [
                        {
                            prim: 'PUSH',
                            args: [
                                {
                                    prim: 'int',
                                },
                                {
                                    int: '292',
                                },
                            ],
                        },
                        {
                            prim: 'FAILWITH',
                        },
                    ],
                    [],
                ],
            },
            {
                prim: 'DUP',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '4',
                    },
                ],
            },
            {
                prim: 'COMPARE',
            },
            {
                prim: 'EQ',
            },
            {
                prim: 'IF',
                args: [
                    [],
                    [
                        {
                            prim: 'PUSH',
                            args: [
                                {
                                    prim: 'string',
                                },
                                {
                                    string: 'Platform_GameWrongPlayer',
                                },
                            ],
                        },
                        {
                            prim: 'FAILWITH',
                        },
                    ],
                ],
            },
            {
                prim: 'SWAP',
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'DUG',
                args: [
                    {
                        int: '2',
                    },
                ],
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'SWAP',
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'DUG',
                args: [
                    {
                        int: '2',
                    },
                ],
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'COMPARE',
            },
            {
                prim: 'EQ',
            },
            {
                prim: 'IF',
                args: [
                    [],
                    [
                        {
                            prim: 'PUSH',
                            args: [
                                {
                                    prim: 'string',
                                },
                                {
                                    string: 'Platform_Wrongmove_nb',
                                },
                            ],
                        },
                        {
                            prim: 'FAILWITH',
                        },
                    ],
                ],
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '5',
                    },
                ],
            },
            {
                prim: 'NONE',
                args: [
                    {
                        prim: 'string',
                    },
                ],
            },
            {
                prim: 'COMPARE',
            },
            {
                prim: 'EQ',
            },
            {
                prim: 'IF',
                args: [
                    [],
                    [
                        {
                            prim: 'PUSH',
                            args: [
                                {
                                    prim: 'string',
                                },
                                {
                                    string: 'Platform_GameNotRunning',
                                },
                            ],
                        },
                        {
                            prim: 'FAILWITH',
                        },
                    ],
                ],
            },
            {
                prim: 'DUP',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'CDR',
            },
            {
                prim: 'SWAP',
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'DUG',
                args: [
                    {
                        int: '2',
                    },
                ],
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'GET',
            },
            {
                prim: 'IF_NONE',
                args: [
                    [
                        {
                            prim: 'DUP',
                        },
                        {
                            prim: 'CAR',
                        },
                        {
                            prim: 'CAR',
                        },
                        {
                            prim: 'CAR',
                        },
                        {
                            prim: 'PUSH',
                            args: [
                                {
                                    prim: 'string',
                                },
                                {
                                    string: 'Platform_ModelNotFound: ',
                                },
                            ],
                        },
                        {
                            prim: 'PAIR',
                        },
                        {
                            prim: 'FAILWITH',
                        },
                    ],
                    [],
                ],
            },
            {
                prim: 'SWAP',
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'DUG',
                args: [
                    {
                        int: '2',
                    },
                ],
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '6',
                    },
                ],
            },
            {
                prim: 'IF_NONE',
                args: [
                    [
                        {
                            prim: 'SWAP',
                        },
                        {
                            prim: 'DUP',
                        },
                        {
                            prim: 'DUG',
                            args: [
                                {
                                    int: '2',
                                },
                            ],
                        },
                        {
                            prim: 'SWAP',
                        },
                        {
                            prim: 'DUP',
                        },
                        {
                            prim: 'DUG',
                            args: [
                                {
                                    int: '2',
                                },
                            ],
                        },
                        {
                            prim: 'GET',
                            args: [
                                {
                                    int: '3',
                                },
                            ],
                        },
                        {
                            prim: 'DIG',
                            args: [
                                {
                                    int: '3',
                                },
                            ],
                        },
                        {
                            prim: 'GET',
                            args: [
                                {
                                    int: '3',
                                },
                            ],
                        },
                        {
                            prim: 'EXEC',
                        },
                        {
                            prim: 'SOME',
                        },
                        {
                            prim: 'UPDATE',
                            args: [
                                {
                                    int: '6',
                                },
                            ],
                        },
                        {
                            prim: 'SWAP',
                        },
                    ],
                    [
                        {
                            prim: 'DROP',
                        },
                    ],
                ],
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'DUP',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '6',
                    },
                ],
            },
            {
                prim: 'IF_NONE',
                args: [
                    [
                        {
                            prim: 'PUSH',
                            args: [
                                {
                                    prim: 'int',
                                },
                                {
                                    int: '378',
                                },
                            ],
                        },
                        {
                            prim: 'FAILWITH',
                        },
                    ],
                    [],
                ],
            },
            {
                prim: 'DIG',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '6',
                    },
                ],
            },
            {
                prim: 'SWAP',
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'DUG',
                args: [
                    {
                        int: '5',
                    },
                ],
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'DUP',
                args: [
                    {
                        int: '7',
                    },
                ],
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'CDR',
            },
            {
                prim: 'PAIR',
                args: [
                    {
                        int: '4',
                    },
                ],
            },
            {
                prim: 'DIG',
                args: [
                    {
                        int: '2',
                    },
                ],
            },
            {
                prim: 'DROP',
            },
            {
                prim: 'DIG',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'DROP',
            },
            {
                prim: 'DIG',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'DROP',
            },
            {
                prim: 'EXEC',
            },
            {
                prim: 'SWAP',
            },
            {
                prim: 'DUP',
            },
            {
                prim: 'DUG',
                args: [
                    {
                        int: '2',
                    },
                ],
            },
            {
                prim: 'UNPAIR',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'DUP',
                args: [
                    {
                        int: '4',
                    },
                ],
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '6',
                    },
                ],
            },
            {
                prim: 'PUSH',
                args: [
                    {
                        prim: 'int',
                    },
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'SUB',
            },
            {
                prim: 'DUP',
                args: [
                    {
                        int: '4',
                    },
                ],
            },
            {
                prim: 'CDR',
            },
            {
                prim: 'IF_NONE',
                args: [
                    [
                        {
                            prim: 'NONE',
                            args: [
                                {
                                    prim: 'string',
                                },
                            ],
                        },
                    ],
                    [
                        {
                            prim: 'DROP',
                        },
                        {
                            prim: 'DUP',
                            args: [
                                {
                                    int: '4',
                                },
                            ],
                        },
                        {
                            prim: 'CDR',
                        },
                        {
                            prim: 'IF_NONE',
                            args: [
                                [
                                    {
                                        prim: 'PUSH',
                                        args: [
                                            {
                                                prim: 'int',
                                            },
                                            {
                                                int: '385',
                                            },
                                        ],
                                    },
                                    {
                                        prim: 'FAILWITH',
                                    },
                                ],
                                [],
                            ],
                        },
                        {
                            prim: 'SOME',
                        },
                    ],
                ],
            },
            {
                prim: 'PUSH',
                args: [
                    {
                        prim: 'nat',
                    },
                    {
                        int: '1',
                    },
                ],
            },
            {
                prim: 'DIG',
                args: [
                    {
                        int: '6',
                    },
                ],
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'GET',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'ADD',
            },
            {
                prim: 'PAIR',
                args: [
                    {
                        int: '3',
                    },
                ],
            },
            {
                prim: 'SWAP',
            },
            {
                prim: 'PAIR',
            },
            {
                prim: 'PAIR',
            },
            {
                prim: 'SWAP',
            },
            {
                prim: 'CAR',
            },
            {
                prim: 'SOME',
            },
            {
                prim: 'UPDATE',
                args: [
                    {
                        int: '6',
                    },
                ],
            },
        ],
        {
            prim: 'SOME',
        },
        {
            prim: 'NIL',
            args: [
                {
                    prim: 'operation',
                },
            ],
        },
        {
            prim: 'PAIR',
        },
    ],
    parameter: {
        prim: 'pair',
        args: [
            {
                prim: 'pair',
                args: [
                    {
                        prim: 'pair',
                        args: [
                            {
                                prim: 'pair',
                                args: [
                                    {
                                        prim: 'pair',
                                        args: [
                                            {
                                                prim: 'pair',
                                                args: [
                                                    {
                                                        prim: 'bytes',
                                                    },
                                                    {
                                                        prim: 'map',
                                                        args: [
                                                            {
                                                                prim: 'int',
                                                            },
                                                            {
                                                                prim: 'address',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                            {
                                                prim: 'pair',
                                                args: [
                                                    {
                                                        prim: 'nat',
                                                    },
                                                    {
                                                        prim: 'pair',
                                                        args: [
                                                            {
                                                                prim: 'option',
                                                                args: [
                                                                    {
                                                                        prim: 'string',
                                                                    },
                                                                ],
                                                            },
                                                            {
                                                                prim: 'int',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        prim: 'pair',
                                        args: [
                                            {
                                                prim: 'bytes',
                                            },
                                            {
                                                prim: 'pair',
                                                args: [
                                                    {
                                                        prim: 'bool',
                                                    },
                                                    {
                                                        prim: 'option',
                                                        args: [
                                                            {
                                                                prim: 'bytes',
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                prim: 'bytes',
                            },
                        ],
                    },
                    {
                        prim: 'pair',
                        args: [
                            {
                                prim: 'nat',
                            },
                            {
                                prim: 'address',
                            },
                        ],
                    },
                ],
            },
            {
                prim: 'pair',
                args: [
                    {
                        prim: 'pair',
                        args: [
                            {
                                prim: 'pair',
                                args: [
                                    {
                                        prim: 'pair',
                                        args: [
                                            {
                                                prim: 'set',
                                                args: [
                                                    {
                                                        prim: 'address',
                                                    },
                                                ],
                                                annots: ['%admins'],
                                            },
                                            {
                                                prim: 'big_map',
                                                args: [
                                                    {
                                                        prim: 'bytes',
                                                    },
                                                    {
                                                        prim: 'pair',
                                                        args: [
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'bool',
                                                                        annots: ['%closed'],
                                                                    },
                                                                    {
                                                                        prim: 'string',
                                                                        annots: ['%nonce'],
                                                                    },
                                                                ],
                                                            },
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'map',
                                                                        args: [
                                                                            {
                                                                                prim: 'address',
                                                                            },
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'map',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'nat',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'nat',
                                                                                                    },
                                                                                                ],
                                                                                                annots: ['%bonds'],
                                                                                            },
                                                                                            {
                                                                                                prim: 'key',
                                                                                                annots: ['%pk'],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'option',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'set',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'bytes',
                                                                                                                            },
                                                                                                                        ],
                                                                                                                        annots: [
                                                                                                                            '%challenge',
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'map',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'nat',
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'int',
                                                                                                                            },
                                                                                                                        ],
                                                                                                                        annots: [
                                                                                                                            '%challenge_tokens',
                                                                                                                        ],
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'timestamp',
                                                                                                                        annots: [
                                                                                                                            '%timeout',
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'map',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'nat',
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'nat',
                                                                                                                            },
                                                                                                                        ],
                                                                                                                        annots: [
                                                                                                                            '%tokens',
                                                                                                                        ],
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                                annots: ['%withdraw'],
                                                                                            },
                                                                                            {
                                                                                                prim: 'nat',
                                                                                                annots: [
                                                                                                    '%withdraw_id',
                                                                                                ],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                        annots: ['%players'],
                                                                    },
                                                                    {
                                                                        prim: 'int',
                                                                        annots: ['%withdraw_delay'],
                                                                    },
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                ],
                                                annots: ['%channels'],
                                            },
                                        ],
                                    },
                                    {
                                        prim: 'pair',
                                        args: [
                                            {
                                                prim: 'big_map',
                                                args: [
                                                    {
                                                        prim: 'bytes',
                                                    },
                                                    {
                                                        prim: 'pair',
                                                        args: [
                                                            {
                                                                prim: 'map',
                                                                args: [
                                                                    {
                                                                        prim: 'address',
                                                                    },
                                                                    {
                                                                        prim: 'int',
                                                                    },
                                                                ],
                                                                annots: ['%addr_players'],
                                                            },
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'pair',
                                                                        args: [
                                                                            {
                                                                                prim: 'map',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'int',
                                                                                    },
                                                                                    {
                                                                                        prim: 'map',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'nat',
                                                                                            },
                                                                                            {
                                                                                                prim: 'nat',
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                                annots: ['%bonds'],
                                                                            },
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'bytes',
                                                                                        annots: ['%channel_id'],
                                                                                    },
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'string',
                                                                                                annots: ['%game_nonce'],
                                                                                            },
                                                                                            {
                                                                                                prim: 'pair',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                        annots: [
                                                                                                            '%model_id',
                                                                                                        ],
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'int',
                                                                                                                annots: [
                                                                                                                    '%play_delay',
                                                                                                                ],
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'map',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'int',
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'address',
                                                                                                                            },
                                                                                                                        ],
                                                                                                                        annots: [
                                                                                                                            '%players_addr',
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'map',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'or',
                                                                                                                                args: [
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'string',
                                                                                                                                        annots: [
                                                                                                                                            '%game_finished',
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'or',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'int',
                                                                                                                                                annots: [
                                                                                                                                                    '%player_double_played',
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'int',
                                                                                                                                                annots: [
                                                                                                                                                    '%player_inactive',
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                ],
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'list',
                                                                                                                                args: [
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'pair',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'map',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'nat',
                                                                                                                                                    },
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'nat',
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                                annots: [
                                                                                                                                                    '%bonds',
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'pair',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'int',
                                                                                                                                                        annots: [
                                                                                                                                                            '%receiver',
                                                                                                                                                        ],
                                                                                                                                                    },
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'int',
                                                                                                                                                        annots: [
                                                                                                                                                            '%sender',
                                                                                                                                                        ],
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                ],
                                                                                                                            },
                                                                                                                        ],
                                                                                                                        annots: [
                                                                                                                            '%settlements',
                                                                                                                        ],
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                        annots: ['%constants'],
                                                                    },
                                                                    {
                                                                        prim: 'pair',
                                                                        args: [
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'nat',
                                                                                        annots: ['%move_nb'],
                                                                                    },
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'option',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'or',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'string',
                                                                                                                annots: [
                                                                                                                    '%game_finished',
                                                                                                                ],
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'or',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'int',
                                                                                                                        annots: [
                                                                                                                            '%player_double_played',
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'int',
                                                                                                                        annots: [
                                                                                                                            '%player_inactive',
                                                                                                                        ],
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                                annots: ['%outcome'],
                                                                                            },
                                                                                            {
                                                                                                prim: 'int',
                                                                                                annots: ['%player'],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                                annots: ['%current'],
                                                                            },
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'bytes',
                                                                                        annots: ['%init_input'],
                                                                                    },
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'map',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'string',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                    },
                                                                                                ],
                                                                                                annots: ['%metadata'],
                                                                                            },
                                                                                            {
                                                                                                prim: 'pair',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'bool',
                                                                                                        annots: [
                                                                                                            '%settled',
                                                                                                        ],
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'option',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'bytes',
                                                                                                                    },
                                                                                                                ],
                                                                                                                annots: [
                                                                                                                    '%state',
                                                                                                                ],
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'map',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'int',
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'timestamp',
                                                                                                                    },
                                                                                                                ],
                                                                                                                annots: [
                                                                                                                    '%timeouts',
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                ],
                                                annots: ['%games'],
                                            },
                                            {
                                                prim: 'big_map',
                                                args: [
                                                    {
                                                        prim: 'pair',
                                                        args: [
                                                            {
                                                                prim: 'address',
                                                            },
                                                            {
                                                                prim: 'nat',
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        prim: 'nat',
                                                    },
                                                ],
                                                annots: ['%ledger'],
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                prim: 'pair',
                                args: [
                                    {
                                        prim: 'pair',
                                        args: [
                                            {
                                                prim: 'big_map',
                                                args: [
                                                    {
                                                        prim: 'string',
                                                    },
                                                    {
                                                        prim: 'bytes',
                                                    },
                                                ],
                                                annots: ['%metadata'],
                                            },
                                            {
                                                prim: 'big_map',
                                                args: [
                                                    {
                                                        prim: 'bytes',
                                                    },
                                                    {
                                                        prim: 'pair',
                                                        args: [
                                                            {
                                                                prim: 'lambda',
                                                                args: [
                                                                    {
                                                                        prim: 'pair',
                                                                        args: [
                                                                            {
                                                                                prim: 'bytes',
                                                                                annots: ['%move_data'],
                                                                            },
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'nat',
                                                                                        annots: ['%move_nb'],
                                                                                    },
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'int',
                                                                                                annots: ['%player'],
                                                                                            },
                                                                                            {
                                                                                                prim: 'bytes',
                                                                                                annots: ['%state'],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                    {
                                                                        prim: 'pair',
                                                                        args: [
                                                                            {
                                                                                prim: 'bytes',
                                                                                annots: ['%new_state'],
                                                                            },
                                                                            {
                                                                                prim: 'option',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'string',
                                                                                    },
                                                                                ],
                                                                                annots: ['%outcome'],
                                                                            },
                                                                        ],
                                                                    },
                                                                ],
                                                                annots: ['%apply_'],
                                                            },
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'lambda',
                                                                        args: [
                                                                            {
                                                                                prim: 'bytes',
                                                                            },
                                                                            {
                                                                                prim: 'bytes',
                                                                            },
                                                                        ],
                                                                        annots: ['%init'],
                                                                    },
                                                                    {
                                                                        prim: 'map',
                                                                        args: [
                                                                            {
                                                                                prim: 'string',
                                                                            },
                                                                            {
                                                                                prim: 'bytes',
                                                                            },
                                                                        ],
                                                                        annots: ['%metadata'],
                                                                    },
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                ],
                                                annots: ['%models'],
                                            },
                                        ],
                                    },
                                    {
                                        prim: 'pair',
                                        args: [
                                            {
                                                prim: 'big_map',
                                                args: [
                                                    {
                                                        prim: 'nat',
                                                    },
                                                    {
                                                        prim: 'map',
                                                        args: [
                                                            {
                                                                prim: 'string',
                                                            },
                                                            {
                                                                prim: 'bytes',
                                                            },
                                                        ],
                                                    },
                                                ],
                                                annots: ['%token_metadata'],
                                            },
                                            {
                                                prim: 'big_map',
                                                args: [
                                                    {
                                                        prim: 'nat',
                                                    },
                                                    {
                                                        prim: 'map',
                                                        args: [
                                                            {
                                                                prim: 'string',
                                                            },
                                                            {
                                                                prim: 'bytes',
                                                            },
                                                        ],
                                                    },
                                                ],
                                                annots: ['%token_permissions'],
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        prim: 'big_map',
                        args: [
                            {
                                prim: 'nat',
                            },
                            {
                                prim: 'lambda',
                                args: [
                                    {
                                        prim: 'pair',
                                        args: [
                                            {
                                                prim: 'or',
                                                args: [
                                                    {
                                                        prim: 'pair',
                                                        args: [
                                                            {
                                                                prim: 'map',
                                                                args: [
                                                                    {
                                                                        prim: 'nat',
                                                                    },
                                                                    {
                                                                        prim: 'nat',
                                                                    },
                                                                ],
                                                            },
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'bytes',
                                                                    },
                                                                    {
                                                                        prim: 'address',
                                                                    },
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        prim: 'pair',
                                                        args: [
                                                            {
                                                                prim: 'address',
                                                            },
                                                            {
                                                                prim: 'map',
                                                                args: [
                                                                    {
                                                                        prim: 'nat',
                                                                    },
                                                                    {
                                                                        prim: 'nat',
                                                                    },
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                            {
                                                prim: 'pair',
                                                args: [
                                                    {
                                                        prim: 'pair',
                                                        args: [
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'set',
                                                                        args: [
                                                                            {
                                                                                prim: 'address',
                                                                            },
                                                                        ],
                                                                    },
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'bytes',
                                                                            },
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'bool',
                                                                                            },
                                                                                            {
                                                                                                prim: 'string',
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'map',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'address',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'map',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'nat',
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'nat',
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'key',
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'option',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'pair',
                                                                                                                                args: [
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'pair',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'set',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'bytes',
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'map',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'nat',
                                                                                                                                                    },
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'int',
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'pair',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'timestamp',
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'map',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'nat',
                                                                                                                                                    },
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'nat',
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                ],
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'nat',
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                            {
                                                                                                prim: 'int',
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                ],
                                                            },
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'bytes',
                                                                            },
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'map',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'address',
                                                                                            },
                                                                                            {
                                                                                                prim: 'int',
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'pair',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'map',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'int',
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'map',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'nat',
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'nat',
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'bytes',
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'string',
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'pair',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'bytes',
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'pair',
                                                                                                                                args: [
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'int',
                                                                                                                                    },
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'pair',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'map',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'int',
                                                                                                                                                    },
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'address',
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'map',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'or',
                                                                                                                                                        args: [
                                                                                                                                                            {
                                                                                                                                                                prim:
                                                                                                                                                                    'string',
                                                                                                                                                            },
                                                                                                                                                            {
                                                                                                                                                                prim:
                                                                                                                                                                    'or',
                                                                                                                                                                args: [
                                                                                                                                                                    {
                                                                                                                                                                        prim:
                                                                                                                                                                            'int',
                                                                                                                                                                    },
                                                                                                                                                                    {
                                                                                                                                                                        prim:
                                                                                                                                                                            'int',
                                                                                                                                                                    },
                                                                                                                                                                ],
                                                                                                                                                            },
                                                                                                                                                        ],
                                                                                                                                                    },
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'list',
                                                                                                                                                        args: [
                                                                                                                                                            {
                                                                                                                                                                prim:
                                                                                                                                                                    'pair',
                                                                                                                                                                args: [
                                                                                                                                                                    {
                                                                                                                                                                        prim:
                                                                                                                                                                            'map',
                                                                                                                                                                        args: [
                                                                                                                                                                            {
                                                                                                                                                                                prim:
                                                                                                                                                                                    'nat',
                                                                                                                                                                            },
                                                                                                                                                                            {
                                                                                                                                                                                prim:
                                                                                                                                                                                    'nat',
                                                                                                                                                                            },
                                                                                                                                                                        ],
                                                                                                                                                                    },
                                                                                                                                                                    {
                                                                                                                                                                        prim:
                                                                                                                                                                            'pair',
                                                                                                                                                                        args: [
                                                                                                                                                                            {
                                                                                                                                                                                prim:
                                                                                                                                                                                    'int',
                                                                                                                                                                            },
                                                                                                                                                                            {
                                                                                                                                                                                prim:
                                                                                                                                                                                    'int',
                                                                                                                                                                            },
                                                                                                                                                                        ],
                                                                                                                                                                    },
                                                                                                                                                                ],
                                                                                                                                                            },
                                                                                                                                                        ],
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                ],
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                            {
                                                                                                prim: 'pair',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'nat',
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'option',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'or',
                                                                                                                                args: [
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'string',
                                                                                                                                    },
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'or',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'int',
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'int',
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                ],
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'int',
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'bytes',
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'map',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'string',
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'bytes',
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'pair',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'bool',
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'pair',
                                                                                                                                args: [
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'option',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'bytes',
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'map',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'int',
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'timestamp',
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                ],
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'address',
                                                                                    },
                                                                                    {
                                                                                        prim: 'nat',
                                                                                    },
                                                                                ],
                                                                            },
                                                                            {
                                                                                prim: 'nat',
                                                                            },
                                                                        ],
                                                                    },
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        prim: 'pair',
                                                        args: [
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'string',
                                                                            },
                                                                            {
                                                                                prim: 'bytes',
                                                                            },
                                                                        ],
                                                                    },
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'bytes',
                                                                            },
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'lambda',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'pair',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'nat',
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'int',
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'bytes',
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                            {
                                                                                                prim: 'pair',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'option',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'string',
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'lambda',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                            {
                                                                                                prim: 'map',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'string',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                ],
                                                            },
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'nat',
                                                                            },
                                                                            {
                                                                                prim: 'map',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'string',
                                                                                    },
                                                                                    {
                                                                                        prim: 'bytes',
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'nat',
                                                                            },
                                                                            {
                                                                                prim: 'map',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'string',
                                                                                    },
                                                                                    {
                                                                                        prim: 'bytes',
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        prim: 'pair',
                                        args: [
                                            {
                                                prim: 'list',
                                                args: [
                                                    {
                                                        prim: 'operation',
                                                    },
                                                ],
                                            },
                                            {
                                                prim: 'pair',
                                                args: [
                                                    {
                                                        prim: 'pair',
                                                        args: [
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'set',
                                                                        args: [
                                                                            {
                                                                                prim: 'address',
                                                                            },
                                                                        ],
                                                                    },
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'bytes',
                                                                            },
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'bool',
                                                                                            },
                                                                                            {
                                                                                                prim: 'string',
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'map',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'address',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'map',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'nat',
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'nat',
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'key',
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'option',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'pair',
                                                                                                                                args: [
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'pair',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'set',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'bytes',
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'map',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'nat',
                                                                                                                                                    },
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'int',
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'pair',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'timestamp',
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'map',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'nat',
                                                                                                                                                    },
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'nat',
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                ],
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'nat',
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                            {
                                                                                                prim: 'int',
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                ],
                                                            },
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'bytes',
                                                                            },
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'map',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'address',
                                                                                            },
                                                                                            {
                                                                                                prim: 'int',
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'pair',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'map',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'int',
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'map',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'nat',
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'nat',
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'bytes',
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'string',
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'pair',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'bytes',
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'pair',
                                                                                                                                args: [
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'int',
                                                                                                                                    },
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'pair',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'map',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'int',
                                                                                                                                                    },
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'address',
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'map',
                                                                                                                                                args: [
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'or',
                                                                                                                                                        args: [
                                                                                                                                                            {
                                                                                                                                                                prim:
                                                                                                                                                                    'string',
                                                                                                                                                            },
                                                                                                                                                            {
                                                                                                                                                                prim:
                                                                                                                                                                    'or',
                                                                                                                                                                args: [
                                                                                                                                                                    {
                                                                                                                                                                        prim:
                                                                                                                                                                            'int',
                                                                                                                                                                    },
                                                                                                                                                                    {
                                                                                                                                                                        prim:
                                                                                                                                                                            'int',
                                                                                                                                                                    },
                                                                                                                                                                ],
                                                                                                                                                            },
                                                                                                                                                        ],
                                                                                                                                                    },
                                                                                                                                                    {
                                                                                                                                                        prim:
                                                                                                                                                            'list',
                                                                                                                                                        args: [
                                                                                                                                                            {
                                                                                                                                                                prim:
                                                                                                                                                                    'pair',
                                                                                                                                                                args: [
                                                                                                                                                                    {
                                                                                                                                                                        prim:
                                                                                                                                                                            'map',
                                                                                                                                                                        args: [
                                                                                                                                                                            {
                                                                                                                                                                                prim:
                                                                                                                                                                                    'nat',
                                                                                                                                                                            },
                                                                                                                                                                            {
                                                                                                                                                                                prim:
                                                                                                                                                                                    'nat',
                                                                                                                                                                            },
                                                                                                                                                                        ],
                                                                                                                                                                    },
                                                                                                                                                                    {
                                                                                                                                                                        prim:
                                                                                                                                                                            'pair',
                                                                                                                                                                        args: [
                                                                                                                                                                            {
                                                                                                                                                                                prim:
                                                                                                                                                                                    'int',
                                                                                                                                                                            },
                                                                                                                                                                            {
                                                                                                                                                                                prim:
                                                                                                                                                                                    'int',
                                                                                                                                                                            },
                                                                                                                                                                        ],
                                                                                                                                                                    },
                                                                                                                                                                ],
                                                                                                                                                            },
                                                                                                                                                        ],
                                                                                                                                                    },
                                                                                                                                                ],
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                ],
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                            {
                                                                                                prim: 'pair',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'nat',
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'option',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'or',
                                                                                                                                args: [
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'string',
                                                                                                                                    },
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'or',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'int',
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'int',
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                ],
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'int',
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'bytes',
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'map',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'string',
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'bytes',
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'pair',
                                                                                                                        args: [
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'bool',
                                                                                                                            },
                                                                                                                            {
                                                                                                                                prim:
                                                                                                                                    'pair',
                                                                                                                                args: [
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'option',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'bytes',
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                    {
                                                                                                                                        prim:
                                                                                                                                            'map',
                                                                                                                                        args: [
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'int',
                                                                                                                                            },
                                                                                                                                            {
                                                                                                                                                prim:
                                                                                                                                                    'timestamp',
                                                                                                                                            },
                                                                                                                                        ],
                                                                                                                                    },
                                                                                                                                ],
                                                                                                                            },
                                                                                                                        ],
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'address',
                                                                                    },
                                                                                    {
                                                                                        prim: 'nat',
                                                                                    },
                                                                                ],
                                                                            },
                                                                            {
                                                                                prim: 'nat',
                                                                            },
                                                                        ],
                                                                    },
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                    {
                                                        prim: 'pair',
                                                        args: [
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'string',
                                                                            },
                                                                            {
                                                                                prim: 'bytes',
                                                                            },
                                                                        ],
                                                                    },
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'bytes',
                                                                            },
                                                                            {
                                                                                prim: 'pair',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'lambda',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'pair',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'pair',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'nat',
                                                                                                            },
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'pair',
                                                                                                                args: [
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'int',
                                                                                                                    },
                                                                                                                    {
                                                                                                                        prim:
                                                                                                                            'bytes',
                                                                                                                    },
                                                                                                                ],
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                            {
                                                                                                prim: 'pair',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'option',
                                                                                                        args: [
                                                                                                            {
                                                                                                                prim:
                                                                                                                    'string',
                                                                                                            },
                                                                                                        ],
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                    {
                                                                                        prim: 'pair',
                                                                                        args: [
                                                                                            {
                                                                                                prim: 'lambda',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                            {
                                                                                                prim: 'map',
                                                                                                args: [
                                                                                                    {
                                                                                                        prim: 'string',
                                                                                                    },
                                                                                                    {
                                                                                                        prim: 'bytes',
                                                                                                    },
                                                                                                ],
                                                                                            },
                                                                                        ],
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                ],
                                                            },
                                                            {
                                                                prim: 'pair',
                                                                args: [
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'nat',
                                                                            },
                                                                            {
                                                                                prim: 'map',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'string',
                                                                                    },
                                                                                    {
                                                                                        prim: 'bytes',
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                    {
                                                                        prim: 'big_map',
                                                                        args: [
                                                                            {
                                                                                prim: 'nat',
                                                                            },
                                                                            {
                                                                                prim: 'map',
                                                                                args: [
                                                                                    {
                                                                                        prim: 'string',
                                                                                    },
                                                                                    {
                                                                                        prim: 'bytes',
                                                                                    },
                                                                                ],
                                                                            },
                                                                        ],
                                                                    },
                                                                ],
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        ],
    },
    returnType: {
        prim: 'option',
        args: [
            {
                prim: 'pair',
                args: [
                    {
                        prim: 'pair',
                        args: [
                            {
                                prim: 'pair',
                                args: [
                                    {
                                        prim: 'bytes',
                                        annots: ['%model_id'],
                                    },
                                    {
                                        prim: 'map',
                                        args: [
                                            {
                                                prim: 'int',
                                            },
                                            {
                                                prim: 'address',
                                            },
                                        ],
                                        annots: ['%players_addr'],
                                    },
                                ],
                            },
                            {
                                prim: 'pair',
                                annots: ['%current'],
                                args: [
                                    {
                                        prim: 'nat',
                                        annots: ['%move_nb'],
                                    },
                                    {
                                        prim: 'pair',
                                        args: [
                                            {
                                                prim: 'option',
                                                args: [
                                                    {
                                                        prim: 'string',
                                                    },
                                                ],
                                                annots: ['%outcome'],
                                            },
                                            {
                                                prim: 'int',
                                                annots: ['%player'],
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        prim: 'pair',
                        args: [
                            {
                                prim: 'bytes',
                                annots: ['%init_input'],
                            },
                            {
                                prim: 'pair',
                                args: [
                                    {
                                        prim: 'bool',
                                        annots: ['%settled'],
                                    },
                                    {
                                        prim: 'option',
                                        annots: ['%state'],
                                        args: [
                                            {
                                                prim: 'bytes',
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        ],
    },
};
