import { hashChannelId, hashGameId, hashModelId } from '../utils/hashers';

describe('Testing hashes', () => {
    it('channel id', () => {
        expect(
            hashChannelId({
                nonce: 'Channel 1',
                platformAddress: 'KT1Je2FTWwVfBLpGQy5Z1ieuUX4G6JKrxDux',
                players: [
                    {
                        address: 'tz1dtXJpDg4e8tYcGE5b7dzStfvHGyVZivaY',
                        publicKey: 'edpkuTgJuK2tK5pixWA27bMZ3JHybicPr6Uy6ang9GWbGvFV4eAPXb',
                    },
                    {
                        address: 'tz1bQx5gL9WLBsc9qyxKj7wGiit78A7frBmK',
                        publicKey: 'edpkv3regPfkqjzWuD29P4tq1zkKokRQnGGpg5UMMvvXnCkXNoRqY2',
                    },
                ],
            }),
        ).toEqual('af484dc3917a539b5f84aaae1c62eb9bf97c9f36144b1ee60e0f4c5625c797d6');
    });

    it('model id', () => {
        expect(
            hashModelId({
                name: 'tictactoe',
                apply: [
                    {
                        prim: 'DROP',
                    },
                    {
                        prim: 'PUSH',
                        args: [
                            {
                                prim: 'option',
                                args: [
                                    {
                                        prim: 'string',
                                    },
                                ],
                            },
                            {
                                prim: 'Some',
                                args: [
                                    {
                                        string: 'transferred',
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        prim: 'UNIT',
                    },
                    {
                        prim: 'UNIT',
                    },
                    {
                        prim: 'PAIR',
                        args: [
                            {
                                int: '3',
                            },
                        ],
                    },
                    {
                        prim: 'DUP',
                    },
                    {
                        prim: 'GET',
                        args: [
                            {
                                int: '4',
                            },
                        ],
                    },
                    {
                        prim: 'SWAP',
                    },
                    {
                        prim: 'DUP',
                    },
                    {
                        prim: 'DUG',
                        args: [
                            {
                                int: '2',
                            },
                        ],
                    },
                    {
                        prim: 'GET',
                        args: [
                            {
                                int: '3',
                            },
                        ],
                    },
                    {
                        prim: 'PACK',
                    },
                    {
                        prim: 'DIG',
                        args: [
                            {
                                int: '2',
                            },
                        ],
                    },
                    {
                        prim: 'CAR',
                    },
                    {
                        prim: 'PACK',
                    },
                    {
                        prim: 'PAIR',
                        args: [
                            {
                                int: '3',
                            },
                        ],
                    },
                ],
                init: [
                    {
                        prim: 'DROP',
                    },
                    {
                        prim: 'UNIT',
                    },
                    {
                        prim: 'PACK',
                    },
                ],
            }),
        ).toEqual('59eb05e21a9832dba7861fe16ce926ca978e9d191a42ddb9c48aa398de59de0b');
    });

    it('game id', () => {
        expect(
            hashGameId({
                channelID: '24581385e1c262bd07770184c16b7fed0deaff8523bbccd6157b0202dbd3e766',
                modelID: '1f3ba8a6fb434d58bb7f572bfe7c0b05c3c9713e139a16437e8b45b1af5d63d0',
                gameNonce: 'game2',
            }),
        ).toEqual('cd1f66404c7e928cb6db3713e9aa7027e8b39badc6cfa18cf9a6e91c7f72cc7a');
    });
});
