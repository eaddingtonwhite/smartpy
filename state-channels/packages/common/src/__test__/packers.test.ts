import { ModelKind, Models } from '../constants/models';
import { Game } from '../utils/packers';

describe('Testing signatures', () => {
    it('new game', () => {
        const packedData = Game.packNewGameAction(Models[ModelKind.tictactoe].parameters(), {
            channelID: '81d6e2fae5bf2d18dee5e6a17d1ee5b39a8be604726b9b6c868a7ae250799d48',
            gameNonce: '20723165754',
            modelID: '4261e0508a06644ceb9339d4b85bde0c96f6e14fb0c5c01044d37f477e70b8e3',
            playDelay: 10,
            bonds: {
                1: {
                    0: 10,
                },
                2: {
                    0: 11,
                },
            },
            settlements: [
                {
                    kind: 'player_1_own',
                    tokenID: 0,
                    amount: 10,
                    sender: 'tz1NHco4L3SqhC8RboiSqXPZCRZsEgym3mff',
                    recipient: 'tz1THWgwHiDfCBk2cjipYPLE3DA6hWqmgEBX',
                },
                {
                    kind: 'player_2_own',
                    tokenID: 0,
                    amount: 11,
                    sender: 'tz1THWgwHiDfCBk2cjipYPLE3DA6hWqmgEBX',
                    recipient: 'tz1NHco4L3SqhC8RboiSqXPZCRZsEgym3mff',
                },
            ],
            players: {
                tz1NHco4L3SqhC8RboiSqXPZCRZsEgym3mff: 1,
                tz1THWgwHiDfCBk2cjipYPLE3DA6hWqmgEBX: 2,
            },
        });
        expect(packedData).toEqual(
            '05070701000000084e65772047616d6507070707020000001e07040001020000000607040000000a07040002020000000607040000000b07070a0000002081d6e2fae5bf2d18dee5e6a17d1ee5b39a8be604726b9b6c868a7ae250799d480707010000000b323037323331363537353407070a000000204261e0508a06644ceb9339d4b85bde0c96f6e14fb0c5c01044d37f477e70b8e30707000a0707020000003e070400010a0000001600001d0c1c01b2dfec703b53d3315102300746c1a648070400020a00000016000053dfa44489438013dec3de6e96b47cf6b550c9f8020000005a07040505010000000c706c617965725f315f776f6e02000000130707020000000607040000000a07070001000207040505010000000c706c617965725f325f776f6e02000000130707020000000607040000000b0707000200010a0000000305030b',
        );
    });
});
