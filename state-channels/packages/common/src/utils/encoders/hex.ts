/**
 * @description Convert hex string into an Buffer of bytes
 * @param {string} hex Hex string
 * @returns {Buffer} Buffer of bytes
 */
export function hexToBuffer(hex: string): Buffer {
    return Buffer.from(hex, 'hex');
}
