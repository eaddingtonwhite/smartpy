import { BytesLiteral, decodeAddressBytes, isMichelsonCode } from '@taquito/michel-codec';
import { Encoders } from '..';
import { PREFIX_STRING } from './bs58';

export interface MichelsonType {
    prim: 'pair' | 'string' | 'address' | 'bytes' | 'nat' | 'int' | 'option' | 'map' | 'bool' | 'or';
    args?: this[];
    annots?: string[];
}

export interface MichelsonValue {
    int?: string;
    string?: string;
    bytes?: string;
    prim?: 'Pair' | 'None' | 'Some' | 'Elt' | 'True' | 'False';
    args?: this[];
}

export function extractFromMichelson(type: MichelsonType, value: MichelsonValue | MichelsonValue[]): any {
    if (Array.isArray(value)) {
        switch (type.prim) {
            case 'map':
                return value.reduce<{ key: unknown; value: unknown }[]>((acc, v) => {
                    if (type.args && v.args) {
                        return [
                            ...acc,
                            {
                                key: extractFromMichelson(type.args?.[0], v.args[0]),
                                value: extractFromMichelson(type.args?.[1], v.args[1]),
                            },
                        ];
                    }
                    throw new Error(`Invalid michelson (${type.prim})`);
                }, []);
        }
        throw new Error(`Invalid michelson type (${type.prim})`);
    }

    console.debug(type, value);
    switch (type.prim) {
        case 'or':
            console.error(type, value);
            // return {
            //     kind: extractAnnot(type.annots),
            //     value: extractFromMichelson(type.args?.[0], v)
            // }
            break;
        case 'bytes':
            return value.bytes;
        case 'string':
            return value.string;
        case 'address':
            if (value.bytes) {
                return Encoders.Bs58.encodeCheck(
                    Buffer.from(decodeAddressBytes(value as BytesLiteral).hash),
                    PREFIX_STRING.tz1,
                );
            }
            throw new Error(`Invalid Michelson (${type.prim})`);
        case 'nat':
        case 'int':
            if (value.int) {
                return BigInt(value.int);
            }
            throw new Error(`Invalid Michelson (${type.prim})`);
        case 'option':
            switch (value.prim) {
                case 'None':
                    return null;
                case 'Some':
                    if (type.args?.[0] && value.args?.[0]) {
                        return extractFromMichelson(type.args[0], value.args[0]);
                    }
            }
            throw new Error(`Invalid Michelson (${type.prim})`);
        case 'bool':
            switch (value.prim) {
                case 'False':
                    return false;
                case 'True':
                    return true;
            }
            throw new Error(`Invalid Michelson (${type.prim})`);
        case 'pair':
            return value.args?.reduce((acc, v, i) => {
                if (type.args?.[i]) {
                    if (type.args?.[i].annots) {
                        return {
                            ...acc,
                            [extractAnnot(type.args?.[i].annots)]: extractFromMichelson(type.args?.[i], v),
                        };
                    }
                    return {
                        ...acc,
                        ...extractFromMichelson(type.args?.[i], v),
                    };
                }
                throw new Error(`Invalid michelson type (${type.prim})`);
            }, {});
    }
}

function extractAnnot(annots?: string[]): string {
    return annots?.[0].replace('%', '') || '';
}
