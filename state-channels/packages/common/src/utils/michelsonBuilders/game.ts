import { MichelsonData } from '@taquito/michel-codec';

import { GameConstants, GameID, SC_Settlement, SC_SettlementKind } from '../../@types/game';
import { ModelKind, Models } from '../../constants';
import { compare } from '../comparison';

/**
 * @description Build outcome michelson
 * @param {ModelKind} model Game model kind
 * @param {SC_GameOutcome} outcome Game outcome
 * @returns {MichelsonData} Game outcome michelson
 */
export function buildOutcome(outcome?: Record<string, string> | string): MichelsonData {
    if (!outcome) {
        return { prim: 'None' };
    }

    if (typeof outcome === 'string') {
        return { prim: 'Some', args: [{ prim: 'Left', args: [{ string: outcome }] }] };
    }

    if (outcome['game_finished']) {
        return { prim: 'Some', args: [{ prim: 'Left', args: [{ string: outcome['game_finished'] }] }] };
    }
    if (outcome['player_double_played']) {
        return {
            prim: 'Some',
            args: [{ prim: 'Right', args: [{ prim: 'Left', args: [{ string: String(outcome) }] }] }],
        };
    }
    if (outcome['player_inactive']) {
        return {
            prim: 'Some',
            args: [{ prim: 'Right', args: [{ prim: 'Right', args: [{ string: String(outcome) }] }] }],
        };
    }

    throw new Error(`Outcome ${JSON.stringify(outcome)} not expected.`);
}

export const buildID = (data: GameID): MichelsonData => ({
    prim: 'Pair',
    args: [
        { bytes: data.channelID },
        {
            prim: 'Pair',
            args: [{ bytes: data.modelID }, { string: data.gameNonce }],
        },
    ],
});

export function buildParameters(modelKind: string): string {
    switch (modelKind) {
        case ModelKind.tictactoe:
            return Models[modelKind].parameters();
    }

    throw new Error(`Unknown model kind (${modelKind}).`);
}

export const buildNewGameAction = (initParams: string, constants: GameConstants): MichelsonData => {
    const constantsMichelson = buildConstants(constants);

    return {
        prim: 'Pair',
        args: [{ string: 'New Game' }, constantsMichelson, { bytes: initParams }],
    };
};

export function buildSettlement(settlement: SC_Settlement, sender: number, recipient: number) {
    const bondsMichelson = [
        {
            prim: 'Pair',
            args: [
                Object.entries(settlement.bonds).map(([tokenID, amount]) => ({
                    prim: 'Elt',
                    args: [{ int: String(tokenID) }, { int: String(amount) }],
                })),
                {
                    prim: 'Pair',
                    args: [{ int: String(sender) }, { int: String(recipient) }],
                },
            ],
        },
    ];
    switch (settlement.kind) {
        case SC_SettlementKind.GameFinished:
            return {
                prim: 'Elt',
                args: [
                    {
                        prim: 'Left',
                        args: [{ string: String(settlement.value) }],
                    },
                    bondsMichelson,
                ],
            };
        case SC_SettlementKind.PlayerDoublePlayed:
            return {
                prim: 'Elt',
                args: [
                    {
                        prim: 'Right',
                        args: [
                            {
                                prim: 'Left',
                                args: [{ int: String(settlement.value) }],
                            },
                        ],
                    },
                    bondsMichelson,
                ],
            };
        case SC_SettlementKind.PlayerInactive:
            return {
                prim: 'Elt',
                args: [
                    {
                        prim: 'Right',
                        args: [
                            {
                                prim: 'Right',
                                args: [{ int: String(settlement.value) }],
                            },
                        ],
                    },
                    bondsMichelson,
                ],
            };
    }
}

export const buildConstants = (constants: GameConstants): MichelsonData => {
    const settlements = constants.settlements
        .sort(({ kind: kind1 }, { kind: kind2 }) => compare(kind1, kind2))
        .reduce<MichelsonData[]>((settlements, cur) => {
            const settlementMichelson = buildSettlement(cur, cur.sender, cur.recipient);

            return [...settlements, (settlementMichelson as unknown) as MichelsonData];
        }, []);

    return ({
        prim: 'Pair',
        args: [
            Object.entries(constants.bonds)
                .sort(([i1], [i2]) => compare(i1, i2))
                .map(([pIndex, pBonds]) => ({
                    prim: 'Elt',
                    args: [
                        { int: pIndex },
                        Object.entries(pBonds)
                            .sort(([i1], [i2]) => compare(i1, i2))
                            .map(([bIndex, bAmount]) => ({
                                prim: 'Elt',
                                args: [{ int: bIndex }, { int: String(bAmount) }],
                            })),
                    ],
                })),
            {
                prim: 'Pair',
                args: [
                    { bytes: constants.channelID },
                    {
                        prim: 'Pair',
                        args: [
                            { string: constants.gameNonce },
                            {
                                prim: 'Pair',
                                args: [
                                    {
                                        bytes: constants.modelID,
                                    },
                                    {
                                        prim: 'Pair',
                                        args: [
                                            { int: String(constants.playDelay) },
                                            {
                                                prim: 'Pair',
                                                args: [
                                                    Object.entries(constants.players)
                                                        .sort((i1, i2) => compare(i1[1], i2[1]))
                                                        .map(([address, index]) => ({
                                                            prim: 'Elt',
                                                            args: [
                                                                { int: String(index) },
                                                                {
                                                                    string: address,
                                                                },
                                                            ],
                                                        })),
                                                    settlements,
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        ],
    } as unknown) as MichelsonData;
};
