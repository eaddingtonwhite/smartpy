import { blake2bHex } from 'blakejs';
import { SC_NewModelRequest } from '../../@types/model';
import { Packers } from '..';

export function hashID(data: SC_NewModelRequest): string {
    return blake2bHex(Buffer.from(Packers.Model.packID(data), 'hex'), undefined, 32);
}
