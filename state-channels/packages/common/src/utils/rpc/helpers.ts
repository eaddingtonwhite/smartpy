import { Constants } from '../../constants';
import Http from '../../services/http';

/**
 * @description Execute code against a given storage
 * @param {Record<string, unknown>} storage contract storage
 * @param {unknown} code Lambda to execute against the storage
 * @param {Record<string, unknown>} parameter lambda input type
 * @param {Record<string, unknown>} returnType lambda ouput type
 * @param {Record<string, unknown>} input lambda input parameters
 * @param {string} chainID chain identifier
 */
export async function runCode(
    storage: Record<string, unknown>,
    code: unknown,
    parameter: Record<string, unknown>,
    returnType: Record<string, unknown>,
    input: Record<string, unknown>,
    chainID: string,
): Promise<Record<string, unknown>> {
    return (
        await Http.post(
            `${Constants.rpc}/chains/main/blocks/head/helpers/scripts/run_code`,
            {
                script: [
                    {
                        prim: 'parameter',
                        args: [parameter],
                    },
                    {
                        prim: 'storage',
                        args: [returnType],
                    },
                    {
                        prim: 'code',
                        args: [code],
                    },
                ],
                storage: {
                    prim: 'None',
                },
                input: {
                    prim: 'Pair',
                    args: [input, storage],
                },
                amount: '0',
                chain_id: chainID,
                balance: '0',
                unparsing_mode: 'Optimized_legacy',
            },
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            },
        )
    ).data;
}
