/**
 * @description Simple compare
 * @param {string | number} v1
 * @param {string | number} v2
 * @returns {number}
 */
export const compare = (v1: string | number, v2: string | number): number => (v1 > v2 ? 1 : v1 === v2 ? 0 : -1);
