import { MichelsonData } from '@taquito/michel-codec';
import { Comparison, MichelsonBuilders } from '..';
import { GameConstants, GameCurrent, GameSignature } from '../../@types/game';
import { buildOutcome } from '../michelsonBuilders/game';

/**
 * @description Build parameters for "new_game" entry point call
 * @param modelKind
 * @param constants
 * @param signatures
 * @returns
 */
export function newGame(
    modelKind: string,
    constants: GameConstants,
    signatures: GameSignature[],
): {
    entrypoint: string;
    value: MichelsonData;
} {
    const constantsMichelson: MichelsonData = MichelsonBuilders.Game.buildConstants(constants);
    return {
        entrypoint: 'new_game',
        value: {
            prim: 'Pair',
            args: [
                constantsMichelson,
                {
                    prim: 'Pair',
                    args: [
                        { bytes: MichelsonBuilders.Game.buildParameters(modelKind) },
                        signatures
                            .sort(({ publicKey: pk1 }, { publicKey: pk2 }) => Comparison.compare(pk1, pk2))
                            .map(({ publicKey, signature }) => ({
                                prim: 'Elt',
                                args: [
                                    { string: publicKey },
                                    {
                                        string: signature,
                                    },
                                ],
                            })),
                    ],
                },
            ],
        },
    };
}

/**
 * @description Build parameters for "update_state" entry point call
 * @param gameID
 * @param current
 * @param state
 * @param signatures
 * @returns
 */
export function updateState(
    gameID: string,
    current: GameCurrent,
    state: string,
    signatures: GameSignature[],
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'update_state',
        value: {
            prim: 'Pair',
            args: [
                {
                    prim: 'Pair',
                    args: [
                        {
                            bytes: gameID,
                        },
                        {
                            prim: 'Pair',
                            args: [
                                { int: String(current.moveNumber) },
                                {
                                    prim: 'Pair',
                                    args: [buildOutcome(current.outcome), { int: String(current.player) }],
                                },
                            ],
                        },
                    ],
                },
                {
                    prim: 'Pair',
                    args: [
                        { bytes: state },
                        signatures
                            .sort(({ publicKey: pk1 }, { publicKey: pk2 }) => Comparison.compare(pk1, pk2))
                            .map(({ publicKey, signature }) => ({
                                prim: 'Elt',
                                args: [
                                    { string: publicKey },
                                    {
                                        string: signature,
                                    },
                                ],
                            })),
                    ],
                },
            ],
        },
    };
}

/**
 * @description Build parameters for "settle" entry point call
 * @param gameID
 * @returns
 */
export function settle(
    gameID: string,
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'settle',
        value: {
            bytes: gameID,
        },
    };
}

/**
 * @description Build parameters for "starving" entry point call
 * @param gameID
 * @param flag
 * @returns
 */
export function starving(
    gameID: string,
    flag: boolean,
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'starving',
        value: {
            prim: 'Pair',
            args: [{ prim: flag ? 'True' : 'False' }, { bytes: gameID }],
        },
    };
}

/**
 * @description Build parameters for "starved" entry point call
 * @param gameID
 * @param playerID
 * @returns
 */
export function starved(
    gameID: string,
    playerID: number,
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'starved',
        value: {
            prim: 'Pair',
            args: [{ bytes: gameID }, { int: String(playerID) }],
        },
    };
}
