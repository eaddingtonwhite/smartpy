import type { MichelsonData } from '@taquito/michel-codec';
import { Comparison } from '..';

/**
 * @description Build parameters for "new_channel" entry point call
 * @param nonce
 * @param withdrawDelay
 * @param players
 * @returns
 */
export function newChannel(
    nonce: string,
    withdrawDelay: number,
    players: { address: string; publicKey: string }[],
): {
    entrypoint: string;
    value: MichelsonData;
} {
    players = players.sort(({ address: a1 }, { address: a2 }) => Comparison.compare(a1, a2));
    return {
        entrypoint: 'new_channel',
        value: {
            prim: 'Pair',
            args: [
                { string: nonce },
                {
                    prim: 'Pair',
                    args: [
                        [
                            {
                                prim: 'Elt',
                                args: [{ string: players[0].address }, { string: players[0].publicKey }],
                            },
                            {
                                prim: 'Elt',
                                args: [{ string: players[1].address }, { string: players[1].publicKey }],
                            },
                        ],
                        { int: String(withdrawDelay) },
                    ],
                },
            ],
        },
    };
}

/**
 * @description Build parameters for "push_bonds" entry point call
 * @param channel_id
 * @param player
 * @param bonds
 * @returns
 */
export function pushBonds(
    channel_id: string,
    player: string,
    bonds: { tokenId: number; amount: number }[],
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'push_bonds',
        value: {
            prim: 'Pair',
            args: [
                bonds.map(({ tokenId, amount }) => ({
                    prim: 'Elt',
                    args: [{ int: `${tokenId}` }, { int: `${amount}` }],
                })),
                {
                    prim: 'Pair',
                    args: [
                        {
                            bytes: channel_id,
                        },
                        {
                            string: player,
                        },
                    ],
                },
            ],
        },
    };
}
