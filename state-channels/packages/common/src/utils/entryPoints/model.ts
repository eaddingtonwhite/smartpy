import type { MichelsonData } from '@taquito/michel-codec';
import type { SC_NewModelRequest } from '../../@types/model';

/**
 * @description Build parameters for "new_model" entry point call
 * @param model
 * @returns
 */
export function newModel(
    model: SC_NewModelRequest,
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'new_model',
        value: {
            prim: 'Pair',
            args: [[[model.apply], [model.init]], { string: model.name }],
        },
    };
}
