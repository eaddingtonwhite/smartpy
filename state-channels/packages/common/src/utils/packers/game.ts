import { MichelsonType, packDataBytes, unpackDataBytes } from '@taquito/michel-codec';
import { MichelsonMap, Schema } from '@taquito/michelson-encoder';
import type { BigNumber } from 'bignumber.js';

import { GameID, GameConstants, SC_GameStateOfModel } from '../../@types/game';
import { ModelKind } from '../../constants/models';
import { buildID, buildNewGameAction, buildOutcome } from '../michelsonBuilders/game';

/**
 * @description Pack a game state action
 * @param {string} initParams Game initialization parameters
 * @param {GameConstants} constants Game constantants
 * @returns {string} Packed new game action
 */
export function packNewGameAction(initParams: string, constants: GameConstants): string {
    const type: MichelsonType = {
        prim: 'pair',
        args: [
            { prim: 'string' },
            {
                prim: 'pair',
                args: [
                    {
                        prim: 'map',
                        args: [{ prim: 'int' }, { prim: 'map', args: [{ prim: 'nat' }, { prim: 'nat' }] }],
                        annots: ['%bonds'],
                    },
                    { prim: 'bytes', annots: ['%channel_id'] },
                    { prim: 'string', annots: ['%game_nonce'] },
                    { prim: 'bytes', annots: ['%model_id'] },
                    { prim: 'int', annots: ['%play_delay'] },
                    { prim: 'map', args: [{ prim: 'int' }, { prim: 'address' }], annots: ['%players_addr'] },
                    {
                        prim: 'map',
                        args: [
                            {
                                prim: 'or',
                                args: [
                                    { prim: 'string', annots: ['%game_finished'] },
                                    {
                                        prim: 'or',
                                        args: [
                                            { prim: 'int', annots: ['%player_double_played'] },
                                            { prim: 'int', annots: ['%player_inactive'] },
                                        ],
                                    },
                                ],
                            },
                            {
                                prim: 'list',
                                args: [
                                    {
                                        prim: 'pair',
                                        args: [
                                            {
                                                prim: 'map',
                                                args: [{ prim: 'nat' }, { prim: 'nat' }],
                                                annots: ['%bonds'],
                                            },
                                            { prim: 'int', annots: ['%receiver'] },
                                            { prim: 'int', annots: ['%sender'] },
                                        ],
                                    },
                                ],
                            },
                        ],
                        annots: ['%settlements'],
                    },
                ],
                annots: ['%constants'],
            },
            { prim: 'bytes', annots: ['%params'] },
        ],
    };
    return packDataBytes(buildNewGameAction(initParams, constants), type).bytes;
}

export function packID(data: GameID): string {
    const type: MichelsonType = {
        prim: 'pair',
        args: [
            { prim: 'bytes' },
            {
                prim: 'pair',
                args: [{ prim: 'bytes' }, { prim: 'string' }],
            },
        ],
    };
    return packDataBytes(buildID(data), type).bytes;
}

/**
 * @description Pack a game state action
 * @param {ModelKind} model Game model kind
 * @param {string} gameID The game identifier (in bytes)
 * @param current
 * @param {string} state packed game
 * @returns {string} packed new state action
 */
export function packNewStateAction(
    gameID: string,
    current: { moveNumber: number; player: number; outcome?: Record<string, string> },
    state: string,
): string {
    return packDataBytes(
        {
            prim: 'Pair',
            args: [
                { string: 'New State' },
                { bytes: gameID },
                {
                    prim: 'Pair',
                    args: [
                        { int: String(current.moveNumber), annots: ['%move_nb'] },
                        buildOutcome(current.outcome),
                        { int: String(current.player), annots: ['%player'] },
                    ],
                },
                { bytes: state },
            ],
        },
        {
            prim: 'pair',
            args: [
                { prim: 'string' },
                { prim: 'bytes' },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'nat', annots: ['%move_nb'] },
                        {
                            prim: 'option',
                            args: [
                                {
                                    prim: 'or',
                                    args: [
                                        { prim: 'string', annots: ['%game_finished'] },
                                        {
                                            prim: 'or',
                                            args: [
                                                { prim: 'int', annots: ['%player_double_played'] },
                                                { prim: 'int', annots: ['%player_inactive'] },
                                            ],
                                        },
                                    ],
                                },
                            ],
                            annots: ['%outcome'],
                        },
                        { prim: 'int', annots: ['%player'] },
                    ],
                },
                { prim: 'bytes' },
            ],
        },
    ).bytes;
}

/**
 * @description Pack game state
 * @param {ModelKind} model Model kind
 * @param {SC_GameStateOfModel<typeof model>} state Game state
 * @returns {string} Packed game state
 */
export function packState(model: ModelKind, state?: SC_GameStateOfModel<typeof model>): string {
    switch (model) {
        case ModelKind.tictactoe:
            // Default state (Necessary at initialization)
            state = state || [
                [0, 0, 0],
                [0, 0, 0],
                [0, 0, 0],
            ];
            return packDataBytes(
                state.map(
                    (row, rowIndex) => ({
                        prim: 'Elt',
                        args: [
                            {
                                int: String(rowIndex),
                            },
                            row.map((value, colIndex) => ({
                                prim: 'Elt',
                                args: [
                                    {
                                        int: String(colIndex),
                                    },
                                    {
                                        int: String(value),
                                    },
                                ],
                            })),
                        ],
                    }),
                    {
                        prim: 'map',
                        args: [
                            { prim: 'int' },
                            {
                                prim: 'map',
                                args: [{ prim: 'int' }, { prim: 'int' }],
                            },
                        ],
                    },
                ),
            ).bytes;
    }
}

/**
 * @description Unpack game state
 * @param {ModelKind} model Model kind
 * @param {string} state Packed game state
 * @returns {SC_GameStateOfModel<typeof model>} Unpacked game state
 */
export function unpackState(model: ModelKind, state: string): SC_GameStateOfModel<typeof model> {
    switch (model) {
        case ModelKind.tictactoe:
            const type: MichelsonType = {
                prim: 'map',
                args: [
                    {
                        prim: 'nat',
                    },
                    {
                        prim: 'map',
                        args: [
                            {
                                prim: 'nat',
                            },
                            {
                                prim: 'nat',
                            },
                        ],
                    },
                ],
            };
            const boardSchema = new Schema(type);
            const boardMap: MichelsonMap<string, MichelsonMap<string, BigNumber>> = boardSchema.Execute(
                unpackDataBytes({ bytes: state }, type),
            );
            // build board from map
            const board: number[][] = [];
            for (const [key, value] of boardMap.entries()) {
                const rowIndex = Number(key);
                board[rowIndex] = [];
                for (const [col, player] of value.entries()) {
                    board[rowIndex][Number(col)] = player.toNumber();
                }
            }
            return board;
    }
}
