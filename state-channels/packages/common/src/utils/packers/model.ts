import { MichelsonData, MichelsonType, packDataBytes } from '@taquito/michel-codec';
import { SC_NewModelRequest } from '../../@types/model';

const ModelIdType: MichelsonType = {
    prim: 'pair',
    args: [
        {
            prim: 'lambda',
            args: [
                {
                    prim: 'pair',
                    args: [
                        {
                            prim: 'pair',
                            args: [
                                { prim: 'nat', annots: ['%move_nb'] },
                                {
                                    prim: 'option',
                                    args: [
                                        {
                                            prim: 'or',
                                            args: [
                                                { prim: 'string', annots: ['%game_finished'] },
                                                {
                                                    prim: 'or',
                                                    args: [
                                                        { prim: 'int', annots: ['%player_double_played'] },
                                                        { prim: 'int', annots: ['%player_inactive'] },
                                                    ],
                                                },
                                            ],
                                        },
                                    ],
                                    annots: ['%outcome'],
                                },
                                { prim: 'int', annots: ['%player'] },
                            ],
                            annots: ['%current'],
                        },
                        { prim: 'bytes', annots: ['%move'] },
                        { prim: 'bytes', annots: ['%state'] },
                    ],
                },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'bytes', annots: ['%messages'] },
                        { prim: 'bytes', annots: ['%new_state'] },
                        { prim: 'option', args: [{ prim: 'string' }], annots: ['%outcome'] },
                    ],
                },
            ],
            annots: ['%apply_'],
        },
        { prim: 'lambda', args: [{ prim: 'bytes' }, { prim: 'bytes' }], annots: ['%init'] },
    ],
};

const buildID = (data: SC_NewModelRequest): MichelsonData => ({
    prim: 'Pair',
    args: [[data.apply], [data.init]],
});

export function packID(data: SC_NewModelRequest): string {
    return packDataBytes(buildID(data), ModelIdType).bytes;
}
