import { MichelsonData, MichelsonType, unpackDataBytes } from '@taquito/michel-codec';

export * as Channel from './channel';
export * as Model from './model';
export * as Game from './game';

export function unpackData(bytes: string, t?: MichelsonType): MichelsonData {
    return unpackDataBytes({ bytes }, t);
}
