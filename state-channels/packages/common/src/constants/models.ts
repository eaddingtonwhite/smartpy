import { packDataBytes } from '@taquito/michel-codec';
import { MichelsonMap } from '@taquito/michelson-encoder';
import { Schema } from '@taquito/michelson-encoder';
import type BigNumber from 'bignumber.js';
import { Constants } from '.';
import { GameConstants, GameSignature, SC_GameMoveOfModel, SC_GameOfModel } from '../@types';
import { offchain_play } from '../offchains_views/offchain_play';
import { offchain_new_game } from '../offchains_views/offchain_new_game';
import { Packers, RPC, MichelsonBuilders, Comparison } from '../utils';
import { Game } from '../utils/michelsonBuilders';

export enum Settlements {
    player_1_own = 'player_1_own',
    player_2_own = 'player_2_own',
    draw = 'draw',
}

export type SettlementID = keyof typeof Settlements;

export enum ModelKind {
    tictactoe = 'tictactoe',
}

const PACKED_UNIT = '05030b';

export function modelKindOfModelID(modelID: string): ModelKind {
    for (const modelKind of Object.keys(Models)) {
        if (Models[modelKind as ModelKind].id === modelID) {
            return modelKind as ModelKind;
        }
    }
    throw Error(`There is no model with ID: ${modelID}.`);
}

export const Models = {
    [ModelKind.tictactoe]: {
        id: '8f49a25bb3840ff377259604a336f810f3807a39938bc739d9cf2ace0de1ad76',
        parameters: () => PACKED_UNIT,
        offchain_new_game: async (
            constants: GameConstants,
            initParams: string,
            signatures: GameSignature[],
        ): Promise<SC_GameOfModel<ModelKind.tictactoe>> => {
            const input = {
                prim: 'Pair',
                args: [
                    MichelsonBuilders.Game.buildConstants(constants),
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: initParams },
                            signatures
                                .sort(({ publicKey: pk1 }, { publicKey: pk2 }) => Comparison.compare(pk1, pk2))
                                .map((signature) => ({
                                    prim: 'Elt',
                                    args: [
                                        { string: signature.publicKey },
                                        {
                                            string: signature.signature,
                                        },
                                    ],
                                })),
                        ],
                    },
                ],
            };

            const chainID = await RPC.Chain.getChainID();
            const storage = await RPC.Contract.getNormalizedStorage(Constants.platformAddress);
            const result = await RPC.Helpers.runCode(
                storage,
                offchain_new_game.code,
                offchain_new_game.parameter,
                offchain_new_game.returnType,
                input,
                chainID,
            );

            const resultSchema = new Schema(offchain_new_game.returnType);
            const viewResult = resultSchema.Execute(result.storage);
            const getPlayers = (playersMap: MichelsonMap<string, BigNumber>): Record<number, string> => {
                const players: Record<number, string> = {};
                for (const [key, value] of playersMap.entries()) {
                    players[value.toNumber()] = key;
                }
                return players;
            };
            return {
                settled: viewResult.settled,
                initParams: viewResult.init_input,
                modelID: viewResult.constants.model_id,
                current: {
                    moveNumber: viewResult.current.move_nb.toNumber(),
                    player: viewResult.current.player.toNumber(),
                    outcome: viewResult.current.outcome,
                },
                players: getPlayers(viewResult.addr_players),
            };
        },
        offchain_play: async (params: {
            player: string;
            move: SC_GameMoveOfModel<ModelKind.tictactoe>;
            game: SC_GameOfModel<ModelKind.tictactoe>;
        }): Promise<SC_GameOfModel<ModelKind.tictactoe>> => {
            const input = {
                prim: 'Pair',
                args: [
                    {
                        prim: 'Pair',
                        args: [
                            {
                                prim: 'Pair',
                                args: [
                                    {
                                        prim: 'Pair',
                                        args: [
                                            {
                                                prim: 'Pair',
                                                args: [
                                                    {
                                                        bytes: params.game.modelID,
                                                    },
                                                    Object.entries(params.game.players).map(([index, address]) => ({
                                                        prim: 'Elt',
                                                        args: [
                                                            {
                                                                int: String(index),
                                                            },
                                                            {
                                                                string: address,
                                                            },
                                                        ],
                                                    })),
                                                ],
                                            },
                                            {
                                                prim: 'Pair',
                                                args: [
                                                    {
                                                        int: String(params.game.current.moveNumber),
                                                    },
                                                    {
                                                        prim: 'Pair',
                                                        args: [
                                                            Game.buildOutcome(params.game.current.outcome),
                                                            {
                                                                int: String(params.game.current.player),
                                                            },
                                                        ],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    {
                                        prim: 'Pair',
                                        args: [
                                            {
                                                bytes: params.game.initParams,
                                            },
                                            {
                                                prim: 'Pair',
                                                args: [
                                                    {
                                                        prim: params.game.settled ? 'True' : 'False',
                                                    },
                                                    params.game.state
                                                        ? {
                                                              prim: 'Some',
                                                              args: [
                                                                  {
                                                                      bytes: Packers.Game.packState(
                                                                          ModelKind.tictactoe,
                                                                          params.game.state,
                                                                      ),
                                                                  },
                                                              ],
                                                          }
                                                        : { prim: 'None' },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                            packDataBytes(
                                {
                                    prim: 'Pair',
                                    args: [
                                        {
                                            int: String(params.move.row),
                                        },
                                        {
                                            int: String(params.move.col),
                                        },
                                    ],
                                },
                                {
                                    prim: 'pair',
                                    args: [
                                        { prim: 'int', annots: ['%x'] },
                                        { prim: 'int', annots: ['%y'] },
                                    ],
                                    annots: ['%move_data'],
                                },
                            ),
                        ],
                    },
                    {
                        prim: 'Pair',
                        args: [
                            {
                                int: String(params.game.current.moveNumber),
                            },
                            {
                                string: params.player,
                            },
                        ],
                    },
                ],
            };

            const chainID = await RPC.Chain.getChainID();
            const storage = await RPC.Contract.getNormalizedStorage(Constants.platformAddress);
            const result = await RPC.Helpers.runCode(
                storage,
                offchain_play.code,
                offchain_play.parameter,
                offchain_play.returnType,
                input,
                chainID,
            );
            const getPlayers = (playersMap: MichelsonMap<number, string>): Record<number, string> => {
                const players: Record<number, string> = {};
                for (const [key, value] of playersMap.entries()) {
                    players[key] = value;
                }
                return players;
            };
            const resultSchema = new Schema(offchain_play.returnType);
            const { init_input, settled, model_id, state, current, players_addr } = resultSchema.Execute(
                result.storage,
            );
            return {
                settled,
                initParams: init_input,
                modelID: model_id,
                current: {
                    moveNumber: current.move_nb.toNumber(),
                    player: current.player.toNumber(),
                    outcome: current.outcome,
                },
                players: getPlayers(players_addr),
                state: Packers.Game.unpackState(ModelKind.tictactoe, state),
            };
        },
    },
} as const;
