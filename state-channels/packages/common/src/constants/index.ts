export * from './models';
export * from './tokens';

export const Constants = {
    network: {
        type: 'granadanet',
    },
    platformAddress: 'KT1DK9ZPjmqQQ8iUjqec6hNuCALpjaWnQ4Uv',
    rpc: 'https://granadanet.smartpy.io',
    api: 'http://localhost:3005',
    graphQlHttp: 'http://localhost:8081/v1/graphql', // 'http://edonet.smartpy.io:8081/v1/graphql'
    graphQlWs: 'ws://localhost:8081/v1/graphql', // 'ws://edonet.smartpy.io:8081/v1/graphql'
};
