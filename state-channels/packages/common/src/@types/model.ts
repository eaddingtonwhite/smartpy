import type { MichelsonData } from '@taquito/michel-codec';

export interface SC_NewModelRequest {
    name: string;
    init: MichelsonData;
    apply: MichelsonData;
}

export interface SC_Model {
    id: string;
    name: string;
    updatedAt: string;
}
