import { ModelKind } from '../constants';

export enum SC_SettlementKind {
    GameFinished = 'game_finished',
    PlayerInactive = 'player_inactive',
    PlayerDoublePlayed = 'player_double_played',
}

export interface SC_Settlement {
    kind: SC_SettlementKind;
    value: number | string;
    bonds: {
        [tokenID: number]: number /* amount */;
    };
    sender: number;
    recipient: number;
}

export interface GameID {
    channelID: string;
    modelID: string;
    gameNonce: string;
}

export interface GameConstants {
    channelID: string;
    modelID: string;
    gameNonce: string;
    playDelay: number;
    bonds: Record<number, { [bound: number]: number }>;
    players: {
        [address: string]: number;
    };
    settlements: SC_Settlement[];
}

export interface NewGameRequest {
    signature: GameSignature;
    initParams: string;
    constants: GameConstants;
}

export interface NewGameStateRequest {
    signature: GameSignature;
    move: SC_GameMoveOfModel<any>;
    current: GameCurrent;
    state: string;
}

export interface AcceptGameStateRequest {
    signature: GameSignature;
    moveNumber: number;
}

export interface GameSignature {
    publicKey: string;
    signature: string;
}

export interface GameCurrent {
    player: number;
    moveNumber: number;
    outcome?: Record<string, string>;
}

export type SC_GameMoveOfModel<M extends ModelKind> = Pick<
    { [ModelKind.tictactoe]: { row: number; col: number } },
    M
>[M];

export type SC_GameStateOfModel<M extends ModelKind> = Pick<{ [ModelKind.tictactoe]: number[][] }, M>[M];

export interface SC_GameOfModel<M extends ModelKind> {
    settled: boolean;
    initParams: string;
    modelID: string;
    current: GameCurrent;
    players: Record<number, string>;
    state?: SC_GameStateOfModel<M>;
}

export interface SC_GameMoveAction<M extends ModelKind> {
    id: number;
    moveNumber: number;
    playerNumber: number;
    outcome: string;
    state: string;
    move: SC_GameMoveOfModel<M>;
    signatures: GameSignature[];
}
export interface SC_Game<M extends ModelKind> {
    id: string;
    onChain: boolean;
    settled: boolean;
    state?: string;
    currentPlayer: number;
    metadata?: any;
    moveNumber: number;
    outcome?: Record<string, string>;
    timeouts: {
        createdAt: string;
        timeout: string;
        playerID: number;
        updatedAt: string;
    }[];
    signatures: GameSignature[];
    // constants
    channelID: string;
    modelID: string;
    nonce: string;
    initParams: string;
    players: Record<string, number>;
    settlements: SC_Settlement[];
    bonds: Record<number, { [bound: number]: number }>;
    playDelay: number;
    // audit fields
    updatedAt: string;
    createdAt: string;
    model: {
        name: string;
    };
    latestMove: SC_GameMoveAction<M>[];
    pendingMove: SC_GameMoveAction<M>[];
}
