export default {
    generic: {
        UNEXPECTED_ERROR: 'api.generic.error.unexpected',
    },
    game: {
        UNEXPECTED_ERROR: 'api.game.error.unexpected',
        INVALID: 'api.game.error.invalid',
        CONFLICT: 'api.game.error.conflict',
        NOT_FOUND: 'api.game.error.not_found',
    },
};
