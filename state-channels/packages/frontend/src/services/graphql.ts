import { ApolloClient, InMemoryCache, gql } from '@apollo/client';
import { split, HttpLink } from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/client/link/ws';

import { Constants } from 'state-channels-common';

export const CHANNEL_QUERY = gql`
    query Channel($id: String!) {
        channels: channel(where: { id: { _eq: $id } }) {
            id
            closed
            nonce
            withdrawDelay: withdraw_delay
            createdAt: created_at
            updatedAt: updated_at
            participants {
                publicKeyHash: public_key_hash
                publicKey: public_key
                withdrawID: withdraw_id
                bonds {
                    amount
                    id: bond_id
                }
            }
            games(order_by: { created_at: desc }, limit: 30) {
                id
                bonds
                nonce: game_nonce
                settlements
                state
                settled
                playDelay: play_delay
                players
                model {
                    name
                }
                signatures {
                    publicKey: public_key
                    signature
                }
                createdAt: created_at
                updatedAt: updated_at
            }
        }
    }
`;

export const SUB_CHANNELS = gql`
    subscription Channels($address: String!) {
        channels: channel(
            where: { participants: { public_key_hash: { _eq: $address } } }
            order_by: { created_at: desc }
            limit: 10
        ) {
            id
            closed
            nonce
            withdrawDelay: withdraw_delay
            createdAt: created_at
            updatedAt: updated_at
            participants {
                publicKeyHash: public_key_hash
                publicKey: public_key
                withdrawID: withdraw_id
                bonds {
                    amount
                    id: bond_id
                }
            }
            games(order_by: { created_at: desc }, limit: 30) {
                id
                bonds
                nonce: game_nonce
                settlements
                state
                settled
                playDelay: play_delay
                players
                model {
                    name
                }
                signatures {
                    publicKey: public_key
                    signature
                }
                createdAt: created_at
                updatedAt: updated_at
            }
        }
    }
`;

export const SUB_MODELS = gql`
    subscription Models {
        models: model(order_by: { created_at: desc }, limit: 30) {
            id
            name
            createdAt: created_at
            updatedAt: updated_at
        }
    }
`;

export const SUB_GAMES = gql`
    subscription Games($channelID: String!) {
        games: game(order_by: { created_at: desc }, where: { channel_id: { _eq: $channelID } }, limit: 30) {
            id
            bonds
            channelID: channel_id
            nonce: game_nonce
            modelID: model_id
            playDelay: play_delay
            settlements
            players
            currentPlayer: current_player
            initParams: init_params
            metadata
            moveNumber: move_nb
            onChain: on_chain
            outcome
            settled
            state
            createdAt: created_at
            updatedAt: updated_at
            timeouts {
                playerID: player_id
                timeout
                createdAt: created_at
                updatedAt: updated_at
            }
            signatures {
                publicKey: public_key
                signature
            }
            model {
                name
            }
            moves {
                id
                move_number
                outcome
                signatures {
                    public_key
                    signature
                }
            }
        }
    }
`;

export const SUB_GAME = gql`
    subscription Game($id: String!) {
        games: game(where: { id: { _eq: $id } }, limit: 1) {
            id
            bonds
            channelID: channel_id
            nonce: game_nonce
            modelID: model_id
            playDelay: play_delay
            settlements
            players
            currentPlayer: current_player
            initParams: init_params
            metadata
            moveNumber: move_nb
            onChain: on_chain
            outcome
            settled
            state
            createdAt: created_at
            updatedAt: updated_at
            timeouts {
                playerID: player_id
                timeout
                createdAt: created_at
                updatedAt: updated_at
            }
            signatures {
                publicKey: public_key
                signature
            }
            model {
                name
            }
            latestMove: moves(
                where: { total_game_move_signatures: { _gte: 2 } }
                order_by: { move_number: desc }
                limit: 1
            ) {
                id
                moveNumber: move_number
                playerNumber: player_number
                outcome
                state
                move: move_data
                signatures {
                    publicKey: public_key
                    signature
                }
            }
            pendingMove: moves(
                where: { total_game_move_signatures: { _lt: 2 } }
                order_by: { move_number: desc }
                limit: 1
            ) {
                id
                moveNumber: move_number
                playerNumber: player_number
                outcome
                state
                move: move_data
                signatures {
                    publicKey: public_key
                    signature
                }
            }
        }
    }
`;

export const SUB_TOKENS = gql`
    subscription Tokens {
        tokens: token(order_by: { id: asc }) {
            id
            metadata
        }
    }
`;

const wsLink = new WebSocketLink({
    uri: Constants.graphQlWs,
    options: {
        reconnect: true,
    },
});

const httpLink = new HttpLink({
    uri: Constants.graphQlHttp,
});

// Split between http / websocket depending on the operation
const splitLink = split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
    },
    wsLink,
    httpLink,
);

const client = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache(),
});

export default client;
