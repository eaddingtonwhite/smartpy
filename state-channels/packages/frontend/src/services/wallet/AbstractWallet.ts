import { TezosToolkit } from '@taquito/taquito';
import { Constants } from 'state-channels-common';
import { Signer } from './Signer';

abstract class AbstractWallet {
    protected source;
    protected rpc: string = Constants.rpc;
    protected pkh?: string;
    protected tezos: TezosToolkit;

    constructor(source: string, public signer: Signer) {
        this.tezos = new TezosToolkit(this.rpc);
        this.source = source;
    }

    /**
     * @description Set RPC address.
     *
     * @returns {void}
     */
    public async setRPC(rpc: string) {
        this.rpc = rpc;
    }

    /**
     * @description Retrieve the Public Key Hash of the account that is currently in use by the wallet.
     *
     * @param options Options to use while fetching the PKH.
     *
     * @returns A promise that resolves to the Public Key Hash string.
     */
    public async getPkh() {
        return this.pkh;
    }

    /**
     * @description Return the public key of the account used by the signer
     */
    public publicKey = () => {
        return this.signer.publicKey();
    };

    /**
     * @description Optionally return the secret key of the account used by the signer
     */
    public secretKey = () => {
        return this.signer.secretKey();
    };
}

export default AbstractWallet;
