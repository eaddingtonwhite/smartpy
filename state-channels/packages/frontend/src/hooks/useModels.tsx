import { useSubscription } from '@apollo/client';
import { SC_Model } from 'state-channels-common';

import { SUB_MODELS } from 'src/services/graphql';

function useModels(): Record<string, SC_Model> {
    const { data } = useSubscription<{ models: SC_Model[] }>(SUB_MODELS);
    return data?.models.reduce<Record<string, SC_Model>>((acc, model) => ({ ...acc, [model.name]: model }), {}) || {};
}

export default useModels;
