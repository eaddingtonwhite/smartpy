import { useSubscription } from '@apollo/client';
import { SC_Token } from 'state-channels-common';

import { SUB_TOKENS } from 'src/services/graphql';

function useTokens(): Record<number, SC_Token> {
    const { data } = useSubscription<{ tokens: SC_Token[] }>(SUB_TOKENS);
    return (
        data?.tokens.reduce<Record<number, SC_Token>>(
            (acc, { id, metadata }) => ({ ...acc, [id]: { id, metadata } }),
            {},
        ) || {}
    );
}

export default useTokens;
