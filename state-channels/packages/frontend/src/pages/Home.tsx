import React from 'react';
import RouterButton from '../components/base/RouterButton';
import { Grid } from '@material-ui/core';

const Home: React.FC = () => (
    <Grid container direction="row" justifyContent="center" alignItems="center" spacing={1}>
        <Grid item xs={3}>
            <RouterButton to="/channels" fullWidth>
                Channels
            </RouterButton>
        </Grid>
        <Grid item xs={3}>
            <RouterButton to="/models" fullWidth>
                Models
            </RouterButton>
        </Grid>
    </Grid>
);

export default Home;
