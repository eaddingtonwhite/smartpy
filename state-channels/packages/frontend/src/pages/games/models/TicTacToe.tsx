import React from 'react';
import { SigningType } from '@airgap/beacon-sdk';
import type { ParamsWithKind } from '@taquito/taquito';

import { Alert, Chip, Grid, Paper, Rating, Theme, Typography } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/styles';
import CircleIcon from '@material-ui/icons/CircleOutlined';
import CrossIcon from '@material-ui/icons/CloseOutlined';

import {
    Api,
    Constants,
    EntryPoints,
    GameConstants,
    ModelKind,
    Models,
    Packers,
    SC_Game,
    SC_GameMoveAction,
} from 'state-channels-common';

import Wallet from 'src/services/wallet';
import Logger from 'src/services/logger';
import Button from 'src/components/base/Button';
import useWalletContext from 'src/hooks/useWalletContext';
import { OpKind } from '@taquito/rpc';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        board: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: 300,
            height: 300,
            margin: 10,
        },
        cell: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: 64,
            height: 64,
            fontSize: '32pt',
        },
        divider: {
            margin: theme.spacing(1),
        },
    }),
);

interface GameHeaderProps {
    outcome?: Record<string, string>;
    currentPlayerNumber: number;
    localPlayerNumber: number;
    latestMove?: SC_GameMoveAction<ModelKind.tictactoe>;
    pendingMove?: SC_GameMoveAction<ModelKind.tictactoe>;
    onAcceptMovement: () => void;
}

const GameHeader: React.FC<GameHeaderProps> = ({
    outcome,
    localPlayerNumber,
    currentPlayerNumber,
    pendingMove,
    latestMove,
    onAcceptMovement,
}) => {
    if (outcome) {
        for (const o in outcome) {
            switch (o) {
                case 'game_finished':
                    return (
                        <Typography textAlign="center" variant="overline">
                            Game finished (
                            {localPlayerNumber === Number(outcome[o].split('_')[1]) ? 'You Won!' : 'You Lost!'})
                        </Typography>
                    );
                case 'player_inactive':
                    return (
                        <Typography textAlign="center" variant="overline">
                            Player inactive ({localPlayerNumber === Number(outcome[o]) ? 'You Won!' : 'You Lost!'})
                        </Typography>
                    );
                case 'player_double_played':
                    return (
                        <Typography textAlign="center" variant="overline">
                            Player double played ({localPlayerNumber === Number(outcome[o]) ? 'You Won!' : 'You Lost!'})
                        </Typography>
                    );
            }
        }
    }
    switch (latestMove?.outcome) {
        case 'player_1_won':
        case 'player_2_won':
            return (
                <Typography textAlign="center" variant="overline">
                    {localPlayerNumber === Number(latestMove.outcome.split('_')[1]) ? 'You Won!' : 'You Lost!'}
                </Typography>
            );
        case 'draw':
            return (
                <Typography textAlign="center" variant="overline">
                    The game ended with draw!
                </Typography>
            );
    }
    if (pendingMove) {
        if (localPlayerNumber === pendingMove?.playerNumber) {
            return (
                <Paper sx={{ padding: 2 }}>
                    {pendingMove.outcome ? (
                        <Alert variant="outlined" severity="warning" icon={false}>
                            This move contains the following outcome: <Chip label={pendingMove.outcome} />
                        </Alert>
                    ) : (
                        <Alert variant="outlined" severity="info" icon={false}>
                            You must accept the opponent move before procceeding
                        </Alert>
                    )}
                    <Button fullWidth onClick={onAcceptMovement}>
                        Accept movement
                    </Button>
                </Paper>
            );
        } else {
            return (
                <Alert variant="outlined" severity="info" icon={false}>
                    Waiting for the opponent to accept your move
                </Alert>
            );
        }
    }

    if (localPlayerNumber !== currentPlayerNumber) {
        return (
            <Typography textAlign="center" variant="overline">
                It is not your turn yet
            </Typography>
        );
    }

    return (
        <Typography textAlign="center" variant="overline">
            It is your turn
        </Typography>
    );
};

interface OwnProps {
    game: SC_Game<ModelKind.tictactoe>;
    onError: (msg: string) => void;
    onSuccess: (msg: string) => void;
}

const TicTacToe: React.FC<OwnProps> = ({ game, onError, onSuccess }) => {
    const classes = useStyles();
    const { pkh, publicKey } = useWalletContext();
    const [move, setMove] = React.useState<{ row: number; col: number }>();

    const pendingMove: SC_GameMoveAction<ModelKind.tictactoe> | undefined = React.useMemo(
        () => (game?.pendingMove?.length ? game.pendingMove[0] : undefined),
        [game],
    );
    const latestMove: SC_GameMoveAction<ModelKind.tictactoe> | undefined = React.useMemo(
        () => (game?.latestMove?.length ? game.latestMove[0] : undefined),
        [game],
    );
    const board = React.useMemo(() => {
        if (pendingMove?.state) {
            return Packers.Game.unpackState(ModelKind.tictactoe, pendingMove.state);
        } else if (latestMove) {
            return Packers.Game.unpackState(ModelKind.tictactoe, latestMove.state);
        } else if (game?.state) {
            return Packers.Game.unpackState(ModelKind.tictactoe, game.state);
        }

        return [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
        ];
    }, [pendingMove, latestMove, game]);

    const enabled = React.useMemo(
        () =>
            !pendingMove &&
            !latestMove?.outcome &&
            !game.outcome &&
            game.players[pkh!] === (latestMove?.playerNumber || game.currentPlayer),
        [game, pkh, latestMove, pendingMove],
    );

    const playerNumber = React.useMemo(() => game.players[pkh!], [game, pkh]);

    const acceptMove = React.useCallback(async () => {
        try {
            if (game && pkh && publicKey && pendingMove) {
                const players = Object.entries(game.players).reduce<Record<number, string>>(
                    (acc, [address, number]) => ({ ...acc, [number]: address }),
                    {},
                );
                const boardState = JSON.parse(JSON.stringify(board));
                boardState[pendingMove.move.row][pendingMove.move.col] = 0;
                const moveResult = await Models[ModelKind.tictactoe].offchain_play({
                    player: players[latestMove?.playerNumber || game.currentPlayer],
                    move: pendingMove.move,
                    game: {
                        modelID: game.modelID,
                        initParams: game.initParams,
                        settled: game.settled,
                        current: {
                            player: latestMove?.playerNumber || game.currentPlayer,
                            moveNumber: latestMove?.moveNumber || game.moveNumber,
                            outcome: latestMove?.outcome ? { game_finished: latestMove.outcome } : game.outcome,
                        },
                        players,
                        state: boardState,
                    },
                });

                const state = Packers.Game.packState(ModelKind.tictactoe, moveResult.state);
                const bytes = Packers.Game.packNewStateAction(game.id, moveResult.current, state);
                const signature = await Wallet.Beacon.signer.sign(bytes, undefined, SigningType.MICHELINE);

                await Api.Game.acceptGameMove(game.id, moveResult.current.moveNumber, {
                    publicKey,
                    signature: signature.prefixSig,
                });
            }
        } catch (e) {
            Logger.debug(e);
            if (e?.isAxiosError && Array.isArray(e.response.data)) {
                onError(
                    e.response.data.reduce(
                        (err: string, cur: any) =>
                            cur.with ? (err ? `${err}, ${cur.with.string}` : cur.with.string) : err,
                        '',
                    ),
                );
            } else {
                onError('Something went wrong!');
            }
        }
    }, [game, pkh, publicKey]);

    const submitMove = React.useCallback(
        async (move: { row: number; col: number }) => {
            try {
                if (game && pkh && publicKey) {
                    const players = Object.entries(game.players).reduce<Record<number, string>>(
                        (acc, [address, number]) => ({ ...acc, [number]: address }),
                        {},
                    );
                    const moveResult = await Models[ModelKind.tictactoe].offchain_play({
                        player: pkh,
                        move,
                        game: {
                            modelID: game.modelID,
                            initParams: game.initParams,
                            settled: game.settled,
                            current: {
                                player: latestMove?.playerNumber || game.currentPlayer,
                                moveNumber: latestMove?.moveNumber || game.moveNumber,
                                outcome: latestMove?.outcome ? { game_finished: latestMove.outcome } : game.outcome,
                            },
                            players,
                            state: board,
                        },
                    });

                    const state = Packers.Game.packState(ModelKind.tictactoe, moveResult.state);
                    const bytes = Packers.Game.packNewStateAction(game.id, moveResult.current, state);
                    const signature = await Wallet.Beacon.signer.sign(bytes, undefined, SigningType.MICHELINE);

                    await Api.Game.newGameMove(game.id, {
                        current: moveResult.current,
                        state: state,
                        move,
                        signature: {
                            publicKey,
                            signature: signature.prefixSig,
                        },
                    });
                }
            } catch (e) {
                Logger.debug(e);
                if (e?.isAxiosError && Array.isArray(e.response.data)) {
                    onError(
                        e.response.data.reduce(
                            (err: string, cur: any) =>
                                cur.with ? (err ? `${err}, ${cur.with.string}` : cur.with.string) : err,
                            '',
                        ),
                    );
                } else {
                    onError('Something went wrong!');
                }
            }
        },
        [game, pkh, publicKey],
    );

    const submitGameStateOnChain = React.useCallback(async () => {
        onError('');
        onSuccess('');
        if (game) {
            try {
                const ops: ParamsWithKind[] = [];
                // Build "new_game" call
                if (!game.onChain) {
                    const constants: GameConstants = {
                        channelID: game.channelID,
                        gameNonce: game?.nonce,
                        modelID: game?.modelID,
                        playDelay: game?.playDelay,
                        settlements: game?.settlements,
                        bonds: game?.bonds,
                        players: game?.players,
                    };
                    ops.push({
                        kind: OpKind.TRANSACTION,
                        to: Constants.platformAddress,
                        amount: 0,
                        parameter: EntryPoints.Game.newGame(ModelKind.tictactoe, constants, game.signatures),
                    });
                }
                // Build "update_state" call
                if (latestMove && latestMove?.moveNumber !== game.moveNumber) {
                    const parameters = EntryPoints.Game.updateState(
                        game.id,
                        {
                            moveNumber: latestMove.moveNumber,
                            player: latestMove.playerNumber,
                            outcome: latestMove?.outcome ? { game_finished: latestMove.outcome } : game.outcome,
                        },
                        latestMove.state,
                        latestMove.signatures,
                    );
                    ops.push({
                        kind: OpKind.TRANSACTION,
                        to: Constants.platformAddress,
                        amount: 0,
                        parameter: parameters,
                    });
                }
                // Build "settle" call
                if (latestMove?.outcome && !game.settled) {
                    ops.push({
                        kind: OpKind.TRANSACTION,
                        to: Constants.platformAddress,
                        amount: 0,
                        parameter: EntryPoints.Game.settle(game.id),
                    });
                }
                await Wallet.Beacon.transferBatch(ops);
            } catch (e) {
                Logger.debug(e);
                onError(e.message);
            }
        }
    }, [game, latestMove]);

    const handleMoveSelection = (row: number, col: number) => {
        setMove({ row, col });
    };

    const onConfirm = () => {
        if (move) {
            submitMove(move);
            setMove(undefined);
        }
    };

    return (
        <Grid
            xs={12}
            item
            display="flex"
            justifyContent="center"
            alignItems="center"
            flexDirection="column"
            padding={5}
        >
            <GameHeader
                outcome={game?.outcome}
                pendingMove={pendingMove}
                latestMove={latestMove}
                localPlayerNumber={game.players[pkh!]}
                currentPlayerNumber={latestMove?.playerNumber || game.currentPlayer}
                onAcceptMovement={acceptMove}
            />
            <div className={classes.board}>
                <Grid
                    container
                    justifyContent="stretch"
                    alignItems="stretch"
                    alignSelf="stretch"
                    justifySelf="stretch"
                    flexWrap="wrap"
                >
                    {board.reduce<JSX.Element[]>(
                        (acc, cur, row) => [
                            ...acc,
                            ...cur.map((pos, col) => (
                                <Grid
                                    item
                                    xs={4}
                                    display="flex"
                                    justifyContent="center"
                                    alignItems="center"
                                    border="solid"
                                    key={`${row}-${col}`}
                                >
                                    <div className={classes.cell}>
                                        {pos ? (
                                            pos === playerNumber ? (
                                                <CrossIcon
                                                    fontSize="inherit"
                                                    color={
                                                        pendingMove?.move.row === row && pendingMove.move.col === col
                                                            ? 'warning'
                                                            : undefined
                                                    }
                                                />
                                            ) : (
                                                <CircleIcon
                                                    fontSize="inherit"
                                                    color={
                                                        pendingMove?.move.row === row && pendingMove.move.col === col
                                                            ? 'warning'
                                                            : undefined
                                                    }
                                                />
                                            )
                                        ) : (
                                            <Rating
                                                onChange={() => handleMoveSelection(row, col)}
                                                size="large"
                                                icon={<CrossIcon fontSize="inherit" />}
                                                emptyIcon={<CrossIcon fontSize="inherit" style={{ opacity: 0.4 }} />}
                                                value={move?.col === col && move?.row == row ? 1 : 0}
                                                max={enabled ? 1 : 0}
                                            />
                                        )}
                                    </div>
                                </Grid>
                            )),
                        ],
                        [],
                    )}
                    <Grid item xs={12}>
                        {move ? (
                            <Button fullWidth onClick={onConfirm}>
                                Confirm Move
                            </Button>
                        ) : undefined}
                    </Grid>
                </Grid>
            </div>

            {!game.onChain || (!!latestMove && latestMove?.moveNumber !== game.moveNumber) ? (
                !game.settled && latestMove?.outcome ? (
                    <Button onClick={submitGameStateOnChain} fullWidth>
                        Settle game (On-Chain)
                    </Button>
                ) : (
                    <Button onClick={submitGameStateOnChain} fullWidth>
                        {game.onChain ? 'Update' : 'Submit'} game state (On-Chain)
                    </Button>
                )
            ) : undefined}
        </Grid>
    );
};

export default TicTacToe;
