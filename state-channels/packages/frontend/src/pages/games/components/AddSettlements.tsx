import React from 'react';

import {
    Divider,
    Fab,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    TextField,
    Theme,
    Typography,
} from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/styles';
import Add from '@material-ui/icons/Add';

import {
    SC_Settlement,
    SettlementID,
    Settlements,
    convertUnitNumber,
    TezUnit,
    SC_SettlementKind,
} from 'state-channels-common';

import BondIcon from 'src/components/base/BondIcon';
import useWalletContext from 'src/hooks/useWalletContext';
import useTokens from 'src/hooks/useTokens';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            padding: 20,
            borderRadius: 10,
            boxShadow: '0 5px 20px rgba(0,0,0,0.30), 0 10px 10px rgba(0,0,0,0.22)',
            display: 'flex',
            flexDirection: 'column',
        },
        button: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            marginTop: theme.spacing(2),
        },
        wrap: {
            overflowWrap: 'anywhere',
        },
        divider: {
            margin: theme.spacing(1),
        },
        alertMessage: {
            textAlign: 'center',
            flex: 1,
        },
    }),
);

interface OwnProps {
    player1?: string;
    player2?: string;
    onAdd: (settlement: SC_Settlement) => void;
}

const AddSettlements: React.FC<OwnProps> = ({ player1 = '', player2 = '', onAdd }) => {
    const classes = useStyles();
    const { pkh } = useWalletContext();
    const [sender, setSender] = React.useState<string>();
    const [recipient, setRecipient] = React.useState<string>();
    const [gameFinishedOutcome, setGameFinishedOutcome] = React.useState<SettlementID>();
    const [tokens, setToken] = React.useState<Record<string, number>>({});
    const [amounts, setAmount] = React.useState<Record<string, number>>({});
    const allTokens = useTokens();

    const handleTokenSelection = React.useCallback((e: SelectChangeEvent<number | SettlementID | string>) => {
        switch (e.target.name) {
            case `settlement_${SC_SettlementKind.GameFinished}_outcome`:
                setGameFinishedOutcome(e.target.value as SettlementID);
                break;
            case `settlement_${SC_SettlementKind.GameFinished}_sender`:
                setSender(e.target.value as string);
                break;
            case `settlement_${SC_SettlementKind.GameFinished}_recipient`:
                setRecipient(e.target.value as string);
                break;
            case `settlement_${SC_SettlementKind.PlayerInactive}_bond`:
                return setToken((tokens) => {
                    return {
                        ...tokens,
                        [SC_SettlementKind.PlayerInactive]: Number(e.target.value),
                    };
                });
            case `settlement_${SC_SettlementKind.PlayerDoublePlayed}_bond`:
                return setToken((tokens) => {
                    return {
                        ...tokens,
                        [SC_SettlementKind.PlayerDoublePlayed]: Number(e.target.value),
                    };
                });
            case `settlement_${SC_SettlementKind.GameFinished}_bond`:
                return setToken((tokens) => {
                    return {
                        ...tokens,
                        [SC_SettlementKind.GameFinished]: Number(e.target.value),
                    };
                });
        }
    }, []);

    const handleAmountChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        switch (e.target.id) {
            case `settlement_${SC_SettlementKind.PlayerDoublePlayed}_bond_amount`:
                return setAmount((amounts) => ({
                    ...amounts,
                    [SC_SettlementKind.PlayerDoublePlayed]: Number(e.target.value),
                }));
            case `settlement_${SC_SettlementKind.PlayerInactive}_bond_amount`:
                return setAmount((amounts) => ({
                    ...amounts,
                    [SC_SettlementKind.PlayerInactive]: Number(e.target.value),
                }));
            case `settlement_${SC_SettlementKind.GameFinished}_bond_amount`:
                return setAmount((amounts) => ({
                    ...amounts,
                    [SC_SettlementKind.GameFinished]: Number(e.target.value),
                }));
        }
    }, []);

    const handleAdd: React.MouseEventHandler<HTMLButtonElement> = React.useCallback(
        (event) => {
            switch (event.currentTarget.id) {
                case `add_${SC_SettlementKind.PlayerDoublePlayed}_settlement`:
                    {
                        const amount = amounts[SC_SettlementKind.PlayerDoublePlayed] || 0;
                        const tokenID = tokens[SC_SettlementKind.PlayerDoublePlayed] || 0;
                        onAdd({
                            kind: SC_SettlementKind.PlayerDoublePlayed,
                            value: 1,
                            bonds: {
                                [tokenID]: tokenID === 0 ? convertUnitNumber(amount, TezUnit.tez).toNumber() : amount,
                            },
                            sender: 1,
                            recipient: 2,
                        });
                        onAdd({
                            kind: SC_SettlementKind.PlayerDoublePlayed,
                            value: 2,
                            bonds: {
                                [tokenID]: tokenID === 0 ? convertUnitNumber(amount, TezUnit.tez).toNumber() : amount,
                            },
                            sender: 2,
                            recipient: 1,
                        });
                    }
                    break;
                case `add_${SC_SettlementKind.PlayerInactive}_settlement`:
                    {
                        const amount = amounts[SC_SettlementKind.PlayerInactive] || 0;
                        const tokenID = tokens[SC_SettlementKind.PlayerInactive] || 0;
                        onAdd({
                            kind: SC_SettlementKind.PlayerInactive,
                            value: 1,
                            bonds: {
                                [tokenID]: tokenID === 0 ? convertUnitNumber(amount, TezUnit.tez).toNumber() : amount,
                            },
                            sender: 1,
                            recipient: 2,
                        });
                        onAdd({
                            kind: SC_SettlementKind.PlayerInactive,
                            value: 2,
                            bonds: {
                                [tokenID]: tokenID === 0 ? convertUnitNumber(amount, TezUnit.tez).toNumber() : amount,
                            },
                            sender: 2,
                            recipient: 1,
                        });
                    }
                    break;
                case `add_${SC_SettlementKind.GameFinished}_settlement`: {
                    const amount = amounts[SC_SettlementKind.GameFinished] || 0;
                    const tokenID = tokens[SC_SettlementKind.GameFinished] || 0;
                    if (gameFinishedOutcome && sender && recipient) {
                        onAdd({
                            kind: SC_SettlementKind.GameFinished,
                            value: gameFinishedOutcome,
                            bonds: {
                                [tokenID]: tokenID === 0 ? convertUnitNumber(amount, TezUnit.tez).toNumber() : amount,
                            },
                            sender: pkh === player1 ? 1 : 2,
                            recipient: pkh === player2 ? 2 : 1,
                        });
                    }
                }
            }
        },
        [amounts, player1, player2, tokens, gameFinishedOutcome, sender, recipient],
    );

    if (player1 && player2) {
        return (
            <>
                <div className={classes.divider} />
                <Typography variant="caption">Game Finished</Typography>
                <Divider flexItem sx={{ marginBottom: 1, marginTop: 1 }} />
                <Grid container justifyContent="space-between">
                    <Grid item xs={5}>
                        <FormControl fullWidth size="small">
                            <InputLabel id={`settlement_${SC_SettlementKind.GameFinished}_outcome`}>Outcome</InputLabel>
                            <Select
                                name={`settlement_${SC_SettlementKind.GameFinished}_outcome`}
                                labelId={`settlement_${SC_SettlementKind.GameFinished}_outcome`}
                                label="Outcome"
                                value={gameFinishedOutcome || ''}
                                onChange={handleTokenSelection}
                            >
                                {Object.keys(Settlements)?.map((settlement) => (
                                    <MenuItem value={settlement} key={settlement}>
                                        {settlement}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3}>
                        <FormControl fullWidth size="small" error={!!sender && sender === recipient}>
                            <InputLabel id={`settlement_${SC_SettlementKind.GameFinished}_sender`}>Sender</InputLabel>
                            <Select
                                name={`settlement_${SC_SettlementKind.GameFinished}_sender`}
                                labelId={`settlement_${SC_SettlementKind.GameFinished}_sender`}
                                label="Sender"
                                value={sender || ''}
                                onChange={handleTokenSelection}
                            >
                                {[player1, player2]?.map((player) => (
                                    <MenuItem value={player} key={player}>
                                        {player === pkh ? 'You' : 'Opponent'}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3}>
                        <FormControl fullWidth size="small" error={!!sender && sender === recipient}>
                            <InputLabel id={`settlement_${SC_SettlementKind.GameFinished}_recipient`}>
                                Recipient
                            </InputLabel>
                            <Select
                                name={`settlement_${SC_SettlementKind.GameFinished}_recipient`}
                                labelId={`settlement_${SC_SettlementKind.GameFinished}_recipient`}
                                label="Recipient"
                                value={recipient || ''}
                                onChange={handleTokenSelection}
                            >
                                {[player1, player2]?.map((player) => (
                                    <MenuItem value={player} key={player}>
                                        {player === pkh ? 'You' : 'Opponent'}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
                <div className={classes.divider} />
                <Grid container justifyContent="space-between">
                    <Grid item xs={5}>
                        <FormControl fullWidth size="small">
                            <InputLabel id="settlement_bond_id">Bond</InputLabel>
                            <Select
                                name={`settlement_${SC_SettlementKind.GameFinished}_bond`}
                                labelId="settlement_bond_id"
                                label="Bond"
                                value={tokens[SC_SettlementKind.GameFinished] || 0}
                                onChange={handleTokenSelection}
                            >
                                {Object.values(allTokens || []).map((token) => (
                                    <MenuItem value={token.id} key={token.id}>
                                        <BondIcon token={token} />
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3} display="flex" justifyContent="center" alignItems="center">
                        <TextField
                            fullWidth
                            id={`settlement_${SC_SettlementKind.GameFinished}_bond_amount`}
                            required
                            type="number"
                            inputProps={{
                                min: 0,
                            }}
                            value={amounts[SC_SettlementKind.GameFinished] || 0}
                            onChange={handleAmountChange}
                            label="Amount"
                            size="small"
                        />
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3} display="flex" justifyContent="center" alignItems="center">
                        <Fab
                            color="primary"
                            id={`add_${SC_SettlementKind.GameFinished}_settlement`}
                            variant="extended"
                            size="small"
                            onClick={handleAdd}
                            sx={{ width: '100%' }}
                        >
                            <Add sx={{ mr: 0.1 }} /> Add
                        </Fab>
                    </Grid>
                </Grid>
                <Typography variant="caption" sx={{ marginTop: 2 }}>
                    Player Inactive
                </Typography>
                <Divider flexItem sx={{ marginBottom: 2, marginTop: 1 }} />
                <Grid container justifyContent="space-between">
                    <Grid item xs={5}>
                        <FormControl fullWidth size="small">
                            <InputLabel id="settlement_bond_id">Bond</InputLabel>
                            <Select
                                name={`settlement_${SC_SettlementKind.PlayerInactive}_bond`}
                                labelId="settlement_bond_id"
                                label="Bond"
                                value={tokens[SC_SettlementKind.PlayerInactive] || 0}
                                onChange={handleTokenSelection}
                            >
                                {Object.values(allTokens || []).map((token) => (
                                    <MenuItem value={token.id} key={token.id}>
                                        <BondIcon token={token} />
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3} display="flex" justifyContent="center" alignItems="center">
                        <TextField
                            fullWidth
                            id={`settlement_${SC_SettlementKind.PlayerInactive}_bond_amount`}
                            required
                            type="number"
                            inputProps={{
                                min: 0,
                            }}
                            value={amounts[SC_SettlementKind.PlayerInactive] || 0}
                            onChange={handleAmountChange}
                            label="Amount"
                            size="small"
                        />
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3} display="flex" justifyContent="center" alignItems="center">
                        <Fab
                            id={`add_${SC_SettlementKind.PlayerInactive}_settlement`}
                            color="primary"
                            aria-label="add player inactive settlement"
                            variant="extended"
                            size="small"
                            onClick={handleAdd}
                            sx={{ width: '100%' }}
                        >
                            <Add sx={{ mr: 0.1 }} /> Add
                        </Fab>
                    </Grid>
                </Grid>
                <Typography variant="caption" sx={{ marginTop: 2 }}>
                    Player Double Played
                </Typography>
                <Divider flexItem sx={{ marginBottom: 2, marginTop: 1 }} />
                <Grid container justifyContent="space-between">
                    <Grid item xs={5}>
                        <FormControl fullWidth size="small">
                            <InputLabel id="settlement_double_played_bond_id">Bond</InputLabel>
                            <Select
                                name={`settlement_${SC_SettlementKind.PlayerDoublePlayed}_bond`}
                                labelId="settlement_double_played_bond_id"
                                label="Bond"
                                value={tokens[SC_SettlementKind.PlayerDoublePlayed] || 0}
                                onChange={handleTokenSelection}
                            >
                                {Object.values(allTokens || []).map((token) => (
                                    <MenuItem value={token.id} key={token.id}>
                                        <BondIcon token={token} />
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3} display="flex" justifyContent="center" alignItems="center">
                        <TextField
                            fullWidth
                            id={`settlement_${SC_SettlementKind.PlayerDoublePlayed}_bond_amount`}
                            required
                            type="number"
                            inputProps={{
                                min: 0,
                            }}
                            value={amounts[SC_SettlementKind.PlayerDoublePlayed] || 0}
                            onChange={handleAmountChange}
                            label="Amount"
                            size="small"
                        />
                    </Grid>
                    <Divider orientation="vertical" flexItem />
                    <Grid item xs={3} display="flex" justifyContent="center" alignItems="center">
                        <Fab
                            id={`add_${SC_SettlementKind.PlayerDoublePlayed}_settlement`}
                            color="primary"
                            aria-label="add player double played settlement"
                            variant="extended"
                            size="small"
                            onClick={handleAdd}
                            sx={{ width: '100%' }}
                        >
                            <Add sx={{ mr: 0.1 }} /> Add
                        </Fab>
                    </Grid>
                </Grid>
            </>
        );
    }

    return null;
};

export default AddSettlements;
