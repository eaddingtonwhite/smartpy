import React from 'react';
import { useSubscription } from '@apollo/client';
import {
    styled,
    TableCell,
    TableHead as MuiTableHead,
    TableBody as MuiTableBody,
    TableRow as MuiTableRow,
    Chip,
    IconButton,
    Box,
    Typography,
    Alert,
    Theme,
    Dialog,
    Collapse,
} from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/styles';

import PlayIcon from '@material-ui/icons/PlayArrow';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { SC_Game, Time } from 'state-channels-common';

import Table from '../../../components/base/Table';
import CopyButton from '../../../components/base/CopyButton';
import { SUB_GAMES } from '../../../services/graphql';
import { copyToClipboard } from '../../../utils/clipboard';
import useWalletContext from '../../../hooks/useWalletContext';

import RouterFab from '../../../components/base/RouterFab';
import GameInfo from './GameInfo';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        divider: {
            margin: theme.spacing(1),
        },
        alertMessage: {
            textAlign: 'center',
            flex: 1,
        },
    }),
);

const StyledTableRow = styled<any>(MuiTableRow)(({ theme }) => ({
    textDecoration: 'none',
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td': {
        border: 0,
    },
}));

const extractOutcome = (outcome?: Record<string, string>): string => {
    if (outcome) {
        for (const variant in outcome) {
            switch (variant) {
                case 'game_finished':
                    return `Finished (${outcome[variant]})`;
                case 'player_double_played':
                    return `Player (${outcome[variant]}) double played`;
                case 'player_inactive':
                    return `Player inactive (${outcome[variant]})`;
            }
        }
    }

    return 'In Progress';
};

interface TableRowProps {
    address: string;
    game: SC_Game<any>;
}

const GameRow: React.FC<TableRowProps> = ({ game }) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [error, setError] = React.useState<string>();
    const [success, setSuccess] = React.useState<string>();

    return (
        <>
            <StyledTableRow>
                <TableCell component="th" scope="row">
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    <CopyButton
                        onClick={() => copyToClipboard(game.id)}
                        label={game.id}
                        sx={{ maxWidth: 120 }}
                    ></CopyButton>
                </TableCell>

                <TableCell component="th" scope="row">
                    {extractOutcome(game.outcome)}
                </TableCell>
                <TableCell component="th" scope="row" align="right">
                    {Time.prettifyTimestamp(game.updatedAt)}
                </TableCell>
                <TableCell component="th" scope="row" align="right">
                    <Chip label={game.onChain ? 'OnChain' : 'Offchain'} color={game.onChain ? 'success' : 'warning'} />
                </TableCell>
                <TableCell component="th" scope="row" align="right">
                    <RouterFab
                        variant="extended"
                        to={`/channels/${game.channelID}/games/${game.id}`}
                        color="primary"
                        size="small"
                    >
                        <PlayIcon sx={{ mr: 1 }} />
                        Play
                    </RouterFab>
                </TableCell>
            </StyledTableRow>
            <StyledTableRow>
                <TableCell colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <GameInfo game={game} gameAccepted={true} />
                    </Collapse>
                </TableCell>
            </StyledTableRow>

            <Dialog open={!!success} onClose={() => setSuccess('')}>
                <Alert variant="outlined" severity="success" icon={false} classes={{ message: classes.alertMessage }}>
                    Bond update was submitted successfully!
                </Alert>
            </Dialog>
            <Dialog open={!!error} onClose={() => setError('')}>
                <Alert variant="outlined" severity="error" icon={false} classes={{ message: classes.alertMessage }}>
                    {error}
                </Alert>
            </Dialog>
        </>
    );
};

interface OwnProps {
    channelID: string;
}

const TableTemplate = (Component: React.FC<OwnProps>) => (props: OwnProps) =>
    (
        <Table
            header={
                <MuiTableHead>
                    <MuiTableRow>
                        <TableCell></TableCell>
                        <TableCell>ID</TableCell>
                        <TableCell>State</TableCell>
                        <TableCell align="right">Last Update</TableCell>
                        <TableCell align="right">Submitted</TableCell>
                        <TableCell></TableCell>
                    </MuiTableRow>
                </MuiTableHead>
            }
            body={<MuiTableBody>{<Component {...props} />}</MuiTableBody>}
        />
    );

const GamesTable: React.FC<OwnProps> = ({ channelID }) => {
    const { pkh } = useWalletContext();
    const { loading, error, data } = useSubscription<{ games: SC_Game<any>[] }>(SUB_GAMES, {
        variables: { channelID },
    });

    if (loading || !pkh) {
        return (
            <MuiTableRow>
                <TableCell colSpan={6}>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Typography variant="overline" textAlign="center">
                            Loading...
                        </Typography>
                    </Box>
                </TableCell>
            </MuiTableRow>
        );
    }
    if (error) {
        return (
            <MuiTableRow>
                <TableCell colSpan={6}>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Typography variant="overline" textAlign="center">
                            {error?.message || 'Something went wrong'}
                        </Typography>
                    </Box>
                </TableCell>
            </MuiTableRow>
        );
    }
    if (!data?.games?.length) {
        return (
            <MuiTableRow>
                <TableCell colSpan={6}>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Typography variant="overline" textAlign="center">
                            No games yet
                        </Typography>
                    </Box>
                </TableCell>
            </MuiTableRow>
        );
    }

    return (
        <>
            {data.games.map((game) => (
                <GameRow key={game.id} address={pkh} game={game} />
            ))}
        </>
    );
};

export default TableTemplate(GamesTable);
