import React from 'react';

import { TableHead, TableRow, TableCell, TableBody, Typography, Tooltip, IconButton } from '@material-ui/core';
import { DeleteForever as DeleteIcon } from '@material-ui/icons';

import { convertUnitWithSymbol, SC_Settlement, SC_SettlementKind, TezUnit } from 'state-channels-common';
import Table from 'src/components/base/Table';
import BondIcon from 'src/components/base/BondIcon';
import useTokens from 'src/hooks/useTokens';

interface OwnProps {
    settlements: SC_Settlement[];
    handleRemoveSettlement?: (kind: SC_SettlementKind, value: number | string) => void;
    yourPlayerNumber: number;
}

const Settlements: React.FC<OwnProps> = ({ settlements, yourPlayerNumber, handleRemoveSettlement }) => {
    const tokens = useTokens();
    return (
        <Table
            header={
                <TableHead>
                    <TableRow>
                        <TableCell>Kind</TableCell>
                        <TableCell>Value</TableCell>
                        <TableCell>Sender</TableCell>
                        <TableCell>Receiver</TableCell>
                        <TableCell>Bonds</TableCell>
                        {handleRemoveSettlement ? <TableCell></TableCell> : null}
                    </TableRow>
                </TableHead>
            }
            body={
                <TableBody>
                    {settlements.length
                        ? settlements.map(({ kind, value, sender, recipient, bonds }) => {
                              return (
                                  <TableRow key={`${kind}-${value}`}>
                                      <TableCell>
                                          <Typography variant="caption">{kind}</Typography>
                                      </TableCell>
                                      <TableCell>
                                          <Typography variant="caption">{value}</Typography>
                                      </TableCell>
                                      <TableCell size="small">
                                          <Typography variant="caption">
                                              {sender === yourPlayerNumber ? 'You' : 'Opponent'}
                                          </Typography>
                                      </TableCell>
                                      <TableCell size="small">
                                          <Typography variant="caption">
                                              {recipient === yourPlayerNumber ? 'You' : 'Opponent'}
                                          </Typography>
                                      </TableCell>
                                      <TableCell>
                                          <Table
                                              aria-label="bonds_table"
                                              header={
                                                  <TableHead>
                                                      <TableRow>
                                                          <TableCell>Bond</TableCell>
                                                          <TableCell align="right">Amount</TableCell>
                                                      </TableRow>
                                                  </TableHead>
                                              }
                                              body={
                                                  <TableBody>
                                                      {Object.entries(bonds).map(([tokenID, amount]) => (
                                                          <TableRow key={tokenID}>
                                                              <TableCell component="th" scope="row">
                                                                  <BondIcon token={tokens?.[Number(tokenID)]} />
                                                              </TableCell>
                                                              <TableCell component="th" scope="row" align="right">
                                                                  {Number(tokenID) === 0
                                                                      ? convertUnitWithSymbol(
                                                                            amount,
                                                                            TezUnit.uTez,
                                                                            TezUnit.tez,
                                                                        )
                                                                      : amount}
                                                              </TableCell>
                                                          </TableRow>
                                                      ))}
                                                  </TableBody>
                                              }
                                          />
                                      </TableCell>
                                      {handleRemoveSettlement ? (
                                          <TableCell align="right">
                                              <Tooltip title="Remove Settlement">
                                                  <IconButton onClick={() => handleRemoveSettlement(kind, value)}>
                                                      <DeleteIcon color="error" />
                                                  </IconButton>
                                              </Tooltip>
                                          </TableCell>
                                      ) : null}
                                  </TableRow>
                              );
                          })
                        : null}
                </TableBody>
            }
        />
    );
};

export default Settlements;
