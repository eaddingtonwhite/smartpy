import React from 'react';
import {
    Grid,
    Typography,
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    TableBody,
    TableRow,
    TableCell,
    TableHead,
    useTheme,
    useMediaQuery,
} from '@material-ui/core';
import { convertUnitWithSymbol, TezUnit, SC_Game, Packers } from 'state-channels-common';

import BondIcon from 'src/components/base/BondIcon';
import CodeBlock from 'src/components/base/CodeBlock';
import CopyButton from 'src/components/base/CopyButton';
import Settlements from './Settlements';
import useWalletContext from 'src/hooks/useWalletContext';
import Table from 'src/components/base/Table';
import useTokens from 'src/hooks/useTokens';

interface OwnProps {
    game: SC_Game<any>;
    gameAccepted?: boolean;
}

const GameInfo: React.FC<OwnProps> = ({ game, gameAccepted }) => {
    const { pkh } = useWalletContext();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.down('md'));
    const tokens = useTokens();

    const initParams = React.useMemo(() => (game ? Packers.unpackData(game.initParams) : undefined), [game]);
    if (!pkh) {
        return null;
    }

    return (
        <Grid container direction="column" justifyContent="center">
            {gameAccepted ? null : (
                <Grid item display="flex" justifyContent="center">
                    <Typography variant="overline" textAlign="center" color="text.secondary" gutterBottom>
                        You have not yet accepted this game
                    </Typography>
                </Grid>
            )}
            <Grid item display="flex" justifyContent="center">
                <Card variant="outlined" style={matches ? { maxWidth: 600 } : undefined}>
                    <CardHeader title={game.model.name} />
                    <CardMedia sx={{ height: 200 }} image={`/models/${game.model.name}.webp`} title={game.model.name} />
                    <CardContent>
                        <Table
                            body={
                                <TableBody>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Game Identifier
                                        </TableCell>
                                        <TableCell component="th" scope="row" align="right">
                                            <CopyButton label={game.id} color="info" sx={{ maxWidth: 200 }} />
                                        </TableCell>
                                    </TableRow>

                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Play Delay
                                        </TableCell>
                                        <TableCell component="th" scope="row" align="right">
                                            {game.playDelay / 60} Minutes
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Players
                                        </TableCell>
                                        <TableCell component="th" scope="row" align="right">
                                            <Table
                                                header={
                                                    <TableHead>
                                                        <TableRow>
                                                            <TableCell>Player Number</TableCell>
                                                            <TableCell align="right">Address</TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                }
                                                body={
                                                    <TableBody>
                                                        {Object.entries(game.players).map(([address, index]) => (
                                                            <TableRow key={address}>
                                                                <TableCell component="th" scope="row">
                                                                    {index}
                                                                    {String(address) === pkh ? ' (You)' : ' (Opponent)'}
                                                                </TableCell>
                                                                <TableCell component="th" scope="row" align="right">
                                                                    <CopyButton
                                                                        label={address}
                                                                        color="info"
                                                                        sx={{ maxWidth: 200 }}
                                                                    />
                                                                </TableCell>
                                                            </TableRow>
                                                        ))}
                                                    </TableBody>
                                                }
                                            />
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Bonds
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            <Grid container direction="row" justifyContent="space-between">
                                                {Object.entries(game.bonds).map(([player, bonds]) => (
                                                    <Grid
                                                        item
                                                        xs={5}
                                                        display="flex"
                                                        justifyContent="center"
                                                        key={player}
                                                    >
                                                        <Table
                                                            header={
                                                                <TableHead>
                                                                    <TableRow>
                                                                        <TableCell colSpan={2}>
                                                                            {String(game.players[pkh]) === player
                                                                                ? 'You'
                                                                                : 'Opponent'}
                                                                        </TableCell>
                                                                    </TableRow>
                                                                    <TableRow>
                                                                        <TableCell>Bond</TableCell>
                                                                        <TableCell align="right">Amount</TableCell>
                                                                    </TableRow>
                                                                </TableHead>
                                                            }
                                                            body={
                                                                <TableBody>
                                                                    {Object.entries(bonds).map(([tokenID, amount]) => (
                                                                        <TableRow key={`${player}-${tokenID}`}>
                                                                            <TableCell component="th" scope="row">
                                                                                <BondIcon
                                                                                    token={tokens?.[Number(tokenID)]}
                                                                                />
                                                                            </TableCell>
                                                                            <TableCell
                                                                                component="th"
                                                                                scope="row"
                                                                                align="right"
                                                                            >
                                                                                {Number(tokenID) === 0
                                                                                    ? convertUnitWithSymbol(
                                                                                          amount,
                                                                                          TezUnit.uTez,
                                                                                          TezUnit.tez,
                                                                                      )
                                                                                    : amount}
                                                                            </TableCell>
                                                                        </TableRow>
                                                                    ))}
                                                                </TableBody>
                                                            }
                                                        />
                                                    </Grid>
                                                ))}
                                            </Grid>
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Settlements
                                        </TableCell>
                                        <TableCell align="right" component="th" scope="row">
                                            <Settlements
                                                settlements={game.settlements}
                                                yourPlayerNumber={game.players[pkh]}
                                            />
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            Initialization Parameters
                                        </TableCell>
                                        <TableCell align="right" component="th" scope="row">
                                            <CodeBlock code={JSON.stringify(initParams, null, 4)} language="json" />
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            }
                        />
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    );
};

export default GameInfo;
