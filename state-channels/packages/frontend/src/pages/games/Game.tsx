import React from 'react';
import { useSubscription } from '@apollo/client';
import { useParams } from 'react-router-dom';
import { OpKind, ParamsWithKind } from '@taquito/taquito';

import { createStyles, makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core/styles';
import {
    Typography,
    Alert,
    Grid,
    Container,
    Divider,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Card,
    CardContent,
    Box,
} from '@material-ui/core';

import {
    Models,
    GameConstants,
    Packers,
    Api,
    MichelsonBuilders,
    SC_Game,
    Cryptography,
    EntryPoints,
    SC_GameMoveAction,
    Constants,
    Time,
} from 'state-channels-common';
import { SigningType } from '@airgap/beacon-sdk';

import Dialog from 'src/components/base/Dialog';
import CopyButton from 'src/components/base/CopyButton';
import { SUB_GAME } from 'src/services/graphql';
import useWalletContext from 'src/hooks/useWalletContext';
import Wallet from 'src/services/wallet';
import Fab from 'src/components/base/Fab';
import TicTacToe from './models/TicTacToe';
import Logger from 'src/services/logger';
import Table from 'src/components/base/Table';
import GameInfo from './components/GameInfo';
import CountDownTime from 'src/components/base/CountDownTime';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        divider: {
            margin: theme.spacing(1),
        },
        alertMessage: {
            textAlign: 'center',
            flex: 1,
        },
    }),
);

const Game: React.FC = () => {
    const { channelID, gameID } = useParams<{ channelID: string; gameID: string }>();
    const { pkh, publicKey } = useWalletContext();
    const { loading, error, data } = useSubscription<{ games: SC_Game<any>[] }>(SUB_GAME, {
        variables: { id: gameID },
    });
    const classes = useStyles();
    const [err, setError] = React.useState<string>();
    const [success, setSuccess] = React.useState<string>();
    const [showInfo, setShowInfo] = React.useState(false);

    const game = React.useMemo(
        () => (typeof data !== 'undefined' && data?.games.length ? data.games[0] : undefined),
        [data],
    );

    const latestMove: SC_GameMoveAction<any> | undefined = React.useMemo(
        () => (game?.latestMove?.length ? game.latestMove[0] : undefined),
        [game],
    );

    const gameAccepted = React.useMemo(() => game?.signatures.some(({ publicKey: pk }) => pk === publicKey), [game]);

    const acceptGame = React.useCallback(async () => {
        setError('');
        setSuccess('');
        try {
            const publicKey = await Wallet.Beacon.signer.publicKey();
            if (game) {
                if (game.signatures.length !== 1) {
                    return setError(`The oponnent did not signed the game yet!`);
                }
                const opponentSignature = game.signatures[0];

                // Prepare new game bytes
                const constants: GameConstants = {
                    channelID: channelID,
                    gameNonce: game?.nonce,
                    modelID: game?.modelID,
                    playDelay: game?.playDelay,
                    settlements: game?.settlements,
                    bonds: game?.bonds,
                    players: game?.players,
                };
                const initParams = MichelsonBuilders.Game.buildParameters(game.model.name);
                const bytes = Packers.Game.packNewGameAction(initParams, constants);

                // Validate opponent signature
                const publicKeyHash = await Cryptography.hashPublicKey(opponentSignature.publicKey);
                // Fail if public key is not owned by one of the players
                if (!Object.keys(game.players).includes(publicKeyHash)) {
                    return setError('This new game was not signed by the opponent.');
                }
                const isValid = await Cryptography.verifySignature(
                    bytes,
                    opponentSignature.signature,
                    opponentSignature.publicKey,
                );
                // Fail if the signature is invalid
                if (!isValid) {
                    return setError('The contents signed by the opponent are not the same you are signing.');
                }

                // Sign the new game confirmation
                const signature = await Wallet.Beacon.signer.sign(bytes, undefined, SigningType.MICHELINE);

                // Validate new game action before acepting the game (This will throw if something is invalid)
                await (Models as any)[game.model.name].offchain_new_game(constants, initParams, [
                    ...game.signatures,
                    {
                        publicKey,
                        signature: signature.prefixSig,
                    },
                ]);

                await Api.Game.acceptGame(game.id, {
                    publicKey,
                    signature: signature.prefixSig,
                });
            }
        } catch (e) {
            Logger.debug(e);
            setError(e.message);
        }
    }, [channelID, game]);

    const startStarving = async () => {
        setError('');
        setSuccess('');
        if (game) {
            try {
                const ops: ParamsWithKind[] = [];
                // Build "new_game" call
                if (!game.onChain) {
                    const constants: GameConstants = {
                        channelID: game.channelID,
                        gameNonce: game?.nonce,
                        modelID: game?.modelID,
                        playDelay: game?.playDelay,
                        settlements: game?.settlements,
                        bonds: game?.bonds,
                        players: game?.players,
                    };
                    ops.push({
                        kind: OpKind.TRANSACTION,
                        to: Constants.platformAddress,
                        amount: 0,
                        parameter: EntryPoints.Game.newGame(game.model.name, constants, game.signatures),
                    });
                }
                // Build "update_state" call
                if (latestMove && latestMove?.moveNumber !== game.moveNumber) {
                    const parameters = EntryPoints.Game.updateState(
                        game.id,
                        {
                            moveNumber: latestMove.moveNumber,
                            player: latestMove.playerNumber,
                            outcome: latestMove.outcome ? { game_finished: latestMove.outcome } : undefined,
                        },
                        latestMove.state,
                        latestMove.signatures,
                    );
                    ops.push({
                        kind: OpKind.TRANSACTION,
                        to: Constants.platformAddress,
                        amount: 0,
                        parameter: parameters,
                    });
                }
                // Build "settle" call
                if (latestMove?.outcome && !game.settled) {
                    ops.push({
                        kind: OpKind.TRANSACTION,
                        to: Constants.platformAddress,
                        amount: 0,
                        parameter: EntryPoints.Game.settle(game.id),
                    });
                }
                // Build "starving" call
                ops.push({
                    kind: OpKind.TRANSACTION,
                    to: Constants.platformAddress,
                    amount: 0,
                    parameter: EntryPoints.Game.starving(game.id, true),
                });
                await Wallet.Beacon.transferBatch(ops);
            } catch (e) {
                Logger.debug(e);
                setError(e.message);
            }
        }
    };

    const stopStarving = async () => {
        setError('');
        setSuccess('');
        if (game) {
            try {
                // Call "starving" entrypoint
                await Wallet.Beacon.transfer(EntryPoints.Game.starving(game.id, false));
            } catch (e) {
                Logger.debug(e);
                setError(e.message);
            }
        }
    };

    const starved = async (playerID: number) => {
        setError('');
        setSuccess('');
        if (game) {
            try {
                // Call "starved" entrypoint
                await Wallet.Beacon.transfer(EntryPoints.Game.starved(game.id, playerID));
            } catch (e) {
                Logger.debug(e);
                setError(e.message);
            }
        }
    };

    if (loading || !pkh) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Typography variant="h3" color="text.secondary" gutterBottom>
                    Loading...
                </Typography>
            </Container>
        );
    }

    if (error) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Typography variant="h3" color="text.secondary" gutterBottom>
                    {error?.message || 'Something went wrong'}
                </Typography>
            </Container>
        );
    }
    if (!game) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Typography variant="h3" color="text.secondary" gutterBottom>
                    The game does not exist.
                </Typography>
            </Container>
        );
    }
    if (!Object.keys(game.players).includes(pkh)) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Typography variant="h3" color="text.secondary" gutterBottom>
                    You are not a player in this game.
                </Typography>
            </Container>
        );
    }

    if (
        !game.onChain &&
        game.signatures.some(({ publicKey: pk }) => pk === publicKey) &&
        game.signatures.length !== Object.keys(game.players).length
    ) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Grid container direction="column" justifyContent="center" spacing={2}>
                    <Grid item display="flex" justifyContent="center">
                        <Typography variant="overline" textAlign="center" color="text.secondary" gutterBottom>
                            The opponent has not accepted the game yet.
                        </Typography>
                    </Grid>
                </Grid>
            </Container>
        );
    }

    if ((!game.onChain && !gameAccepted) || showInfo) {
        return (
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Grid container direction="column" justifyContent="center">
                    <Grid item display="flex" justifyContent="center">
                        <GameInfo game={game} gameAccepted={gameAccepted || showInfo} />
                    </Grid>
                    <Grid item display="flex" justifyContent="center" sx={{ marginTop: 5 }}>
                        {showInfo ? (
                            <Fab variant="extended" color="primary" onClick={() => setShowInfo(false)}>
                                Go back to game
                            </Fab>
                        ) : (
                            <Fab variant="extended" color="primary" onClick={acceptGame}>
                                Accept game
                            </Fab>
                        )}
                    </Grid>
                </Grid>
            </Container>
        );
    }

    return (
        <>
            <Container maxWidth="md" sx={{ margin: 5 }}>
                <Grid container justifyContent="center" alignItems="center">
                    <Grid
                        item
                        xs={12}
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        sx={{ marginBottom: 5 }}
                    >
                        <Fab onClick={() => setShowInfo(true)}>Show Game Info</Fab>
                    </Grid>
                    <Grid item xs={12} display="flex" flexDirection="column" sx={{ marginBottom: 5 }}>
                        <Typography variant="overline">Timeouts</Typography>
                        <Divider flexItem sx={{ marginBottom: 1 }} />
                        <Table
                            header={
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Player</TableCell>
                                        <TableCell align="right">Timeout</TableCell>
                                        <TableCell align="right">Timeout in</TableCell>
                                        <TableCell align="right">Last Update</TableCell>
                                        {game?.outcome || latestMove?.outcome ? null : (
                                            <TableCell align="right"></TableCell>
                                        )}
                                    </TableRow>
                                </TableHead>
                            }
                            body={
                                <TableBody>
                                    {game.timeouts ? (
                                        game.timeouts.map((timeout) => (
                                            <TableRow>
                                                <TableCell>{timeout.playerID}</TableCell>
                                                <TableCell align="right">
                                                    {Time.prettifyTimestamp(timeout.timeout)}
                                                </TableCell>
                                                <TableCell align="right">
                                                    <CountDownTime targetTime={timeout.timeout} />
                                                </TableCell>
                                                <TableCell align="right">
                                                    {Time.prettifyTimestamp(timeout.updatedAt)}
                                                </TableCell>

                                                {game?.outcome || latestMove?.outcome ? null : (
                                                    <TableCell align="right">
                                                        <Fab
                                                            variant="extended"
                                                            color="primary"
                                                            onClick={() => starved(timeout.playerID)}
                                                        >
                                                            Starved
                                                        </Fab>
                                                    </TableCell>
                                                )}
                                            </TableRow>
                                        ))
                                    ) : (
                                        <TableRow>
                                            <TableCell colSpan={6}>
                                                <Box
                                                    sx={{
                                                        display: 'flex',
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                    }}
                                                >
                                                    <Typography variant="overline" textAlign="center">
                                                        No active timeouts
                                                    </Typography>
                                                </Box>
                                            </TableCell>
                                        </TableRow>
                                    )}
                                </TableBody>
                            }
                        />
                    </Grid>

                    <Grid item md={12}>
                        <TicTacToe game={game} onError={setError} onSuccess={setSuccess} />
                    </Grid>
                    {game?.outcome || latestMove?.outcome ? null : game.timeouts.filter(
                          (t) => t.playerID !== game.players[pkh],
                      ).length ? (
                        <Grid item md={12} display="flex" justifyContent="center">
                            <Fab variant="extended" color="primary" onClick={stopStarving}>
                                Stop starving
                            </Fab>
                        </Grid>
                    ) : (
                        <Grid item md={12} display="flex" justifyContent="center">
                            <Fab variant="extended" color="primary" onClick={startStarving}>
                                Start starving
                            </Fab>
                        </Grid>
                    )}
                </Grid>
            </Container>

            <Dialog open={!!success} onClose={() => setSuccess('')}>
                <Grid container direction="column" justifyContent="center">
                    <Alert
                        variant="outlined"
                        severity="success"
                        icon={false}
                        classes={{ message: classes.alertMessage }}
                    >
                        Channel was successfully created!
                    </Alert>
                    <div className={classes.divider} />
                </Grid>
            </Dialog>

            <Dialog open={!!err} onClose={() => setError('')}>
                <Alert variant="outlined" severity="error" icon={false} classes={{ message: classes.alertMessage }}>
                    {err}
                </Alert>
            </Dialog>
        </>
    );
};

export default Game;
