import React from 'react';

import { createStyles, makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core/styles';
import { Typography, Alert, Grid, Box, Container } from '@material-ui/core';
import { EntryPoints, Hashers } from 'state-channels-common';

import Button from '../../components/base/Button';
import wallet from '../../services/wallet';
import TextField from '../../components/base/TextField';
import Dialog from '../../components/base/Dialog';
import { copyToClipboard } from '../../utils/clipboard';
import CopyButton from '../../components/base/CopyButton';
import Logger from '../../services/logger';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            padding: 20,
            borderRadius: 10,
            boxShadow: '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)',
            display: 'flex',
            flexDirection: 'column',
            border: '1px solid #FFF',
        },
        button: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
            marginTop: theme.spacing(2),
        },
        wrap: {
            overflowWrap: 'anywhere',
        },
        divider: {
            margin: theme.spacing(1),
        },
        alertMessage: {
            textAlign: 'center',
            flex: 1,
        },
    }),
);

const CreateModel: React.FC = () => {
    const classes = useStyles();
    const [error, setError] = React.useState<string>();
    const [success, setSuccess] = React.useState<string>();
    const [init, setInit] = React.useState<string>();
    const [apply, setApply] = React.useState<string>();
    const [name, setName] = React.useState<string>('');

    const modelID = React.useMemo(() => {
        if (init && apply) {
            try {
                const lambdaInit = JSON.parse(init);
                const lambdaApply = JSON.parse(apply);
                return Hashers.Model.hashID({
                    name,
                    init: lambdaInit,
                    apply: lambdaApply,
                });
            } catch (e) {
                Logger.debug(e);
            }
        }
    }, [init, apply]);

    const handleLambdaChange = React.useCallback(async (e: React.ChangeEvent<HTMLInputElement>) => {
        const id = e.target.id;
        try {
            if (id === 'init') {
                setInit(e.target.value);
            } else if (id === 'apply') {
                setApply(e.target.value);
            }
        } catch (e) {
            setError(e.message);
        }
    }, []);

    const handleChange: React.ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement> = React.useCallback((e) => {
        switch (e.target.id) {
            case 'name':
                setName(e.target.value);
        }
    }, []);

    const injectOperation = React.useCallback(async () => {
        try {
            if (!init) {
                return setError('You must provide an init lambda.');
            }
            if (!apply) {
                return setError('You must provide an apply lambda.');
            }
            if (!name) {
                return setError('You must provide a name.');
            }
            const lambdaInit = JSON.parse(init);
            const lambdaApply = JSON.parse(apply);

            const parameters = EntryPoints.Model.newModel({ name, init: lambdaInit, apply: lambdaApply });
            await wallet.Beacon.transfer(parameters);

            setSuccess(modelID);
        } catch (e) {
            setError(e.message);
        }
    }, [init, apply, modelID, name]);

    const clipboard = React.useCallback(() => {
        success && copyToClipboard(success);
    }, [success]);

    return (
        <>
            <Container maxWidth="sm" sx={{ margin: 5 }}>
                <Box
                    component="form"
                    className={classes.container}
                    sx={{ border: '1px solid', borderColor: 'primary.main' }}
                    autoComplete="off"
                    onSubmit={injectOperation}
                >
                    <Typography variant="h4" textAlign="center">
                        New Model
                    </Typography>
                    <div className={classes.divider} />
                    {error && (
                        <Alert variant="outlined" severity="error" onClose={() => setError('')}>
                            <Typography className={classes.wrap}>{error}</Typography>
                        </Alert>
                    )}
                    <div className={classes.divider} />
                    <TextField
                        id="name"
                        required
                        label="Model Name"
                        value={name}
                        onChange={handleChange}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        id="init"
                        required
                        label="(Lambda) Initilialization Logic"
                        value={init}
                        onChange={handleLambdaChange}
                        variant="outlined"
                        multiline
                        rows={10}
                    />
                    <TextField
                        id="apply"
                        required
                        label="(Lambda) Apply Logic"
                        value={apply}
                        onChange={handleLambdaChange}
                        variant="outlined"
                        multiline
                        margin="normal"
                        rows={10}
                    />
                    <div className={classes.divider} />
                    <Button className={classes.button} onClick={injectOperation}>
                        Inject Operation
                    </Button>
                </Box>
            </Container>
            <Dialog open={!!success} onClose={() => setSuccess('')}>
                <Grid container direction="column" justifyContent="center">
                    <Alert
                        variant="outlined"
                        severity="success"
                        icon={false}
                        classes={{ message: classes.alertMessage }}
                    >
                        Model was successfully created!
                    </Alert>
                    <div className={classes.divider} />
                    <CopyButton
                        sx={{ width: '100%' }}
                        color="success"
                        label={`Model ID: ${success}`}
                        onClick={clipboard}
                    />
                </Grid>
            </Dialog>
        </>
    );
};

export default CreateModel;
