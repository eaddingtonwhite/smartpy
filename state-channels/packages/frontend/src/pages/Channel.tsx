import React from 'react';
import { useParams } from 'react-router-dom';
import { Grid } from '@material-ui/core';

import RouterButton from '../components/base/RouterButton';

const Channel: React.FC = () => {
    const { id } = useParams<{ id: string }>();

    return (
        <Grid container direction="row" justifyContent="center" alignItems="center" spacing={1}>
            <Grid item justifyContent="center" alignItems="center">
                <RouterButton to="/push-bonds">Push bonds</RouterButton>
            </Grid>
        </Grid>
    );
};

export default Channel;
