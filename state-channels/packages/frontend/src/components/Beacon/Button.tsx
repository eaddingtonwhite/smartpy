import React from 'react';

import { useTheme } from '@material-ui/core/styles';
import { useMediaQuery } from '@material-ui/core';

import * as jdenticon from 'jdenticon';

import Button from '../base/Button';
import useWalletContext from '../../hooks/useWalletContext';
import BeaconFullIcon from './icons/BeaconFull';

const BeaconButton: React.FC = () => {
    const { pkh, connectWallet } = useWalletContext();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('sm'));

    const avatar = React.useMemo(() => {
        return <div dangerouslySetInnerHTML={{ __html: jdenticon.toSvg(pkh, 32) }} style={{ display: 'flex' }} />;
    }, [pkh]);

    return (
        <Button onClick={connectWallet} endIcon={matches ? pkh ? avatar : <BeaconFullIcon /> : undefined}>
            {pkh ? 'Re-Connect' : 'Connect'}
        </Button>
    );
};

export default BeaconButton;
