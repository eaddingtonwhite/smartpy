import React from 'react';

import CopyIcon from '@material-ui/icons/FileCopy';
import { Chip, ChipProps } from '@material-ui/core';
import { copyToClipboard } from '../../utils/clipboard';

interface OwnProps extends ChipProps {
    onClick?: () => void;
}

const CopyButton: React.FC<OwnProps> = ({ onClick, ...props }) => {
    const useClipboard = React.useCallback(() => {
        copyToClipboard(props.label as string);
    }, [props.label]);

    return (
        <Chip
            {...props}
            clickable
            onClick={onClick || useClipboard}
            onDelete={onClick || useClipboard}
            deleteIcon={<CopyIcon />}
        />
    );
};

export default CopyButton;
