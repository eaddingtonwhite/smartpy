import React from 'react';
import { TextField, TextFieldProps, IconButton } from '@material-ui/core';

import RefreshIcon from '@material-ui/icons/Autorenew';

const NonceTextField: React.FC<TextFieldProps> = ({ onClick, ...props }) => {
    return (
        <TextField
            {...props}
            disabled
            InputProps={{
                endAdornment: (
                    <IconButton onClick={onClick as () => void}>
                        <RefreshIcon />
                    </IconButton>
                ),
            }}
        />
    );
};

export default NonceTextField;
