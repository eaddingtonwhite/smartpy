import React from 'react';
import { TextField as MuiTextField, TextFieldProps } from '@material-ui/core';

const TextField: React.FC<TextFieldProps> = (props) => {
    return <MuiTextField variant="outlined" {...props}></MuiTextField>;
};

export default TextField;
