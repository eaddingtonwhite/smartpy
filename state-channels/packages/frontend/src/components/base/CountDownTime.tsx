import React from 'react';

import { Time } from 'state-channels-common';

interface OwnProps {
    targetTime: string;
}

const remaining = (targetTime: string) =>
    Time.getDuration(Time.getCurrentInstant().until(Time.instantFrom(targetTime)).seconds * Time.SECOND);

const CountDownTime: React.FC<OwnProps> = ({ targetTime }) => {
    const [elapsedTime, setElapsedTime] = React.useState(remaining(targetTime));

    React.useEffect(() => {
        const timeout = setTimeout(() => {
            setElapsedTime(remaining(targetTime));
        }, Time.SECOND);
        return () => {
            clearInterval(timeout);
        };
    });

    return <React.Fragment>{elapsedTime}</React.Fragment>;
};

export default CountDownTime;
