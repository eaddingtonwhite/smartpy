import React from 'react';
import { Dialog as MuiDialog, DialogContent, DialogTitle, Slide } from '@material-ui/core';
import type { TransitionProps } from '@material-ui/core/transitions';

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children?: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

interface OwnProps {
    open: boolean;
    onClose: () => void;
    title?: string;
    actions?: React.ReactElement;
}

const Dialog: React.FC<OwnProps> = (props) => {
    const { open, onClose, title, children, actions } = props;
    return (
        <MuiDialog open={open} TransitionComponent={Transition} keepMounted onClose={onClose} aria-describedby="dialog">
            {title ? <DialogTitle>{title}</DialogTitle> : null}
            <DialogContent>{children}</DialogContent>
            {actions}
        </MuiDialog>
    );
};

export default Dialog;
